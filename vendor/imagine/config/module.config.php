<?php

use Imagine\Gd\Imagine;
use Imagine\Image\Box;

return [
    'service_manager' => [
		'factories' => [
			'Imagine\Gd\Imagine' => function ($sm) {
				return new Imagine();
			},
		]
	]
];

