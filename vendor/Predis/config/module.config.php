<?php
return array(
	'service_manager' => array(
		'factories' => array(
			'PredisClient' => function($sm) {
                                $config = $sm->get('Config');
                                $settings = array();
                                if (isset($config['predis_settings']) && is_array($config['predis_settings'])) {
                                    $settings = $config['predis_settings'];
                                }
				return new Predis\Client($settings);
			}
		)
	)
);