<?php

namespace Predis;

use Zend\Mvc\MvcEvent;

use Zend\EventManager\StaticEventManager;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;

class Module implements AutoloaderProviderInterface {

	public function getAutoloaderConfig() {
		return array(
			'Zend\Loader\ClassMapAutoloader' => array(
				__DIR__ . '/autoload_classmap.php' 
			)
		);
	}

	public function getConfig() {
		return include __DIR__ . '/config/module.config.php';
	}

}
