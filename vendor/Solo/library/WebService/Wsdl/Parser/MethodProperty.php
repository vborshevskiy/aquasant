<?php

namespace Solo\WebService\Wsdl\Parser;

class MethodProperty {

	/**
	 *
	 * @var string
	 */
	private $type;

	/**
	 *
	 * @var string
	 */
	private $name;

	/**
	 *
	 * @param string $name        	
	 * @param string $type        	
	 */
	public function __construct($name = null, $type = null) {
		if (null !== $name) {
			$this->setName($name);
		}
		if (null !== $type) {
			$this->setType($type);
		}
	}

	/**
	 *
	 * @return string
	 */
	public final function getType() {
		return $this->type;
	}

	/**
	 *
	 * @param string $type        	
	 */
	public final function setType($type) {
		$this->type = $type;
	}

	/**
	 *
	 * @return string
	 */
	public final function getName() {
		return $this->name;
	}

	/**
	 *
	 * @param string $name        	
	 */
	public final function setName($name) {
		$this->name = $name;
	}

}

?>