<?php
namespace Solo\WebService\Wsdl\Parser;

class Method {

	/**
	 *
	 * @var string
	 */
	private $name;

	/**
	 *
	 * @var array
	 */
	private $properties = [];

	/**
	 *
	 * @param string $name        	
	 */
	public function __construct($name) {
		$this->name = $name;
	}

	/**
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 *
	 * @param string $name        	
	 * @param string $type        	
	 * @return \Solo\WebService\Wsdl\Parser\MethodProperty
	 */
	public function addProperty($name, $type) {
		$prop = new MethodProperty($name, $type);
		$this->properties[] = $prop;
		return $prop;
	}

	/**
	 *
	 * @return array
	 */
	public function getProperties() {
		return $this->properties;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasProperties() {
		return (0 < sizeof($this->properties));
	}
}

?>