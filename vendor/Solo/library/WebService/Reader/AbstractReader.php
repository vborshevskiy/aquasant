<?php
namespace Solo\WebService\Reader;

abstract class AbstractReader implements \Iterator, \ArrayAccess, \Countable {

	/**
	 *
	 * @var array
	 */
	protected $result;

	/**
	 *
	 * @var int
	 */
	private $position = 0;

	/**
	 *
	 * @param array $result        	
	 */
	public function __construct($result) {
		$this->result = $result;
	}

	/**
	 *
	 * @param int $position        	
	 * @return multitype:
	 */
	abstract protected function getRow($position);

	/**
	 *
	 * @return boolean
	 */
	public function isEmpty() {
		if (!empty($this->result) && (0 < sizeof($this->result)));
	}

	/**
	 *
	 * @param int $timestamp        	
	 * @return string
	 */
	public function parseDate($timestamp) {
		return date('Y-m-d H:i:s', $timestamp);
	}

	/**
	 * Iterator methods
	 */
	public function rewind() {
		$this->position = 0;
	}

	public function current() {
		return $this->getRow($this->position);
	}

	public function key() {
		return $this->position;
	}

	public function next() {
		++ $this->position;
	}

	public function valid() {
		return ($this->position < sizeof($this->result));
	}

	/**
	 * ArrayAccess methods
	 */
	public function offsetExists($offset) {
		$offset = intval($offset);
		return ((0 <= $offset) && ($offset < sizeof($this->result)));
	}

	public function offsetGet($offset) {
		if (! $this->offsetExists($offset)) {
			throw new Exception\OutOfBoundsException('Undefined index: ' . $offset);
		}
		return $this->getRow($offset);
	}

	public function offsetSet($index, $object) {
		throw new Exception\BadMethodCallException('Write methods disallows for WebService\Reader');
	}

	public function offsetUnset($index) {
		throw new Exception\BadMethodCallException('Write methods disallows for WebService\Reader');
	}

	/**
	 * Countable methods
	 */
	public function count() {
		return sizeof($this->result);
	}
}

?>