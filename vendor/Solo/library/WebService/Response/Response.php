<?php
namespace Solo\WebService\Response;

class Response {

	/**
	 *
	 * @var string
	 */
	protected $response = null;

	/**
	 *
	 * @var mixed
	 */
	protected $error = null;

	/**
	 *
	 * @param string $response        	
	 */
	public function __construct($response) {
		$this->setResponse($response);
        
	}

	/**
	 *
	 * @return string
	 */
	public function getResponse() {
		return $this->response;
	}

	/**
	 *
	 * @param string $response        	
	 */
	public function setResponse($response) {
		$this->response = $response;
	}

	/**
	 *
	 * @return mixed
	 */
	public function getError() {
		return $this->error;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasError() {
		return (null !== $this->error);
	}
}

?>