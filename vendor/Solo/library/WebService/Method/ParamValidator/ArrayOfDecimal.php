<?php
namespace Solo\WebService\Method\ParamValidator;

final class ArrayOfDecimal implements ParamValidatorInterface {

	public function isValid($value) {
		$r = true;
		if (is_array($value)) {
			foreach ($value as $v) {
				if (is_numeric($v) && is_float($v)) {
					$r &= true;
				} else {
					$r &= false;
				}
			}
		} else {
			$r &= false;
		}
		return $r;
	}
}
?>
