<?php
namespace Solo\WebService\Method\ParamValidator;

final class String implements ParamValidatorInterface {

	public function isValid($value) {
		return is_string($value);
	}
}
?>
