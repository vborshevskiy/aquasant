<?php
namespace Solo\WebService\Method\ParamValidator;

interface ParamValidatorInterface {

	/**
	 * 
	 * @param mixed $value
	 * @return boolean
	 */
	public function isValid($value);
}
?>
