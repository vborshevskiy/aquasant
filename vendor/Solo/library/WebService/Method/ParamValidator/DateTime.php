<?php
namespace Solo\WebService\Method\ParamValidator;

final class DateTime implements ParamValidatorInterface {

	public function isValid($value) {
		return (bool)(preg_match("/^\d{2}\-\d{2}\-\d{4}$/", $value) || preg_match("/^\d{2}\-\d{2}\-\d{4}\s\d{2}\:\d{2}\:\d{2}$/", $value));
	}
}
?>
