<?php
namespace Solo\WebService\Method\ParamValidator;

final class Decimal implements ParamValidatorInterface {

	public function isValid($value) {

        // это я похачил для передачи float полей в виде обернутых строк, то есть
        // 'soapParam' => '0.123',
        // Илья, 23.04.13
        if(is_float($value)) {
            $value = str_replace(',', '.', (string) $value);
        }

        if (preg_match("~^[0-9]+$~", $value) || preg_match("~^[0-9]+\.[0-9]+$~", $value)){

            return true;
        } else {

            return false;
        }

        // это оригинальная валидация
//		return (is_numeric($value) && is_float($value));
	}
}
