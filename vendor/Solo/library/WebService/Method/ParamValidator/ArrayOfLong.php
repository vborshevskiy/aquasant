<?php
namespace Solo\WebService\Method\ParamValidator;

final class ArrayOfLong implements ParamValidatorInterface {

	public function isValid($value) {
		$r = true;
		if (is_array($value)) {
			foreach ($value as $v) {
				if (is_long($v)) {
					$r &= true;
				} else {
					$r &= false;
				}
			}
		} else {
			$r &= false;
		}
		return $r;
	}
}
?>
