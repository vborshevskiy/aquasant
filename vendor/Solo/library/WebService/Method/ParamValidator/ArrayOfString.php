<?php
namespace Solo\WebService\Method\ParamValidator;

final class ArrayOfString implements ParamValidatorInterface {

	public function isValid($value) {
		$r = true;
		if (is_array($value)) {
			foreach ($value as $v) {
				if (is_string($v)) {
					$r &= true;
				} else {
					$r &= false;
				}
			}
		} else {
			$r &= false;
		}
		return $r;
	}
}
?>
