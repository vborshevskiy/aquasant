<?php
namespace Solo\WebService\Method\ParamValidator;

final class Int implements ParamValidatorInterface {

	public function isValid($value) {
		return is_int($value);
	}
}
?>
