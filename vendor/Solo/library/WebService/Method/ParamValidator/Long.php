<?php
namespace Solo\WebService\Method\ParamValidator;

final class Long implements ParamValidatorInterface {

	public function isValid($value) {
		return is_long($value);
	}
}
?>
