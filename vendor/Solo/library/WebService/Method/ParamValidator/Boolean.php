<?php
namespace Solo\WebService\Method\ParamValidator;

final class Boolean implements ParamValidatorInterface {

	public function isValid($value) {
		return is_bool($value);
	}
}
?>
