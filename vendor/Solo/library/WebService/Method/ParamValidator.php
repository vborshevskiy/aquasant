<?php
namespace Solo\WebService\Method;
use Solo\Inflector\Inflector;

class ParamValidator {

	/**
	 *
	 * @param string $type        	
	 * @param mixed $value        	
	 * @throws \RuntimeException
	 * @return boolean
	 */
	public static function isValid($type, $value) {
		$validator = self::createValidator($type);
		if (null === $validator) {
			throw new \RuntimeException('Failed to create param validator for type: ' . $type);
		}
		return $validator->isValid($value);
	}

	/**
	 *
	 * @param string $type        	
	 * @return ParamValidator\ParamValidatorInterface NULL
	 */
	private static function createValidator($type) {
		$includeFile = __DIR__ . '/ParamValidator/' . Inflector::camelize($type) . '.php';
		if (file_exists($includeFile)) {
			include_once $includeFile;
			$className = __NAMESPACE__ . '\\ParamValidator\\' . Inflector::camelize($type);
			if (class_exists($className)) {
				return new $className($type);
			}
		}
		return null;
	}
}
?>
