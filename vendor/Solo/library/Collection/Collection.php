<?php

namespace Solo\Collection;

use Zend\EventManager\EventManagerAwareInterface;
use Solo\EventManager\ProvidesEvents;

class Collection implements \ArrayAccess, \Iterator, \Countable, \Serializable, EventManagerAwareInterface {
	
	use ProvidesEvents;

	/**
	 *
	 * @var array
	 */
	protected $list = [];

	/**
	 *
	 * @param
	 *        	array | \Traversable $iterable
	 */
	public function __construct($items = null) {
		if (null !== $items) {
			$this->copyFrom($items);
		}
	}

	/**
	 * Add one object in collection
	 *
	 * @param mixed $object        	
	 * @param string $key        	
	 * @return boolean
	 */
	public function add($object, $key = null) {
		$args = $this->events()->prepareArgs(compact('object', 'key'));
		$this->events()->trigger(__METHOD__ . '.pre', $this, $args);
		
		if (null !== $key) {
			$this->list[$key] = $args['object'];
		} else {
			$this->list[] = $args['object'];
		}
		
		$this->events()->trigger(__METHOD__ . '.post', $this, $args);
		
		return true;
	}

	/**
	 * Add range of objects in collection
	 *
	 * @return boolean
	 */
	public function addRange() {
		$arg_list = func_get_args();
		$arg_num = func_num_args();
		for ($i = 0; $i < $arg_num; $i++) {
			if (!$this->add($arg_list[$i])) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Remove object from collection
	 *
	 * @param mixed $object        	
	 * @return boolean
	 */
	public function remove($object) {
		foreach ($this->list as $key => $obj) {
			if ($obj === $object) {
				$args = $this->events()->prepareArgs(compact('object'));
				$this->events()->trigger(__METHOD__ . '.pre', $this, $args);
				
				unset($this->list[$key]);
				
				$this->events()->trigger(__METHOD__ . '.post', $this, $args);
				
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Remove object by index or key in collection
	 *
	 * @param mixed $index        	
	 * @return boolean
	 */
	public function removeAt($index) {
		$result = false;
		if (array_key_exists($index, $this->list)) {
			$args = $this->events()->prepareArgs(compact('index'));
			$this->events()->trigger(__METHOD__ . '.pre', $this, $args);
			
			unset($this->list[$index]);
			
			$this->events()->trigger(__METHOD__ . '.post', $this, $args);
			
			return true;
		}
		
		return false;
	}

	/**
	 * Checks is specified object at first position in collection
	 *
	 * @param mixed $object        	
	 * @return boolean
	 */
	public function isFirst($object) {
		foreach ($this->list as $key => $obj) {
			if (($obj === $object) && (0 == $key)) return true;
		}
		return false;
	}

	/**
	 * Checks is specified object at last position in collection
	 *
	 * @param mixed $object        	
	 * @return boolean
	 */
	public function isLast($object) {
		$size = sizeof($this->list);
		foreach ($this->list as $key => $obj) {
			if (($obj === $object) && (($size - 1) == $key)) return true;
		}
		return false;
	}

	/**
	 * Remove all objects from collection
	 */
	public function clear() {
		$this->events()->trigger(__METHOD__ . '.pre', $this, []);
		
		$this->list = array();
		
		$this->events()->trigger(__METHOD__ . '.post', $this, []);
	}

	/**
	 * Check is exists specified object
	 *
	 * @param mixed $object        	
	 * @return boolean
	 */
	public function exists($object) {
		return in_array($object, $this->list, true);
	}

	/**
	 *
	 * @param mixed $index        	
	 * @return boolean
	 */
	public function keyExists($index) {
		return array_key_exists($index, $this->list);
	}

	/**
	 *
	 * @return array
	 */
	public function toArray() {
		return $this->list;
	}

	/**
	 *
	 * @param
	 *        	string | callback $callback
	 */
	public function apply($callback) {
		array_walk($this->list, $callback);
	}

	/**
	 *
	 * @param
	 *        	array | \Traversable $items
	 * @throws \InvalidArgumentException
	 */
	public function copyFrom($items) {
		if (!is_array($items) && !($items instanceof \Traversable)) {
			throw new \InvalidArgumentException('Copied items must be array or implements Traversable interface');
		}
		foreach ($items as $key => $item) {
			$this->add($item, $key);
		}
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see ArrayAccess::offsetExists()
	 */
	public function offsetExists($index) {
		return $this->keyExists($index);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see ArrayAccess::offsetGet()
	 */
	public function offsetGet($index) {
		if ($this->offsetExists($index)) {
			return $this->list[$index];
		}
		return null;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see ArrayAccess::offsetSet()
	 */
	public function offsetSet($index, $object) {
		if (0 > $index) {
			throw new Exception\OutOfBoundsException('Index value = ' . $index . ' is not valid for ' . __CLASS__);
		}
		if ($index) {
			$args = $this->events()->prepareArgs(compact('index', 'object'));
			$this->events()->trigger('set.pre', $this, $args);
			
			$this->list[$index] = $args['object'];
			
			$this->events()->trigger('set.post', $this, $args);
		} else {
			$this->add($object);
		}
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see ArrayAccess::offsetUnset()
	 */
	public function offsetUnset($index) {
		if ($this->offsetExists($index)) {
			$this->removeAt($index);
		}
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Iterator::current()
	 */
	public function current() {
		return current($this->list);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Iterator::key()
	 */
	public function key() {
		return key($this->list);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Iterator::next()
	 */
	public function next() {
		return next($this->list);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Iterator::rewind()
	 */
	public function rewind() {
		reset($this->list);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Iterator::valid()
	 */
	public function valid() {
		return (array_key_exists(key($this->list), $this->list));
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Countable::count()
	 */
	public function count() {
		return sizeof($this->list);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Serializable::serialize()
	 */
	public function serialize() {
		return serialize($this->list);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Serializable::unserialize()
	 */
	public function unserialize($data) {
		$this->list = unserialize($data);
	}

}

?>