<?php

namespace Solo\Benchmark\Measurement;

use Solo\Benchmark\Exception;

abstract class AbstractMeasurement {

	/**
	 *
	 * @var string
	 */
	private $name = '';

	/**
	 *
	 * @var mixed
	 */
	private $startState = null;

	/**
	 *
	 * @var mixed
	 */
	private $stopState = null;

	/**
	 *
	 * @var array
	 */
	private $marks = [];

	/**
	 *
	 * @var mixed
	 */
	private $pauseState = null;

	/**
	 *
	 * @var integer
	 */
	private $pauseStateTotal = 0;

	/**
	 *
	 * @return mixed
	 */
	public abstract function commitState();

	/**
	 *
	 * @param string $name        	
	 */
	public function __construct($name) {
		$this->setName($name);
	}

	/**
	 *
	 * @param string $name        	
	 * @throws Exception\InvalidArgumentException
	 * @return \Solo\Benchmark\Measurement\AbstractMeasurement
	 */
	public function setName($name) {
		if (empty($name)) {
			throw new Exception\InvalidArgumentException('Name can\'t be empty');
		}
		$this->name = $name;
		return $this;
	}

	/**
	 * Starts measurement
	 */
	public function start() {
		$this->startState = $this->commitState();
		$this->addMark(0, 'Benchmark start ...');
	}

	/**
	 * Stops measurement
	 * 
	 * @throws Exception\RuntimeException
	 */
	public function stop() {
		if (null !== $this->pauseState) {
			throw new Exception\RuntimeException('Benchmark has not restore after pause!');
		}
		$this->stopState = $this->commitState();
		$this->addMark($this->getResult(), 'Benchmark stop ...');
	}

	/**
	 * Pause measurement
	 * 
	 * @throws Exception\RuntimeException
	 */
	public function pause() {
		if (null === $this->startState) {
			throw new Exception\RuntimeException('Benchmark has not been started!');
		}
		if (null !== $this->pauseState) {
			throw new Exception\RuntimeException("Benchmark has been suspended!");
		}
		if (null !== $this->stopState) {
			throw new Exception\RuntimeException('Benchmark has been finished!');
		}
		$this->pauseState = $this->commitState();
		$this->addMark($this->getResult(), 'Benchmark paused ...');
	}

	/**
	 * Resume measurement
	 * 
	 * @throws Exception\RuntimeException
	 */
	public function resume() {
		if (null === $this->startState) {
			throw new Exception\RuntimeException('Benchmark has not been started!');
		}
		if (null === $this->pauseState) {
			throw new Exception\RuntimeException('Benchmark has not set pause!');
		}
		$this->pauseStateTotal += ($this->commitState() - $this->pauseState);
		$this->pauseState = null;
		$this->addMark($this->getResult(), 'Benchmark resume');
	}

	/**
	 * Mark measurement state by specified message
	 * 
	 * @param string $message        	
	 */
	public function mark($message) {
		$this->addMark($this->getResult(), $message);
	}

	/**
	 *
	 * @param integer $precision        	
	 * @return array
	 */
	public function getResultMarked($precision = null) {
		$marks = $this->marks;
		if (null !== $precision) {
			$ar = &$this->marks;
			$label = $this->name;
			array_walk($ar, function ($val, $key) use(&$ar, &$precision, $label) {
				$ar[$key][$label] = round($val['state'], $precision);
			});
		}
		return array(
			$label => $this->marks 
		);
	}

	/**
	 *
	 * @param integer $precision        	
	 * @return float
	 */
	public function getResult($precision = null) {
		$start = $this->startState;
		$stop = (null !== $this->stopState) ? $this->stopState : $this->commitState();
		$pause = (null !== $this->pauseStateTotal) ? $this->pauseStateTotal : 0;
		$result = $stop - $start - $pause;
		if (null !== $precision) {
			$result = round($result, $precision);
		}
		return $result;
	}

	/**
	 *
	 * @param mixed $state        	
	 * @param string $message        	
	 */
	private function addMark($state, $message) {
		$this->marks[] = array(
			'state' => $state,
			'message' => $message 
		);
	}

}

?>