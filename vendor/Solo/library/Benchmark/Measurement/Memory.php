<?php
namespace Solo\Benchmark\Measurement;

class Memory extends AbstractMeasurement {
	
	public function commitState() {
		return memory_get_usage();
	}
}

?>