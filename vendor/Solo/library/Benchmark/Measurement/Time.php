<?php
namespace Solo\Benchmark\Measurement;

class Time extends AbstractMeasurement {
	
	public function commitState() {
		return microtime(true);
	}
	
}

?>