<?php

namespace Solo\Benchmark;

use Solo\Inflector\Inflector;
use Solo\Benchmark\Measurement\AbstractMeasurement;

final class Benchmark {

	/**
	 *
	 * @var array
	 */
	private $measurements = [];

	/**
	 *
	 * @var integer
	 */
	private $resultPrecision = 5;

	public function __construct() {
		$types = array();
		if (0 < func_num_args()) {
			if (is_array(func_get_arg(0))) {
				$types = func_get_arg(0);
			} else {
				$types = func_get_args();
			}
		} else {
			$types = array(
				'time' 
			);
		}
		if (0 < sizeof($types)) {
			$this->loadMeasurements($types);
		}
	}

	/**
	 * return integer
	 */
	public function getResultPrecision() {
		return $this->resultPrecision;
	}

	/**
	 *
	 * @param integer $resultPrecision        	
	 * @throws Exception\InvalidArgumentException
	 * @return \Solo\Benchmark\Benchmark
	 */
	public function setResultPrecision($resultPrecision) {
		if (!is_integer($resultPrecision)) {
			throw new Exception\InvalidArgumentException('Result precision must be integer');
		}
		if (0 > $resultPrecision) {
			throw new Exception\InvalidArgumentException('Result precision must be greater than zero');
		}
		$this->resultPrecision = $resultPrecision;
		return $this;
	}

	/**
	 *
	 * @param array $types        	
	 */
	private function loadMeasurements(array $types) {
		foreach ($types as $type) {
			$measurement = $this->createMeasurement($type);
			if (null !== $measurement) {
				$this->measurements[$type] = $measurement;
			}
		}
	}

	/**
	 *
	 * @param string $type        	
	 * @return AbstractMeasurement | NULL
	 */
	private function createMeasurement($type) {
		$includeFile = __DIR__ . '/Measurement/' . Inflector::camelize($type) . '.php';
		if (file_exists($includeFile)) {
			include $includeFile;
			$className = __NAMESPACE__ . '\\Measurement\\' . Inflector::camelize($type);
			if (class_exists($className)) {
				return new $className($type);
			}
		}
		return null;
	}

	/**
	 * Starts measurements
	 */
	public function start() {
		array_walk($this->measurements, function ($measure) {
			$measure->start();
		});
	}

	/**
	 * Stops measurements
	 */
	public function stop() {
		array_walk($this->measurements, function ($measure) {
			$measure->stop();
		});
	}

	/**
	 * Mark measurement image by specified message
	 *
	 * @param string $message        	
	 */
	public function mark($message) {
		array_walk($this->measurements, function ($measure) use($message) {
			$measure->mark($message);
		});
	}

	/**
	 * Pauses measurements
	 */
	public function pause() {
		array_walk($this->measurements, function ($measure) {
			$measure->pause();
		});
	}

	/**
	 * Start measuremenets after pause
	 */
	public function restore() {
		array_walk($this->measurements, function ($measure) {
			$measure->restore();
		});
	}

	/**
	 *
	 * @param string $type        	
	 * @return multitype:
	 */
	public function getResultMarked($type = null) {
		if ((null === $type) && (1 == sizeof($this->measurements))) {
			$type = key($this->measurements);
		}
		if ((null !== $type) && isset($this->measurements[$type])) {
			return $this->measurements[$type]->getResultMarked($this->getResultPrecision());
		} else {
			$result = array();
			foreach ($this->measurements as $m) {
				$result = array_merge($result, $m->getResultMarked($this->getResultPrecision()));
			}
			return $result;
		}
	}

	/**
	 *
	 * @param string $type        	
	 * @return multitype:
	 */
	public function getResult($type = null) {
		if ((null === $type) && (1 == sizeof($this->measurements))) {
			$type = key($this->measurements);
		}
		if ((null !== $type) && isset($this->measurements[$type])) {
			return $this->measurements[$type]->getResult($this->getResultPrecision());
		} else {
			$result = array();
			foreach ($this->measurements as $m) {
				$result = array_merge($result, $m->getResult($this->getResultPrecision()));
			}
			return $result;
		}
	}

}

?>