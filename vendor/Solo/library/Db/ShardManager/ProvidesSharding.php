<?php

namespace Solo\Db\ShardManager;

use Solo\Db\QueryGateway\Feature\GlobalShardingFeature;

trait ProvidesSharding {

	/**
	 *
	 * @param string $name        	
	 * @param array $options        	
	 * @return string
	 */
	public function getShard($name, array $options) {
		if (GlobalShardingFeature::hasStaticShardManager()) {
			$shardManager = GlobalShardingFeature::getStaticShardManager();
			return $shardManager->get($name, $options);
		} else {
			return $name;
		}
	}

}

?>