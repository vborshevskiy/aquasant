<?php

namespace Solo\Db\ShardManager;

class ShardManager {

	/**
	 *
	 * @var array
	 */
	private $config;

	/**
	 *
	 * @param array $config        	
	 */
	public function __construct(array $config) {
		$this->config = $config;
	}

	/**
	 *
	 * @param string $name        	
	 * @param array $options        	
	 * @return string
	 */
	public function get($name, array $options) {
		$config = $this->config[$name];
		$shard = $config['template'];
		foreach ($config['parameters'] as $paramKey => $paramValue) {
			if (is_string($paramValue) && (0 === strcasecmp('variable', $paramValue))) {
				$shard = str_replace($paramKey, $paramKey . $options[$paramKey], $shard);
			} elseif (is_callable($paramValue)) {
				$shard = str_replace($paramKey, $paramKey . $paramValue($options[$paramKey]), $shard);
			}
		}
		return $shard;
	}

}

?>