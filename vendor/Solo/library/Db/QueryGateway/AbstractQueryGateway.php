<?php

namespace Solo\Db\QueryGateway;

use Zend\Db\Adapter\Adapter;

abstract class AbstractQueryGateway {

	/**
	 *
	 * @param string $name        	
	 * @return string
	 */
	public function quoteIdentifier($name) {
		return $this->getAdapter()->getPlatform()->quoteIdentifier($name);
	}

	/**
	 *
	 * @param string $value        	
	 * @return string
	 */
	public function quoteValue($value) {
		return $this->getAdapter()->getPlatform()->quoteValue($value);
	}

	/**
	 *
	 * @param string $name        	
	 * @return string
	 */
	public function formatParameterName($name) {
		return $this->getAdapter()->getDriver()->formatParameterName($name);
	}

	/**
	 *
	 * @return Adapter
	 */
	abstract protected function getAdapter();

}

?>