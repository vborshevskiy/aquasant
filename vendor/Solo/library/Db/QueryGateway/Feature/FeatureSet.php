<?php

namespace Solo\Db\QueryGateway\Feature;

use Solo\Db\QueryGateway\QueryGateway;

class FeatureSet {

	/**
	 *
	 * @var stop apply
	 */
	const APPLY_HALT = 'halt';

	/**
	 *
	 * @var QueryGateway
	 */
	protected $queryGateway = null;

	/**
	 *
	 * @var array
	 */
	protected $features = [];

	/**
	 *
	 * @param array $features        	
	 */
	public function __construct(array $features = []) {
		if (0 < sizeof($features)) {
			$this->addFeatures($features);
		}
	}

	/**
	 *
	 * @param array $features        	
	 * @return \Solo\Db\QueryGateway\Feature\FeatureSet
	 */
	public function addFeatures(array $features) {
		foreach ($features as $feature) {
			$this->addFeature($feature);
		}
		return $this;
	}

	/**
	 *
	 * @param AbstractFeature $feature        	
	 * @return \Solo\Db\QueryGateway\Feature\FeatureSet
	 */
	public function addFeature(AbstractFeature $feature) {
		$this->features[] = $feature;
		if (null !== $this->queryGateway) {
			$feature->setQueryGateway($this->queryGateway);
		}
		return $this;
	}

	/**
	 *
	 * @param QueryGateway $queryGateway        	
	 * @return \Solo\Db\QueryGateway\Feature\FeatureSet
	 */
	public function setQueryGateway(QueryGateway $queryGateway) {
		$this->queryGateway = $queryGateway;
		foreach ($this->features as $feature) {
			$feature->setQueryGateway($this->queryGateway);
		}
		return $this;
	}

	/**
	 * 
	 * @param string $method
	 * @param mixed $args
	 */
	public function apply($method, $args = null) {
		foreach ($this->features as $feature) {
			if (method_exists($feature, $method)) {
				if (null !== $args) {
					if (is_object($args)) {
						$args = [$args];
					}
				} else {
					$args = [];
				}
				$return = call_user_func_array(array(
					$feature,
					$method 
				), $args);
				if ($return === self::APPLY_HALT) {
					break;
				}
			}
		}
	}
	
	/**
	 * 
	 * @param array $args
	 * @return \ArrayObject
	 */
	public function prepareArgs(array $args) {
		return new \ArrayObject($args);
	}
	
	/**
	 * Remove all registered features
	 */
	public function clear() {
		$this->features = [];
	}

}

?>