<?php

namespace Solo\Db\QueryGateway\Feature;

use Solo\Db\Adapter\Adapter;

class GlobalAdapterFeature extends AbstractFeature {

	/**
	 * 
	 * @var Adapter
	 */
	private static $adapter = null;

	/**
	 * 
	 * @param Adapter $adapter
	 */
	public static function setStaticAdapter(Adapter $adapter) {
		self::$adapter = $adapter;
	}

	/**
	 * 
	 * @return \Zend\Db\Adapter\Adapter
	 */
	public static function getStaticAdapter() {
		return self::$adapter;
	}
	
	/**
	 * 
	 * @return boolean
	 */
	public static function hasStaticAdapter() {
		return (null !== self::$adapter);
	}
	
	public function preInitialize() {
		if (self::hasStaticAdapter() && !$this->queryGateway->hasAdapter()) {
			$this->queryGateway->setAdaper(self::getStaticAdapter());
		}
	}

}

?>