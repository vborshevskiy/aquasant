<?php

namespace Solo\Db\QueryGateway\Feature;

use Solo\Db\QueryGateway\QueryGateway;

class AbstractFeature {

	/**
	 *
	 * @var QueryGateway
	 */
	protected $queryGateway = null;

	/**
	 * 
	 * @param QueryGateway $queryGateway
	 * @return \Solo\Db\QueryGateway\Feature\AbstractFeature
	 */
	public function setQueryGateway(QueryGateway $queryGateway) {
		$this->queryGateway = $queryGateway;
		return $this;
	}

}

?>