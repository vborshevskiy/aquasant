<?php

namespace Solo\Db\QueryGateway\Feature;

use Solo\Db\BalancingManager\BalancingManager;

class GlobalBalancingFeature extends AbstractFeature {

	/**
	 *
	 * @var BalancingManager
	 */
	private static $staticBalancingManager = null;

	/**
	 *
	 * @param BalancingManager $balancingManager        	
	 */
	public static function setStaticBalancingManager(BalancingManager $balancingManager) {
		self::$staticBalancingManager = $balancingManager;
	}

	/**
	 *
	 * @return \Solo\Db\BalancingManager\BalancingManager
	 */
	public static function getStaticBalancingManager() {
		return self::$staticBalancingManager;
	}

	/**
	 *
	 * @return boolean
	 */
	public static function hasStaticBalancingManager() {
		return (null !== self::$staticBalancingManager);
	}

	public function preInitialize() {
		if (self::hasStaticBalancingManager()) {
			$this->queryGateway->setAdaper(self::getStaticBalancingManager()->getAnyAdapter());
		}
	}

	public function preQuery(\ArrayObject $args) {
		$this->initializeAdapterBySql($args->sql);
	}

	public function preCreateStatement(\ArrayObject $args) {
		$this->initializeAdapterBySql($args->sql);
	}

	private function initializeAdapterBySql($sql) {
		if ($this->sqlHasWriteMethods($sql)) {
			$this->queryGateway->setAdaper(self::getStaticBalancingManager()->getMasterAdapter());
		} else {
			if (!$this->queryGateway->getAdapter()->getDriver()->getConnection()->isConnected()) {
				$this->queryGateway->setAdaper(self::getStaticBalancingManager()->getAnyAdapter());
			}
		}
	}

	/**
	 *
	 * @param string $sql        	
	 * @return boolean
	 */
	private function sqlHasWriteMethods($sql) {
		if (false !== stripos($sql, 'insert')) return true;
		if (false !== stripos($sql, 'update')) return true;
		if (false !== stripos($sql, 'delete')) return true;
		if (false !== stripos($sql, 'drop table')) return true;
		if (false !== stripos($sql, 'create table')) return true;
		return false;
	}

}

?>