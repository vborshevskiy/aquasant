<?php

namespace Solo\Db\QueryGateway\Feature;

use Solo\Db\ShardManager\ShardManager;

class GlobalShardingFeature extends AbstractFeature {

	/**
	 *
	 * @var ShardManager
	 */
	private static $staticShardManager = null;

	/**
	 *
	 * @param ShardManager $shardManager        	
	 */
	public static function setStaticShardManager(ShardManager $shardManager) {
		self::$staticShardManager = $shardManager;
	}

	/**
	 *
	 * @return \Solo\Db\ShardManager\ShardManager
	 */
	public static function getStaticShardManager() {
		return self::$staticShardManager;
	}

	/**
	 *
	 * @return boolean
	 */
	public static function hasStaticShardManager() {
		return (null !== self::$staticShardManager);
	}

}

?>