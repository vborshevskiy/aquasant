<?php

namespace Solo\Db\QueryGateway\Feature;

use Zend\Stdlib\PriorityQueue;

class TemplatedSqlFeature extends AbstractFeature {

    /**
     *
     * @var string
     */
    private $template;

    /**
     *
     * @var PriorityQueue
     */
    private $processors;

    /**
     *
     * @param string $template        	
     */
    public function __construct($template) {
        $this->template = $template;
        $this->processors = new PriorityQueue();
    }

    /**
     *
     * @param callable $func        	
     * @param integer $priority        	
     * @return \Solo\Db\QueryGateway\Feature\TemplatedSqlFeature
     */
    public function addProcessor(callable $func, $priority = 1) {
        $this->processors->insert($func, $priority);
        return $this;
    }

    /**
     *
     * @param \ArrayObject $args        	
     */
    public function preQuery(\ArrayObject $args) {
        $args['sql'] = $this->processSql($args['sql'], $args['reload']);
    }

    /**
     *
     * @param \ArrayObject $args
     */
    public function preCreateStatement(\ArrayObject $args) {
        $args['sql'] = $this->processSql($args['sql'], $args['reload']);
    }

    /**
     *
     * @param string $sql
     * @param boolean $reload
     * @return string
     */
    private function processSql($sql, $reload = false) {
        $matches = [];
        preg_match_all($this->template, $sql, $matches);
        $searches = [];
        $replaces = [];
        foreach ($matches[0] as $index => $search) {
            $processorArgs = [];
            foreach ($matches as $key => $values) {
                $processorArgs[$key] = $values[$index];
            }
            $replace = '';
            foreach ($this->processors as $processor) {
                $replace = $processor($replace, $processorArgs, $reload);
            }
            $searches[] = $search;
            $replaces[] = $replace;
        }
        if (0 < sizeof($searches)) {
            $sql = str_replace($searches, $replaces, $sql);
        }
        return $sql;
    }

}
