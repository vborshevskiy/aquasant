<?php

namespace Solo\Db\QueryGateway;

use Solo\Db\Adapter\Adapter;
use Solo\Db\Exception\InvalidArgumentException;
use Solo\Db\QueryGateway\Feature\FeatureSet;
use Zend\Db\Adapter\AdapterInterface;
use Solo\Db\Exception\RuntimeException;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\ResultSet\ResultSetInterface;
use Zend\Db\Adapter\Driver\ResultInterface;
use Solo\Db\QueryGateway\Feature\AbstractFeature;

class QueryGateway extends AbstractQueryGateway {

    /**
     *
     * @var Adapter
     */
    private $adapter = null;

    /**
     *
     * @var FeatureSet
     */
    protected $featureSet = null;

    /**
     *
     * @var boolean
     */
    private $isInitialized = false;

    /**
     *
     * @var ResultSetInterface
     */
    private $resultSetPrototype = null;

    /**
     * 
     * @var array
     */
    private static $defaultFeatures = [];

    /**
     *
     * @throws RuntimeException
     */
    protected function initialize() {
        if (!$this->isInitialized) {
            $this->featureSet->addFeatures(self::$defaultFeatures);
            $this->featureSet->setQueryGateway($this);
            $this->featureSet->apply('preInitialize');

            if (!$this->adapter instanceof AdapterInterface) {
                throw new RuntimeException(sprintf('This query gateway %s doesn\'t have an Adapter setup', get_called_class()));
            }

            $this->featureSet->apply('postInitilalize');

            $this->isInitialized = true;
        }
    }

    /**
     *
     * @param Adapter $adapter        	
     */
    public function __construct(Adapter $adapter = null, FeatureSet $featureSet = null) {
        if (null !== $adapter) {
            $this->adapter = $adapter;
        }
        $this->featureSet = (null !== $featureSet) ? $featureSet : new FeatureSet();
        $this->resultSetPrototype = new ResultSet();
    }

    /**
     * 
     * @param AbstractFeature $feature
     */
    public static function addDefaultFeature(AbstractFeature $feature) {
        self::$defaultFeatures[] = $feature;
    }

    /**
     *
     * @param Adapter $adapter        	
     * @return \Solo\Db\QueryGateway\QueryGateway
     */
    public function setAdaper(Adapter $adapter) {
        $this->adapter = $adapter;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function hasAdapter() {
        return ($this->adapter instanceof Adapter);
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Solo\Db\QueryGateway\AbstractQueryGateway::getAdapter()
     */
    public function getAdapter() {
        if (!$this->hasAdapter()) {
            throw new Exception\RuntimeException(sprintf('Query gateway %s does not have an Adapter setup', get_called_class()));
        }
        return $this->adapter;
    }

    /**
     *
     * @return \Solo\Db\QueryGateway\Feature\FeatureSet
     */
    public function getFeatureSet() {
        return $this->featureSet;
    }

    /**
     *
     * @param string $tableName        	
     * @throws InvalidArgumentException
     */
    public function clearTable($tableName) {
        if (empty($tableName)) {
            throw new InvalidArgumentException('Table name can\'t be empty');
        }
        $this->query(sprintf('TRUNCATE TABLE %s', $this->quoteIdentifier($tableName)));
    }

    /**
     *
     * @param
     *        	object | ResultSetInterface $resultSetPrototype
     * @throws InvalidArgumentException
     */
    public function setResultSetPrototype($resultSetPrototype) {
        if (!is_object($resultSetPrototype)) {
            throw new InvalidArgumentException(sprintf('Invalid prototype object in %s', __CLASS__));
        }
        if ($resultSetPrototype instanceof ResultSet) {
            $this->resultSetPrototype = $resultSetPrototype;
        } else {
            $resultSet = new ResultSet();
            $resultSet->setArrayObjectPrototype($resultSetPrototype);
            $this->resultSetPrototype = $resultSet;
        }
    }

    /**
     *
     * @param string $sql    
     * @param boolean $reload    	
     * @return ResultSet
     */
    public function query($sql, $reload = false) {
        $this->initialize();

        $args = $this->featureSet->prepareArgs([
            'sql' => $sql,
            'reload' => $reload,
        ]);
        $this->featureSet->apply('preQuery', $args);

        $result = $this->getAdapter()->getDriver()->getConnection()->execute($args['sql']);
        if (($result instanceof ResultInterface) && $result->isQueryResult()) {
            $resultSet = clone $this->resultSetPrototype;
            $resultSet->initialize($result);
            $args->result = $resultSet;
        } else {
            $args->result = $result;
        }

        $this->featureSet->apply('postQuery', $args);

        return $args->result;
    }

    /**
     *
     * @param string $sql        	
     * @param array $optionalParameters        	
     * @return \Zend\Db\Adapter\Driver\StatementInterface
     */
    public function createStatement($sql, array $optionalParameters = []) {
        $this->initialize();

        $args = $this->featureSet->prepareArgs([
            'sql' => $sql
        ]);
        $this->featureSet->apply('preCreateStatement', $args);

        $stmt = $this->getAdapter()->createStatement($args['sql'], $optionalParameters);

        $this->featureSet->apply('postCreateStatement', $args);

        return $stmt;
    }

    /**
     * Begins transaction
     */
    public function beginTransaction() {
        $this->getAdapter()->getDriver()->getConnection()->beginTransaction();
    }

    /**
     * Commits transaction
     */
    public function commit() {
        $this->getAdapter()->getDriver()->getConnection()->commit();
    }

    /**
     * Rollback transaction
     */
    public function rollback() {
        $this->getAdapter()->getDriver()->getConnection()->rollback();
    }

}
