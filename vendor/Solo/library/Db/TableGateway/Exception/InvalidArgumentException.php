<?php
namespace Solo\Db\TableGateway\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface {}

?>