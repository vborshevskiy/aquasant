<?php

namespace Solo\Db\TableGateway;

interface TableInterface {

	/**
	 *
	 * @return \Solo\Db\TableGateway\TableGateway
	 */
	public function getTableGateway();

	/**
	 *
	 * @param array $set        	
	 * @param boolean $updateOnDuplicate        	
	 */
	public function insertSet(array $set, $updateOnDuplicate = false);

	/**
	 *
	 * @return \Zend\Db\Adapter\Driver\ResultInterface
	 */
	public function truncate();

}

?>