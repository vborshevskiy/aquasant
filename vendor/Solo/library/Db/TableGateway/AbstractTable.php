<?php

namespace Solo\Db\TableGateway;

use Solo\Db\TableGateway\TableGateway;

abstract class AbstractTable {

	/**
	 *
	 * @var TableGateway
	 */
	protected $tableGateway = null;

	/**
	 *
	 * @param TableGateway $tableGateway        	
	 */
	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway = $tableGateway;
	}

	/**
	 *
	 * @return \Solo\Db\TableGateway\TableGateway
	 */
	public function getTableGateway() {
		return $this->tableGateway;
	}

	/**
	 *
	 * @return \Zend\Db\Sql\Select
	 */
	protected function createSelect() {
		if (!$this->tableGateway->isInitialized()) {
			$this->tableGateway->initialize();
		}
		return $this->tableGateway->getSql()->select();
	}

	/**
	 *
	 * @return \Zend\Db\Sql\Insert
	 */
	protected function createInsert() {
		if (!$this->tableGateway->isInitialized()) {
			$this->tableGateway->initialize();
		}
		return $this->tableGateway->getSql()->insert();
	}

	/**
	 *
	 * @return \Zend\Db\Sql\Update
	 */
	protected function createUpdate() {
		if (!$this->tableGateway->isInitialized()) {
			$this->tableGateway->initialize();
		}
		return $this->tableGateway->getSql()->update();
	}

	/**
	 *
	 * @return \Zend\Db\Sql\Delete
	 */
	protected function createDelete() {
		if (!$this->tableGateway->isInitialized()) {
			$this->tableGateway->initialize();
		}
		return $this->tableGateway->getSql()->delete();
	}

	/**
	 *
	 * @param string $sql        	
	 * @return \Zend\Db\Adapter\Driver\ResultInterface
	 */
	protected function query($sql) {
		if (!$this->isInitialized) {
			$this->tableGateway->initialize();
		}
		return $this->tableGateway->getAdapter()->getDriver()->getConnection()->execute($sql);
	}

	/**
	 *
	 * @param array $set        	
	 * @param boolean $updateOnDuplicate        	
	 */
	public function insertSet(array $set, $updateOnDuplicate = false) {
		try {
			if (!$this->isInitialized) {
				$this->tableGateway->initialize();
			}
			$this->tableGateway->insertSet($set, $updateOnDuplicate);
		} catch (\PDOException $ex) {
			if (false !== mb_stripos($ex->getMessage(), 'server has gone away')) {
				$this->getTableGateway()->getAdapter()->getDriver()->getConnection()->disconnect();
				$this->getTableGateway()->getAdapter()->getDriver()->getConnection()->connect();
				
				$this->tableGateway->insertSet($set, $updateOnDuplicate);
			} else {
				throw $ex;
			}
		} catch (\Exception $ex) {
			$exCheck = $ex;
			do {
				if (($exCheck instanceof \PDOException) && (false !== mb_stripos($exCheck->getMessage(), 'server has gone away'))) {
					$this->getTableGateway()->getAdapter()->getDriver()->getConnection()->disconnect();
					$this->getTableGateway()->getAdapter()->getDriver()->getConnection()->connect();
					
					$this->tableGateway->insertSet($set, $updateOnDuplicate);
				}
				$exCheck = $exCheck->getPrevious();
			} while ($exCheck);
			throw $ex;
		}
	}

	/**
	 *
	 * @return \Zend\Db\Adapter\Driver\ResultInterface
	 */
	public function truncate() {
		if (!$this->tableGateway->isInitialized()) {
			$this->tableGateway->initialize();
		}
		return $this->tableGateway->truncate();
	}

	/**
	 *
	 * @param Select $select        	
	 * @return \Zend\Db\ResultSet\ResultSetInterface
	 */
	protected function selectWith(Select $select) {
		try {
			$this->getTableGateway()->initialize();
			return $this->getTableGateway()->selectWith($select);
		} catch (\PDOException $ex) {
			if (false !== mb_stripos($ex->getMessage(), 'server has gone away')) {
				$this->getTableGateway()->getAdapter()->getDriver()->getConnection()->disconnect();
				$this->getTableGateway()->getAdapter()->getDriver()->getConnection()->connect();
				
				return $this->getTableGateway()->selectWith($select);
			} else {
				throw $ex;
			}
		} catch (\Exception $ex) {
			$exCheck = $ex;
			do {
				if (($exCheck instanceof \PDOException) && (false !== mb_stripos($exCheck->getMessage(), 'server has gone away'))) {
					$this->getTableGateway()->getAdapter()->getDriver()->getConnection()->disconnect();
					$this->getTableGateway()->getAdapter()->getDriver()->getConnection()->connect();
					
					return $this->getTableGateway()->selectWith($select);
				}
				$exCheck = $exCheck->getPrevious();
			} while ($exCheck);
			throw $ex;
		}
	}

	/**
	 *
	 * @param Insert $insert        	
	 * @return \Zend\Db\ResultSet\ResultSetInterface
	 */
	protected function insertWith(Insert $insert) {
		try {
			$this->getTableGateway()->initialize();
			return $this->getTableGateway()->insertWith($insert);
		} catch (\PDOException $ex) {
			if (false !== mb_stripos($ex->getMessage(), 'server has gone away')) {
				$this->getTableGateway()->getAdapter()->getDriver()->getConnection()->disconnect();
				$this->getTableGateway()->getAdapter()->getDriver()->getConnection()->connect();
				
				return $this->getTableGateway()->insertWith($insert);
			} else {
				throw $ex;
			}
		} catch (\Exception $ex) {
			throw $ex;
		}
	}

}
