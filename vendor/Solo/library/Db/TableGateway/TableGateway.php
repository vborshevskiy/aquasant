<?php

namespace Solo\Db\TableGateway;

use Zend\Db\ResultSet\ResultSetInterface;
use Zend\Db\TableGateway\Feature\FeatureSet;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\Feature\AbstractFeature;

class TableGateway extends AbstractTableGateway {

	/**
	 *
	 * @var string
	 */
	protected $tableName = null;

	/**
	 *
	 * @var array
	 */
	protected $primaryKeys = [];
	
	/**
	 * 
	 * @var array
	 */
	private static $defaultFeatures = [];

	/**
	 * 
	 * @param string $tableName
	 * @param mixed $primaryKeys
	 */
	public function __construct($tableName = null, $primaryKeys = null, FeatureSet $featureSet = null) {
		if (null !== $tableName) {
			$this->setTable($tableName);
		}
		if ((null !== $this->tableName) && empty($this->table)) {
			$this->table = $this->tableName;
		}
		if (null !== $primaryKeys) {
			$this->addPrimaryKeys($primaryKeys);
		}
		
		$this->featureSet = (null !== $featureSet) ? $featureSet : new FeatureSet();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \Zend\Db\TableGateway\AbstractTableGateway::initialize()
	 */
	public function initialize() {
		$this->featureSet->addFeatures(self::$defaultFeatures);
		parent::initialize();
	}
	
	/**
	 * 
	 * @param AbstractFeature $feature
	 */
	public static function addDefaultFeature(AbstractFeature $feature) {
		self::$defaultFeatures[] = $feature;
	}

	/**
	 *
	 * @param string $table        	
	 */
	public function setTable($table) {
		$this->table = $table;
		$this->tableName = $table;
	}

	/**
	 *
	 * @param mixed $primaryKeys
	 */
	public function addPrimaryKeys($primaryKeys) {
		if (is_string($primaryKeys) && !in_array($primaryKeys, $this->primaryKeys)) {
			$this->primaryKeys[] = $primaryKeys;
		} elseif (is_array($primaryKeys)) {
			$this->primaryKeys = array_merge($this->primaryKeys, $primaryKeys);
		}
	}

	/**
	 *
	 * @return array
	 */
	public function getPrimaryKeys() {
		return $this->primaryKeys;
	}

	/**
	 *
	 * @return Ambigous <\Zend\Db\ResultSet\ResultSet, NULL, \Zend\Db\ResultSet\ResultSetInterface>
	 */
	public function selectPrimary() {
		return $this->select($this->_wherePrimary(func_get_args()));
	}

	/**
	 *
	 * @param array $set        	
	 * @param array $primaryKeys        	
	 * @return Ambigous <number, \Zend\Db\TableGateway\mixed>
	 */
	public function updatePrimary($set, $primaryKeys) {
		return $this->update($set, $this->_wherePrimary($primaryKeys));
	}

	/**
	 *
	 * @param array $primaryKeys        	
	 * @return Ambigous <number, \Zend\Db\TableGateway\mixed>
	 */
	public function deletePrimary($primaryKeys) {
		return $this->delete($this->_wherePrimary($primaryKeys));
	}

	protected function _wherePrimary() {
		$values = array();
		if ((0 < func_num_args()) && is_array(func_get_arg(0))) {
			$values = func_get_arg(0);
		} else {
			$values = func_get_args();
		}
		if (0 == sizeof($values)) {
			throw new Exception\InvalidArgumentException('Not specified argument(s) for select at primary keys');
		}
		$primaryKeys = $this->getPrimaryKeys();
		if (sizeof($values) != sizeof($primaryKeys)) {
			throw new Exception\InvalidArgumentException('Parameters count not equals primary keys count');
		}
		$where = array();
		for ($i = 0, $size = sizeof($primaryKeys); $i < $size; $i++) {
			$where[$primaryKeys[$i]] = $values[$i];
		}
		return $where;
	}
	
	public function truncate() {
		$sql = sprintf('TRUNCATE TABLE %s', $this->quoteIdentifier($this->getTable()));
		return $this->getAdapter()->getDriver()->getConnection()->execute($sql);
	}

	/**
	 *
	 * @param object $resultSetPrototype        	
	 */
	public function setResultSetPrototype($prototype) {
		if ($prototype instanceof ResultSetInterface) {
			$this->resultSetPrototype = $prototype;
		} else {
			$resultSet = new ResultSet();
			$resultSet->setArrayObjectPrototype($prototype);
			$this->resultSetPrototype = $resultSet;
		}
	}
	
	/**
	 * 
	 * @param array $set
	 * @param boolean $updateOnDuplicate
	 */
	public function insertSet(array $set, $updateOnDuplicate = false) {        
		if (0 == sizeof($set)) return;
	
		reset($set);
		$firstRow = current($set);
		$cols = array_keys($firstRow);
			
		$cmd = "INSERT IGNORE INTO " . $this->quoteIdentifier($this->getTable()) . " (`" . implode('`, `', $cols) . "`) VALUES (%s)";
		if ($updateOnDuplicate) {
			foreach ($set as $row) {
				$values = [];
				foreach ($row as $key => $val) {
					if (null === $val) {
						$values[$key] = 'NULL';
					} else {
						$values[$key] = $this->quoteValue(str_replace("\\", "\\\\", $val));
					}
				}
				$sql = sprintf($cmd, implode(', ', $values));
					
				$onDuplicateSql = [];
				foreach ($values as $col => $value) {
					if (!in_array($col, $this->getPrimaryKeys())) {
						$onDuplicateSql[] = '`'.$col.'`'.'='.$value;
					}
				}
				$sql .= " ON DUPLICATE KEY UPDATE ".implode(', ', $onDuplicateSql);
				$this->getAdapter()->getDriver()->getConnection()->execute($sql);
			}
		} else {
			$values = [];
			foreach ($set as $row) {
				$valueRow = [];
				foreach ($row as $val) {
					if (null === $val) {
						$valueRow[] = 'NULL';
					} else {
						$valueRow[] = $this->quoteValue(str_replace("\\", "\\\\", $val));
					}
				}
				$values[] = implode(', ', $valueRow);
			}
	
			$sql = sprintf($cmd, implode('), (', $values));
			$this->getAdapter()->getDriver()->getConnection()->execute($sql);
		}
	}
	
	/**
	 * Begins transaction
	 */
	public function beginTransaction() {
		$this->getAdapter()->getDriver()->getConnection()->beginTransaction();
	}
	
	/**
	 * Commits transaction
	 */
	public function commit() {
		$this->getAdapter()->getDriver()->getConnection()->commit();
	}
	
	/**
	 * Rollback transaction
	 */
	public function rollback() {
		$this->getAdapter()->getDriver()->getConnection()->rollback();
	}

	/**
	 *
	 * @param string $name        	
	 * @return string
	 */
	protected function quoteIdentifier($name) {
		return $this->getAdapter()->getPlatform()->quoteIdentifier($name);
	}

	/**
	 *
	 * @param string $value        	
	 * @return string
	 */
	protected function quoteValue($value) {
		return $this->getAdapter()->getPlatform()->quoteValue($value);
	}

	/**
	 *
	 * @param string $name        	
	 * @return string
	 */
	protected function formatParameterName($name) {
		return $this->getAdapter()->getDriver()->formatParameterName($name);
	}

}

?>