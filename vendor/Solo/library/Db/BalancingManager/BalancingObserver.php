<?php

namespace Solo\Db\BalancingManager;

use Solo\Db\Adapter\Adapter;

class BalancingObserver {

	/**
	 * 
	 * @var array
	 */
	private $slaveServers = [];
	
	/**
	 * 
	 * @var BalancingStatistics
	 */
	private $statistics;
	
	
	/**
	 * 
	 * @param array $slaveServers
	 * @param BalancingStatistics $statistics
	 */
	public function __construct(array $slaveServers, BalancingStatistics $statistics) {
		$this->slaveServers = $slaveServers;
		$this->statistics = $statistics;
	}
	
	/**
	 * Save slave servers metrics in statistics
	 */
	public function saveSlaveMetrics() {
		foreach ($this->slaveServers as $server) {
			$adapter = new Adapter($server);
			$rows = $adapter->query('SHOW SLAVE STATUS');
			if (0 < $rows->count()) {
				$result = $rows->current();
				$secondsBehind = isset($result['Seconds_Behind_Master']) ? intval($result['Seconds_Behind_Master']) : -1;
				$ioRunning = (isset($result['Slave_IO_Running']) && (0 == strcasecmp('yes', $result['Slave_IO_Running'])));
				$sqlRunning = (isset($result['Slave_SQL_Running']) && (0 == strcasecmp('yes', $result['Slave_SQL_Running'])));
				
				$host = $adapter->getConnectionParameter('host');
				$database = $adapter->getConnectionParameter('database');
				$this->statistics->setSecondsBehind($host, $database, $secondsBehind);
				$this->statistics->setAccessible($host, $database, (($ioRunning && $sqlRunning) ? 1 : 0));
			}
		}
	}
	
}

?>