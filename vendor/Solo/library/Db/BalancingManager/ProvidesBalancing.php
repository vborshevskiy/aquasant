<?php

namespace Solo\Db\BalancingManager;

trait ProvidesBalancing {

	/**
	 *
	 * @var BalancingManager
	 */
	private static $staticBalancingManager = null;

	/**
	 *
	 * @var BalancingManager
	 */
	private $balancingManager = null;

	/**
	 *
	 * @param BalancingManager $balancingManager        	
	 */
	public static function setStaticBalancingManager(BalancingManager $balancingManager) {
		self::$staticBalancingManager = $balancingManager;
	}

	/**
	 *
	 * @param BalancingManager $balancingManager        	
	 */
	public function setBalancingManager(BalancingManager $balancingManager) {
		$this->balancingManager = $balancingManager;
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return \Solo\Db\BalancingManager\BalancingManager
	 */
	protected function getBalancingManager() {
		if (null !== $this->balancingManager) {
			return $this->balancingManager;
		}
		if (null !== self::$staticBalancingManager) {
			return self::$staticBalancingManager;
		}
		throw new \RuntimeException('Balancing manager not specified');
	}

	/**
	 *
	 * @return \Zend\Db\Adapter\Adapter | NULL
	 */
	public function getMasterAdapter() {
		return $this->getBalancingManager()->getMasterAdapter();
	}

	/**
	 *
	 * @return \Zend\Db\Adapter\Adapter | NULL
	 */
	public function getSlaveAdapter() {
		return $this->getBalancingManager()->getSlaveAdapter();
	}

}

?>