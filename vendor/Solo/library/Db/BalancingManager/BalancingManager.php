<?php

namespace Solo\Db\BalancingManager;

use Solo\Db\Adapter\Adapter;

class BalancingManager {

	/**
	 *
	 * @var array
	 */
	private $servers = [];

	/**
	 *
	 * @var BalancingStatistics
	 */
	private $statistics;

	/**
	 *
	 * @var array
	 */
	private $adapters = [];

	/**
	 *
	 * @param array $servers        	
	 * @param array $statistics        	
	 */
	public function __construct(array $servers, BalancingStatistics $statistics) {
		$this->servers = $servers;
		$this->statistics = $statistics;
	}

	/**
	 *
	 * @return \Zend\Db\Adapter\Adapter | NULL
	 */
	public function getMasterAdapter() {
		$server = $this->getMasterOptions();
		if (null !== $server) {
			return $this->createAdapter($server['adapter']);
		}
		return null;
	}

	/**
	 *
	 * @return \Zend\Db\Adapter\Adapter | NULL
	 */
	public function getSlaveAdapter() {
		$options = $this->getSlavesOptions();
		if (0 < sizeof($options)) {
			$server = current($options);
			return $this->createAdapter($server['adapter']);
		}
		return $this->getMasterAdapter();
	}

	/**
	 *
	 * @return Ambigous <\Zend\Db\Adapter\Adapter, multitype:>|NULL
	 */
	public function getAnyAdapter() {
		$options = $this->getServersOptions();
		$options = $this->sortServersOptions($options);
		if (0 < sizeof($options)) {
			$server = current($options);
			return $this->createAdapter($server['adapter']);
		}
		return $this->getMasterAdapter();
	}

	/**
	 *
	 * @return array | NULL
	 */
	private function getMasterOptions() {
		foreach ($this->servers as $server) {
			if (0 == strcasecmp('master', $server['state'])) {
				return $server;
			}
		}
		return null;
	}

	/**
	 *
	 * @return array
	 */
	private function getSlavesOptions() {
		$options = $this->getServersOptions('slave');
		$options = $this->sortServersOptions($options);
		return $options;
	}

	/**
	 * 
	 * @param array $options
	 * @return array
	 */
	private function sortServersOptions($options) {
		usort(
			$options, 
			function ($a, $b) {
				if ($a['priority'] < $b['priority']) {
					return 1;
				} elseif ($a['priority'] > $b['priority']) {
					return -1;
				}
				if ($a['secondsBehind'] > $b['secondsBehind']) {
					return 1;
				} elseif ($a['secondsBehind'] < $b['secondsBehind']) {
					return -1;
				}
				if (!$a['isAccessible'] && $b['isAccessible']) {
					return 1;
				} elseif ($a['isAccessible'] && !$b['isAccessible']) {
					return -1;
				}
				return 0;
			});
		reset($options);
		return $options;
	}

	/**
	 *
	 * @param string $state        	
	 * @return array
	 */
	private function getServersOptions($state = null) {
		$servers = [];
		foreach ($this->servers as $server) {
			if ((null === $state) || (0 === strcasecmp($state, $server['state']))) {
				$host = Adapter::getParameter('host', $server);
				$database = Adapter::getParameter('database', $server);
				$servers[] = [
					'adapter' => $server,
					'priority' => $server['priority'],
					'isAccessible' => $this->statistics->getAccessible($host, $database),
					'secondsBehind' => $this->statistics->getSecondsBehind($host, $database) 
				];
			}
		}
		return $servers;
	}

	/**
	 *
	 * @param array $options        	
	 * @return \Zend\Db\Adapter\Adapter
	 */
	private function createAdapter($options) {
		$key = Adapter::getParameter('host', $options);
		if (!isset($this->adapters[$key])) {
			$adapter = new Adapter($options);
			$this->adapters[$key] = $adapter;
		}
		return $this->adapters[$key];
	}

}

?>