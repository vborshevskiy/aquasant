<?php

namespace Solo\Db\BalancingManager;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Solo\Db\Exception\RuntimeException;
use Zend\Stdlib\ArrayUtils;

class BalancingManagerFactory implements FactoryInterface {

	/**
	 * (non-PHPdoc)
	 *
	 * @see \Zend\ServiceManager\FactoryInterface::createService()
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		$config = $serviceLocator->get('Config');
		if (!isset($config['db']) || !isset($config['db']['balancing']) || !isset($config['db']['balancing']['servers'])) {
			throw new RuntimeException('Not set configuration for balancing servers');
		}
		$servers = [];
		foreach ($config['db']['balancing']['servers'] as $server) {
			if (isset($config['db']['adapter'])) {
				$server = ArrayUtils::merge($config['db']['adapter'], $server);
			}
			$servers[] = $server;
		}
		return new BalancingManager($servers, $serviceLocator->get('Solo\\Db\\BalancingManager\\BalancingStatistics'));
	}

}

?>