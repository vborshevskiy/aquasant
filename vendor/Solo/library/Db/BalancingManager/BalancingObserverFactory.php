<?php

namespace Solo\Db\BalancingManager;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Solo\Db\Exception\RuntimeException;
use Zend\Stdlib\ArrayUtils;

class BalancingObserverFactory implements FactoryInterface {

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \Zend\ServiceManager\FactoryInterface::createService()
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		$config = $serviceLocator->get('Config');
		if (!isset($config['db']) || !isset($config['db']['balancing']) || !isset($config['balancing']['servers'])) {
			throw new RuntimeException('Not set configuration for balancing servers');
		}
		$servers = [];
		foreach ($config['db']['balancing']['servers'] as $server) {
			if (0 === strcasecmp('slave', $server['state'])) {
				if (isset($config['db']['adapter'])) {
					$server = ArrayUtils::merge($config['db']['adapter'], $server);
				}
				$servers[] = $server;
			}
		}
		return new BalancingObserver($servers, $serviceLocator->get('Solo\\Db\\BalancingManager\\BalancingStatistics'));
	}

}

?>