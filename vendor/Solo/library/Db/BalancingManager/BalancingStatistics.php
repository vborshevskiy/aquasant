<?php

namespace Solo\Db\BalancingManager;

use Zend\Cache\StorageFactory;
use Zend\Cache\Storage\StorageInterface;
use Solo\Db\Exception\InvalidArgumentException;

class BalancingStatistics {

	/**
	 *
	 * @var StorageInterface
	 */
	private $storage = null;

	/**
	 *
	 * @var array
	 */
	private $storageConfig;

	/**
	 *
	 * @param array $storageConfig        	
	 */
	public function __construct($storageConfig) {
		$this->storageConfig = $storageConfig;
	}

	/**
	 *
	 * @return \Zend\Cache\Storage\StorageInterface
	 */
	private function getStorage() {
		if (null === $this->storage) {
			$this->storage = StorageFactory::factory($this->storageConfig);
		}
		return $this->storage;
	}

	/**
	 *
	 * @param string $host        	
	 * @param string $database        	
	 * @return string
	 */
	private function createKeyPrefix($host, $database) {
		return sprintf('DbBalancing_%s_%s', str_replace('.', '_', $host), $database);
	}

	/**
	 *
	 * @param string $host        	
	 * @param string $database        	
	 * @param integer $seconds        	
	 * @throws InvalidArgumentException
	 */
	public function setSecondsBehind($host, $database, $seconds) {
		if (empty($host)) {
			throw new InvalidArgumentException('Host can\'t be empty');
		}
		if (empty($database)) {
			throw new InvalidArgumentException('Database can\'t be empty');
		}
		if (!is_integer($seconds)) {
			throw new InvalidArgumentException('Seconds must be integer');
		}
		$key = $this->createKeyPrefix($host, $database) . '_SecondsBehind';
		$this->getStorage()->setItem($key, $seconds);
	}

	/**
	 *
	 * @param string $host        	
	 * @param string $database        	
	 * @throws InvalidArgumentException
	 * @return integer
	 */
	public function getSecondsBehind($host, $database) {
		if (empty($host)) {
			throw new InvalidArgumentException('Host can\'t be empty');
		}
		if (empty($database)) {
			throw new InvalidArgumentException('Database can\'t be empty');
		}
		$key = $this->createKeyPrefix($host, $database) . '_SecondsBehind';
		if ($this->getStorage()->hasItem($key)) {
			return intval($this->getStorage()->getItem($key));
		}
		return 0;
	}

	/**
	 *
	 * @param string $host        	
	 * @param string $database        	
	 * @param mixed $status        	
	 * @throws InvalidArgumentException
	 */
	public function setAccessible($host, $database, $status) {
		if (empty($host)) {
			throw new InvalidArgumentException('Host can\'t be empty');
		}
		if (empty($database)) {
			throw new InvalidArgumentException('Database can\'t be empty');
		}
		if (!is_integer($status) && !is_bool($status)) {
			throw new InvalidArgumentException('Status must be integer or boolean');
		}
		$key = $this->createKeyPrefix($host, $database) . '_Accessible';
		$this->getStorage()->setItem($key, intval($status));
	}

	/**
	 *
	 * @param string $host        	
	 * @param string $database        	
	 * @throws InvalidArgumentException
	 * @return boolean
	 */
	public function getAccessible($host, $database) {
		if (empty($host)) {
			throw new InvalidArgumentException('Host can\'t be empty');
		}
		if (empty($database)) {
			throw new InvalidArgumentException('Database can\'t be empty');
		}
		$key = $this->createKeyPrefix($host, $database) . '_Accessible';
		if ($this->getStorage()->hasItem($key)) {
			return (1 == intval($this->getStorage()->getItem($key)));
		}
		return true;
	}

}

?>