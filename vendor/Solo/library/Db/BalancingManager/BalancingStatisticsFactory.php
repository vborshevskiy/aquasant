<?php

namespace Solo\Db\BalancingManager;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class BalancingStatisticsFactory implements FactoryInterface {

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \Zend\ServiceManager\FactoryInterface::createService()
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		$config = $serviceLocator->get('Config');
		if (!isset($config['db']) || !isset($config['db']['balancing']) || !isset($config['db']['balancing']['statistics'])) {
			throw new \RuntimeException('Not set configuration for balancing statistics');
		}
		return new BalancingStatistics($config['db']['balancing']['statistics']);
	}

}

?>