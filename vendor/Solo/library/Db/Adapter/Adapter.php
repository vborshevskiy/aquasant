<?php

namespace Solo\Db\Adapter;

use Zend\Db\Adapter\Adapter as ZendAdapter;

class Adapter extends ZendAdapter {

	/**
	 *
	 * @param string $name        	
	 * @param mixed $parameters        	
	 * @return string | NULL
	 */
	public static function getParameter($name, $parameters) {
		$name = strtolower($name);
		foreach ($parameters as $key => $value) {
			if (strtolower($key) == $name) {
				return $value;
			}
			if ('dsn' == strtolower($key)) {
				$dsnParams = explode(';', substr($value, strpos($value, ':') + 1));
				foreach ($dsnParams as $dsnParam) {
					$dsnPartName = substr($dsnParam, 0, strpos($dsnParam, '='));
					$dsnPartValue = substr($dsnParam, strpos($dsnParam, '=') + 1);
					if (in_array($name, [
						'dbname',
						'database' 
					]) && in_array(strtolower($dsnPartName), [
						'dbname',
						'database' 
					])) {
						return $dsnPartValue;
					} elseif (in_array($name, [
						'host',
						'hostname' 
					]) && in_array($dsnPartName, [
						'host',
						'hostname' 
					])) {
						return $dsnPartValue;
					} elseif (strtolower($dsnPartName) == $name) {
						return $dsnPartValue;
					}
				}
			}
		}
		return null;
	}

	/**
	 *
	 * @param strubg $name        	
	 * @return string | NULL
	 */
	public function getConnectionParameter($name) {
		$parameters = $this->getDriver()->getConnection()->getConnectionParameters();
		return self::getParameter($name, $parameters);
	}

}

?>