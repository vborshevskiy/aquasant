<?php

namespace Solo\Db\Adapter;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Solo\Db\Adapter\Adapter;

class AdapterServiceFactory implements FactoryInterface {

	/**
	 * Create db adapter service
	 *
	 * @param ServiceLocatorInterface $serviceLocator        	
	 * @return Adapter
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		$config = $serviceLocator->get('Config');
		return new Adapter($config['db']['adapter']);
	}

}

?>