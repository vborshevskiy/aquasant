<?php

namespace Solo\Db\Mapper;

use Solo\Db\QueryGateway\QueryGateway;

class AbstractMapper {

    /**
     *
     * @var QueryGateway
     */
    protected $queryGateway = null;

    /**
     *
     * @param QueryGateway $queryGateway        	
     */
    public function __construct(QueryGateway $queryGateway) {
        $this->queryGateway = $queryGateway;
    }

    /**
     *
     * @param string $sql
     * @param boolean $reload
     * @return \Zend\Db\ResultSet\ResultSet
     */
    protected function query($sql, $reload = false) {
        return $this->queryGateway->query($sql, $reload);
    }

    /**
     *
     * @param string $sql        	
     * @param array $optionalParameters        	
     * @return \Zend\Db\Adapter\Driver\StatementInterface
     */
    protected function createStatement($sql, array $optionalParameters = []) {
        return $this->queryGateway->createStatement($sql, $optionalParameters);
    }

}
