<?php

namespace Solo\Inflector;

class Inflector {

	/**
	 *
	 * @var array
	 */
	private static $_pluralRules = array(
		'/(x|ch|ss|sh)$/' => '\1es', // search, switch, fix, box, process,
		                             // address
		'/series$/' => '\1series',
		'/([^aeiouy]|qu)ies$/' => '\1y',
		'/([^aeiouy]|qu)y$/' => '\1ies', // query, ability, agency
		'/(?:([^f])fe|([lr])f)$/' => '\1\2ves', // half, safe, wife
		'/sis$/' => 'ses', // basis, diagnosis
		'/([ti])um$/' => '\1a', // datum, medium
		'/person$/' => 'people', // person, salesperson
		'/man$/' => 'men', // man, woman, spokesman
		'/child$/' => 'children', // child
		'/(.+)status$/' => '\1statuses',
		'/s$/' => 's', // no change (compatibility)
		'/$/' => 's' 
	);

	/**
	 *
	 * @var array
	 */
	private static $_singularRules = array(
		'/(x|ch|ss)es$/' => '\1',
		'/movies$/' => 'movie',
		'/series$/' => 'series',
		'/([^aeiouy]|qu)ies$/' => '\1y',
		'/([lr])ves$/' => '\1f',
		'/([^f])ves$/' => '\1fe',
		'/(analy|ba|diagno|parenthe|progno|synop|the)ses$/' => '\1sis',
		'/([ti])a$/' => '\1um',
		'/people$/' => 'person',
		'/men$/' => 'man',
		'/(.+)status$/' => '\1status',
		'/children$/' => 'child',
		'/news$/' => 'news',
		'/s$/' => '' 
	);

	/**
	 *
	 * @param string $str        	
	 * @return string
	 */
	public static function underscore($str) {
		$str = preg_replace('/([A-Z]+)([A-Z])/', '\1_\2', $str);
		return strtolower(preg_replace('/([a-z])([A-Z])/', '\1_\2', $str));
	}

	/**
	 *
	 * @param string $str        	
	 * @return string
	 */
	public static function camelize($str) {
		return str_replace(' ', '', ucwords(str_replace('_', ' ', $str)));
	}

	/**
	 *
	 * @param string $word        	
	 * @return string
	 */
	public static function pluralize($word) {
		$original = $word;
		reset(self::$_pluralRules);
		while ((list($rule, $replacement) = each(self::$_pluralRules)) && ($original == $word)) {
			$word = preg_replace($rule, $replacement, $word);
		}
		return $word;
	}

	/**
	 *
	 * @param string $word        	
	 * @return string
	 */
	public static function singularize($word) {
		$original = $word;
		reset(self::$_singularRules);
		while ((list($rule, $replacement) = each(self::$_singularRules)) && ($original == $word)) {
			$word = preg_replace($rule, $replacement, $word);
		}
		return $word;
	}

}

?>