<?php

namespace Solo\Stdlib;

abstract class ClassUtils {

    /**
     *
     * @param mixed $class        	
     * @param string $traitName        	
     * @return boolean
     */
    public static function hasTrait($class, $traitName) {
        return in_array($traitName, class_uses($class));
    }

    public static function comareObjects($a, $b) {
        if (is_object($a) && is_object($b)) {
            if (get_class($a) != get_class($b))
                return false;
            foreach ($a as $key => $val) {
                if (!static::comareObjects($val, $b->$key))
                    return false;
            }
            return true;
        }
        else if (is_array($a) && is_array($b)) {
            while (!is_null(key($a) && !is_null(key($b)))) {
                if (key($a) !== key($b) || !static::comareObjects(current($a), current($b)))
                    return false;
                next($a);
                next($b);
            }
            return true;
        }
        else
            return $a === $b;
    }

}

?>