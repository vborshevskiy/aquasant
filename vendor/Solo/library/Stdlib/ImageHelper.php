<?php

namespace Solo\Stdlib;

class ImageHelper {

    /*
     * @param string $file
     * @return boolean
     */
	public static function widthLess($width,$file) {
		if (!self::fileExists($file)) {
            return null;
        }
        $params = getimagesize($file);
        if (isset($params[0])) {
            return ($params[0] < $width);
        }
        return null;
	}
    
    /*
     * @param string $file
     * @return boolean
     */
    public static function heightLess($height,$file) {
		if (!self::fileExists($file)) {
            return null;
        }
        $params = getimagesize($file);
        if (isset($params[1])) {
            return ($params[1] < $height);
        }
        return null;
	}
    
    /*
     * @param string $file
     * @return integer
     */
    public static function getWidth($file) {
		if (!self::fileExists($file)) {
            return null;
        }
        $params = getimagesize($file);
        if (isset($params[0])) {
            return (int)$params[0];
        }
        return null;
	}
    
    /*
     * @param string $file
     * @return integer
     */
    public static function getHeight($file) {
		if (!self::fileExists($file)) {
            return null;
        }
        $params = getimagesize($file);
        if (isset($params[1])) {
            return (int)$params[1];
        }
        return null;
	}
    
    /*
     * @param string $file
     * @return stdClass
     */
    public static function getSize($file) {
		if (!self::fileExists($file)) {
            return null;
        }
        $params = getimagesize($file);
        $size = new \stdClass();
        $size->width = (int)$params[0];
        $size->height = (int)$params[1];
        if (isset($params[0]) && isset($params[1])) {
            return $size;
        }
        return null;
	}
    
    /*
     * @param string $file
     * @return boolean
     */
    public static function fileExists($file) {
        if (file_exists($file) && substr($file, -1) != '/') {
            return true;
        }
        return false;
    }
    
    public static function viewGallery($file1,$file2) {
        $sizeFile1 = self::getSize($file1);
        $sizeFile2= self::getSize($file2);
        return ($sizeFile1->width > $sizeFile2->width + 100 || $sizeFile1->height > $sizeFile2->height + 60);
    }

}

?>