<?php

namespace Solo\Stdlib;

class StringHelper {

    public static function wordending($total, $end1, $end2, $end3) {
        if (intval(substr($total, strlen($total) - 1)) >= 2 && intval(substr($total, strlen($total) - 1)) <= 4 && !(intval(substr($total, strlen($total) - 2)) >= 11 && intval(substr($total, strlen($total) - 2)) <= 19))
            return $end2;
        else if (( intval(substr($total, strlen($total) - 1)) == 1 ) && !(intval(substr($total, strlen($total) - 2)) >= 11 && intval(substr($total, strlen($total) - 2)) <= 19))
            return $end1;
        return $end3;
    }

}
