<?php

namespace Solo\Stdlib\Exception;

/**
 *
 * @category Solo
 * @package Solo_Stdlib
 * @subpackage Exception
 *            
 */
final class NotImplementedException extends \LogicException implements ExceptionInterface {

}

?>