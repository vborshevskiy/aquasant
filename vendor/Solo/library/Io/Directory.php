<?php

namespace Solo\Io;

abstract class Directory {

	/**
	 * Remove directory with all files and included directories
	 * 
	 * @param string $path        	
	 * @return boolean
	 */
	public static function remove($path) {
		if (self::exists($path)) {
			self::clean($path);
			return rmdir($path);
		}
		return false;
	}

	/**
	 * Clean directory out of files and directories
	 * 
	 * @param string $path        	
	 * @return boolean
	 */
	public static function clean($path) {
		if (self::exists($path)) {
			$dir = new \DirectoryIterator($path);
			foreach ($dir as $fileinfo) {
				if (!$fileinfo->isDot()) {
					if ($fileinfo->isFile()) {
						if (! unlink($fileinfo->getPathname())) {
							return false;
						}
					}
					if ($fileinfo->isDir()) {
						if (! self::remove($fileinfo->getPath())) {
							return false;
						}
					}
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Create directory with specified path
	 * 
	 * @param string $path        	
	 * @param integer $mode        	
	 * @param boolean $recursive        	
	 * @return boolean
	 */
	public static function create($path, $mode = null, $recursive = null) {
		if (! self::exists($path)) {
			return call_user_func_array('mkdir', func_get_args());
		}
		return false;
	}

	/**
	 * Checks is directory exists
	 * 
	 * @param string $path        	
	 * @return boolean
	 */
	public static function exists($path) {
		return is_dir($path);
	}

}

?>