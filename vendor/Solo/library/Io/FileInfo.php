<?php

namespace Solo\File;

class FileInfo {

	/**
	 *
	 * @param string $fileName        	
	 * @return string
	 */
	public static function getExt($fileName) {
		$dotPos = strrpos($fileName, '.');
		return substr($fileName, ($dotPos + 1));
	}

	/**
	 *
	 * @param string $fileName        	
	 * @return string
	 */
	public static function getFilename($fileName) {
		$dotPos = strrpos($fileName, '.');
		return substr($fileName, 0, $dotPos);
	}

}

?>