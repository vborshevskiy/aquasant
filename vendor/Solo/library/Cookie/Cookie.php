<?php

namespace Solo\Cookie;

class Cookie {

	/**
	 *
	 * @var Cookie
	 */
	protected static $__instance = null;

	/**
	 *
	 * @var string
	 */
	protected $key = null;

	/**
	 *
	 * @var mixed
	 */
	protected $value = null;

	/**
	 *
	 * @var integer
	 */
	protected $expire = 0;

	/**
	 *
	 * @var string
	 */
	protected $path = null;

	/**
	 *
	 * @var string
	 */
	protected $domain = null;

	/**
	 *
	 * @var boolean
	 */
	protected $isSecure = false;

	/**
	 *
	 * @param string $key        	
	 * @param mixed $value        	
	 * @param integer $expire        	
	 * @param string $path        	
	 * @param string $domain        	
	 * @param boolean $secure        	
	 */
	public function __construct($key, $value = null, $expire = 0, $path = null, $domain = null, $secure = false) {
		$this->key = $key;
		if (null === $value) {
			$v = $_COOKIE[$this->key];
			$this->value = $v;
		} else {
			$v = $value;
			$this->value = $v;
			$this->setCookieValue();
		}
		$this->value = $v;
		$this->expire = $expire;
		$this->path = $path;
		$this->domain = $domain;
		$this->isSecure = $secure;
	}

	/**
	 *
	 * @param string $name        	
	 * @return Cookie
	 */
	public static function getInstance($name) {
		if ((null !== self::$__instance) && (self::$__instance->key == $name)) {
			return self::$__instance;
		}
		self::$__instance = new self($name);
		return self::$__instance;
	}

	/**
	 *
	 * @param string $name        	
	 */
	public static function exists($name) {
		return isset($_COOKIE[$name]);
	}

	/**
	 *
	 * @param mixed $value        	
	 */
	public function setValue($value) {
		$this->value = $value;
		$this->setCookieValue();
	}

	/**
	 *
	 * @param string $name        	
	 * @param mixed $val        	
	 * @param mixed $subVal        	
	 * @param number $expire        	
	 * @param string $path        	
	 * @param string $domain        	
	 * @param boolean $secure        	
	 */
	public static function set($name, $val, $subVal = null, $expire = 0, $path = null, $domain = null, $secure = false) {
		if (null !== $subVal) {
			$name = $name . "[" . $val . "]";
			$val = $subVal;
		}
		setcookie($name, $val, $expire, $path, $domain, $secure);
	}

	/**
	 *
	 * @param string $var        	
	 * @return mixed
	 */
	public function getValue($var = null) {
		if (null === $var) {
			return $this->value;
		} else {
			return $this->value[$var];
		}
	}

	public function kill() {
		$this->setExpires(time() - 3600);
		$this->unsetCookie($this->key);
	}

	/**
	 *
	 * @param string $name        	
	 * @return boolean
	 */
	public static function killCookie($name) {
		if (self::exists($name)) {
			$value = self::get($name);
			$time = time() - 3600;
			if (is_array($value)) {
				foreach ($value as $k => $v) {
					setcookie($name . "[" . $k . "]", "", $time);
				}
			} else {
				setcookie($name, "", $time, '/');
			}
			unset($_COOKIE[$name]);
			return true;
		}
		return false;
	}

	/**
	 *
	 * @param array $names        	
	 * @return boolean
	 */
	public static function killCookies($names) {
		$result = true;
		foreach ($names as $name) {
			if (!self::killCookie($name)) $result = false;
		}
		return $result;
	}

	/**
	 *
	 * @param string $name        	
	 * @param string $var        	
	 * @return mixed
	 */
	public static function get($name, $var = null) {
		if (null !== $var) {
			return $_COOKIE[$name][$var];
		} else {
			return $_COOKIE[$name];
		}
	}

	/**
	 *
	 * @param int $expire        	
	 * @throws Exception\InvalidArgumentException
	 */
	public function setExpires($expire) {
		if (is_integer($expire)) {
			$ex = $expire;
		} elseif (is_string($expire)) {
			$expire = preg_replace("/(\s){2,}/", " ", $expire);
			$timeElementsCount = sizeof(explode(" ", $expire));
			if (preg_match_all('/([\d])+(\w)\b/ui', $expire, $matches)) {
				if (sizeof($matches[2]) == $timeElementsCount) {
					$letters = $matches[2];
					$numbers = $matches[1];
					$parsedArray = array_combine($letters, $numbers);
					$ex = time();
					foreach ($parsedArray as $unity => $value) {
						switch ($unity) {
							case 'y':
								$ex += 60 * 60 * 24 * 365 * $value;
								break;
							case 'M':
								$ex += 60 * 60 * 24 * 30 * $value;
								break;
							case 'd':
								$ex += 60 * 60 * 24 * $value;
								break;
							case 'h':
								$ex += 60 * 60 * $value;
								break;
							case 'm':
								$ex += 60 * $value;
								break;
							case 's':
								$ex += $value;
								break;
							default:
								throw new Exception\InvalidArgumentException('Expires string is invalid!');
								break;
						}
					}
				} else {
					throw new Exception\InvalidArgumentException('Expires string is invalid!');
				}
			} else {
				throw new Exception\InvalidArgumentException('Expires string is invalid!');
			}
		}
		$this->expire = $ex;
		$this->setCookieValue();
	}

	public function setSecure() {
		$this->isSecure = true;
		$this->setCookieValue();
	}

	private function setCookieValue() {
		if (is_array($this->value)) {
			foreach ($this->value as $k => $v) {
				setcookie($this->key . '[' . $k . ']', $v, $this->expire, $this->path, $this->domain, $this->isSecure);
			}
		} else {
			setcookie($this->key, $this->value, $this->expire, $this->path, $this->domain, $this->isSecure);
		}
	}

	/**
	 *
	 * @param string $name        	
	 */
	private function unsetCookie($name) {
		unset($_COOKIE[$name]);
	}

}

?>