<?php

namespace Solo\DateTime;

class DateTime extends \DateTime {

	/**
	 *
	 * @var array
	 */
	private $dateinfo = null;

	/**
	 *
	 * @return DateTime
	 */
	public static function now() {
		return new DateTime('now');
	}

	/**
	 *
	 * @return DateTime
	 */
	public static function tomorrow() {
		$now = self::now();
		$now->addDays(1);
		return $now;
	}

	/**
	 *
	 * @return DateTime
	 */
	public static function yesterday() {
		$now = self::now();
		$now->addDays(-1);
		return $now;
	}

	/**
	 *
	 * @param int $timestamp        	
	 * @return DateTime
	 */
	public static function dayBegin($timestamp = 'now') {
		$date = null;
		if ('now' == $timestamp) {
			$date = self::now();
		} else {
			$date = self::createFromTimestamp($timestamp);
		}
		$dayBegin = self::createFromTimestamp(mktime(0, 0, 0, $date->getMonth(), $date->getMonthDay(), $date->getYear()));
		return $dayBegin;
	}

	/**
	 *
	 * @param int $timestamp        	
	 * @return DateTime
	 */
	public static function dayEnd($timestamp = 'now') {
		$date = null;
		if ('now' == $timestamp) {
			$date = self::now();
		} else {
			$date = self::createFromTimestamp($timestamp);
		}
		$dayEnd = self::createFromTimestamp(mktime(23, 59, 59, $date->getMonth(), $date->getMonthDay(), $date->getYear()));
		return $dayEnd;
	}

	/**
	 *
	 * @param int $timestamp        	
	 * @return DateTime
	 */
	public static function createFromTimestamp($timestamp) {
		$date = new DateTime();
		$date->setTimestamp($timestamp);
		return $date;
	}

	/**
	 *
	 * @param string $str        	
	 * @return DateTime
	 */
	public static function createFromString($str) {
		return self::createFromTimestamp(strtotime($str));
	}

	/**
	 *
	 * @param int $seconds        	
	 * @return DateTime
	 */
	public function addSeconds($seconds) {
		$interval = new \DateInterval('PT' . abs($seconds) . 'S');
		if (0 < intval($seconds)) {
			if (false === $this->add($interval)) {
				return false;
			}
		} else {
			if (false === $this->sub($interval)) {
				return false;
			}
		}
		return $this;
	}

	/**
	 *
	 * @param int $minutes        	
	 * @return DateTime
	 */
	public function addMinutes($minutes) {
		$interval = new \DateInterval('PT' . abs($minutes) . 'M');
		if (0 < intval($minutes)) {
			if (false === $this->add($interval)) {
				return false;
			}
		} else {
			if (false === $this->sub($interval)) {
				return false;
			}
		}
		return $this;
	}

	/**
	 *
	 * @param int $hours        	
	 * @return DateTime
	 */
	public function addHours($hours) {
		$interval = new \DateInterval('PT' . abs($hours) . 'H');
		if (0 < intval($hours)) {
			if (false === $this->add($interval)) {
				return false;
			}
		} else {
			if (false === $this->sub($interval)) {
				return false;
			}
		}
		return $this;
	}

	/**
	 *
	 * @param int $days        	
	 * @return DateTime
	 */
	public function addDays($days) {
		$interval = new \DateInterval('P' . abs($days) . 'D');
		if (0 < intval($days)) {
			if (false === $this->add($interval)) {
				return false;
			}
		} else {
			if (false === $this->sub($interval)) {
				return false;
			}
		}
		return $this;
	}

	/**
	 *
	 * @param int $months        	
	 * @return DateTime
	 */
	public function addMonths($months) {
		$interval = new \DateInterval('P' . abs($months) . 'M');
		if (0 < intval($months)) {
			if (false === $this->add($interval)) {
				return false;
			}
		} else {
			if (false === $this->sub($interval)) {
				return false;
			}
		}
		return $this;
	}

	/**
	 *
	 * @param int $years        	
	 * @return DateTime
	 */
	public function addYears($years) {
		$interval = new \DateInterval('P' . abs($years) . 'Y');
		if (0 < intval($years)) {
			if (false === $this->add($interval)) {
				return false;
			}
		} else {
			if (false === $this->sub($interval)) {
				return false;
			}
		}
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getInfo() {
		if (null === $this->dateinfo) {
			$this->dateinfo = getdate($this->getTimestamp());
		}
		return $this->dateinfo;
	}

	/**
	 *
	 * @return int
	 */
	public function getSeconds() {
		$info = $this->getInfo();
		return $info['seconds'];
	}

	/**
	 *
	 * @return int
	 */
	public function getMinutes() {
		$info = $this->getInfo();
		return $info['minutes'];
	}

	/**
	 *
	 * @return int
	 */
	public function getHours() {
		$info = $this->getInfo();
		return $info['hours'];
	}

	/**
	 *
	 * @return int
	 */
	public function getMonthDay() {
		$info = $this->getInfo();
		return $info['mday'];
	}

	/**
	 *
	 * @return int
	 */
	public function getYearDay() {
		$info = $this->getInfo();
		return $info['yday'];
	}

	/**
	 *
	 * @return int
	 */
	public function getWeekday() {
		$info = $this->getInfo();
		return $info['wday'];
	}
	
	public function add ($interval) {
		parent::add($interval);
		$this->dateinfo = null;
	}

	/**
	 *
	 * @return int
	 */
	public function getMonth() {
		$info = $this->getInfo();
		return $info['mon'];
	}

	/**
	 *
	 * @return int
	 */
	public function getYear() {
		$info = $this->getInfo();
		return $info['year'];
	}

	/**
	 *
	 * @return string
	 */
	public function getWeekdayName() {
		$info = $this->getInfo();
		return $info['weekday'];
	}

	/**
	 *
	 * @return Time
	 */
	public function getTime() {
		return new Time($this->getHours(), $this->getMinutes());
	}

	/**
	 *
	 * @return string
	 */
	public function getLocalWeekdayName() {
		return strftime('%A', $this->getTimestamp());
	}

	/**
	 *
	 * @return string
	 */
	public function getLocalWeekdayAbbr() {
		return strftime('%a', $this->getTimestamp());
	}

	/**
	 *
	 * @return string
	 */
	public function getLocalMonthName() {
		return strftime('%B', $this->getTimestamp());
	}

	/**
	 *
	 * @return string
	 */
	public function getLocalMonthAbbr() {
		return strftime('%b', $this->getTimestamp());
	}

	/**
	 *
	 * @param DateTime $datetime        	
	 * @return boolean
	 */
	public function equals(DateTime $datetime) {
		return ($this->getTimestamp() == $datetime->getTimestamp());
	}

	/**
	 *
	 * @param DateTime $datetime        	
	 * @return boolean
	 */
	public function lowerThan(DateTime $datetime) {
		return ($this->getTimestamp() < $datetime->getTimestamp());
	}

	/**
	 *
	 * @param DateTime $datetime        	
	 * @return boolean
	 */
	public function greaterThan(DateTime $datetime) {
		return ($this->getTimestamp() > $datetime->getTimestamp());
	}

	/**
	 *
	 * @param DateTime $datetime        	
	 * @return int
	 */
	public function compare(DateTime $datetime) {
		if ($this->lowerThan($datetime)) return -1;
		if ($this->greaterThan($datetime)) return 1;
		return 0;
	}

	/**
	 *
	 * @return string
	 */
	public function __toString() {
		return date('Y.m.d H:i:s', $this->getTimestamp());
	}

	/**
	 *
	 * @param \DateInterval $datetime        	
	 */
	public function diffDate(DateTime $datetime) {
		$date1 = DateTime::createFromTimestamp(mktime(0, 0, 0, $this->getMonthDay(), $this->getMonth(), $this->getYear()));
		$date2 = DateTime::createFromTimestamp(mktime(0, 0, 0, $datetime->getMonthDay(), $datetime->getMonth(), $datetime->getYear()));
		return $date1->diff($date2);
	}

	/**
	 * 
	 * @param integer $month
	 * @throws Exception\InvalidArgumentException
	 * @return string
	 */
	public static function getMonthNameGenitive($month) {
		if (!is_integer($month)) {
			throw new Exception\InvalidArgumentException('Month must be integer');
		}
		$names = array(
			1 => 'января',
			2 => 'февраля',
			3 => 'марта',
			4 => 'апреля',
			5 => 'мая',
			6 => 'июня',
			7 => 'июля',
			8 => 'августа',
			9 => 'сентября',
			10 => 'октября',
			11 => 'ноября',
			12 => 'декабря' 
		);
		if (!array_key_exists($month, $names)) {
			throw new Exception\InvalidArgumentException(sprintf('Specified month number %s not found', $month));
		}
		return $names[$month];
	}

}

?>