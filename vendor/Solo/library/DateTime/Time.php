<?php

namespace Solo\DateTime;

class Time {

    private $hour = null;
    private $minute = null;

    public function __construct($hour = null, $minute = null) {
        if (null !== $hour)
            $this->hour = $hour;
        if (null !== $minute)
            $this->minute = $minute;
    }

    public function setHour($hour) {
        if (0 > $hour) {
            throw new Exception\InvalidArgumentException('Hour can\'t be lower than zero');
        }
        if (23 < $hour) {
            throw new Exception\InvalidArgumentException('Hour can\' be greater than 23');
        }
        $this->hour = $hour;
    }

    public function getHour() {
        return $this->hour;
    }

    public function setMinute($minute) {
        if (0 > $minute) {
            throw new Exception\InvalidArgumentException('Minute can\'t be lower than zero');
        }
        if (59 < $minute) {
            throw new Exception\InvalidArgumentException('Minute can\' be greater than 59');
        }
        $this->minute = $minute;
    }

    public function getMinute() {
        return $this->minute;
    }

    public function toString() {
        return sprintf('%02s:%02s', $this->hour, $this->minute);
    }

    public function equals(Time $time) {
        if ($this->isEmpty() && !$time->isEmpty())
            return false;
        if (!$this->isEmpty() && $time->isEmpty())
            return false;
        if (!$this->isEmpty() && !$time->isEmpty()) {
            if ($this->hour != $time->hour)
                return false;
            if ($this->minute != $time->minute)
                return false;
        }
        return true;
    }

    public function lowerThan(Time $time) {
        if ($this->isEmpty()) {
            throw new Exception\RuntimeException('Current time object is empty');
        }
        if ($time->isEmpty()) {
            throw new Exception\InvalidArgumentException('Compared time object is empty');
        }
        return $this->getHour() * 60 + $this->getMinute() < $time->getHour() * 60 + $time->getMinute();
    }

    public function lowerOrEqualsThan(Time $time) {
        return ($this->lowerThan($time) || $this->equals($time));
    }

    public function greaterThan(Time $time) {
        if ($this->isEmpty()) {
            throw new Exception\RuntimeException('Current time object is empty');
        }
        if ($time->isEmpty()) {
            throw new Exception\InvalidArgumentException('Compared time object is empty');
        }
        return $this->getHour() * 60 + $this->getMinute() > $time->getHour() * 60 + $time->getMinute();
    }

    public function greaterOrEqualsThan(Time $time) {
        return ($this->greaterThan($time) || $this->equals($time));
    }

    public function between(Time $from, Time $to, $orEquals = false) {
        if ($from->isEmpty()) {
            throw new Exception\InvalidArgumentException('From time object is empty');
        }
        if ($to->isEmpty()) {
            throw new Exception\InvalidArgumentException('To time object is empty');
        }
        if ($orEquals) {
            return ($this->greaterOrEqualsThan($from) && $this->lowerOrEqualsThan($to));
        } else {
            return ($this->greaterThan($from) && $this->lowerThan($to));
        }
    }

    public function __toString() {
        return $this->toString();
    }

    public function isEmpty() {
        if (null === $this->hour)
            return true;
        if (null === $this->minute)
            return true;
        return false;
    }

    /**
     * @todo fix this method
     * @return int
     */
    public function getFromTimestamp() {
        return mktime($this->getHour(), $this->getMinute(), 0, date("m"), date("d"), date("Y")) + 86400;
    }

}

?>