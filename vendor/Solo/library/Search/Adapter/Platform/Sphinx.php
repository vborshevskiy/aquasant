<?php

namespace Solo\Search\Adapter\Platform;

class Sphinx {

	/**
	 *
	 * @param string $string        	
	 * @return mixed
	 */
	public static function escape($string) {
		$from = [
			'\\',
			'(',
			')',
			'|',
			'-',
			'!',
			'@',
			'~',
			'"',
			'&',
			'/',
			'^',
			'$',
			'=' 
		];
		$to = [
			'\\\\',
			'\(',
			'\)',
			'\|',
			'\-',
			'\!',
			'\@',
			'\~',
			'\" ',
			'\&',
			'\/',
			'\^',
			'\$',
			'\=' 
		];
		return str_replace($from, $to, $string);
	}

}

?>
