<?php

namespace Solo\Search;

class OptionsParameter {

	/**
	 *
	 * @var string
	 */
	private $name;

	/**
	 *
	 * @var mixed
	 */
	private $value;

	/**
	 *
	 * @param string $name        	
	 * @param mixed $value        	
	 */
	public function __construct($name, $value) {
		$this->setName($name);
		$this->setValue($value);
	}

	/**
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 *
	 * @param string $name        	
	 * @throws \InvalidArgumentException
	 * @return \SoloSearch\Entity\OptionsParameter
	 */
	public function setName($name) {
		if (empty($name)) {
			throw new \InvalidArgumentException('Name can\'t be empty');
		}
		$this->name = $name;
		return $this;
	}

	/**
	 *
	 * @return mixed
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 *
	 * @param mixed $value        	
	 * @return \SoloSearch\Entity\OptionsParameter
	 */
	public function setValue($value) {
		$this->value = $value;
		return $this;
	}

}

?>