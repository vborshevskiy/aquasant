<?php

namespace Solo\Search\IndexLocator\Sphinx;

use Zend\Db\Adapter\Adapter;
use Zend\EventManager\EventManagerAwareInterface;
use Solo\EventManager\ProvidesEvents;
use Solo\Search\IndexLocator\IndexLocatorInterface;
use Solo\Search\Options;
use Solo\Search\IndexLocator\AbstractIndexLocator as NonPlatformAbstractIndexLocator;

abstract class AbstractIndexLocator extends NonPlatformAbstractIndexLocator implements IndexLocatorInterface, EventManagerAwareInterface {
	
	use ProvidesEvents;

	/**
	 *
	 * @var Adapter
	 */
	protected $connection = null;

	/**
	 *
	 * @var string
	 */
	protected $nameTemplate = null;

	/**
	 *
	 * @var string
	 */
	protected $ranker = 'sph04';

	/**
	 *
	 * @var string
	 */
	protected $expressionTemplate = null;

	/**
	 *
	 * @var array
	 */
	protected $fieldWeights = [];

	/**
	 *
	 * @param string $name        	
	 * @param Adapter $connection        	
	 */
	public function __construct($name, Adapter $connection = null, array $options = []) {
		parent::__construct($name);
		if (null !== $connection) {
			$this->setConnection($connection);
		}
		if (0 < sizeof($options)) {
			$this->fillOptions($options);
		}
	}

	/**
	 *
	 * @param array $options        	
	 * @throws \InvalidArgumentException
	 */
	private function fillOptions(array $options) {
		if (isset($options['nameTemplate'])) {
			$this->setNameTemplate($options['nameTemplate']);
		}
		if (isset($options['ranker'])) {
			$this->setRanker($options['ranker']);
		}
		if (isset($options['expressionTemplate'])) {
			$this->setExpressionTemplate($options['expressionTemplate']);
		}
		if (isset($options['fieldWeights'])) {
			if (!is_array($options['fieldWeights'])) {
				throw new \InvalidArgumentException('Field weight must be array');
			}
			foreach ($options['fieldWeights'] as $name => $weight) {
				$this->addFieldWeight($name, intval($weight));
			}
		}
		if (isset($options['sortingColumn'])) {
			$this->options()->setSortingColumn($options['sortingColumn']);
		}
		if (isset($options['sortingDirection'])) {
			$this->options()->setSortingDirection($options['sortingDirection']);
		}
		if (isset($options['limit'])) {
			$this->options()->setLimitItemsPerPage(intval($options['limit']));
		}
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return \Zend\Db\Adapter\Adapter
	 */
	public function getConnection() {
		if (null === $this->connection) {
			throw new \RuntimeException('Connection not specified');
		}
		return $this->connection;
	}

	/**
	 *
	 * @param Adapter $connection        	
	 * @return \SoloSearch\Data\AbstractIndexLocator
	 */
	public function setConnection(Adapter $connection) {
		$this->connection = $connection;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getNameTemplate() {
		return $this->nameTemplate;
	}

	/**
	 *
	 * @param string $nameTemplate        	
	 */
	public function setNameTemplate($nameTemplate) {
		if (empty($nameTemplate)) {
			throw new \InvalidArgumentException('Name template can\'t be empty');
		}
		$this->nameTemplate = $nameTemplate;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasNameTemplate() {
		return (null !== $this->nameTemplate);
	}

	/**
	 *
	 * @return string
	 */
	public function getRanker() {
		return $this->ranker;
	}

	/**
	 *
	 * @param string $ranker        	
	 * @return \SoloSearch\Data\AbstractIndexLocator
	 */
	public function setRanker($ranker) {
		$this->ranker = $ranker;
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	protected function hasRanker() {
		return !empty($this->ranker);
	}

	/**
	 *
	 * @return string
	 */
	public function getExpressionTemplate() {
		return $this->expressionTemplate;
	}

	/**
	 *
	 * @param string $expressionTemplate        	
	 * @throws \InvalidArgumentException
	 * @return \SoloSearch\Data\AbstractIndexLocator
	 */
	public function setExpressionTemplate($expressionTemplate) {
		if (empty($expressionTemplate)) {
			throw new \InvalidArgumentException('Expression can\'t be empty');
		}
		$this->expressionTemplate = $expressionTemplate;
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	protected function hasExpressionTemplate() {
		return empty($this->expressionTemplate);
	}

	/**
	 *
	 * @param array $parameters        	
	 * @throws \InvalidArgumentException
	 * @return string
	 */
	public function createExpression(array $parameters) {
		if (!$this->hasExpressionTemplate()) {
			throw new \InvalidArgumentException('Expression template not specified');
		}
		if (0 < sizeof($parameters)) {
			$keys = array_keys($parameters);
			array_map(function ($name) {
				return sprintf('{%s}', $name);
			}, $keys);
			return str_replace($keys, array_values($parameters), $this->getExpressionTemplate());
		} else {
			return $this->getExpressionTemplate();
		}
	}

	/**
	 *
	 * @param string $name        	
	 * @param integer $weight        	
	 * @throws \InvalidArgumentException
	 * @return \SoloSearch\Data\AbstractIndexLocator
	 */
	public function addFieldWeight($name, $weight) {
		if (empty($name)) {
			throw new \InvalidArgumentException('Name can\'t be empty');
		}
		if (!is_integer($weight)) {
			throw new \InvalidArgumentException('Weight must be integer');
		}
		$this->fieldWeights[$name] = $weight;
		return $this;
	}

	/**
	 *
	 * @param string $name        	
	 * @throws \InvalidArgumentException
	 * @return integer
	 */
	public function getFieldWeight($name) {
		if (empty($name)) {
			throw new \InvalidArgumentException('Name can\'t be empty');
		}
		if (!array_key_exists($name, $this->fieldWeights)) {
			throw new \InvalidArgumentException('Specified field \'' . $name . '\' hasn\'t weight');
		}
		return $this->fieldWeights[$name];
	}

	/**
	 *
	 * @return array
	 */
	public function enumFieldWeights() {
		return $this->fieldWeights;
	}

	/**
	 *
	 * @throws \InvalidArgumentException
	 * @return string
	 */
	public function formatName() {
		$args = func_get_args();
		if (0 == sizeof($args)) {                    
			throw new \InvalidArgumentException('Not specified parameters to format name from template');
		}
		if (!$this->hasNameTemplate()) {
			return $this->getName();
		} else {
			return vsprintf($this->getNameTemplate(), $args);
		}
	}

	/**
	 *
	 * @param Options $options        	
	 * @throws \InvalidArgumentException
	 * @return array
	 */
	public function search($expression = null, Options $options = null) {
		if (!$this->hasRanker()) {
			throw new \InvalidArgumentException('Ranker not specified');
		}
		if ((null === $expression) && !$this->hasExpressionTemplate()) {
			throw new \InvalidArgumentException('Expression not specified');
		}
		$this->options->merge($options);
		
		$args = $this->events()->prepareArgs(compact('expression'));
		$this->events()->trigger('search.pre', $this, $args);
		
		if ((null === $expression) && (!isset($args['expression']) || empty($args['expression']))) {
			$args['expression'] = $this->createExpression($this->options()->enumParameters());
		}
		
		if (empty($args['expression'])) {
			throw new \InvalidArgumentException('Expression is empty');
		}
		
		$result = $this->processSearch($args['expression']);
		
		$this->events()->trigger('search.post', $this, $args);
		
		return $result;
	}

	abstract protected function processSearch($expression);

}

?>