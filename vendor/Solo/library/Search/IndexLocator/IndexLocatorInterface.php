<?php

namespace Solo\Search\IndexLocator;

use Solo\Search\Options;

interface IndexLocatorInterface {

	/**
	 *
	 * @param string $expression        	
	 * @param SearchOptions $options        	
	 * @return array
	 */
	public function search($expression, Options $options = null);

}

?>
