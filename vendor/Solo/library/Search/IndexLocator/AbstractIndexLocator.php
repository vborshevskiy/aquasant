<?php

namespace Solo\Search\IndexLocator;

use Solo\Search\Options;

abstract class AbstractIndexLocator {

	/**
	 *
	 * @var string
	 */
	protected $name;

	/**
	 *
	 * @var Options
	 */
	protected $options = null;

	/**
	 *
	 * @param string $name        	
	 */
	public function __construct($name) {          
		$this->setName($name);
		$this->options = new Options();
	}

	/**
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * 
	 * @param string $name
	 * @throws \InvalidArgumentException
	 * @return \Solo\Search\IndexLocator\AbstractIndexLocator
	 */
	public function setName($name) {
		if (empty($name)) {
			throw new \InvalidArgumentException('Name can\'t be empty');
		}
		$this->name = $name;
		return $this;
	}
	
	/**
	 * 
	 * @return \Solo\Search\Options
	 */
	public function options() {
		return $this->options;
	}

}

?>
