<?php

namespace Solo\Search;

use Solo\Collection\Collection;

class Options {

	/**
	 *
	 * @var string
	 */
	const SORT_ASCENDING = 'ASC';

	/**
	 *
	 * @var string
	 */
	const SORT_DESCENDING = 'DESC';

	/**
	 *
	 * @var Collection
	 */
	private $parameters;

	/**
	 *
	 * @var array
	 */
	private $sorting = [];

	/**
	 *
	 * @var array
	 */
	private $limit = [];

	/**
	 * Create Options instance
	 */
	public function __construct() {
		$this->parameters = new Collection();
		$this->sorting = [
			'column' => null,
			'direction' => self::SORT_ASCENDING 
		];
		$this->limit = [
			'page' => 0,
			'itemsPerPage' => null 
		];
	}

	/**
	 *
	 * @param string $name        	
	 * @param string $value        	
	 * @return \SoloSearch\Entity\Options
	 */
	public function setParameter($name, $value) {
		$this->parameters[$name] = $value;
		return $this;
	}

	/**
	 *
	 * @param string $name        	
	 * @throws \OutOfBoundsException
	 * @return mixed
	 */
	public function getParameter($name) {
		if (!$this->hasParameter($name)) {
			throw new \OutOfBoundsException('Parameter name \'' . $name . '\' not exists');
		}
		return $this->parameters[$name];
	}

	/**
	 *
	 * @param string $name        	
	 * @return boolean
	 */
	public function hasParameter($name) {
		return isset($this->parameters[$name]);
	}

	/**
	 *
	 * @return array
	 */
	public function getParameters() {
		return $this->parameters;
	}

	/**
	 *
	 * @param string $column        	
	 * @param string $direction        	
	 * @return \SoloSearch\Entity\Options
	 */
	public function setSorting($column, $direction = null) {
		$this->setSortingColumn($column);
		if (null !== $direction) {
			$this->setSortingDirection($direction);
		}
		return $this;
	}

	/**
	 *
	 * @param string $column        	
	 * @throws \InvalidArgumentException
	 * @return \SoloSearch\Entity\Options
	 */
	public function setSortingColumn($column) {
		if (empty($column)) {
			throw new \InvalidArgumentException('Column can\'t be empty');
		}
		$this->sorting['column'] = $column;
		return $this;
	}

	/**
	 *
	 * @param string $direction        	
	 * @throws \InvalidArgumentException
	 * @return \SoloSearch\Entity\Options
	 */
	public function setSortingDirection($direction) {
		$direction = strtoupper($direction);
		if (!in_array($direction, [
			self::SORT_ASCENDING,
			self::SORT_DESCENDING 
		])) {
			throw new \InvalidArgumentException('Direction not in allowable values');
		}
		$this->sorting['direction'] = $direction;
		return $this;
	}

	/**
	 *
	 * @throws \InvalidArgumentException
	 * @return string
	 */
	public function getSortingColumn() {
		if (!$this->hasSortingColumn()) {
			throw new \InvalidArgumentException('Sorting column not specified');
		}
		return $this->sorting['column'];
	}

	/**
	 *
	 * @throws \InvalidArgumentException
	 * @return string
	 */
	public function getSortingDirection() {
		if (!$this->hasSortingDirection()) {
			throw new \InvalidArgumentException('Sorting direction not specified');
		}
		return $this->sorting['direction'];
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasSorting() {
		return ($this->hasSortingColumn() && $this->hasSortingDirection());
	}

	/**
	 *
	 * @return boolean
	 *
	 */
	public function hasSortingColumn() {
		return isset($this->sorting['column']);
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasSortingDirection() {
		return isset($this->sorting['direction']);
	}
	
	/**
	 * 
	 */
	public function clearSorting() {
		$this->sorting = [];
	}

	/**
	 *
	 * @param integer $page        	
	 * @param integer $itemsPerPage        	
	 * @return \SoloSearch\Entity\Options
	 */
	public function setLimit($page, $itemsPerPage) {
		$this->setLimitPage($page);
		$this->setLimitItemsPerPage($itemsPerPage);
		return $this;
	}

	/**
	 *
	 * @param integer $page        	
	 * @throws \InvalidArgumentException
	 * @return \SoloSearch\Entity\Options
	 */
	public function setLimitPage($page) {
		if (!is_integer($page)) {
			throw new \InvalidArgumentException('Limit page must be integer');
		}
		if (0 > $page) {
			throw new \InvalidArgumentException('Limit page must be greater or equals than zero');
		}
		$this->limit['page'] = $page;
		return $this;
	}

	/**
	 *
	 * @param integer $itemsPerPage        	
	 * @throws \InvalidArgumentException
	 * @return \SoloSearch\Entity\Options
	 */
	public function setLimitItemsPerPage($itemsPerPage) {
		if (!is_integer($itemsPerPage)) {
			throw new \InvalidArgumentException('Limit items per page must be integer');
		}
		if (0 > $itemsPerPage) {
			throw new \InvalidArgumentException('Limit items per page must be greater or equals than zero');
		}
		$this->limit['itemsPerPage'] = $itemsPerPage;
		return $this;
	}

	/**
	 *
	 * @throws \InvalidArgumentException
	 * @return integer
	 */
	public function getLimitPage() {
		if (!$this->hasLimitPage()) {
			throw new \InvalidArgumentException('Limit page not specified');
		}
		return $this->limit['page'];
	}

	/**
	 *
	 * @throws \InvalidArgumentException
	 * @return integer
	 */
	public function getLimitItemsPerPage() {
		if (!$this->hasLimitItemsPerPage()) {
			throw new \InvalidArgumentException('Limit items per page not specified');
		}
		return $this->limit['itemsPerPage'];
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasLimit() {
		return ($this->hasLimitPage() && $this->hasLimitItemsPerPage());
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasLimitPage() {
		return isset($this->limit['page']);
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasLimitItemsPerPage() {
		return isset($this->limit['itemsPerPage']);
	}
	
	/**
	 * 
	 */
	public function clearLimit() {
		$this->limit = [];
	}

	/**
	 *
	 * @param Options $options        	
	 */
	public function merge(Options $options) {
		$this->parameters->copyFrom($options->getParameters());
		if ($options->hasSortingColumn()) {
			$this->setSortingColumn($options->getSortingColumn());
		}
		if ($options->hasSortingDirection()) {
			$this->setSortingDirection($options->getSortingDirection());
		}
		if ($options->hasLimitPage()) {
			$this->setLimitPage($options->getLimitPage());
		}
		if ($options->hasLimitItemsPerPage()) {
			$this->setLimitItemsPerPage($options->getLimitItemsPerPage());
		}
	}

}

?>