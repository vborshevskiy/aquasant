<?php

namespace Solo\ServiceManager;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Solo\Stdlib\ArrayHelper;
use Zend\ServiceManager\ServiceLocatorInterface;

abstract class AbstractFactory implements FactoryInterface {

	/**
	 *
	 * @var \Zend\ServiceManager\ServiceLocatorInterface
	 */
	protected $serviceLocator;

	/**
	 *
	 * @return \Zend\ServiceManager\ServiceLocatorInterface
	 */
	protected function getServiceLocator() {
		if ($this->serviceLocator instanceof ServiceLocatorAwareInterface) {
			return $this->serviceLocator->getServiceLocator();
		}
		return $this->serviceLocator;
	}

	/**
	 *
	 * @param string $name        	
	 * @return mixed
	 */
	protected function service($name) {
		return $this->getServiceLocator()->get($name);
	}

	/**
	 *
	 * @param string $name        	
	 * @throws Exception\RuntimeException
	 * @return mixed
	 */
	protected function getConfig($name = null) {
		$config = $this->getServiceLocator()->get('Config');
		if (empty($config)) {
			throw new Exception\RuntimeException(sprintf('Failed to load config section in %s', __CLASS__));
		}
		if (null !== $name) {
			$value = ArrayHelper::multiKeyGet($config, $name, '.');
			if (null === $value) {
				throw new Exception\RuntimeException(sprintf('Failed to get config section "%s" in %s', $name, __CLASS__));
			}
			return $value;
		} else {
			return $config;
		}
	}

	/**
	 *
	 * @param string $name        	
	 * @return boolean
	 */
	protected function hasConfig($name) {
		$config = $this->getConfig();
		return ArrayHelper::multiKeyExists($config, $name, '.');
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \Zend\ServiceManager\FactoryInterface::createService()
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		$this->serviceLocator = $serviceLocator;
		return $this->create();
	}

	/**
	 * Creates instance
	 */
	abstract protected function create();

}

?>