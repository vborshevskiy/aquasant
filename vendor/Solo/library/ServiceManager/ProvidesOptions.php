<?php

namespace Solo\ServiceManager;

use Solo\Stdlib\ArrayHelper;
trait ProvidesOptions {

	/**
	 * 
	 * @var array
	 */
	private $options = [];
	
	/**
	 * 
	 * @param string $name
	 * @param mixed $value
	 */
	public function setOption($name, $value) {
		$this->options[$name] = $value;
	}
	
	/**
	 * 
	 * @param array $options
	 */
	public function setOptions(array $options = []) {
		foreach ($options as $name => $value) {
			$this->setOption($name, $value);
		}
	}
	
	/**
	 * 
	 * @param string $name
	 * @return boolean
	 */
	public function hasOption($name) {
		return ArrayHelper::multiKeyExists($this->options, $name, '.');
	}
	
	/**
	 * 
	 * @param string $name
	 */
	public function getOption($name) {
		return ArrayHelper::multiKeyGet($this->options, $name, '.');
	}
	
}

?>