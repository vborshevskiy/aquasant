<?php

namespace Solo\ServiceManager;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ServiceLocatorAwareService implements ServiceLocatorAwareInterface {

	/**
	 *
	 * @var ServiceLocatorInterface
	 */
	protected $services;

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::setServiceLocator()
	 */
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->services = $serviceLocator;
	}

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::getServiceLocator()
	 */
	public function getServiceLocator() {
		return $this->services;
	}

}

?>