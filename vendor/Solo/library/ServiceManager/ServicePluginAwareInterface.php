<?php

namespace Solo\ServiceManager;

interface ServicePluginAwareInterface {

	/**
	 *
	 * @param ServicePluginManager $pluginManager        	
	 */
	public function setPluginManager(ServicePluginManager $pluginManager);

}

?>