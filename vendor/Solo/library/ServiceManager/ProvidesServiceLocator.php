<?php

namespace Solo\ServiceManager;

use Zend\ServiceManager\ServiceLocatorInterface;

trait ProvidesServiceLocator {

	/**
	 *
	 * @var ServiceLocatorInterface
	 */
	private $services;

	/**
	 * (non-PHPdoc)
	 *
	 * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::setServiceLocator()
	 */
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->services = $serviceLocator;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::getServiceLocator()
	 */
	public function getServiceLocator() {
		return $this->services;
	}

}

?>