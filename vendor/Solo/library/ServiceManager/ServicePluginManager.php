<?php

namespace Solo\ServiceManager;

use Zend\ServiceManager\AbstractPluginManager;
use Zend\ServiceManager\ConfigInterface;

class ServicePluginManager extends AbstractPluginManager {

	/**
	 * Default set of replicator plugins
	 *
	 * @var array
	 */
	protected $invokableClasses = [];

	/**
	 * Whether or not share by default; default true
	 *
	 * @var boolean
	 */
	protected $shareByDefault = true;

	/**
	 *
	 * @param ConfigInterface $config        	
	 */
	public function __construct(ConfigInterface $config = null) {
		parent::__construct($config);
		$this->addInitializer([
			$this,
			'injectServiceLocator' 
		]);
		$this->addInitializer([
			$this,
			'injectPluginManager' 
		]);
	}

	/**
	 *
	 * @param object $plugin        	
	 */
	public function injectServiceLocator($plugin) {
		if (!is_object($plugin)) {
			return;
		}
		if (!($plugin instanceof ServicePluginInterface)) {
			return;
		}
		$plugin->setServiceLocator($this->getServiceLocator());
	}
	
	/**
	 * 
	 * @param object $plugin
	 */
	public function injectPluginManager($plugin) {
		if (!is_object($plugin)) {
			return;
		}
		if (!($plugin instanceof ServicePluginInterface)) {
			return;
		}
		$plugin->setPluginManager($this);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \Zend\ServiceManager\AbstractPluginManager::validatePlugin()
	 */
	public function validatePlugin($plugin) {
		if (!($plugin instanceof ServicePluginInterface)) {
			throw new \RuntimeException(sprintf('Plugin of type %s is invalid; must implement %s\PluginInterface', (is_object($plugin) ? get_class($plugin) : gettype($plugin)), __NAMESPACE__));
		}
	}

}

?>