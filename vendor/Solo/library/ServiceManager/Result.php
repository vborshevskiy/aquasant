<?php

namespace Solo\ServiceManager;

class Result implements \ArrayAccess {

	/**
	 *
	 * @var integer
	 */
	const SUCCESS = 1;

	/**
	 *
	 * @var integer
	 */
	const FAILURE = 2;

	/**
	 *
	 * @var integer
	 */
	protected $result;

	/**
	 *
	 * @var mixed
	 */
	protected $data = null;

	/**
	 *
	 * @var integer
	 */
	protected $errorCode = null;

	/**
	 *
	 * @var string
	 */
	protected $errorMessage = null;

	/**
	 *
	 * @var string
	 */
	protected $originalResponse;

	/**
	 *
	 * @var string
	 */
	protected $originalRequest;

	/**
	 *
	 * @param integer $result        	
	 */
	public function __construct($result = self::SUCCESS) {
		$this->result = $result;
	}

	/**
	 *
	 * @param boolean $isSuccess        	
	 * @return boolean
	 */
	public function success($isSuccess = null) {
		if (null !== $isSuccess) {
			if (true === $isSuccess) {
				$this->result = self::SUCCESS;
			} else {
				$this->result = self::FAILURE;
			}
		} else {
			return (self::SUCCESS == $this->result);
		}
	}

	/**
	 *
	 * @param integer $code        	
	 * @param string $message        	
	 */
	public function setError($code = null, $message = null) {
		$this->success(false);
		if (null !== $code) {
			$this->errorCode = $code;
		}
		if (null !== $message) {
			$this->errorMessage = $message;
		}
	}

	/**
	 *
	 * @param integer $code        	
	 * @return boolean
	 */
	public function hasError($code = null) {
		if (null === $code) {
			return (null !== $this->errorCode);
		} else {
			return ($code === $this->errorCode);
		}
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasErrorMessage() {
		return (null !== $this->errorMessage);
	}

	/**
	 *
	 * @return string
	 */
	public function getErrorMessage() {
		return $this->errorMessage;
	}

	/**
	 *
	 * @return integer
	 */
	public function getErrorCode() {
		return $this->errorCode;
	}

	/**
	 *
	 * @param multitype: $data        	
	 */
	public function setData($data) {
		$this->data = $data;
	}

	/**
	 *
	 * @return mixed
	 */
	public function getData() {
		return $this->data;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasData() {
		return (null !== $this->data);
	}

	/**
	 *
	 * @param string $name        	
	 * @param mixed $value        	
	 */
	public function __set($name, $value) {
		if ((null === $this->data) || !is_array($this->data)) {
			$this->data = [];
		}
		$this->data[$name] = $value;
	}

	/**
	 *
	 * @param string $name        	
	 * @throws \RuntimeException
	 * @return mixed
	 */
	public function __get($name) {
		if ((null === $this->data) || !is_array($this->data) || !array_key_exists($name, $this->data)) {
			throw new \RuntimeException('Undefined property \'' . $name . '\' in ' . __CLASS__);
		}
		return $this->data[$name];
	}

	/**
	 *
	 * @param string $name        	
	 * @return boolean
	 */
	public function __isset($name) {
		return ((null !== $this->data) && is_array($this->data) && array_key_exists($name, $this->data));
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see ArrayAccess::offsetSet()
	 */
	public function offsetSet($offset, $value) {
		$this->data[$offset] = $value;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see ArrayAccess::offsetGet()
	 */
	public function offsetGet($offset) {
		if (!array_key_exists($offset, $this->data)) {
			throw new \RuntimeException('Undefined parameter \'' . $offset . '\' in ' . __CLASS__);
		}
		return $this->data[$offset];
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see ArrayAccess::offsetExists()
	 */
	public function offsetExists($offset) {
		return array_key_exists($offset, $this->data);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see ArrayAccess::offsetUnset()
	 */
	public function offsetUnset($offset) {
		if (!array_key_exists($offset, $this->data)) {
			throw new \RuntimeException('Undefined parameter \'' . $offset . '\' in ' . __CLASS__);
		}
		unset($this->data[$offset]);
	}

	/**
	 *
	 * @return string
	 */
	public function getOriginalResponse() {
		return $this->originalResponse;
	}

	/**
	 *
	 * @param string $response        	
	 */
	public function setOriginalResponse($response) {
		$this->originalResponse = $response;
	}

	/**
	 *
	 * @return string
	 */
	public function getOriginalRequest() {
		return $this->originalRequest;
	}

	/**
	 *
	 * @param string $request        	
	 */
	public function setOriginalRequest($request) {
		$this->originalRequest = $request;
	}

}

?>