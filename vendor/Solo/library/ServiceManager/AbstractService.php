<?php

namespace Solo\ServiceManager;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Solo\ServiceManager\ProvidesServiceLocator;

abstract class AbstractService implements ServicePluginAwareInterface, ServiceLocatorAwareInterface {
	
	use ProvidesServiceLocator;

	/**
	 *
	 * @var ServicePluginManager
	 */
	private $pluginManager = null;

	/**
	 * (non-PHPdoc)
	 *
	 * @see \Solo\ServiceManager\ServicePluginAwareInterface::setPluginManager()
	 */
	public function setPluginManager(ServicePluginManager $pluginManager) {
		$this->pluginManager = $pluginManager;
	}

	/**
	 *
	 * @param string $method        	
	 * @param array $params        	
	 * @throws \RuntimeException
	 * @return mixed | object
	 */
	public function __call($method, $params) {
		if (null !== $this->pluginManager) {
			$method = strtolower($method);
			if ($this->pluginManager->has($method)) {
				$plugin = $this->pluginManager->get(strtolower($method), $params);
				if (is_callable($plugin)) {
					return call_user_func_array($plugin, $params);
				}
				return $plugin;
			}
			
			$classMethod = sprintf('%s::%s', get_class($this), $method);
			if ($this->pluginManager->has($classMethod)) {
				$plugin = $this->pluginManager->get($classMethod, $params);
				if (null !== $plugin) {
					if (is_callable($plugin)) {
						return call_user_func_array($plugin, $params);
					}
					return $plugin;
				}
			}
		}
		throw new \RuntimeException(sprintf('Call to undefined method %s in %s', $method, get_called_class()));
	}

}

?>