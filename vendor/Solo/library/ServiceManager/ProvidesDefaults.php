<?php

namespace Solo\ServiceManager;

use Solo\Stdlib\ArrayHelper;

trait ProvidesDefaults {

	/**
	 *
	 * @var array
	 */
	private $defaults = [];

	/**
	 *
	 * @param string $name        	
	 * @param mixed $value        	
	 */
	public function setDefault($name, $value) {
		$this->defaults[$name] = $value;
	}

	/**
	 *
	 * @param array $defaults        	
	 */
	public function setDefaults(array $defaults = []) {
		foreach ($defaults as $name => $value) {
			$this->setDefault($name, $value);
		}
	}

	/**
	 *
	 * @param string $name        	
	 * @return boolean
	 */
	public function hasDefault($name) {
		return ArrayHelper::multiKeyExists($this->defaults, $name, '.');
	}

	/**
	 *
	 * @param string $name        	
	 */
	public function getDefault($name) {
		return ArrayHelper::multiKeyGet($this->defaults, $name, '.');
	}
	
	/**
	 * 
	 * @param string $name
	 * @param mixed $value
	 * @return mixed
	 */
	protected function valueOrDefault($value, $name) {
		if ((null === $value) && $this->hasDefault($name)) return $this->getDefault($name);
		else return $value; 
	}

}

?>