<?php

namespace Solo\ServiceManager;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\AbstractPluginManager;

class AbstractServicePlugin implements ServicePluginInterface {

	/**
	 *
	 * @return \Zend\ServiceManager\ServiceLocatorInterface
	 */
	private $serviceLocator = null;

	/**
	 *
	 * @var ServicePluginManager
	 */
	private $pluginManager = null;

	/**
	 * (non-PHPdoc)
	 *
	 * @see \Solo\ServiceManager\PluginInterface::setService()
	 */
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->serviceLocator = $serviceLocator;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \Solo\ServiceManager\PluginInterface::getService()
	 */
	public function getServiceLocator() {
		if (null === $this->serviceLocator) {
			throw new \RuntimeException(sprintf('ServiceLocator not set in %s', get_called_class()));
		}
		return $this->serviceLocator;
	}

	/**
	 *
	 * @see \Solo\ServiceManager\ServicePluginInterface::setPluginManager()
	 */
	public function setPluginManager(AbstractPluginManager $pluginManager) {
		$this->pluginManager = $pluginManager;
	}

	/**
	 *
	 * @param string $method        	
	 * @param array $params        	
	 * @throws \RuntimeException
	 * @return mixed | object
	 */
	public function __call($method, $params) {
		if (null !== $this->pluginManager) {
			$method = strtolower($method);
			if ($this->pluginManager->has($method)) {
				$plugin = $this->pluginManager->get(strtolower($method), $params);
				if (is_callable($plugin)) {
					return call_user_func_array($plugin, $params);
				}
				return $plugin;
			}
		}
		throw new \RuntimeException(sprintf('Call to undefined method %s in %s', $method, get_called_class()));
	}

}

?>