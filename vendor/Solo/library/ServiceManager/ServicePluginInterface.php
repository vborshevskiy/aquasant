<?php

namespace Solo\ServiceManager;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\AbstractPluginManager;

interface ServicePluginInterface {

	/**
	 *
	 * @param ServiceLocatorInterface $service        	
	 */
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator);

	/**
	 *
	 * @return ServiceLocatorInterface
	 */
	public function getServiceLocator();
	
	/**
	 * F
	 * @param AbstractPluginManager $pluginManager
	 */
	public function setPluginManager(AbstractPluginManager $pluginManager);

}

?>