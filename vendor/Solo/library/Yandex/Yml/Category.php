<?php

namespace Solo\Yandex\Yml;

class Category {

    /**
     *
     * @var integer
     */
    private $id = null;

    /**
     *
     * @var integer
     */
    private $parentId = 0;
    
    /**
     * @var string
     */
    private $name = null;

    /**
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }
    
    public function __construct($id, $name, $parentId = 0) {
        $this->setId($id);
        $this->setName($name);
        $this->setParentId($parentId);
    }

    /**
     *
     * @param integer $id        	
     * @throws Exception\InvalidArgumentException
     * @return \Solo\Yandex\Yml\Category
     */
    public function setId($id) {
        if (!is_integer($id)) {
            throw new Exception\InvalidArgumentException('Id must be integer');
        }
        if (0 > $id) {
            throw new Exception\InvalidArgumentException('Id must be greater than zero');
        }
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getParentId() {
        return $this->parentId;
    }

    /**
     *
     * @param integer $parentId        	
     * @throws Exception\InvalidArgumentException
     * @return \Solo\Yandex\Yml\Category
     */
    public function setParentId($parentId) {
        if (!is_integer($parentId)) {
            throw new Exception\InvalidArgumentException('Parent id must be integer');
        }
        if (0 > $parentId) {
            throw new Exception\InvalidArgumentException('Parent id must be greater than zero');
        }
        $this->parentId = $parentId;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function hasParentId() {
        return ($this->parentId > 0);
    }
    
    /**
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * 
     * @param string $name
     * @return \Solo\Yandex\Yml\Category
     * @throws Exception\InvalidArgumentException
     */
    public function setName($name) {
        if (empty($name)) {
            throw new Exception\InvalidArgumentException('Category name can\'t be empty');
        }
        $this->name = $name;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getYml() {
        $xml = '<category id="' . $this->getId() . '"';
        if ($this->hasParentId()) {
            $xml .= ' parentId="'.$this->getParentId().'"';
        }
        $xml .= '>';
        $xml .= $this->getName();
        $xml .= '</category>';
        return $xml;
    }

}
