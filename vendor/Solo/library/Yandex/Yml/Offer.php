<?php

namespace Solo\Yandex\Yml;

class Offer {

	/**
	 *
	 * @var string
	 */
	private $id = null;

	/**
	 *
	 * @var string
	 */
	private $available = false;

	/**
	 *
	 * @var string
	 */
	private $url = null;

	/**
	 *
	 * @var float
	 */
	private $price = 0;

	/**
	 *
	 * @var string
	 */
	private $currencyId = null;

	/**
	 *
	 * @var integer
	 */
	private $categoryId = 0;

	/**
	 *
	 * @var string
	 */
	private $picture = null;

	/**
	 *
	 * @var boolean
	 */
	private $store = null;

	/**
	 *
	 * @var boolean
	 */
	private $pickup = null;

	/**
	 *
	 * @var boolean
	 */
	private $delivery = false;

	/**
	 *
	 * @var float
	 */
	private $localDeliveryCost = null;

	/**
	 *
	 * @var integer
	 */
	private $localDeliveryDays = null;

	/**
	 *
	 * @var string
	 */
	private $name = null;

	/**
	 *
	 * @var string
	 */
	private $vendor = null;

	/**
	 *
	 * @var string
	 */
	private $vendorCode = null;

	/**
	 *
	 * @var string
	 */
	private $model = null;

	/**
	 *
	 * @var string
	 */
	private $description = null;

	/**
	 *
	 * @var boolean
	 */
	private $manufacturerWarranty = null;

	/**
	 *
	 * @var float
	 */
	private $volume = 0;

	/**
	 *
	 * @var float
	 */
	private $weight = 0;

	/**
	 *
	 * @var array
	 */
	private $params = [];
	
	/**
	 * 
	 * @var string
	 */
	private $salesNotes = '';
	
	/**
	 * 
	 * @var integer
	 */
	private $oldPrice;

	/**
	 *
	 * @param integer $id        	
	 * @return \Solo\Yandex\Yml\Offer
	 * @throws Exception\InvalidArgumentException
	 */
	public function setId($id) {
		if (mb_strlen((string)$id) > 20) {
			throw new Exception\InvalidArgumentException('Id can\'t by empty');
		}
		if (!is_integer($id)) {
			throw new Exception\InvalidArgumentException('Id must be integer');
		}
		if (0 >= $id) {
			throw new Exception\InvalidArgumentException('Id must be greater than 0');
		}
		$this->id = $id;
		return $this;
	}

	/**
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param boolean $available        	
	 * @return \Solo\Yandex\Yml\Offer
	 * @throws Exception\InvalidArgumentException
	 */
	public function setAvailable($available) {
		if (!is_bool($available)) {
			throw new Exception\InvalidArgumentException('Available must be boolean');
		}
		$this->available = $available;
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function getAvailable() {
		return $this->available;
	}

	/**
	 *
	 * @return string
	 */
	public function getAvailableText() {
		if ($this->available === true) {
			return 'true';
		}
		return 'false';
	}

	/**
	 *
	 * @param string $url        	
	 * @return \Solo\Yandex\Yml\Offer
	 * @throws Exception\InvalidArgumentException
	 */
	public function setUrl($url) {
		if (mb_strlen($url) > 512) {
			$name = mb_substr($name, 0, 512);
		}
		if (empty($url)) {
			throw new Exception\InvalidArgumentException('Url can\'t by empty');
		}
		$this->url = $url;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 *
	 * @param float $price        	
	 * @return \Solo\Yandex\Yml\Offer
	 * @throws Exception\InvalidArgumentException
	 */
	public function setPrice($price) {
		$this->price = $price;
		return $this;
	}

	/**
	 *
	 * @return float
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 *
	 * @param string $currencyId        	
	 * @return \Solo\Yandex\Yml\Offer
	 */
	public function setCurrencyId($currencyId) {
		$this->currencyId = $currencyId;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getCurrencyId() {
		return $this->currencyId;
	}

	/**
	 *
	 * @param integer $categoryId        	
	 * @return \Solo\Yandex\Yml\Offer
	 * @throws Exception\InvalidArgumentException
	 */
	public function setCategoryId($categoryId) {
		if (!is_integer($categoryId)) {
			throw new Exception\InvalidArgumentException('Category id must be integer');
		}
		$this->categoryId = $categoryId;
		return $this;
	}

	/**
	 *
	 * @return integer
	 */
	public function getCategoryId() {
		return $this->categoryId;
	}

	/**
	 *
	 * @param string $picture        	
	 * @return \Solo\Yandex\Yml\Offer
	 * @throws Exception\InvalidArgumentException
	 */
	public function setPicture($picture) {
		if (mb_strlen($picture) > 512) {
			throw new Exception\InvalidArgumentException('Picture can\'t be greater than 512 characters');
		}
		$this->picture = $picture;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getPicture() {
		return $this->picture;
	}

	/**
	 *
	 * @param boolean $store        	
	 * @return \Solo\Yandex\Yml\Offer
	 * @throws Exception\InvalidArgumentException
	 */
	public function setStore($store) {
		if (!is_bool($store)) {
			throw new Exception\InvalidArgumentException('Store must be boolean');
		}
		$this->store = $store;
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function getStore() {
		return $this->store;
	}

	/**
	 *
	 * @return string
	 */
	function getStoreText() {
		if ($this->store === true) {
			return 'true';
		}
		return 'false';
	}

	/**
	 *
	 * @param boolean $pickup        	
	 * @return \Solo\Yandex\Yml\Offer
	 * @throws Exception\InvalidArgumentException
	 */
	public function setPickup($pickup) {
		if (!is_bool($pickup)) {
			throw new Exception\InvalidArgumentException('Pickup must be boolean');
		}
		$this->pickup = $pickup;
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function getPickup() {
		return $this->pickup;
	}

	/**
	 *
	 * @return string
	 */
	public function getPickupText() {
		if ($this->pickup === true) {
			return 'true';
		}
		return 'false';
	}

	/**
	 *
	 * @param boolean $delivery        	
	 * @return \Solo\Yandex\Yml\Offer
	 * @throws Exception\InvalidArgumentException
	 */
	public function setDelivery($delivery) {
		if (!is_bool($delivery)) {
			throw new Exception\InvalidArgumentException('Delivery must be boolean');
		}
		$this->delivery = $delivery;
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function getDelivery() {
		return $this->delivery;
	}

	/**
	 *
	 * @return string
	 */
	public function getDeliveryText() {
		if ($this->delivery === true) {
			return 'true';
		}
		return 'false';
	}

	/**
	 *
	 * @param float $localDeliveryCost        	
	 * @return \Solo\Yandex\Yml\Offer
	 * @throws Exception\InvalidArgumentException
	 */
	public function setLocalDeliveryCost($localDeliveryCost) {
		if (!is_float($localDeliveryCost)) {
			throw new Exception\InvalidArgumentException('Local delivery cost must be float');
		}
		$this->localDeliveryCost = $localDeliveryCost;
		return $this;
	}

	/**
	 *
	 * @return floar
	 */
	public function getLocalDeliveryCost() {
		return $this->localDeliveryCost;
	}

	/**
	 *
	 * @param integer $localDeliveryDays        	
	 * @throws Exception\InvalidArgumentException
	 */
	public function setLocalDeliveryDays($localDeliveryDays) {
		if (!is_int($localDeliveryDays)) {
			throw new Exception\InvalidArgumentException('Local delivery days must be integer');
		}
		$this->localDeliveryDays = $localDeliveryDays;
	}

	/**
	 *
	 * @return integer
	 */
	public function getLocalDeliveryDays() {
		return $this->localDeliveryDays;
	}

	/**
	 *
	 * @param string $name        	
	 * @return \Solo\Yandex\Yml\Offer
	 * @throws Exception\InvalidArgumentException
	 */
	public function setName($name) {
		if (empty($name)) {
			throw new Exception\InvalidArgumentException('Name can\'t by empty');
		}
		if (mb_strlen($name) > 255) {
			$name = mb_substr($name, 0, 255);
		}
		$this->name = $name;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 *
	 * @param string $vendor        	
	 * @return \Solo\Yandex\Yml\Offer
	 */
	public function setVendor($vendor) {
		$this->vendor = $vendor;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getVendor() {
		return $this->vendor;
	}

	/**
	 *
	 * @param string $vendorCode        	
	 * @return \Solo\Yandex\Yml\Offer
	 */
	public function setVendorCode($vendorCode) {
		$this->vendorCode = $vendorCode;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getVendorCode() {
		return $this->vendorCode;
	}

	/**
	 *
	 * @param string $model        	
	 * @return \Solo\Yandex\Yml\Offer
	 */
	public function setModel($model) {
		$this->model = $model;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getModel() {
		return $this->model;
	}

	/**
	 *
	 * @param string $description        	
	 * @return \Solo\Yandex\Yml\Offer
	 */
	public function setDescription($description) {
		if (mb_strlen($description) > 512) {
			$description = mb_substr($description, 0, 512);
			if (false !== mb_strrpos($description, '.')) {
				$description = mb_substr($description, 0, mb_strrpos($description, '.') + 1);
			} else {
				$description = mb_substr($description, 0, mb_strrpos($description, ' '));
			}
		}
		if (!empty($description)) {
			$this->description = $description;
		}
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 *
	 * @param boolean $manufacturerWarranty        	
	 * @return \Solo\Yandex\Yml\Offer
	 * @throws Exception\InvalidArgumentException
	 */
	public function setManufacturerWarranty($manufacturerWarranty) {
		if (!is_bool($manufacturerWarranty)) {
			throw new Exception\InvalidArgumentException('Manufacturer warranty must be boolean');
		}
		$this->manufacturerWarranty = $manufacturerWarranty;
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function getManufacturerWarranty() {
		return $this->manufacturerWarranty;
	}

	/**
	 *
	 * @return string
	 */
	public function getManufacturerWarrantyText() {
		if ($this->manufacturerWarranty === true) {
			return 'true';
		}
		return 'false';
	}

	/**
	 *
	 * @param float $volume        	
	 * @return \Solo\Yandex\Yml\Offer
	 */
	public function setVolume($volume) {
		$this->volume = $volume;
		return $this;
	}

	/**
	 *
	 * @return float
	 */
	public function getVolume() {
		return $this->volume;
	}

	/**
	 *
	 * @param float $weight        	
	 * @return \Solo\Yandex\Yml\Offer
	 */
	public function setWeight($weight) {
		$this->weight = $weight;
		return $this;
	}

	/**
	 *
	 * @return float
	 */
	public function getWeight() {
		return $this->weight;
	}

	/**
	 *
	 * @return boolean
	 */
	public function check($onlyDelivery, $isRegion, $excludeWithoutDelivery = false) {
		if ($onlyDelivery && !$this->getDelivery()) {
			return 'Delivery is impossible, city has not pick up';
		}
		if ($excludeWithoutDelivery && !$this->getDelivery()) {
			return 'Delivery is impossible, parameter excludeWithoutDelivery is true';
		}
		// hotfix: если доставка включена, то должна быть указана цена
		if (!$isRegion && $this->getDelivery() && (is_null($this->getLocalDeliveryCost()) || $this->getLocalDeliveryCost() <= 0)) {
			return 'Delivery price is zero';
		}
		// если не задан вес и размеры, исключаем из региональной доставки через тк
		if ($isRegion && (0 >= $this->getVolume() || 0 >= $this->getWeight())) {
			return 'Weight and volume are nesessary parameters for region price';
		}
		if ($this->getId() <= 0) {
			return 'Good id must be greater than zero';
		}
		if (is_null($this->getUrl())) {
			return 'Good url can\'t be empty';
		}
		if (false && is_numeric($this->getPrice()) && ($this->getPrice() <= 0)) {
			return 'Good price must be greater than zero';
		}
		if (is_null($this->getCurrencyId())) {
			return 'Good currency can\'t be empty';
		}
		if ($this->getCategoryId() <= 0) {
			return 'Good category must be greater than zero';
		}
		if (is_null($this->getName())) {
			return 'Good name can\'t be empty';
		}
		return true;
	}

	/**
	 *
	 * @return string
	 */
	public function getYml() {
		$xml = "\t\t\t\t" . '<offer id="' . $this->getId() . '" available="' . $this->getAvailableText() . '">' . "\r\n";
		$xml .= "\t\t\t\t\t" . '<url>' . $this->getUrl() . '</url>' . "\r\n";
		$xml .= "\t\t\t\t\t" . '<price>' . $this->getPrice() . '</price>' . "\r\n";
		if ($this->getOldPrice() && (0 < $this->getOldPrice())) {
			$xml .= "\t\t\t\t\t" . '<oldprice>' . $this->getOldPrice() . '</oldprice>' . "\r\n";
		}
		$xml .= "\t\t\t\t\t" . '<currencyId>' . $this->getCurrencyId() . '</currencyId>' . "\r\n";
		$xml .= "\t\t\t\t\t" . '<categoryId>' . $this->getCategoryId() . '</categoryId>' . "\r\n";
		if (!is_null($this->getPicture())) {
			$xml .= "\t\t\t\t\t" . '<picture>' . $this->getPicture() . '</picture>' . "\r\n";
		}
		if (!is_null($this->getStore())) {
			$xml .= "\t\t\t\t\t" . '<store>' . $this->getStoreText() . '</store>' . "\r\n";
		}
		if (!is_null($this->getPickup())) {
			$xml .= "\t\t\t\t\t" . '<pickup>' . $this->getPickupText() . '</pickup>' . "\r\n";
		}
		if (!is_null($this->getDelivery())) {
			$xml .= "\t\t\t\t\t" . '<delivery>' . $this->getDeliveryText() . '</delivery>' . "\r\n";
		}
		if ($this->getLocalDeliveryCost() > 0 && $this->getLocalDeliveryDays() >= 0) {
			$xml .= "\t\t\t\t\t" . '<delivery-options>' . "\r\n" . "\t\t\t\t\t" . '<option cost="' . $this->getLocalDeliveryCost() . '" days="' . $this->getLocalDeliveryDays() . '"/>' . "\r\n" . "\t\t\t\t\t" . '</delivery-options>' . "\r\n";
		} elseif ($this->getLocalDeliveryCost() > 0) {
			$xml .= "\t\t\t\t\t" . '<local_delivery_cost>' . $this->getLocalDeliveryCost() . '</local_delivery_cost>' . "\r\n";
		}
		$xml .= "\t\t\t\t\t" . '<name>' . $this->getName() . '</name>' . "\r\n";
		if (!is_null($this->getVendor())) {
			$xml .= "\t\t\t\t\t" . '<vendor>' . $this->getVendor() . '</vendor>' . "\r\n";
		}
		if ($this->getVendorCode()) {
			$xml .= "\t\t\t\t\t" . '<vendorCode>' . $this->getVendorCode() . '</vendorCode>' . "\r\n";
		}
		if ($this->getModel()) {
			$xml .= "\t\t\t\t\t" . '<model>' . $this->getModel() . '</model>' . "\r\n";
		}
		if (!is_null($this->getDescription())) {
			$xml .= "\t\t\t\t\t" . '<description>' . $this->getDescription() . '</description>' . "\r\n";
		}
		if (!is_null($this->getManufacturerWarranty())) {
			$xml .= "\t\t\t\t\t" . '<manufacturer_warranty>' . $this->getManufacturerWarrantyText() . '</manufacturer_warranty>' . "\r\n";
		}
		if ($this->hasParams()) {
			foreach ($this->getParams() as $paramName => $param) {
				$paramValue = $param['value'];
				$paramUnit = $param['unit'];
				$xml .= "\t\t\t\t\t" . '<param name="' . $paramName . '"' . ($paramUnit ? ' unit="' . $paramUnit . '"' : '') . '>' . $paramValue . '</param>' . "\r\n";
			}
		}
		if ($this->getSalesNotes()) {
			$xml .= "\t\t\t\t\t" . '<sales_notes>' . $this->getSalesNotes() . '</sales_notes>' . "\r\n";
		}
		$xml .= "\t\t\t\t" . '</offer>' . "\r\n";
		return $xml;
	}

	public function addParam($name, $value, $unit = null) {
		$this->params[$name] = [
			'value' => $value,
			'unit' => $unit 
		];
	}

	public function getParams() {
		return $this->params;
	}

	public function hasParams() {
		return (0 < count($this->params));
	}
	/**
	 * @return string
	 */
	public function getSalesNotes() {
		return $this->salesNotes;
	}


	/**
	 * @param string $salesNotes
	 */
	public function setSalesNotes($salesNotes) {
		$this->salesNotes = $salesNotes;
		return $this;
	}
	/**
	 * @return number
	 */
	public function getOldPrice() {
		return $this->oldPrice;
	}


	/**
	 * @param number $oldPrice
	 */
	public function setOldPrice($oldPrice) {
		$this->oldPrice = $oldPrice;
		return $this;
	}





}
