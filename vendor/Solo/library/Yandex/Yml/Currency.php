<?php

namespace Solo\Yandex\Yml;

class Currency {

    const ID_RUBLE = 'RUR';
    const ID_GRIVNA = 'UAH';
    const ID_BEL_RUBLE = 'BYR';
    const ID_TENGE = 'KZT';
    const ID_DOLLAR = 'USD';
    const ID_EURO = 'EUR';
    const RATE_CBRF = 'CBRF';
    const RATE_NBU = 'NBU';
    const RATE_NBK = 'NBK';
    const RATE_CB = 'CB';

    /**
     *
     * @var string
     */
    private $id = null;

    /**
     *
     * @var mixed
     */
    private $rate = null;

    /**
     *
     * @var float
     */
    private $plus = null;

    /**
     *
     * @return string
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @param string $id        	
     * @param string $rate        	
     */
    public function __construct($id, $rate, $plus = null) {
        $this->setId($id);
        $this->setRate($rate);
        if (null !== $plus) {
            $this->setPlus($plus);
        }
    }

    /**
     *
     * @param string $id        	
     * @throws \InvalidArgumentException
     * @return \Solo\Yandex\Yml\Currency
     */
    public function setId($id) {
        if (empty($id)) {
            throw new Exception\InvalidArgumentException('Id can\'t be empty');
        }
        if (!in_array($id, [
                    self::ID_RUBLE,
                    self::ID_GRIVNA,
                    self::ID_BEL_RUBLE,
                    self::ID_TENGE,
                    self::ID_DOLLAR,
                    self::ID_EURO
                ])) {
            throw new Exception\InvalidArgumentException(sprintf('Specified id %s is not allowed', $id));
        }
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return mixed
     */
    public function getRate() {
        return $this->rate;
    }

    /**
     *
     * @param mixed $rate        	
     * @throws Exception\RuntimeException
     * @throws Exception\InvalidArgumentException
     * @return \Solo\Yandex\Yml\Currency
     */
    public function setRate($rate) {
        if (null === $this->id) {
            throw new Exception\RuntimeException('Needs to specify id before rate');
        }
        if (empty($rate)) {
            throw new Exception\InvalidArgumentException('Rate can\'t be empty');
        }
        if (!is_numeric($rate) && !in_array($rate, [
                    self::RATE_CBRF,
                    self::RATE_NBU,
                    self::RATE_NBK,
                    self::RATE_CB
                ])) {
            throw new Exception\InvalidArgumentException('Rate must be numeric or in allowed rates uids');
        }
        if (is_numeric($rate) && (1 == $rate) && !in_array($this->getId(), [
                    self::ID_RUBLE,
                    self::ID_BEL_RUBLE,
                    self::ID_GRIVNA,
                    self::ID_TENGE
                ])) {
            throw new Exception\InvalidArgumentException(sprintf('Current currency identifier %s can\'t be set as primary currency', $this->getId()));
        }
        $this->rate = $rate;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getPlus() {
        return $this->plus;
    }

    /**
     *
     * @param float $plus        	
     * @throws Exception\InvalidArgumentException
     * @return \Solo\Yandex\Yml\Currency
     */
    public function setPlus($plus) {
        if (!is_numeric($plus)) {
            throw new Exception\InvalidArgumentException('Plus must be numeric');
        }
        if (0 >= $plus) {
            throw new Exception\InvalidArgumentException('Plus must be greater that zero');
        }
        $this->plus = $plus;
        return $this;
    }
    
    /**
     *
     * @return boolean
     */
    public function hasPlus() {
        return (!is_null($this->plus));
    }
    
    public function getYml() {
        $xml = '<currency id="' . $this->getId() . '" rate="' . $this->getRate() .'"';
        if ($this->hasPlus()) {
            $xml .= ' plus="'.$this->getPlus().'"';
        }
        $xml .= '/>';
        return $xml;
    }

}
