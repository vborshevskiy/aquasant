<?php

namespace Solo\Yandex\Yml;

class Catalog {

    /**
     *
     * @var string
     */
    private $version = '1.0';
    
    /**
     *
     * @var string
     */
    private $encoding = 'UTF-8';
    
    /**
     *
     * @var type 
     */
    private $doctype = '<!DOCTYPE yml_catalog SYSTEM "shops.dtd">';


    /**
     *
     * @var \DateTime
     */
    private $date = null;
    
    /**
     *
     * @var \Solo\Yandex\Yml\Shop
     */
    private $shop = null;
    
    public function __construct() {
        $this->date = new \DateTime();
        $this->shop = new Shop();
    }
    
    /**
     * 
     * @param string $version
     * @return \Solo\Yandex\Yml\Catalog
     */
    public function setVersion($version) {
        $this->version = $version;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getVersion() {
        return $this->version;
    }
    
    /**
     * 
     * @param string $encoding
     * @return \Solo\Yandex\Yml\Catalog
     */
    public function setEncoding($encoding) {
        $this->encoding = $encoding;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getEncoding() {
        return $this->encoding;
    }
    
    /**
     * 
     * @return \Solo\Yandex\Yml\Shop
     */
    public function shop() {
        return $this->shop;
    }
    
    /**
     * 
     * @param \DateTime $date
     * @return \Solo\Yandex\Yml\Catalog
     */
    public function setDate(\DateTime $date) {
        $this->date = $date;
        return $this;
    }
    
    /**
     * 
     * @return \DateTime
     */
    public function getDate() {
        return $this->date;
    }
    
    /**
     * 
     * @param string $doctype
     * @return \Solo\Yandex\Yml\Catalog
     */
    public function setDoctype($doctype) {
        $this->doctype = $doctype;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getDoctype() {
        return $this->doctype;
    }
    
    /**
     * 
     * @param string $path
     * @param string $fileName
     * @return integer | false
     */
    public function saveYml($path,$fileName) {
        $xml .= '<?xml version="'.$this->getVersion().'" encoding="'.$this->getEncoding().'"?>' . "\r\n";
        $xml .= $this->getDoctype() . "\r\n";
        $xml .= '<yml_catalog date="' . $this->getDate()->format('Y-m-d H:i') . '">' . "\r\n";
        $xml .= $this->shop()->getYml();
        $xml .= '</yml_catalog>';
        file_put_contents($path . $fileName,  $xml);
    }

}
