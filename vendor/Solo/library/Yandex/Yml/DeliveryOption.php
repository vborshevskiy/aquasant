<?php

namespace Solo\Yandex\Yml;

class DeliveryOption {

    /**
     *
     * @var float
     */
    private $cost = 990;
    
    /**
     * @var integer
     */
    private $days = 3;
    
    /**
     * 
     * @param float $cost
     * @param integer $days
     */
    public function __construct($cost = null, $days = null) {
        if (!is_null($cost)) {
            $this->setCost($cost);
        }
        if (!is_null($days)) {
            $this->setDays($days);
        }
    }
    
    /**
     * 
     * @return float
     */
    public function getCost() {
        return $this->cost;
    }

    /**
     * 
     * @param float $cost
     * @return \Solo\Yandex\Yml\DeliveryOption
     * @throws Exception\InvalidArgumentException
     */
    public function setCost($cost) {
        if (!is_numeric($cost)) {
            throw new Exception\InvalidArgumentException('Cost must be integer');
        }
        $this->cost = (float)$cost;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getDays() {
        return $this->days;
    }

    /**
     * 
     * @param integer $days
     * @return \Solo\Yandex\Yml\DeliveryOption
     * @throws Exception\InvalidArgumentException
     */
    public function setDays($days) {
        if (!is_integer($days)) {
            throw new Exception\InvalidArgumentException('Days quantity must be integer');
        }
        $this->days = $days;
        return $this;
    }

    /**
     * 
     * @return string
     */
    public function getYml() {
        $xml = '<option cost="' . $this->getCost() . '" days="' . $this->getDays() . '"/>';
        return $xml;
    }

}
