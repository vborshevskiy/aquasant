<?php

namespace Solo\Yandex\Yml;

use Zend\Validator\EmailAddress;
use Solo\Collection\Collection;

class Shop {

    /**
     *
     * @var string
     */
    private $name = null;

    /**
     *
     * @var string
     */
    private $company = null;

    /**
     *
     * @var string
     */
    private $url = null;

    /**
     *
     * @var string
     */
    private $platform = null;

    /**
     *
     * @var string
     */
    private $version = null;

    /**
     *
     * @var string
     */
    private $agency = null;

    /**
     *
     * @var string
     */
    private $email = null;

    /**
     *
     * @var float
     */
    private $localDeliveryCost = null;

    /**
     *
     * @var Collection
     */
    private $currencies;

    /**
     *
     * @var Collection
     */
    private $categories;
    
    /**
     *
     * @var Collection
     */
    private $deliveryOptions;

    /**
     *
     * @var Collection
     */
    private $offers;
    
    /**
     *
     * @param string $name        	
     */
    public function __construct($name = null) {
        if (null !== $name) {
            $this->setName($name);
        }
        $this->currencies = new Collection();
        $this->categories = new Collection();
        $this->deliveryOptions = new Collection();
        $this->offers = new Collection();
    }

    /**
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     *
     * @param string $name        	
     * @throws \InvalidArgumentException
     * @throws Exception\LengthException
     * @return \Solo\Yandex\Yml\Shop
     */
    public function setName($name) {
        if (empty($name)) {
            throw new \InvalidArgumentException('Name can\'t be empty');
        }
        if (20 < mb_strlen($name)) {
            throw new Exception\LengthException('Name length can\'t be greater than 20 characters');
        }
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function hasName() {
        return (null !== $this->name);
    }

    /**
     *
     * @return string
     */
    public function getCompany() {
        return $this->company;
    }

    /**
     *
     * @param string $company        	
     * @throws \InvalidArgumentException
     * @return \Solo\Yandex\Yml\Shop
     */
    public function setCompany($company) {
        if (empty($company)) {
            throw new \InvalidArgumentException('Company can\'t be empty');
        }
        $this->company = $company;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function hasCompany() {
        return (null !== $this->company);
    }

    /**
     *
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     *
     * @param string $url        	
     * @throws \InvalidArgumentException
     * @return \Solo\Yandex\Yml\Shop
     */
    public function setUrl($url) {
        if (empty($url)) {
            throw new \InvalidArgumentException('Url can\'t be empty');
        }
        $this->url = $url;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function hasUrl() {
        return (null !== $this->url);
    }

    /**
     *
     * @return string
     */
    public function getPlatform() {
        return $this->platform;
    }

    /**
     *
     * @param string $platform        	
     * @throws \InvalidArgumentException
     * @return \Solo\Yandex\Yml\Shop
     */
    public function setPlatform($platform) {
        if (empty($platform)) {
            throw new \InvalidArgumentException('Platform can\'t be empty');
        }
        $this->platform = $platform;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function hasPlatform() {
        return (null !== $this->platform);
    }

    /**
     *
     * @return string
     */
    public function getVersion() {
        return $this->version;
    }

    /**
     *
     * @param string $version        	
     * @throws \InvalidArgumentException
     * @return \Solo\Yandex\Yml\Shop
     */
    public function setVersion($version) {
        if (empty($version)) {
            throw new \InvalidArgumentException('Version can\'t be empty');
        }
        $this->version = $version;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function hasVersion() {
        return (null !== $this->version);
    }

    /**
     *
     * @return string
     */
    public function getAgency() {
        return $this->agency;
    }

    /**
     *
     * @param string $agency        	
     * @throws \InvalidArgumentException
     * @return \Solo\Yandex\Yml\Shop
     */
    public function setAgency($agency) {
        if (empty($agency)) {
            throw new \InvalidArgumentException('Agency can\'t be empty');
        }
        $this->agency = $agency;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function hasAgency() {
        return (null !== $this->agency);
    }

    /**
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     *
     * @param string $email        	
     * @throws \InvalidArgumentException
     * @return \Solo\Yandex\Yml\Shop
     */
    public function setEmail($email) {
        if (empty($email)) {
            throw new \InvalidArgumentException('Email can\'t be empty');
        }
        if (!(new EmailAddress())->isValid($email)) {
            throw new \InvalidArgumentException('Specified email is not valid');
        }
        $this->email = $email;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function hasEmail() {
        return (null !== $this->email);
    }

    /**
     *
     * @return float
     */
    public function getLocalDeliveryCost() {
        return $this->localDeliveryCost;
    }

    /**
     *
     * @param float $localDeliveryCost        	
     * @throws Exception\InvalidArgumentException
     * @return \Solo\Yandex\Yml\Shop
     */
    public function setLocalDeliveryCost($localDeliveryCost) {
        if (!is_numeric($localDeliveryCost)) {
            throw new Exception\InvalidArgumentException('Local delivery cost must be numeric');
        }
        if (0 > $localDeliveryCost) {
            throw new Exception\InvalidArgumentException('Local delivery cost must be greater than zero');
        }
        $this->localDeliveryCost = $localDeliveryCost;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function hasLocalDeliveryCost() {
        return (null !== $this->localDeliveryCost);
    }

    /**
     *
     * @return \Solo\Collection\Collection
     */
    public function currencies() {
        return $this->currencies;
    }

    /**
     *
     * @return \Solo\Collection\Collection
     */
    public function categories() {
        return $this->categories;
    }
    
    /**
     * 
     * @return \Solo\Collection\Collection
     */
    public function deliveryOptions() {
        return $this->deliveryOptions;
    }

    /**
     *
     * @return \Solo\Collection\Collection
     */
    public function offers() {
        return $this->offers;
    }
    
    public function getYml() {
        $xml = "\t" . '<shop>' . "\r\n";
        if ($this->hasName()) {
            $xml .= "\t\t" . '<name>' . $this->getName() . '</name>' . "\r\n";
        }
        if ($this->hasCompany()) {
            $xml .= "\t\t" . '<company>' .$this->getCompany() . '</company>' . "\r\n";
        }
        if ($this->hasUrl()) {
            $xml .= "\t\t" . '<url>' .$this->getUrl() . '</url>' . "\r\n";
        }
        if ($this->hasPlatform()) {
            $xml .= "\t\t" . '<platform>' .$this->getPlatform() . '</platform>' . "\r\n";
        }
        if ($this->hasVersion()) {
            $xml .= "\t\t" . '<version>' .$this->getVersion() . '</version>' . "\r\n";
        }
        if ($this->hasAgency()) {
            $xml .= "\t\t" . '<agency>' .$this->getAgency() . '</agency>' . "\r\n";
        }
        if ($this->hasEmail()) {
            $xml .= "\t\t" . '<email>' .$this->getEmail() . '</email>' . "\r\n";
        }
        if ($this->currencies()->count() > 0) {
            $xml .= "\t\t" . '<currencies>' . "\r\n";
            $xml .= $this->getCurrenciesYml();
            $xml .= "\t\t" . '</currencies>' . "\r\n";
        } else {
            throw new Exception\RuntimeException('Needs to create currency');
        }
        if ($this->categories()->count() > 0) {
            $xml .= "\t\t" . '<categories>' . "\r\n";
            $xml .= $this->getCategoriesYml();
            $xml .= "\t\t" . '</categories>' . "\r\n";
        } else {
            throw new Exception\RuntimeException('Needs to create cutegories');
        }
        if ($this->deliveryOptions()->count() > 0) {
            $xml .= "\t\t" . '<delivery-options>' . "\r\n";
            $xml .= $this->getDeliveryOptionsYml();
            $xml .= "\t\t" . '</delivery-options>' . "\r\n";
        }
        if ($this->offers()->count() > 0) {
            $xml .= "\t\t" . '<offers>' . "\r\n";
            $xml .= $this->getOffersYml();
            $xml .= "\t\t" . '</offers>' . "\r\n";
        } else {
            throw new Exception\RuntimeException('Needs to create offers');
        }
        $xml .= "\t" . '</shop>' . "\r\n";
        return $xml;
    }
    
    protected function getCurrenciesYml() {
        $xml = '';
        foreach ($this->currencies()->toArray() as $currency) {
            $xml .= "\t\t\t" . $currency->getYml() . "\r\n";
        }
        return $xml;
    }
    
    protected function getCategoriesYml() {
        $xml = '';
        foreach ($this->categories()->toArray() as $category) {
            $xml .= "\t\t\t" . $category->getYml() . "\r\n";
        }
        return $xml;
    }
    
    protected function getDeliveryOptionsYml() {
        $xml = '';
        foreach ($this->deliveryOptions()->toArray() as $deliveryOption) {
            $xml .= "\t\t\t" . $deliveryOption->getYml() . "\r\n";
        }
        return $xml;
    }
    
    protected function getOffersYml() {
        $xml = '';
        foreach ($this->offers()->toArray() as $offer) {
            $xml .= $offer->getYml() . "\r\n";
        }
        return $xml;
    }

}
