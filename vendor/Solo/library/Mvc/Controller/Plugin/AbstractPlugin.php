<?php

namespace Solo\Mvc\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin as ZendAbstractPlugin;
use Zend\ServiceManager\ServiceLocatorInterface;

class AbstractPlugin extends ZendAbstractPlugin {

	/**
	 *
	 * @return ServiceLocatorInterface
	 */
	public function getServiceLocator() {
		return $this->getController()->getServiceLocator();
	}

}

?>