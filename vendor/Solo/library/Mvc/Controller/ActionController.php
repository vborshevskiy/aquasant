<?php

namespace Solo\Mvc\Controller;

use Solo\Mvc\Controller\Layout\LayoutInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Solo\Mvc\Exception\RuntimeException;

class ActionController extends AbstractActionController {

	/**
	 *
	 * @var LayoutInterface
	 */
	private $layout = null;

	/**
	 *
	 * @param LayoutInterface $layout        	
	 */
	public function __construct(LayoutInterface $layout = null) {
		if (null !== $layout) {
			$this->setLayout($layout);
		}
	}
        
        public function preDispatch(\Zend\Mvc\MvcEvent $event) {
            //Called before action
        }
        
        public function postDispatch(\Zend\Mvc\MvcEvent $event) {
            //Called after action
        }
        
        public function attachDefaultListeners() {
            parent::attachDefaultListeners();
            $events = $this->getEventManager();
            $events->attach(\Zend\Mvc\MvcEvent::EVENT_DISPATCH, array($this, 'preDispatch'),100);
            $events->attach(\Zend\Mvc\MvcEvent::EVENT_DISPATCH, array($this, 'postDispatch'),-100);
        }

	/**
	 * 
	 * @throws RuntimeException
	 * @return \Solo\Mvc\Controller\Layout\LayoutInterface
	 */
	protected function getLayout() {
		if (!$this->hasLayout()) {
			throw new RuntimeException('Not set layout');
		}
		return $this->layout;
	}

	/**
	 *
	 * @param LayoutInterface $layout        	
	 */
	protected function setLayout(LayoutInterface $layout) {
		$this->layout = $layout;
	}
	
	/**
	 * 
	 * @return boolean
	 */
	protected function hasLayout() {
		return (null !== $this->layout);
	}

	/**
	 *
	 * @param array $vars        	
	 */
	protected function outputVars($vars = []) {
		$vars = $this->getLayout()->mutate($vars);
		if ($this->getRequest()->isXmlHttpRequest()) {
			return $this->getResponse()->setContent(json_encode($vars, JSON_UNESCAPED_SLASHES));
		} else {
			return $vars;
		}
	}

	protected function service($name) {
		return $this->getServiceLocator()->get($name);
	}

}

?>