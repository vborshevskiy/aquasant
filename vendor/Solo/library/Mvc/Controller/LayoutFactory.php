<?php

namespace Solo\Mvc\Controller;

use Solo\Mvc\Exception\RuntimeException;
use Solo\Mvc\Controller\Layout\LayoutInterface;

abstract class LayoutFactory {
	
	/**
	 * 
	 * @var array
	 */
	private static $layouts = [
		'frontend' => 'Solo\Mvc\Controller\Layout\FrontendLayout'
	];
	
	/**
	 * 
	 * @var string
	 */
	private static $defaultLayout = 'frontend';

	/**
	 * 
	 * @param string $name
	 * @throws RuntimeException
	 * @return LayoutInterface
	 */
	public static function factory($name) {
		if (!array_key_exists($name, static::$layouts)) {
			throw new RuntimeException(sprintf('Not found specified layout type %s', $name));
		}
		return new static::$layouts[$name]();
	}
	
	/**
	 * @return LayoutInterface
	 */
	public static function factoryDefault() {
		return static::factory(static::$defaultLayout);
	} 
	
}

?>