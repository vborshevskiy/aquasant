<?php

namespace Solo\Mvc\Controller\Layout;

use Zend\View\Model\ViewModel;
use Solo\Mvc\Exception\RuntimeException;

class FrontendLayout implements LayoutInterface {

    /**
     *
     * @var string
     */
    private $title = '';

    /**
     *
     * @var array
     */
    private $keywords = [];

    /**
     *
     * @var array
     */
    private $description = [];

    /**
     *
     * @var array
     */
    private $stylesheetFiles = [];

    /**
     *
     * @var array
     */
    private $javascriptFiles = [];

    /**
     *
     * @var array
     */
    private $javascriptCustomCode = [];

    /**
     * 
     * @var string
     */
    private $stylesheetExtension = 'css';

    /**
     *
     * @var string
     */
    private $stylesheetFolder = 'css';

    /**
     *
     * @var string
     */
    private $stylesheetIeFolder = 'css/ie';

    /**
     * 
     * @var string
     */
    private $javascriptExtension = 'js';

    /**
     *
     * @var string
     */
    private $javascriptFolder = 'js';

    /**
     *
     * @var string
     */
    private $javascriptIeFolder = 'js';

    /**
     *
     * @var integer
     */
    private $stylesheetFilesPerRequest = 10;

    /**
     *
     * @var integer
     */
    private $javascriptFilesPerRequest = 10;

    /**
     *
     * @var string
     */
    private $bodyClass = '';

    /**
     * 
     * @var array
     */
    private $bodyAttributes = [];

    /**
     *
     * @var boolean
     */
    private $debug = false;

    /**
     *
     * @var boolean
     */
    private $noAppearance = false;

    /**
     *
     * @var boolean
     */
    private $isBlankPage = false;

    /**
     *
     * @var array
     */
    private $variables = [];

    public function __construct() {
        $this->clearCssFiles();
        $this->clearJsFiles();
    }

    /**
     *
     * @param string $title        	
     * @return \Solo\Mvc\Controller\FrontendLayout\Layout
     */
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     *
     * @param string $className        	
     * @return \Solo\Mvc\Controller\FrontendLayout\Layout
     */
    public function setBodyClass($className) {
        $this->bodyClass = $className;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getBodyClass() {
        return $this->bodyClass;
    }

    /**
     * 
     * @param string $name
     * @param string $value
     */
    public function setBodyAttribute($name, $value) {
        $this->bodyAttributes[$name] = $value;
    }

    /**
     * 
     * @param string $name
     * @throws RuntimeException
     * @return string
     */
    public function getBodyAttribute($name) {
        if (!array_key_exists($name, $this->bodyAttributes)) {
            throw new RuntimeException(sprintf('Specified body attibute %s not exists', $name));
        }
        return $this->bodyAttributes[$name];
    }

    /**
     * 
     * @return array
     */
    public function getBodyAttributes() {
        return $this->bodyAttributes;
    }

    public function disableAppearance() {
        $this->noAppearance = true;
    }
    
    /**
     * 
     * @param boolean | null $isBlankPage
     * @return boolean | void
     */
    public function isBlankPage($isBlankPage = null) {
        if (is_null($isBlankPage)) {
            return $this->isBlankPage;
        }
        $this->isBlankPage = (bool)$isBlankPage;
    }

    /**
     * 
     * @param string $extension
     * @return \Solo\Mvc\Controller\Layout\FrontendLayout
     */
    public function setStylesheetExtension($extension) {
        $this->stylesheetExtension = $extension;
        return $this;
    }

    /**
     * 
     * @return string
     */
    public function getStylesheetExtension() {
        return $this->stylesheetExtension;
    }

    /**
     *
     * @param string $folderName        	
     * @return \Solo\Mvc\Controller\FrontendLayout\Layout
     */
    public function setStylesheetFolder($folderName) {
        $this->stylesheetFolder = $folderName;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getStylesheetFolder() {
        return $this->stylesheetFolder;
    }

    /**
     *
     * @param string $folderName        	
     * @return \Solo\Mvc\Controller\FrontendLayout\Layout
     */
    public function setStylesheetIeFolder($folderName) {
        $this->stylesheetIeFolder = $folderName;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getStylesheetIeFolder() {
        return $this->stylesheetIeFolder;
    }

    /**
     * 
     * @param string $extension
     * @return \Solo\Mvc\Controller\Layout\FrontendLayout
     */
    public function setJavascriptExtension($extension) {
        $this->javascriptExtension = $extension;
        return $this;
    }

    /**
     * 
     * @return string
     */
    public function getJavascriptExtension() {
        return $this->javascriptExtension;
    }

    /**
     *
     * @param string $folderName        	
     * @return \Solo\Mvc\Controller\FrontendLayout\Layout
     */
    public function setJavascriptFolder($folderName) {
        $this->javascriptFolder = $folderName;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getJavascriptFolder() {
        return $this->javascriptFolder;
    }

    /**
     *
     * @param string $folderName        	
     * @return \Solo\Mvc\Controller\FrontendLayout\Layout
     */
    public function setJavascriptIeFolder($folderName) {
        $this->javascriptIeFolder = $folderName;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getJavascriptIeFolder() {
        return $this->javascriptIeFolder;
    }
    
    /**
     * 
     * @return \Solo\Mvc\Controller\Layout\FrontendLayout
     */
    public function clearMetaKeywords() {
    	$this->keywords = [];
    	return $this;
    }

    /**
     *
     * @param
     *        	mixed [optional]
     */
    public function addMetaKeywords() {
        $args = func_get_args();
        foreach ($args as $arg) {
            if (is_array($arg)) {
                foreach ($arg as $keyword) {
                    $this->keywords[] = $keyword;
                }
            } else {
                $this->keywords[] = $arg;
            }
        }
    }
    
    /**
     * 
     * @return \Solo\Mvc\Controller\Layout\FrontendLayout
     */
    public function clearMetaDescription() {
    	$this->description = [];
    	return $this;
    }

    /**
     *
     * @param
     *        	mixed [optional]
     */
    public function addMetaDescription() {
        $args = func_get_args();
        foreach ($args as $arg) {
            if (is_array($arg)) {
                foreach ($arg as $keyword) {
                    $this->description[] = $keyword;
                }
            } else {
                $this->description[] = $arg;
            }
        }
    }

    /**
     *
     * @param string $condition        	
     * @return string
     */
    private function normalizeIeCondition($condition) {
        $norm = trim($condition);
        if (empty($norm)) {
            $norm = 'IE';
        }
        $norm = str_replace('<=', 'lte', $norm);
        $norm = str_replace('<', 'lt', $norm);
        $norm = str_replace('>=', 'gte', $norm);
        $norm = str_replace('>', 'gt', $norm);
        $norm = strtoupper($norm);
        $norm = str_replace(array(
            'LTE',
            'LT',
            'GTE',
            'GT'
                ), array(
            'lte',
            'lt',
            'gte',
            'gt'
                ), $norm);
        return $norm;
    }

    private function normalizeCssFilename($filename) {
        $extension = (('' !== $this->getStylesheetExtension()) ? '.' . $this->getStylesheetExtension() : '');
        if (!empty($extension) && (false === strrpos($filename, $extension))) {
            $filename .= $extension;
        }
        return $filename;
    }

    private function normalizeJsFilename($filename) {
        $extension = (('' !== $this->getJavascriptExtension()) ? '.' . $this->getJavascriptExtension() : '');
        if (!empty($extension) && (false === strrpos($filename, $extension))) {
            $filename .= $extension;
        }
        return $filename;
    }

    /**
     *
     * @param string $filename        	
     * @param string $folder        	
     * @throws \InvalidArgumentException
     */
    public function addCssFile($filename, $folder = null) {
        if (empty($filename)) {
            throw new \InvalidArgumentException('Filename can\'t be empty');
        }
        if (null === $folder) {
            $folder = $this->stylesheetFolder;
        }
        $filename = $this->normalizeCssFilename($filename);
        $this->stylesheetFiles['simple'][$folder][] = $filename;
    }

    /**
     *
     * @param string $filename        	
     * @param string $condition        	
     * @param string $folder        	
     * @throws \InvalidArgumentException
     */
    public function addCssIeFile($filename, $condition = null, $folder = null) {
        if (empty($filename)) {
            throw new \InvalidArgumentException('Filename can\'t be empty');
        }
        if (null === $folder) {
            $folder = $this->stylesheetIeFolder;
        }
        $filename = $this->normalizeCssFilename($filename);
        $condition = $this->normalizeIeCondition($condition);
        if (!isset($this->stylesheetFiles['ie'][$condition])) {
            $this->stylesheetFiles['ie'][$condition] = array();
        }
        if (!isset($this->stylesheetFiles['ie'][$condition][$folder])) {
            $this->stylesheetFiles['ie'][$condition][$folder] = array();
        }
        $this->stylesheetFiles['ie'][$condition][$folder][] = $filename;
    }

    /**
     */
    public function clearCssFiles() {
        $this->stylesheetFiles = [
            'simple' => [],
            'ie' => []
        ];
    }

    /**
     *
     * @param string $filename        	
     * @param string $folder        	
     * @throws \InvalidArgumentException
     */
    public function removeCssFile($filename, $folder = null) {
        if (empty($filename)) {
            throw new \InvalidArgumentException('Filename can\'t be empty');
        }
        if (null === $folder) {
            $folder = $this->stylesheetFolder;
        }
        $filename = $this->normalizeCssFilename($filename);
        if (isset($this->stylesheetFiles['simple'][$folder][$filename])) {
            unset($this->stylesheetFiles['simple'][$folder][$filename]);
        }
    }

    /**
     *
     * @param string $filename        	
     * @param string $folder        	
     * @throws \InvalidArgumentException
     */
    public function addJsFile($filename, $folder = null) {
        if (empty($filename)) {
            throw new \InvalidArgumentException('Filename can\'t be empty');
        }
        if (null === $folder) {
            $folder = $this->javascriptFolder;
        }
        $filename = $this->normalizeJsFilename($filename);
        $this->javascriptFiles['simple'][$folder][] = $filename;
    }

    /**
     *
     * @param string $filename        	
     * @param string $condition        	
     * @param string $folder        	
     * @throws \InvalidArgumentException
     */
    public function addJsIeFile($filename, $condition = '', $folder = null) {
        if (empty($filename)) {
            throw new \InvalidArgumentException('Filename can\'t be empty');
        }
        if (null === $folder) {
            $folder = $this->javascriptIeFolder;
        }
        $filename = $this->normalizeJsFilename($filename);
        $condition = $this->normalizeIeCondition($condition);
        if (!isset($this->javascriptFiles['ie'][$condition])) {
            $this->javascriptFiles['ie'][$condition] = array();
        }
        if (!isset($this->javascriptFiles['ie'][$condition][$folder])) {
            $this->javascriptFiles['ie'][$condition][$folder] = array();
        }
        $this->javascriptFiles['ie'][$condition][$folder][] = $filename;
    }

    /**
     */
    public function clearJsFiles() {
        $this->javascriptFiles = [
            'simple' => [],
            'ie' => []
        ];
    }

    /**
     *
     * @param string $filename        	
     * @param string $folder        	
     * @throws \InvalidArgumentException
     */
    public function removeJsFile($filename, $folder = null) {
        if (empty($filename)) {
            throw new \InvalidArgumentException('Filename can\'t be empty');
        }
        if (null === $folder) {
            $folder = $this->javascriptFolder;
        }
        $filename = $this->normalizeJsFilename($filename);
        if (isset($this->javascriptFiles['simple'][$folder][$filename])) {
            unset($this->javascriptFiles['simple'][$folder][$filename]);
        }
    }

    /**
     *
     * @param boolean $mode        	
     */
    public function setDebug($mode = true) {
        $this->debug = $mode;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Solo\Mvc\Controller\FrontendLayout\LayoutInterface::getViewModel()
     */
    public function getViewModel() {
        $vm = new ViewModel();
        $vm->setVariable('meta', array(
            'title' => $this->title,
            'keywords' => $this->keywords,
            'description' => $this->description
        ));
        $vm->setVariable('stylesheetFiles', $this->stylesheetFiles);
        $vm->setVariable('javascriptFiles', $this->javascriptFiles);
        $vm->setVariable('layoutSettings', array(
            'stylesheetFilesPerRequest' => $this->stylesheetFilesPerRequest,
            'javascriptFilesPerRequest' => $this->javascriptFilesPerRequest
        ));
        $vm->setVariable('bodyClass', $this->bodyClass);
        $vm->setVariable('bodyAttributes', $this->getBodyAttributes());
        $vm->setVariable('debug', $this->debug);
        $vm->setVariable('noAppearance', $this->noAppearance);
        $vm->setVariable('isBlankPage', $this->isBlankPage);

        return $vm;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Solo\Mvc\Controller\FrontendLayout\LayoutInterface::mutate()
     */
    public function mutate($vars) {
        $vm = $this->getViewModel();
        $vm->setVariables($vars, false);
        return $vm;
    }

    /**
     * (non-PHPdoc)
     * 
     * @see \Solo\Mvc\Controller\Layout\LayoutInterface::setOptions()
     */
    public function setOptions(array $options) {
        foreach ($options as $key => $option) {
            switch (strtolower($key)) {
                case 'stylesheet':
                    if (isset($option['folder'])) {
                        $this->setStylesheetFolder($option['folder']);
                    }
                    if (isset($option['ieFolder'])) {
                        $this->setStylesheetIeFolder($option['ieFolder']);
                    }
                    break;
                case 'javascript':
                    if (isset($option['folder'])) {
                        $this->setJavascriptFolder($option['folder']);
                    }
                    if (isset($option['ieFolder'])) {
                        $this->setJavascriptIeFolder($option['ieFolder']);
                    }
                    break;
                case 'autoload':
                    if (isset($option['stylesheet'])) {
                        if (!is_array($option['stylesheet'])) {
                            throw new RuntimeException('Autoload stylesheet files configuration must be array');
                        }
                        foreach ($option['stylesheet'] as $file) {
                            $this->addCssFile($file);
                        }
                    }
                    if (isset($option['stylesheetIe'])) {
                        if (!is_array($option['stylesheetIe'])) {
                            throw new RuntimeException('Autoload IE stylesheet conditions configuration must be array');
                        }
                        foreach ($option['stylesheetIe'] as $condition => $files) {
                            if (!is_array($files)) {
                                throw new RuntimeException('Autoload IE stylesheet condition files configuration must be array');
                            }
                            foreach ($files as $file) {
                                $this->addCssIeFile($file, $condition);
                            }
                        }
                    }
                    if (isset($option['javascript'])) {
                        if (!is_array($option['javascript'])) {
                            throw new RuntimeException('Autoload javascript files configuration must be array');
                        }
                        foreach ($option['javascript'] as $file) {
                            $this->addJsFile($file);
                        }
                    }
                    if (isset($option['javascriptIe'])) {
                        if (!is_array($option['javascriptIe'])) {
                            throw new RuntimeException('Autoload IE javascript conditions configuration must be array');
                        }
                        foreach ($option['javascriptIe'] as $condition => $files) {
                            if (!is_array($files)) {
                                throw new RuntimeException('Autoload IE javascript condition files configuration must be array');
                            }
                            foreach ($files as $file) {
                                $this->addJsIeFile($file, $condition);
                            }
                        }
                    }
                    break;
            }
        }
    }

}
