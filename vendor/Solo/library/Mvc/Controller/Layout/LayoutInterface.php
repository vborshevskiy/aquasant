<?php
namespace Solo\Mvc\Controller\Layout;

interface LayoutInterface {
	
	public function setOptions(array $options);
	public function getViewModel();
	public function mutate($vars);
	
}

?>