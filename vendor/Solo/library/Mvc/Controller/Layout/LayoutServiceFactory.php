<?php

namespace Solo\Mvc\Controller\Layout;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Solo\Mvc\Controller\LayoutFactory;

class LayoutServiceFactory implements FactoryInterface {

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \Zend\ServiceManager\FactoryInterface::createService()
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		$config = $serviceLocator->get('Config');
		if (!isset($config['layout']) || !isset($config['layout']['adapter'])) {
			$layout = LayoutFactory::factoryDefault();
		} else {
			$layout = LayoutFactory::factory($config['layout']['adapter']);
		}
		if (isset($config['layout']) && isset($config['layout']['options'])) {
			$layout->setOptions($config['layout']['options']);
		}
		return $layout;
	}

}

?>