<?php

namespace Solo\Sitemap;
use Solo\Collection\Collection;

class Sitemap {

    /**
     *
     * @var string
     */
    private $name = null;
    
    /**
     *
     * @var string
     */
    private $location = null;
    
    /**
     *
     * @var \DateTime
     */
    private $lastModified = null;
    
    /**
     *
     * @var \Solo\Collection\Collection
     */
    private $urls = null;
    
    /**
     * 
     * @param string $name
     */
    public function __construct($name) {
        if (is_null($name)) {
            throw new \Solo\Sitemap\Exception\RuntimeException('Sitemap name can\'t be empty');
        }
        $this->name = $name;
        $this->urls = new Collection();
    }
    
    /**
     * 
     * @return string
     */
    public function getName() {
        return $this->name;
    }
    
    /**
     * 
     * @param string $name
     * @return \Solo\Sitemap\Sitemap
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getLocation() {
        return $this->location;
    }
    
    /**
     * 
     * @param string $location
     * @return \Solo\Sitemap\Sitemap
     */
    public function setLocation($location) {
        $this->location = $location;
        return $this;
    }
    
    /**
     * 
     * @return \DateTime
     */
    public function getLastModified() {
        return $this->lastModified;
    }
    
    /**
     * 
     * @return string
     */
    public function getLastModifiedInIso8601Format() {
        if ($this->lastModified instanceof \DateTime) {
            return $this->lastModified->format('c');
        }
        return null;
    }
    
    /**
     * 
     * @param \DateTime $dateTime
     * @return \Solo\Sitemap\Sitemap
     */
    public function setLastModified(\DateTime $dateTime) {
        $this->lastModified = $dateTime;
        return $this;
    }
    
    /**
     * 
     * @param \Solo\Sitemap\Link $url
     * @return \Solo\Sitemap\Sitemap
     */
    public function addUrl(Url $url) {
        $this->urls->add($url);
        return $this;
    }
    
    public function getXml($version, $encoding, $xmlns) {
        $xml = '';
        $xml .= '<?xml version="'.$version.'" encoding="'.$encoding.'"?>'. "\r\n";
        $xml .= '<urlset xmlns="'.$xmlns.'">'. "\r\n";
        foreach ($this->urls->toArray() as $url) {
            $xml .= $url->getXml();
        }
        $xml .= '</urlset>';
        return $xml;
    }

}

?>