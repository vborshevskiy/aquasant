<?php

namespace Solo\Sitemap;
use Solo\Collection\Collection;
use Solo\Sitemap\Sitemap;

class SitemapIndex {
    
    const CHANGE_FREQUENCY_ALWAYS = 'always';
    
    const CHANGE_FREQUENCY_HOURLY = 'hourly';
    
    const CHANGE_FREQUENCY_DAYLY = 'dayly';
    
    const CHANGE_FREQUENCY_WEEKLY = 'weekly';
    
    const CHANGE_FREQUENCY_MONTHLY = 'monthly';
    
    const CHANGE_FREQUENCY_YEARLY = 'yearly';
    
    const CHANGE_FREQUENCY_NEVER = 'never';

    /**
     *
     * @var \Solo\Collection\Collection
     */
    private $sitemaps = null;
    
    /**
     *
     * @var string
     */
    private $xmlVersion = '1.0';
    
    /**
     *
     * @var string
     */
    private $xmlEncoding = 'UTF-8';

    /**
     *
     * @var string
     */
    private $xmlns = 'http://www.sitemaps.org/schemas/sitemap/0.9';

    public function __construct() {
        $this->sitemaps = new Collection();
    }
    
    /**
     * 
     * @return \Solo\Collection\Collection
     */
    public function sitemaps() {
        return $this->sitemaps;
    }
    
    /**
     * 
     * @param string $name
     * @return boolean
     */
    public function hasSitemap($name) {
        return $this->sitemaps->keyExists($name);
    }

    /**
     * 
     * @param string $name
     * @return \Solo\Sitemap\Sitemap
     */
    public function getSitemap($name) {
        if ($this->hasSitemap($name)) {
            return $this->sitemaps->offsetGet($name);
        }
        return null;
    }
    
    /**
     * 
     * @param \Solo\Sitemap\Sitemap $sitemap
     * @param string $name
     * @return \Solo\Sitemap\SitemapIndex
     */
    public function addSitemap(Sitemap $sitemap, $name = null) {
        if (is_null($name)) {
            $name = $sitemap->getName();
        }
        $this->sitemaps->add($sitemap, $name);
        return $this;
    }
    
    /**
     * 
     * @param string $xmlVersion
     * @return \Solo\Sitemap\SitemapIndex
     */
    public function setXmlVersion($xmlVersion) {
        $this->xmlVersion = $xmlVersion;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getXmlVersion() {
        return $this->xmlVersion;
    }
    
    /**
     * 
     * @param string $xmlEncoding
     * @return \Solo\Sitemap\SitemapIndex
     */
    public function setXmlEncoding($xmlEncoding) {
        $this->xmlEncoding = $xmlEncoding;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getXmlEncoding() {
        return $this->xmlEncoding;
    }
    
    /**
     * 
     * @param string $xmlns
     * @return \Solo\Sitemap\SitemapIndex
     */
    public function setXmlns($xmlns) {
        $this->xmlns = $xmlns;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getXmlns() {
        return $this->xmlns;
    }

    /**
     * 
     * @param string $path
     */
    public function saveSitemap($path, $cityUid, $mainDomain, $subdomain) {
        $xmlIndex .= '<?xml version="' . $this->getXmlVersion() . '" encoding="' . $this->getXmlEncoding() . '"?>' . "\r\n";
        $xmlIndex .= '<sitemapindex xmlns="' . $this->getXmlns() . '">' . "\r\n";
        $xmls = [];
        foreach ($this->sitemaps()->toArray() as $sitemap) {
            $xmlIndex .= '<sitemap>' . "\r\n";
            $xmlIndex .= "\t" . '<loc>' . 'http://' . (is_null($subdomain) || empty($subdomain) ? '' : $subdomain . '.') . $mainDomain . '/sitemap/' . $cityUid . '/' . $sitemap->getLocation() . '</loc>' . "\r\n";
            if (!is_null($sitemap->getLastModified())) {
                $xmlIndex .= "\t" . '<lastmod>' . $sitemap->getLastModifiedInIso8601Format() . '</lastmod>' . "\r\n";
            }
            $xmlIndex .= '</sitemap>' . "\r\n";
            $xmls[$sitemap->getName()] = $sitemap->getXml($this->getXmlVersion(), $this->getXmlEncoding(), $this->getXmlns());
        }
        $xmlIndex .= '</sitemapindex>' . "\r\n";
        file_put_contents($path . 'sitemap.xml', $xmlIndex);
        foreach ($xmls as $fileName => $xml) {
            file_put_contents($path . $fileName . '.xml', $xml);
        }
    }

}

?>