<?php

namespace Solo\Sitemap;

class Url {

    private $location = null;
    
    /**
     *
     * @var string
     */
    private $changeFrequency = null;
    
    /**
     *
     * @var \DateTime
     */
    private $lastModified = null;
    
    /**
     *
     * @var float
     */
    private $priority = null;
    
    /**
     * 
     * @return string
     */
    public function getLocation() {
        return $this->location;
    }
    
    /**
     * 
     * @param string $location
     * @return \Solo\Sitemap\Url
     * @throws Solo\Sitemap\Exception\LengthException
     */
    public function setLocation($location) {
        if (mb_strlen($location) > 2048) {
            throw new Solo\Sitemap\Exception\LengthException('Url location too long');
        }
        if (substr($location, strlen($location) - 1) == '?') {
            $location = substr($location, 0, strlen($location) - 1);
        }
        $this->location = $location;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getChangeFrequency() {
        return $this->changeFrequency;
    }
    
    /**
     * 
     * @param string $changeFrequency
     * @return \Solo\Sitemap\Url
     */
    public function setChangeFrequency($changeFrequency) {
        $this->changeFrequency = $changeFrequency;
        return $this;
    }
    
    /**
     * 
     * @return \DateTime
     */
    public function getLastModified() {
        return $this->lastModified;
    }
    
    /**
     * 
     * @param \DateTime $lastModified
     * @return \Solo\Sitemap\Url
     */
    public function setLastModified($lastModified) {
        $this->lastModified = $lastModified;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getLastModifiedInIso8601Format() {
        if ($this->lastModified instanceof \DateTime) {
            return $this->lastModified->format('c');
        }
        return null;
    }
    
    /**
     * 
     * @return float
     */
    public function getPriority() {
        return $this->priority;
    }
    
    /**
     * 
     * @param float $priority
     * @return \Solo\Sitemap\Url
     * @throws \Solo\Sitemap\Exception\InvalidArgumentException
     */
    public function setPriority($priority) {
        if ($priority > 1 || $priority < 0) {
            throw new \Solo\Sitemap\Exception\InvalidArgumentException('Sitemap priority must be in interval from 0 to 1');
        }
        $this->priority = $priority;
        return $this;
    }
    
    public function getXml() {
        $xml = '';
        $xml .= "\t".'<url>'. "\r\n";
        $xml .= "\t\t".'<loc>'.$this->getLocation().'</loc>'. "\r\n";
        if (!is_null($this->getChangeFrequency())) {
            $xml .= "\t\t".'<changefreq>'.$this->getChangeFrequency().'</changefreq>'. "\r\n";
        }
        if (!is_null($this->getPriority())) {
            $xml .= "\t\t".'<priority>'.$this->getPriority().'</priority>'. "\r\n";
        }
        if (!is_null($this->getLastModified())) {
            $xml .= "\t\t".'<lastmod>'.$this->getLastModifiedInIso8601Format().'</lastmod>'. "\r\n";
        }
        $xml .= "\t".'</url>'. "\r\n";
        return $xml;
    }

}

?>