<?php

namespace Solo\Sphinx;

/**
 * Searchd
 *
 * @author slava
 */
class Searchd {

	private $command = null;

	public function __construct($command = '/etc/init.d/sphinxsearch') {
		if ($command !== null) {
			$this->setCommand($command);
		}
	}

	public function getCommand() {
		return $this->command;
	}

	public function setCommand($command) {
		$this->command = $command;
	}

	public function stop() {
		exec($this->getCommand() . " stop", $output);
		return $output;
	}

	public function start() {
		exec($this->getCommand() . " start", $output);
		return $output;
	}

	public function restart() {
		exec($this->getCommand() . " restart", $output);
		return $output;
	}

}

?>
