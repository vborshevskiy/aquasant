<?php

namespace Solo\Sphinx;

/**
 * Description of Config
 *
 * @author slava
 */
class Config {

	private $path = null;

	private $savePath = null;

	private $content = null;

	private $renderedContent = null;

	private $vars = null;

	public function __construct($path) {
		$this->setPath($path);
	}

	public function readConfig() {
		if (null === $this->path) {
			throw new \Exception('Sphinx config path can not be empty');
		}
		$this->content = file_get_contents($this->path);
	}

	public function setVar($name, $value) {
		$key = '<' . $name . '>';
		$this->vars[$key] = $value;
	}

	public function removeVar($name) {
		if (isset($this->var['<' . $name . '>'])) {
			unset($this->var['<' . $name . '>']);
		}
	}

	public function render() {
		$this->setRenderedContent(str_replace(array_keys($this->vars), array_values($this->vars), $this->content));
	}

	public function save($path = null) {
		if (null === $path) {
			$path = $this->path;
		}
		$this->setSavePath($path);
		file_put_contents($this->getSavePath(), $this->getRenderedContent());
	}

	public function getPath() {
		return $this->path;
	}

	public function setPath($path) {
		$this->path = $path;
	}

	public function getSavePath() {
		return $this->savePath;
	}

	public function setSavePath($savePath) {
		$this->savePath = $savePath;
	}

	public function getContent() {
		return $this->content;
	}

	public function setContent($content) {
		$this->content = $content;
	}

	public function getRenderedContent() {
		return $this->renderedContent;
	}

	public function setRenderedContent($renderedContent) {
		$this->renderedContent = $renderedContent;
	}

	public static function process($fromPath, $toPath, array $vars) {
		$config = new self($fromPath);
		$config->readConfig();
		foreach ($vars as $varName => $varValue) {
			$config->setVar($varName, $varValue);
		}
		$config->render();
		$config->save($toPath);
		return $config;
	}

}

?>
