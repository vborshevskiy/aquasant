<?php

namespace Solo\Sphinx;

/**
 * Indexer
 *
 * @author slava
 */
class Indexer {

	private $command = 'indexer';

	private $options = array();

	private $index = null;

	private $params = null;

	public function __construct($index = null, array $options = array(), array $params = array(), $command = 'indexer') {
		if (null !== $index) {
			$this->setIndex($index);
		}
		if (sizeof($options) > 0) {
			$this->setOptions($options);
		}
		if ($command !== null) {
			$this->setCommand($command);
		}
		if (sizeof($params) > 0) {
			$this->setParams($params);
		}
	}

	public function setParam($name, $value) {
		$this->params[$name] = $value;
		return $this;
	}

	public function removeParam($name) {
		if (isset($this->params[$name])) {
			unset($this->params[$name]);
		}
		return $this;
	}

	public function getParams() {
		return $this->params;
	}

	public function setParams($params) {
		$this->params = $params;
		return $this;
	}

	public function getCommand() {
		return $this->command;
	}

	public function setCommand($command) {
		$this->command = $command;
		return $this;
	}

	public function getOptions() {
		return $this->options;
	}

	public function setOptions($options) {
		$this->options = $options;
		return $this;
	}

	public function getIndex() {
		return $this->index;
	}

	public function setIndex($index) {
		$this->index = $index;
		return $this;
	}

	public function addOption($option) {
		$this->options[] = $option;
		return $this;
	}

	public function removeOption($option) {
		$key = array_search($option, $this->options);
		if ($key !== false) {
			unset($this->options[$k]);
		}
		return $this;
	}

	public function run() {
		$command = $this->getCommand();
		$command .= ' ' . $this->getIndex();
		if (sizeof($this->params) > 0) {
			foreach ($this->getParams() as $paramName => $paramVal) {
				$command .= ' --' . $paramName . ' ' . $paramVal;
			}
		}
		if (sizeof($this->options)) {
			foreach ($this->getOptions() as $option) {
				$command .= ' --' . $option;
			}
		}
		exec($command, $output);
		return $output;
	}

}

?>
