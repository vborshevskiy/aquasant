<?php

namespace Solo\Google\Feed;

use Solo\DateTime\DateTime;

class Feed {

	/**
	 *
	 * @var string
	 */
	private $title;

	/**
	 *
	 * @var string
	 */
	private $link;

	/**
	 *
	 * @var DateTime
	 */
	private $updatedDate;

	/**
	 *
	 * @var string
	 */
	private $description;

	/**
	 *
	 * @var array
	 */
	private $items = [];

	/**
	 *
	 * @param FeedItem $item        	
	 * @return FeedItem
	 */
	public function addItem(FeedItem $item) {
		$this->items[$item->getId()] = $item;
		return $item;
	}

	/**
	 *
	 * @return array
	 */
	public function getItems() {
		return $this->items;
	}

	/**
	 *
	 * @param string $itemId        	
	 * @return boolean
	 */
	public function hasItem($itemId) {
		return isset($this->items[$itemId]);
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasItems() {
		return (0 < count($this->items));
	}

	/**
	 *
	 * @param string $itemId        	
	 * @return FeedItem
	 */
	public function getItem($itemId) {
		$result = null;
		
		if (isset($this->items[$itemId])) {
			$result = $this->items[$itemId];
		}
		
		return $result;
	}

	/**
	 *
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 *
	 * @param string $title        	
	 */
	public function setTitle($title) {
		$this->title = $title;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getLink() {
		return $this->link;
	}

	/**
	 *
	 * @param string $link        	
	 */
	public function setLink($link) {
		$this->link = $link;
		return $this;
	}

	/**
	 *
	 * @return \Solo\DateTime\DateTime
	 */
	public function getUpdatedDate() {
		return $this->updatedDate;
	}

	/**
	 *
	 * @param \Solo\DateTime\DateTime $updatedDate        	
	 */
	public function setUpdatedDate($updatedDate) {
		$this->updatedDate = $updatedDate;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 *
	 * @param string $description        	
	 */
	public function setDescription($description) {
		$this->description = $description;
		return $this;
	}

}

?>