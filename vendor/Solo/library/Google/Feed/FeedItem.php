<?php

namespace Solo\Google\Feed;

use Solo\DateTime\DateTime;

class FeedItem {

	/**
	 *
	 * @var string
	 */
	private $id;

	/**
	 *
	 * @var string
	 */
	private $title;

	/**
	 *
	 * @var string
	 */
	private $description;

	/**
	 *
	 * @var string
	 */
	private $link;

	/**
	 *
	 * @var string
	 */
	private $imageLink;

	/**
	 *
	 * @var array
	 */
	private $additionalImageLinks = [];

	const AVAILABILITY_IN_STOCK = 'in stock';

	const AVAILABILITY_OUT_OF_STOCK = 'out of stock';

	const AVAILABILITY_PREORDER = 'preorder';

	/**
	 *
	 * @var string
	 */
	private $availability;

	/**
	 *
	 * @var DateTime
	 */
	private $availabilityDate;

	/**
	 *
	 * @var float
	 */
	private $price;

	/**
	 *
	 * @var float
	 */
	private $salePrice;

	/**
	 *
	 * @var string
	 */
	private $category;

	/**
	 *
	 * @var string
	 */
	private $productType;

	/**
	 *
	 * @var string
	 */
	private $brand;

	/**
	 *
	 * @var string
	 */
	private $gtin;

	/**
	 *
	 * @return string
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param string $id        	
	 * @throws \InvalidArgumentException
	 * @return \Solo\Google\Feed\FeedItem
	 */
	public function setId($id) {
		if (50 < mb_strlen($id)) {
			throw new \InvalidArgumentException('Id must be less than 50 symbols');
		}
		$this->id = trim($id);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 *
	 * @param string $title        	
	 * @throws \InvalidArgumentException
	 * @return \Solo\Google\Feed\FeedItem
	 */
	public function setTitle($title) {
		if (150 < mb_strlen($title)) {
			throw new \InvalidArgumentException('Title must be less than 150 symbols');
		}
		$this->title = trim($title);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 *
	 * @param string $description        	
	 * @throws \InvalidArgumentException
	 * @return \Solo\Google\Feed\FeedItem
	 */
	public function setDescription($description) {
		if (5000 < mb_strlen($description)) {
			throw new \InvalidArgumentException('Description must be less than 5000 symbols');
		}
		$this->description = trim($description);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getLink() {
		return $this->link;
	}

	/**
	 *
	 * @param string $link        	
	 * @return \Solo\Google\Feed\FeedItem
	 */
	public function setLink($link) {
		$this->link = $link;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getImageLink() {
		return $this->imageLink;
	}

	/**
	 *
	 * @param string $imageLink        	
	 * @return \Solo\Google\Feed\FeedItem
	 */
	public function setImageLink($imageLink) {
		$this->imageLink = $imageLink;
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getAdditionalImageLinks() {
		return $this->additionalImageLinks;
	}

	/**
	 *
	 * @param string $additionalImageLink        	
	 * @return \Solo\Google\Feed\FeedItem
	 */
	public function addAdditionalImageLink($additionalImageLink) {
		$this->additionalImageLinks[] = $additionalImageLink;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getAvailability() {
		return $this->availability;
	}

	/**
	 *
	 * @param string $availability        	
	 * @throws \InvalidArgumentException
	 * @return \Solo\Google\Feed\FeedItem
	 */
	public function setAvailability($availability) {
		if (!in_array($availability, [
			self::AVAILABILITY_IN_STOCK,
			self::AVAILABILITY_OUT_OF_STOCK,
			self::AVAILABILITY_PREORDER 
		])) {
			throw new \InvalidArgumentException(sprintf('Invalid availability value "%s"', $availability));
		}
		$this->availability = $availability;
		return $this;
	}

	/**
	 *
	 * @return \Solo\DateTime\DateTime
	 */
	public function getAvailabilityDate() {
		return $this->availabilityDate;
	}

	/**
	 *
	 * @param DateTime $availabilityDate        	
	 * @return \Solo\Google\Feed\FeedItem
	 */
	public function setAvailabilityDate(DateTime $availabilityDate) {
		$this->availabilityDate = $availabilityDate;
		return $this;
	}

	/**
	 *
	 * @return float
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 *
	 * @param float $price        	
	 * @throws \InvalidArgumentException
	 * @return \Solo\Google\Feed\FeedItem
	 */
	public function setPrice($price) {
		if (!is_numeric($price)) {
			throw new \InvalidArgumentException('Price must be numeric');
		}
		$this->price = floatval($price);
		return $this;
	}

	/**
	 *
	 * @return float
	 */
	public function getSalePrice() {
		return $this->salePrice;
	}

	/**
	 *
	 * @param float $salePrice        	
	 * @throws \InvalidArgumentException
	 * @return \Solo\Google\Feed\FeedItem
	 */
	public function setSalePrice($salePrice) {
		if (!is_numeric($salePrice)) {
			throw new \InvalidArgumentException('Sale price must be numeric');
		}
		$this->salePrice = floatval($salePrice);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getCategory() {
		return $this->category;
	}

	/**
	 *
	 * @param string $category        	
	 * @return \Solo\Google\Feed\FeedItem
	 */
	public function setCategory($category) {
		$this->category = $category;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getBrand() {
		return $this->brand;
	}

	/**
	 *
	 * @param string $brand        	
	 * @return \Solo\Google\Feed\FeedItem
	 */
	public function setBrand($brand) {
		$this->brand = $brand;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getGtin() {
		return $this->gtin;
	}

	/**
	 *
	 * @param string $gtin        	
	 * @return \Solo\Google\Feed\FeedItem
	 */
	public function setGtin($gtin) {
		$this->gtin = $gtin;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getProductType() {
		return $this->productType;
	}

	/**
	 *
	 * @param string $productType        	
	 * @return \Solo\Google\Feed\FeedItem
	 */
	public function setProductType($productType) {
		$this->productType = $productType;
		return $this;
	}

}

?>