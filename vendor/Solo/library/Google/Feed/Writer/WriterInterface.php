<?php

namespace Solo\Google\Feed\Writer;

interface WriterInterface {

	/**
	 *
	 * @param \XmlWriter $xml        	
	 */
	public function write(\XmlWriter $xml);

}

?>