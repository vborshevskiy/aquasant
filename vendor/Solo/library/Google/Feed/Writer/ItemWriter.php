<?php

namespace Solo\Google\Feed\Writer;

use Solo\Google\Feed\FeedItem;

class ItemWriter implements WriterInterface {

	/**
	 *
	 * @var FeedItem
	 */
	private $item;

	/**
	 *
	 * @param FeedItem $item        	
	 */
	public function __construct(FeedItem $item) {
		$this->item = $item;
	}

	/**
	 *
	 * @see \Solo\Google\Feed\Writer\WriterInterface::write()
	 */
	public function write(\XmlWriter $xml) {
		$xml->startElement('item');
		
		$xml->writeElement('g:id', $this->item->getId());
		$xml->writeElement('title', $this->item->getTitle());
		$xml->writeElement('g:description', $this->item->getDescription());
		$xml->writeElement('link', $this->item->getLink());
		$xml->writeElement('g:image_link', $this->item->getImageLink());
		
		$maxLimit = 10;
		$index = 1;
		foreach ($this->item->getAdditionalImageLinks() as $additionalImageLink) {
			if ($index < $maxLimit) {
				$xml->writeElement('g:additional_image_link', $additionalImageLink);
			}
			$index++;
		}
		
		$xml->writeElement('g:availability', $this->item->getAvailability());
		if (FeedItem::AVAILABILITY_PREORDER == $this->item->getAvailability()) {
			$xml->writeElement('g:availability_date', $this->item->getAvailabilityDate()->format('Y-m-d\TH:i:s'));
		}
		
		$xml->writeElement('g:price', $this->item->getPrice() . ' RUB');
		if ((0 < $this->item->getSalePrice()) && ($this->item->getSalePrice() < $this->item->getPrice())) {
			$xml->writeAttribute('g:sale_price', $this->item->getPrice() . ' RUB');
		}
		
		if ($this->item->getCategory()) {
			$xml->writeElement('g:google_product_category', $this->item->getCategory());
		}
		if ($this->item->getProductType()) {
			$xml->writeElement('g:product_type', $this->item->getProductType());
		}
		$xml->writeElement('g:brand', $this->item->getBrand());
		$xml->writeElement('g:gtin', $this->item->getGtin());
		
		$xml->endElement();
	}

}

?>