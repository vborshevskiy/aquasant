<?php

namespace Solo\Google\Feed\Writer;

use Solo\Google\Feed\Feed;

class FeedWriter implements WriterInterface {

	/**
	 *
	 * @var Feed
	 */
	private $feed;

	/**
	 *
	 * @param Feed $feed        	
	 */
	public function __construct(Feed $feed) {
		$this->feed = $feed;
	}

	/**
	 *
	 * @see \Solo\Google\Feed\Writer\WriterInterface::write()
	 */
	public function write(\XmlWriter $xml) {
		$xml->startElement('rss');
		$xml->writeAttribute('version', '2.0');
		$xml->writeAttribute('xmlns:g', 'http://base.google.com/ns/1.0');
		
		$xml->startElement('channel');
		$xml->writeElement('title', $this->feed->getTitle());
		$xml->writeElement('link', $this->feed->getLink());
		$xml->writeElement('description', $this->feed->getDescription());
		
		foreach ($this->feed->getItems() as $item) {
			$itemWriter = new ItemWriter($item);
			$itemWriter->write($xml);
		}
		
		$xml->endElement(); // channel
		
		$xml->endElement();
	}

	/**
	 *
	 * @return string
	 */
	public function writeToString() {
		$xml = new \XMLWriter();
		$xml->openMemory();
		$xml->setIndent(true);
		$xml->startDocument('1.0');
		
		$this->write($xml);
		
		$xml->endDocument();
		return $xml->outputMemory(true);
	}

	/**
	 *
	 * @param string $filePath        	
	 */
	public function writeToFile($filePath) {
		$xml = new \XMLWriter();
		$xml->openUri($filePath);
		$xml->setIndent(true);
		$xml->startDocument('1.0');
		
		$this->write($xml);
		
		$xml->endDocument();
	}

}

?>