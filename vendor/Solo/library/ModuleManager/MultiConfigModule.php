<?php
namespace Solo\ModuleManager;

use Zend\Stdlib\ArrayUtils;
class MultiConfigModule {

	/**
	 *
	 * @var array
	 */
	private $configFiles = [];

	/**
	 *
	 * @var string
	 */
	private $moduleFolder;

	/**
	 *
	 * @param string $moduleFolder        	
	 */
	public function __construct($moduleFolder) {
		$this->moduleFolder = $moduleFolder;
		$this->addConfigFile('module.config.php');
		$this->addConfigFile('global.php', false);
		$this->addConfigFile('local.php', false);
	}

	/**
	 *
	 * @param string $filename        	
	 * @param boolean $throw_exception        	
	 * @throws Exception\FileNotFoundException
	 */
	protected function addConfigFile($filename, $throw_exception = true) {
		$filepath = $this->moduleFolder . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . $filename;
		if (! file_exists($filepath) || ! is_file($filepath)) {
			if ($throw_exception) {
				throw new Exception\FileNotFoundException('Config file ' . $filepath . ' not found');
			} else {
				return;
			}
		}
		$this->configFiles[$filename] = $filepath;
	}

	/**
	 * Merge and return config files
	 * 
	 * @return array
	 */
	public function getConfig() {
		$config = [];
		foreach ($this->configFiles as $configFile) {
			$config = ArrayUtils::merge($config, include $configFile);
		}
		return $config;
	}

}

?>