<?php
return array(
	'di' => array(),
	'service_manager' => array(
		'factories' => array(
			'SmartyEngine' => function($sm) {
				$engine = new \Smarty();

				$config = $sm->get('Config');
				if (isset($config['smarty_settings']) && is_array($config['smarty_settings'])) {
					foreach ($config['smarty_settings'] as $param => $value) {
						if (!property_exists(get_class($engine), $param)) {
							throw new \InvalidArgumentException('Invalid smarty settings parameter named \''.$param.'\'');
						}
						$engine->$param = $value;
					}
				}
				
				return $engine;
			}
		)
	)
);