<?php

function smarty_modifier_geo_accusative($txt) {
    $txtArray = explode(' ', $txt);
    foreach ($txtArray as &$word) {
        $lastSymbol = substr($word, -2);
        if (in_array($lastSymbol, [
            'а'
        ])) {
            $word = substr(0, strlen($word) - 2) . 'ы';
        } elseif (in_array($lastSymbol, [
            'б',
            'в',
            'г',
            'д',
            'ж',
            'з',
            'к',
            'л',
            'м',
            'н',
            'п',
            'р',
            'с',
            'т',
            'ф',
            'х',
            'ц',
            'ч',
            'ш',
            'щ',
        ])) {
            $word .= 'а';
        } elseif (in_array($lastSymbol, [
            'й',
            'ь'
        ])) {
            $word = substr(0, strlen($word) - 2) . 'я';
        } elseif (in_array($lastSymbol, [
            'я'
        ])) {
            $word = substr(0, strlen($word) - 2) . 'и';
        }
    }
    return implode(' ', $txtArray);
}
