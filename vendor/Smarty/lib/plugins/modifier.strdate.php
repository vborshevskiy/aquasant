<?php
function smarty_modifier_strdate($strdate, $toEmail = false)
{
    $timestamp = strtotime($strdate);
    $month = date("m", $timestamp);
    $monthRuName = "";
    switch ($month) {
        case 1:
            $monthRuName = "января";
            break;
        case 2:
            $monthRuName = "февраля";
            break;
        case 3:
            $monthRuName = "марта";
            break;
        case 4:
            $monthRuName = "апреля";
            break;
        case 5:
            $monthRuName = "мая";
            break;
        case 6:
            $monthRuName = "июня";
            break;
        case 7:
            $monthRuName = "июля";
            break;
        case 8:
            $monthRuName = "августа";
            break;
        case 9:
            $monthRuName = "сентября";
            break;
        case 10:
            $monthRuName = "октября";
            break;
        case 11:
            $monthRuName = "ноября";
            break;
        case 12:
            $monthRuName = "декабря";
            break;
    }
    if ($toEmail) {
        $weekDay = date("N", $timestamp);
        $weekDayName = null;
        switch ($weekDay) {
            case 1:
                $weekDayName = "понедельника";
                break;
            case 2:
                $weekDayName = "вторника";
                break;
            case 2:
                $weekDayName = "среды";
                break;
            case 2:
                $weekDayName = "четверга";
                break;
            case 2:
                $weekDayName = "пятницы";
                break;
            case 2:
                $weekDayName = "субботы";
                break;
            case 2:
                $weekDayName = "воскресенья";
                break;
        }
        return date("H", $timestamp).":".date("i", $timestamp)." ".$weekDayName." ".date("d", $timestamp)." ".$monthRuName;
    } else {
        return date("d", $timestamp)." ".$monthRuName." ".date("H", $timestamp).":".date("i", $timestamp);
    }
} 
?>
