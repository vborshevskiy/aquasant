<?php

function smarty_modifier_clear_question_mark($str)
{
    if (substr($str, strlen($str) - 1) == '?') {
        return substr($str, 0, strlen($str) - 1);
    }
    return $str;
} 

?>