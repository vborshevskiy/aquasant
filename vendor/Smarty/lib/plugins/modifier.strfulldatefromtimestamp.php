<?php
function smarty_modifier_strfulldatefromtimestamp($timestamp)
{
    $month = date('m', $timestamp);
    $monthRuName = '';
    switch ($month) {
        case 1:
            $monthRuName = 'января';
            break;
        case 2:
            $monthRuName = 'февраля';
            break;
        case 3:
            $monthRuName = 'марта';
            break;
        case 4:
            $monthRuName = 'апреля';
            break;
        case 5:
            $monthRuName = 'мая';
            break;
        case 6:
            $monthRuName = 'июня';
            break;
        case 7:
            $monthRuName = 'июля';
            break;
        case 8:
            $monthRuName = 'августа';
            break;
        case 9:
            $monthRuName = 'сентября';
            break;
        case 10:
            $monthRuName = 'октября';
            break;
        case 11:
            $monthRuName = 'ноября';
            break;
        case 12:
            $monthRuName = 'декабря';
            break;
    }
    return date('j', $timestamp).' '.$monthRuName.' '.date("Y", $timestamp);
} 
?>
