<?php

function smarty_modifier_clear_punctuation_marks($str) {
    return preg_replace('/[^\w\s]/u', ' ', $str);
}

?>
