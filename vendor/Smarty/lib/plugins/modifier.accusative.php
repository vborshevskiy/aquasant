<?php

function smarty_modifier_accusative($txt) {
    $txtArray = explode(' ', $txt);
    foreach ($txtArray as &$word) {
        $word = preg_replace('/ая$/', 'ую', $word);
        $word = preg_replace('/я$/', 'ю', $word);
        $word = preg_replace('/а$/', 'у', $word);
    }
    return implode(' ', $txtArray);
}
