<?php

	function smarty_modifier_parse_json_date($val, $format = 'U') {
		preg_match('/(\d{10})(\d{3})([\+\-]\d{4})/', $val, $matches);
        // Get the timestamp as the TS tring / 1000
        $ts = (int) $matches[1];

        // Get the timezone name by offset
        $tz = (int) $matches[3];
        $tz = timezone_name_from_abbr("", $tz / 100 * 3600, false);
        $tz = new \DateTimeZone($tz);

        // Create a new DateTime, set the timestamp and the timezone
        $dt = new \DateTime();
        $dt->setTimestamp($ts);
        $dt->setTimezone($tz);
        // Echo the formatted value         
        return $dt->format($format);
	}

?>