<?php

	function smarty_modifier_format_price($price) {
		$price = intval($price);
		if (3 < strlen($price)) {
			$chunks = str_split(strrev($price), 3);
			return strrev(implode(' ', $chunks));
		}
		return $price;
	}

?>
