<?php
function smarty_modifier_counterJustWord($word, $count) {
    $strCountLastNum = substr((string)$count, -1);
    switch ($strCountLastNum) {
        case '0':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            return $word;
            break;
        case '1':
            return $word."a";
            break;
        case '2':
        case '3':
        case '4':
            return $word."и";
            break;
    }
}
?>
