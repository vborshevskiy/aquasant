<?php

function smarty_modifier_geo_prepositional($txt) {
    
    $exceptions = [
        'Березники' => 'Березниках',
        'Великий Новгород' => 'Великом Новгороде',
        'Волжский' => 'Волжском',
        'Каменск-Уральский' => 'Каменске-Уральском',
        'Каменск-Шахтинский' => 'Каменске-Шахтинском',
        'Набережные Челны' => 'Набережных Челнах',
        'Нижний Новгород' => 'Нижнем Новгороде',
        'Нижний Тагил' => 'Нижнем Тагиле',
        'Октябрьский' => 'Октябрьском',
        'Орел' => 'Орле',
        'Ростов-на-Дону' => 'Ростове-на-Дону',
        'Сочи' => 'Сочи',
        'Старый Оскол' => 'Старом Осколе',
        'Тольятти' => 'Тольятти',
        'Чебоксары' => 'Чебоксарах',
        'Череповец' => 'Череповце',
        'Шахты' => 'Шахтах',
        'Ярославль' => 'Ярославле',
    ];
    
    if (isset($exceptions[$txt])) {
        return $exceptions[$txt];
    }
    
    $txtArray = explode(' ', $txt);
    foreach ($txtArray as &$word) {
        $lastSymbol = substr($word, -2);
        if (in_array($lastSymbol, [
            'а'
        ])) {
            $word = substr($word, 0, strlen($word) - 2) . 'е';
        } elseif (in_array($lastSymbol, [
            'б',
            'в',
            'г',
            'д',
            'ж',
            'з',
            'к',
            'л',
            'м',
            'н',
            'п',
            'р',
            'с',
            'т',
            'ф',
            'х',
            'ц',
            'ч',
            'ш',
            'щ',
        ])) {
            $word .= 'е';
        } elseif (in_array($lastSymbol, [
            'й',
            'ь'
        ])) {
            $word = substr($word, 0, strlen($word) - 2) . 'и';
        } elseif (in_array($lastSymbol, [
            'я'
        ])) {
            $word = substr($word, 0, strlen($word) - 2) . 'е';
        }
    }
    return implode(' ', $txtArray);
}
