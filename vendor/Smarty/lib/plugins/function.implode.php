<?php

	/**
	 * @param array $params
	 * @param Smarty $smarty
	 */
	function smarty_function_implode($params, &$smarty) {
		if (!empty($params['pieces']) && is_array($params['pieces']) && (0 < sizeof($params['pieces']))) {
			if ('' === $params['glue']) $params['glue'] = ' ';
			return implode($params['glue'], $params['pieces']);
		}
		return '';
	}

?>