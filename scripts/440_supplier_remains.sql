ALTER TABLE `all_goods_1` ADD COLUMN `Markup` varchar(32) DEFAULT NULL COMMENT 'Рейтинг продавца';
ALTER TABLE `all_goods_2` ADD COLUMN `Markup` varchar(32) DEFAULT NULL COMMENT 'Рейтинг продавца';

INSERT INTO `triggers` (`TriggerName`, `State`, `TableName`) VALUES
('suppliers',	'active',	'suppliers_2'),
('suppliers',	'passive',	'suppliers_1'),
('supplier_goods',	'active',	'supplier_goods_2'),
('supplier_goods',	'passive',	'supplier_goods_1');

DROP TABLE IF EXISTS `supplier_goods_1`;
CREATE TABLE `supplier_goods_1` (
                                  `GoodID` bigint(20) unsigned NOT NULL DEFAULT '0',
                                  `SupplierID` int(10) unsigned NOT NULL,
                                  `OfficeID` int(11) unsigned NOT NULL,
                                  `Quantity` int(11) DEFAULT NULL,
                                  PRIMARY KEY (`GoodID`,`OfficeID`,`SupplierID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=5461;


DROP TABLE IF EXISTS `supplier_goods_2`;
CREATE TABLE `supplier_goods_2` (
                                  `GoodID` bigint(20) unsigned NOT NULL DEFAULT '0',
                                  `SupplierID` int(10) unsigned NOT NULL,
                                  `OfficeID` int(11) unsigned NOT NULL,
                                  `Quantity` int(11) DEFAULT NULL,
                                  PRIMARY KEY (`GoodID`,`SupplierID`,`OfficeID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=5461;

DROP TABLE IF EXISTS `suppliers_1`;
CREATE TABLE `suppliers_1` (
                             `SupplierID` int(10) unsigned NOT NULL,
                             `SupplierName` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                             `LastPriceUpdated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `suppliers_2`;
CREATE TABLE `suppliers_2` (
                             `SupplierID` int(10) unsigned NOT NULL,
                             `SupplierName` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                             `LastPriceUpdated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;