#!/bin/sh
echo "___________________"
date
if [ -f /home/aqua/web/aquaweb.supportit.ru/aquasant/scripts/quick-deploy.lock ]; then
	echo "lockfile exists!"
	exit 1
fi
touch /home/aqua/web/aquaweb.supportit.ru/aquasant/scripts/quick-deploy.lock

/bin/rm /home/aqua/web/aquaweb.supportit.ru/aquasant/scripts/quick-deploy.lock