<?php

/**
 * Global Configuration Override
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return [
    'office' => [
        'distance' => 50
    ],
    'service_manager' => [
        'factories' => [
            'Zend\Db\Adapter\Adapter' => 'Solo\Db\Adapter\AdapterServiceFactory',
            'Solo\Db\ShardManager\ShardManager' => 'Solo\Db\ShardManager\ShardManagerServiceFactory',
            'Solo\Db\BalancingManager\BalancingManager' => 'Solo\Db\BalancingManager\BalancingManagerFactory',
            'Solo\Db\BalancingManager\BalancingStatistics' => 'Solo\Db\BalancingManager\BalancingStatisticsFactory',
            'Solo\Db\BalancingManager\BalancingObserver' => 'Solo\Db\BalancingManager\BalancingObserverFactory'
        ]
    ],
    'smarty_settings' => [
        'compile_dir' => __DIR__ . '/../../data/smarty/templates_c',
        'cache_dir' => __DIR__ . '/../../data/smarty/cache',
        'caching' => false,
        'cache_lifetime' => 1 * 60 * 60,
        'compile_check' => true,
        'plugins_dir' => [
            __DIR__ . '/../../vendor/Smarty/lib/plugins',
            __DIR__ . '/../../vendor/Smarty/src/Plugins'
        ]
    ],
    'session_cache' => [
         'adapter' => [
             'name' => 'memcached',
             'options' => [
                 'servers' => [
                     [
                         'host' => '127.0.0.1',
                         'port' => '11211',
                         'weight' => '1'
                     ]
                 ],
                 'ttl' => 1800
             ]
         ],
         'plugins' => [
             'serializer' => 'IgBinary'
         ]
    ],
    'predis_settings' => [
        'scheme' => 'tcp',
        'host' => '127.0.0.1',
        'port' => '6379',
    ],
    'solo_erp' => [
        'default' => 'site',
        'default_dir' => 'Method',
        'connections' => [
            'site' => [
                'provider' => 'UltimaRest',
                'options' => [
                    'path' => 'http://109.248.237.80:8338/json/reply/',
                    'login' => 'bitrix',
                    'pass' => 'bitrix',
                    'config' => [
                        'timeout' => 300
                    ]
                ]
            ],
            'replicator' => [
                'provider' => 'UltimaRest',
                'options' => [
                ]
            ],
            'webMethodsGenerator' => [
                'provider' => 'UltimaSoap',
                'options' => [
                    'wsdl' => 'http://109.248.237.80:8338/soap12',
                    'options' => [
                    ]
                ],
            ],
        ]
    ],
    'mail' => [
        'name' => 'smtp.yandex.ru',
        'host' => 'smtp.yandex.ru',
        'port' => 465,
        'from' => 'ibu@ebdrive.com',
        'fromName' => 'Santehbaza',
        'reply_to' => 'ibu@ebdrive.com',
        'transport' => 'smtp',
        'connection_class' => 'plain',
        'connection_config' => [
            'username' => 'ibu@ebdrive.com',
            'password' => 'SuperIlya',
            'ssl' => 'ssl'
        ]
    ],
    'city' => [
        'id' => 2
    ],
    'mailer' => [
        'max_send_attempts' => 3
    ],
    'feedback_emails' => [
        'retail' => 'zakaz@aquasant.ru',
        'wholesale' => 'b2b@aquasant.ru',
        'purchase' => 'zakupki@aquasant.ru',
        'development' => 'pr@aquasant.ru',
        'questions' => 'vopros@aquasant.ru'
    ]
];
