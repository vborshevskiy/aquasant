<?php

return [
    'modules' => [
        'Solo',
        'Application',
        'Smarty',
        'Predis',
        'SxGeo',
        'SoloLog',
        'SoloCache',
        'SoloAdmin',
        'SoloAdvAction',
        'SoloCabinet',
        'SoloCatalog',
        'SoloCompare',
        'SoloCustomPage',
        'SoloRedis',
        'SoloERP',
        'SoloIdentity',
        'SoloItem',
        'SoloOrder',
        'SoloDelivery',
        'SoloSearch',
        'SoloSocial',
        'SoloSettings',
        'SoloReplication',
        'SoloBank',
        'SoloFacility',
        'SoloNotify',
        'SoloPayment',
        'Google',
        'SoloCpa',
        'AlfaBank',
        'Pel'
    ],
    'module_listener_options' => [
        'config_glob_paths' => [
            'config/autoload/{,*.}{global,local}.php'
        ],
        'module_paths' => [
            './module',
            './vendor'
        ]
    ]
];
