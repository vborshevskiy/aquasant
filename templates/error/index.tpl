<h1>An error occurred</h1>
<h2>{$message}</h2>

{if isset($display_exceptions) && $display_exceptions}
  {if isset($exception) && $exception instanceof Exception}
    <hr/>
    <h2>Additional information</h2>
    {assign var='exceptionClass' value=get_class($exception)}
    <h3>{$exceptionClass}</h3>
    <dl>
      <dt>File:</dt>
      <dd>
        <pre class="prettyprint linenums">{$exception->getFile()}:{$exception->getLine()}</pre>
      </dd>
      <dt>Message:</dt>
      <dd>
        <pre class="prettyprint linenums">{$exception->getMessage()}</pre>
      </dd>
      <dt>Stack trace:</dt>
      <dd>
        <pre class="prettyprint linenums">{$exception->getTraceAsString()}</pre>
      </dd>
    </dl>

    {assign var='e' value=$exception->getPrevious()}
    {if ($e)}
    <hr/>
    <h2>Previous exceptions:</h2>
    <ul class="unstyled">
      {while $e}
        <li>
          {assign var='exceptionClass' value=get_class($e)}
          <h3>{$exceptionClass}</h3>
          <dl>
            <dt>File:</dt>
            <dd>
              <pre class="prettyprint linenums">{$e->getFile()}:{$e->getLine()}</pre>
            </dd>
            <dt>Message:</dt>
            <dd>
              <pre class="prettyprint linenums">{$e->getMessage()}</pre>
            </dd>
            <dt>Stack trace:</dt>
            <dd>
              <pre class="prettyprint linenums">{$e->getTraceAsString()}</pre>
            </dd>
          </dl>
        </li>
        {assign var='e' value=$e->getPrevious()}
      {/while}
    {/if}

  {else}
    <h3>No Exception available</h3>
  {/if}
{/if}
