{* Smarty *}
{strip}

<table>
  <tr>
    <td style="width: 90%;">SAdmin: Energoboom</td>
    <td>
      <a href="/admin/logout/">Выход</a>
    </td>
  </tr>
</table>
<div class="menu">
  {foreach from=$menu item='menuItem'}
      {if $menuItem.selected}
          <span>{if $menuItem.icon}<i class="-icon-{$menuItem.icon}"></i> {/if}{$menuItem.title}</span>
      {else}
          <a href="{$menuItem.url}">{if $menuItem.icon}<i class="-icon-{$menuItem.icon}"></i> {/if}{$menuItem.title}</a>
      {/if}
  {/foreach}
</div>
<div class="content">

{/strip}