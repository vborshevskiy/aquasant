{* Smarty *}
{strip}

<div class="admin-login">
  {if $err}
      <p>Неверный пользователь или пароль</p>
  {/if}
  <form method="post">
    <input type="hidden" name="ret" value="{$smarty.get.ret}" />
    <div>
      <label>Логин: </label>
      <span>
        <input type="text" name="login" value="{if !empty($login)}{$login}{/if}" maxlength="64" required requiredtext="Логин" />
      </span>
    </div>
    <div>
      <label>Пароль: </label>
      <span>
        <input type="password" name="password" value="" maxlength="64" required requiredtext="Пароль" />
      </span>
    </div>
    <div>
      <div>
        <button type="submit" />Вход</button>
      </div>
    </div>
  </form>
</div>

{/strip}