{* Smarty *}
{strip}

<div class="comments-search">
    <form method="post">
        ID товара: <input type="number" value="{if !is_null($goodId)}{$goodId}{/if}" name="good-id">
        <button type="submit">Найти</button>
    </form>
</div>
{if 0 < count($comments)}
    <div class="comments">
        <table border="1">
            <thead>
                <tr>
                    <th>№ комментария</th>
                    <th>№ товара</th>
                    <th>Пользователь</th>
                    <th>Рейтинг</th>
                    <th>Комментарий</th>
                    <th>Достоинства</th>
                    <th>Недостатки</th>
                    <th>Дата</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$comments item='comment'}
                    <tr>
                        <td>{$comment['ID']}</td>
                        <td>{$comment['GoodID']}</td>
                        <td>{$comment['UserName']}</td>
                        <td>{$comment['Rating']}</td>
                        <td>{$comment['Text']}</td>
                        <td>{$comment['Good']}</td>
                        <td>{$comment['Bad']}</td>
                        <td>{$comment['Date']}</td>
                        <td><a href="/admin/comments/delete/?comment-id={$comment['ID']}&good-id={$comment['GoodID']}">Удалить</a></td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    </div>
{/if}

{/strip}