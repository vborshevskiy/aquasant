<div class="filters">
  <div class="add"><a href="#">Добавить организацию</a></div>
  {if (count($agents))}
    <div class="filter" data-name="agent">
      <span class="expander"></span>
      <ul>
        {foreach from=$agents item='agent'}
          <li data-value="{$agent->getId()}" {if $agent@index == 0}class="selected"{/if}>{$agent->getName()}</li>
        {/foreach}
      </ul>
    </div>
  {/if}
</div>

<div class="req">
  <ul></ul>
</div>

{if $banner}
  <div class="banner btm">
    {if $banner['url']}<a href="{$banner['url']}">{/if}
      <img src="{$banner['image']}" alt="">
    {if $banner['url']}</a>{/if}
  </div>
{/if}