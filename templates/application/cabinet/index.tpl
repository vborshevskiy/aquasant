{* Smarty *}

{if $this->auth()->getUser()->isManager()}

  <div class="filters">
    <div class="f-line">
      <div class="search-block filter" data-name="search">
        <div class='form'>
          <input type="text" placeholder="Поиск" id="orders-search">
          <a href="#" class="submit"></a>
        </div>
      </div>
      <div class="date filter" data-name="dates">
        <input type="text" readonly value="{$createdFrom}" id="date-from" class="-date -std"> —
        <input type="text" readonly value="{$createdTo}" id="date-to" class="-date -std">
      </div>
    </div>
    <div class="f-line">
      <div class="filter" data-name="type">
        <span class="expander"></span>
        <ul>
          <li class="selected" data-value="0">Все заказы</li>
          {if (count($reserveStatuses))}
            {foreach from=$reserveStatuses item='reserveStatus'}
              <li {if !$reservesCount[$reserveStatus->Id]}class="null"{/if} data-value="{$reserveStatus->Id}">{$reserveStatus->Name}</li>
            {/foreach}
          {/if}
        </ul>
      </div>
      <div class="filter" data-name="manager-id">
        {if $this->auth()->getUser()->isSuperManager()}
          <span class="expander"></span>
          <ul>
            <li class="selected" data-value="0">Все менеджеры</li>
            {foreach from=$managers item='managerName' key='managerId'}
              <li data-value="{$managerId}">{$managerName}</li>
            {/foreach}
          </ul>
        {/if}
      </div>
    </div>
  </div>

  <ul class="orders-list">
  </ul>
  
  <div class="nothing-found">
    <p>Ничего не найдено.</p>
    
    <p>Поиск работает:<br>
    - по номеру заказа<br>
    - по артикулу и названию товара в заказе<br>
    - по имени и фамилии покупателя<br>
    - по телефону покупателя<br>
    - по электронной почте покупателя<br>
    </p>
  </div>

{else}

  <div class="filters">
    <div class="filter" data-name="agent">
      <span class="expander"></span>
      <ul>
        <li class="selected" data-value="0">Все контрагенты</li>
        {if (count($agents))}
          {foreach from=$agents item='agent'}
            <li data-value="{$agent->getId()}">{$agent->getName()}</li>
          {/foreach}
        {/if}
      </ul>
    </div>
    <div class="filter" data-name="type">
      <span class="expander"></span>
      <ul>
        <li class="selected" data-value="0">Все заказы (<b>{$totalReservesCount}</b>)</li>
        {if (count($reserveStatuses))}
          {foreach from=$reserveStatuses item='reserveStatus'}
            <li {if !$reservesCount[$reserveStatus->Id]}class="null"{/if} data-value="{$reserveStatus->Id}">{$reserveStatus->Name} (<b>{if $reservesCount[$reserveStatus->Id]}{$reservesCount[$reserveStatus->Id]}{else}0{/if}</b>)</li>
          {/foreach}
        {/if}
      </ul>
    </div>
  </div>

  <ul class="orders-list">
  </ul>

{/if}

{if $banner}
  <div class="banner btm">
    {if $banner['url']}<a href="{$banner['url']}">{/if}
      <img src="{$banner['image']}" alt="">
    {if $banner['url']}</a>{/if}
  </div>
{/if}