<div class="filters">
  <div class="filter" data-name="agent">
    <span class="expander"></span>
    <ul>
      <li class="selected" data-value="0">Все контрагенты</li>
      {if (count($agents))}
        {foreach from=$agents item='agent'}
          <li data-value="{$agent->getId()}">{$agent->getName()}</li>
        {/foreach}
      {/if}
    </ul>
  </div>
</div>

<ul class="documents-list"></ul>

{if $banner}
  <div class="banner btm">
    {if $banner['url']}<a href="{$banner['url']}">{/if}
      <img src="{$banner['image']}" alt="">
    {if $banner['url']}</a>{/if}
  </div>
{/if}