<div class="profile-form">
  <div class="field phone">
    <p>Мобильный телефон</p>
    <em>+7</em><input value="{$userPhone}" type="text" class="phone" maxlength="10">
  </div>
  <div class="field">
    <p>Электронная почта</p>
    <input value="{$userEmail}" type="email" class="email">
  </div>
  <div class="buttons">
    <a href="#" class="change-pass">Изменить пароль</a>
    <br>
    <a href="#" class="save">Сохранить</a>
  </div>
</div>

{if $banner}
  <div class="banner btm">
    {if $banner['url']}<a href="{$banner['url']}">{/if}
      <img src="{$banner['image']}" alt="">
    {if $banner['url']}</a>{/if}
  </div>
{/if}