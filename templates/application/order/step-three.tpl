<section{if !$smarty.const.MANAGER_MODE} class="no-header"{/if}>
  
  {if $smarty.const.MANAGER_MODE}
    <ul class="steps step-3"><li></li><li></li><li></li></ul>
    <h1>Телефон</h1>
  {/if}

  <div class="get-phone">
    <h4>Укажите телефон <br class="onlymobile">для отправки уведомлений <br class="onlymobile">о готовности заказа</h4>
    <div class="b-enter">
      <div class="phone">
        +7 <input type="text" maxlength="10" class="num-phone" value="{$this->formatPhone($phone, '{code}{number}')}"> <!-- значение можно не указывать -->
      </div>
			<div class="client-name">
				<input type="text" placeholder="ФИО в свободной форме" value="{if $contactName && !is_numeric($contactName)}{$contactName}{/if}">
				<div class='enter-field'>Введите хотя бы свое имя — мы будем знать, как к вам обращаться.</div>
			</div>
      <div class="remarks">
        <h4>Комментарий к доставке</h4>
        <textarea name="delivery-comment" placeholder="Знаете удобный способ подъезда? Код от домофона?
Пожелание по времени доставки?
— Напишите все, что может ускорить обработку заказа."></textarea>
      </div>
      {if $smarty.const.MANAGER_MODE}
        <div class="remarks-delivery">
          <h4>Комментарий к заказу</h4>
          <textarea name="comment" placeholder="Сопроводительная информация к заказу"></textarea>
        </div>
      {/if}
    </div>
    <div class="b-check">
      <h5></h5>
      <p></p>
      <div class="e-err"></div>
      <div class="check-form">
        <input type="password">
        <span class="actions">
          <a href="#" class="forgot">забыли пароль?</a><br>
          <a href="#" class="cancel">ввести другой телефон</a>
        </span>
      </div>
      <div class="check-done">
        <a href="#">Войти и продолжить</a>
      </div>
    </div>
  </div>
  <div class="next-step">
    <a href='/order/{if $smarty.const.MANAGER_MODE}step-source{else}complete{/if}/'>Готово</a>
  </div>
</section>
<iframe class="snake-preloader" src='/snake/'></iframe>