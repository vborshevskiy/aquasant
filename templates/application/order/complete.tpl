
{if $ordersCount > 1}
  <h1 {if $ordersCount > 1}class="partial"{/if}>Заказ<i>({$currentOrderNumber} из {$ordersCount})</i> успешно оформлен</h1>
{else}
  <h1>Заказ успешно оформлен</h1>
{/if}
{if $paymentTypeId == 7}
  <h1 class="online-pay">Онлайн-оплата прошла успешно</h1>
{/if}

{if (isset($deliveryAddress))}
{assign var='regionId' value=$this->detectRegionId($deliveryAddress)}
<p><strong>Заказ №{$orderNumber}</strong> будет доставлен{if 0 < $regionId} <strong>{$deliveryDateText}</strong>{/if} по адресу:<br>
  <strong>{$deliveryAddress}</strong>
  {if $isUndefinedDelivery}
    <br />
    <strong>Стоимость доставки оператор сообщит сегодня</strong>
  {/if}
</p>
{else}
  <p><strong>Заказ №{$orderNumber}</strong><br> можно забрать по адресу:
    МКАД, 14-й километр (внутренняя сторона), дом 10, СК «Восточные ворота», вход 9а.
    <br />
    Выдача производится с 09:00 до 20:00 без выходных.
    <br />
    Въезд и вход осуществляется через КПП, прихватите с собой документ.
  </p>
{/if}

{if $paymentTypeId == 7}
  <p>Оплата <strong>онлайн.</strong> Статус: <strong>Оплачено.</strong><br>
    В комплекте будет кассовый чек на сумму <strong>{$amount|format_price} р.</strong>, включая НДС.</p>
{/if}

{if $paymentTypeId == 3}
  <p>
  {if $payerPerson == 'legalPerson'}
    Оплата <strong>по безналичному расчету</strong>, сумма <strong>{$amount|format_price} р.</strong>, включая НДС.
  {else}
    Оплата <strong>наличными при получении.</strong>
    <br>
    В комплекте будет кассовый чек на сумму <strong>{$amount|format_price} р.</strong>, включая НДС.
  {/if}
  </p>
{/if}

{if $payerPerson == 'legalPerson'}
  <p><a href='/cabinet/download/?reserveId={$orderNumber}&formId=11855&agentId={$agentId}' class="download-button">Скачать счет</a></p>
  <p><a href='/cabinet/download/?reserveId={$orderNumber}&formId=79228&agentId={$agentId}' class="download-button">Скачать доверенность на получение</a></p>
{/if}

{if (!isset($deliveryAddress))}
  <div id="map" data-center='[55.650643,37.830976]' data-zoom='15' data-balloon='База сантехники «Аквасант»<br>МКАД, 14-й километр, дом 10<br>Каждый день с 09:00 до 20:00'></div>
{/if}

{if $currentOrderNumber < $ordersCount}
  <div class="next-order">
    <a href="/order/step-one/">Оформить следующий заказ</a>
  </div>
{/if}