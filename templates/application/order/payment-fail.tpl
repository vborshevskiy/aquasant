<section class='transaction-error'>
  <h1>Упс, оплата не прошла</h1>
  <p class="transaction-error">Попробуйте <a href="/order/payment-repeat/?reserveId={$reserveId}">повторить транзакцию</a> или <a href="/order/change-online-payment/?reserveId={$reserveId}">оплатить наличными</a> </p>
</section>