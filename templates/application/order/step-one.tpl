<section>
  <ul class="steps step-1"><li></li><li></li><li></li></ul>

  <h1>Способ получения заказа</h1>
</section>
<div class="b-tabs">
  <section>
    <ul data-block="delivery-win">
      {if $smarty.const.MANAGER_MODE}
        {assign var='obtainMethod' value='ownStorePickup'}
      {/if}
      <li {if $obtainMethod == 'delivery'}class="active"{/if} data-for="s-delivery">Доставка</li>
      <li {if $obtainMethod == 'ownStorePickup'}class="active"{/if} data-for="s-pickup">Самовывоз <small>0 <span class='rub'>р</span></small></li>
    </ul>
  </section>
</div>

<section id="s-delivery" class="tab-block delivery-win" data-defaultcity="{if $userRegion}{$userRegion->getName()}{/if}">
  <div class="addresses-list">
    <h4>Куда?</h4>
    <ul>
      {if !$smarty.const.MANAGER_MODE && (count($deliveryAddresses))}
        {foreach from=$deliveryAddresses item="deliveryAddress"}
          <li data-id="{$deliveryAddress->getId()}" {if $deliveryAddress->getElevatorTypeId() || $deliveryAddress->getFloor()} data-elevator="{if $deliveryAddress->getElevatorTypeId()}{$deliveryAddress->getElevatorTypeId()}{else}0{/if}"{/if} {if $deliveryAddress->getFloor()} data-stage="{$deliveryAddress->getFloor()}"{/if} {if $deliveryAddressId == $deliveryAddress->getId()}class="selected"{/if} data-coords="{$deliveryAddress->getLatitude()},{$deliveryAddress->getLongitude()}">
						{$deliveryAddress->getAddress()}{if $deliveryAddress->getFloor()}, {$deliveryAddress->getFloor()}-й этаж{/if}
					</li>
        {/foreach}
      {/if}
      {if !empty($newDeliveryAddress)}
        <li data-id="{$newDeliveryAddress}" {if !$deliveryAddressId}class="selected"{/if}>{$newDeliveryAddress}</li>
      {/if}
    </ul>
    <br>
    <a href='#' class='add-address'>добавить новый адрес...</a>
  </div>
  <div class="map"></div>
  <div class="details">
    <div class='no-delivery'>
      <p>Стоимость доставки определится после ввода адреса</p>
    </div>
    <div class="delivery-set delivery-no-region">
      <div class="checkbox {if $liftToTheFloor}checked{/if}">
        нужен подъем на <input type="text" maxlength="2" class="number" value="{$floor}"> этаж
      </div>
      <div class="elevator" {if $liftToTheFloor}style="display: block"{/if}>
        <span>лифт</span>
        <ul>
        {foreach from=$elevatorTypes item='elevator'}
          <li {if $elevatorTypeId == $elevator['ElevatorTypeID']}class="selected"{/if} data-value="{$elevator['ElevatorTypeID']}" data-baseprice='0' data-perstage='{$liftingPrices[$elevator['ElevatorTypeID']]}'>{$elevator['ElevatorType']}</li>
        {/foreach}
      </div>
    </div>
    <div class="when delivery-no-region">
      <h4>Когда?</h4>
      <ul class="dates"></ul>
    </div>

    <div class="order-done delivery-no-region">
      <h5>Стоимость доставки <strong>490</strong> <span class="rub">р</span></h5>
      <h6>на 9 февраля, c подъемом на 8 этаж</h6>
      <div class="next-step">
        <a href='/order/step-two'>Продолжить</a>
      </div>
    </div>

    <div class="order-done delivery-region">
      <p>Точную стоимость доставки по вашему адресу<br>
       сообщит оператор после создания заказа.</p>
      <div class="next-step">
        <a href='/order/step-two'>Продолжить</a>
      </div>
    </div>
  </div>
</section>

<section id="s-pickup" class="tab-block delivery-win">

  <h4>Адрес базы:</h4>
  <p>МКАД, 14-й километр (внутренняя сторона), дом 10, СК «Восточные ворота», вход 9а</p>
  <p>Выдача производится с 09:00 до 20:00 без выходных.</p>
  <p>Въезд и вход осуществляется через КПП, прихватите с собой документ.</p>

  {if !$smarty.const.MANAGER_MODE}
    <div class="m-map" id="m-map"></div>
  {/if}

  <div class="get-phone">
    <div class="b-enter">
      <h4>Укажите телефон для отправки уведомлений о готовности заказа</h4>
      <div class="phone">
        +7 <input type="text" maxlength="10" class="num-phone" value="{if !$smarty.const.MANAGER_MODE}{$this->formatPhone($phone, '{code}{number}')}{/if}"> <!-- значение можно не указывать -->
      </div>
    </div>
    {if $smarty.const.MANAGER_MODE}
      <div class="username">
        <input type="text" class="user-name" value="" placeholder="ФИО клиента" autocomplete="off">
      </div>
    {/if}
    <div class="b-check">
      <h5></h5>
      <p></p>
      <div class="e-err"></div>
      <div class="check-form">
        <input type="password">
        <span class="actions">
          <a href="#" class="forgot">забыли пароль?</a><br>
          <a href="#" class="cancel">ввести другой телефон</a>
        </span>
      </div>
      <div class="check-done">
        <a href="#">Войти и продолжить</a>
      </div>
    </div>
  </div>
  <div class="next-step">
    <a href='/order/step-two/'>Продолжить</a>
  </div>
</section>

<div id="get-flat-number">
    <div class='bg'>
        <p>№ квартиры не забыли?</p>
        <div class='buttons'>
            <a href='#' class='y'>Ой, да</a>
            <a href='#' class='n'>Нет, частный дом</a>
        </div>
    </div>
</div>