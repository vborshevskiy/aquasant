<section>
  <ul class="steps step-2"><li></li><li></li><li></li></ul>

  <h1>способ оплаты</h1>
</section>
<div class="b-tabs">
  <section>
    <ul data-block="payer-win">
      <li {if $payerPerson == 'naturalPerson'}class="active"{/if} data-for="s-physic">Физическое лицо</li>
      <li {if $payerPerson == 'legalPerson'}class="active"{/if}data-for="s-firm">Организация</li>
    </ul>
  </section>
</div>

<section id="s-physic" class="tab-block payer-win">
  <ul class='-x-selector-list payment-type-list' data-name="payment_type_id">
    <li {if $order->isCashPayment()}class="selected"{/if} data-value='3'>Оплата наличными{if $obtainMethod == 'ownStorePickup'} / картой{/if} при получении</li>
    <li {if $order->isAlfabankPayment()}class="selected"{/if} data-value='7'>{if $obtainMethod == 'ownStorePickup'}Онлайн оплата{if $order->getBankPercent()} (+{$order->getBankPercent()}%){/if}, не нужно стоять в кассу{else}Онлайн оплата по карте {if $order->getBankPercent()}(+{$order->getBankPercent()}%){/if}{/if}</li>
  </ul>

  <div class="next-step">
    <a href='#'>Продолжить</a>
  </div>

</section>

<section id="s-firm" class="tab-block payer-win">
  <ul class='-x-selector-list' data-name="agent_id">
    {foreach from=$agents item="agent"}
      <li data-value="{$agent->getId()}" {if $agentId == $agent->getId()}class="selected"{/if}>{$agent->getName()}</li>
    {/foreach}
    {if $newAgent}<li data-value="{$newAgent->getName()}|{$newAgent->requisites()->getInn()}|{$newAgent->requisites()->getKpp()}|{$newAgent->requisites()->getAddress()}" {if empty($agents) || !$agentId}class="selected"{/if}>{$newAgent->getName()}</li>{/if}
    <li class='service add-new'>+ добавить организацию</li>
  </ul>
	<div class="email-enter -email">
		<p>Отправить счет на почту:</p>
		<input type="text" class="email" value="{$email}">
	</div>
	{if true || 'ownStorePickup' == $obtainMethod}
		<div class="name-enter -name">
			<p>ФИО:</p>
			<input type="text" class="name" value="{if $agentFio && !is_numeric($agentFio)}{$agentFio}{/if}">
		</div>
	{/if}
  <div class="next-step">
    <a href='#'>Продолжить</a>
  </div>
</section>

<iframe class="snake-preloader" src='/snake/'></iframe>