<div class="st-wrap">
  <h1><span>оптово-розничная<br /> база сантехники<br /> аквасант</span></h1>
  <div class="header-image" style="background-image: url('/images/baza-header.jpg');"></div>
  <div class="contents">
    <p style="padding: 10px 0">
      Бизнес, начавшийся в&nbsp;2014&nbsp;году, был изначально ориентирован на&nbsp;оптовую торговлю сантехническим оборудованием. Мы&nbsp;заключили прямые контракты с&nbsp;производителями, наладили логистику и&nbsp;торговля пошла. Поработав пару лет &laquo;на&nbsp;опте&raquo;, мы, вдохновившись поступком Ингвара Кампрада (основателя IKEA), приоткрыли двери нашего склада и&nbsp;для розничных покупателей.
    </p>
    <p style="padding: 10px 0">
      С&nbsp;2016 года на&nbsp;нашей оптовой базе закупаются не&nbsp;только магазины, загружая сантехнику фурами, но&nbsp;и&nbsp;по&nbsp;одной штуке покупают обычные люди, выбирающие себе, скажем, новый смеситель в&nbsp;ванную комнату (по&nbsp;цене, кстати, не&nbsp;сильно отличающейся от&nbsp;оптовой).
    </p>
    <p style="padding: 10px 0">
      В&nbsp;недалеком будущем мы&nbsp;расширим ассортимент около сантехнической и&nbsp;строительной тематиками, предложив и&nbsp;оптовым, и розничным покупателям выгодные условия с&nbsp;безукоризненным сервисом.
    </p>
    <h2>склад аквасант</h2>
  </div>
</div>

<section class="gallery wide">
  <div class="viewport">
    <img src="/images/galleries/sklad1.jpg" alt="">
    <img src="/images/galleries/sklad2.jpg" alt="">
    <img src="/images/galleries/sklad3.jpg" alt="">
    <img src="/images/galleries/sklad4.jpg" alt="">
    <img src="/images/galleries/sklad5.jpg" alt="">
  </div>
  <div class="board">
    <a href="#" class="scrl left"></a>
    <a href="#" class="scrl right"></a>
  </div>
</section>

<div class="st-wrap">
  <div class="contents">
    <ul class="c-list">
      <li>Отгрузка со&nbsp;склада на&nbsp;следующий день</li>
      <li>Принимаем наличные, безналичные и&nbsp;е-бабки</li>
      <li>Самовывоз с&nbsp;оптово-розничного склада</li>
      <li><a href="/vanni/">1580 ванн в&nbsp;наличии</a> на&nbsp;складе</li>
      <li>Отгружаем в&nbsp;регионы по&nbsp;всей стране</li>
      <li>База: 985 брендов со&nbsp;всего мира</li>
      <li><a href="/contacts/">Удобный подъезд</a> на&nbsp;базу со&nbsp;МКАДа</li>
    </ul>

    <h2>торговый зал аквасант</h2>
  </div>
</div>

<section class="gallery wide">
  <div class="viewport">
    <img src="/images/galleries/zal1.jpg" alt="">
    <img src="/images/galleries/zal2.jpg" alt="">
    <img src="/images/galleries/zal3.jpg" alt="">
    <img src="/images/galleries/zal4.jpg" alt="">
    <img src="/images/galleries/zal5.jpg" alt="">
  </div>
  <div class="board">
    <a href="#" class="scrl left"></a>
    <a href="#" class="scrl right"></a>
  </div>
</section>


<div class="st-wrap">
  <div class="contents">
    <ul class="c-list">
      <li><a href="/dushevie_kabini">5434 душевые кабины</a> в&nbsp;наличии</li>
      <li><a href="/smesiteli_dlya_kuhni/">3025 смесителей</a> по&nbsp;оптовым ценам</li>
      <li>Выставочный павильон&nbsp;&mdash; 1500&nbsp;м<sup>2</sup></li>
      <li>Установка сантехники специалистами</li>
      <li><a href="/komplekti_mebeli_dlya_vannoy/">Мебель для ванной</a>: 10&nbsp;000+ вариантов</li>
      <li>30&nbsp;честных дней на&nbsp;возврат</li>
      <li>Собственный сервисный центр</li>
      <li>Продаем &laquo;физикам&raquo; в&nbsp;кредит</li>
    </ul>
  </div>
</div>

<div class="st-wrap">
        <div class="contents req">
        <h2>реквизиты аквасант</h2>
            <p>ООО «АКВАСАНТ»</p>

            <p>Юридический адрес<br>
               109429 г. Москва,  МКАД 14-й километр,  д.10, этаж 4, пом IX, ком 45</p>

            <p>Фактический адрес (адрес для доставки)<br>
               109429 г. Москва,  МКАД 14-й километр,  д.10, этаж 4, пом IX, ком 45</p>

            <p>Адрес для корреспонденции<br>
               109144, г. Москва а/я 10, ООО «Аквасант»</p>

            <p>ИНН 7723389141<br>
               КПП 772301001<br>
               ОГРН 1157746385720</p>

            <p>Банк АО "АЛЬФА-БАНК" г. Москва<br>
               БИК 044525593<br>
               Расчетный счет 40702810102200007722<br>
               Кор. счет 30101810200000000593</p>

            <p>Генеральный директор<br>
               Ромашкин Александр Николаевич</p>

            <p>Телефон +7 (495) 215-16-95</p>

            <br />
    <br />
    <p>Оптово-розничная база сантехники &laquo;Аквасант&raquo;&nbsp;&mdash; вся сантехника в&nbsp;одном месте</p>
        </div>
    </div>