<h1>Аквасант: <br> доставка сантехники</h1>
<p>Да, мы с большой радостью доставляем заказы по всей России.</p>

<p>Доставка сантехники По Москве, Московской области и в Санкт-Петербург осуществляется собственной логистической группой.<br />
Стоимость доставки вычисляется на первом шаге оформления заказа, сразу, как только вы введете адрес.<br />
И эта стоимость не меняется — никаких скрытых платежей и «сюрпризов», как это принято в отрасли, не будет — железобетон.</p>

<p>А вот стоимость доставки в регионы России определяется после создания заказа.<br />
С вами свяжется представитель логистического департамента, обсудит детали и нюансы доставки в регион, и назовет итоговую сумму за транспортировку.</p>

<p>Для желающих сначала пощупать вещь перед покупкой работает выставочный зал.<p>
<p>
  Для желающих сначала пощупать перед покупкой вещь руками работает <a href="/about/">выставочный зал</a>.
</p>