 <h1>
        Аквасант:<br>
        оплата заказов
    </h1>
    <p>Исторически мы работали только с юридическими лицами, поэтому принимали только безнал. Сейчас же, когда мы отгрохали большой выставочный зал и допустили к ассортименту розничных покупателей, наравне с безналом мы принимаем к оплате наличные и пластиковые карты.</p>
    <p><img class="m-img" src="/images/cards.png"></p>
    <p class="small">Услуга оплаты через интернет осуществляется в соответствии с правилами международных платежных систем Visa, MasterCard и Платежная система «Мир» на принципах соблюдения конфиденциальности и безопасности совершения платежа, для чего используются самые современные методы проверки, шифрования и передачи данных по закрытым каналам связи. Ввод данных банковской карты осуществляется на защищенной платежной странице АО «АЛЬФА-БАНК».</p>
    <p>Хотите оплатить заказ курьеру — нет проблем, готовьте наличные, но не слишком много, заказы дороже 120 тысяч отгружаются по предоплате.</p>
    <p>А ещё физическим лицам может быть выдан кредит банками-партнерами.<br>
       Для этого надо заявиться на нашу сантехническую базу лично, прихватив с собой паспорт.</p>