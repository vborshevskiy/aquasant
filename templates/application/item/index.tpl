<div itemscope itemtype="http://schema.org/Product">
  {include file="./partial/breadcrumbs.tpl"}

  {include file="./partial/buy-block.tpl"}

  <div class="section-header" id="characteristics">
    <div class="-wrap">
      <h2>Характеристики товара</h2>
    </div>
  </div>

  <section class="characteristics" data-menu="Характеристики">
    {assign var='hasHiddenProperty' value=false}
    {if count($properties) > 0}
      <ul>
        {foreach from=$properties item='property'}
          {assign var='showProperty' value=false}
          {if $property->displayInMiddleDescription}
            {assign var='showProperty' value=true}
          {/if}
          {if !$showProperty}
            {assign var='hasHiddenProperty' value=true}
          {/if}
          <li class="{if !$showProperty} hidden{/if}{if $showProperty} printable{/if}">
            <span>
              {$property->name}
              {if !empty($property->hint)}
                <a href="#" class="-info">
                      <span class="content">
                        {$property->hint}
                        {if !empty($property->hintImage)}<img src="{$absHost}/img/pi/{$property->hintImage}">{/if}
                       </span>
                    </a>
              {/if}
            </span>
            {if array_key_exists($property->propId, $selectableProperties)}
              <div>
                <div class="-x-selector" data-name="sizes"><div class="expander"></div>
                  <ul>
                    <li class="selected" data-value="{$property->valueId}">{$property->value}</li>
                    {assign var='propertyValues' value=$selectableProperties[$property->propId]}
                    {foreach from=$propertyValues item='propertyValue' key="valueId"}
                      <li data-value="{$valueId}">
                        <a style="color: #fff; text-decoration: none" href="{$propertyValue['url']}">{$propertyValue['value']}</a>
                      </li>
                    {/foreach}
                  </ul>
                </div>
              </div>
            {else}
              <div>{$property->value}</div>
            {/if}
          </li>
        {/foreach}
        {if (count($goodNotes))}
          {foreach from=$goodNotes item='goodNote'}
            <li class="marked"><div>{$goodNote->Text}</div></li>
          {/foreach}
        {/if}
      </ul>

      {if $hasHiddenProperty}
        <br>
        <a href="#" class="show-all">Показать все характеристики</a>
      {/if}
    {/if}

    {if $centerBanner}
      <div class="banner">
        {if $centerBanner['url']}<a href="{$centerBanner['url']}">{/if}
          <img src="{$centerBanner['image']}">
        {if $centerBanner['url']}</a>{/if}
      </div>
    {/if}
  </section>

  {if $item->GoodDescription}
    <div class="section-header" id="description">
      <div class="-wrap">
        <h2>Описание<br class="onlymobile"> {if $item->GenitiveDetailName}{$item->GenitiveDetailName}{else}{$item->GoodName}{/if}</h2>
      </div>
    </div>

    <section class="item-description {if empty($images)}no-photo{/if}" data-menu="Описание" itemprop="description">
      {$shortDescription}
      {if $description}
        <div class="hidden">{$description}</div>
        <div class="read-more">
        <a href="#" class="show-all">Читать целиком</a>
        </div>
      {/if}
    </section>
  {/if}

  {if $item->VideoUrl && $item->VideoName}
    <div class="section-header" id="video">
      <div class="-wrap">
        <h2>Видео<br class="onlymobile"> {if $item->GenitiveDetailName}{$item->GenitiveDetailName}{else}{$item->GoodName}{/if}</h2>
      </div>
    </div>

    <section class="video" data-menu="Видео">
      <iframe src="{$item->VideoUrl}" frameborder="0" allowfullscreen></iframe>
      <h3>{$item->VideoName}</h3>
      <div class="length"></div>
    </section>
  {/if}

  {if count($files)}
    <div class="section-header" id="downloads">
      <div class="-wrap">
        <h2>Файлы<br class="onlymobile"> {if $item->GenitiveDetailName}{$item->GenitiveDetailName}{else}{$item->GoodName}{/if}</h2>
      </div>
    </div>

    <section class="downloads" data-menu="Файлы">
      <ul>
        {foreach from=$files item='file'}
          <li><span>{$file->FileExtension}</span>
            <strong>{$file->Name}.{$file->FileExtension}</strong>
            <em>{$file->FileSize|format_bytes}</em>
            <a href="/download/?file={$file->FileName}">скачать</a>
          </li>
        {/foreach}
      </ul>
    </section>
  {/if}

  {if 0 < count($satelliteGroups)}
    <div class="section-header" id="accessories">
      <div class="-wrap">
        <h2>Товары, 100% совместимые с {if $item->AblativeDetailName}{$item->AblativeDetailName}{else}{$item->GoodName}{/if}</h2>
      </div>
    </div>

    <section class="accessories" data-menu="Аксессуары">
      <ul class="short-goods">
        {foreach from=$satelliteGroups item='satelliteGroup'}
          {foreach from=$satelliteGroup.goods item='satelliteGood'}
            {assign var='satelliteImages' value=$satelliteGoodsImages[$satelliteGood.GoodID]}
            <li>
              <a href="/{$satelliteGood['CategoryUID']}/{$satelliteGood['GoodID']}_{$satelliteGood['GoodEngName']}/">
                <div class="image">
                  {foreach from=$satelliteImages item="image" name='satelliteImages'}
                    {if $smarty.foreach.satelliteImages.first}
                      <img src="{$absHost}/img/260x280/{$image['MiniatureUrl']}" />
                    {/if}
                  {foreachelse}
                    <img src="/i/nophoto.png" />
                  {/foreach}
                </div>
                <span>
                  {$satelliteGood.GoodName|escape}
                </span>
              </a>
              <div class="time-to-pick">
                {if ($satelliteGood['AvailQuantity'] > 0)}
                  {if $isDefaultCity}
                    можно забрать сейчас
                  {else}
                    отгрузим сегодня
                  {/if}
                {else}
                  {if true}
                    отгрузим {$dateHelper->getOutcomeShortDateText($satelliteGood['ArrivalDate'])}
                  {else}
                    срок поставки {$dateHelper->getDeliveryDateRange($satelliteGood['ArrivalDate'])}
                  {/if}
                {/if}
              </div>
              <div class="price">
                <strong>{$satelliteGood.Value|format_price} <span class="rub">р</span></strong>
                <a href="#" class="add-to-basket" data-id="{$satelliteGood.GoodID}"></a>
              </div>
            </li>
          {/foreach}
        {/foreach}
      </ul>
      {if count($satelliteGroup.goods) > 5}
        <div class="show-more">
          <a href="#">Еще совместимые товары</a>
        </div>
      {/if}
    </section>
  {/if}

  {if count($packages)}
    <div class="section-header large" id="analogs">
      <div class="-wrap">
        <h2>Подходит к комплектам</h2>
      </div>
    </div>

    <section class="analogs" data-menu="Другие варианты">
      <ul class="goods">
        {foreach from=$packages item='package'}
          <li>
            <a href="/{$package['CategoryUID']}/{$package['GoodID']}_{$package['GoodEngName']}/">
              <div class="images">
                {assign var="packageImages" value=$packagesImages[$package['GoodID']]}
                {assign var='packageImage' value=null}
                {if 0 < count($packageImages)}
                  {assign var='packageImage' value=reset($packageImages)}
                {/if}
                <div class="image" style="background-image: {if $packageImage}url('{$absHost}/img/260x280/{$packageImage['MiniatureUrl']}'){else}url('/i/nophoto.png');{/if}"></div>
              </div>
            </a>
            <a class="title" href="/{$package['CategoryUID']}/{$package['GoodID']}_{$package['GoodEngName']}/">{$package['GoodName']}</a>
            {*<div class="sizes">габариты: 48,5x86x45 </div>*}
            {if ($package['AvailQuantity'] > 0)}
              <div class="delivery now">
                {if $isDefaultCity}
                  можно забрать сейчас
                {else}
                  отгрузим сегодня
                {/if}
              </div>
            {else}
              <div class="delivery later">
                {if true}
                  отгрузим {$dateHelper->getOutcomeShortDateText($package['ArrivalDate'])}
                {else}
                  срок поставки {$dateHelper->getDeliveryDateRange($package['ArrivalDate'])}
                {/if}
              </div>
            {/if}
            <div class="price {if $package['PrevValue'] > 0}discounted{/if}">
              <strong>{$package['Value']|format_price} <span class="rub">р</span></strong>
              {if $package['PrevValue'] > 0}
                <strong class="old">{$package['PrevValue']|format_price} <span class="rub">р</span></strong>
              {/if}
              <a href="#" class="add-to-basket" data-id="{$package['GoodID']}"></a>
            </div>
          </li>
        {/foreach}
      </ul>
    </section>
  {/if}

  {if (count($similarGoods) > 0)}
    <div class="section-header" id="similar">
      <div class="-wrap">
        <h2>Похожие {$category->CategoryName|lower}</h2>
      </div>
    </div>


    <section class="similar-goods" data-menu="Похожие товары">
      <ul class="goods">
        {foreach from=$similarGoods item='good' name='goods'}
          {include file="../catalog/partial/item.tpl" goodsImages=$similarGoodsImages showRoom=$similarGoodsRemainsInTheMainStore[$good['GoodID']] > 0 action="SimilarProduct" position=$smarty.foreach.goods.iteration}
        {/foreach}
      </ul>

      {if count($similarGoods) > 5}
        <div class="show-more">
          <a href="#">Еще похожие {$itemCategory->CategoryName|lower}</a>
        </div>
      {/if}
    </section>
  {/if}

  {if (count($notSimilarGoods) > 0)}
    <div class="section-header" id="dissimilar">
      <div class="-wrap with-note">
        <h2>Непохожие {$category->CategoryName|lower}</h2>
        <div class="remarks">Как знать, возможно вас заинтересуют совсем другие {$itemCategory->CategoryName|lower}, на нашей базе их много</div>
      </div>
    </div>


    <section class="dissimilar-goods" data-menu="Непохожие товары">
      <ul class="goods">
        {foreach from=$notSimilarGoods item='good' name='goods'}
          {include file="../catalog/partial/item.tpl"
						goodsImages=$notSimilarGoodImages
						showRoom=$notSimilarGoodsRemainsInTheMainStore[$good['GoodID']] > 0
						action="UnlikeGoods"
						position=$smarty.foreach.goods.iteration}
        {/foreach}
      </ul>

      {if count($notSimilarGoods) > 5}
        <div class="show-more">
          <a href="#">Еще непохожие {$itemCategory->CategoryName|lower}</a>
        </div>
      {/if}
    </section>
  {/if}

  {include file="./partial/warranty.tpl"}

  <div id="top-panel">
    <div class="article"><span class="onlymobile">артикул {$item->GoodID}</span><span class="nomobile"><a href="#" class="scroll-top">{$item->GoodName}</a></span></div>
    <div class="price">
      <strong>
        {$price->value|format_price} <span class='rub'>р</span>
      </strong>
      <a href="#" class="add-to-basket" data-id="{$item->GoodID}"{if $smarty.const.MANAGER_MODE && $onlyInShowRoom} data-only-showroom="1"{/if}{if $smarty.const.MANAGER_MODE && $inShowRoom && $isDefaultCity} data-showroom="1"{/if}{if $smarty.const.MANAGER_MODE && $availability && (0 < $availability.SuborderQuantity) && (0 == ($availability.AvailQuantity - $availability.WindowQuantity))} data-has-suborder="1"{/if}><span class="nomobile">Купить</span></a>
    </div>
</div>
</div>
