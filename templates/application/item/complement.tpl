{include file="./partial/breadcrumbs.tpl"}

{include file="./partial/buy-block.tpl" nocompare=true complect=true}



<div class="section-header large" id="complectation" data-id="4738">
  <div class="-wrap">
    <h2>Базовая комплектация
      <small class="nomobile">
        {if $item->GenitiveDetailName}
          {$item->GenitiveDetailName}
        {else}
          {$item->GoodName}
        {/if}
      </small></h2>
    <span class="selected-info" style="display: none;">
            <em>{$selectedComponentsCount}</em> из {$componentsCount} выбрано
        </span>
  </div>
</div>

<section class="complectation" data-menu="Комплектация">

  {foreach from=$packageGroups item='packageGroup'}
    {if false && 'or' == $packageGroup->getSelectMode()}

      {foreach from=$packageGroup->getItems() item='component'}
        {assign var="componentPrice" value=$componentPrices[$component->getId()]['Value']}
        {assign var="oldComponentPrice" value=$componentPrices[$component->getId()]['PrevValue']}
        <div class="c-part single complect-part" data-id="{$component->getId()}" data-price="{$componentPrice|intval}">
          <div class="block-wrap">
            {assign var="componentImage" value=$componentImages[$component->getId()]}
            <div class="image">
              <a href="{$component->getUrl()}" target="_blank">
                <img src="{if $componentImage}/img/260x280/{$componentImage['MiniatureUrl']}{else}/i/nophoto.png{/if}">
              </a>
            </div>
            <div class="title" data-href="{$component->getUrl()}">
              <h3>
                <a href="{$component->getUrl()}" target="_blank">
                  {$component->getName()}
                </a>
              </h3>
              <div class="price">
                {if ($component->getAvailQuantity() > 0)}
                  <span class="date">
                    {if $isDefaultCity}
                      можно забрать сейчас
                    {else}
                      отгрузим сегодня
                    {/if}
                  </span>
                {elseif $component->getArrivalDate()}
                  <span class="date{if !$dateHelper->isToday($component->getArrivalDate())} later{/if}">
                    {if $dateHelper->isToday($component->getArrivalDate())}
                      отгрузим {$dateHelper->getOutcomeShortDateText($component->getArrivalDate())}
                    {else}
                      срок поставки {$dateHelper->getDeliveryDateRange($component->getArrivalDate())}
                    {/if}
                  </span>
                {/if}
                <span class="digs">
                  <div>
                    <strong>{$componentPrice|format_price} <span class="rub">р</span></strong>
                    {if $oldComponentPrice > 0}
                      <strong>{$oldComponentPrice|format_price}  <span class="rub">р</span></strong>
                    {/if}
                  </div>
                  <div class="-x-checkbox checked disabled"></div>
                </span>
              </div>
						</div>
          </div>
        </div>
      {/foreach}

    {else}

      {if 0 < count($packageGroup->getItems())}
        <div class="c-part scrollable {if 'or' == $packageGroup->getSelectMode()}single{elseif 'and' == $packageGroup->getSelectMode()}multiple{/if}-selector{if ('or' == $packageGroup->getSelectMode()) && !$packageGroup->isRequired()} c-optional{/if}">
          <div class="viewport">
            <ul>
              {foreach from=$packageGroup->getItems() item='component'}
                {assign var="componentPrice" value=$componentPrices[$component->getId()]['Value']}
                {assign var="oldComponentPrice" value=$componentPrices[$component->getId()]['PrevValue']}
                <li class="complect-part" data-id="{$component->getId()}" data-price="{$componentPrice|intval}">
                  {assign var="componentImage" value=$componentImages[$component->getId()]}
                  <div class="image">
                    <a href="{$component->getUrl()}" target="_blank">
                      <img src='{if $componentImage}{$absHost}/img/260x280/{$componentImage['MiniatureUrl']}{else}/i/nophoto.png{/if}'>
                    </a>
                  </div>
                  <a href="{$component->getUrl()}" class="title">
                    {$component->getName()}
                  </a>
                  <div class="price">
										{assign var='pickupDate' value=$component->getPickupDate()}
                    {if $pickupDate && ($dateHelper->isToday($pickupDate) || $dateHelper->isTomorrow($pickupDate)) && 0 < ($component->getAvailQuantity() - $component->getWindowQuantity())}
                    
                    {*if $pickupDate && ($dateHelper->isToday($pickupDate) || $dateHelper->isTomorrow($pickupDate)) && 0 < ($component->getAvailQuantity() - $component->getWindowQuantity())*}
                      
                        {*if $isDefaultCity}
                        {if $dateHelper->isToday($pickupDate)}
                            можно забрать сегодня
                            
                        {else}
                            можно забрать  {$dateHelper->getOutcomeShortDateText($pickupDate)}
                        {/if}{*$dateHelper->getPickupDateDescription($pickupDate)*}
                        {*else}
                          отгрузим сегодня
                        {/if*}
                        {if $isDefaultCity}
                          {if $dateHelper->isToday($pickupDate)}
                             <span class="date">
                             можно забрать сегодня
                             </span>
                          {else}
                          <span class="date later">
                            отгрузим {$dateHelper->getOutcomeShortDateText($pickupDate)}
                          </span>
                          {/if}{*$dateHelper->getPickupDateDescription($pickupDate)*}
                        {else}
                          {if $dateHelper->isToday($pickupDate)}
                          отгрузим сегодня
                          {else}
                          отгрузим {$dateHelper->getOutcomeShortDateText($pickupDate)}
                          {/if}
                        {/if}
                      
                    {elseif $component->getArrivalDate()}
                      {*{if true}*}
                          {*if $dateHelper->isToday($pickupDate)}
                            можно забрать сегодня
                            
                          {else}
                            отгрузим {$dateHelper->getOutcomeShortDateText($component->getArrivalDate())}
                          {/if*}
                          
                        {if $isDefaultCity}
                          {if $dateHelper->isToday($pickupDate)}
                          <span class="date">            
                             можно забрать сегодня
                             </span>
                          {else}
                          <span class="date later">
                            отгрузим {$dateHelper->getOutcomeShortDateText($pickupDate)}
                          </span>
                          {/if}{*$dateHelper->getPickupDateDescription($pickupDate)*}
                        {else}
                          {if $dateHelper->isToday($pickupDate)}
                          <span class="date">
                          отгрузим сегодня
                          </span>
                          {else}
                          <span class="date later">
                          отгрузим {$dateHelper->getOutcomeShortDateText($pickupDate)}
                          </span>
                          {/if}
                        {/if}
                          
                       {* {else} 
                          срок поставки {$dateHelper->getDeliveryDateRange($component->getArrivalDate())}
                        {/if} *}
                    {/if}

                    <span class="digs">
                      <div{if $oldComponentPrice > $componentPrice} class="discounted"{/if}>
                        {if $oldComponentPrice > $componentPrice}
                          <strong>{$oldComponentPrice|format_price} <span class="rub">р</span></strong>
                        {/if}
                        <strong>{$componentPrice|format_price} <span class="rub">р</span></strong>
                      </div>
                      <div class="-x-checkbox {if $component->isSelected()}checked{/if}"></div>
                    </span>
                  </div>
									{if 0 < $component->getWindowQuantity()}
										{assign var='place' value=$component->getPlace()}
										<div class="show-room" {if $place}data-placeinfo="<p>ряд:<b>{$place.row}</b></p><p>место:<b>{$place.cell}</b></p>"{/if}>{if $isWorkTime}сегодня {/if}можно посмотреть в торговом зале</div>
									{/if}
                </li>
              {/foreach}
            </ul>
          </div>
          <div class="board">
            <span class='left'><b></b></span>
            <span class='right'><b></b></span>
            {assign var='componentCount' value=$packageGroup->getItems()|count}
            <span class='right more'>еще {$componentCount-1}</span>
          </div>
        </div>
      {/if}

    {/if}
  {/foreach}






  {if false}
    {foreach from=$components key="groupId" item='groupComponents'}
      {assign var="group" value=$groups[$groupId]}
      {if $group['PickingMethodID'] == 2}
        {foreach from=$groupComponents item='component'}
          {assign var="componentPrice" value=$componentPrices[$component['GoodID']]['Value']}
          {assign var="oldComponentPrice" value=$componentPrices[$component['GoodID']]['PrevValue']}
          <div class="c-part single complect-part" data-id="{$component['GoodID']}" data-price="{$componentPrice|intval}">
            <div class="block-wrap">
              {assign var="componentImage" value=$componentImages[$component['GoodID']]}
              <div class="image">
                <a href="/{$component['CategoryUID']}/{$component['GoodID']}_{$component['GoodEngName']}/" target="_blank">
                  <img src="{if $componentImage}/img/260x280/{$componentImage['MiniatureUrl']}{else}/i/nophoto.png{/if}">
                </a>
              </div>
              <div class="title" data-href="/{$component['CategoryUID']}/{$component['GoodID']}_{$component['GoodEngName']}">
                <h3>
                  <a href="/{$component['CategoryUID']}/{$component['GoodID']}_{$component['GoodEngName']}/" target="_blank">
                    {$component['GoodName']}
                  </a>
                </h3>
                <div class="price">
                  {if ($component['AvailQuantity'] > 0)}
                    <span class="date">
                      {if $isDefaultCity}
                        можно забрать сейчас
                      {else}
                        отгрузим сегодня
                      {/if}
                    </span>
                  {else}
                    
                      {*if true*} 
                      
                      {if $dateHelper->isTodayString($component['ArrivalDate'])}
                      <span class="date">
                        отгрузим сегодня
                      </span>
                      {else}
                      <span class="date later">
                        отгрузим {$dateHelper->getOutcomeShortDateText($component['ArrivalDate'])}
                      </span>
                      {/if}
                      {*else}
                        срок поставки {$dateHelper->getDeliveryDateRange($component['ArrivalDate'])}
                      {/if*}
                    
                  {/if}
                  <span class="digs">
                    <div>
                        <strong>{$componentPrice|format_price} <span class="rub">р</span></strong>
                        {if $oldComponentPrice > 0}
                          <strong>{$oldComponentPrice|format_price}  <span class="rub">р</span></strong>
                        {/if}
                    </div>
                    <div class="-x-checkbox checked disabled"></div>
                  </span>
                </div>
              </div>
            </div>
          </div>
        {/foreach}
      {else}
        <div class="c-part scrollable single-selector {if $group['PickingMethodID'] == 3}c-optional{/if}">
          <div class="viewport">
            <ul>
              {foreach from=$groupComponents item='component'}
                {assign var="componentPrice" value=$componentPrices[$component['GoodID']]['Value']}
                {assign var="oldComponentPrice" value=$componentPrices[$component['GoodID']]['PrevValue']}
                <li class="complect-part" data-id="{$component['GoodID']}" data-price="{$componentPrice|intval}">
                  {assign var="componentImage" value=$componentImages[$component['GoodID']]}
                  <div class="image"><img src='{if $componentImage}/img/260x280/{$componentImage['MiniatureUrl']}{else}/i/nophoto.png{/if}'></div>
                  <a href="/{$component['CategoryUID']}/{$component['GoodID']}_{$component['GoodEngName']}/" class="title">{$component['GoodName']}</a>
                  <div class="price">
                    {if ($component['AvailQuantity'] > 0)}
                      <span class="date">
                        {if $isDefaultCity}
                          можно забрать сейчас
                        {else}
                          отгрузим сегодня
                        {/if}
                      </span>
                    {else}
                    {*
                      <span class="date later">
                        {if true}
                          отгрузим  {$dateHelper->getOutcomeShortDateText($component['ArrivalDate'])}
                        {else}
                          срок поставки {$dateHelper->getDeliveryDateRange($component['ArrivalDate'])}
                        {/if}
                      </span>
                      *}
                      {if $dateHelper->isTodayString($component['ArrivalDate'])}
                      <span class="date">
                        отгрузим сегодня
                      </span>
                      {else}
                      <span class="date later">
                        отгрузим {$dateHelper->getOutcomeShortDateText($component['ArrivalDate'])}
                      </span>
                      {/if}
                    {/if}

                    <span class="digs">
                      <div>
                          <strong>{$componentPrice|format_price} <span class="rub">р</span></strong>
                          {if $oldComponentPrice > 0}
                            <strong>{$oldComponentPrice|format_price} <span class="rub">р</span></strong>
                          {/if}
                        </div>
                        <div class="-x-checkbox {if $component['IsChecked']}checked{/if}"></div>
                    </span>
                  </div>
                </li>
              {/foreach}
            </ul>
          </div>
          <div class="board">
            <span class='left'><b></b></span>
            <span class='right'><b></b></span>
            {assign var='componentCount' value=$groupComponents|count}
            <span class='right more'>еще {$componentCount-1}</span>
          </div>
        </div>
      {/if}
    {/foreach}
  {/if}

</section>

{if 0 < count($satelliteGroups)}
  {foreach from=$satelliteGroups item='satelliteGroup'}
    <div class="section-header large" id="options">
      <div class="-wrap">
        <h2>Опции: {$satelliteGroup.name}
        
          <small class="nomobile">для {if $item->GenitiveDetailName}{$item->GenitiveDetailName}{else}{$item->GoodName}{/if}</small>
        </h2>
        <span class="selected-info">
            <em>0</em> выбранo
        </span>
      </div>
    </div>

    <section class="options" data-menu="Опции">
      <div class="c-part scrollable multiple-selector">
        <div class="viewport">
          <ul>
            {foreach from=$satelliteGroup.goods item='satelliteGood'}
              {assign var='satelliteImages' value=$satelliteGoodsImages[$satelliteGood.GoodID]}
              <li class="complect-part satellite-part" data-id="{$satelliteGood.GoodID}" data-price="{$satelliteGood.Value|intval}">
                <div class="image">
                  {foreach from=$satelliteImages item="image" name='satelliteImages'}
                    {if $smarty.foreach.satelliteImages.first}
                      <img src="{$absHost}/img/260x280/{$image['MiniatureUrl']}" />
                    {/if}
                  {foreachelse}
                    <img src="/i/nophoto.png" />
                  {/foreach}
                </div>
                <a href="/{$satelliteGood['CategoryUID']}/{$satelliteGood['GoodID']}_{$satelliteGood['GoodEngName']}/" class="title">
                  {$satelliteGood.GoodName|escape}
                </a>
                <div class="price">
                  
                    {if $satelliteGood.PickupDate && 0 < ($satelliteGood['AvailQuantity'] - $satelliteGood['WindowQuantity'])}
                      {if $isDefaultCity}
                        
                        {if $dateHelper->isTodayString($satelliteGood.PickupDate)}
                            <span class="date">можно забрать сегодня</span>
                            
                        {else}
                        <span class="date later">отгрузим {$dateHelper->getOutcomeShortDateText($satelliteGood.PickupDate)}</span>
                        {/if}
                        {*$dateHelper->getPickupDateDescription($satelliteGood.PickupDate)*}
                      {else}
                        <span class="date">отгрузим сегодня</span>
                      {/if}
                    {elseif $satelliteGood['ArrivalDate']}
                      
                        {if $dateHelper->isTodayString($satelliteGood['ArrivalDate'])}
                        <span  class="date">
                          отгрузим сегодня
                        </span>
                        {else}
                          <span class="date later">
                          отгрузим {$dateHelper->getOutcomeShortDateText($satelliteGood['ArrivalDate'])}
                           </span>
                        {/if}
                     
                    {/if}
                  
                  <span class="digs">
                      <div>
                          <strong>{$satelliteGood.Value|format_price} <span class="rub">р</span></strong>
                      </div>
                      <div class="-x-checkbox"></div>
                  </span>
                </div>
								{if 0 < $satelliteGood.WindowQuantity}
									<div class="show-room" {if ($satelliteGood.PlaceRow || $satelliteGood.PlaceCell || $satelliteGood.PlaceRack)}data-placeinfo="<p>ряд:<b>{$satelliteGood.PlaceRow}</b></p><p>место:<b>{$satelliteGood.PlaceCell}</b></p>"{/if}>{if $isWorkTime}сегодня {/if}можно посмотреть в торговом зале</div>
								{/if}
              </li>
            {/foreach}
          </ul>
        </div>
        <div class="board">
          <span class='left'><b></b></span>
          <span class='right'><b></b></span>
          <span class='right more'>еще</span>
        </div>
      </div>
    </section>

  {/foreach}
{/if}

{if 0 < count($properties)}
  <div class="section-header" id="characteristics">
      <div class="-wrap">
          <h2>Характеристики {if $item->GenitiveDetailName}{$item->GenitiveDetailName}{else}{$item->GoodName}{/if}</h2>
      </div>
  </div>

  <section class="characteristics" data-menu="Характеристики">
    {include file='./partial/complect.properties.tpl'}
  </section>
{/if}

{if count($anotherPackages)}
  <div class="section-header large" id="analogs">
    <div class="-wrap">
      <h2>другие варианты комплекта</h2>
    </div>
  </div>

  <section class="analogs" data-menu="Другие варианты">
    <ul class="goods">
      {foreach from=$anotherPackages item='package' name='goods'}
        <li>
          <a href="/{$package['CategoryUID']}/{$package['GoodID']}_{$package['GoodEngName']}/" class="product-link" data-id="{$package['GoodID']}" data-name="{$package['GoodName']}"{if isset($package['CategoryName'])} data-category="{$package['CategoryName']}"{/if}{if isset($package['BrandName'])} data-brand="{$package['BrandName']}"{/if} data-price="{$package['Value']|intval}" data-action="SuitableGoods" data-position="{$smarty.foreach.goods.iteration}">
            <div class="images">
              {assign var="packageImage" value=$anotherPackagesImages[$package['GoodID']]}
              <div class="image" style="background-image: {if $packageImage}url('{$absHost}/img/260x280/{$packageImage['MiniatureUrl']}'){else}url('/i/nophoto.png');{/if}"></div>
            </div>
          </a>
          <a class="title" href="/{$package['CategoryUID']}/{$package['GoodID']}_{$package['GoodEngName']}/">{$package['GoodName']}</a>
          {*<div class="sizes">габариты: 48,5x86x45 </div>*}
          {if $package.PickupDate && 0 < ($package['AvailQuantity'] - $package['WindowQuantity'])}
            
              {if $isDefaultCity}
              
                 {if $dateHelper->isTodayString($package.PickupDate)}
                         
                 <div class="delivery now">
                            можно забрать сегодня
                </div>
                            
                 {else}
                 <div class="delivery later">
                     отгрузим {$dateHelper->getOutcomeShortDateText($package.PickupDate)}
                 </div>
                 {/if}
                {*$dateHelper->getPickupDateDescription($package.PickupDate)*}
              {else}
              <div class="delivery now">
                отгрузим сегодня
              </div>
              {/if}
            
          {elseif $package['ArrivalDate']}
            
              {if $dateHelper->isTodayString($package['ArrivalDate'])}
              <div class="delivery now">
                отгрузим сегодня
              </div>
              {else}
              <div class="delivery later">
                отгрузим  {$dateHelper->getOutcomeShortDateText($package['ArrivalDate'])}
              </div>
              {/if}
              {*else}
              <div class="delivery later">
                срок поставки {$dateHelper->getDeliveryDateRange($package['ArrivalDate'])}
              </div>
              {/if*}
            
          {/if}
          <div class="price {if $package['PrevValue'] > 0}discounted{/if}">
            <strong>{$package['Value']|format_price} <span class="rub">р</span></strong>
            {if $package['PrevValue'] > 0}
              <strong class="old">{$package['PrevValue']|format_price} <span class="rub">р</span></strong>
            {/if}
            <a href="#" class="add-to-basket" data-id="{$package['GoodID']}"></a>
          </div>
        </li>
      {/foreach}
    </ul>
    <div class="show-more">
      <a href="#">Еще варианты комплекта</a>
    </div>
  </section>
{/if}

{include file="./partial/warranty.tpl" complect=true}

<div id="top-panel" class="complect">
  <div class="article"><span class="onlymobile">артикул {$item->GoodID}</span><span class="nomobile"><a href="#" class="scroll-top">Один комплект из <span class="quantity">{$selectedComponentsCount}</span> позиций</a></span></div>
  <div class="price">
    <strong><b>{$price->value|format_price}</b> <span class='rub'>р</span></strong>
    <a href="#" class="add-to-basket" data-id="{$item['GoodID']}"><span class="nomobile">Купить комплект</span></a>
  </div>
</div>