{* Smarty *}
{strip}

{foreach from=$productIds item='productId'}
  {assign var='productProperties' value=$properties[$productId]}
  {assign var='productSelectableProperties' value=$selectableProperties[$productId]}
  {if 0 < count($productProperties) && isset($goods[$productId])}
    {assign var='component' value=$goods[$productId]}
    <ul id="characteristics-{$groupId}">
      <li class="element-photo">
        {assign var="componentImage" value=$componentImages[$productId]}
        <a href="/{$component['CategoryUID']}/{$component['GoodID']}_{$component['GoodEngName']}/" target="_blank">
          <img src="{if $componentImage}/img/260x280/{$componentImage['MiniatureUrl']}{else}/i/nophoto.png{/if}">
        </a>
        <div class="title">Характеристики <a href="/{$component['CategoryUID']}/{$component['GoodID']}_{$component['GoodEngName']}/" target="_blank">{$component.GoodName}</a></div>
      </li>
      {assign var='hasHiddenProperty' value=false}
      {foreach from=$productProperties item='property'}
        {assign var='showProperty' value=false}
        {if $property->displayInMiddleDescription}
          {assign var='showProperty' value=true}
        {/if}
        {if !$showProperty}
          {assign var='hasHiddenProperty' value=true}
        {/if}
        <li class="{if !$showProperty} hidden{/if}{if $showProperty} printable{/if}">
          <span>
            {$property->name}
            {if !empty($property->hint)}
              <a href="#" class="-info">
                <span class="content">
                  {$property->hint}
                  {if !empty($property->hintImage)}<img src="/img/pi/{$property->hintImage}">{/if}
                 </span>
              </a>
            {/if}
          </span>
          {if array_key_exists($property->propId, $productSelectableProperties)}
            <div>
              <div class="-x-selector" data-name="sizes"><div class="expander"></div>
                <ul>
                  <li class="selected" data-value="{$property->valueId}">{$property->value}</li>
                  {assign var='propertyValues' value=$productSelectableProperties[$property->propId]}
                  {foreach from=$propertyValues item='propertyValue' key="valueId"}
                    <li data-value="{$valueId}">
                      <a style="color: #fff; text-decoration: none" href="{$propertyValue['url']}">{$propertyValue['value']}</a>
                    </li>
                  {/foreach}
                </ul>
              </div>
            </div>
          {else}
            <div>{$property->value}</div>
          {/if}
        </li>
       {/foreach}

       {assign var='notes' value=$productsNotes[$productId]}
       {if is_array($notes) && (0 < count($notes))}
         {foreach from=$notes item='note'}
           <li class="printable marked"><div>{$note->Text}</div></li>
         {/foreach}
       {/if}

     </ul>

     {if $hasHiddenProperty}
       <br>
       <a href="#" class="show-all" data-show="characteristics-{$groupId}">Показать все характеристики</a>
       <br>
     {/if}
   {/if}

{/foreach}

{/strip}