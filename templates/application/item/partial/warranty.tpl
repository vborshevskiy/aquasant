<div class="section-header" id="returns">
  <div class="-wrap">
    <h2>30 дней на возврат</h2>
  </div>
</div>

<section class="returns">
  <div class="text">
    <p>Наша стратегия — честность. Гарантируем легкий возврат товара в течение 30 дней.</p>
    <p>Не подошел? Не влез? — Бывает, возвращайте в состоянии идентичном новому и получайте свои деньги назад.</p>
    <p>Если же {$item->ShortVendorName} вдруг не работает должным образом — оформить возврат можно прямо из личного кабинета.</p>
  </div>
  <div class="buy-now">
      <div class="buy">
      <span class="price">
        {if $price->prevValue > $price->value}
            <strong>{$price->prevValue|format_price} <span class="rub">р</span></strong>
        {/if}
        <strong class="item-total-amount">{$price->value|format_price} <span class="rub">р</span></strong>
      </span>
          {if 0 < $availability.AvailQuantity || 0 < $availability.SuborderQuantity}
              <a href="#" data-id="{$item->GoodID}" class="add-to-basket"{if $smarty.const.MANAGER_MODE && $onlyInShowRoom} data-only-showroom="1"{/if}{if $smarty.const.MANAGER_MODE && $inShowRoom && $isDefaultCity} data-showroom="1"{/if}{if $smarty.const.MANAGER_MODE && $availability && (0 < $availability.SuborderQuantity) && (0 == ($availability.AvailQuantity - $availability.WindowQuantity))} data-has-suborder="1"{/if}><span>купить <span class="nomobile">{$item->AccusativeDetailName}</span></span></a>
          {/if}
      </div>

      {if !$smarty.const.MANAGER_MODE}
        <div class="info">Сантехника оптом и в розницу — база сантехники «Аквасант», успейте дешево купить
          {if $item->AccusativeDetailName}{$item->AccusativeDetailName}{elseif $item->ShortVendorName}{$item->ShortVendorName}{/if} со склада в Москве до повышения цены.</div>
      {/if}
  </div>
</section>

{if $complect}
  {if (!$onlyInShowRoom || $smarty.const.MANAGER_MODE) && ((0 < $availability.AvailQuantity) || (0 < $availability.SuborderQuantity))}
    <div class="section-header onlymobile" id="payments-types">
     <div class="-wrap">
        <h2>Все способы оплаты</h2>
      </div>
    </div>
    <section class="payments-types onlymobile">
      <ul>
      <li>Наличные</li>
      <li>Безналичный перевод</li>
      <li>Банковская карта</li>
      <li>Электронные деньги</li>
      </ul>
    </section>
  {/if}

  {if $rightBanner}
    <div class="banner onlymobile">
      {if $rightBanner['url']}<a href="{$rightBanner['url']}">{/if}
        <img src="{$rightBanner['image']}">
      {if $rightBanner['url']}</a>{/if}
    </div>
  {/if}
{/if}