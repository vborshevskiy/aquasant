<section class="breadscrumbs">
  <div class="-wrap">
    <ul class="path">
      <li>артикул {$item->GoodID}</li>
      {foreach from=$breadcrumbs item=i name=breadcrumbs}
        {if !$i.href}
          <li>{$i.title}</li>
        {else}
          <li><a href="{$i.href}">{$i.title}</a></li>
        {/if}
      {/foreach}
    </ul>
    <div>
      <a href="#" class="print-page">Напечатать</a>
    </div>
  </div>
</section>
<div class="-wrap">
  <h1 itemprop="name">{$item->GoodName}</h1>
  <div class="only-print print-article">артикул {$item->GoodID}</div>
</div>