{* Smarty *}
{assign var='fullImageSize' value='864x1080'}
{if true || isset($smarty.get.lower)}
  {assign var='fullImageSize' value='450x600'}
{/if}

<div class="-wrap">
  <section class="description" data-menu="Основное">
    <aside class="gallery">
      {if count($images)}
        <div class="only-print print-image">
          <img src="{$absHost}/img/{$fullImageSize}/{$images[0]['MiniatureUrl']}" alt=""  itemprop="image" />
        </div>
        <div class="viewport">
          <img src="{$absHost}/img/{$fullImageSize}/{$images[0]['MiniatureUrl']}" alt=""  itemprop="image" />
        </div>
        <ul class="thumbs">
          {foreach from=$images item='image'}
            <li><img src="{$absHost}/img/60x60/{$image['MiniatureUrl']}" data-full="{$absHost}/img/{$fullImageSize}/{$image['MiniatureUrl']}"></li>
          {/foreach}
        </ul>
      {else}
        <div class="viewport no-photo">
          <div class="slide"><img src='/i/nophoto.png'></div>
        </div>
      {/if}
    </aside>
    <aside class="buy-block">
      {*<div class="action gift">*}
        {*гидромассаж в подарок*}
      {*</div>*}
      {*{if false && $inShowRoom && $isDefaultCity}
        <div class="details only-mobile">
          <div class="show-room" {if $place}data-placeinfo="<p>ряд:<b>{$place.PlaceRow}</b></p><p>место:<b>{$place.RowCell}</b></p>"{/if}>
            {if $this->isStoreWork()}сегодня{/if} можно посмотреть в <a href="/contacts/" target="_blank">торговом зале</a>
          </div>
        </div>
      {/if}*}

      <nav class="buy">
          <div class="-mng-chars">
              <ul class="-items">
                  {strip}
                      <li class="-price">{if $price->prevValue > $price->value}<em>{$price->prevValue|format_price} <span class="rub">р</span></em>{/if}
                          <b>{$price->value|format_price} <span class="rub">р</span></b></li>
                      {if $userRegion}
                          {if $userRegion->getId() == 42}
                              <li class="-line">
                                  <em>Доставка по Москве</em>
                                  <strong>
                                      {if !empty($deliveryDayName)}
                                          {$deliveryDayName}
                                      {elseif $deliveryDay}
                                          {$deliveryDay}
                                      {/if}
                                      {if $userRegion && ($userRegion->getId() == 42)}
                                          {if !$deliveryCost}
                                              , бесплатно
                                          {else}
                                              , {$deliveryCost|format_price} <span class="rub">р</span>
                                          {/if}
                                      {/if}
                                  </strong>
                              </li>
                          {else}
                              <li class="-line">
                                  <em>Отгрузка с оптового московского склада</em>
                                  <strong>
                                      {if !empty($pickupDayName)}
                                          {$pickupDayName}
                                      {else}
                                          {$deliveryDay}
                                      {/if}
                                  </strong>
                                  <br>
                                  <br>
                                  Стоимость доставки в ваш город<br>
                                  уточняйте у операторов
                                  <br>
                                  <br>
                              </li>
                          {/if}
                      {else}
                          <li class="-line">
                              <em>Отгрузка с оптового московского склада</em>
                              <strong>
                                  {if !empty($pickupDayName)}
                                      {$pickupDayName}
                                  {else}
                                      {$deliveryDay}
                                  {/if}
                              </strong>
                              <br>
                              <br>
                              Стоимость доставки в ваш город<br>
                              уточняйте у операторов
                              <br>
                              <br>
                          </li>
                      {/if}
                      {if $userRegion && ($userRegion->getId() == 42)}
                          {if ($availability.AvailQuantity > 0) || (0 < $availability.SuborderQuantity)}
                              <li class="-line">
                                  <em>Самовывоз со склада</em>
                                  <strong>
                                      {if (0 < $availability.AvailQuantity) && ($availability.AvailQuantity > $availability.WindowQuantity)}
                                          {if $pickupDayName}
                                              {$pickupDayName}
                                          {elseif $pickupDate}
                                              {$dateHelper->getPickupShortDateText($pickupDate)}
                                          {/if}
                                      {elseif $showRoomPickupDay}
                                          {if $dateHelper->isToday($pickupDate)}
                                              сегодня
                                          {else}
                                              {$showRoomPickupDay}
                                          {/if}
                                      {/if}
                                      {if $smarty.const.MANAGER_MODE && $store.Avail}, {$store.Avail} шт{/if}
                                  </strong>
                              </li>
                          {/if}

                          {if $inShowRoom && $isDefaultCity && $place}
                              <li class="-line"><em>Есть в торговом зале</em><strong class="small">ряд {$place.PlaceRow}, место {$place.PlaceCell}
                                      {if $smarty.const.MANAGER_MODE}, {$place.Avail} шт{/if}</strong></li>
                          {/if}
                          {if $smarty.const.MANAGER_MODE}
                              {assign var='break' value=false}
                              {foreach from=$supplierRemains item = 'supplier'} {if !$break && !empty($supplier['ArrivalQuantity'])}
                                  <li class="-line"><em>Ожидаем {$supplier['ArrivalDate']}</em><strong>{$supplier['ArrivalQuantity']} шт</strong></li>
                                  {assign var='break' value=true}
                              {/if}
                              {/foreach}
                              {foreach from=$supplierRemains item = 'supplier'}
                                  <li class="-line"><em>{$supplier['SupplierName']}</em><strong>
                                          {if $supplier['Quantity']}
                                              {$supplier['Quantity']} шт <i>{$supplier['LastPriceUpdated']}</i>
                                          {else}
                                              {$supplier['ProductionTerm']} дней
                                          {/if}
                                      </strong></li>
                              {/foreach}
                              {if $item->Markup}
                                  <li class="-line"><em>Рейтинг</em><strong>{$item->Markup}</strong></li>
                              {/if}
                          {/if}
                      {/if}
                  {/strip}
              </ul>
              <a href="#" class="add-to-basket" data-id="{$item->GoodID}">Купить {$item->AccusativeCategoryName}</a>
          </div>

          <div data-menu="Цена и наличие" data-for="priceavail" id="priceavail" class="price element" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
              <strong>Цена<span class='onlymobile'> и наличие</span></strong>
              <em><b>{if $price->prevValue > $price->value}{$price->prevValue|format_price} <span class="rub">р</span>{/if}</b>
                  <b>{$price->value|format_price} <span class="rub">р</span></b>
              </em>
              <meta itemprop="price" content="{$price->value}">
              <meta itemprop="priceCurrency" content="RUB">
          </div>

        <div class="only-mobile element -compact-delivery">
          {*{if false}
            <div class="block">
              {if $userRegion}
                {if $userRegion->getId() == 42}
                  <strong>наличие</strong>
                {else}
                  <strong>отгрузка</strong>
                  <strong class="marked">
                    {if !empty($pickupDayName)}
                      {$pickupDayName}
                    {else}
                      {$deliveryDay}
                    {/if}
                  </strong>
                {/if}
              {else}
                <strong>отгрузка</strong>
                  <strong class="marked">
                    {if !empty($pickupDayName)}
                      {$pickupDayName}
                    {else}
                      {$deliveryDay}
                    {/if}
                  </strong>
              {/if}
            </div>
          {/if}*}
            <ul class="-items">

              {strip}
              {if $userRegion}
                {if $userRegion->getId() == 42}
                  <li>
                    <span>Доставка по Москве</span>
                    <em>
                      {if !empty($deliveryDayName)}
                        {$deliveryDayName}
                      {elseif $deliveryDay}
                        {$deliveryDay}
                      {/if}
                      {if $userRegion && ($userRegion->getId() == 42)}
                        {if !$deliveryCost}
                          , бесплатно
                        {else}
                          , {$deliveryCost|format_price} <span class="rub">р</span>
                        {/if}
                      {/if}
                    </em>
                  </li>
                {else}
                  <li>
                    <span>Отгрузка с оптового московского склада</span>
                    <em>
                      {if !empty($pickupDayName)}
                        {$pickupDayName}
                      {else}
                        {$deliveryDay}
                      {/if}
                    </em>
                    <br>
                    <br>
                    Стоимость доставки в ваш город<br>
                    уточняйте у операторов
                    <br>
                    <br>
                  </li>
                {/if}
              {else}
                <li>
                  <span>Отгрузка с оптового московского склада</span>
                  <em>
                    {if !empty($pickupDayName)}
                      {$pickupDayName}
                    {else}
                      {$deliveryDay}
                    {/if}
                  </em>
                  <br>
                  <br>
                  Стоимость доставки в ваш город<br>
                  уточняйте у операторов
                  <br>
                  <br>
                </li>
              {/if}
              {if $userRegion && ($userRegion->getId() == 42)}
                {if ($availability.AvailQuantity > 0) || (0 < $availability.SuborderQuantity)}
                  <li>
                    <span>Самовывоз со склада</span>
                    <em>
                      {if (0 < $availability.AvailQuantity) && ($availability.AvailQuantity > $availability.WindowQuantity)}
                        {if $pickupDayName}
                            {$pickupDayName}
                        {elseif $pickupDate}
                            {$dateHelper->getPickupShortDateText($pickupDate)}
                        {/if}
                      {elseif $showRoomPickupDay}
                        {if $dateHelper->isToday($pickupDate)}
                          сегодня
                        {else}
                          {$showRoomPickupDay}
                        {/if}
                      {/if}
                        {if $smarty.const.MANAGER_MODE && $store.Avail}, {$store.Avail} шт{/if}
                    </em>
                  </li>
                {/if}

                {if $inShowRoom && $isDefaultCity && $place}
                  <li class="one-row">
                    <span>Есть в торговом зале</span>
                    <em>ряд {$place.PlaceRow}, место {$place.PlaceCell}{if $smarty.const.MANAGER_MODE}, {$place.Avail} шт{/if}</em>
                  </li>
                {/if}
                  {if $smarty.const.MANAGER_MODE}
                      {assign var='break' value=false}
                      {foreach from=$supplierRemains item = 'supplier'} {if !$break && !empty($supplier['ArrivalQuantity'])}
                          <li class="one-row"><span>Ожидаем {$supplier['ArrivalDate']}</span><em>{$supplier['ArrivalQuantity']} шт</em></li>
                          {assign var='break' value=true}
                      {/if}
                      {/foreach}
                      {foreach from=$supplierRemains item = 'supplier'}
                          <li class="one-row"><span>{$supplier['SupplierName']}</span>
                              <em>
                                  {if $supplier['Quantity']}
                                      {$supplier['Quantity']} шт <i>{$supplier['LastPriceUpdated']}</i>
                                  {else}
                                      {$supplier['ProductionTerm']} дней
                                  {/if}
                              </em>
                          </li>
                      {/foreach}
                      {if $item->Markup}<li class="one-row"><span>Рейтинг</span><em>{$item->Markup}</em></li>{/if}
                  {/if}
              {/if}
              {/strip}
            </ul>
            {if !$smarty.const.MANAGER_MODE}
              <h5 data-menu="Торговый зал" data-for="ourshop" id="ourshop">Торговый зал 2000 м<sup>2</sup></h5>
              <div class="gallery for-shop">
                <div class="viewport">
                  <img src="/images/galleries/zal1.jpg" alt="">
                  <img src="/images/galleries/zal2.jpg" alt="">
                  <img src="/images/galleries/zal3.jpg" alt="">
                  <img src="/images/galleries/zal4.jpg" alt="">
                  <img src="/images/galleries/zal5.jpg" alt="">
                  <div class="map" id="map"><img src='/i/empty.png'></div>
                </div>
                <div class='scroll-buttons'>
                  <a href="#" class="scroll left"></a>
                  <a href="#" class="scroll right"></a>
                </div>
              </div>
            {/if}
        </div>

        {if (!$onlyInShowRoom || $smarty.const.MANAGER_MODE) && ((0 < $availability.AvailQuantity) || (0 < $availability.SuborderQuantity))}
          <div class="pay element">
            <div class="block nomobile">
              <strong>Все способы оплаты</strong>
              <em>нал, безнал, онлайн</em>
            </div>
            <div class="details nomobile">
              <ul>
                <li>Наличные</li>
                <li>Безналичный перевод</li>
                <li>Банковская карта</li>
                <li>Электронные деньги</li>
              </ul>
            </div>
            <div class="set">
              <a href="#" class="add-to-basket" data-id="{$item->GoodID}"{if $smarty.const.MANAGER_MODE && $onlyInShowRoom} data-only-showroom="1"{/if}{if $smarty.const.MANAGER_MODE && $inShowRoom && $isDefaultCity} data-showroom="1"{/if}{if $smarty.const.MANAGER_MODE && $availability && (0 < $availability.SuborderQuantity) && (0 == ($availability.AvailQuantity - $availability.WindowQuantity))} data-has-suborder="1"{/if}>
                  <span>Купить {$item->AccusativeCategoryName}</span></a>
            </div>
            {if !isset($nocompare) || (false === $nocompare)}
              <div class="compare {if $isCompared}remove{else}add{/if}">
                <a href="#" class="compare"></a>
              </div>
            {/if}
          </div>
          <div class="only-print buy-info-block">
            <em>Купить на сайте:</em><span>www.{$smarty.const.DOMAIN_NAME}</span>
          </div>
        {/if}
      </nav>
      {if count($properties) > 0}
        <ul class="table">
          {foreach from=$properties item = 'property'}
            {if !$property->displayInSmallDescription}
              {continue}
            {/if}
            <li>
              <p>
                {$property->name}
                {if !empty($property->hint)}
                  <a href="#" class="-info">
                    <span class="content">
                      {$property->hint}
                      {if !empty($property->hintImage)}<img src="/img/pi/{$property->hintImage}">{/if}
                     </span>
                  </a>
                {/if}
              </p>
              <span>
                {if array_key_exists($property->propId, $selectableProperties)}
                  <div class="-x-selector" data-name="sizes"><div class="expander"></div>
                      <ul>
                        <li class="selected" data-value="{$property->valueId}">{$property->value}</li>
                        {assign var='propertyValues' value=$selectableProperties[$property->propId]}
                        {foreach from=$propertyValues item='propertyValue' key="valueId"}
                          <li data-value="{$valueId}">
                            <a style="color: #fff; text-decoration: none" href="{$propertyValue['url']}">{$propertyValue['value']}</a>
                          </li>
                        {/foreach}
                      </ul>
                    </div>
                {else}
                  <span>{$property->value}</span>
                {/if}
              </span>
            </li>
          {/foreach}
        </ul>
        <a href="#" class="to-info">Все характеристики {if $item->GenitiveDetailName}{$item->GenitiveDetailName}{else}{$item->GoodName}{/if}</a>
      {/if}

      {if $rightBanner}
        <div class="banner nomobile">
          {if $rightBanner['url']}<a href="{$rightBanner['url']}">{/if}
            <img src="{$rightBanner['image']}">
          {if $rightBanner['url']}</a>{/if}
        </div>
      {/if}

    </aside>
  </section>
</div>