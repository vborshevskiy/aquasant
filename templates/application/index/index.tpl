{assign var="position" value=1}
{if count($categories)}
  {foreach from=$categoryIds item='categoryId'}
    {assign var="items" value=$goods[$categoryId]}

    {if count($items)}
      <section class="main-group">
        <h2 style="background-image: url('{$categoriesImages[$categoryId]}');">
          <span>
            {if 35 == $categoryId}
              Мебель для ванны по оптовым ценам
            {elseif 81 == $categoryId}
              Унитазы со скидкой по оптовой цене
            {elseif 22 == $categoryId}
              Душевые кабины по акции
            {else}
              {$categories[$categoryId]['CategoryName']}
            {/if}
          </span>
        </h2>
        <ul class="goods">
          {foreach from=$items item='good'}
            {include file="../catalog/partial/item.tpl" showRoom=$goodsRemainsInTheMainStore[$good['GoodID']] action="ProductMainPage" position=$position}
          {/foreach}
        </ul>
        <div class="show-more">
          <a href="#">Еще {$categories[$categoryId]['CategoryName']|lower}</a>
        </div>
      </section>
      {assign var="position" value=$position+1}
    {/if}
  {/foreach}
{/if}
