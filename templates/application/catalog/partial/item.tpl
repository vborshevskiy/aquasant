{* Smarty *}
{strip}

{assign var='hasOldPrice' value=false}
{if isset($good['PrevValue']) && ($good['Value'] < $good['PrevValue'])}
  {assign var='hasOldPrice' value=true}
{/if}

<li{if 1 == $good['IsPackage']} class="package"{/if}>
  <a href="/{$good['CategoryUID']}/{$good['GoodID']}_{$good['GoodEngName']}/" class="images-wrapper product-link" data-id="{$good['GoodID']}" data-name="{$good['GoodName']}"{if isset($good['CategoryName'])} data-category="{$good['CategoryName']}"{/if}{if isset($good['BrandName'])} data-brand="{$good['BrandName']}"{/if} data-price="{$good['Value']|intval}" data-action="{$action}" data-position="{$position}">
    <div class="images">
      {foreach from=$goodsImages[$good['GoodID']] item="image"}
        <div class="image" style="background-image: url('{$absHost}/img/260x280/{$image['MiniatureUrl']}')"></div>
      {foreachelse}
        <div class="image no-photo" style="background-image: url('/i/nophoto.png');"></div>
      {/foreach}
    </div>
  </a>
  <a class="title" href="/{$good['CategoryUID']}/{$good['GoodID']}_{$good['GoodEngName']}/">{$good['GoodName']}</a>

	{assign var='pickupDate' value=$dateTime::createFromFormat('Y-m-d H:i:s', $good.PickupDate)}
	{assign var='deliveryDate' value=$dateTime::createFromFormat('Y-m-d H:i:s', $good.DeliveryDate)}

	{if $pickupDate && ($dateHelper->isToday($pickupDate) || $dateHelper->isTomorrow($pickupDate)) && $good.AvailQuantity > 0}
		<div class="delivery now">
			{if $isDefaultCity}
				можно забрать {$dateHelper->getPickupDateDescription($pickupDate)}
			{else}
				отгрузим сегодня
			{/if}
		</div>
	{elseif $deliveryDate}
		<div class="delivery later">
      {if true}
        отгрузим {$dateHelper->getOutcomeShortDateText($deliveryDate)}
      {else}
        срок поставки {$dateHelper->getDeliveryDateRange($deliveryDate)}
      {/if}
    </div>
	{/if}

  <div class="price {if $hasOldPrice}discounted{/if}">
    <strong>
      {$good['Value']|format_price} <span class="rub">р</span>
    </strong>
    {if $hasOldPrice}
      <strong class="old">
        {$good['PrevValue']|format_price} <span class="rub">р</span>
      </strong>
    {/if}
    <a href="#" class="add-to-basket" data-id="{$good['GoodID']}"></a>
  </div>
  {if $showRoom && $isDefaultCity}
    <div class="action show-room not-mobile" {if ($good.PlaceRow || $good.PlaceCell || $good.PlaceRack)}data-placeinfo="<p>ряд:<b>{$good.PlaceRow}</b></p><p>место:<b>{$good.PlaceCell}</b></p>"{/if}>
      {if $isWorkTime}сегодня {/if}можно посмотреть<br>
      в торговом зале
    </div>
    <div class="action show-room{if 'ProductMainPage' != $action} mobile{/if}" {if ($good.PlaceRow || $good.PlaceCell || $good.PlaceRack)}data-placeinfo="<p>ряд:<b>{$good.PlaceRow}</b></p><p>место:<b>{$good.PlaceCell}</b></p>"{/if}>
      есть в шоу-руме
    </div>
  {/if}
</li>

{/strip}