{assign var='querySorting' value=''}
{if $this->catalog()->goods()->hasUserDefinedSorting()}
  {assign var='defaultSorting' value=$this->catalog()->goods()->getUserDefinedSorting()}
  {if null !== $defaultSorting}
    {assign var='querySorting' value="&sort=`$defaultSorting.column`&dir=`$defaultSorting.direction`"}
  {/if}
{/if}


{if ($category)}
  {assign var='urlRootPath' value='/'|cat:$category['CategoryUID']}
  {if 0 < count($currentSets)}
    {foreach from=$currentSets item='currentSet'}
      {assign var='urlRootPath' value=$urlRootPath|cat:'-'|cat:$currentSet.SetUrlPart}
    {/foreach}
  {/if}
  <h1>
    {$category['CategoryName']}{if isset($brandNameForTitle) && !empty($brandNameForTitle)} {$brandNameForTitle}{/if}
    {if 0 < count($currentSets)}
      {foreach from=$currentSets item='currentSet'} {$currentSet.SetName}{/foreach}
    {/if}
  </h1>
  {if $categories}
    <ul class="subcategories">
      {foreach from=$categories item='subcategory'}
        <li><a href="{$subcategory->getUrl()}">{$subcategory->getName()}</a></li>
      {/foreach}
    </ul>
  {/if}
{elseif $isPolygon}
  {assign var='urlRootPath' value='/polygon'}
  {assign var='querySearch' value="&q=`$smarty.get.q`"}
  <h1>Тестовый полигон</h1>
{else}
  {assign var='urlRootPath' value='/search'}
  {assign var='querySearch' value="&q=`$smarty.get.q`"}
  <h1>Результаты поиска</h1>
{/if}

{assign var='setBaseUrl' value=''}
{if $category}
  {assign var='setBaseUrl' value='/'|cat:$category['CategoryUID']}
  {foreach from=$currentSets item='currentSet'}
    {assign var='setBaseUrl' value=$setBaseUrl|cat:'-'|cat:$currentSet['SetUrlPart']}
  {/foreach}
{/if}

<input id="scroll-control" type="text" tabindex="-1" value="">

{if (count($goods))}
  {if (!$selectedFilters->isEmpty())}
    <div class="selected-filters">
      {if null !== $priceRange}
        {assign var='rangeMin' value=$priceRange.minPrice|intval}
        {assign var='rangeMax' value=$priceRange.maxPrice|intval}
        {if $selectedFilters->has('price')}
          {assign var='rangeMin' value=$selectedFilters->price()->getFrom()|intval}
          {assign var='rangeMax' value=$selectedFilters->price()->getTo()|intval}
        {/if}
        {if $rangeMin > $priceRange.minPrice|intval || $rangeMax < $priceRange.maxPrice|intval }
        <div class="x-filter">
          {assign var='correctUrl' value=$urlRootPath|cat:$selectedFilters->toQueryString(array('price', 'limit'))|cat:$querySorting|cat:$querySearch|clear_question_mark}
          <span>Цена</span><em>{$rangeMin|format_price} – {$rangeMax|format_price} Р<a href="{$correctUrl}" class="del"></a></em>
        </div>
        {/if}
      {/if}

      {foreach from=$filters item='filter'}
        {if !$filter->isEmpty() && $filter->getId() != 'brands'}
          {if $filter->getType() == 1}
            {if ($filter->getSelectRangeMin() > $filter->getMinValue()) || ($filter->getSelectRangeMax() < $filter->getMaxValue())}
              <div class="x-filter">
                <span>{$filter->getTitle()}</span><em>{$filter->getSelectRangeMin()} – {$filter->getSelectRangeMax()} {if $filter->getUnit()} {$filter->getUnit()} {/if}<a href="#link-with-remove" class="del"></a></em>
              </div>
            {/if}
          {else}
            {if $filter->hasSelectedValues()}
              <div class="x-filter">
                {if $filter->getType() != 2 }
                  <span>{$filter->getTitle()}</span>
                {/if}
                <div class="group">
                  {foreach from=$filter->enumSelectedValues() item='filterValue' name='filterValues'}
                    {assign var='correctUrl' value=$urlRootPath|cat:$filterValue->additional('deleteUrl')|cat:$querySorting|cat:$querySearch|clear_question_mark}
                    <em>{$filterValue->getTitle()}<a href="{$correctUrl}" class="del"></a></em>
                  {/foreach}
                </div>
              </div>
            {/if}
          {/if}
        {/if}
      {/foreach}

      {if $selectedFilters->has('brands')}
        <div class="x-filter">
          <span>Бренды</span>
          <div class="group">
            {foreach from=$filters->getFilter('brands') item='brand'}
              {if (null !== $brand) && (0 < $brand->countGoods()) && $brand->isSelected()}
                {$correctUrl = $urlRootPath|cat:$brand->additional('deleteUrl')|cat:$querySorting|cat:$querySearch|clear_question_mark}
                <em>{$brand->getLabel()}<a href="{$correctUrl}" class="del"></a></em>
              {/if}
            {/foreach}
          </div>
        </div>
      {/if}
    </div>
  {/if}
{/if}
{if (count($goods))}
  <a href="#" class="toggle-filters"><em></em><span data-txt="показать фильтры" data-alt="свернуть фильтры">показать фильтры</span></a>
{/if}
<aside class="filters">
  {if (count($goods))}
    {assign var='hasHiddenFilters' value=false}

    <input type="hidden" value="{$categoryId}" name="categoryId">

    {foreach from=$imageFilters item="filter"}
      <div class="filter options selected" data-name="{$filter->getId()}">
        {foreach from=$filter->enumNonEmptyValues() item='filterValue' name='filterValues'}
          <div class="option {if $filterValue->isSelected()}selected{/if}" data-value="{$filterValue->getId()}">
            <img src="{$absHost}/img/pvi/{$filterValue->getImage()}">
            <h4>{$filterValue->getTitle()}</h4>
          </div>
        {/foreach}
      </div>
    {/foreach}

    {if count($filters->getFilter('brands'))}
      <div class="filter selector" data-name="brands">
        <h4>Бренды</h4>
        <ul class="options">
          {foreach from=$filters->getFilter('brands') item='brand'}
            {if (null !== $brand) && (0 < $brand->countGoods())}
              <li {if $brand->isSelected()} class="selected"{/if} data-value="{$brand->getTitle()}">{$brand->getLabel()}</li>
            {/if}
          {/foreach}
        </ul>
      </div>
    {/if}

    {if null !== $priceRange}
      {assign var='rangeMin' value=$priceRange.minPrice|intval}
      {assign var='rangeMax' value=$priceRange.maxPrice|intval}
      {if $selectedFilters->has('price')}
        {assign var='rangeMin' value=$selectedFilters->price()->getFrom()|intval}
        {assign var='rangeMax' value=$selectedFilters->price()->getTo()|intval}
      {/if}
      <div class="filter slider {if $rangeMin > $priceRange.minPrice|intval || $rangeMax < $priceRange.maxPrice|intval }expanded changed{/if}" data-name="price">
        <h4>Цена, <span class="rub">р</span></h4>
        <div class="range">
          <input type="range" min="{$priceRange.minPrice|intval}" max="{$priceRange.maxPrice|intval}" value="{$rangeMin},{$rangeMax}">
        </div>
      </div>
    {/if}

    {foreach from=$simpleFilters item='filter'}
      {assign var='isHidden' value=false}
      {if !$filter->getMain()}
        {assign var='isHidden' value=true}
      {/if}
      {if !$isHidden}
        {if !$filter->isEmpty() && $filter->getId() != 'brands'}
          {if $filter->getType() == 1}
            {if $filter->getMinValue() < $filter->getMaxValue()}
              <div class="filter slider {if $filter->getSelectRangeMin() > $filter->getMinValue() || $filter->getSelectRangeMax() < $filter->getMaxValue()}expanded changed{/if}" data-name="{$filter->getId()}">
                <h4>{$filter->getTitle()}{if $filter->getUnit()}, {$filter->getUnit()} {/if}</h4>
                <div class="range">
                  <input type="range" min="{$filter->getMinValue()}" max="{$filter->getMaxValue()}" value="{$filter->getSelectRangeMin()},{$filter->getSelectRangeMax()}">
                </div>
              </div>
            {/if}
          {else}
            <div class="filter selector" data-name="{$filter->getId()}">
              {if $filter->getType() != 2 }
                <h4>{$filter->getTitle()}</h4>
              {/if}
              <ul class="options">
                {foreach from=$filter->enumNonEmptyValues() item='filterValue' name='filterValues'}
                  <li {if $filterValue->isSelected()}class="selected"{/if} data-value="{$filterValue->getId()}">{$filterValue->getTitle()}</li>
                {/foreach}
              </ul>
            </div>
          {/if}
        {/if}
      {/if}
    {/foreach}

    {foreach from=$simpleFilters item='filter'}
      {assign var='isHidden' value=false}
      {if !$filter->getMain()}
        {assign var='isHidden' value=true}
      {/if}
      {if $isHidden}
        {if !$filter->isEmpty() && ($filter->getId() != 'brands') && (1 != $filter->getType())}
          {assign var='hasHiddenFilters' value=true}
          <div class="filter selector default-hidden" data-name="{$filter->getId()}">
            {if $filter->getType() != 2 }
              <h4>{$filter->getTitle()}</h4>
            {/if}
            <ul class="options">
              {foreach from=$filter->enumNonEmptyValues() item='filterValue' name='filterValues'}
                <li {if $filterValue->isSelected()}class="selected"{/if} data-value="{$filterValue->getId()}">{$filterValue->getTitle()}</li>
              {/foreach}
            </ul>
          </div>
        {/if}
      {/if}
    {/foreach}

    {if $hasHiddenFilters}
      <div class="show-all-filters-button">
        <a href="#">Еще фильтры</a>
      </div>
    {/if}

  {/if}
  {if $banner}
  <div class="banner">
    {if $banner['url']}<a href="{$banner['url']}">{/if}
      <img src="{$banner['image']}" alt="">
    {if $banner['url']}</a>{/if}
  </div>
  {/if}

  {if !$categories && (0 < count($sets))}
    <div class="selections-info">
      {assign var='titleShown' value=false}
      {foreach from=$setsGroups item='setsGroup'}
        {if !$setsGroup.IsMain}
          {if !$titleShown}
            <h2>{$setsTitle}</h2>
            {assign var='titleShown' value=true}
          {/if}
          <ul>
            {assign var='replace' value=''}
            {foreach from=$sets item='set'}
              {if $set.SetGroupID == $setsGroup.SetGroupID}
                {if isset($currentSets[$set['SetID']])}
                  {assign var='replace' value=$set['SetUrlPart']}
                {/if}
              {/if}
            {/foreach}
            {foreach from=$sets item='set'}
              {if $set.SetGroupID == $setsGroup.SetGroupID}
                {assign var='isSelectedSet' value=false}
                {if isset($currentSets[$set['SetID']])}
                  {assign var='isSelectedSet' value=true}
                {/if}
                <li>
                  {if !$isSelectedSet}
                    {assign var='setUrl' value=$setBaseUrl|cat:'-'|cat:$set['SetUrlPart']|cat:'/'}
                    {if false !== stripos($setBaseUrl, $replace)}
                      {assign var='setUrl' value=$setBaseUrl|replace:$replace:$set['SetUrlPart']|cat:'/'}
                    {/if}
                    <a href="{$setUrl}">
                      {$set.SetName}
                    </a>
                  {/if}
                </li>
              {/if}
            {/foreach}
          </ul>
        {/if}
      {/foreach}
    </div>
  {/if}
</aside>

<section class="catalogue-part cat-pag">
  {if !$categories && (0 < count($sets))}
    <div class="selections">
      {foreach from=$setsGroups item='setsGroup'}
        {if $setsGroup.IsMain}
          <ul>
            {assign var='replace' value=''}
            {foreach from=$sets item='set'}
              {if $set.SetGroupID == $setsGroup.SetGroupID}
                {if isset($currentSets[$set['SetID']])}
                  {assign var='replace' value=$set['SetUrlPart']}
                {/if}
              {/if}
            {/foreach}
            {foreach from=$sets item='set'}
              {if $set.SetGroupID == $setsGroup.SetGroupID}
                {assign var='isSelectedSet' value=false}
                {if isset($currentSets[$set['SetID']])}
                  {assign var='isSelectedSet' value=true}
                {/if}
                <li>
                  {if !$isSelectedSet}
                    {assign var='setUrl' value=$setBaseUrl|cat:'-'|cat:$set['SetUrlPart']|cat:'/'}
                    {if false !== stripos($setBaseUrl, $replace)}
                      {assign var='setUrl' value=$setBaseUrl|replace:$replace:$set['SetUrlPart']|cat:'/'}
                    {/if}
                    <a href="{$setUrl}">
                      {$set.SetName}
                    </a>
                  {/if}
                </li>
              {/if}
            {/foreach}
          </ul>
        {/if}
      {/foreach}
    </div>
  {/if}

  {if count($goods)}
    <div class="pages">
      <div class="order">
        Сначала
        <div class="order-list">
          <a href="#" class="order-expander">
            {if 'avail' == $sorting.uid}
              по наличию
            {elseif 'price' == $sorting.uid}
              {if 'asc' == $sorting.direction}подешевле{else}подороже{/if}
            {/if}
          </a>
          <ul>
            {assign var='correctUrl' value=$urlRootPath|cat:$selectedFilters->toQueryString(array('limit'))|cat:$querySearch|cat:'&sort=avail&dir=desc'|clear_question_mark}
            <li><a href="{$correctUrl}">по наличию</a></li>
            {assign var='correctUrl' value=$urlRootPath|cat:$selectedFilters->toQueryString(array('limit'))|cat:$querySearch|cat:'&sort=price&dir=asc'|clear_question_mark}
            <li><a href="{$correctUrl}">подешевле</a></li>
            {assign var='correctUrl' value=$urlRootPath|cat:$selectedFilters->toQueryString(array('limit'))|cat:$querySearch|cat:'&sort=price&dir=desc'|clear_question_mark}
            <li><a href="{$correctUrl}">подороже</a></li>
          </ul>
        </div>
      </div>

      {if isset($pagination) && (1 < $pagination.last)}
        <div class="page-list">
          {if !is_null(($pagination.pages[$pagination.previous]))}
            {assign var='correctUrl' value=$urlRootPath|cat:$pagination.pages[$pagination.previous].url|cat:'?'|cat:$querySorting|cat:$querySearch|clear_question_mark}
          {else}
            {assign var='correctUrl' value=$urlRootPath|cat:$pagination.pages[$pagination.current].url|cat:'?'|cat:$querySorting|cat:$querySearch|clear_question_mark}
          {/if}
          {assign var='correctUrl' value=$correctUrl|replace:'?&':'?'}
          <a href="{$correctUrl}" class="page-prev"{if 1 == $pagination.current} style="visibility: hidden;"{/if}></a>
          <ul>
            {foreach from=$pagination.pages key='page' item='pageItem'}
              {if 'delimiter' != $pageItem}
                {if $page == $pagination.current}
                  <li class="current"><a>{$page}</a></li>
                {else}
                  {assign var='correctUrl' value=$urlRootPath|cat:$pageItem.url|cat:'?'|cat:$querySorting|cat:$querySearch|clear_question_mark}
                  {assign var='correctUrl' value=$correctUrl|replace:'?&':'?'}
                  {assign var='correctUrl' value=$correctUrl|regex_replace:'/page=([0-9]+)\?/':'page=$1&'}
                  <li><a href="{$correctUrl}">{$page}</a></li>
                {/if}
              {else}
                <li class="ellipsis">...</li>
              {/if}
            {/foreach}
          </ul>
          {if !is_null(($pagination.pages[$pagination.next]))}
            {assign var='correctUrl' value=$urlRootPath|cat:$pagination.pages[$pagination.next].url|cat:'?'|cat:$querySorting|cat:$querySearch|clear_question_mark}
          {else}
            {assign var='correctUrl' value=$urlRootPath|cat:$pagination.pages[$pagination.current].url|cat:'?'|cat:$querySorting|cat:$querySearch|clear_question_mark}
          {/if}
          {assign var='correctUrl' value=$correctUrl|replace:'?&':'?'}
          <a href="{$correctUrl}" class="page-next"{if is_null(($pagination.pages[$pagination.next]))} style="visibility: hidden;"{/if}></a>
        </div>
      {/if}
    </div>

    <ul class="goods">
      {foreach from=$goods item='good' name='goods'}
        {assign var='hasOldPrice' value=false}
        {if $good->getPrice('prev') && ($good->getPrice('retail') < $good->getPrice('prev'))}
          {assign var='hasOldPrice' value=true}
        {/if}

        <li class="{if $good->Compared}comparing{/if}{if $good->isPackage()} package{/if}">
          <a href="{$good->getUrl()}{if isset($smarty.get.f)}?f={$smarty.get.f}{/if}" class="images-wrapper product-link" data-id="{$good->getId()}" data-name="{$good->getName()}"{if $good->getCategoryName()} data-category="{$good->getCategoryName()}"{/if}{if $good->getBrandName()} data-brand="{$good->getBrandName()}"{/if} data-price="{$good->getPrice('retail')|intval}" data-action="ProductList" data-position="{$smarty.foreach.goods.iteration}">
            <div class="images">
              {foreach from=$goodsImages[$good->getId()] item="image"}
                <div class="image" style="background-image: url('{$absHost}/img/260x280/{$image['MiniatureUrl']}')"></div>
                {foreachelse}
                <div class="image no-photo" style="background-image: url('/i/nophoto.png');"></div>
              {/foreach}
            </div>
          </a>
          <a class="title" href="{$good->getUrl()}{if isset($smarty.get.f)}?f={$smarty.get.f}{/if}">{$good->getName()}</a>

					{assign var='pickupDate' value=$good->additional('pickupDate')}
					{if $pickupDate && ($dateHelper->isToday($pickupDate) || $dateHelper->isTomorrow($pickupDate)) && !$good->onlyInShowroom()}
						<div class="delivery now">
              {if $isDefaultCity}
                можно забрать {$dateHelper->getPickupDateDescription($pickupDate)}
              {else}
                отгрузим сегодня
              {/if}
            </div>
          {elseif ($good->additional('deliveryDate'))}
            <div class="delivery later">
              {if true}
                отгрузим {$dateHelper->getOutcomeShortDateText($good->additional('deliveryDate'))}
              {else}
                срок поставки {$dateHelper->getDeliveryDateRange($good->additional('deliveryDate'))}
              {/if}
            </div>
					{/if}

          <div class="price{if $hasOldPrice} discounted{/if}">
            <strong>
              {$good->getPrice('retail')|format_price} <span class="rub">р</span>
            </strong>
            {if $hasOldPrice}
              <strong class="old">
                {$good->getPrice('prev')|format_price} <span class="rub">р</span>
              </strong>
            {/if}
            {if !$good->onlyInShowroom() || $smarty.const.MANAGER_MODE}
              <a href="#" class="add-to-basket" data-id="{$good->getId()}"{if $smarty.const.MANAGER_MODE && $good->onlyInShowroom()} data-only-showroom="1"{/if}{if $smarty.const.MANAGER_MODE && ($goodsRemainsInTheMainStore[$good->getId()] > 0) && $isDefaultCity} data-showroom="1"{/if}{if $smarty.const.MANAGER_MODE && ($good->additional('arrivalDate'))} data-has-suborder="1"{/if}></a>
            {/if}
          </div>
          {if ($goodsRemainsInTheMainStore[$good->getId()] > 0) && $isDefaultCity}
						{assign var='showRoomData' value=$good->getShowRoomData()}
            <div class="action show-room not-mobile"{if $showRoomData} data-placeinfo="<p>ряд:<b>{$showRoomData.row}</b></p><p>место:<b>{$showRoomData.cell}</b></p>{/if}">
              {if $isWorkTime}сегодня {/if}можно посмотреть<br>
              в торговом зале
            </div>
            <div class="action show-room mobile" data-placeinfo="<p>ряд:<b>{$showRoomData.row}</b></p><p>место:<b>{$showRoomData.cell}</b></p>">
              есть в шоу-руме
            </div>
          {/if}
        </li>
      {/foreach}
    </ul>
  {/if}
</section>

{if isset($pagination) && (1 < $pagination.last) && count($goods)}
  <section class="cat-pag catalogue-pages-bottom">
    <div class="pages">
      <div class="page-list">
        {if !is_null(($pagination.pages[$pagination.previous]))}
          {assign var='correctUrl' value=$urlRootPath|cat:$pagination.pages[$pagination.previous].url|cat:'?'|cat:$querySorting|cat:$querySearch|clear_question_mark}
        {else}
          {assign var='correctUrl' value=$urlRootPath|cat:$pagination.pages[$pagination.current].url|cat:'?'|cat:$querySorting|cat:$querySearch|clear_question_mark}
        {/if}
        {assign var='correctUrl' value=$correctUrl|replace:'?&':'?'}
        <a href="{$correctUrl}" class="page-prev"></a>
        <ul>
          {foreach from=$pagination.pages key='page' item='pageItem'}
            {if 'delimiter' != $pageItem}
              {if $page == $pagination.current}
                <li class="current"><a>{$page}</a></li>
              {else}
                {assign var='correctUrl' value=$urlRootPath|cat:$pageItem.url|cat:'?'|cat:$querySorting|cat:$querySearch|clear_question_mark}
                {assign var='correctUrl' value=$correctUrl|replace:'?&':'?'}
                {assign var='correctUrl' value=$correctUrl|regex_replace:'/page=([0-9]+)\?/':'page=$1&'}
                <li><a href="{$correctUrl}">{$page}</a></li>
              {/if}
            {else}
              <li class="ellipsis">...</li>
            {/if}
          {/foreach}
        </ul>
        {if !is_null(($pagination.pages[$pagination.next]))}
          {assign var='correctUrl' value=$urlRootPath|cat:$pagination.pages[$pagination.next].url|cat:'?'|cat:$querySorting|cat:$querySearch|clear_question_mark}
        {else}
          {assign var='correctUrl' value=$urlRootPath|cat:$pagination.pages[$pagination.current].url|cat:'?'|cat:$querySorting|cat:$querySearch|clear_question_mark}
        {/if}
        {assign var='correctUrl' value=$correctUrl|replace:'?&':'?'}
        <a href="{$correctUrl}" class="page-next"></a>
      </div>
    </div>
  </section>
{/if}

{if (!empty($category->Description))}
<section class="seo-text">
  {$category->Description}
</section>
{elseif false && $category && (33 == $category.SoftCategoryID)}
  <section class="seo-text">
    <h3>Душевые кабины – это удобно и полезно</h3>
    <br />
    Широкое распространение в российских частных домах и квартирах душевые кабины получили не так давно, и, тем не менее, все чаще, планируя ремонт в квартире, люди задумываются о преимуществах душевых кабин, а строя особняки, устанавливают в ванных комнатах одновременно и ванну, и душевую кабину.<br />
    <br />
    Первое преимущество, что приходит в голову – экономия места в ванной комнате. Компактность этого устройства (<a href="www.yandex.ru">80х80см</a>, 90х90 см, 100х100см и более), позволяет устанавливать душевые кабины там, где не помещается ванна.
  </section>
{else}
  {if $bannerBottom}
    <div class="banner btm">
      {if $banner['url']}<a href="{$banner['url']}">{/if}
      <img src="{$banner['image']}" alt="">
    {if $banner['url']}</a>{/if}
    </div>
  {/if}
{/if}