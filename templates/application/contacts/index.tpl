{assign var='calltouchCode' value='495'}
{if $userRegion}
  {if $userRegion->getId() == 42}
    {assign var='calltouchCode' value='495'}
  {else}
    {assign var='calltouchCode' value='812'}
  {/if}
{/if}

<h1><span class="nomobile">Аквасант – </span>контактная информация</h1>

<div class="phones call_phone_{$calltouchCode}_3">
  <address><a href="tel:+74955405152">+7-495-540-51-52</a></address>
  <address class="with-8"><a href="tel:88005553055">8-800-555-30-55</a></address>
</div>

<div class="text">
  Москва, МКАД, 14-й километр (внутренняя сторона), дом 10,<br>
  СК «Восточные ворота», вход 9а<br>
  <br>
  GPS: 55.650643, 37.830976<br>
  <br>
  Метро: Люблино, Марьино, Выхино, Жулебино, Алма-Атинская<br>
  <br>
  Часы работы: с 09:00 до 20:00 без выходных.<br>
  Въезд и вход осуществляется через КПП, прихватите с собой документ.
</div>

<div id="map"></div>

<h2 id="way-to-warehouse">Как проехать на базу</h2>

<img src="/images/way.png" class="path">
<img src="/images/warehouse.jpg">

<form id="feedback" method="get" action="./">
  <input type="hidden" name="action" value="feedback">
  <input type="hidden" name="send-to" id="send-to" value="">
  <h3>Обратная связь</h3>
  <div class="wrapper">
    <nav class="selector">
      <a href="#" class="expander">Кому написать</a>
      <ul>
        <li data-value="retail">Розница</li>
        <li data-value="wholesale">Опт</li>
        <li data-value="purchase">Закупки</li>
        <li data-value="development">Развитие</li>
        <li data-value="questions">Вопросы</li>
      </ul>
    </nav>
    <aside>
      <div class="field"><label for="field-message">Сообщение</label>
        <textarea name="message" id="field-message"></textarea>
      </div>
      <div class="field"><label for="field-name">Ваше имя</label>
        <input name="username" id="field-name" type="text">
      </div>
      <div class="field"><label for="field-email">Электронная почта</label>
        <input name="email" id="field-email" type="email">
      </div>

      <input type="submit" value="Отправить" disabled>
      <div class="error-message"></div>
    </aside>
  </div>
</form>
