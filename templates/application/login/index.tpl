<h1>Вход на сайт</h1>

<form id="login-form">
  <div class="field"><label for="field-login">Мобильный телефон или e-mail</label>
    <div class="password-was-sent">Пароль выслан, проверяйте входящие</div>
    <input type="text" name="login" id="field-login" value="">
  </div>
  <div class="field"><label for="field-password">Пароль</label>
    <input type="password" name="password" id="field-password" value="">
  </div>
  <input type="submit" value="Войти" disabled>
  <div class="error-message">kdoe</div>
  <div class="forgot">
    <a href="#" class="forgot-button">забыли пароль?</a>
  </div>
</form>