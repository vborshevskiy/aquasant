<h1>сравнение</h1>

<aside class="params">
  <div class="filters">
    <ul>
      <li class="all selected"><span>все характеристики</span></li>
      <li class="diff"><span>только различия</span></li>
    </ul>
  </div>
  <ul>
    {foreach from=$comparisonInfo.goodsDescriptions.propertiesList item='isSame' key='property'}
      <li {if $isSame.goods == $comparisonInfo.count} class="nd"{/if}>{$property}</li>
    {/foreach}
  </ul>
</aside><section class="items">
  {foreach from=$comparisonInfo.goods item='good' key='goodId'}<div class="compared-item" data-id="{$goodId}">
      <div class="description">
        <a href='#' class='delete'>убрать</a>
        <div class="image"><img src="{if $images[$goodId]}/img/260x280/{$images[$goodId]['MiniatureUrl']}{else}/i/nophoto.png{/if}" height="120"></div> <!-- Указание высоты изображения обязательно -->
        <div class="title"><a href="{$category['CategoryUID']}/{$goodId}_{$good['GoodEngName']}">{$good->GoodName}</a></div>
        <div class='price'>
          <strong>{$prices[$goodId]|format_price} <span class='rub'>р</span></strong>
          <a href='#' class='add-to-basket' data-id="{$goodId}"></a>
        </div>
      </div>
      <ul>
        {foreach from=$comparisonInfo.goodsDescriptions.propertiesList item='isSame' key='property'}
          <li>{$comparisonInfo.goodsDescriptions.goods[$goodId][$property]}</li>
        {/foreach}
      </ul>
    </div>{/foreach}
</section>