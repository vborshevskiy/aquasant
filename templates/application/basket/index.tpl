<div class="basket-header">
  <h1>в корзине </h1>
  <h2><span class="total-count">{$itemsCount}</span> товар<span class='total-suffix'>{$itemsCount|wordending:'':'а':'ов'}</span> на <span class="amount">{$totalAmount|format_price}</span> <span class="rub">р</span></h2>
  <div class="order-button">
    <a href="/order/">Оформить заказ</a>
  </div>
</div>

<ul class="basket-items">
  {foreach from=$basketGoods item='good'}
    {assign var='avail' value=$avails[$good->getId()]}
    {assign var='product' value=$goods[$good->getId()]}
    {assign var='inShowroom' value=false}
    {assign var='onlyInShowroom' value=false}
    {if (0 < $avail['WindowQuantity'])}
      {assign var='inShowroom' value=true}
    {/if}
    {if (0 < $avail['WindowQuantity']) && ($avail['AvailQuantity'] == $avail['WindowQuantity']) && (0 == $avail['SuborderQuantity'])}
      {assign var='onlyInShowroom' value=true}
    {/if}
    {assign var='isShowcaseSale' value=false}

    <li data-id="{$good->getId()}">
      <div class="image"><img src="{if $goodsMiniatures[$good->getId()]}{$absHost}/img/260x280/{$goodsMiniatures[$good->getId()]['MiniatureUrl']}{else}/i/nophoto.png{/if}"></div>
      <div class="title">
        <a href="/{$product->CategoryUID}/{$good->getId()}_{$product->GoodEngName}">{$product->GoodName}</a>
        <span class="article">артикул {$good->getId()}</span>
        {if false}
          {if ($avail['AvailQuantity'] > 0)}
            <span class="availability available">
              {if $isDefaultCity && $inShowroom && $isShowcaseSale}
                можно забрать сейчас
              {elseif $isDefaultCity && ($avail['AvailQuantity'] > 0) && !$inShowroom}
                можно забрать сейчас
              {else}
                отгрузим сегодня
              {/if}
            </span>
          {else}
            {if $avail['ArrivalDate']}
              <span class="availability later">
                {if true}
                  отгрузим {$dateHelper->getOutcomeShortDateText($avail['ArrivalDate'])}
                {else}
                  срок поставки {$dateHelper->getDeliveryDateRange($avail['ArrivalDate'])}
                {/if}
              </span>
            {else}
              <span>&nbsp;</span>
            {/if}
          {/if}
        {/if}
      </div>
      <div class="price" data-price="{$good->getPrice()}">
        <div class="count">
          {if (isset($avail['TotalQuantity']))}
            {assign var = 'totalQuantity' value = intval($avail['TotalQuantity'])}
          {else}
            {assign var = 'totalQuantity' value = 0}
          {/if}
          <input type="text" data-item="{$good->getId()}" maxlength="2" value="{$good->getQuantity()}" min="1" max="{if $totalQuantity > $good->getQuantity()}{$totalQuantity}{else}{$good->getQuantity()}{/if}">
        </div>
        <div class="amount">
          <strong><span>{$good->getAmount()|format_price}</span> <span class="rub">р</span></strong>
          <a href="#" class="delete">удалить</a>
        </div>
      </div>
    </li>
  {/foreach}
</ul>
<div class="basket-header footer">
  <h2>Итого: <strong>{$totalAmount|format_price}</strong> <span class="rub">р</span></h2>
  <div class="order-button">
    <a href="/order/">Оформить заказ</a>
  </div>
</div>


</main>
