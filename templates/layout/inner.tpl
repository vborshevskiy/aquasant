{* Smarty *}<!DOCTYPE html>
<html {if isset($htmlClass) && !empty($htmlClass)} class="{$htmlClass}"{/if}>
{include file="./partial/head.tpl"}
<body {if isset($bodyClass) && !empty($bodyClass)} class="{$bodyClass} {if $smarty.get.precheck == 1}-pre-check-basket{/if}"{/if}>
{if $ecommerceTracker}
  {include file='./ga.helper.tpl' tracker=$ecommerceTracker}
{/if}
{literal}<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MHXHNZZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->{/literal}
{include file='./partial/calltouch.tpl'}

<header>
  <section class="top-part">
    <div class="region-selector x-selector">
      <strong>{if $userRegion}{$userRegion->getName()}{else}Доставка по России{/if}</strong>
      <ul>
        <li><a href='/city/42/'>Москва</a></li>
        <li><a href='/city/59/'>Санкт-Петербург</a></li>
        <li><a href='/city/0'>Доставка по России</a></li>
      </ul>
    </div>

    {if $userRegion}
      {if $userRegion->getId() == 42}
        <address class="warehouse"><span class="only-print">Москва, </span><a href="/contacts/">14-й километр МКАД (внутренняя сторона), дом 10</a></address>
        <address class="warehouse mobile"><a href="/contacts/">Адрес базы</a></address>
        <address class="main-phone">
          <span>
            <a href="tel:+74955405152">+7-495-540-51-52</a>
          </span>
          <span class="only-print s-ph">8-800-555-30-55</span>
          <span class="only-print s-ph">www.{$smarty.const.DOMAIN_NAME}</span>
        </address>
      {else}
        <address class="warehouse">с московского оптового склада сантехники</address>
        <address class="main-phone">
          <span>
            <a href="tel:+78123132727">+7-812-313-27-27</a>
          </span>
          <span class="only-print s-ph">8-800-555-30-55</span>
          <span class="only-print s-ph">www.{$smarty.const.DOMAIN_NAME}</span>
        </address>
      {/if}
    {else}
      <address class="warehouse">с московского оптового склада сантехники</address>
      <address class="main-phone">
        <a href="tel:88005553055">8-800-555-30-55</a>
        <span class="only-print s-ph">8-800-555-30-55</span>
        <span class="only-print s-ph">www.{$smarty.const.DOMAIN_NAME}</span>
      </address>
    {/if}

    <div class='top-menu-expander'>
      <ul class="top-menu">
        <li><a href="/about/">База «Аквасант»</a></li>
        <li><a href="/delivery/">Доставка</a></li>
        <li><a href="/payments/">Оплата</a></li>
        <li><a href="/refund/">Возврат</a></li>
        <li><a href="/install/">Сборка</a></li>
        <li><a href="/contacts/">Контакты</a></li>
      </ul>
    </div>
  </section>

  <section class="start">
    <div class="holder">
      <div style="background-image: url('{$slideImage}');" class="-bg current -first"></div>
    </div>
    <div id="logo"><a href="/">aquasant</a></div>
    <form method="get" action="/search/" id="search-top">
      <input type="text" name="q" placeholder="поиск по оптово-розничной базе" autocomplete="off">
      {if isset($isPolygon) && (true === $isPolygon)}
        <input type="hidden" name="polygon" value="1" />
      {/if}
      <input type="submit" value="">
    </form>

    <div class="slogan">
      <div>оптово-розничная база сантехники</div>
      <p>тороговый выставочный зал 1500м<sup>2</sup>, работаем каждый день с 9 до 20</p>
    </div>

    <nav class="catalogue" style="visibility: hidden;">
      <strong>Каталог<em> сантехники</em></strong>
      {assign var = 'menuSplitOnColumns' value = array_chunk($menu->getItems(), ceil(count($menu->getItems()) / 3))}

      <div class="items">
        {foreach from = $menuSplitOnColumns item = 'menu'}
          <ul>
            {foreach from = $menu item = 'menuRoot'}
              <li class="group"><a href="{if !$menuRoot->hasItems()}{$menuRoot->getUrl()}{/if}">{$menuRoot->getName()}</a>
                {if ($menuRoot->hasItems())}
                  <ul>
                    {foreach from = $menuRoot->getItems() item = 'item'}
                      <li><a href="{$item->getUrl()}">{$item->getName()} <em>({$item->getGoodsCount()})</em></a></li>
                    {/foreach}
                  </ul>
                {/if}
              </li>
            {/foreach}
          </ul>
        {/foreach}
        {*{if $menuBanner}*}
        {*<div class="menu-banner" style="background-image: url('{$menuBanner['image']}');">*}
          {*{if $menuBanner['url']}<a href="#banner"></a>{/if}*}
        {*</div>*}
        {*{/if}*}
      </div>
    </nav>

  </section>
</header>

<main>{$content}</main>

{include file="./partial/footer.tpl"}

</body>
</html>