<!DOCTYPE html>
<html lang="ru">
<head>
  <title>Страница не найдена</title>
  <meta name="viewport" content="width=400,user-scalable=no">
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta charset="UTF-8">

  <link rel="stylesheet" href="/css/reset.css">
  <link rel="stylesheet" href="/css/fonts.css">
  <link rel="stylesheet" href="/css/errors.css">
</head>

<body class="e404">
{$content}
</body>
</html>