{* Smarty *}
{strip}

<div id="ga-ecommerce-data" data-page-type="{$tracker->getPageType()}" {foreach from=$tracker->getAttributes() key='attrName' item='attrValue'} data-{$attrName}="{$attrValue|escape}"{/foreach}{if $tracker->orderInfo()->getId()} data-order-id="{$tracker->orderInfo()->getId()}" data-order-revenue="{$tracker->orderInfo()->getRevenue()}" data-order-tax="{$tracker->orderInfo()->getTax()}" data-order-shipping="{$tracker->orderInfo()->getShipping()}" data-order-coupon="{$tracker->orderInfo()->getCoupon()|escape}" data-purchase-type="{$tracker->orderInfo()->getPurchaseType()}"{/if}>
{if $tracker->hasImpressions()}
  {foreach from=$tracker->getImpressions() item='impression'}
    <div class="impression" data-id="{$impression->getId()}" data-name="{$impression->getName()|escape}" data-category="{$impression->getCategoryName()|escape}" data-brand="{$impression->getBrandName()}" data-price="{$impression->getPrice()}" data-position="{$impression->getPosition()}" data-list="{$impression->getListName()|escape}"></div>
  {/foreach}
{/if}
{if $tracker->hasBasketProducts()}
  {foreach from=$tracker->getBasketProducts() item='product'}
    <div class="basket-product" data-id="{$product->getId()}" data-name="{$product->getName()|escape}" data-category="{$product->getCategoryName()|escape}" data-brand="{$product->getBrandName()}" data-price="{$product->getPrice()}" data-position="{$product->getPosition()}" data-list="{$product->getListName()|escape}" data-quantity="{$product->getQuantity()}"></div>
  {/foreach}
{/if}
{if $tracker->hasOrderProducts()}
  {foreach from=$tracker->getOrderProducts() item='product'}
    <div class="order-product" data-id="{$product->getId()}" data-name="{$product->getName()|escape}" data-category="{$product->getCategoryName()|escape}" data-brand="{$product->getBrandName()}" data-price="{$product->getPrice()}" data-position="{$product->getPosition()}" data-list="{$product->getListName()|escape}" data-quantity="{$product->getQuantity()}"></div>
  {/foreach}
{/if}
{if $tracker->hasPromoActions()}
  {foreach from=$tracker->getPromoActions() item='action'}
    <div class="promo-action" data-id="{$action->getId()}" data-name="{$action->getName()|escape}" data-creative="{$action->getCreative()}" data-position="{$action->getPosition()}"></div>
  {/foreach}
{/if}
</div>

{/strip}