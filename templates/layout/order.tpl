{* Smarty *}<!DOCTYPE html>
<html {if isset($htmlClass) && !empty($htmlClass)} class="{$htmlClass}"{/if}>
{include file="./partial/head.tpl"}
<body {if isset($bodyClass) && !empty($bodyClass)} class="{$bodyClass}"{/if}>
{if $ecommerceTracker}
  {include file='./ga.helper.tpl' tracker=$ecommerceTracker}
{/if}
<header {if $ordersCount > 1}class="partial"{/if}>
  <a href="/" class="logo"></a>
    {if $ordersCount > 1}
    <div class="order-info-partial">
        <div class="-info">
            <strong>Заказ {$currentOrderNumber} из {$ordersCount}</strong>
            <p>сумма {$orderAmount|format_price} <span class="rub">р</span></p>
            <p>{if $currentDateTimestamp == $arrivalDateTimestamp}в наличии <em>сегодня</em>{else}срок поставки <em>{$dateHelper->parseDate(2, $arrivalDateTimestamp)}</em>{/if}</p>
        </div>
        <div class="-items">
            <ul>
                {foreach from=$orderGoods item="orderGood"}
                <li title="{$goods[$orderGood->getMarking()]['GoodName']}"><b>{$orderGood->getQuantity()} шт</b><img src='{if isset($goodsImages[$orderGood->getMarking()])}/img/260x280/{$goodsImages[$orderGood->getMarking()]['MiniatureUrl']}{else}/i/nophoto.png{/if}' alt=''></li>
                {/foreach}
            </ul>
        </div>
    </div>
    {else}
        <div class="order-info">
            <strong>Заказ на <span>{$basketTotalAmount|format_price}</span> <span class="rub">р</span></strong>
            <a href="/basket">Вернуться в корзину</a>
        </div>
    {/if}

</header>

<main>{$content}</main>

<footer>
    <div class="go-to-shop"><a href="/basket">Вернуться в корзину</a></div>
    <p class="rights">© 2014−{date("Y")}
        <span class="nomobile">Оптово-розничная база сантехники «Аквасант» — вся сантехника в&nbsp;одном месте</span>
        <span class="onlymobile">Аквасант<br>Оптово-розничная база сантехники</span>
    </p>
</footer>
{include file="./partial/footer_bottom.tpl"}

</body>
</html>