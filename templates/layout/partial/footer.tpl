<div id="confirm-basket-add">
    <div class='bg'>
        <p>Товар есть на витрине.<br />Продаем выставочный образец?</p>
        <div class='buttons-set'>
            <a href='#' class='y'>Да, продаем</a>
            <a href='#' class='n'>Нет!</a>
        </div>
    </div>
</div>
<footer>
  <ul class="bottom-menu">
    <li><a href="/about/">База Аквасант</a></li>
    <li><a href="/delivery/">Доставка</a></li>
    <li><a href="/payments/">Оплата</a></li>
    <li><a href="/refund/">Возврат сантехники</a></li>
    <li><a href="/install/">Сборка и подключение</a></li>
    <li><a href="/contacts/">Контакты</a></li>
  </ul>
  <p class="rights">© 2014−{date("Y")} Оптово-розничная база сантехники «Аквасант» — вся сантехника в&nbsp;одном месте<br />
  <span class="fta"></span></p>
  <p class="terms">
    {if false}
      Данный интернет-сайт носит исключительно информационный характер и ни при каких условиях<br class="nomobile"> не является публичной офертой, определяемой положениями <nobr>статьи 437 (2) ГК РФ.</nobr>
      <br />
    {/if}
    <a href="/agreement/">Политика конфиденциальности</a>.
  </p>
  <div class="bottom-contacts">
    <div class="address">
      <address>Москва, 14-й километр МКАД <br class="only-print">(внутренняя сторона), дом 10</address>
      <a href="//yandex.ru/maps/-/CBQfIWx30B" target="_blank">показать на карте</a><span class="no-print nomobile">, </span>GPS: 55.650643, 37.830976
    </div>
    {assign var='calltouchCode' value='495'}
    {if $userRegion}
      {if $userRegion->getId() == 42}
        {assign var='calltouchCode' value='495'}
      {else}
        {assign var='calltouchCode' value='812'}
      {/if}
    {/if}
    <div class="phones call_phone_{$calltouchCode}_2">
      <address>8 (800) 555-30-55</address>
      <address>+7 (495) 540-51-52</address>
    </div>
    <div class="only-print web"></div>
  </div>
</footer>
{include file="./footer_bottom.tpl"}
<div class="only-print" id="page-qr"></div>