<div class="nav-bottom-panel">
  <!-- <a href="#" class="scroll-top"></a> -->

   <span class="user-number">{$clientCode}</span>

  <div class="compare {if (!$comparison['totalCount'])}empty{/if}">
    {if ($comparison['totalCount'])}
      <span>Сравнить <b>({$comparison['totalCount']})</b></span>
      <div class="popup">
        <ul>
          {foreach from=$comparison['groups'] item="group"}
            <li data-id='{$group['id']}'><a href='{$group['link']}'>{$group['title']} ({$group['count']})</a><a href="#" class="del"></a></li>
          {/foreach}
      </div>
    {/if}
  </div>

  {if empty($user)}
    <div class="login unauth">
      <a href="/login/">Войти с паролем</span></a>
    </div>
  {else}
    <div class="login auth">
      <a href="/cabinet/">
        {if $user->getEmail()}{$user->getEmail()}{else}{$this->formatPhone($user->getPhone(), '+7{code}{number}')}{/if}
        <span class='failed-msg'>есть проблемы: <em>0</em></span>
      </a>
    </div>
  {/if}
  {if $basketItemsCount}
    <div class="basket">
      <a href="/basket/"><strong>{$basketTotalAmount|format_price}</strong> <span class="rub"> р</span></a>
    </div>
  {else}
    <div class="basket empty">
      <a href="">пусто</a>
    </div>
  {/if}
</div>
{if false}
  <div id="site-is-under-construction">
    <p>
      Сайт еще в разработке — заказы не обрабатываются, цены и наличие к реальности отношения не имеют. Пока.<br>
      Пока можно купить сантехнику онлайн на сайте <a href="http://www.aquasant.ru">www.aquasant.ru</a><a href='#' class='close-block'></a></p>
  </div>
{/if}
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

{if isset($javascriptFiles.simple)}
  {foreach from=$javascriptFiles.simple item='files' key='folderName'}
    {if $layoutSettings.javascriptFilesPerRequest >= sizeof($files)}
      <script type="text/javascript" src="/min/?b={$folderName}&f={implode pieces=$files glue=','}{if $debug}&debug=1{/if}"></script>
    {else}
      {strip}<script type="text/javascript" src="/min/?b={$folderName}&f=
            {foreach from=$files item='fileName' name='files'}
              {$fileName}
              {if !$smarty.foreach.files.last}
                {if 0 != ($smarty.foreach.files.iteration % $layoutSettings.javascriptFilesPerRequest)},{else}
                  {if $debug}&debug=1{/if}"></script><script type="text/javascript" src="/min/?b={$folderName}&f=
                {/if}
              {/if}
            {/foreach}
            {if $debug}&debug=1{/if}"></script>
      {/strip}
    {/if}
  {/foreach}
{/if}

{literal}
<!-- Yandex.Metrika counter --> <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(51476554, "init", { id:51476554, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); </script> <noscript><div><img src="https://mc.yandex.ru/watch/51476554" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
{/literal}