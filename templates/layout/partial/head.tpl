<head>
  <title>{$meta.title}</title>
  <meta name="viewport" content="width=400,user-scalable=no">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="pragma" content="no-cache">
  <meta http-equiv="imagetoolbar" content="no">
  <link rel="icon" href="/i/favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" href="/i/favicon.ico" type="image/x-icon" />

  {if !empty($meta.keywords)}
    <meta name="keywords" content="{implode pieces=$meta.keywords glue=' '}">
  {else}
    <meta name="keywords" content="{str_replace(' ',', ',$meta.title)}">
  {/if}

  {if !empty($meta.description)}
    <meta name="description" content="{implode pieces=$meta.description glue='. '}">
  {else}
    <meta name="description" content="{$meta.title} {$randomSeoWord}">
  {/if}

  <!-- jQuery UI 1.11.4 -->
  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

  <link href="https://cdn.jsdelivr.net/npm/suggestions-jquery@18.3.3/dist/css/suggestions.min.css" type="text/css" rel="stylesheet" />
  <!--[if lt IE 10]>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
  <![endif]-->

  {if isset($stylesheetFiles.simple)}
    {foreach from=$stylesheetFiles.simple item='files' key='folderName'}
      {if $layoutSettings.stylesheetFilesPerRequest >= sizeof($files)}
        <link rel="stylesheet" type="text/css" href="/min/?b={$folderName}&f={implode pieces=$files glue=','}{if $debug}&debug=1{/if}"></link>
      {else}
        {strip}<link rel="stylesheet" type="text/css" href="/min/?b={$folderName}&f=
            {foreach from=$files item='fileName' name='files'}
              {$fileName}
              {if !$smarty.foreach.files.last}
                {if 0 != ($smarty.foreach.files.iteration % $layoutSettings.stylesheetFilesPerRequest)},{else}
                  {if $debug}&debug=1{/if}"></link><link rel="stylesheet" type="text/css" href="/min/?b={$folderName}&f=
                {/if}
              {/if}
            {/foreach}
            {if $debug}&debug=1{/if}"></link>
        {/strip}
      {/if}
    {/foreach}
  {/if}

{if $ecommerceTracker && in_array($ecommerceTracker->getPageType(), ['product', 'basket'])}
<script type="text/javascript">{literal}
dataLayer = [{/literal}{$ecommerceTracker->toJson()}{literal}];
{/literal}</script>
{/if}

{literal}<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MHXHNZZ');</script>
<!-- End Google Tag Manager -->{/literal}
</head>