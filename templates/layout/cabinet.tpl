{* Smarty *}<!DOCTYPE html>
<html {if isset($htmlClass) && !empty($htmlClass)} class="{$htmlClass}"{/if}>
  {include file="./partial/head.tpl"}
  <body {if isset($bodyClass) && !empty($bodyClass)} class="{$bodyClass}"{/if}>
  <header>
    <h1><a href="/" class="logo"></a><span>кабинет</span>
      {if $user}
        <em>{if $user->getEmail()}{$user->getEmail()}{else}{$this->formatPhone($user->getPhone(), '+7{code}{number}')}{/if}</em>
      {/if}
    </h1>
    <div id="scroll-nav">
      <span class="left hidden"></span>
      <span class="right"></span>
      <ul class="cabinet-menu">
        <li {if $currentPage == 'orders'}class="current"{/if}><a href="/cabinet/">Заказы</a></li>
        {*<li><a href="/cabinet/claims">Претензии</a></li>*}
        <li {if $currentPage == 'documents'}class="current"{/if}><a href="/cabinet/documents/">Документы</a></li>
        <li {if $currentPage == 'requisites'}class="current"{/if}><a href="/cabinet/requisites/">Реквизиты</a></li>
        {*<li><a href="/cabinet/notifications">Уведомления</a></li>*}
        <li {if $currentPage == 'profile'}class="current"{/if}><a href="/cabinet/profile/">Личные данные</a></li>
      </ul>
    </div>
    <a class="logout" href="/cabinet/logout/">выйти</a>
  </header>

  <main>{$content}</main>

  <footer>
    <div class="go-to-shop"><a href="/">Вернуться в магазин</a></div>
    <p class="rights">© 2014−{date("Y")}
      <span class="nomobile">Оптово-розничная база сантехники «Аквасант» — вся сантехника в&nbsp;одном месте</span>
      <span class="onlymobile">Аквасант<br>Оптово-розничная база сантехники</span>
    </p>
  </footer>
  {include file="./partial/footer_bottom.tpl"}

  </body>
</html>