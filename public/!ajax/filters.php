<?php
/*  Заглушка для работы с фильтрами

    $_REQUEST["action"] => get-filter / reset-filter
    $_REQUEST["name"] => имя фильтра
    $_REQUEST["value"] => выбранное значение (либо массив выбранных значенией)

    $_REQUEST["all-filters"] => array( name => value, ... ) -- для get-filter: помимо фильтра, на котором была нажата "применить", здесь передаются значения всех выбранных фильтров.

*/

    header("Content-Type: application/json; charset=utf-8", true);

    $result = array("state"=>"error", "message"=>"Неизвестная команда");

    switch($_REQUEST["action"]) {
        case "get-filter":
            // запрос при изменении значения фильтра, возвращает кол-во товаров для "применить" и ссылку для применения фильтра
            $result["state"] = "ok";

            $result["count"] = rand(5,30); // Количество товаров, подходящих под условия фильтра
            $vals = $_REQUEST["value"];
            if (is_array($vals)) $vals = implode(";", $vals);
            $result["href"] = "?".$_REQUEST["name"]."=".$vals;

        //  $result["state"] = "error";
        //  $result["message"] = "Сообщение об ошибке";
        break;
        case "reset-filter":
        // запрос при сбросе опции в списочном фильтре, возвращает только ссылку для применения сброса, в $_REQUEST["value"] - сбрасываемое значение

            $result["state"] = "ok";
            $result["href"] = "?".$_REQUEST["name"]."=not(".$vals.")";
        break;
   }


    echo json_encode($result);
