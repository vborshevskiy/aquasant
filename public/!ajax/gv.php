<?php
/*

    $_REQUEST["low"] = 0/1/-1
    возвращает список доступных видеороликов, если low==1, то в низком разрешении, low==-1 -- список изображений для слайд-шоу
*/

    header("Content-Type: application/json; charset=utf-8", true);

    if ($_REQUEST["low"]>=0) {
        $g = glob($_SERVER["DOCUMENT_ROOT"]."/video/".($_REQUEST["low"]*1 ? "low/" : "")."*.*");
    } else {
        if ($_REQUEST["low"]==-2) {
            $g = glob($_SERVER["DOCUMENT_ROOT"]."/images/slideshow/mobile/*.*");
        } else {
            $g = glob($_SERVER["DOCUMENT_ROOT"]."/images/slideshow/*.*");
        }
        shuffle($g);
    }
    $result = array();
    foreach ($g as $v) {
        $result[] = substr($v, strlen($_SERVER["DOCUMENT_ROOT"]));
    }

    echo json_encode($result);

