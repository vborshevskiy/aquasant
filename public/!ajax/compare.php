<?php
/*
    Добавление и удаление из сравнения

    $_REQUEST["action"] = add | remove | remove-group
    $_REQUEST["id"] = id товара или группы
*/

    header("Content-Type: application/json; charset=utf-8", true);

    $result = array("id"=>intval($_REQUEST["id"]), "state"=>"ok", "added"=>$_REQUEST["action"]=="add");

    $result["groups"] = array(
        array("id"=>12930, "title"=>"Душевые кабины", "count"=>4, "link"=>"/compare/?id=12930"),
        array("id"=>93401, "title"=>"Мебель для ванной", "count"=>3, "link"=>"/compare/?id=93401"),
        array("id"=>19403, "title"=>"Смесители", "count"=>1, "link"=>"/compare/?id=19403")
    );

    if ($_REQUEST["action"]=="remove-group") {
        foreach ($result["groups"] as $k=>$v) {
            if ($v["id"]==$_REQUEST["id"]) unset($result["groups"][$k]);
        }
    }
    sort($result["groups"]);

    echo json_encode($result);

