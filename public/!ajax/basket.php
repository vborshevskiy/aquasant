<?php
/*  Заглушка для работы с корзиной

    $_REQUEST["action"] => add / remove / update / change-count / get / service-cancel / less-variant / check-order

*/
    session_start();
    if (!isset($_SESSION["win"])) $_SESSION["win"] = 1;

    header("Content-Type: application/json; charset=utf-8", true);

    $result = array("state"=>"error", "message"=>"Неизвестная команда");

    switch($_REQUEST["action"]) {
        case "remove":
            $result["state"] = "ok";
            $result["items"] = getItems();

        //  $result["state"] = "error";
        //  $result["message"] = "Сообщение об ошибке";
        break;

        case "less-variant":
            // фиксирует выбор варианта оформления заказа, например при недостаточном количестве на складе
            // индекс варианта приходит в $_REQUEST["id"], в этом индексе можно закодировать любые прочие параметры ( ... "variants"=> array("item-5_variant-1" => ...) )

        case "service-cancel":
            // позволяет отказаться от предлагаемой в корзине услуги, напр., установки
            $result["state"] = "ok";

        //  $result["state"] = "error";
        //  $result["message"] = "Сообщение об ошибке";
        break;

        case "change-count":
            // Изменение кол-ва товара в корзине
            // $_REQUEST["id"], $_REQUEST["count"];

            $result["state"] = "ok";
        break;

        case "check-order":
            // Проверка содержимого заказа перед оформлением
            // Если за время оформления количество товара на складе стало недостаточным, 
            // юзер должен откорректировать выбор.
            if (rand(1,5)<0) {
                $result["state"]="ok"; // ok | error | less
            } else {
                $result["state"] = "less";
                $result["modals"] =
                    array(
                        array(
                            "title"=>"Душевая кабина «Nautilus» S-02 90/90 матовая",
                            "id"=>439248,
                            "image"=>"/images/goods/01.jpg",
                            "content-title"=>"Раскупили!",
                            "content"=>"Осталось только <strong>5</strong> штук",
                            "variants"=>array(
                                "1"=>array(
                                    "title"=>"Забрать частично",
                                    "details"=>"5 штук забрать сейчас,<br style='onlymobile'>а е ще 2 штуки — <strong>18 февраля</strong>"
                                ),
                                "2"=>array(
                                    "title"=>"Забрать, что есть",
                                    "details"=>"Оформить заказ только на 5 штук в наличии"
                                ),
                                "3"=>array(
                                    "title"=>"Забрать все, но 18 февраля",
                                    "details"=>"Мы подвезем желаемое количество<br>и отгрузим заказ целиком <strong>18 февраля</strong>"
                                )
                            )),
                        array(
                            "title"=>"Смеситель для ванны «Edelform» Iris IR1810",
                            "id"=>58492,
                            "image"=>"/images/goods/21.jpg",
                            "content-title"=>"Раскупили!",
                            "content"=>"Осталось только <strong>2</strong> штуки",
                            "variants"=>array(
                                "1"=>array(
                                    "title"=>"Забрать частично",
                                    "details"=>"2 штуки забрать сейчас,<br style='onlymobile'>а е ще 5 штук — <strong>18 февраля</strong>"
                                ),
                                "2"=>array(
                                    "title"=>"Забрать, что есть",
                                    "details"=>"Оформить заказ только на 2 штуки в наличии"
                                ),
                                "3"=>array(
                                    "title"=>"Забрать все, но 18 февраля",
                                    "details"=>"Мы подвезем желаемое количество<br>и отгрузим заказ целиком <strong>18 февраля</strong>"
                                )
                            ))
                    );
            }
            
            break;

        case "add":
            $result["state"]="ok";
            // $_REQUEST["id"], $_REQUEST["count"]

        if (isset($_REQUEST["check-context"])) { // Передается при добавлении товара в корзину на разных страницах,
                                                 // когда может потребоваться модальное окошко

            switch($_SESSION["win"]*0+1) {
                case 1: //  Сопутка
    
                        $details = array(
                            "title"=>"Душевая кабина «Nautilus» S-02 90/90 матовая Душевая кабина «Nautilus» S-02 90/90 матовая Душевая кабина «Nautilus» S-02 90/90 матовая",
                            "item_id" => $_REQUEST["id"],
                            "image"=>"/images/goods/01.jpg",
                            "count"=>2,
                            "min"=>1,
                            "max"=>21,
                            "price"=>15999,

                            "items"=>array(             /* При отсутствии сопутки передать пустой массив */
/*
                                array(
                                    "title"=>"Монтажный набор для установки душевой кабины",
                                    "image"=>"/images/accessories/01.jpg",
                                    "price"=>1599,
                                    "id"=>3020
                                ),
                                array(
                                    "title"=>"Комплект для защиты душевой кабины от перепадов давления",
                                    "image"=>"/images/accessories/02.jpg",
                                    "price"=>1399,
                                    "id"=>3022
                                ),
                                array(
                                    "title"=>"Полка ромб «Tatkraft» Mega Lock 11489",
                                    "image"=>"/images/accessories/03.jpg",
                                    "price"=>4122,
                                    "id"=>34932
                                )
*/
                            )
                        );
    
                        $result["modal"] = $details;
                        $result["mode"] = "items";
                    break;
    
                case 2: // Установка
                        $result["mode"] = "install";
                        $result["modal"] = array(
                            "title"=>"а устанавливать сами&nbsp;будете?",
                            "content"=>"<p>Обычно самостоятельная установка ванны занимает у наших клиентов весь день.</p><p>Квалифицированные мастера сервисного центра сделают вам все за 3 часа и 3000 <span class='rub'>р</span>, да еще и мусор за собой увезут.</p>",
                            "agree-text"=>"Заказать установку за 3 000 <span class='rub'>р</span>",
                            "cancel-text"=>"Спасибо, справлюсь сам",
                            "image"=>"/images/install2.png",
                            "id"=>32989, // при согласии с предложением, будет выполнен запрос на добавление этого id в корзину
                            "count"=>1   // количество добавленного товара при согласии
                        );
                    break;
    
                case 3: // Аксессуар -- не отображается на мобильной версии
                        $result["mode"] = "accessory";
                        $result["modal"] = array(
                            "title"=>"Душевая кабина «Nautilus» S-02 90/90 матовая",
                            "item_id"=>$_REQUEST["id"],
                            "image"=>"/images/goods/01.jpg",        
                            "count"=>2,
                            "min"=>1,
                            "max"=>21,
                            "price"=>15999,
                            "accessory"=>array(
                                "placeholder"=>"роскошный аксессуар",
                                "image"=>"/images/accessory.png",
                                "title"=>"Парогенератор для душевых кабин «Luxus»",
                                "item-link"=>"/item/",
                                "discount-text"=>"со скидкой 10% – только сейчас",
                                "price"=>28990,
                                "discount-price"=>24500,
                                "id"=>348993
                            )                            
                        );
                    break;
    
                case 4: // Мало товара
                        $result["mode"] = "less";
                        $result["modal"] = array(
                            "title"=>"Душевая кабина «Nautilus» S-02 90/90 матовая",
                            "image"=>"/images/goods/01.jpg",
                            "content-title"=>"Раскупили!",
                            "content"=>"Осталось только <strong>5</strong> штук",
                            "variants"=>array(
                                "1"=>array(
                                    "title"=>"Забрать частично",
                                    "details"=>"5 штук забрать сейчас,<br style='onlymobile'>а е ще 2 штуки — <strong>18 февраля</strong>"
                                ),
                                "2"=>array(
                                    "title"=>"Забрать, что есть",
                                    "details"=>"Оформить заказ только на 5 штук в наличии"
                                ),
                                "3"=>array(
                                    "title"=>"Забрать все, но 18 февраля",
                                    "details"=>"Мы подвезем желаемое количество<br>и отгрузим заказ целиком <strong>18 февраля</strong>"
                                )
                            )
                        );
                    break;
            }
            $_SESSION["win"]++;
            if ($_SESSION["win"]==5) $_SESSION["win"]=1;
        }

        //  $result["state"] = "error";
        //  $result["message"] = "Сообщение об ошибке";
        break;
    }

    $result["items"] = getItems();

    /* при изменении корзины для перерисовки кнопки в футере передавать текущий состав корзины */

    echo json_encode($result);

function getItems() {
    return array(
        3219 => array( "title"=>"Душевая кабина «Nautilus» S-02 90/90 матовая", "article" => 51152, "price"=>10999+rand(1,5)*743, "count"=>2 ),
        4391 => array( "title"=>"Смеситель для ванны «Edelform» Iris IR1810", "article" => 4391, "price"=>4525+rand(1,5)*171, "count"=>1 ),
    );
}