<?php
/*  Заглушка для оформления заказа

    $_REQUEST["action"] => step1 / add-address
    $_REQUEST["method"] => delivery / outpost
    $_REQUEST["phone"] ( outpost );

    RESPONSE: 
        "state" => "ok" | "error"
        "message" => error_message (state=='error');
        "redirect" => url 

*/
    header("Content-Type: application/json; charset=utf-8", true);

    $result = array("state"=>"error", "message"=>"Неизвестная команда");

    switch($_REQUEST["action"]) {
      case "step1":
        switch($_REQUEST["method"]) {
            case "outpost":
                // $_REQUEST [ phone ]
                $result["state"] = "ok";
                $result["redirect"] = "./step2.php";
            break;
            case "delivery":
                // $_REQUEST [ address: id, elevation: 0|1, stage: number, elevator: 0|1|2, date: date_id ]
                $result["state"] = "ok";
                $result["redirect"] = "./step2.php";
            break;
        }

      case "add-address":
        // $_REQUEST [ city/street/house/flat ]
        $result["state"] = "ok";
        $result["address"] = $_REQUEST["city"].", ".$_REQUEST["street"].", ".$_REQUEST["house"].", ".$_REQUEST["flat"];
        $result["id"] = 239;
      break;
    }

    echo json_encode($result);

