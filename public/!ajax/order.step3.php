<?php
/*  Заглушка для оформления заказа

    $_REQUEST["action"] => step3
    $_REQUEST["phone"] => телефон

    RESPONSE: 
        "state" => "ok" | "error"
        "message" => error_message (state=='error');
        "redirect" => url 

*/
    header("Content-Type: application/json; charset=utf-8", true);

    $result = array("state"=>"error", "message"=>"Неизвестная команда");

    $result["state"] = "ok";
    $result["redirect"] = "./step4.php";

    echo json_encode($result);

