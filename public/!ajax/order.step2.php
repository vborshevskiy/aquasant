<?php
/*  Заглушка для оформления заказа

    $_REQUEST["action"] => step2 / new-org
    $_REQUEST["phone"] ( outpost );

    RESPONSE: 
        "state" => "ok" | "error"
        "message" => error_message (state=='error');
        "redirect" => url 

*/
    header("Content-Type: application/json; charset=utf-8", true);

    $result = array("state"=>"error", "message"=>"Неизвестная команда");

    switch($_REQUEST["action"]) {
      case "step2":
            // $_REQUEST[ mode => "physic|firm", list-name1 => value ]
            $result["state"] = "ok";
            $result["redirect"] = "./step3.php";
      break;
      case "new-org":
        // $_REQUEST [ name/address/inn/kpp ]
        $result["state"] = "ok";
        $result["name"] = $_REQUEST["name"];
        $result["id"] = 239;
      break;
    }

    echo json_encode($result);

