$(function() {


    $(".field.phone input[type=text]").on("keyup keydown", function(ev) {
        if (ev.which==13 && ev.type=="keydown") {
            return true;
		}
        if (!numberKeyIsValid(ev)) { ev.preventDefault(); return false }
        $("a.save").removeClass('done');
    }).on("change", function(ev) {
        if ($(this).val().match(/\D/)) $(this).val($(this).val().replace(/\D/g, ''));
    }).on("paste", function(ev) {
        var $this = $(this);
        setTimeout(function() {
            if ($this.val().match(/\D/)) $this.val($this.val().replace(/\D/g, ''));
        }, 5);
    });
    $("main input").on("keydown", function(ev) {
        if (ev.which==13) $("a.save").trigger("click");
    });

    $("a.save").on("click", function(ev) {
        ev.preventDefault();
        if ($(this).hasClass("disabled") || $(this).hasClass("done")) return false;
        $(this).addClass("disabled");
        var $this = $(this), cb = {}, _c = $(".checkbox.checked");
        for (var i=0; i<_c.length; i++) cb[_c.eq(i).data("name")] = 1;
        $.post($_AJAX_PATHES.cabinet.notifications, { action: "save", checkboxes: cb, phone: $(".field.phone input").val(), email:$(".field.email input").val() },
            function(data) {
console.log(data);
                if (data.state=="ok") {
                    $this.addClass('done');
                    $this.removeClass("disabled");
                } else {
                    $this.removeClass("disabled");
                    alert(data.message);
                }
            }
        );
        return false;
    });

});