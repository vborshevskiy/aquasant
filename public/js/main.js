$(function() {
    var itemsPerLine = 0;
    var ct = $(".catalogue-main-page");
    $("nav.catalogue").remove();
    ct.appendTo("body>header").removeClass(".catalogue-main-page").addClass('catalogue');


    var calcSectionWidth = function() {

        if (window.innerWidth>=950) {
            var N = 0, N0 = 0;
            $("section.main-group").each(function() {
                var items = $(this).find(".goods>li"), y = items.eq(0).offset().top;
                N=0;
                for (var i=0; i<items.length; i++) {
                    if (items.eq(i).offset().top>y) break;
                    N++;
                }
                if (N>N0) N0=N;
            });
            $("section.main-group").each(function() {
                var items = $(this).find(".goods>li:not(.flex-filler)");
                for (var i=N0; i<items.length; i++) items.eq(i).addClass("hidden");
                $(this).find(".show-more").toggleClass("hidden", items.filter(".hidden").length==0);
            });
        } else {
            N0 = 3;
        }
        itemsPerLine = N0;
        $("section.main-group").each(function() {
            var items = $(this).find(".goods>li:not(.flex-filler)");
            for (var i=N0; i<items.length; i++) items.eq(i).addClass("hidden");
            $(this).find(".show-more").toggleClass("hidden", items.filter(".hidden").length==0);
        });

    }
    calcSectionWidth();

    $("body>header a.logo").on("click", function(ev) {
        ev.preventDefault();
        return false;
    });

    var onrs = function() {
        var h = window.innerHeight, h2 = h<850?850:(h>1100?1100:h);
        var logoK = h2/1100;
        $("body>header #logo").css({transform: "scale("+logoK+")"});
    }
    onrs();
    window.addEventListener("resize", onrs);

    $("nav.catalogue>ul>li>a").on("click", function(ev) {
        $("nav.catalogue .expanded").not($(this).parent()).removeClass("expanded");
        if ($(this).parent().find("ul").length==0) return true;
        $(this).parent().toggleClass('expanded');
		  if ($("body").hasClass("--mobile")) {
            if ($(this).parent().hasClass("expanded") && this.getBoundingClientRect().bottom>$(window).height() - 100) {
                $("html,body").stop().animate({scrollTop: $("body").scrollTop() + 300 }, 250);
            }
        }
        ev.preventDefault();
        return false;
    }).parent().on("mouseenter", function() {
        if ($(window).width()==400) return true;
        $(this).addClass('expanded');
    }).on("mouseleave", function() {
        if ($(window).width()==400) return true;
        $(this).removeClass('expanded');
    });;
    $("nav.catalogue>ul>li").each(function() {
        if ($(this).find("ul").length==0) $(this).addClass("directLink");
    });

    $(".show-more a").on("click", function(ev) {
        ev.preventDefault();
        var r = $(this).parents("section").eq(0), items = r.find(".goods li.hidden");
        for (var i=0; i<itemsPerLine && i<items.length; i++) {
            items.eq(i).removeClass("hidden");
        }
        $(this).parent().toggleClass("hidden", r.find(".goods .hidden").length==0);
        return false;
    });

	$('body').on("focus", 'header section.start form#search-top input[type=text]', function() {
        if (!$('body').hasClass('--mobile')) return true;
        $('html,body').stop().animate({scrollTop: $('body>header section.start form#search-top input[type=text]').offset().top - 20}, 120);
    });

});