_menu_no_scroll = false;

$(function() {
    var _xfast = false;

    var $item_action = $(".buy-block>.action"),
        $buy_now = $("div.buy-now"),
        $short_char = $("aside.buy-block .table, a.to-info"),
        $returns = $("#returns, section.returns"),
        $markdown = $("#markdown-1");

    $("ul.goods").each(function() {
        if ($(this).parents(".accessories").length==0) return false;
        for (var i=0; i<5; i++) {
            $(this).append("<li class='flex-filler'></li>");
        }
        $(this).find("li:not(.item-action):not(.flex-filler)").each(function() {
            var x = $("<div class='wr'></div>");
                x.appendTo(this);
            $(this).find(">*:not(.images)").appendTo(x);
        });
    });

    $("a.print-page").on("click", function(ev) {
        ev.preventDefault();
        print();
        return false;
    });

    $("a.context-buy-button").on("click", function(ev) {
        ev.preventDefault();
        setTimeout(function() { $("aside.buy-block .element .add-to-basket").trigger("click"); });
        return false;
    });

    var onrs = function() {
      $("ul.short-goods, ul.goods").each(function() {
        var z = $(this).find(">li:not(.flex-fix)"), N=0;
            z.removeClass('hidden');
        if (z.length>0) {
            y = z.eq(0).offset().top;
            for (var i=0; i<z.length; i++) {
                if (z.eq(i).offset().top>y) break;
                N++;
            }
            z.addClass('hidden');
            var NX = N;
            if ($(this).hasClass("short-goods") || $(this).parents(".analogs").length>0) NX*=2;
            if (window.innerWidth<900) NX=4;
            for (i=0; i<NX && i<z.length; i++) z.eq(i).removeClass("hidden");
            $(this).parent().find(".show-more").toggleClass("hidden", z.filter(".hidden").length==0);
        } else $(this).parent().find(".show-more").addClass("hidden");
        $(this).parent().find(".show-more").on("click", function(ev) {
            ev.preventDefault();
            z.removeClass('hidden');
            $(this).addClass("hidden");
            return false;
        });
      });
      if (window.innerWidth<=900) {
        $returns.insertAfter("aside.buy-block .element.pay .details ul");
        $item_action.insertBefore("section.description");
        //$buy_now.insertAfter("aside.buy-block .buy .price.element");
		  $buy_now.insertAfter(".-compact-delivery .-items");
        $markdown.insertBefore("aside.buy-block");
        $short_char.insertBefore("aside.buy-block");
      } else {
        $returns.appendTo("main");
        $item_action.insertBefore(".buy-block>.buy");
        $buy_now.insertAfter("section.returns .text");
        $short_char.insertAfter("aside.buy-block .buy");
        $markdown.insertAfter("aside.buy-block .buy");
      }
    }
    var onsc = function() {
        var t = $(document.body).scrollTop(), itms = $("#scroll-nav a");
        if (t==0) t = $(window).scrollTop();
        if ($(document.body).hasClass("basket")) return true;
        $(document.body).toggleClass("pan-1", t>120);
        $(document.body).toggleClass("pan-2", t>$("aside.buy-block .buy").offset().top-50);
        var XTX  =0;
        for (var i=0; i<itms.length; i++) {
            var x = itms.eq(i).data("id"), tx = $("#"+x).offset().top + t, hx = $("#"+x).next().height()+80;
            if (x=="x-root") hx = $("#"+itms.eq(1).data('id')).offset().top + t, tx =0;
            if (t+window.innerHeight/5<tx+hx) { XTX = i; break }
        }
        if (i==itms.length) XTX = i-1;
        _menu_no_scroll = true;
        itms.eq(XTX).trigger("click");
        _menu_no_scroll = false;
    }

	 $(".-compact-delivery .gallery").each(function() {
		console.log('o');
        var curN = 0, root = $(this).find(".viewport"), items = root.find(">*"), len = items.length;
		  var $this = $(this);
        $(this).find("a.scroll").on("click", function(e) {
            e.preventDefault();
            items = root.find(">*");
            if (root.hasClass("animating")) return false;
            root.parent().find("a.scroll").addClass("disabled");
            setTimeout(function() {
                root.parent().find("a.scroll").removeClass("disabled");            
            }, 5);
            var st = 1;
            if ($(this).hasClass("left")) st*=-1;
            curN+=st;
            var mv = root.scrollLeft() + items.eq(0).outerWidth()*st;
            root.addClass("animating").stop().animate({ scrollLeft: mv }, function(e) {
                root.removeClass("animating");
                if (st>0) {
                    items.eq(0).insertAfter(items.eq(-1));
                    curN-=1;
                    root.stop().animate({ scrollLeft: items.eq(0).width() }, 0);
                } else {
                    items.eq(-1).insertBefore(items.eq(0));
                    curN+=1;
                    root.scrollLeft(items.eq(0).width());
                }
            });
            return false;
        });        

			setSwipe(root); 

		  var autm = false;
        
        var autoSlide = function() {
            $this.find(".scroll.right").trigger('click');
            autm = setTimeout(autoSlide, 3000);
        }
		  items.eq(-1).prependTo(items.eq(0).parent());
        items.eq(0).parent().scrollLeft(items.eq(0).outerWidth());
        $(".-compact-delivery .gallery #map").css({ visibility: "visible" });
        autm = setTimeout(autoSlide, 3000);
        
        $(this).on("mouseenter touchstart", function() {
            if (autm) clearTimeout(autm);
        }).on("mouseleave", function() {
            if (autm) clearTimeout(autm);
            autm = setTimeout(autoSlide, 3000);
        });
    });
    


/*********************/
    initScroll();
    onrs();
    onsc();
    window.addEventListener("resize", onrs);
	 document.body.addEventListener("scroll", onsc);
    window.addEventListener("scroll", onsc);
    $(".description .gallery:not(.for-shop) .viewport img[itemprop]").remove();
    setTimeout(function() {
        setSwipe($(".description .gallery:not(.for-shop) .viewport"));
        $(".c-part .viewport").each(function() {
            setSwipe($(this));
        });
    }, 100);


    var galleryInit = function() {
        $("aside.gallery").each(function() {
            var $this = $(this), $thumbs = $this.find(".thumbs"), $vp = $this.find(".viewport"), mult = true;
            $this.find(".thumbs li").eq(0).addClass("selected");
            if ($this.find(".thumbs li").length>1) {
                $this.append("<div class='buttons'><a href='#' class='left'></a><a href='#' class='right'></a></div>");
                $this.find(".buttons a").on("click", function(ev) {
                    ev.preventDefault();
                    var e; if ($(this).hasClass("right")) {
                        e = $thumbs.find("li.selected").next();
                        if (e.length==0) e = $thumbs.find("li").eq(0);
                    } else {
                        e = $thumbs.find("li.selected").prev();
                        if (e.length==0) e = $thumbs.find("li").eq(-1);
                    }
                    e.trigger("click");
                    return false;
                });
                $this.find(".buttons").on("click", function(ev) {
                    if ($(ev.target).hasClass("buttons")) $(this).parent().find(".viewport").trigger("click");
                });
            } else {
                $this.find(".thumbs").addClass("hidden");
            }
            $this.find(".thumbs li").on("click", function(ev) {
                ev.preventDefault();
                var ix = $(this).parent().find("li").index(this), l = $vp.width() * ix;
                $vp.finish().animate({scrollLeft: l}, _xfast?0:200);
                $(this).parent().find(".selected").removeClass("selected");
                $(this).addClass("selected");
                return false;
            }).each(function() {
                var g = $(this).parents(".gallery").find(".viewport");
                    g.append("<div class='slide'><img src='"+$(this).find("img").data("full")+"'></div>");
            });

            $this.find(".slide").on("click", function() {
                if ($(".-overlay-win.full-gallery").length>0) {
                    $(".-overlay-win.full-gallery").find("a.right").trigger("click");
                }
            });
            
        });
        $(document.body).on("keydown", function(ev) {
            if ($(".-overlay-win.full-gallery").length>0) {
                if (ev.which==37) $(".-overlay-win.full-gallery").find("a.left").trigger("click");
                if (ev.which==39) $(".-overlay-win.full-gallery").find("a.right").trigger("click");
            };
        });
    }
    var viewportOnClose = function() {
        var o = $(".full-gallery").data("gal");
        $(".full-gallery .buttons").prependTo(o);
        $(".full-gallery .viewport").prependTo(o);
        _xfast = true;
        o.find(".thumbs .selected").trigger("click");
        _xfast = false;
    }

    $("div.c-part .board").each(function() {
        $(this).find("span:not(.more)").css({height: $(this).parent().outerHeight()-2});
        var $this = $(this).parents(".c-part").find(".viewport");
        $(this).find("span b").on("click", function(ev) {
            ev.preventDefault();
            var st = $this.find("li").eq(0).outerWidth(), $b = $(this);
            if (window.innerWidth<450) st = 370;
				//console.log(window.innerWidth);
            if ($(this).parent().hasClass("left")) st*=-1;
				if ($this.scrollLeft() + st > $this[0].scrollWidth - 8) {
                st = 1e9;
            }
				var offset = ($this.attr('data-offset') ? parseFloat($this.attr('data-offset')) : $this.scrollLeft()) + st;
				//offset += 0.5;
				$this.attr('data-offset', offset);
				console.log(offset);
            $this.finish().delay(1).animate({scrollLeft: $this.scrollLeft() + st}, 150, function() {
                    if ($this.scrollLeft()+$this.width()>=$this[0].scrollWidth-90) {
                        $this.parent().find(".board .right:not(.more)").hide();
                    } else {
                        $this.parent().find(".board .right:not(.more)").show();
                    }
                    if ($this.scrollLeft()==0) {
                        $this.parent().find(".board .left").hide();
                    } else {
                            $this.parent().find(".board .left").show();
                    }
            });
            return false;
        });
        $(this).find(".more").css({top: $(this).parent().outerHeight()/2 - 24 }).on("click", function(ev) {
            ev.preventDefault();
            $(this).parent().addClass('started');
            $(this).parent().find(".right:not(.more) b").trigger('click');
            return false;
        });
    });


    $("ul.goods").each(function() {
        for (var i=0; i<5; i++) $(this).append("<li class='flex-fix'></li>");
    });
    $("ul.goods li:not(.item-action):not(.flex-fix)").each(function() {
        var h = $(this).find(".add-to-basket"), z = $(this).find(".title");
        $(this).data("id", h.data("id"));
        $(this).data("href", z.attr("href"));
        $(this).append("<div class='compare-add'></div>");
        if ($(this).hasClass("comparing")) {
            $(this).find(".compare-add").addClass("remove");
        } 
    });
    $("ul.goods li .compare-add").on("click", function(ev) {
        ev.preventDefault();
        var $this = $(this);
        Basket.addCompare($(this).parents("li").find(".add-to-basket").data("id"), !$(this).parents("li").hasClass("comparing"))
              .then(function(state) {
                if (state) {
                    $this.parents("li").addClass("comparing");
                } else {
                    $this.parents("li").removeClass("comparing");
                }
              });
        return false;
    });
    $("aside.buy-block .element .compare a").on("click", function(ev) {
        ev.preventDefault();
        var $this = $(this);
        Basket.addCompare($(this).parents(".element").find(".add-to-basket").data("id"), !$this.parent().hasClass("remove"))
              .then(function(state) {
                if (state) {
                    $this.parent().addClass("remove");
                } else {
                    $this.parent().removeClass("remove");
                }
              });
        return false;
    });


    $(".description .gallery:not(.for-shop) .viewport").on("click", ".slide img", function(ev) {
        ev.preventDefault();
        var $vp = $(this).parents(".viewport");
        if ($vp.hasClass("no-photo")) return false;
        $this = $vp.parents(".gallery");
        if ($(document.body).hasClass('overlayed')) {

        } else {
            var m = modalDialog({ width: "calc(100% - 100px)", height: "calc(100% - 140px)", class: "full-gallery", onclose: viewportOnClose });
                m.data("gal", $this);
            m.css({left: 20, top: 40});
            m.append($vp.parent().find(".buttons"));
            m.append($vp);
            _xfast = true;
            $this.find(".selected").trigger("click");
            _xfast = false;
        }
        return false;
    });
    galleryInit();

    $(".-info").each(function() {
        $(this).find(".content").html("<span>"+$(this).find(".content").html()+"</span>");
    });

    $(document).on('click', '.-info', function(ev) {
		ev.preventDefault();
      $(".-info").not(this).removeClass("expanded");
		$(this).toggleClass('expanded'); 
		if ($(this).hasClass("expanded")) {
			var ol = $(this).find(".content").offset().left;
			if (ol<0) {
				$(this).find(".content").css({ "padding-left": ""+(12-ol)+"px" });
			} else {
				$(this).find(".content").css({ "padding-left": "" });
			}
		}
		return false;
    });

    $("section.characteristics").on("click",  "a.show-all", function(ev) {
        ev.preventDefault();
        var $root = $("section.characteristics ul");
        if ($(this).data("show")) {
            $root = $root.filter("#"+$(this).data("show"));
        }
        $root.find("li.hidden").show();
        $(this).hide();
        return false;
    });
    $("section.item-description a.show-all").on("click", function(ev) {
        ev.preventDefault();
        $("section.item-description .hidden").removeClass("hidden");
        $(this).hide();
        return false;
    });

    $("section.video video").each(function() {
        var t = $(this).parent().find(".length");
        this.onloadedmetadata = function() {
            var m = Math.floor(this.duration/60),
                s = Math.round(this.duration%60);
                t.html(m+"′"+" "+s+"″");
        }
    });

    $("#top-panel a.scroll-top").on("click", function(ev) {
        ev.preventDefault();
        $("html,body").finish().animate({scrollTop: 0}, 100);
        return false;
    });
    recalculate();

    $(".to-info").on("click", function(ev) {
        ev.preventDefault();
        $("html,body").finish().animate({scrollTop: $("#characteristics").offset().top - 60 });
        return false;
    });

    $(".c-part").each(function() {
        if ($(this).find("ul").length>0) {
            if ($(this).find("ul").width()<$(window).width()) {
                $(this).find(".board").remove();
            }
        }
    });

    /* *** QR-code *** */

	try {
		 var qrcode = new QRCode($("#page-qr")[0], {
			text: location.href,
			width: 400,
			height: 400,
			colorDark : "#000000",
			colorLight : "#ffffff",
			correctLevel : QRCode.CorrectLevel.H
		 });
	} catch (e) {
		console.log('QRCode', 'error', e);
	}

	 complectRefresh();
});

var recalculate = function() {
    var c = $(".-x-checkbox.checked");
    var t = 0, t2 = 0, n, n2;
    for (var i=0; i<c.length; i++) {
        n = c.eq(i).parent().find("strong").eq(0).text().replace(/[^0-9\.]/g, '')*1;
        if (c.eq(i).parent().find("strong").eq(1).length>0) {
            n2 = c.eq(i).parent().find("strong").eq(1).text().replace(/[^0-9\.]/g, '')*1;
        } else {
            n2 = n;
        }
        if (n>0) t+=n;
        if (n2>0) t2+=n2;
    }
    $("#top-panel .price strong b").text(formatNum(t));
    $("aside.buy-block .price.element b i").eq(0).text(formatNum(t));
    $("aside.buy-block .price.element b i").eq(1).text(formatNum(t2));

    var nc = $(".complectation .-x-checkbox.checked").length;
        $("#complectation .selected-info em").text(formatNum(nc));

    $("section.options").each(function() {
        var nc = $(this).find(".-x-checkbox.checked").length;
            $(this).prev().find(".selected-info em").text(formatNum(nc));        
    });
}

var characteristicsRefresh = function() {   
    var $items = [], x = $(".-x-checkbox.checked, #complectation");
    $("section.characteristics").html("").addClass("preloader");
    for (var i=0; i<x.length; i++) {
        if (x.eq(i).parents("li").data("id")) {
            $items.push(x.eq(i).parents("li").data("id"));
        }
        if (x.eq(i).data("id")) {
            $items.push(x.eq(i).data("id"));
        }
    }

	if (true) {
		$.ajax({
			url: $_AJAX_PATHES.item.getCharacteristics,
			method: 'post',
			data: { items: $items },
			dataType: 'json',
			success: function(data) {
				$("section.characteristics").removeClass("preloader");
				  $("section.characteristics").html(data.characteristics);
			}
		});
	}

	if (false) {
		 $.post($_AJAX_PATHES.item.getCharacteristics, { items: $items }, function(data) {
			  $("section.characteristics").removeClass("preloader");
			  $("section.characteristics").html(data.characteristics);
			  console.log(data.characteristics);
			  if (false) {
				  for (var i=0; i<data.characteristics.length; i++) {
						var $ul = $(data.characteristics[i]);
							 $ul.attr("id", "characteristics-"+(i+1));
						$("section.characteristics").append($ul);
						$("section.characteristics").append('<br><a href="#" class="show-all" data-show="characteristics-'+(i+1)+'">Показать все характеристики</a><br>');
				  }
				}
		 });
	}
}

var complectRefresh = function() {
	if (0 == $('.complect-part').length) return;

	var amount = 0;
	$('.complect-part .-x-checkbox.checked').each(function() {
		var _ = $(this);
		var wr = _.closest('.complect-part');
		if (wr.attr('data-price')) {
			amount += parseInt(wr.attr('data-price'));
		}
	});
	if (0 < amount) {
		$('.item-total-amount').html(format_price(amount) + '  <span class="rub">р</span>');
		var container = $('.item-total-amount').parent();
		if (1 < container.find('b').length) {
			var oldPrice = parseInt(container.find('b').first().html().replace('<span class="rub">р</span>', '').replace(' ', ''));
			if ((0 < oldPrice) && (oldPrice <= amount)) {
				container.find('b').first().css('visibility', 'hidden');
			}
		}
	}
}

var complectSubitemsQuantityRefresh = function() {
	if (0 == $('.complect-part').length) return;

	var quantity = $('.complect-part .-x-checkbox.checked').length;
	$('.complect .scroll-top .quantity').html(quantity);
}