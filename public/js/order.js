$(function () {
    var dtmp = {}, dtus = {}, __ = '', scrl = false, orderMap = false, _stage_price = 0, _current_stage_control = -1;
    var clienCode;

    $.widget("ui.autocomplete", $.ui.autocomplete, {
        _renderItem: function (ul, item) {
            var li = $("<li data-value='" + item.value + "'>" + item.label + "</li>");
            li.appendTo(ul);
            return li;
        }

    });
    //$("input#cliend-id").mask("99-99", { placeholder: "__-__" });
    Inputmask({ "mask": "99-99" }).mask("input#cliend-id");
    $(".get-phone .username input").add(".email-enter.-name input").suggestions({
        token: "0bb9876cbd5d802426809feafddb7d41d173fbff",
        type: "NAME",
        count: 5,
        /* Вызывается, когда пользователь выбирает одну из подсказок */
        onSelect: function (s) {

        }
    });
    $(".email-enter:not(.-name) input").suggestions({
        token: "0bb9876cbd5d802426809feafddb7d41d173fbff",
        type: "EMAIL",
        count: 5,
        /* Вызывается, когда пользователь выбирает одну из подсказок */
        onSelect: function (s) {

        }
    });

    $(".order-info-partial .-items").append("<a href='#' class='scroll left'></a>");
    $(".order-info-partial .-items").append("<a href='#' class='scroll right visible'></a>");
    $(".order-info-partial .-items a.scroll").on("click", function (ev) {
        ev.preventDefault();
        var s = 100, l = $(this).parents(".-items").find("ul").scrollLeft(), n = $(this).parents(".-items").find("ul")[0].scrollWidth - $(this).parents(".-items").width();
        if ($(this).hasClass('left')) s *= -1;
        l += s;
        $(this).parents(".-items").find("ul").stop().animate({ scrollLeft: l }, 200);
        $(this).parent().find(".scroll.left").toggleClass('visible', l > 0);
        $(this).parent().find(".scroll.right").toggleClass('visible', l < n);
        return false;
    });
    var onrs = function () {
        if ($(".order-info-partial").length > 0) {
            var itm = $(".order-info-partial .-items ul");
            if (itm[0].scrollWidth > itm.width()) {
                $(".order-info-partial .-items a.scroll").addClass("enabled");
            } else {
                $(".order-info-partial .-items a.scroll").removeClass("enabled");
            }
        }
    }
    onrs();
    window.addEventListener("resize", onrs);

    if ($("#s-delivery .map").length > 0) {

        init = function () {
            orderMap = new ymaps.Map($("#s-delivery .map")[0], {
                center: [55.753676, 37.619899],
                zoom: 12,
                controls: []
            }, {
                    minZoom: 9
                });
        }

        ymaps.ready(init);

        //orderMap.container.fitToViewport()
    }

    var $yamap = $('.m-map');
    if ($yamap.length > 0) {
        var map;
        function init() {
            map = new ymaps.Map("m-map", {
                center: [55.65064256909375, 37.83097599999998],
                zoom: 14,
                controls: ['fullscreenControl', 'rulerControl', 'typeSelector', 'zoomControl']
            });
            placemark = new ymaps.Placemark(map.getCenter(), { balloonContentBody: 'МКАД, 14-й км, д. 10<br><a href="#" class="-map-button">забрать здесь</a>' });
            map.geoObjects.add(placemark);
            placemark.balloon.open();

            $("body").on("click", ".-map-button", function (ev) {
                ev.preventDefault();
                $("html,body").stop().animate({ scrollTop: $(".get-phone").offset().top + $(".get-phone").outerHeight() - $(window).height() + 250 }, 300);
                $(".get-phone input").focus();
                return false;
            });
        }
        ymaps.ready(init);
    }



    $(".b-tabs").each(function () {
        var t = $(this).find("ul li"), r = $(this).find("ul").data("block"), $this = $(this);
        t.on("click", function (ev) {
            ev.preventDefault();
            $(".tab-block." + r).hide();
            $(".tab-block#" + $(this).data("for")).show();
            $(this).parents("ul").find(".active").removeClass('active');
            $(this).addClass('active');
            return false;
        });
        if ($(this).find("li.active").length == 0) $(this).find("li").eq(0).addClass('active');
        $this.find("li.active").trigger("click");
    });

    $(".addresses-list").delegate("li", "click", function (ev) {
        ev.preventDefault();
        $(this).parents("ul").find("li").removeClass('selected');
        $(this).addClass('selected');
        var stage = 1, elevator_type = -1;
        if ($(this).data("stage")) stage = $(this).data("stage");
        if (typeof ($(this).data("elevator")) != "undefined") elevator_type = $(this).data("elevator");
        $(".delivery-set input.number").val(stage);
        if (elevator_type == -1) {
            $(".elevator li[data-value=0]").trigger("click");
            $("#s-delivery .checkbox").addClass("checked").trigger("click");
        } else {
            $("#s-delivery .checkbox").removeClass("checked").trigger("click");
            $(".elevator li[data-value=" + elevator_type + "]").trigger("click");
        }
        setMapPointer($(this));
        getDelivery($(this).data("id"));
        return false;
    });

    $(".elevator, .dates").delegate("li", "click", function (ev) {
        ev.preventDefault();
        if ($(this).hasClass("user-select")) return false;
        $(this).parent().find(".selected").removeClass('selected');
        $(this).addClass("selected");
        _recalc();
        return false;
    });

    $(".dates").delegate("li.user-select", "click", function (ev) {
        ev.preventDefault();
        var $this = $(this);
        var dy = ["вс", "пн", "вт", "ср", "чт", "пт", "сб", "вс"];
        console.log($(this).find("input"));
        xCal($(this).find("input")[0], {
            fn: function (a, b) {
                $this.find("b").text(b.dat);
                var _dt = new Date(b.year, b.month, b.dat);
                $this.find("span").text(dy[_dt.getDay()]);
                $this.data("id", _dt.getTime() / 1000);
                console.log(_dt);
                $this.parents("ul").find(".selected").removeClass("selected");
                $this.addClass("selected").removeClass("empty");
                _recalc();
            }
        });
        return false;
    });

    var _recalc = function () {
        var amount = $(".dates li.selected").data("val"), add = ", без подъема на этаж", st = 1 * $(".delivery-set .checkbox input").val(),
            _m = ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"];
        if ($(".delivery-set .checkbox").hasClass("checked")) {
            //amount += $(".elevator .selected").data("baseprice")*1;
            //amount += $(".elevator .selected").data("perstage")*st;
            amount += _stage_price;
            add = ", с подъемом на " + st + " этаж";
        }
        var _z = new Date($(".dates li.selected").data("id") * 1000), _d = _z.getDate(), _mn = _m[_z.getMonth()];
        $(".order-done h5 strong").text(formatNum(amount));
        $(".order-done h6").text("на " + _d + " " + _mn + add);
    }

    var checkOutpostPhone = function () {
        if ($("#s-pickup div.next-step a").length == 0) return false;
        $("#s-pickup div.next-step a").toggleClass("disabled", $("#s-pickup .get-phone input").val().replace(/\D/g, '').length != 10);
    }
    var checkPagePhone = function () {
        if ($("body.phone div.next-step a").length == 0) return false;
        if (0 < $(".get-phone input").length) {
            $("div.next-step a").toggleClass("disabled", $(".get-phone input").val().replace(/\D/g, '').length != 10);
        }
    }

    var checkAddresses = function () {
        if ($("#s-delivery").length == 0) return false;
        $("#s-delivery").toggleClass("empty", $(".addresses-list li").length == 0);
        setTimeout(function () {
            if ($("#s-delivery").hasClass("empty")) {
                $("#s-delivery .add-address").trigger("click");
                $(".-overlay-win.add-new-address").appendTo("#s-delivery");
                $(".-overlay-win.add-new-address h4").text("Куда доставить?");
                $("#s-delivery .details").clone().insertBefore(".-overlay-win.add-new-address h4");
                $(".-overlay").remove();
                $("body").removeClass("overlayed");
                setTimeout(function () {
                    $("#full-address").focus();
                }, 50);
            }

            try { orderMap.container.fitToViewport(); } catch (e) { }
        });
    }

    var getDelivery = function (id) {
        var sdl = function (id) {
            $("ul.dates").html("");
            var dy = ["вс", "пн", "вт", "ср", "чт", "пт", "сб", "вс"];
            for (var i in dtmp[id]) {
                var _d = new Date(i * 1000), xt = dy[_d.getDay()], xd = _d.getDate();
                $("ul.dates").append("<li data-id='" + i + "'><b>" + xd + "</b>" + xt + "</li>");
                $("ul.dates li:last-child").attr("data-val", dtmp[id][i]);
            }
            if (dtus[id]) {
                $("ul.dates").append("<li class='user-select empty' data-id='0' data-val='" + dtus[id]["price"] + "'><input type='text' name='date-value' class='btm' value='' min='" + dtus[id]["min-date"] + "' max='" + dtus[id]["max-date"] + "'><b>...</b><span>&nbsp;</span></li>");
            }
            $(".when, .order-done:not(.hidden)").show();
            $("#s-delivery .details").removeClass("-prld");
            $("ul.dates li:first-child").trigger("click");
            if (scrl) {
                try {
                    var t = $('#s-delivery .when').offset().top, wb = $(window).scrollTop() + $(window).height(), xt = $(window).scrollTop() + (t - wb + 400);
                    if (t > wb - 80) {
                        $("html,body").stop().animate({ scrollTop: xt }, 300);
                    }
                } catch (e) { }
            }

        }
        if (typeof (dtmp[id]) != "undefined") sdl(id);
        else {
            $(".when, .order-done").hide();
            $("#s-delivery .details").addClass("-prld");
            $.get($_AJAX_PATHES.order.delivery, { action: "delivery", address: id }, function (data) {
                $("#s-delivery .details").removeClass("-prld");
                if (data.coords) {
                    console.log('------- delivery debug     ---------');
                    console.log('Request address', id);
                    console.log('Request coords', data.coords);
                    var deliveryPrice = 0;
                    if ('ok' == data.state && data['user-date']) {
                        var deliveryData = data['user-date'];
                        if (deliveryData.price) {
                            deliveryPrice = deliveryData.price;
                        }
                    }
                    console.log('Response', (('ok' == data.state && !data.isRegion) ? deliveryPrice : data.message));
                    console.log('------- END delivery debug ---------');
                    if (data.method) {
                        console.log('------- GetPreliminaryDelivery request dump --------');
                        console.log(data.method);
                        console.log('------- END dump --------');
                    }
                }
                if (data.state == "ok") {
                    if (!data.isRegion) {
                        $('.delivery-no-region').removeClass('hidden').show();
                        $('.delivery-region').addClass('hidden').hide();
                        dtmp[id] = data.delivery;
                        dtus[id] = data["user-date"];
                        sdl(id);
                    } else {
                        $('.delivery-no-region').addClass('hidden').hide();
                        $('.delivery-region').removeClass('hidden').show();
                    }
                } else {
                    alert(data.message);
                }
            });
        }
    }

    setMapPointer = function (li) {
        if (!li) {
            return;
        }
        if (!orderMap) {
            return setTimeout(function () { setMapPointer(li) }, 50);
        }
        var coords = li.data("coords");
        if (!coords) {
            return;
        }
        if (typeof (coords) == "string") coords = coords.split(",");
        if (typeof (ymaps.geocode) == "undefined") return setTimeout(function () { setMapPointer(li); });
        if (coords && coords.length != 2) {
            var myGeocoder = ymaps.geocode(li.text());
            if (__ != '') __.geoObjects.remove();
            myGeocoder.then(
                function (res) {
                    __ = res;
                    console.log(res.geoObjects);
                    orderMap.geoObjects.add(res.geoObjects);
                    var c = res.geoObjects.getBounds()[0];
                    orderMap.setCenter(c);
                    console.log('cc', c);
                    orderMap.setZoom(14);
                    li.data("coords", c);
                },
                function (err) {
                    // обработка ошибки
                }
            );
        } else {
            console.log('coords', coords);
            try {
                var mo = new ymaps.GeoObject({
                    geometry: {
                        type: "Point",
                        coordinates: coords
                    },
                    properties: { hintContent: li.text() }
                },
                    {});
                orderMap.geoObjects.add(mo);
                orderMap.setCenter(coords);
                orderMap.setZoom(14);
            } catch (e) { console.log('err', e); }
        }
    }


    checkOutpostPhone();
    checkPagePhone();
    checkAddresses();
    $(".get-phone .b-enter input.num-phone, #s-delivery .checkbox input")
        .on("keydown keyup paste change", function (ev) {
            if (ev.which == 13 && ev.type == "keydown") {
                return true;
            }
            setTimeout(function () {
                checkOutpostPhone();
                checkPagePhone();
                _recalc();
            }, 20);
            if (typeof (ev.which) != "undefined" && !numberKeyIsValid(ev)) { ev.preventDefault(); return false }
        });
    var gotoStep2 = function () {
        $(".next-step a").addClass("x-next");
        var dt = { action: "step1", method: "outpost", phone: $("#s-pickup .get-phone input").val() };
        if ($(".get-phone .username").length > 0) {
            dt.username = $(".get-phone .username input").val();
        }
        $.post($_AJAX_PATHES.order.step1, dt, function (data) {
            if (data.state != "ok") {
                alert(data.message);
                return checkOutpostPhone();
            } else {
                location.href = data.redirect;
            }
        });
    }
    var gotoStep4 = function () {
        $(".next-step a:not(.set-phone)").addClass("x-next");
        var dt = { action: "step3", clientName: $('.client-name input[type="text"]').val(), phone: $(".get-phone input").val(), comment: $("body.phone .get-phone .remarks textarea").val() };
        if ($(".get-phone .remarks-delivery").length > 0) {
            dt.deliveryRemarks = $(".get-phone .remarks-delivery textarea").val();
        }
        $.post($_AJAX_PATHES.order.step3, dt, function (data) {
            if (data.state != "ok") {
                $("body").removeClass("ready");
                alert(data.message);
                return checkOutpostPhone();
            } else {
                location.href = data.redirect;
            }
        });
    }
    var checkPhoneAuth = function (callback) {
        $.post($_AJAX_PATHES.order.step1CheckPhone, { action: "check-number", phone: $(".get-phone input.num-phone").val(), clientName: $(".get-phone .client-name input").val() }, function (data) {
            if (data.state == "ok") return callback();
            $("body").removeClass("ready");
            if (data.state == "request-password") {
                $(".b-check h5").text(data.phone);
                $(".b-check p").text(data.message);
                $(".b-check .check-done a").text(data.button).addClass('disabled');
                $(".get-phone").addClass("check");
                $("#s-pickup div.next-step a, body.phone .next-step a, body.phone .next-step").hide();
                $(".b-check input").val("");
                $(".b-check .e-err").text("");
            }
        });
    }
    nextStepCallback = function () { }
    $("#s-pickup div.next-step a").on("click", function (ev) {
        ev.preventDefault();
        if ($(this).hasClass('disabled')) return false;
        var $this = $(this);
        $this.addClass('disabled');
        nextStepCallback = gotoStep2;
        checkPhoneAuth(nextStepCallback);
        return false;
    });
    $(".b-check .check-form .actions a.cancel").on("click", function (ev) {
        ev.preventDefault();
        $(".get-phone").removeClass("check");
        $("#s-pickup div.next-step a, body.phone .next-step a, body.phone .next-step").show().removeClass("disabled");
        return false;
    });
    $(".b-check input").on("keyup change paste blur", function (ev) {
        $(".b-check .check-done a").toggleClass("disabled", $(this).val().trim() == "");
        if (typeof (ev.which) != "undefined" && ev.which == 13) $(".b-check .check-done a").trigger("click");
    });
    $(".b-check .actions a.forgot").on("click", function (ev) {
        ev.preventDefault();
        var win = modalDialog({ class: "forgot-win", width: 320, height: 160 });
        win.append("<strong>Мобильный телефон или e-mail</strong>");
        win.append("<input type='text' class='forgot-input' value='" + $(".get-phone input").val() + "'>");
        win.append("<a href='#' class='disabled forgot-set'>Выслать пароль</a>");
        win.append("<div class='err-msg'></div>");
        win.find("input").on("keyup keydown blur focus paste change", function (ev) {
            win.removeClass('error');
            win.find(".forgot-set").toggleClass("disabled", $(this).val().trim() == "");
        }).focus();
        win.find(".forgot-set").on("click", function (ev) {
            ev.preventDefault();
            if ($(this).hasClass('disabled')) return false;
            var $this = $(this);
            $this.addClass("disabled").addClass("preloader");
            $.get($_AJAX_PATHES.login, {
                action: "forgot",
                login: win.find("input").val(),
            }, function (data) {
                $this.removeClass("disabled").removeClass("preloader");
                if (data.state == "error") {
                    win.addClass("error");
                    win.find(".err-msg").text(data.message);
                }
                if (data.state == "ok") {
                    $(".-win-close").trigger("click");
                    $(".b-check input").val("").focus();
                }
            });
            return false;
        });
        return false;
    });
    $(".b-check .check-done a").on("click", function (ev) {
        ev.preventDefault();
        var $this = $(this);
        if ($this.hasClass("disabled")) return false;
        $this.addClass("disabled");
        $.post($_AJAX_PATHES.order.step1CheckPass, { action: "check-pass", phone: $(".get-phone input").val(), pass: $(".b-check input").val() }, function (data) {
            if (data.state == "error") {
                $this.removeClass('disabled');
                $(".b-check .e-err").text(data.message);
            } else if (data.state == "ok") {
                nextStepCallback();
            }
        });
        return false;
    });
    $("body.phone .client-name input").suggestions({
        token: "0bb9876cbd5d802426809feafddb7d41d173fbff",
        type: "NAME",
        count: 5,
        /* Вызывается, когда пользователь выбирает одну из подсказок */
        onSelect: function (s) {

        }
    });
    if ($('#s-firm .name-enter input').length > 0) {
        $("#s-firm .name-enter input").suggestions({
            token: "0bb9876cbd5d802426809feafddb7d41d173fbff",
            type: "NAME",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (s) {

            }
        });
    }
    $("body.phone div.next-step a").on("click", function (ev) {
        ev.preventDefault();
        if ($(this).hasClass('disabled')) return false;
        if ($('.get-phone .client-name input').length > 0 && $('.get-phone .client-name input').val().trim() == "") {
            $(".get-phone .enter-field").show();
            $('.get-phone .client-name input').focus();
            setTimeout(function () {
                $(".get-phone .enter-field").fadeOut(150);
            }, 3000);
            return false;
        }
        $("body").addClass("ready").add("html").stop().animate({ scrollTop: 0 }, 80);
        var $this = $(this);
        $this.addClass('disabled');
        nextStepCallback = gotoStep4;
        checkPhoneAuth(nextStepCallback);
        return false;
    });


    var numberKeyIsValid = function (ev) {

        if (ev.ctrlKey) return true;

        if (ev.which < 32 || ev.which == 46 || ev.which > 47 && ev.which < 58 && !ev.shiftKey || ev.which >= 37 && ev.which <= 40 || ev.which >= 33 && ev.which <= 36 || ev.which >= 96 && ev.which <= 105) return true;

        return false;

    }



    $("div.source-buttons a").on("click", function (ev) {
        ev.preventDefault();
		  if ($("body").hasClass("ready")) {
				return;
		  }

        if ($(this).data("type") == "phone") {
            if (!$("input#cliend-id").val().match(/^\d{2}\-\d{2}$/)) {
                $(this).parents('div').eq(0).addClass("inputs").find("input").focus();
                return false;
            }
            $("input#cliend-id").prop("disabled", true);
        }
        $(this).parent().addClass("disabled");
        $(this).addClass("preloader");
        $("body").addClass("ready").add("html").stop().animate({ scrollTop: 0 }, 80);
        $.post($_AJAX_PATHES.order.step3, { action: "set-source", source: $(this).data("value"), clientCode: $("input#cliend-id").val() }, function (data) {
            location.href = data.redirect;
        });
        return false;
    });

    $(".set-comment a").on("click", function (ev) {
        ev.preventDefault();
        $(this).parent().addClass("disabled");
        $(this).addClass("preloader");
        $.post($_AJAX_PATHES.order.step3, { action: "set-comment", comment: $('[name="comment"]').val() }, function (data) {
            location.href = data.redirect;
        });
        return false;
    });
    var tm = false;
    $("input#cliend-id").on("change keypress keydown keyup", function (e) {
        if (typeof (e.which) != "undefined" && !numberKeyIsValid(e)) {
            e.preventDefault();
            return false;
        }
        var $this = $(this);
        setTimeout(function () {
            if (!tm && $this.val().match(/^\d{2}\-\d{2}$/)) tm = true, $this.parents('div').find("a[data-type=phone]").trigger("click");
    });
    });

    $("#s-delivery div.next-step a").on("click", function (ev) {
        ev.preventDefault();
        if ($(this).hasClass('disabled')) return false;
        var $this = $(this);
        $this.addClass('disabled');
        $(".next-step a").addClass("x-next");
        $.post($_AJAX_PATHES.order.step1, {
            action: "step1", method: "delivery",
            address: $(".addresses-list li.selected").data("id"),
            coords: $(".addresses-list li.selected").data("coords"),
            elevation: $(".delivery-set .checkbox").hasClass("checked") ? 1 : 0,
            stage: $(".delivery-set .checkbox input").val(),
            elevator: $(".delivery-set .elevator li.selected").data("value"),
            date: $("ul.dates li.selected").data("id")
        }, function (data) {
            if (data.state != "ok") {
                $(".next-step a").removeClass("x-next");
                alert(data.message);
                return checkOutpostPhone();
            } else {
                location.href = data.redirect;
            }
        });
        return false;
    });

    $("#s-delivery .checkbox input").on("click", function (ev) {
        ev.preventDefault();
        ev.stopPropagation();
        return false;
    });
    $("#s-delivery .checkbox").on("click", function (ev) {
        var c = $(this);
        _current_stage_control = $("#s-delivery .checkbox.checked input").val();
        setTimeout(function () {
            $("div.elevator").toggle(c.hasClass('checked'));
            if (c.hasClass("checked")) {
                //                setTimeout(function() {
                $("#s-delivery .checkbox.checked input").focus()[0].select();
                if ($("body").hasClass("--mobile")) {
                    $("html,body").stop().animate({ scrollTop: $("#s-delivery .checkbox.checked input").offset().top - 120 }, 50);
                }

                //                }, 150);
            }
            stage_recalc();
        });
    });

    var stage_recalc = function () {
        var st = $("#s-delivery .checkbox.checked input").val();
        if (!st) st = ""; else st.trim();
        var dt = {
            action: "change-stage",
            stage: st,
            address_id: $(".addresses-list li.selected").data("id"),
            elevator: $(".elevator li.selected").data('value')
        }
        $("#s-delivery .details").addClass("-prld");
        $("#s-delivery .when, #s-delivery .order-done").css({ visibility: "hidden" });
        $.post($_AJAX_PATHES.order.step1, dt, function (data) {
            $("#s-delivery .when, #s-delivery .order-done").css({ visibility: "" });
            $("#s-delivery .details").removeClass("-prld");
            if (data.state != "ok") return alert(data.message);
            _stage_price = data.delivery_price;
            _recalc();
            if (data.method) {
                console.log('------- GetPreliminaryDelivery request dump --------');
                console.log(data.method);
                console.log('------- END dump --------');
            }
        });
    }

    $("#s-delivery .checkbox input").on("blur change focus keyup paste", function () {
        if (_current_stage_control != $("#s-delivery .checkbox.checked input").val()) {
            _current_stage_control = $("#s-delivery .checkbox.checked input").val()
            stage_recalc();
        };
    });

    $(".add-address").on('click', function (ev) {
        ev.preventDefault();
        var w = modalDialog({ class: "add-new-address", width: 430, height: 250 }),
            f = $("<div class='add-form'></div>");
        w.append("<h4>Адрес доставки</h4>");
        f.append("<input type='text' placeholder='Начните вводить адрес' id='full-address'>");
        f.append("<label for='a-city'><span>город</span><input type='text' id='a-city'></label>");
        f.append("<label for='a-street'><span>улица</span><input type='text' id='a-street'></label>");
        f.append("<label for='a-house'><span>дом, корп, стр.</span><input type='text' id='a-house'></label>");
        f.append("<label for='a-flat'><span>офис/кв.</span><input type='text' id='a-flat'></label>");
        w.append(f);
        w.append("<a href='#' class='disabled save-addr'>Доставить сюда</a>");
        w.append("<div class='cancel'><a href='#'>Отмена</a></div>");

        $("#full-address").focus();
        var flatSelected = false;

        w.find("#full-address").suggestions({
            token: "0bb9876cbd5d802426809feafddb7d41d173fbff",
            type: "ADDRESS",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (s) {
                $(".-overlay-win.add-new-address .add-form div.hint").toggleClass("visible", !s.data.flat);
                $("#a-city").val(s.data.city);
                $("#a-street").val(s.data.street_with_type);
                $("#a-house").val(s.data.house);
                $("#a-flat").val(s.data.flat);
                $("#full-address").data("text", s.unrestricted_value);
                flatSelected = s.data.flat;
                $(".-overlay-win.add-new-address .save-addr").data("coords", [s.data.geo_lat, s.data.geo_lon]).toggleClass("disabled", !s.data.house);
                if (!s.data.geo_lat || !s.data.geo_lon) {
                    $.ajax("https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address",
                        {
                            type: "POST",
                            contentType: "application/json",
                            headers: {
                                "Authorization": "Token 0bb9876cbd5d802426809feafddb7d41d173fbff"
                            },
                            data: JSON.stringify({ query: w.find("#full-address").val() + " кв 65535" }),
                            success: function (data) {
                                $(".-overlay-win.add-new-address .save-addr").data("coords", [data.suggestions[0].data.geo_lat, data.suggestions[0].data.geo_lon]);
                            }
                        }
                    );

                }
                console.log('coords', $(".-overlay-win.add-new-address .save-addr").data("coords"));
            }
        });

        if ($("#s-delivery").data("defaultcity")) {
            var def = $("#s-delivery").data("defaultcity");
            f.find("#a-city").val(def);
            f.find("#a-street, #a-house, #a-flat").attr("disabled", null).filter("#a-street").focus();
        }

        w.find(".cancel a").on("click", function (ev) {
            ev.preventDefault();
            w.find(".-win-close").trigger("click");
            return false;
        });

        $("#get-flat-number .buttons a").on("click", function (ev) {
            ev.preventDefault();
            $("#get-flat-number").hide();
            if ($(this).hasClass("y")) return $("#full-address").focus();
            else {
                flatSelected = true;
                setTimeout(function () {
                    w.find(".save-addr").trigger("click");
                }, 5);
            }
            return false;
        });

        w.find(".save-addr").on("click", function (ev) {
            ev.preventDefault();
            if (!flatSelected) {
                $("#get-flat-number").show();
                return false;
            }
            if ($(this).hasClass("disabled")) return false;
            var $this = $(this);
            $this.addClass('disabled').next().hide();
            $(".next-step a").addClass("x-next");
            $.post($_AJAX_PATHES.order.step1, { action: "add-address", city: $("#a-city").val(), street: $("#a-street").val(), house: $("#a-house").val(), flat: $("#a-flat").val(), coords: $this.data("coords"), full: $("#full-address").data("text") },
                function (data) {
                    if (data.state != "ok") {
                        $(".next-step a").removeClass("x-next");
                        alert(data.message);
                        $this.removeClass('disabled');
                        $this.next().show();
                        return false;
                    }
                    var addr = $(".addresses-list ul li");
                    for (var i = 0; i < addr.length; i++) {
                        if (addr.eq(i).data("id") == data.id) {
                            addr.eq(i).trigger("click");
                            checkAddresses();
                            w.find(".-win-close").trigger("click");
                            return false;
                        }
                    }
                    $(".addresses-list ul").append("<li data-id='" + data.id + "' data-coords='" + ($this.data("coords")) + "'>" + data.address + "</li>");
                    $(".addresses-list ul li:last-child").trigger("click");
                    setTimeout(function () {
                        $("#s-delivery .-overlay-win.add-new-address").remove();
                    }, 50);
                    checkAddresses();
                    w.find(".-win-close").trigger("click");
                    $(".next-step a").removeClass("x-next");
                }
            );
            return false;
        });
        var $this2 = $("#a-street");
        $this2.autocomplete({
            source: function (req, res) {
                ymaps.suggest($("#a-city").val() + ", " + req.term, { results: 8 }).then(function (data) {
                    var r = [], pr = {};
                    for (var i = 0; i < data.length; i++) {
                        var x = data[i].displayName.replace(/^(.*?)\,.*$/, '$1');
                        if (data[i].hl.length >= 1) {
                            var hl = data[i].hl[0];
                            x = x.substr(0, hl[0]) + "<b>" + x.substr(hl[0], hl[1] - hl[0]) + "</b>" + x.substr(hl[1]);
                        }
                        if (typeof (pr[x]) != "undefined") continue;
                        r.push({ label: x, value: i });
                        pr[x] = 1;
                    }
                    res(r);
                }).catch(function () {
                    res([]);
                });
            },
            appendTo: $("label[for=a-street]"),
            focus: function (ev, ui) {
                var lb = ui.item.label;
                setTimeout(function () {
                    $this2.val(lb.replace(/\<.*?\>/g, ''));
                });
            },
            select: function (ev, ui) {
                var lb = ui.item.label;
                setTimeout(function () {
                    $this2.val(lb.replace(/\<.*?\>/g, ''));
                    w.find("input").each(function () { this.disabled = false; });
                    $("#a-house").focus();
                    $(".save-addr").removeClass("disabled");
                });
            }
        });

        var $this = $("#a-city");
        $("#a-city").on("keydown paste", function () {
            $(".add-form input").not(this).attr('disabled', 1).val("");
            $(".save-addr").addClass("disabled");
        }).autocomplete({
            source: $_AJAX_PATHES.autocomplete.cities,
            appendTo: $("label[for=a-city]"),
            focus: function (ev, ui) {
                var lb = ui.item.label;
                setTimeout(function () {
                    $this.val(lb.replace(/\<.*?\>/g, ''));
                });
            },
            select: function (ev, ui) {
                var lb = ui.item.label;
                var lv = ui.item.value;
                setTimeout(function () {
                    $this.val(lb.replace(/\<.*?\>/g, ''));
                    $("#a-street")[0].disabled = false;
                    $("#a-street").focus();
                    selectedCity = lv;


                    var $this2 = $("#a-street");
                    $("#a-street").on("keydown paste", function () {
                        $(".add-form input").not(this).not("#a-city").attr('disabled', 1).val("");
                        $(".save-addr").addClass("disabled");
                    });

                });
            }
        });


        return false;
    });


    $(".-x-selector-list").delegate("li", "click", function (ev) {
        if ($(this).hasClass('service')) return true;
        ev.preventDefault();
        $(this).parent().find(".selected").removeClass('selected');
        $(this).addClass("selected");
        return false;
    });

    $("#s-firm .add-new").on("click", function (ev) {
        ev.preventDefault();
        var w = modalDialog({ class: "add-new-organisation", width: 380, height: 210 }),
            f = $("<div class='add-form'></div>");
        w.append("<h4>Начните вводить</h4>");
        w.append("<input id='full-org' type='text' placeholder='Название / ИНН / юр. адрес'>");
        w.append("<div class='org-info'></div>");
        w.append(f);
        w.append("<a href='#' class='disabled save-org'>Счет на это юрлицо</a>");
        w.append("<div class='cancel'><a href='#'>Отмена</a></div>");
        w.find("#full-org").focus();

        w.find("#full-org").suggestions({
            token: "0bb9876cbd5d802426809feafddb7d41d173fbff",
            type: "PARTY",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function (s) {
                console.log(s.data);
                w.find(".org-info").html("");
                w.find(".org-info").append("<p>" + s.data.address.unrestricted_value + "</p>");
                w.find(".org-info").append("<p><span>ОГРН</span>" + s.data.ogrn + "</p>");
                w.find(".org-info").append("<p><span>ИНН</span>" + s.data.inn + "</p>");
                if (typeof (s.data.kpp) != "undefined") w.find(".org-info").append("<p><span>КПП</span>" + s.data.kpp + "</p>");
                if (typeof (s.data.management) == "object" && s.data.management != null && s.data.management.post && s.data.management.name) w.find(".org-info").append("<p><span>" + s.data.management.post + "</span><br>" + s.data.management.name + "</p>");
                $(".-overlay-win .save-org").data("info", s.data).removeClass("disabled");
                setTimeout(function () {
                    var h = w.find(".org-info").height();
                    w.eq(0).css({ height: 210 + h, marginTop: ((210 + h) / -2) + "px" });
                });
            }
        });

        w.find(".cancel a").on("click", function (ev) {
            ev.preventDefault();
            w.find(".-win-close").trigger("click");
            return false;
        });
        $("#a-inn, #a-kpp")
            .on("keydown keyup", function (ev) {
                if (ev.which == 13 && ev.type == "keydown") {
                    return true;
                }
                checkOutpostPhone();
                if (!numberKeyIsValid(ev)) { ev.preventDefault(); return false }
            });

        w.find("input").on("keyup paste change", function (ev) {
            $(".save-org").removeClass('disabled');
            w.find("input").each(function () {
                if ($(this).val().trim() == "") $(".save-org").addClass('disabled');
            });
            try {
                var inn = $("#a-inn").val().trim(), kpp = $("#a-kpp").val().trim();
                if (inn.length != 10 && inn.length != 12 || kpp.length != 9) $(".save-org").addClass('disabled');
            } catch (e) { }
        });

        w.find(".save-org").on("click", function (ev) {
            ev.preventDefault();
            if ($(this).hasClass('disabled')) return false;
            $(this).addClass('disabled').next().hide();
            var $this = $(this);
            $(".next-step a").addClass("x-next");
            $("body").addClass("ready").add("html").stop().animate({ scrollTop: 0 }, 80);
            $.post($_AJAX_PATHES.order.step2, { action: "new-org", data: $this.data("info") },
                function (data) {
                    if (data.state != "ok") {
                        $("body").removeClass("ready");
                        $(".next-step a").removeClass("x-next");
                        alert(data.message);
                        $this.removeClass('disabled');
                        $this.next().show();
                        return false;
                    }
                    var li = $("<li data-value='" + data.id + "'>" + data.name + "</li>");
                    li.insertBefore("#s-firm .add-new.service");
                    li.trigger("click");
                    w.find(".-win-close").trigger("click");
                    $("body").removeClass("ready");
                    $(".next-step a").removeClass("x-next");
                }
            );
            return false;
        });

        return false;
    });

    $("#s-physic .next-step a, #s-firm .next-step a").on("click", function (ev) {
        ev.preventDefault();
        if ($(this).hasClass('disabled')) return false;
        var $this = $(this);
        $this.addClass('disabled');

        var data = { action: "step2", mode: "physic" }, k = $(this).parents("section").eq(0).find(".-x-selector-list").data("name");
        data[k] = $(this).parents("section").eq(0).find("li.selected").eq(0).data("value");
        if ($(this).parents("#s-firm").length > 0) {
            data.mode = "firm";
            data.email = $(".email-enter.-email input").val();
            if ($(".email-enter.-name").length) {
                data.name = $(".email-enter.-name input").val();
            } else {
                data.name = $(".name-enter input").val();
            }
        }
        $(".next-step a").addClass("x-next");
        $("body").addClass("ready").add("html").stop().animate({ scrollTop: 0 }, 80);
        $.post($_AJAX_PATHES.order.step2, data, function (data) {
            if (data.state != "ok") {
                $("body").removeClass("ready");
                $(".next-step a").removeClass("x-next");
                $this.removeClass("disabled");
                alert(data.message);
                return false;
            }
            location.href = data.redirect;
        });
        return false;
    });

    if ($(".addresses-list li").length > 0) {
        $(".addresses-list li.selected").trigger("click");
    }

    setTimeout(function () {
        scrl = true;
    }, 500);
});