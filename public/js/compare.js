$(function() {
    var BH  =0, MMX = 0;
    var resizeTable = function() {

        var mw = ($(".compared-item").length+1)*240;
        if (mw>window.innerWidth-80) mw = window.innerWidth-80;
        $("main").css({maxWidth: mw, margin: "auto"});


        var _crh = 10;
        if (!$(document.body).hasClass("init-c")) {
            var setr = function() {
                var maxh = $("aside.params .filters").outerHeight();
                $(".compared-item .description").each(function() { if ($(this).outerHeight()>maxh) maxh=$(this).outerHeight() });
                $("aside.params .filters, .compared-item .description").css({height: maxh+40});
                $(document.body).addClass('init-c');
                BH = maxh+40;
            }
            setTimeout(setr, 100);
        }
        var N = Math.round($("main").width()/240), w0 = Math.round($("main").width()/N);
        $("section.items").css({maxWidth: "calc(100% - "+w0+"px)"});
        $("aside.params, .compared-item, .static-header ul li").css({width: w0}).find(">ul li").css({height: ""});
        setTimeout(function() {
            MMX = BH;
            var mxh = [], ttl = $("aside.params>ul li"), cl = $(".compared-item>ul");
            for (var i=0; i<ttl.length; i++) {
                mxh[i] = ttl.eq(i).height();
                for (var j=0; j<cl.length; j++) {
                    var h = cl.eq(j).find("li").eq(i).height();
                    if (mxh[i]<h) mxh[i] = h;
                }
            }
            for (var i=0; i<ttl.length; i++) {
                ttl.eq(i).css({height: mxh[i]}).addClass('line-'+i);
                for (var j=0; j<cl.length; j++) {
                    cl.eq(j).find("li").eq(i).css({height: mxh[i]}).addClass('line-'+i);
                }
                MMX += mxh[i]+21;
            }

            $(".static-header ul").css({ marginLeft: $(".compared-item").eq(0).offset().left, width: $("section.items").width() });
        }, 150);

        $(".table-board").toggleClass("visible", $("section.items").width()<$("section.items")[0].scrollWidth);
    }

    var onscroll = function() {
        var t = $(window).scrollTop();
        $(".static-header").toggleClass('visible', t>=$('.compared-item').eq(0).find('.title').offset().top);
    }


    window.addEventListener("resize", resizeTable);
    window.addEventListener("scroll", onscroll);
    $(".compared-item>ul li, aside.params>ul li").on("mouseenter", function() {
        var l = this.className.replace(/\D/g, '')*1;
        $("li.hovered").removeClass('hovered');
        $("li.line-"+l).addClass('hovered');
    }).on("mouseleave", function() {
        var l = this.className.replace(/\D/g, '')*1;
        $("li.hovered").removeClass('hovered');
    });

    $("aside.params .filters span").on("click", function(ev) {
        var t = $("li.nd");
        if ($(this).parent().hasClass('diff')) {
            for(var i=0; i<t.length; i++) {
                var l = t.eq(i)[0].className.replace(/\D/g, '')*1;
                $("li.line-"+l).addClass("hidden");
            }
        }
        if ($(this).parent().hasClass('all')) {
            $("section.items").css({minHeight: MMX});
            $("li.hidden").removeClass('hidden');
            setTimeout(function() {
                $("section.items").css({minHeight: ""});
            }, 500);
        }
        $(this).parents("ul").find(".selected").removeClass("selected");
        $(this).parent().addClass('selected');
    });

    $(".compared-item").each(function() {
        var li = $("<li data-id='"+$(this).data("id")+"'><a href='#' class='del'></a><span>"+$(this).find(".title").html()+"</span></li>");
        $(".static-header ul").append(li);
    });

    $(".table-board").on("click", function(ev) {
        ev.preventDefault();
        var s = -1, vl = Math.ceil($("section.items").width()/$(".compared-item").eq(0).width());
        if ($(this).hasClass("right")) s=1;
        var l = Math.ceil($("section.items").scrollLeft()/$(".compared-item").eq(0).width());
        l+=s;
        if (l<0) l = 0;
        if ($(".compared-item").length<=l) l = $(".compared-item").length-1 ;
        $(".table-board.right").toggle($(".compared-item").length-vl-1>=l); 
        $(".table-board:not(.right)").toggle(l>0);
        var sl = l*$(".compared-item").eq(0).width();
        $("section.items, .static-header ul").stop().animate({ scrollLeft: sl }, 300);
        return false;
    });

    $(".table-board:not(.right)").hide();

    resizeTable();

    $(".compared-item .delete, .static-header ul li .del").on("click", function(ev) {
        ev.preventDefault();
        var id = $(this).hasClass("delete") ? $(this).parents(".compared-item").data("id") : $(this).parents("li").data("id");
        var $this = $(this);
        Basket.addCompare(id, false)
              .then(function(state) {
                  $(".compared-item").each(function() {
                        if ($(this).data("id")==id) $(this).fadeOut(500, function() { $(this).remove(); });
                  });
                  $(".static-header ul li").each(function() {
                        if ($(this).data("id")==id) $(this).fadeOut(500, function() { $(this).remove(); });
                  });
                  setTimeout(function() {
                      resizeTable();
                      if ($(".compared-item").length==0) location.href='/';
                  }, 550);
              });
        return false;

    });
});