$(function() {

    $("div.add a").on("click", function(ev) {
        ev.preventDefault();
        win = $("<div class='order-window'><h3><a href='#' class='close'></a><span>Добавление организации</span></h3></div>");
        win.append("<div class='org-add-form'><input type='text' class='name' placeholder='Название организации'><input type='text' class='address' placeholder='Юридический адрес'><input type='text' class='inn' maxlength='12' placeholder='ИНН'><input type='text' class='kpp' maxlength='12' placeholder='КПП'></div>");
        win.append("<p>Остальные реквизиты будут заполнены<br>автоматически при оплате</p>");
        win.append("<div class='buttons'><a href='#' class='done'>Сохранить организацию</a><a href='#' class='cancel'>Отмена</a></div>");

        $(document.body).append(win);
        lastScroll = $(window).scrollTop();
        $(window).scrollTop(0);
        $("main, footer").hide();
        $(".cabinet-menu").css({visibility: "hidden"});

        win.find(".cancel").on("click", function(ev) {
            ev.preventDefault();
            win.find("h3 .close").trigger("click");
            return false;
        });

        win.find(".inn, .kpp").on("keyup keydown", function(ev) {
            if (ev.which==13 && ev.type=="keydown") {
                return true;
    		}
            if (!numberKeyIsValid(ev)) { ev.preventDefault(); return false }
        }).on("change", function(ev) {
            if ($(this).val().match(/\D/)) $(this).val($(this).val().replace(/\D/g, ''));
        }).on("paste", function(ev) {
            var $this = $(this);
            setTimeout(function() {
                if ($this.val().match(/\D/)) $this.val($this.val().replace(/\D/g, ''));
            }, 5);
        });

        win.find(".done").on("click", function(ev) {
            ev.preventDefault();
            if ($(this).hasClass('disabled')) return false;
            var ok = true;
            var f = win.find("input");
            for (var i=0; i<f.length; i++) {
                if (f.eq(i).val().trim()=="") ok=false;
            }
            if (!ok) return alert("Пожалуйста, заполните все поля");        
            $(this).addClass('disabled');
            $.post($_AJAX_PATHES.cabinet.requisites, { action: "add", name: $("input.name").val(), address: $("input.address").val(), inn: $("input.inn").val(), kpp: $("input.kpp").val() },
                function(data) {
                    if (data.state=="error") return alert(data.message);
                    win.find("h3 .close").trigger("click");
                    location.reload();
                });
            return false;
        });

        return false;
    });


});