calcPositions = true;
window._less_queue = [];
window._less_queue_step_flag = false;
window._less_queue_step = function() {
    if (typeof(window._less_queue.length)!="undefined" && window._less_queue.length>0) {
        var st = window._less_queue.shift();
        if (typeof(st.mode)=="undefined") showModalLess(st);
		  else if (st.mode=="redates") showModalDates(st);
        else {
            switch(st.mode) {
                case "redirect":
                    setTimeout(function() { location.href = st.url; }, 150);
                break;
            }
        }
    }
}

$(function() {

    $(".order-button a").each(function() {
		var _ = $(this);
		_.data('href', _.attr('href'));
		_.attr('data-href', _.attr('href'));
		_.prop('href', '#');
    }).on("click", function(ev) {
        ev.preventDefault();
        if ($(this).hasClass("disabled")) return false;
        $(".order-button a").addClass("disabled");
        var it = $("ul.basket-items li"), data = { action: "check-order", items: {} }, $this = $(this);
        for (var i=0; i<it.length; i++) {
            data.items[it.eq(i).data("id")] = it.eq(i).find(".count input").val();
        }
        $.post($_AJAX_PATHES.basket.check, data, function(data) {
            $(".order-button a").removeClass("disabled");
            if (data.state=="ok") {
                location.href = $this.data("href");
            } else if (data.state=="less") {
                $("body").removeClass("-pre-check-basket");
                window._less_queue = data.modals;
                window._less_queue.push({ mode: "redirect", "url": $this.data("href") });
                _less_queue_step();
            }
        });
        return false;
    });
    if ($("body").hasClass("-pre-check-basket")) {
        if ($(".basket-items li").length>1) {
            $("body").removeClass("-pre-check-basket")
        } else {
            setTimeout(function() {
                $(".order-button a").trigger("click");
            }, 10);
        }
    }

    var totalRecalc = function() {
        var total = 0, p = $(".basket-items li:not(.disabled) .price"), p2 = $(".basket-items li.service .price");
        for (var i=0; i<p2.length; i++) {
            var x = p2.eq(i).data("price");
            p2.eq(i).find("strong").text(formatNum(x));
        }
        for (var i=0; i<p.length; i++) {
            total += p.eq(i).text().replace(/\D/g, '')*1;
        }
        var suf = "ов", cnt = p.length;
        if (!calcPositions) {
            cnt = 0;
            $("ul.basket-items li .price .count input").each(function() { cnt+= $(this).val()*1; });
        }
        var cnt2 = cnt%100;
        if (cnt2==1 || cnt2>20 && (cnt2%10)==1) suf="";
        if ((cnt2<10 || cnt2>20) && (cnt2%10)>1 && (cnt2%10)<5) suf="а";
        $("body.basket.full .basket-header h2 .total-suffix").text(suf);
        $("body.basket.full .basket-header h2 .total-count").text(cnt);
        $(".basket-header.footer h2 strong, .basket-header .amount, .nav-bottom-panel .basket strong").text(formatNum(total));
    }

    setTimeout(function() {
        $("ul.basket-items li .price .count a.change").on("click", function() {
            totalRecalc();
        });
    }, 50);



    $(".basket-items .count input").each(function() { setInputButtons($(this)); });

    $(".basket-items .delete").on("click", function(ev) {
        ev.preventDefault();
        var item = $(this).parents("li");
        Basket.remove($(this).parents("li").data('id')).then(function(data) {
            item.fadeOut(200, function() {
                var p = $(this).parents("ul");
                $(this).remove();
                totalRecalc();
                if (p.find("li").length==0) location.href = $_AJAX_PATHES.basket.onEmptyRedirect;
            });
        }).catch(function(data) {
            alert(data.message);
        });
        return false;
    });

    $(".basket-items li.service.disabled .board a").on("click", function(ev) {
        ev.preventDefault();
        if ($(this).hasClass("cancel")) {
            var $li = $(this).parents("li");
            Basket.serviceCancel($(this).parents("li").data('id'))
                .then(function(data) {
                    $li.fadeOut(200, function() {
                        $li.remove();
                    });
                })
                .catch(function(data) {
                    alert(data.message);
                });
        }
        if ($(this).hasClass("add")) {
            var $li = $(this).parents("li");
            Basket.add($(this).parents("li").data('id'))
                .then(function(data) {
                    $li.removeClass('disabled');
                    totalRecalc();
                })
                .catch(function(data) {
                    alert(data.message);
                });
        }

        return false;
    });
});