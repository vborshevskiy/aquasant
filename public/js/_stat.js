$(function() {
    var data = {};
        data.navigator = {};
        for (var i in navigator) {
            if (typeof(navigator[i])!='object' && typeof(navigator[i])!='function') data.navigator[i] = navigator[i];
        }
        data.screen = {};
        for (var i in screen) {
            if (typeof(screen[i])!='object' && typeof(screen[i])!='function') data.screen[i] = screen[i];
        }
        data.document = { width: $(window).width(), height: $(window).height() }


        $.post("!.stat.php", { a: "save-stat", data: data }, function(data) {
            if (data.state!='ok') {
                $("p").text("Something went wrong =(");
            } else {
                $("p").text("Thank you =)");
            }
        });
});