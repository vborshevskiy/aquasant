$(function() {

    var feedbackValidate = function(ev, xset) {
        var lf = $("form#feedback input, form#feedback textarea"), e = false;
        for (var i=0; i<lf.length; i++) {
            if (lf.eq(i).val().trim()=="" && !e) {
                e = lf.eq(i);
                if (xset===1) {
                    lf.eq(i).addClass("error");
                }
            } else {
                lf.eq(i).removeClass('error');
            }
        }
        $("form#feedback").removeClass("error");
        if ($("form#feedback .selector li.current").length==0) e=true;
        if (e) $("form#feedback input[type=submit]")[0].setAttribute("disabled", true);
        else   $("form#feedback input[type=submit]")[0].removeAttribute("disabled", true);
        if (typeof(ev)=="undefined") return e;
    }

    $(".selector .expander").on("click", function(ev) {
        ev.preventDefault();
        $(this).parents(".selector").toggleClass("expanded");
        return false;
    });
    $(".selector ul li").on("click", function(ev) {
        $(this).parents("ul").find(".current").removeClass("current");
        $(this).addClass("current");
        $(this).parents(".selector").find(".expander").text($(this).text());
        $(this).parents(".selector").removeClass("expanded");
        $("input#send-to").val($(this).data("value"));
        feedbackValidate();
    });
    $("form#feedback input[type=submit]").on("click", function(ev) {
        ev.preventDefault();
        var e = feedbackValidate(false, 1);
        if (e) {
            $("form#feedback").addClass("error");
            $("form#feedback .error-message").text("Заполните все поля!");
            return false;
        }
        this.setAttribute("disabled", true);
        $.get($_AJAX_PATHES.feedback, { 
            action: "feedback",
            sendTo: $("input#send-to").val(),
            message: $("textarea[name=message]").val(),
            username: $("input[name=username]").val(),
            email: $("input[name=email]").val()
        }, function(data) {
            if (data.state=="error") {
                $("form#feedback").addClass("error");
                $("form#feedback .error-message").text(data.message);
            }
            if (data.state=="ok") {
                $("form#feedback .wrapper>*").css({visibility: "hidden"});
                $("form#feedback .wrapper").append("<div class='done'>Сообщение отправлено, спасибо!<br>Ответим в ближайшее время.</div>");
            }
        });
        return false;
    });
    $("form#feedback input, form#feedback textarea").on("focus blur keydown keyup change paste", feedbackValidate);

});