$(function() {
    $("form#login-form .field input").eq(0).focus();
    var validateLoginForm = function(ev, setErr) {
        var e = false;
        if (typeof(ev)!="undefined") $("form#login-form").removeClass('error pass-sent');
        $("form#login-form .field input").each(function() {
            if ($(this).val().trim()=="") {
                e=$(this);
                if (setErr===1) $(this).addClass("error");
            } else if ($(this).hasClass("error")) {
                var etm = $(this).data("errortime");
                if (typeof(etm)=="undefined" || etm == -1) etm = 20;
                if (etm>0) {
                    etm--;
                    $(this).data("errortime", etm)
                }
                if (etm==0) {
                    $(this).removeClass("error");
                    $(this).data("errortime", -1);
                }
            }
        });
        if (e)  $("form#login-form input[type=submit]")[0].setAttribute("disabled", true);
        else    $("form#login-form input[type=submit]")[0].removeAttribute("disabled");
        if (setErr===1) return e;
        else return true;
    }

    $("form#login-form .field input").on("change keyup keydown paste focus blur", validateLoginForm);
	 setInterval(validateLoginForm, 250);

    $("form#login-form input[type=submit]").on("click", function(ev) {
        ev.preventDefault();
        var e = validateLoginForm(undefined, 1);
        if (e) return false;
		  var $this = $(this);
        $this.attr("disabled", true).addClass("preloader");
		  var params = { 
            action: "login",
            login: $("#field-login").val(),
            password: $("#field-password").val(),
        };
        $.get($_AJAX_PATHES.login, params, function(data) {
				$this.attr("disabled", null).removeClass("preloader");
            if (data.state=="error") {
                $("form#login-form").addClass("error");
                $("form#login-form .error-message").text(data.message);
            }
            if (data.state=="ok") {
                location.href = data.redirect;
            }
        });
        return false;
    });

    /* *** */

    $(".forgot-button").on("click", function(ev) {
        ev.preventDefault();
        var win = modalDialog({ class: "forgot-win", width: 320, height: 160 });
        win.append("<strong>Мобильный телефон или e-mail</strong>");
        win.append("<input type='text' class='forgot-input'>");
        win.append("<a href='#' class='disabled forgot-set'>Выслать пароль</a>");
        win.append("<div class='err-msg'></div>");
        win.find("input").on("keyup keydown blur focus paste change", function(ev) {
            win.removeClass('error');
            win.find(".forgot-set").toggleClass("disabled", $(this).val().trim()=="");
        }).focus();
        win.find(".forgot-set").on("click", function(ev) {
            ev.preventDefault();
            if ($(this).hasClass('disabled')) return false;
				var $this = $(this);
            $this.addClass("disabled").addClass("preloader");
            $.get($_AJAX_PATHES.login, { 
                action: "forgot",
                login: win.find("input").val(),
            }, function(data) {
					$this.removeClass("disabled").removeClass("preloader");
                if (data.state=="error") {
                    win.addClass("error");
                    win.find(".err-msg").text(data.message);
                }
                if (data.state=="ok") {
                    $("#field-login").val(win.find("input").val());
                    $(".-win-close").trigger("click");
                    $("#field-password").val("").focus();
                    $("form#login-form").addClass("pass-sent");
                }
            });
            return false;
        });
        return false;
    });
});