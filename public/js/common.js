$SLIDESHOWINTERVAL = 4000;
$MINVIDEOSPEED = 3072;
$MINVIDEOLOW   = 3071;
$CLIPS = [];
$_started = false;

$_AJAX_PATHES = {
    feedback: "/contacts/mail/",
    login: "/login/login/",
    compareManage: "/compare/update/",
    catalogue: {
        getFilterInfo: window.location,
        resetSelectorFilter: window.location
    },
    basket: {
        set_order_link: "/basket/?precheck=1",
        basket_link: "/basket/",
        add: "/basket/update/",
        remove: "/basket/update/",
        update: "/basket/update/",
        check:  "/basket/check/",
        get: "/!ajax/basket.php",
        serviceCancel: "/!ajax/basket.php",
        setLessVariant: "/basket/set-pickup-method/",
        onEmptyRedirect: "/basket/"
    },
    cabinet: {
        orders: "/cabinet/reserves/",
        extendOrder: "/!ajax/cabinet.orders.php",
        claimUpload: "/!ajax/cabinet.claim.php",
        claimLoadedRemove: "/!ajax/cabinet.claim.php",
        claimSend: "/!ajax/cabinet.claim.php",
        claimsList: "/!ajax/cabinet.claim.php",
        getClaim:  "/!ajax/cabinet.claim.php",
        documentsList: "/cabinet/documents/",
        notifications: "/!ajax/cabinet.notif.php",
        requisites:  "/cabinet/requisites/",
        profile:  "/cabinet/change-profile/",
		  checkFailed: "/cabinet/reserves/?action=check-failed"
    },
    autocomplete: {
        cities: "/post-delivery/search-city/",
        streets: "/!ajax/a.city.php",
        search: "/search/autocomplete/"
    },
    order: {
        step1CheckPhone: "/order/check-phone/",
        step1CheckPass: "/order/login/",
        step1: "/order/change-order/",
        step2: "/order/change-order/",
        step3: "/order/change-order/",
        delivery: "/order/delivery/"
    },
	 item: {
        getCharacteristics: "/item/properties/"
    },
};

$(function() {

	$("nav.catalogue, nav.catalogue-main-page").css({ visibility: "" });

	var cfi = false;
    var checkFail = function() {
        $.post($_AJAX_PATHES.cabinet.checkFailed, { action: "check-failed" }, function(data) {
            if (typeof(data.state)=="undefined" || data.state=='dontcheck') {
                if (cfi) clearInterval(cfi);
                $(".nav-bottom-panel .login").removeClass("failed");
                return false;
            }
            if (data.state=="ok") {
                $(".nav-bottom-panel .login").removeClass("failed");
            } else {
                $(".nav-bottom-panel .login .failed-msg em").text(data.count);
                $(".nav-bottom-panel .login").addClass("failed");
            }
            if (!cfi) {
                cfi = setInterval(checkFail, 6e4);
            }
        });
    }
    checkFail();

	 $("input,textarea").on("focus", function(e) {
        if ($("body").hasClass("--mobile")) 
            $('.nav-bottom-panel').css({ visibility: "hidden" });
        else $('.nav-bottom-panel').css({ visibility: "visible" });
    }).on("blur", function(e) {
        $('.nav-bottom-panel').css({ visibility: "visible" });
    });

		$("[data-placeinfo]").each(function() {
        if ($(this).css("position")=="static") $(this).css({position:"relative"});
        $(this).append("<div class='-placeinfo'>"+$(this).data("placeinfo")+"</div>");
        $(this).attr("data-placeinfo", null).css({ cursor: "pointer" });

        $(this).on("click", function(e) {
            $(".place-info-expanded").removeClass("place-info-expanded");
            $(this).addClass("place-info-expanded");
        });
        $(window).on("click", function(e) {
            if ($(e.target).parents(".place-info-expanded").length==0 && $(e.target).find(">.-placeinfo").length==0) $(".place-info-expanded").removeClass("place-info-expanded");
        });
    });

    var showSlideShow = function() {
        var N = 1;
        if (window.innerWidth<600) N=2;
        $.get("/!ajax/gv.php?low=-"+N, {}, function(data) {
            $("body>header .start .holder>*:first-child").addClass("--rm");
            setTimeout(function() {
                $("body>header .start .holder .--rm").remove();
            }, 3000);
            for (var i=0; i<data.length; i++) {
                var img = $("<div "+(i<2?"":"data-")+"style='background-image:url(\""+data[i]+"\");' class='-bg"+(i==0?' current':'')+"'></div>");
                    img.appendTo("body>header .start .holder");
            }


            setInterval(function() {
                var c = $("body>header .start .holder .-bg.current"), n = c.next();
                if (n.length==0) n=c.parent().find(".-bg").eq(0);
                n.addClass('current');
                showNextSlogan();
                var z = n.next();
                if (z.hasClass('-bg')) {
                    z.attr("style", z.data("style"));
                }
                setTimeout(function() {
                    c.removeClass('current');
                }, 1000);
            }, $SLIDESHOWINTERVAL);
        });
    }

    var showVideoShow = function() {
        $("body>header .start .holder>*:first-child").addClass("--rm");
        setTimeout(function() {
            $("body>header .start .holder .--rm").remove();
        }, 3000);
        var shuffleVideo = function() {
            var v = $("body>header .start .holder video");
            for (var i=0; i<v.length; i++) {
                if (Math.random()>=.5) {
                    v.eq(i).insertBefore("body>header .start .holder .over");
                } else {
                    v.eq(i).prependTo("body>header .start .holder");
                }
            }
        }
        $("body>header .start .holder").append("<div class='over'></div>");
        for (var i=0; i<$CLIPS.length; i++) {
                (function(i) {
                    setTimeout(function() {
                            var vid = $("<video muted='muted'><source src='"+$CLIPS[i]+"'></source></video>");
                                        vid.insertBefore("body>header .start .holder .over");
                                        vid[0].addEventListener("ended", function() {
                                            $(this).removeClass("visible");
                                            var n = $(this).next();
                                            if (n.length==0 || n[0].tagName.toLowerCase()!="video") {
                                                shuffleVideo();
                                                n=$("body>header .start .holder video").eq(0);
                                            }
                                            n.addClass("visible")[0].play();
                                            showNextSlogan();
                                        });
                               }, i*1000);
                })(i);
        }
        setTimeout(function() {
            $("body>header .start .holder video").eq(0).addClass('visible');
            $("body>header .start .holder video")[0].play();
        }, 100);
    }

    var sTexts = [
            "«Аквасант» — оптово-розничная база сантехники",
            "Теперь мы работаем и&nbsp;с&nbsp;физ. лицами",
            "Отгрузка со склада в&nbsp;день заказа",
            "Принимаем наличные, безналичные и&nbsp;е-бабки",
            "Самовывоз с&nbsp;оптово-розничного склада",
            "5834 душевые кабины в&nbsp;наличии",
            "1580 ванн в&nbsp;наличии на&nbsp;складе",
            "3725 смесителей по&nbsp;оптовым ценам",
            "Отгружаем в&nbsp;регионы по&nbsp;всей стране",
            "Выставочный павильон — 1500 м<sup>2</sup>",
            "База: 985 брендов со&nbsp;всего мира",
            "Удобный подъезд на&nbsp;базу со МКАДа",
            "Установка сантехники специалистами",
            "Мебель для ванной: 10&nbsp;000+ вариантов",
            "30 честных дней на&nbsp;возврат",
            "Собственный сервисный центр",
            "Продаем \"физикам\" в кредит"
    ], stx = 1;
    var showNextSlogan = function() {
        $("body>header section.start .slogan p").html(sTexts[stx++]);
        if (stx==sTexts.length) stx=0;
    }

    var getConnectionInfo = function() {
        var st;//, sx = sessionStorage.getItem("_s");
        return new Promise(function(resolve) {
            var tn = new Image(), st = performance.now();
                tn.onload = function() {
                    var d = performance.now()-st, l = 491524,
                    sx = Math.floor(9000*l/(performance.now()-st))*1.25;
//                    sessionStorage.setItem("_s", sx);
                    resolve(sx);
                }
            tn.src = "/images/noise.png?n="+Math.random();
            $_started = setTimeout(function() {
                    $_started = true;
                    showSlideShow();
            }, 5000);
        });
    }

    var reorderVideo = function(arr) {
        var preset = ['w1', 's36', 'w29', 's42', 'w2', 's113'], pr = [], result = [];
        for (var i=0; i<arr.length; i++) {
            var x = preset.indexOf(arr[i].replace(/^.*\/(.*?)\.\w+$/, '$1'));
            if (x>-1) {
                pr[x]=arr[i];
                arr.splice(i, 1);
                i--;
            }
        }
        arr.sort(function(a, b) {
            return Math.round(Math.random()*2.8 - 1.4);
        });
        for (i in pr) result.push(pr[i]);
        while (arr.length>0) result.push(arr.shift());
        return result;
    }


    getConnectionInfo().then(function(speed) {
        if ($_started===true) return true;
        if ($_started) clearTimeout($_started);
        $(document.body).append("<div id='test-www' style='position: fixed; right: 10px; top: 50px; background: rgba(0,0,0,.7); padding: 5px 15px; color: rgba(255,255,255,.5);z-index:1000; opacity:0;'>["+window.innerWidth+"; "+Math.floor(speed)+"]</div>");
        $("body.main>header section.start form#search-top input[type=text]").on("keyup change", function() {
            if ($(this).val()=="100") $("#test-www").css({opacity: 1});
        });
        if (window.innerWidth<600) { return showSlideShow(); }
        if (speed<$MINVIDEOLOW*1024 || !$('body').hasClass('main')) { return showSlideShow(); }
        if (speed<$MINVIDEOSPEED*1024) { console.log("Low video"); } else { console.log("Normal video"); }
        $.get("/!ajax/gv.php?low="+(speed<$MINVIDEOSPEED*1024 ? 1: 0), {}, function(data) {
            $CLIPS = reorderVideo(data);
            showVideoShow();
        });
    });


    var rscls = function() {
        var w = $(window).width(), pl = "поиск по оптово-розничной базе";
        $("body").toggleClass("--mobile", w<=950);
		  if (w<=1050 && w>400) {
            pl = "поиск сантехники";
        }
        $("body>header section.start form#search-top input[type=text]").attr("placeholder", pl);
    };
    rscls();
    window.addEventListener("resize", rscls);


    $("body:not(.main)>header nav.catalogue>strong").on("click", function(ev) {
        if ($("body").hasClass("--mobile")) {
            $("body>header nav.catalogue").toggleClass("-hover");
        };
    });
    $("body:not(.main)>header nav.catalogue>strong").on("mouseenter", function(ev) {
        if (!$("body").hasClass("--mobile")) {
            $("body>header nav.catalogue").addClass("-hover");
        };
    }).parents("nav.catalogue").on("mouseleave", function(ev) {
        if (!$("body").hasClass("--mobile")) {
            $("body>header nav.catalogue").removeClass("-hover");
        };
    });

    if ($('html').hasClass('first') && !$("body").hasClass("cabinet")) {
        setTimeout(function() {
            $('html').removeClass('first').addClass("uc-shown");
        }, 2000);
    }
    $("#site-is-under-construction .close-block").on("click", function(ev) {
        ev.preventDefault();
        $('html').removeClass('uc-shown');
        return false;
    });

    $("#top-panel").appendTo(document.body);
    assignAutoComplete($("section.start form#search-top input[type=text]"), $_AJAX_PATHES.autocomplete.search);
    $("section.start form#search-top input[type=text]")
        .on("focus", function() {
            $(this).parent().addClass("focused");
        })
        .on("blur", function() {
            $(this).parent().removeClass("focused");
        });


    $("ul.goods li").delegate(".image, .board", "click", function(ev) {
        if (ev.ctrlKey) {
            return true;
        }
        $(this).parents("li").find("a.title")[0].click();
        return true;
    });

	var $yamap, _maps = $('#map, #map-mobile');
	_maps.each(function() {
	   $yamap = $(this);
    	if($yamap.length > 0){
        	var map;
        	function init(){     
            	map = new ymaps.Map("map", {
                	center: $yamap.data("center") ? $yamap.data("center") : [55.650643,37.830976],
                	zoom: $yamap.data('zoom') ? $yamap.data('zoom') : 15,
    				controls: ['fullscreenControl', 'rulerControl', 'typeSelector']
            	});
                map.behaviors.disable("scrollZoom");
                map.behaviors.disable("drag");
            	placemark = new ymaps.Placemark(map.getCenter(), {balloonContentBody: $yamap.data("balloon") ? $yamap.data("balloon"): 'Aquasant<br>СК «Восточные ворота»'});
            	map.geoObjects.add(placemark);
            	placemark.balloon.open();
        	}
        	ymaps.ready(init);
     	}

    try {
            !function(a){function f(a,b){if(!(a.originalEvent.touches.length>1)){a.preventDefault();var c=a.originalEvent.changedTouches[0],d=document.createEvent("MouseEvents");d.initMouseEvent(b,!0,!0,window,1,c.screenX,c.screenY,c.clientX,c.clientY,!1,!1,!1,!1,0,null),a.target.dispatchEvent(d)}}if(a.support.touch="ontouchend"in document,a.support.touch){var e,b=a.ui.mouse.prototype,c=b._mouseInit,d=b._mouseDestroy;b._touchStart=function(a){var b=this;!e&&b._mouseCapture(a.originalEvent.changedTouches[0])&&(e=!0,b._touchMoved=!1,f(a,"mouseover"),f(a,"mousemove"),f(a,"mousedown"))},b._touchMove=function(a){e&&(this._touchMoved=!0,f(a,"mousemove"))},b._touchEnd=function(a){e&&(f(a,"mouseup"),f(a,"mouseout"),this._touchMoved||f(a,"click"),e=!1)},b._mouseInit=function(){var b=this;b.element.bind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),c.call(b)},b._mouseDestroy=function(){var b=this;b.element.unbind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),d.call(b)}}}(jQuery);
        } catch(e){}

	});

    window.addEventListener("keydown", function(ev) {
        if (ev.which==27) {
            if ($(document.body).hasClass("overlayed")) $(".-win-close").trigger("click");
        }
    });

    var tmet = -1;
    $(".top-menu-expander").on("mouseenter", function(ev) {
        clearTimeout(tmet);
        $(this).addClass("-hover");
    }).on("mouseleave", function(ev) {
        clearTimeout(tmet);
        var $this = $(this);
        tmet = setTimeout(function() {
            $this.removeClass("-hover");
        }, 450);
    });


    $(".bottom-menu").each(function() {
        $(this).css({maxHeight: $(this).find("li").eq(0).height()*Math.ceil($(this).find("li").length/2) });
    });

    $("nav.catalogue>.items>ul>li").each(function() {
        if ($(this).find("ul").length>0){
            $(this).addClass('expandable');
        }
    });

    window.addEventListener("click", function(ev) {
        var t = $(ev.target);
        if (!t.hasClass('-hover') && t.parents('.-hover').length==0) $("body>header nav.catalogue").removeClass("-hover");
        if (t.hasClass('expanded') || t.parents('.expanded').length>0) return true;
        $(".expanded:not(.changed)").removeClass('expanded');
        $("main").removeClass('order-list-expanded');
    });

    $(".expandable>a").on("click", function(ev){
        ev.preventDefault();
        $("nav.catalogue>.items .expanded").not($(this).parent()).removeClass('expanded');
        $(this).parent().toggleClass('expanded');
        return false;
    });

    $(".-x-selector>.expander").on("click", function(ev) {
        ev.preventDefault();
        $(".-x-selector").not($(this).parent()).removeClass('expanded');
        $(this).parent().toggleClass('expanded');
        return false;
    });
    $(".-x-selector li").on("click", function(ev) {
        $(this).parents(".-x-selector").removeClass('expanded').data("value", $(this).data("value")).find(".expander").text($(this).text());
    });
    $(".-x-selector").each(function() {
        var s = $(this).find(".selected");
        if (s.length==0) s = $(this).find("li").eq(0);
        s.trigger("click");
    });

    $(".-x-checkbox").on("click", function(ev) {
        ev.preventDefault();
        if ($(this).hasClass('disabled')) return false;
        if ($(this).parents(".single-selector").length>0) {
            var tmp = $(this).hasClass("checked");
            $(this).parents(".single-selector").find(".-x-checkbox.checked").removeClass('checked');
            if (tmp && $(this).parents(".c-optional").length>0) {
                $(this).removeClass('checked');
            } else {
                $(this).addClass("checked");
            }
        } else {
            $(this).toggleClass("checked");
        }
        if (typeof(recalculate)=="function") {
			 recalculate();
		  }
		  if (typeof(characteristicsRefresh)=="function") {
				characteristicsRefresh();
		  }
		  if (typeof(complectSubitemsQuantityRefresh)=="function") {
				complectSubitemsQuantityRefresh();
		  }
		  if (typeof(complectRefresh)=="function") {
				complectRefresh();
		  }
        if ($(this).parents("li").length>0) {
            $(this).parents("ul").find(".-x-check").removeClass("-x-check");
            if ($(this).hasClass("checked")) $(this).parents("li").addClass("-x-check");
        }
        return false;
    });

    $(".checkbox").on("click", function(ev) {
        ev.preventDefault();
        $(this).toggleClass('checked');
        if ($(this).parent().find(".acc").length>0) {
            $(this).parent().find(".acc").toggleClass("disabled", !$(this).hasClass("checked"));
            if ($(this).parent().find(".acc").hasClass('disabled')){
                $(this).parent().find(".acc input").attr('disabled', true);
            } else {
                $(this).parent().find(".acc input").attr('disabled', null).focus();
            }
        }

        $("a.save").removeClass('done');
        return false;
    });

    $(".-x-checkbox").each(function() {
        if ($(this).hasClass('checked') && $(this).parents('li').length>0) $(this).parents('li').addClass('-x-check');
    });


    $("form#search-top input").on("focus", function() {
        $("form#search-top").addClass("focus");
    }).on("blur", function() {
        $("form#search-top").removeClass("focus");
    });


    $("a.scroll-top").on("click", function(ev) {
        ev.preventDefault();
        $("html,body").stop().animate({scrollTop: 0}, 200);
        return false;
    });

window.items, window.ids, window.cnf = {}, window.isbtn = false;
    window.confirmNextShop = function(shop, resolve, reject) {
        var wh = window.innerWidth;
        var w = modalDialog({ 
            class: "basket-confirm-shop",
            width: wh>500?450:wh-80,
            onclose: function() {
                if (items.length>0 && isbtn) {
                    setTimeout(function() { confirmNextShop(items.shift(), resolve, reject); });
                } else if (isbtn) {
                    Basket.add(ids, 1, 1, cnf)
                        .then(function(data) {
                            resolve(data);
                        })
                        .catch(function(data) {
                            reject(data);
                        });
                } else {
                    reject(true);
                }
            }
        });
        w.append("<h6>"+shop.header+"</h2>");
        w.append("<div class='itm'><div class='img'><img src='"+shop.img+"'></div><div class='title'>"+shop.title+"</div></div>");
        w.append("<div class='btn' data-id='"+shop.id+"'></div>");
        for (var i=0; i<shop.buttons.length; i++) {
            w.find(".btn").append("<a href='#' "+(shop.buttons[i].type!='button'?'class="'+shop.buttons[i].type+'" ':'')+"data-key='"+shop.buttons[i].key+"' data-sact='"+shop.buttons[i].sourceModalAction+"'>"+shop.buttons[i].title+"</a>");
        }
		  setTimeout(function() {
            w.css({height: ""});
        });
        
        w.find(".btn a").on("click", function(ev) {
            ev.preventDefault();
            isbtn = true;
            cnf[$(this).parent().data("id")] = $(this).data("key");
            if ($(this).hasClass("-cancel")) {
                isbtn = false;
            }
            if ($(this).data('action')) {
                eval($(this).data('action'));
            }
            if ($(this).data("sact") && $(".-overlay-win.basket-add-items").length>0) {
                switch($(this).data("sact")) {
                    case "close":
                        $(".-overlay-win.basket-add-items .-win-close").trigger("click");
                        break;
                    case "remove":
                        $(".-overlay-win.basket-add-items .manage a.delete").trigger("click");
                        break;
                    case "ok":
                        break;
                    default:
                        if ($(this).data("sact")*1 === parseInt($(this).data("sact"))) {
                            $(".-overlay-win.basket-add-items input[type=text]").val($(this).data("sact"));
                        }
                        break;
                }
            }
            w.find(".-win-close").trigger("click");
            return false;
        });
    }
    window.confirmShops = function(it, id) {
        return new Promise(function(resolve, reject) {
            items = it, ids = id;
            confirmNextShop(items.shift(), resolve, reject);
        });
    }

    $(document.body).delegate(".add-to-basket", "click", function(ev) {
			ev.preventDefault();
			var id = $(this).data("id");
			if ($("body").hasClass("complect") && $(this).parents(".analogs").length==0) {
				id = [id];
				var ch = $(".-x-checkbox.checked");
				for (var i=0; i<ch.length; i++) {
					if (ch.eq(i).parents("li").length==0) {
						id.push(ch.eq(i).parents(".complectation").prev().data("id"));
					} else {
						id.push(ch.eq(i).parents("li").data("id"));
					}
				}
			}

        Basket.add(id, 1, 1)
            .then(function(data) {
                if (data.onlyInShop.length>0)
                     return confirmShops(data.onlyInShop, id);
                else return data;
            })
            .then(function(data) {
                if (data.mode) {
                    switch(data.mode) {
                        case "items":
                        case "accessory":
                                showModalItems(data.modalItems, data.mode);
                            break;
                        case "install":
                                showInstallInfo(data.modal);
                            break;
                        case "less":
                                showModalLess(data.modal);
                            break;
								case "redates":
                                showModalDates(data.modal);
                            break;
                    }
                }
            }).catch(function(data) {
                if (data===true) return data;
                alert(data.message);
            });
        return false;
    });

    $(".nav-bottom-panel .compare").delegate("li a.del", "click", function(ev) {
        ev.preventDefault();
        Basket.addCompare($(this).parents("li").data("id"), false, "group");
        return false;
    });

    var correctGalleryPosition = function(g) {
        var ci = g.find("img.current"), l = ci.position().left, sl = g.find(".viewport").scrollLeft()+l-($(window).width()-g.find("img.current").width())/2;
        g.find(".viewport").stop().animate({scrollLeft:sl}, 150);
    }


    $("section.gallery").each(function() {
        var $g = $(this), n = $g.find("img").length, lim = [0,0];
        if ($g.find("img").length<2) return false;
        $g.find("img").each(function() {
            $(this).clone().appendTo($(this).parent());
            lim[0]++;
            var i = new Image();
            i.onload=function() {
                lim[1]++;
                if (lim[0]==lim[1]) correctGalleryPosition($g);
            };
            i.src=$(this).attr('src');
        });
        $g.find("img").eq(0).addClass('current');
        for (var i=0;i<n/2;i++) {
            $g.find("img").eq(-1).prependTo($g.find(".viewport"));
        }
        correctGalleryPosition($g);
        $g.find(".board a").on("click", function(ev) {
            ev.preventDefault();
            var sl = $g.find(".viewport").scrollLeft();
            if ($(this).hasClass("left")) {
                sl +=  $g.find("img").eq(-1).width()+50;
                $g.find("img").eq(-1).prependTo($g.find(".viewport"));
                var el = $g.find("img.current").prev();
            } else {
                sl -=  $g.find("img").eq(0).width()+50;
                $g.find("img").eq(0).appendTo($g.find(".viewport"));
                var el = $g.find("img.current").next();
            }
            $g.find(".viewport").scrollLeft(sl);
            $g.find("img.current").removeClass("current");
            el.addClass('current');
            correctGalleryPosition($g);
            return false;
        });
        var _auto = 1;
        setInterval(function() {
            if (!_auto || $("html").hasClass("mobile")) return false;
            $g.find("a.right").trigger("click");
        }, 3000);
        $g.on("mouseenter", function() {
            _auto = 0;
        }).on("mouseleave", function() {
            _auto = 1;
        });


    var sx,sy,mx,my,dd, sl, $this = $(this).find(".viewport");
        $this[0].addEventListener("touchstart", function(ev) {
            if (ev.touches.length!=1) return true;
            sx = ev.touches[0].screenX, sy = ev.touches[0].screenY;
            mx = ev.touches[0].screenX, my = ev.touches[0].screenY;
            $this.stop();
            sl = $this.scrollLeft();
            dd = false;
        });
        $this[0].addEventListener("touchmove", function(ev) {
            if (ev.touches.length!=1) return true;
            mx = ev.touches[0].screenX, my = ev.touches[0].screenY;
            if (!dd && Math.abs(t)<Math.abs(d)) return true;
            var t = mx-sx, d = my-sy;
            if (Math.abs(t)>20) dd=true;
            if (!dd) return true;
            ev.preventDefault();
            $this.scrollLeft(sl-t);
        });
        $this[0].addEventListener("touchend", function(ev) {
            var t = mx-sx, d = my-sy;
//            if (!dd && Math.abs(t)<Math.abs(d)) return true;
            if (t<-20) $this.parent().find(".board .right").trigger("click");
            if (t>20) $this.parent().find(".board .left").trigger("click");
            if (Math.abs(t)<=20) correctGalleryPosition($this.parent());
        });
    });

	 setPass();
});

function setPass() {
    $("input[type=password]").not(".-init").each(function() {
        $(this).wrap("<div class='-pss'></div>");
        $(this).addClass("-init");
        $(this).parent().append("<a href='#' class='-tgl'></a>");
        $(this).parent().find(".-tgl").on("click", function(ev) {
            ev.preventDefault();
            var inp = $(this).parent().find("input");
            if (inp.attr("type")=="text") {
                inp.attr("type", "password");
                $(this).removeClass("txt");
            } else {
                inp.attr("type", "text");
                $(this).addClass("txt");
            }
            return false;
        });
    });
}

function formatPrice(x) {
    var dec = Math.round((x - Math.floor(x))*100);
    if (dec<10) dec="0"+dec;
	var val = (x+"").replace(/^(.*)\..*$/, '$1').replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1 ');
    return val;
}

function modalDialog(props) {
//    if ($(document.body).hasClass("overlayed")) return false;
    if (!document.body.ovl) document.body.ovl=0;
    $(document.body).addClass("overlayed");
    document.body.ovl++;
    var cls = props.class?props.class:"",
        width = props.width || 400,
        height= props.height || 200,
        _onclose = function() {};
    if (typeof(props.onclose)=="function") _onclose = props.onclose;

    var back = $("<div class='-overlay'></div>");
        back.appendTo(document.body);
    var win = $("<div class='-overlay-win'></div>");
        if (cls!="") win.addClass(cls);
        win.appendTo(back);
    win.append("<a href='#' class='-win-close'></a>");
    win.find(".-win-close").on("click", function(ev) {
        ev.preventDefault();
        if (document.body.ovl) document.body.ovl--;
        _onclose();
        if (!document.body.ovl) $(document.body).removeClass("overlayed");
        win.parents(".-overlay").remove();
        if (typeof(window._less_queue)!='undefined' && window._less_queue.length>0) {
            if (!window._less_queue_step_flag)
                window._less_queue = [];
        }
        return false;
    });
    win.css({width: width, height: height, marginLeft: (width+60)/-2, marginTop: (height+60)/-2} );
    return win;
}

function formatNum(x) {
    var dec = Math.round((x - Math.floor(x))*100);
    if (dec<10) dec="0"+dec;
	var val = (x+"").replace(/^(.*)\..*$/, '$1').replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1 ');
    return val;
}


function setSwipe(obj) {
    if (obj.length==0) return false;
    var dx, dy, sls, sly, slx, bw = $(this).width(), ch = $(obj).find(">*"), LNS = 0;
    if (ch.length==1 && ch.find(">*").length>0) ch = ch.find(">*");
    $(obj)[0].addEventListener("touchstart", function(ev) {
        $(this).finish();
        dx  =0;
        sls = $(this).scrollLeft(), slx = ev.touches[0].pageX, sly = ev.touches[0].pageY;
    });
    $(obj)[0].addEventListener("touchmove", function(ev) {
        dx = slx - ev.touches[0].pageX;
        dy = sly - ev.touches[0].pageY;
        if (Math.abs(dy)<Math.abs(dx) && Math.abs(dx)>20) $(this).scrollLeft(sls+dx);
        else  $(this).scrollLeft(sls);
        if (typeof(obj.onmove)!="undefined") obj.onmove(this.scrollLeft);
    });
    $(obj)[0].addEventListener("touchend", function(ev) {
        var N = LNS, Z = -1;
        if (Math.abs(dx)>20) {
            if (dx>20) N+=1; else if (dx<-20) N-=1;
            if ($(obj).attr("id")!='scroll-nav' && $(obj).parent().find(".thumbs").length>0 && N>=0) {
                N = $(obj).parent().find(".thumbs li").index($(obj).parent().find(".thumbs li.selected"));
                if (dx>20) N+=1; else if (dx<-20) N-=1;
                $(obj).parent().find(".thumbs li").eq(N).trigger("click");
            } else {
                var fgvp = $(".full-gallery .viewport").length>0 ? $(".full-gallery .viewport").width() : $(obj).parent().find(".viewport").width();
                N = Math.round(/*$(obj).scrollLeft()*/ sls / fgvp);
                if (dx>20) N+=1; else if (dx<-20) N-=1;
                if ($(".full-gallery .viewport").length>0) {
                    var lf = $(".full-gallery .viewport").width()*N;
                } else {
                    var lf = $(obj).parent().find(".viewport").width()*N;
                }
                if ($(obj).parents(".-compact-delivery").length>0) {
                    if (lf<=0) {
                        lf+= window.innerWidth, $(obj).stop().scrollLeft($(obj).stop().scrollLeft()+window.innerWidth);
                        $(obj).find(">*").eq(-1).insertBefore($(obj).find(">*").eq(0));
                    } else if (lf>=($(obj).find(">*").length-1)*window.innerWidth) {
                        lf-= window.innerWidth, $(obj).stop().scrollLeft($(obj).stop().scrollLeft()-window.innerWidth);
                        $(obj).find(">*").eq(0).insertAfter($(obj).find(">*").eq(-1));
                    }
                    $(obj).parent().find("a.scroll").addClass("disabled");
                    setTimeout(function() {
                        $(obj).parent().find("a.scroll").removeClass("disabled");            
                    }, 5);

                } else {
                    if (N==ch.length-1) lf+=50;
                    if (N<1) lf=0;
                }
                $(obj).stop().animate({scrollLeft: lf}, 200);
                if ($(obj).parent().find(".board").length>0) {
                    $(obj).parent().find(".board").addClass('started');
                    if (N>=ch.length-1) $(obj).parent().find(".board .right:not(.more)").hide(); else $(obj).parent().find(".board .right:not(.more)").show();
                    if (N<1) $(obj).parent().find(".board .left").hide(); else $(obj).parent().find(".board .left").show();
                }
            }
            LNS = N;
        }
        if (typeof(obj.onstop)!="undefined") obj.onstop(this.scrollLeft);
    });

}

assignAutoComplete = function(field, href) {
	var _tmo = false;
    $(field).on("keydown keyup focus paste change", function() {
		if (_tmo) clearTimeout(_tmo), _tmo = false;
        var t = $(this).val().trim();
        if (t.length<3 || $(this).filter(":focus").length==0) return true;
		  var ac = field.next(".-autocomplete");
        if (ac.length==0) {
            ac = $("<div class='-autocomplete'><ul></ul></div>");
            ac.insertAfter(field);
        } 
        $.get(href, { q: t }, function(data) {
            if (data.state!="ok" || data.items.length==0) {
                ac.remove();
                return true;
            }
            ac.find("ul").html("");
            for (var i=0; i<data.items.length; i++) {
                ac.find("ul").append("<li><a href='"+data.items[i].href+"'>"+data.items[i].context+"</a></li>");
            }
            ac.find("a").on("click", function(ev) {
                ev.preventDefault();
                $(field).val($(this).text()).addClass("fast-link");
                $href = $(this).attr("href");
                setTimeout(function() {
                    location.href = $href;
                }, 20);
                return false;
            });
        });
    }).on("blur", function() {
        _tmo = setTimeout(function() {
            if ($(field).hasClass("fast-link")) return true;
            var x = field.next(".-autocomplete");
            if (x.length>0) x.remove();
        }, 150);
    });
}

Basket = {
    add: function(id, count, checkContext, confirmations) {
        if (typeof(confirmations)=="undefined") confirmations = 0;
        if (typeof(count)=="undefined") count = 1;
        if (typeof(checkContext)=="undefined") checkContext=false;
        return new Promise(function(resolve, reject) {
            try {
                var data = { action: "add", id: id, count: count, confirmations: confirmations };
								if (0 < $('.complect-part').length) {
									data.complectGoods = [];
									data.satelliteGoods = [];
									$('.complect-part .-x-checkbox.checked').each(function() {
										var _ = $(this);
										var wr = _.closest('.complect-part');
										if (wr.attr('data-id')) {
											var productId = parseInt(wr.attr('data-id'));
											if (wr.hasClass('satellite-part')) {
												data.satelliteGoods.push(productId);
											} else {
												data.complectGoods.push(productId);
											}
										}
									});
								}
                if (checkContext) data["check-context"] = 1;
						console.log(data);
                $.get($_AJAX_PATHES.basket.add, data, function(data) {
                    Basket.redraw(data);
                    if (data.state=="ok") {
											if (data.product) {
												var product = data.product;
												GaEcommerceHelper.addProductToCart(
													product.id,
													product.name,
													product.category,
													product.brand,
													product.price,
													product.quantity
												);
											}
											console.log(data);
											return resolve(data);
										}
                    else reject(data);
                });
            } catch(e) { reject(e); }
        });
    },
    remove: function(id, silent) {
        if (typeof(silent)=="undefined") silent=false;
        if (!silent && !confirm("Удалить товар из корзины?")) return false;
        return new Promise(function(resolve, reject) {
            try {
                $.get($_AJAX_PATHES.basket.remove, { action: "remove", id: id }, function(data) {
                    Basket.redraw(data);
                    if (data.state=="ok") {
							  if (data.product) {
									var product = data.product;
									GaEcommerceHelper.removeProductFromCart(
										product.id,
										product.name,
										product.category,
										product.brand,
										product.price,
										product.quantity
									);
								}
								return resolve(data);
							}
                    else reject(data);
                });
            } catch(e) { reject(e); }
        });
    },
    changeCount: function(item_id, count) {
        return new Promise(function(resolve, reject) {
            try {
                $.get($_AJAX_PATHES.basket.update, { action: "change-count", id: item_id, count: count }, function(data) {
                    Basket.redraw(data);
                    if (data.state=="ok") {
								if (data.product) {
									var product = data.product;
									if (0 < product.quantity) {
										GaEcommerceHelper.addProductToCart(
											product.id,
											product.name,
											product.category,
											product.brand,
											product.price,
											product.quantity
										);
									} else {
										GaEcommerceHelper.removeProductFromCart(
											product.id,
											product.name,
											product.category,
											product.brand,
											product.price,
											-product.quantity
										);
									}
								}

								return resolve(data);
							}
                    else reject(data);
                });
            } catch(e) { reject(e); }
        });
    },
    update: function(items) {

    },
    serviceCancel: function(id) {
        return new Promise(function(resolve, reject) {
            try {
                $.get($_AJAX_PATHES.basket.serviceCancel, { action: "service-cancel", id: id }, function(data) {
                    Basket.redraw(data);
                    if (data.state=="ok") return resolve(data);
                    else reject(data);
                });
            } catch(e) { reject(e); }
        });
    },
    get: function() {

    },
    addCompare: function(id, add, isgroup) {
        // add - boolean true->add, false->remove
        if (typeof(add)=="undefined") add = true;
        return new Promise(function(resolve, reject) {
            if (typeof(isgroup)=="undefined") isgroup = false;
            $.get($_AJAX_PATHES.compareManage, { action: add?"add":"remove"+(isgroup?"-group":""), id: id }, function(data) {
                if (data.id==id && data.state=="ok") {
                    if (data.groups.length>0) {
                        var ul = $("<ul></ul>"), N = 0;
                        $(".nav-bottom-panel .compare").removeClass("empty").html("<span>Сравнить (<b></b>)</span><div class='popup'></div>");
                        for (var i=0; i<data.groups.length; i++) {
                            var li = $("<li data-id='"+data.groups[i].id+"'><a href='"+data.groups[i].link+"'>"+data.groups[i].title+" ("+data.groups[i].count+")</a><a href='#' class='del'></a></li>");
                            ul.append(li);
                            N+=data.groups[i].count*1;
                        }
                        ul.appendTo($(".nav-bottom-panel .compare .popup"));
                        $(".nav-bottom-panel .compare>span b").text(N);
                    } else {
                        $(".nav-bottom-panel .compare").addClass("empty").html("");
                    }
                    return resolve(data.added);
                }
                return reject();
            });
        });
    },
    redraw: function(data) {
        if (typeof(data.items)=="undefined") return false;
        var total = 0;
        for (var i in data.items) total += data.items[i].price * data.items[i].count;
        $(".nav-bottom-panel .basket").toggleClass("empty", total*1==0);
        if (total*1==0) {
            $(".nav-bottom-panel .basket").html("<a>пусто</a>");
        } else {
            $(".nav-bottom-panel .basket").html('<a href="'+$_AJAX_PATHES.basket.basket_link+'"><strong>'+formatNum(total)+'</strong> <span class="rub">р</span></a>');
        }
    }
}

var setInputButtons = function(obj) {
    if (obj.hasClass("initialized")) return true;
    obj.addClass("initialized");
    obj.parent().append("<a href='#' class='change dec'></a><a href='#' class='change inc'></a>");
    if (typeof(obj.attr("min"))=="undefined") obj.attr('min', 1);
    if (typeof(obj.attr("max"))=="undefined") obj.attr('min', 99);
    obj[0].recalculate = function() {
        if ($(this).val()*1<$(this).attr("min")) $(this).val($(this).attr("min"));
        if ($(this).val()*1>$(this).attr("max")) $(this).val($(this).attr("max"));
        $(this).parent().find(".dec").toggleClass("disabled", $(this).val()*1<=$(this).attr("min"));
        $(this).parent().find(".inc").toggleClass("disabled", $(this).val()*1>=$(this).attr("max"));
        var price = $(this).parents(".price").data("price")*1, amount = price*$(this).val();
        $(this).parents(".price").find("strong span:not(.rub)").text(formatNum(amount));
        if (typeof(totalRecalc)=="function") totalRecalc();
        if (typeof($(this).data("item"))!="undefined" && $(this).hasClass("--init")) {
            Basket.changeCount($(this).data("item"), $(this).val())
                .then(function(data) {
						console.log('check', data);
                    if (typeof(data.onlyInShop)!="undefined" && data.onlyInShop.length>0) {
								console.log('check', 'yes');
                         return confirmShops(data.onlyInShop, obj.data("item"));
							} else {
								console.log('check', 'no');
								return data;
							}

                })
                .catch(function(e) {
                console.log("YYY", e);
                });
        }
    }
    obj[0].recalculate();
    obj.on("keyup keydown", function(ev) {
        if (ev.which==13 && ev.type=="keydown") {
            return true;
		}
        if (!numberKeyIsValid(ev)) { ev.preventDefault(); return false }
        this.recalculate();
    }).on("change", function(ev) {
        if ($(this).val().match(/\D/)) $(this).val($(this).val().replace(/\D/g, ''));
    }).on("paste", function(ev) {
        var $this = $(this);
        setTimeout(function() {
            if ($this.val().match(/\D/)) $this.val($this.val().replace(/\D/g, ''));
        }, 5);
    });
    obj.parent().find("a.change").on("click", function(ev) {
        ev.preventDefault();
        var s = 1, i = $(this).parent().find("input");
        if ($(this).hasClass('dec')) s*=-1;
        i.val(i.val()*1+s);
        i[0].recalculate();
        return false;
    });
    obj.addClass("--init");
}
var numberKeyIsValid = function(ev) {
	if (ev.ctrlKey) return true;
	if (ev.which<32 || ev.which==46 || ev.which>47 && ev.which<58 && !ev.shiftKey || ev.which>=37 && ev.which<=40 || ev.which>=33 && ev.which<=36 || ev.which>=96 && ev.which<=105) return true;
	return false;
}

showModalItems = function(data, mode) {
    var wh = window.innerWidth;
    if (typeof(data.accessory)!="undefined" && wh<800) return false;
    var w = modalDialog({ class: "basket-add-items", width: wh>800?750:wh-80 });
		w.append("<h2>Товар добавлен в корзину</h2>");
		for (var i=0; i<data.length; i++) {
				var item = data[i];
				console.log(item);
				w.append("<div class='added-item'><div class='image'><img src='"+item.image+"'></div><div class='title'>"+item.title+"</div><div class='manage price' data-price='"+item.price+"'><div class='count'><input data-item='"+item.item_id+"' type='text' maxlength='2' value='"+item.count+"' min='"+item.min+"' max='"+item.max+"'></div><div class='amount'><strong><span>"+formatNum(item.price)+"</span> <span class='rub'>р</span></strong><a href='#' class='delete' data-id='"+item.item_id+"'>удалить</a></div></div></div>");
		}
    setInputButtons($("div.-overlay-win.basket-add-items .added-item .manage .count input"));
    w.find(".added-item .delete").on("click", function(ev) {
        Basket.remove(data.item_id)
					.then(function() {
						 w.find(".-win-close").eq(0).trigger("click");
					}).catch(function() {
					});
		 });
		 data = data[0];
    if (mode=="items") {
            if (data.items.length>0)  {
                w.append("<div class='go-order'><a href='"+$_AJAX_PATHES.basket.set_order_link+"'>Оформить заказ</a></div>");
                w.append("<h2>Ничего не забыли?</h2>");
                var it = $("<ul class='goods'></ul>");
                for (var i=0; i<data.items.length; i++) {
                    var item = data.items[i];
                    var li = $("<li data-id='"+item.id+"'><a class='title'><div class='image'><img src='"+item.image+"'></div><span>"+item.title+"</span></a><div class='price'><strong>"+formatNum(item.price)+" <span class='rub'>р</span></strong><a href='#' class='add'>в корзину</a><a href='#' class='del'>отменить</a></div></li>");
                    li.appendTo(it);
                }
                it.appendTo(w);

            } else {
                setTimeout(function() {
                    $(".basket-add-items").each(function() {
                        var mt = $(this).outerHeight()/-2;
                        $(this).css({marginTop: mt+"px", top: "50%"});
                    });
                }, 50);
            }

            $("div.-overlay-win.basket-add-items").addClass("no-addd");
            setTimeout(function() {
                $(".basket-add-items").each(function() {
                    var h1 = $(this).find(".added-item .title").outerHeight() + 40;
//                        $(this).find(".added-item .image").css({height:h1});
//                        $(this).find(".added-item .image img").css({maxHeight:h1+"px"});
                });
            }, 30);

            w.append("<div class='bottom-buttons'><a href='#' class='close'>Продолжить покупки</a><a href='"+$_AJAX_PATHES.basket.set_order_link+"' class='set-order'>Оформить заказ</a></div>");
            w.find(".goods .add").on("click", function(ev) {
                var $this = $(this);
                $this.css({display: "none"});
                Basket.add($this.parents("li").data("id"))
                    .then(function() {
                        $this.css({display: ""});
                        $this.parents("li").addClass('added');
                    }).catch(function() {
                        $this.show();
                    });
            });
            w.find(".goods .del").on("click", function(ev) {
                var $this = $(this);
                $this.css({display: "none"});
                Basket.remove($this.parents("li").data("id"), true)
                    .then(function() {
                        $this.css({display: ""});
                        $this.parents("li").removeClass('added');
                    }).catch(function() {
                        $this.show();
                    });
            });
            w.find(".bottom-buttons .close").on("click", function(ev) {
                ev.preventDefault();
                $(".-overlay .-win-close").trigger("click");
                return false;
            });

            setTimeout(function() {
                var h = w[0].scrollHeight - 20;
                    w.css({height: h, marginTop: 0, top: 30});
            }, 40);
    }
    else {
        w.addClass("accessory");
        w.append("<div class='add-accessory'><div class='placeholder'><span>"+data.accessory.placeholder+"</span></div></div>");
        w.append("<div class='bottom-buttons'><a href='#' class='close'>Продолжить покупки</a><a href='"+$_AJAX_PATHES.basket.set_order_link+"' class='set-order'>Оформить заказ</a></div>");

        w.find(".add-accessory").append("<div class='acc-block'><div class='image'><img src='"+data.accessory.image+"'></div></div>");
        w.find(".acc-block").append("<div class='acc-content'><div class='top'><div class='title'><a href='"+data.accessory['item-link']+"'>"+data.accessory.title+"</a><br><span>"+data.accessory["discount-text"]+"</span></div><div class='price'><strong>"+data.accessory["discount-price"]+" <span class='rub'>р</span></strong><strong>"+data.accessory["price"]+" <span class='rub'>р</span></strong></div></div></div></div>");
        w.find(".acc-content").append("<div class='buy-block'><a href='#' class='cancel'>Спасибо, не надо</a><a href='#' class='add'>Добавить в корзину</a></div>");

        w.find(".buy-block .cancel").on("click", function(ev) {
            ev.preventDefault();
            var _h0 = w.find(".add-accessory").outerHeight();
            w.find(".add-accessory").remove();
            setTimeout(function() {
                var h = w[0].scrollHeight - 90 - _h0;
                    w.css({height: h, marginTop: (-h/2)+"px"});
            });

            return false;
        });
        w.find(".buy-block .add").on("click", function(ev) {
            ev.preventDefault();
            if ($(this).hasClass('disabled')) return false;
            $(this).addClass("disabled");
            $(this).next().hide();
            var $this = $(this);
            Basket.add(data.accessory.id, 1)
                .then(function(data) {
                    $this.removeClass('disabled');
                    $this.next().show();
                    $("div.-overlay-win .-win-close").trigger("click");
                })
                .catch(function(data) {
                    $this.removeClass('disabled');
                    $this.next().show();
                })
            return false;
        });

        w.find(".bottom-buttons .close").on("click", function(ev) {
            w.find(".-win-close").eq(0).trigger("click");
        });

        setTimeout(function() {
            var h = w[0].scrollHeight - 50;
                w.css({height: h, marginTop: (-h/2)+"px"});
        });
    }

}

showInstallInfo = function(data) {
    var wh = window.innerWidth;
    var w = modalDialog({ class: "basket-add-install", width: wh>800?520:wh-50 });
    w.append("<h2>"+data.title+"</h2>");
    w.append("<div class='image'><img src='"+data.image+"'></div>");
    w.append("<div class='content'>"+data.content+"</div>");
    w.find(".content").append("<div class='x-buttons'><a href='#' class='agree'>"+data['agree-text']+"</a><a href='#' class='cancel'>"+data["cancel-text"]+"</a></div>");

    w.find(".x-buttons .cancel").on("click", function(ev) {
        ev.preventDefault();
        $("div.-overlay-win .-win-close").trigger("click");
        return false;
    });
    w.find(".x-buttons .agree").on("click", function(ev) {
        ev.preventDefault();
        if ($(this).hasClass("disabled")) return false;
        $(this).addClass('disabled');
        $(this).next().hide();
        var $this = $(this);
        Basket.add(data.id, data.count)
            .then(function(data) {
                $this.removeClass('disabled');
                $this.next().show().trigger("click");
            })
            .catch(function(data) {
                $this.removeClass('disabled');
                $this.next().show();
            });
        return false;
    });

    setTimeout(function() {
        var h = w[0].scrollHeight - 50;
            w.css({height: h, marginTop: (-h/2)+"px", marginLeft: "-190px"});
    });
}

showModalDates = function(data) {
    var wh = window.innerWidth;
    var w = modalDialog({ class: "basket-add-less -redates", width: wh>800?430:wh-40 });
    $("div.-overlay").addClass("-scrollable").css({ height: $("body").outerHeight() + "px" });
    $("body,html").scrollTop(0);
    w.append("<div class='header'>"+data.title+"</div><div class='-contents'>"+data.items_html+"</div>");
    w.append("<div class='variants'><ul></ul></div>");
    w.find(".header a.expand-items").on("click", function(e) {
        e.preventDefault();
        $(this).hide();
        $(".basket-add-less.-redates .-contents").addClass('-expanded');
        return false;
    });

    for (var i in data.variants) {
        var vr = data.variants[i];
        var li = $("<li data-id='"+i+"'><em></em><h5>"+vr.title+"</h5><p>"+vr.details+"</p></li>");
        li.appendTo(w.find(".variants ul"));
    }

    w.find(".variants ul li").on("click", function(ev) {
        $(this).parent().find("li").removeClass('selected');
        $(this).addClass('selected');
    }).eq(0).addClass('selected');
    
    w.append("<div class='continue-block'><a href='#'>Продолжить</a></div>");

    w.find(".continue-block a").on("click", function(ev) {
        ev.preventDefault();
        if ($(this).hasClass('disabled')) return false;
        $(this).addClass('disabled');
        var $this = $(this), $$id = w.find(".variants .selected").data("id"), $$item_id = 0;
        if (typeof(data.id)!="undefined") $$item_id = data.id;
        $.post($_AJAX_PATHES.basket.setLessVariant, { action: "less-variant", id: $$id, item_id: $$item_id }, function(data) {
            if (data.state=="ok") {
                if (typeof(window._less_queue)!='undefined' && window._less_queue.length>0) {
                    window._less_queue_step_flag = true;
                    $("div.-overlay-win .-win-close").trigger("click");
                    window._less_queue_step_flag = false;
                    setTimeout(window._less_queue_step, 150);
                } else {
                    $this.removeClass("disabled");
                    $("div.-overlay-win .-win-close").trigger("click");
                }
            } else {
                $this.removeClass("disabled");
            }
        });
        return false;
    });

    
}

showModalLess = function(data) {
    var wh = window.innerWidth;
    var w = modalDialog({ class: "basket-add-less", width: wh>800?430:wh-40 });
    w.append("<div class='header'><div class='image'><img src='"+data.image+"'></div><div class='title'><p>"+data.title+"</p><div class='block'><h4>"+data["content-title"]+"</h4><p>"+data.content+"</p></div></div>");
    w.append("<div class='variants'><ul></ul></div>");

    for (var i in data.variants) {
        var vr = data.variants[i];
        var li = $("<li data-id='"+i+"'><em></em><h5>"+vr.title+"</h5><p>"+vr.details+"</p></li>");
        li.appendTo(w.find(".variants ul"));
    }

    w.find(".variants ul li").on("click", function(ev) {
        $(this).parent().find("li").removeClass('selected');
        $(this).addClass('selected');
    }).eq(0).addClass('selected');

    w.append("<div class='continue-block'><a href='#'>Продолжить</a></div>");

    w.find(".continue-block a").on("click", function(ev) {
        ev.preventDefault();
        if ($(this).hasClass('disabled')) return false;
        $(this).addClass('disabled');
        var $this = $(this), $$id = w.find(".variants .selected").data("id"), $$item_id = 0;
        if (typeof(data.id)!="undefined") $$item_id = data.id;
        $.post($_AJAX_PATHES.basket.setLessVariant, { action: "less-variant", id: $$id, item_id: $$item_id }, function(data) {
            if (data.state=="ok") {
                if (typeof(window._less_queue)!='undefined' && window._less_queue.length>0) {
                    window._less_queue_step_flag = true;
                    $("div.-overlay-win .-win-close").trigger("click");
                    window._less_queue_step_flag = false;
                    setTimeout(window._less_queue_step, 150);
                } else {
                    $this.removeClass("disabled");
                    $("div.-overlay-win .-win-close").trigger("click");
                }
            } else {
                $this.removeClass("disabled");
            }
        });
        return false;
    });


    setTimeout(function() {
        var h = w[0].scrollHeight - (wh>800?50:10);
            w.css({height: h, marginTop: (-h/2)+"px", marginLeft: "-190px"});
    });
}

var initScroll = function() {
    var IX=0;
    if ($("#scroll-nav").length==0) {
        var scpane = $("<div id='scroll-nav' class='hidden'><span class='left hidden'></span><span class='right'></span></div>"), sc = $("*[data-menu]");
        scpane.appendTo(document.body);
        $(document.body).attr("id", "x-root");
        for (var i=0; i<sc.length; i++) {
            if (!sc.eq(i).data("menu")) continue;
            var p = $("<a href='#' class='item'>"+sc.eq(i).data("menu")+"</a>");
            var id = sc.eq(i).prev().attr("id");
				if (typeof(id)=="undefined") id = sc.eq(i).data("for"); 
            if (typeof(id)=="undefined") id="x-root";
            p.attr("data-id", id);
            p.appendTo(scpane);
            if (IX==0) p.addClass("current");
            IX++;
        }
    } else {
        scpane = $("#scroll-nav");
    }
    setSwipe(scpane);
    scpane.find(">span").on("click", function(ev) {
        ev.preventDefault();
        if (!$(document.body).hasClass('cabinet')) {
            var st = 100, scr = $(this).parent().scrollLeft();
            if ($(this).hasClass('left')) st*=-1;
            $(this).parent().stop().animate({scrollLeft: scr+st}, 50);
            $(this).parent().find(".left").toggleClass("hidden", (scr+st<=0));
            $(this).parent().find(".right").toggleClass("hidden", (scr+st>=$(this).parent()[0].scrollWidth-400));
        } else {
            var st = 100, scr = $(this).parent().find("ul").scrollLeft();
            if ($(this).hasClass('left')) st*=-1;
            $(this).parent().find("ul").stop().animate({scrollLeft: scr+st}, 50);
            $(this).parent().find(".left").toggleClass("hidden", (scr+st<=0));
            $(this).parent().find(".right").toggleClass("hidden", (scr+st>=$(this).parent().find("ul")[0].scrollWidth-400));
        }
        return false;
    });
    if (!$(document.body).hasClass('cabinet')) {
        scpane.find("a").on("click", function(ev) {
            ev.preventDefault();
            var id = $(this).data("id");
            var fst = $("#"+id).offset().top + $('body').scrollTop();
            if (!_menu_no_scroll) $("body").stop().animate({scrollTop: fst - 120 });
            $(this).parent().find(".current").removeClass("current");
            $(this).addClass("current");
            if ($(this).offset().left<50) {
                $(this).parent().stop().animate({scrollLeft: $(this).parent().scrollLeft()+$(this).offset().left-50}, 100);
            }
            if ($(this).offset().left+$(this).outerWidth()>350) {
                $(this).parent().stop().animate({scrollLeft: $(this).parent().scrollLeft()+($(this).offset().left+$(this).outerWidth()-350)}, 100);
            }
            var st = 0, scr = $(this).parent().scrollLeft();
            $(this).parent().find(".left").toggleClass("hidden", (scr+st<=0));
            $(this).parent().find(".right").toggleClass("hidden", (scr+st>=$(this).parent()[0].scrollWidth-400));

            return false;
        });
        if(scpane.find("a").length==0) scpane.remove();
    }
}

function format_price(price) {
	var s = new String(price);
	var sInt = s;
	var sFloat = '';
	var dot = s.indexOf('.');
	if (0 <= dot) {
		var sInt = s.substring(0, dot);
		var sFloat = s.substr(dot);
		if (2 == sFloat.length) sFloat += '0';
	}
	var arr = sInt.split('');
	var res = [];
	for (var i = 0, length = arr.length; i < length; i++) {
		res.push(arr[i]);
		if ((length > (i + 1)) && (0 == ((length - i - 1) % 3))) res.push(' ');
	}
	return res.join('')+sFloat;
}

function setShowcaseSale(productId) {
	console.log('setShowcaseSale', productId);
}

function decrementBasketQuantity(productId) {
	console.log('decrementBasketQuantity', productId);
	$('.basket-items li[data-id="'+productId+'"] .change.dec').click();
}