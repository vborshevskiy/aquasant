$(function() {

    window._isCatPage = true;

	 if ($("input#scroll-control").val()*1>0) {
        setTimeout(function() {
            $("body").scrollTop($("input#scroll-control").val()*1);
        }, 20);
    }
    
    $("body").on("scroll", function(e) {
        $("input#scroll-control").val($(document.body).scrollTop());
    });

		$("ul.goods li .action").each(function() {
        $(this).html("<i></i><span>"+$(this).html()+"</span>");
    });

    var refreshSelectedFilters = function(filter) {
        filter.find(".sel-items").remove();
        var s = $(filter).find(".options li.selected");
        if (s.length==0) {
            filter.removeClass("selected");
        } else {
            var sl = $("<ul class='sel-items'></ul>");
            for (var i=0; i<s.length; i++) {
                var xc = "-item-"+filter.data("name")+"-"+s.eq(i).data("value");
                s.eq(i).addClass(xc);
                sl.append("<li><span>"+s.eq(i).text()+"</span><a href='#' class='del' data-id='"+xc+"'>"+"</a></li>");
            }
            filter.addClass('selected');
            filter.append(sl);

            sl.find('.del').on("click", function(ev) {
                ev.preventDefault();
                var _val = $(this).data("id").split("-").pop();
                $.post($_AJAX_PATHES.catalogue.resetSelectorFilter, { action: "reset-filter", name: $(this).parents(".filter").data("name"), value: _val }, function(data) {
                    if (data.state=="ok") location.href = data.href;
                });
                return false;
            });
        }
    }

    var winresize = function() {
        var w = window.innerWidth, g = $("ul.goods li:not(.flex-fix):not(.item-action)");
        if (w<=950) {
            g.each(function() {
                $(this).find(".action").insertBefore($(this).find(".price"));
            });
            $(".order-list").insertBefore($(".toggle-filters"));
            $(".selections").insertBefore($(".order-list"));
            $(".selections-info").insertAfter(".catalogue-pages-bottom .pages");
        } else {
            g.each(function() {
                $(this).find(".action").insertAfter($(this).find(".price"));
            });
            $(".order-list").appendTo($(".cat-pag .pages>.order"));
            $(".selections").prependTo($(".catalogue-part"));
            $(".selections-info").appendTo("aside.filters");
        }
    }

    winresize();
    window.addEventListener("resize", winresize);


    $("ul.goods").each(function() {
        for (var i=0; i<5; i++) {
            $(this).append("<li class='flex-filler'></li>");
        }
        $(this).find("li:not(.item-action):not(.flex-filler)").each(function() {
            var x = $("<div class='wr'></div>");
                x.appendTo(this);
            $(this).find(">*:not(.images-wrapper):not(.images)").appendTo(x);
        });
    });

    $("ul.goods li:not(.item-action):not(.flex-filler)").each(function() {
        var h = $(this).find(".add-to-basket"), z = $(this).find(".title");
        $(this).data("id", h.data("id"));
        $(this).append("<div class='compare-add'></div>");
        if ($(this).hasClass("comparing")) {
            $(this).find(".compare-add").addClass("remove");
        }
    });
    $("ul.goods li .compare-add").on("click", function(ev) {
        ev.preventDefault();
        var $this = $(this);
        Basket.addCompare($(this).parents("li").find(".add-to-basket").data("id"), !$(this).parents("li").hasClass("comparing"))
              .then(function(state) {
                if (state) {
                    $this.parents("li").addClass("comparing");
                } else {
                    $this.parents("li").removeClass("comparing");
                }
              });
        return false;
    });

    $(".order-list .order-expander").on("click", function(ev) {
        ev.preventDefault();
        $(this).parents(".order-list").toggleClass('expanded');
        $("main").toggleClass("order-list-expanded", $(this).parents(".order-list").hasClass("expanded"));
        return false;
    });
    $(".order-list li a").on("click", function(ev) {
        $(this).parents(".order-list").removeClass('expanded');
        $(".order-list").find('.order-expander').text($(this).text());
        $("main").toggleClass("order-list-expanded", $(this).parents(".order-list").hasClass("expanded"));
        return true;
    });


    var getApplyFilters = function(filterName, filterValue) {
        return new Promise(function(resolve, reject) {
            var $all = {}, f = $(".filter");
            for (var i=0; i<f.length; i++) {
                if (f.eq(i).hasClass("slider")) {
                    if (f.eq(i).hasClass("changed")) {
                        var vl = f.eq(i).find(".ui-slider-handle .value");
                        $all[f.eq(i).data("name")] = [vl.eq(0).text().replace(/\D/g, ''), vl.eq(1).text().replace(/\D/g, '')];
                    }
                } else {
                    var vals = [], vl = f.eq(i).find(".selected");
                    for (var j=0; j<vl.length; j++) vals.push(vl.eq(j).data('value'));
                    $all[f.eq(i).data("name")] = vals;
                }
            }

            $.get($_AJAX_PATHES.catalogue.getFilterInfo, { action: "get-filter", name: filterName, value: filterValue, "all-filters": $all }, function(data) {
                    if (data.state=='ok') resolve(data); else reject(data);
                }
            );
        });
    }

    $("aside.filters .options .option").on("click", function(ev) {
        if (ev.target.tagName.toLowerCase()=="a") return true;
        ev.preventDefault();
        $(this).toggleClass("selected").toggleClass("changed");
        $(this).parents(".filters").find(".apply-button").remove();
        $(this).parents(".filters").find("a.apply").removeClass('visible');
        if ($(this).parents(".filter").find(".changed").length>0) {
            var apply = $("<a href='#' class='apply-button waiting'>применить<em></em></a>"), autoclick = false;
                apply.appendTo(this);
                apply.on("click", function(ev) {
						if ($(this).hasClass("-preloader")) return false;
                    $(this).addClass("-preloader");
                    if ($(this).hasClass("waiting")) {
                        autoclick = true;
                        ev.preventDefault();
                        return false;
                    }
                    return true;
                });
                var $flt = $(this).parents(".filter"), vals = [], sel = $flt.find(".selected");
                for (var i=0; i<sel.length; i++) vals.push(sel.eq(i).data("value"));
                getApplyFilters($(this).parents(".filter").data("name"), vals)
                .then(function(data) {
                    $flt.find(".apply-button").attr("href", data.href).removeClass("waiting").find("em").text(" ("+data.count+")");
                    if (autoclick) location.href = data.href;
                });
        }
        return false;
    });

    $("aside.filters .selector .options li").on("click", function(ev) {
        if (ev.target.tagName.toLowerCase()=="a") return true;
        ev.preventDefault();
        $(this).toggleClass("selected").toggleClass('changed');
        $(this).parents(".filters").find(".apply-button").remove();
        $(this).parents(".filters").find("a.apply").removeClass('visible');
        if ($(this).parents(".filter").find(".changed").length>0) {
            var apply = $("<a href='#' class='apply-button waiting'>применить<em></em></a>"), autoclick = false;
                apply.appendTo(this);
                apply.on("click", function(ev) {
						if ($(this).hasClass("-preloader")) return false;
                    $(this).addClass("-preloader");
                    if ($(this).hasClass("waiting")) {
                        autoclick = true;
                        ev.preventDefault();
                        return false;
                    }
                    return true;
                });
                var $flt = $(this).parents(".filter"), vals = [], sel = $flt.find(".selected");
                for (var i=0; i<sel.length; i++) vals.push(sel.eq(i).data("value"));
                getApplyFilters($(this).parents(".filter").data("name"), vals)
                .then(function(data) {
                    $flt.find(".apply-button").attr("href", data.href).removeClass('waiting').find("em").text(" ("+data.count+")");
                    if (autoclick) location.href = data.href;
                });
        }

        return false;
    });

    $("aside.filters .filter h4").on("click", function(ev) {
//        if (!$(this).parents(".filter").hasClass("selected")) {
            $("aside.filters .filter").not($(this).parents(".filter")).removeClass('expanded');
            $(this).parents(".filter").toggleClass('expanded');
//            if ($(window).width()<=950) {
//                $("html,body").stop().animate({scrollTop: $(this).offset().top - 60}, 100);
//            }
//        }
        $("aside.filters").toggleClass("expanded", $("aside.filters .filter.expanded").length>0);
        correctValuesPositions();
    });

    $("aside.filters .filter.selector").each(function(){
        refreshSelectedFilters($(this));
    });

    $("aside.filters .show-all-filters-button a").on("click", function(ev) {
        ev.preventDefault();
        $(this).fadeOut(200, function() {
            $("aside.filters .filter.default-hidden").removeClass("default-hidden");
        });
        return false;
    });

    var correctValuesPositions = function() {
        $(".ui-slider-handle div.value:visible em").each(function() {
            $(this).css({ "transform": "translateX(0)" });
            var a1 = Math.floor($(this).offset().left), a2 = Math.floor($(this).parents('.slider').offset().left),
                b1 = Math.floor($(this).offset().left) + Math.floor($(this).outerWidth()), b2 = Math.floor($(this).parents('.slider').offset().left) + Math.floor($(this).parents('.slider').outerWidth()),
                da = a1-a2<10 ? -1*(a1-a2-10) : 0;
                if (da==0 && b1+10>b2) {
                    da = b2 - b1 - 10;
                }

                $(this).css({ "transform": "translateX("+da+"px)" });
        });
    }


    $("aside.filters .filter.slider").each(function() {
        var $sl = $("<div class='slider-element'></div>"), vl = $(this).find("input").attr("value").split(",");
        if (typeof(vl[1])=="undefined") {
            var i = $(this).find("input");
            vl = [i.attr("min")*1, i.attr("max")*1];
        }
        $(this).append($sl);
        var appTO = false, def_val = vl;

        $sl.slider({
            range: true,
            values: vl,
            min: $(this).find("input").attr("min")*1,
            max: $(this).find("input").attr("max")*1,
            slide: function(e, ui) {
                if (appTO) { clearTimeout(appTO); appTO=false; }
                $(".filter .apply").removeClass("visible");
                $(this).parents(".filters").find(".apply-button").remove();
                if (ui.values[0]==def_val[0] && ui.values[1]==def_val[1]) $(this).parents(".filter").removeClass('changed'); else $(this).parents(".filter").addClass("changed");
                $(this).find(".min .value em").text(formatPrice(ui.values[0]));
                $(this).find(".max .value em").text(formatPrice(ui.values[1]));
                var $flt = $(this).parents(".filter"), $vals = ui.values;
                setTimeout(correctValuesPositions);
                appTO = setTimeout(function() {
                    getApplyFilters($flt.data("name"), $vals)
                    .then(function(data) {
                        $flt.find(".apply").addClass("visible").attr('href', data.href).find("em").text(' ('+data.count+')');
                    }).catch(function(e) { console.log("ERROR", e); } );
                }, 100);
            }
        });
        var sl = $sl.find(".ui-slider-handle");
        var i = 0;
        sl.each(function() {
            if (i==0) $(this).addClass("min"); else $(this).addClass("max");
            $(this).append("<div class='value'><em>"+formatPrice(vl[i])+"</em></div>");
            i++;
        });
        var apply = $("<a href='#' class='apply'>применить<em></em></a>");
            apply.insertAfter($(this).find("h4"));
    });

    $("a.toggle-filters").on("click", function(ev) {
        ev.preventDefault();
        $(".order-list.expanded").removeClass("expanded");
        $("main").removeClass("order-list-expanded");
        $(document.body).toggleClass("shown-filters");
        if ($(document.body).hasClass("shown-filters")) {
            $(".toggle-filters span").text($(".toggle-filters span").data('alt'));
        } else {
            $(".toggle-filters span").text($(".toggle-filters span").data('txt'));
        }
        return false;
    });

    $(".goods li").each(function() {
        if ($(this).find(".images>.image").length<=1) return true;
        var b = $("<div class='board'><a href='#' class='scrl left'></a><a href='#' class='scrl right'></a></div>");
            b.insertAfter($(this).find(".images"));
    });
    $(".goods li .images").each(function() {
        var n = this, tn = [];
        for (var n=n.firstChild;n;n=n.nextSibling){
            if (n.nodeType == Node.TEXT_NODE) tn.push(n);
        }
        while (tn.length>0) tn.pop().remove();
    });
    $(".goods li .board a").on("click", function(ev) {
        ev.preventDefault();
        var $this = $(this);
        if ($this.parent().hasClass("disabled")) return false;
        $this.parent().addClass('disabled');
        var v = $this.parents("li").find(".images"), l = v.scrollLeft(), s = $this.hasClass("left") ? -1 : 1, w = v.width();
        l+=w*s;
        if (l<=0) {
            l = (v.find(".image").length-1)*w;
        } else if (l>(v.find(".image").length-1)*w) {
            l = 0;
        }
        v.stop().animate({scrollLeft: l}, 150);
        setTimeout(function() {
            $this.parent().removeClass('disabled');
        }, 150);
        return false;
    });
});
