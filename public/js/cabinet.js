$(function() {
    var lastScroll = -1;
    var curm = $(".cabinet-menu .current");
    if (curm.offset().left + curm.width() > 380) {
        $(".cabinet-menu").scrollLeft(curm.offset().left + curm.width() - 340);
        $("#scroll-nav>span.left").removeClass("hidden");
    }

    $("body").delegate(".order-window a.close", "click", function(ev) {
        ev.preventDefault();
        var wn = $(".order-window");
        if (wn.length==1) {
            $("main, footer").show();
            $(".cabinet-menu").css({visibility: ""});
            history.pushState({}, "", "#");
            $(window).scrollTop(lastScroll);
        } else {
            wn.eq(wn.length-2).show();
        }
        wn.eq(wn.length-1).remove();
        return false;
    });

    $(document.body).on("keydown", function(ev) {
        if ($(".order-window").length>0 && ev.which==27) {
            $(".order-window a.close").eq(0).trigger("click");
        }
    });

    var showClaim = function(id) {
        var win = $("<div class='order-window set-claim-form'><h3><a href='#' class='close'></a><span></span></h3></div>");
            win.appendTo(document.body);
            $.get($_AJAX_PATHES.cabinet.getClaim, { action: "get-claim", id: id }, function(data) {
                if (data.state=="ok") {
                    lastScroll = $(window).scrollTop();
                    $(window).scrollTop(0);
                    $("main, footer").hide();
                    $(".cabinet-menu").css({visibility: "hidden"});
                    claim = data.claim;
                    win.find("h3>span").text(claim.title);
                    win.append($('<div class="product-info"><div class="image"><img src="'+claim.item.image+'"></div><div class="title"><span class="article">Артикул '+claim.item.article+'</span><strong>'+claim.item.title+'</strong><div class="price"><em>'+formatNum(claim.item.price)+'</em> <span class="rub">р</span></div></div></div>'));
                    win.append($('<div class="claim-info"><h6>Суть претензии:</h6><p></p><div class="attachments"><ul></ul></div></div>'));
                    win.find(".claim-info p").text(claim.text);
                    for (var i=0; i<data.claim.attachments.length; i++) {
                        var att = data.claim.attachments[i];
                        win.find(".attachments ul").append("<li><div class='preview' style='background-image: url("+att.preview+")'></div><div class='descr'><strong>"+att.title+"</strong></div></li>");
                    }
                    win.append("<div class='status'>"+claim.state+"</div>");
                    win.append("<div class='go-back'><a href='#' class='close'>к списку претензий</a></div>");
                }
            });

    }

    var showOrder = function(id, agent) {
		lastScroll = $(window).scrollTop();
        $(".orders-list").hide();
        var win = $("<div class='order-window -preload'><h3><a href='#' class='close'></a><span></span><strong><em>0</em><span class='rub'> р</span></strong></h3></div>");
            win.appendTo(document.body);

        $.get($_AJAX_PATHES.cabinet.orders, { action: "get-order", id: id, agent: agent }, function(data) {
				$(".order-window").removeClass('-preload');
            $(".orders-list").show();
            if (data.state=="ok") {
                var ord = data.order, win = $(".order-window"), cnt = $("<div class='order-content no-files'></div>");
                var cancelable = "";
                if (typeof(ord.cancelable)!="undefined" && ord.cancelable) {
                    cancelable = "<a href='#' class='-cancel-order'>отменить</a>";
                }
                win.data("id", ord.id);
                history.pushState({}, "", "#"+ord.id+","+agent);
                win.find("h3>span").text(ord.name);
                win.find("h3>strong em").text(formatNum(ord.amount));
                cnt.append("<div class='description'></div>");
                cnt.find(".description").append("<div class='field nomobile'><label>Сумма</label><span>"+formatNum(ord.amount)+" <span class='rub'>р</span></span></div>");
                cnt.find(".description").append("<div class='field'><label>Создан</label><span>"+ord.date+cancelable+"</span></div>");
                if (ord.agent!="") cnt.find(".description").append("<div class='field'><label>Контрагент</label><span>"+ord.agent+"</span></div>");
					 if (ord.address!="") cnt.find(".description").append("<div class='field'><label>Адрес</label><span>"+ord.address+"</span></div>");
				if (ord.deliveryCost > 0) cnt.find(".description").append("<div class='field'><label>Доставка</label><span>"+formatNum(ord.deliveryCost)+" <span class='rub'>р</span>"+(ord.needLifting && ord.floor ? ", включая подъем на "+ord.floor+"-й этаж " : "")+(ord.elevatorType ? ', ' + ord.elevatorType : '')+"</span></div>");
                if (ord.deliveryDate!="") cnt.find(".description").append("<div class='field'><label>Привезем</label><span>"+ord.deliveryDate+"</span></div>");
					 if (ord.manager!="") cnt.find(".description").append("<div class='field'><label>Менеджер</label><span>"+ord.manager+"</a></span></div>");
					 if (ord.phone!="") cnt.find(".description").append("<div class='field'><label>Покупатель</label><span><a href='tel:"+ord.phone+"'>"+ord.phone+"</a></span></div>");
                cnt.find(".description").append("<div class='field'><label>Статус</label><span>"+ord.fullState+"</span></div>");
					 if (ord.comment || ('' == ord.comment)) {
                    cnt.find(".description").append("<div class='field -comment first'><label>Коммент. 1</label><span>"+ord.comment+"</span></div>");
                    if ($("body").hasClass('manager')) {
                        cnt.find(".-comment.first").prepend("<a href='#' class='-edit'></a>");
                        $("<div class='-edit-comment'><textarea>"+ord.comment.replace(/\</, '&lt;')+"</textarea><a href='#' class='-cancel'></a><a href='#' class='-save'></a></div>").insertAfter(cnt.find(".-comment.first span"));
                    }
                }
					 if (ord.comment2) {
                    cnt.find(".description").append("<div class='field -comment -scnd'><label>Коммент. 2</label><span>"+ord.comment2+"</span></div>");
                    if ($("body").hasClass('manager')) {
                        cnt.find(".-comment.-scnd").prepend("<a href='#' class='-edit'></a>");
                        $("<div class='-edit-comment'><textarea>"+ord.comment2.replace(/\</, '&lt;')+"</textarea><a href='#' class='-cancel'></a><a href='#' class='-save'></a></div>").insertAfter(cnt.find(".-comment.-scnd span"));
                    }
                }
                if (ord.extend) {
                    cnt.find(".description").append("<div class='field reserve extend'><label></label><span><a href='#'>"+ord.extend+"</a></span></div>");
                }
                if (ord.onlinePayment) {
                    cnt.find(".description").append("<div class='field reserve payment'><label></label><span><a href='"+ord.onlinePayment+"'>"+ord.onlinePaymentButton+"</a></span></div>");
                }

                if (ord.files.length>0) {
                    cnt.append("<ul class='files'></ul>");
                    for (var i=0; i<ord.files.length; i++) {
                        var li = $("<li><em>"+ord.files[i].type+"</em><span>"+ord.files[i].name+"</span><a href='"+ord.files[i].href+"'>скачать</a></li>");
                        cnt.find(".files").append(li);
                    }
                    cnt.append("<div class='files-collapse'><a href='#'>Документы для скачивания</a></div>");
                }

                if (ord.items.length>0) {
                    cnt.append("<ul class='items'></ul>");
                    for (var i=0; i<ord.items.length; i++) {
                        var item = ord.items[i];
                        var li = $("<li><div class='image'><a href='"+item.href+"'><img src='"+item.image+"'></a></div><div class='title'><span class='article'><a href='"+item.href+"'>Артикул "+item.article+"</a></span>"+item.title+"<div class='price'>"+formatNum(item.price)+" <span class='rub'>р</span>"+(item.count > 1 ? " &times; " + item.count + " шт." : "")+"</div></li>");
                            li.data("id", item.id);
                        cnt.find(".items").append(li);
                        if (item.claimed*1==1) li.append("<div class='claim-wait'>заявка на претензию в рассмотрении</div>");
                        else if (item.claimed*1==0 || isNaN(item.claimed*1)) {
									// @temp
                            //li.append("<a href='#' class='claim'>претензия</a>");
                            li.find(".claim").on("click", function(ev) {
                                ev.preventDefault();
                                showClaimForm($(this).parents("li"));
                                return false;
                            });
                        }
                    }
                }

                cnt.append("<div class='order-footer'>Итого "+formatNum(ord.amount)+" <span class='rub'>р</span><div class='back'><a href='#'>вернуться к заказам</a></div></div>");
                cnt.find(".back a").on("click", function(ev) {
                    ev.preventDefault();
                    $("div.order-window h3 a.close").trigger("click");
                    return false;
                });

					 if (ord.notice) {
                    $("<div id='order-notice'>"+ord.notice+"</div>").insertBefore(cnt.find(".items"));
                }
                cnt.find(".-cancel-order").on("click", function(e) {
                    e.preventDefault();
                    if (!confirm("Точно отменяем заказ?")) return false;
                    var $this = $(this);
                        $this.hide();
                    $.post($_AJAX_PATHES.cabinet.orders, { action: "cancel", id: ord.id  }, function(resp) {
                            $this.show();
                            if (resp.state!="ok") return alert(resp.message);
                            $(".order-window").remove();
                            showOrder(ord.id, agent);
                        });
                    return false;
                });
                win.append(cnt);
					 var defcom = "";
                $("div.order-window .order-content .description .field a.-edit").on('click', function(ev) {
                    ev.preventDefault();
                    defcom = $(this).parents(".field").addClass("edit-mode").find("textarea").val();
                    return false;
                });
                $("div.order-window .order-content .description .field a.-cancel").on("click", function(ev) {
                    ev.preventDefault();
                    $(this).parents(".field").removeClass("edit-mode").find("textarea").val(defcom);
                    return false;
                });
                $("div.order-window .order-content .description .field a.-save").on("click", function(ev) {
                    ev.preventDefault();
						  var number = $(this).parents(".field").hasClass("-scnd") ?  2 : 1;
						  $.post($_AJAX_PATHES.cabinet.orders, { action: "set-comment", number: number, id: ord.id, comment: $(this).parents(".field").find("textarea").val() }, 
                        function(data) {
                          if (data.state=="ok") {
                            $("div.order-window .order-content .description .field.-comment").removeClass("edit-mode");
									 if (1 == number) {
	                            $("div.order-window .order-content .description .field.-comment.first span").html(data.comment);
		                         $("div.order-window .order-content .description .field.-comment.first textarea").val(data.comment);
									} else {
										$("div.order-window .order-content .description .field.-comment.-scnd span").html(data.comment);
		                         $("div.order-window .order-content .description .field.-comment.-scnd textarea").val(data.comment);
									}
                          }
                        });
                    return false;
                });
                $(window).scrollTop(0);
                $("main, footer").hide();
                $(".cabinet-menu").css({visibility: "hidden"});
                $(".files-collapse a").on("click", function(ev) {
                    $(this).parents(".order-content").toggleClass("no-files");
                    if ($(this).parents(".order-content").hasClass('no-files')) {
                        $(this).text("Документы для скачивания");
                    } else {
                        $(this).text("свернуть");
                    }
                    return false;
                });
                $(".field.reserve.extend span a").on("click", function(ev) {
                    ev.preventDefault();
                    if ($(this).hasClass("disabled")) return false;
                    $(this).addClass('disabled');
                    var $this = $(this);
                    $.get($_AJAX_PATHES.cabinet.extendOrder, { action: "extend", id: win.data("id") }, function(data) {
                        if (data.state!="ok") {
                            $this.removeClass('disabled');
                            return alert(data.message);
                        }
                        location.reload();
                    });
                    return false;
                });
            } else win.find("a.close").trigger("click");
        });
    }

    $('.filter .expander').on("click", function(ev) {
        $(this).parents(".filter").toggleClass('expanded');
    });

    var filter_skip = $(".filter ul").length;
    var refreshOrders = function(ev) {
        try {
            var val = $(this).data('value');
                $(this).parents(".filter").data("value", val).removeClass('expanded').find("li").removeClass('selected');
                $(this).addClass("selected");
                $(this).parents(".filter").find(".expander").html($(this).html());
            }
        catch(e) {}
            var data = {}, $sel = $(".filters .filter");
            for (var i=0; i<$sel.length; i++) {
                var _x = $sel.eq(i).find("input");
                if (_x.length==0) {
                    data["f-"+$sel.eq(i).data("name")] = $sel.eq(i).data("value");
                } else {
                    var vals = [];
                    for (var _i=0; _i<_x.length; _i++) vals.push(_x.eq(_i).val());
                    data["f-"+$sel.eq(i).data("name")] = vals.join(";");
                }
            }
            data.action="get-list";
            var url = $_AJAX_PATHES.cabinet.orders;
            if ($(document.body).hasClass('claims')) url = $_AJAX_PATHES.cabinet.claimsList;
            if ($(document.body).hasClass('documents')) url = $_AJAX_PATHES.cabinet.documentsList;
            if ($(document.body).hasClass('requisites')) url = $_AJAX_PATHES.cabinet.requisites;
            if (--filter_skip>0) return true;
				$(".orders-list").html("").addClass('-preload').show();
            $.get(url, data, function(data) {
					$(".orders-list").removeClass('-preload');
                if (data.state=="ok" && !$(document.body).hasClass('claims') && !$(document.body).hasClass('documents') && !$(document.body).hasClass('requisites')) {
                    $(".orders-list").html("");
                    if (!$("body").hasClass("manager")) {
                        for (var i=0; i<data.orders.length; i++) {
                            var li = $("<li></li>"), order = data.orders[i];
                            if (order.done) li.addClass("done");
                            li.append("<a href='#' data-id='"+order.id+"' data-agent='"+order.agent+"'><span><strong>"+order.name+"</strong><em>"+order.date+"</em></span><span><b>"+order.state+"</b><i>"+formatNum(order.amount)+" <span class='rub'>р</span></a>");
                            $(".orders-list").append(li);
                            li.find("a").on("click", function(ev) {
                                ev.preventDefault();
                                showOrder($(this).data("id"), $(this).data("agent"));
                                return false;
                            });
                        }
                    } else {
                        $(".orders-list").html("<li class='thead'><span class='-id'>№</span><span class='-time'>дата-время</span><span class='-count'>кол-во</span><span class='-am'>сумма</span><span class='-mng'>менеджер</span><span class='-st'>статус</span></li>");
                        var _now = new Date(), _d = _now.getDate(), _m = _now.getMonth()+1, _y = _now.getFullYear() % 100;
                        if (_d<10) _d="0"+_d;
                        if (_m<10) _m="0"+_m;
                        _now = _d+"."+_m+"."+_y;
                        var total = 0, fl = false;
                        for (var i=0; i<data.orders.length; i++) {
                            var li = $("<li></li>"), order = data.orders[i];
                            var ttm = order.date;
                            console.log(order.date, _now, order.date.replace(/^.*(\d{2})$/, '$1'), _y);
                                if (ttm==_now) ttm = order.time;
										  else if (order.date.replace(/^.*\.(\d{2})$/, '$1')*1==_y) ttm = order.date.replace(/^(\d+\.\d+)\..*$/, '$1')+"<i></i> "+order.time;
                                else ttm = order.date ;
                            if (order.failed) li.addClass("failed");
                            li.append("<span class='-id'><a href='#"+order.id+","+order.agent+"' data-id='"+order.id+"' data-agent='"+order.agent+"'>"+order.id+"</a></span>");
                            li.append("<span class='-time'><i>"+ttm+"</i></span>");
                            li.append("<span class='-count'>"+order.count+" шт.</span>");
                            li.append("<span class='-am'>"+formatNum(order.amount)+" <span class='rub'>р</span></span>");
									 li.append("<span class='-mng'>"+order.managerName+"</span>");
                            li.append("<span class='-st'>"+order.state+"</span>");
                            if (!order.failed && order.canSummarize) total += order.amount; else fl = true;
                            $(".orders-list").append(li);
                            li.find("a").on("click", function(ev) {
                                ev.preventDefault();
                                showOrder($(this).data("id"), $(this).data("agent"));
                                return false;
                            });
                        }
                        var footer=$('<li class="tfoot'+(fl?' -fl':'')+'">Итого отгружено за период</span>: <strong>'+formatNum(total)+' <span class="rub">р</span></strong>');
                        footer.appendTo(".orders-list");
                        $("div.nothing-found").toggle(data.orders.length==0);
                        $(".orders-list").toggle(data.orders.length!=0);
                    }
                    if (typeof(data["f-type"])!="undefined") {
                        var lt = $(".filter[data-name=type]"), lit = lt.find("li");
                        lit.removeClass("null");
                        for (var i=0; i<lit.length; i++) {
                            var l = lit.eq(i), id = l.data('value');
                            if (typeof(data["f-type"][id])=="undefined") continue;
                            l.find("b").text(data["f-type"][id]);
                            if (data["f-type"][id]*1==0) l.addClass("null");
                            if (lt.data('value')==id) lt.find('.expander b').text(data["f-type"][id]);
                        }
                    }
                }
                else if (data.state=="ok" && $(document.body).hasClass('claims')) {
                    $(".claims-list").html("");
                    for (var i=0; i<data.claims.length; i++) {
                        var li = $("<li></li>"), claim = data.claims[i];
                        li.append("<a href='#' data-id='"+claim.id+"'><span><strong>"+claim.title+"</strong><em>от "+claim.date+"</em></span><p><strong>"+claim.item+"</strong></p><p class='state'>"+claim.state+"</p></a>");
                        $(".claims-list").append(li);
                        li.find("a").on("click", function(ev) {
                            ev.preventDefault();
                            showClaim($(this).data("id"));
                            return false;
                        });
                    }
                }
                else if (data.state=="ok" && $(document.body).hasClass('documents')) {
                    $(".documents-list").html("");
                    for (var i=0; i<data.orders.length; i++) {
                        var order = data.orders[i];
                        var li = $("<li><div class='title'><strong>"+order.name+"</strong><em>"+formatNum(order.amount)+" <span class='rub'>р</span></em></div><div class='details'><strong>"+order.date+"</strong><em>"+order.paymentType+"</em></div><ul class='files'></ul></li>"), order = data.orders[i];
                        for (var j=0; j<order.docs.length; j++) {
                            li.find("ul.files").append("<li><em>"+order.docs[j].type+"</em><span>"+order.docs[j].title+"</span><a href='"+order.docs[j].href+"'>скачать</a></li>");
                        }
                        $(".documents-list").append(li);
                    }
                }
                else if (data.state=="ok" && $(document.body).hasClass('requisites')) {
                    $(".req ul").html("");
                    for (var i=0; i<data.requisites.length; i++) {
                        var req = data.requisites[i];
                        var li = $("<li><span>"+req.title+"</span><em>"+req.value+"</em></li>");
                        $(".req ul").append(li);
                    }
                }
            });
    }
	 $(".filter ul li").on("click", refreshOrders);

    $(".filter li.selected").each(function() {
        $(this).trigger("click");
    });


    initScroll();
    var o = $("header .cabinet-menu"),
        _m=function(x) {
        o.parent().find(".left").toggleClass("hidden", (x<10));
        o.parent().find(".right").toggleClass("hidden", (o[0].scrollWidth-window.innerWidth)-10<=x);
    };
    o.onmove=_m;
    o.onstop=_m;
    setSwipe(o);

    if (location.hash.replace(/\#/g, '')!='') {
        var t = location.hash.replace(/\#/g, '').split(",");
        showOrder(t[0], t[1]);
    }

	 $(".-date").on("click", function() {
        xCal(this, {
            fn: function(a,b) {
					 var y = a.replace(/^\d+.\d+.(\d+)$/, '$1');
                if (y>100) y = y%100;
                $(b.o).val(a.replace(/^(\d+.\d+).\d+$/, '$1')+'.'+y);
                refreshOrders();
            }
        });
    });

    $(".manager div.filters .filter.search-block .form input")
        .on("focus", function() {
            $(this).parents(".search-block").addClass('focused');
        })
        .on("blur", function(e) {
            if ($(e.relatedTarget).hasClass("submit")) return true;
            $(this).parents(".search-block").removeClass('focused');
        })
        .on("keydown", function(e) {
            if (e.which==13) $(this).next().trigger("click"), $(this).blur();
        });

    $(".manager div.filters .filter.search-block .form .submit").on("click", function(ev) {
        ev.preventDefault(0);
        if ($(this).prev().val().trim()=="" && !$(this).parents(".filter").hasClass("focused")) return 0*parseInt($(this).prev().focus());
        refreshOrders();
        $(this).parents(".search-block").removeClass('focused');
        return false;
    });
    $(".manager div.filters .filter.date input").on("focus", function() {
        $(".manager div.filters .filter.date input").removeClass('-clicked');
        $(this).addClass('-clicked');
    });
    setInterval(function() {
        $(".manager div.filters .filter.date input").removeClass("focused");
        if ($('#xcalend:visible').length>0) $(".manager div.filters .filter.date input.-clicked").addClass("focused");
    }, 100);

	 window.onpopstate = function(ev) {
        if (location.hash.replace(/\#/g, '').trim()!="") {
            $(".order-window a.close").trigger("click");
            var t = location.hash.replace(/\#/g, '').split(",");
            if (t[0]*1+t[1]*1) showOrder(t[0], t[1]);
        } else {
            $(".order-window a.close").trigger("click");
				setTimeout(function() {
                $('html,body').stop().scrollTop(lastScroll);
            }, 10);
        }
    }

});

showClaimForm = function(item) {
    $(".order-window").hide();
    var win = $('<div class="order-window set-claim-form"><h3><a href="#" class="close"></a><span>Претензия</span></h3><div class="product-info"><div class="image"><img></div><div class="title"><span class="article"></span><strong></strong><div class="price"><em></em> <span class="rub">р</span></div></div></div></div>');
        win.append("<div class='claim-form-block'><div class='text'><h6>Укажите суть претензии:</h6><textarea name='claim-text'></textarea></div></div>");
        win.find(".claim-form-block").append("<div class='attachments'><ul></ul><div class='browse'><span>Приложить <i>еще</i> фото...</span><input type='file' accept='image/*' multiple='true'></div></div>");
        win.find(".claim-form-block").append("<div class='claim-board'><a href='#' class='send'>Отправить претензию</a><br><a href='#' class='cancel'>Отмена</a></div>");
        win.find(".image img").attr('src', item.find('.image img').attr("src"));
        win.find(".article").text(item.find('.article').text());
        win.find(".title>strong").text(item.find('.title>a').text());
        win.find(".price>em").text(item.find('.price').text().replace(/[^0-9\s]/g, ''));
    lastScroll = $(window).scrollTop();
    $(window).scrollTop(0);
    $("main, footer").hide();
    $(".cabinet-menu").css({visibility: "hidden"});

    win.find(".cancel").on("click", function(ev) {
        ev.preventDefault();
        $(".order-window a.close").eq(0).trigger("click");
        return false;
    });
    win.find(".send").on("click", function(ev) {
        ev.preventDefault();
        if ($(this).hasClass('disabled')) return false;
        if ($(".claim-form-block textarea").val().trim()=="") {
            alert("Пожалуйста, опишите вашу претензию");
            return $(".claim-form-block textarea").focus();
        }
        if ($(".attachments li.upl").length>0) {
            alert("Пожалуйста, дождитесь загрузки всех фотографий");
            return false;
        }
        $(this).addClass('disabled');
        $(this).next().hide();
        var att = [], f = $(".attachments li");
        for (var i=0; i<f.length; i++) att.push(f.eq(i).data("id"));
        $.post($_AJAX_PATHES.cabinet.claimSend, { action: "set-claim", attachments: att }, function(data) {
            $(".order-window .product-info, .order-window .claim-form-block").remove();
            $(".order-window.set-claim-form h3>span").text("Претензия принята");
            $(".order-window.set-claim-form").append("<div class='done'><p>"+data.message+"</p><div><a href='"+data.claimsLink+"'>к списку претензий</a></div>");
        });
        return false;
    });

    var uploadFile = function(file, item) {
        var reader = new FileReader();
            reader.onloadend = function() {
                item.find(".preview").css({backgroundImage: 'url("'+reader.result+'")'});
                var upl = reader.result.replace(/^.*base64\,(.*)$/, '$1');
                $.post($_AJAX_PATHES.cabinet.claimUpload, { action: "img-upload", image: upl }, function(data) {
                    item.removeClass("upl");
                    if (data.state!="ok") {
                        item.find("em").addClass("error").html(data.message+"&nbsp;&nbsp;&nbsp;");
                    } else {
                        item.data("id", data.id);
                        item.find("em").text("");
                    }
                    item.find("em").append("<a href='#' class='remove'>удалить</a>");
                    item.find("em a.remove").on("click", function(ev) {
                        ev.preventDefault();
                        var id = $(this).parents('li').data('id');
                        $.post($_AJAX_PATHES.cabinet.claimLoadedRemove, { action: 'img-remove', id: id }, function(data) {
                            if (item.parent().find("li").length==1) $(".claim-form-block .browse").removeClass("more");
                            item.remove();
                        });
                        return false;
                    });
                });
            }
        reader.readAsDataURL(file);
    }

    win.find("input[type=file]").on("change", function(ev) {
        var files = this.files;
        if (files.length==0) return false;
        for (var i=0; i<files.length; i++) {
            if (!files[i].type.match(/^image\//)) continue;
            var li = $("<li class='file upl'><div class='preview'></div><div class='descr'><strong>"+files[i].name+"</strong><em>загружается...&nbsp;&nbsp;&nbsp;<a href='#' class='cancel-upload'>отменить</a></em></div></li>");
            li.appendTo(win.find(".attachments ul"));
            win.find(".browse").addClass('more');
            uploadFile(files[i], li);
        }
    });

    win.appendTo(document.body);
}