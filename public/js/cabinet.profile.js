$(function() {

    $("a.change-pass").on("click", function(ev) {
        ev.preventDefault();
        win = $("<div class='order-window new-pass'><h3><a href='#' class='close'></a><span>Изменение пароля</span></h3></div>");
        win.append("<div class='new-pass-form'><div class='field'><p>Текущий пароль</p><input type='password' class='cpass'></div><div class='field'><p>Новый пароль</p><input type='password' class='npass1'></div><div class='field'><p>Новый пароль еще раз</p><input type='password' class='npass2'></div><div class='buttons'><a href='#' class='save-pass'>Сохранить пароль</a><a href='#' class='cancel'>Отмена</a></div></div>");
        //win.append("<div class='buttons'><a href='#' class='done'>Сохранить пароль</a><a href='#' class='cancel'>Отмена</a></div>");

        $(document.body).append(win);
        lastScroll = $(window).scrollTop();
        $(window).scrollTop(0);
        $("main, footer").hide();
        $(".cabinet-menu").css({visibility: "hidden"});

        win.find(".cancel").on("click", function(ev) {
            ev.preventDefault();
            win.find("h3 .close").trigger("click");
            return false;
        });

        win.find(".save-pass").on("click", function(ev) {
            if ($(this).hasClass("disabled")) return false;
            $(this).addClass("disabled");
            var $this = $(this);
            $.post($_AJAX_PATHES.cabinet.profile, { action: "change-pass", current: $("input.cpass").val(), pass1: $("input.npass1").val(), pass2: $("input.npass2").val() }, 
                function(data) {
                    $this.removeClass("disabled");
                    if (data.state!="ok") return alert(data.message);
                    win.find("a.close").trigger('click');
            });
        });

			setPass();
        return false;
    });

    $("a.save").on("click", function(ev) {
        ev.preventDefault();
        if ($(this).hasClass('disabled')) return false;
        var ok = true;
        var f = $("input");
        for (var i=0; i<f.length; i++) {
            if (f.eq(i).val().trim()=="") ok=false;
        }
        if (!ok) return alert("Пожалуйста, заполните все поля");        
        $(this).addClass('disabled');
        var $this = $(this);
        $.post($_AJAX_PATHES.cabinet.profile, { action: "save", phone: $("input.phone").val(), email: $("input.email").val() },
            function(data) {
                if (data.state=="error") return alert(data.message);
                $this.removeClass('disabled').addClass('done');
            });
        return false;
    });

    $("input").on('keydown change', function(ev) {
        $("a.save").removeClass('done');
    }); 


});