$(document).ready(function() {
	GaEcommerceHelper.init();
});

var GaEcommerceHelper = {

	init: function() {
		if (0 < $('#ga-ecommerce-data').length) {
			this.dataContainer = $('#ga-ecommerce-data');
			this.trackPage();
		}
	},

	trackPage: function() {
		var pageType = this.getPageType();
		switch (pageType) {
			case 'main':
				//this.trackMainPage();
				break;
			case 'category':
			case 'tags':
			case 'brand':
			case 'filter':
			case 'search':
			case 'compatibles':
			case 'promo':
				//this.trackCategoryPage();
				break;
			case 'product':
				//this.trackProductPage();
				break;
			case 'basket':
				//this.trackCartPage();
				break;
			case 'order2':
				this.trackOrder2Page();
				break;
			case 'order3':
				this.trackOrder3Page();
				break;
			case 'order4':
				this.trackOrder4Page();
				break;
			case 'purchase':
				this.trackOrderPurchasePage();
				break;
			case 'portal':
			case 'static':
			case 'contacts':
				//this.trackGenericPage();
				break;
		}

		//this.trackImpressions();
		this.trackNonImpressions();
		this.trackPromoActions();

		$('.product-link').each(function() {
			var _ = $(this);
			_.click(function(e) {
				console.log(e);
				var link = _.attr('href');
				var id = _.attr('data-id');
				var name = _.attr('data-name');
				var category = _.attr('data-category');
				var brand = _.attr('data-brand');
				var price = parseInt(_.attr('data-price'));
				var actionField = _.attr('data-action');
				var position = _.attr('data-position');
				GaEcommerceHelper.clickProductLink(link, id, name, category, brand, price, actionField, position);
				if (!e.ctrlKey && !e.shiftKey) {
					window.location.href = link;
				}
			});
		});
	},

	trackGenericPage: function() {
		var obj = this.createDataObject();
		this.sendDataObject(obj);
	},

	trackMainPage: function() {
		var obj = this.createDataObject();
		this.sendDataObject(obj);
	},

	trackCategoryPage: function() {
		var obj = this.createDataObject();
		this.sendDataObject(obj);
	},

	trackProductPage: function() {
		var obj = this.createDataObject();
		obj.dimension18 = this.getAttr('availability');
		obj.ecommerce.detail = {
			products: [{
				id: this.getAttr('product-id'),
				name: this.getAttr('product-name'),
				category: this.getAttr('product-category'),
				price: this.getAttr('product-price'),
				dimension18: this.getAttr('availability')
			}]
		};
		this.sendDataObject(obj);
	},

	trackCartPage: function() {
		var obj = this.createDataObject();
		obj.ecommerce.checkout = {
			step: 1,
			products: []
		};
		this.dataContainer.find('div.basket-product').each(function() {
			var row = $(this);
			obj.ecommerce.checkout.products.push({
				id: row.attr('data-id'),
				name: row.attr('data-name'),
				category: row.attr('data-category'),
				price: row.attr('data-price'),
				quantity: row.attr('data-quantity')
			});
		});
		this.sendDataObject(obj);
	},

	trackNonImpressions: function() {
		var impressions = this.getImpressions();
		while (0 < impressions.length) {
			var obj = {
				event: 'CategoryView',
				nonInteraction: true,
				ecommerce: {
					impressions: impressions.splice(0, 10)
				}
			};
			this.sendDataObject(obj);
		}
	},

	trackNonImpressionsFromBlock: function(blockIdentity) {
		var impressions = this.getImpressions(blockIdentity);
		if (0 < impressions.length) {
			var obj = {
				event: 'Non_impressions',
				eventCategory: 'Non-Interactions',
				eventAction: 'show',
				eventLabel: 'products',
				ecommerce: {
					impressions: impressions
				}
			};
			this.sendDataObject(obj);
		}
	},

	addProductToCart: function(id, name, category, brand, price, quantity) {
		var obj = this.createDataObject();
		obj.event = 'addToCart';
		obj.nonInteraction = false;
		obj.ecommerce.currencyCode = 'RUR';
		obj.ecommerce.add = {
			products: [{
				id: id,
				name: name,
				category: category,
				brand: brand,
				price: price,
				quantity: quantity || 1
			}]
		}
		this.sendDataObject(obj);
	},

	removeProductFromCart: function(id, name, category, price, brand, quantity) {
		if ((null == quantity) || (0 == quantity)) {
			return;
		}
		var obj = this.createDataObject();
		obj.event = 'removeFromCart';
		obj.nonInteraction = true;
		obj.ecommerce.currencyCode = 'RUR';
		obj.ecommerce.remove = {
			products: [{
				id: id,
				name: name,
				category: category,
				price: price,
				quantity: quantity || 1
			}]
		}
		this.sendDataObject(obj);
	},

	clickProductLink: function(link, id, name, category, brand, price, actionField, position, successFunc) {
		var obj = {
			event: 'ProductClick',
			nonInteraction: true,
			ecommerce: {
				click: {
					actionField: { list: actionField || 'generic' },
					products: [{
						id: id,
						name: name,
						category: category,
						brand: brand,
						price: price,
						list: (actionField || 'generic'),
						position: position
					}]
				}
			}
		};
		this.sendDataObject(obj);
		if (successFunc) {
			successFunc();
		}
	},

	trackPromoActions: function() {
		var actions = this.getPromoActions();
		if (0 < actions.length) {
			var obj = this.createDataObject();
			obj.event = 'promoView';
			obj.ecommerce.promoView = {
				promotions: actions
			};
			if (obj.ecommerce.impressions) {
				delete obj.ecommerce.impressions;
			}
			this.sendDataObject(obj);
		}
	},

	clickPromo: function(link, id, name, creative, position) {
		var obj = this.createDataObject();
		obj.event = 'promotionClick';
		obj.ecommerce.promoClick = {
			promotions: [{
				id: id,
				name: name,
				creative: creative,
				position: position
			}]
		};
		obj.eventCallback = function() {
			document.location = link;
		};
		if (obj.ecommerce.impressions) {
			delete obj.ecommerce.impressions;
		}
		this.sendDataObject(obj);
		if ((navigator.appVersion.indexOf("Mac")!=-1)) {
			document.location = link;
		}
	},

	trackQuickOrderPage: function(order) {
		var obj = this.createDataObject();
		obj.event = 'order';
		obj.type_purchase = 'one_click';
		obj.ecommerce.purchase = {
			actionField: {
				id: order.id,
				revenue: order.amount,
				tax: 0.00,
				shipping: 0.00,
				coupon: ''
			},
			products: []
		};
		for (var i = 0; i < order.products.length; i++) {
			var product = order.products[i];
			obj.ecommerce.purchase.products.push({
				id: product.id,
				name: product.name,
				category: product.categoryName,
				price: product.price,
				quantity: product.quantity
			});
		}
		this.sendDataObject(obj);
	},

	trackOrder2Page: function() {
		var obj = this.createDataObject();
		obj.event = 'CheckoutStep2';
		obj.nonInteraction = false;
		obj.ecommerce = {
			checkout: {
				actionField: {
					step: '2'
				},
				products: []
			}
		};
		this.dataContainer.find('div.basket-product').each(function() {
			var row = $(this);
			obj.ecommerce.checkout.products.push({
				id: row.attr('data-id'),
				name: row.attr('data-name'),
				category: row.attr('data-category'),
				brand: row.attr('data-brand'),
				price: row.attr('data-price'),
				quantity: row.attr('data-quantity')
			});
		});
		this.sendDataObject(obj);
	},

	trackOrder3Page: function() {
		var obj = this.createDataObject();
		obj.event = 'CheckoutStep3';
		obj.nonInteraction = false;
		obj.ecommerce = {
			checkout: {
				actionField: {
					step: '3'
				},
				products: []
			}
		};
		this.dataContainer.find('div.basket-product').each(function() {
			var row = $(this);
			obj.ecommerce.checkout.products.push({
				id: row.attr('data-id'),
				name: row.attr('data-name'),
				category: row.attr('data-category'),
				brand: row.attr('data-brand'),
				price: row.attr('data-price'),
				quantity: row.attr('data-quantity')
			});
		});
		this.sendDataObject(obj);
	},

	trackOrder4Page: function() {
		var obj = this.createDataObject();
		obj.event = 'CheckoutStep4';
		obj.nonInteraction = false;
		obj.ecommerce = {
			checkout: {
				actionField: {
					step: '4'
				},
				products: []
			}
		};
		this.dataContainer.find('div.basket-product').each(function() {
			var row = $(this);
			obj.ecommerce.checkout.products.push({
				id: row.attr('data-id'),
				name: row.attr('data-name'),
				category: row.attr('data-category'),
				brand: row.attr('data-brand'),
				price: row.attr('data-price'),
				quantity: row.attr('data-quantity')
			});
		});
		this.sendDataObject(obj);
	},

	trackOrderPurchasePage: function() {
		var obj = this.createDataObject();
		obj.event = 'purchase';
		obj.nonInteraction = false;
		obj.ecommerce = {
			purchase: {
				actionField: {
					id: this.getAttr('data-order-id'),
					affiliation: 'santbaza.ru',
					revenue: this.getAttr('data-order-amount'),
					tax: 0,
					shipping: this.getAttr('data-order-delivery-amount'),
					coupon: ''
				},
				products: []
			}
		};
		this.dataContainer.find('div.basket-product').each(function() {
			var row = $(this);
			obj.ecommerce.purchase.products.push({
				id: row.attr('data-id'),
				name: row.attr('data-name'),
				category: row.attr('data-category'),
				brand: row.attr('data-brand'),
				price: row.attr('data-price'),
				quantity: row.attr('data-quantity')
			});
		});
		this.sendDataObject(obj);
	},

	createDataObject: function() {
		var obj = {
			ecommerce: {}
		};
		return obj;
	},

	getAttr: function(attrName) {
		return this.dataContainer.attr('data-' + attrName);
	},

	getPageType: function() {
		return this.getAttr('page-type');
	},

	getClientId: function() {
		return this.getAttr('client-id');
	},

	getUserId: function() {
		return this.getAttr('user-id');
	},

	getAuth: function() {
		return this.getAttr('auth');
	},

	getCity: function() {
		return this.getAttr('city');
	},

	getImpressions: function(blockIdentity) {
		var container = this.dataContainer;
		if (blockIdentity) {
			container = $(blockIdentity);
		}

		var list = [];
		container.find('div.impression').each(function() {
			var product = $(this);
			var obj = {
				id: product.attr('data-id'),
				name: product.attr('data-name'),
				category: product.attr('data-category'),
				price: product.attr('data-price'),
				brand: product.attr('data-brand'),
				position: product.attr('data-position'),
				list: product.attr('data-list')
			};
			list.push(obj);
		});
		return list;
	},

	getPromoActions: function() {
		var list = [];
		this.dataContainer.find('div.promo-action').each(function() {
			var action = $(this);
			var obj = {
				id: action.attr('data-id'),
				name: action.attr('data-name'),
				creative: action.attr('data-creative'),
				position: action.attr('data-position')
			};
			list.push(obj);
		});
		return list;
	},

	sendDataObject: function(dataObject) {
		console.log('ga push', dataObject);
		dataLayer.push(dataObject);
	}

};