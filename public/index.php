<?php
error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT ^ E_DEPRECATED);
//ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
ini_set('date.timezone', 'Europe/Moscow');
setlocale(LC_ALL, 'ru_RU.UTF-8');

/**
 * This makes our life easier when dealing with paths.
 * Everything is relative
 * to the application root now.
 */
$dir = dirname(__DIR__);
chdir($dir);

// Setup autoloading
require 'init_autoloader.php';

define('DEBUG_MODE', isset($_GET['debug']), true);
define('DOMAIN_NAME', 'santehbaza.ru');

// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();
