<?php

namespace Google\Controller\Plugin;

use Solo\Mvc\Controller\Plugin\AbstractPlugin;

class GooglePlugin extends AbstractPlugin {

	/**
	 *
	 * @return \Google\Analytics\Service\AnalyticsService
	 */
	public function analytics() {
		return $this->getServiceLocator()->get('Google\Analytics\Service\AnalyticsService');
	}

}

?>