<?php

namespace Google\Task\Plugin;

use SoloTasks\Service\AbstractTaskPlugin;

class GooglePlugin extends AbstractTaskPlugin {

	/**
	 *
	 * @return \Google\Analytics\Service\AnalyticsService
	 */
	public function analytics() {
		return $this->getServiceLocator()->get('Google\Analytics\Service\AnalyticsService');
	}

}

?>