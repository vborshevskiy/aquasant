<?php

namespace Google\Analytics\Task\Factory;

use Solo\ServiceManager\AbstractFactory;
use Google\Analytics\Task\TransactionsUpdater;

class TransactionsUpdaterFactory extends AbstractFactory {

	/**
	 *
	 * @see \Solo\ServiceManager\AbstractFactory::create()
	 */
	protected function create() {
		$commitsTable = $this->getServiceLocator()->get('Google\Analytics\Data\TransactionsCommitsTable');
		$task = new TransactionsUpdater($commitsTable);
		
		$task->isLockable(true);
		
		return $task;
	}

}

?>