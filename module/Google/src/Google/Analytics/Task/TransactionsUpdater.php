<?php

namespace Google\Analytics\Task;

use SoloTasks\Service\AbstractReplicationTask;
use Google\Analytics\Data\TransactionsCommitsTableInterface;
use Solo\DateTime\DateTime;

class TransactionsUpdater extends AbstractReplicationTask {

	/**
	 *
	 * @var TransactionsCommitsTableInterface
	 */
	protected $commitsTable;

	/**
	 *
	 * @param TransactionsCommitsTableInterface $commitsTable        	
	 */
	public function __construct(TransactionsCommitsTableInterface $commitsTable) {
		$this->commitsTable = $commitsTable;
	}

	/**
	 *
	 * @see \SoloTasks\Service\AbstractTask::getId()
	 */
	public function getId() {
		return $this->commitsTable->getTableGateway()->getTable();
	}

	/**
	 * 
	 * @see \SoloTasks\Service\TaskInterface::process()
	 */
	public function process() {
		$commits = $this->commitsTable->findAllUncommited($this->sites()->currentSite()->getId());
		if (0 < count($commits)) {
			$this->getLog()->info(sprintf('Found %d transactions to update', count($commits)));
			foreach ($commits as $commit) {
				$this->getLog()->info(sprintf('Reserve #%s', $commit->getReserveId()));
				if ($this->google()->analytics()->ecommerce()->sendExtendedTransaction($commit->getClientId(), $commit->getTransaction())) {
					$commit->setCommitedAt(DateTime::now());
					$this->google()->analytics()->ecommerce()->commits()->save($commit);
					$this->getLog()->info('Success');
				} else {
					$this->getLog()->info('Failed');
				}
			}
		} else {
			$this->getLog()->info('Nothing to update');
		}
	}

}

?>