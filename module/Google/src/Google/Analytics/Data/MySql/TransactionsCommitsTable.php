<?php

namespace Google\Analytics\Data\MySql;

use Solo\Db\Table\AbstractTable;
use Google\Analytics\Data\TransactionsCommitsTableInterface;

class TransactionsCommitsTable extends AbstractTable implements TransactionsCommitsTableInterface {

	/**
	 *
	 * @see \Google\Analytics\Data\TransactionsCommitsTableInterface::findAllUncommited()
	 */
	public function findAllUncommited($siteId) {
		$select = $this->createSelect();
		$select->where($this->where()->isNull('CommitedAt'));
		$select->where([
			'SiteID' => $siteId 
		]);
		return $this->selectWith($select);
	}

}

?>