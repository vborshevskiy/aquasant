<?php

namespace Google\Analytics\Data\MySql\Factory;

use Solo\ServiceManager\AbstractFactory;
use Solo\Db\TableGateway\TableGateway;
use Google\Analytics\Data\MySql\TransactionsCommitsTable;

class TransactionsCommitsTableFactory extends AbstractFactory {

	/**
	 *
	 * @see \Solo\ServiceManager\AbstractFactory::create()
	 */
	protected function create() {
		$tableGateway = new TableGateway('google_transactions_commits', 'ReserveID');
		$tableGateway->setResultSetPrototype($this->getServiceLocator()->get('Google\Analytics\Entity\TransactionCommit'));
		return new TransactionsCommitsTable($tableGateway);
	}

}

?>