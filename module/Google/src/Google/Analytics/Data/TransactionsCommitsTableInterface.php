<?php

namespace Google\Analytics\Data;

use Solo\Db\Table\TableInterface;
use Zend\Db\ResultSet\ResultSet;

interface TransactionsCommitsTableInterface extends TableInterface {

	/**
	 *
	 * @param integer $siteId        	
	 * @return ResultSet
	 */
	public function findAllUncommited($siteId);

}

?>