<?php

namespace Google\Analytics\Entity;

class TransactionItem {

	/**
	 *
	 * @var string
	 */
	private $transactionId;

	/**
	 *
	 * @var Transaction
	 */
	private $transaction;

	/**
	 *
	 * @var string
	 */
	private $name;

	/**
	 *
	 * @var string
	 */
	private $sku;

	/**
	 *
	 * @var string
	 */
	private $category;

	/**
	 *
	 * @var float
	 */
	private $price;

	/**
	 *
	 * @var integer
	 */
	private $quantity;

	/**
	 *
	 * @var string
	 */
	private $brand;

	/**
	 *
	 * @var string
	 */
	private $variant;

	/**
	 *
	 * @return string
	 */
	public function getTransactionId() {
		return $this->transactionId;
	}

	/**
	 *
	 * @param string $transactionId        	
	 * @throws \InvalidArgumentException
	 * @return \Google\Analytics\Entity\TransactionItem
	 */
	public function setTransactionId($transactionId) {
		$this->transactionId = $transactionId;
		return $this;
	}

	/**
	 *
	 * @return \Google\Analytics\Entity\Transaction
	 */
	public function getTransaction() {
		return $this->transaction;
	}

	/**
	 *
	 * @param \Google\Analytics\Entity\Transaction $transaction        	
	 */
	public function setTransaction(Transaction $transaction) {
		$this->transaction = $transaction;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 *
	 * @param string $name        	
	 * @return \Google\Analytics\Entity\TransactionItem
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getSku() {
		return $this->sku;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasSku() {
		return (null !== $this->sku);
	}

	/**
	 *
	 * @param string $sku        	
	 * @return \Google\Analytics\Entity\TransactionItem
	 */
	public function setSku($sku) {
		$this->sku = $sku;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getCategory() {
		return $this->category;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasCategory() {
		return (null !== $this->category);
	}

	/**
	 *
	 * @param string $category        	
	 * @return \Google\Analytics\Entity\TransactionItem
	 */
	public function setCategory($category) {
		$this->category = $category;
		return $this;
	}

	/**
	 *
	 * @return float
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasPrice() {
		return (null !== $this->price);
	}

	/**
	 *
	 * @param float $price        	
	 * @throws \InvalidArgumentException
	 * @return \Google\Analytics\Entity\TransactionItem
	 */
	public function setPrice($price) {
		if (!is_numeric($price)) {
			throw new \InvalidArgumentException('Price must be numeric');
		}
		$this->price = floatval($price);
		return $this;
	}

	/**
	 *
	 * @return integer
	 */
	public function getQuantity() {
		return $this->quantity;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasQuantity() {
		return (null !== $this->quantity);
	}

	/**
	 *
	 * @param integer $quantity        	
	 * @throws \InvalidArgumentException
	 * @return \Google\Analytics\Entity\TransactionItem
	 */
	public function setQuantity($quantity) {
		if (!is_numeric($quantity)) {
			throw new \InvalidArgumentException('Quantity must be numeric');
		}
		$this->quantity = intval($quantity);
		return $this;
	}

	/**
	 *
	 * @see \Solo\Stdlib\ArrayConvertible::toArray()
	 */
	public function toArray() {
		$out = [
			'id' => $this->getTransactionId(),
			'name' => $this->getName() 
		];
		if ($this->hasSku()) {
			$out['sku'] = $this->getSku();
		}
		if ($this->hasCategory()) {
			$out['category'] = $this->getCategory();
		}
		if ($this->hasPrice()) {
			$out['price'] = $this->getPrice();
		}
		if ($this->hasQuantity()) {
			$out['quantity'] = $this->getQuantity();
		}
		return $out;
	}

	/**
	 *
	 * @see \Solo\Stdlib\ArrayConvertible::fromArray()
	 */
	public function fromArray(array $data) {
		if (isset($data['id'])) $this->setTransactionId($data['id']);
		if (isset($data['name'])) $this->setName($data['name']);
		if (isset($data['sku'])) $this->setSku($data['sku']);
		if (isset($data['category'])) $this->setCategory($data['category']);
		if (isset($data['price'])) $this->setPrice($data['price']);
		if (isset($data['quantity'])) $this->setQuantity($data['quantity']);
	}

	/**
	 *
	 * @return string
	 */
	public function toJson() {
		return json_encode($this->toArray(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
	}

	/**
	 *
	 * @return string
	 */
	public function getBrand() {
		return $this->brand;
	}

	/**
	 *
	 * @param string $brand        	
	 */
	public function setBrand($brand) {
		$this->brand = $brand;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getVariant() {
		return $this->variant;
	}

	/**
	 *
	 * @param string $variant        	
	 */
	public function setVariant($variant) {
		$this->variant = $variant;
		return $this;
	}

}

?>