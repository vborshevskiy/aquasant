<?php

namespace Google\Analytics\Entity;

use Solo\Db\Entity\AbstractEntity;
use Solo\DateTime\DateTime;

class TransactionCommit extends AbstractEntity {
	
	/**
	 * @dbname ReserveID
	 *
	 * @var integer
	 */
	protected $reserveId;

	/**
	 * @dbname SiteID
	 *
	 * @var integer
	 */
	protected $siteId;

	/**
	 * @dbname ClientID
	 * 
	 * @var string
	 */
	protected $clientId;

	/**
	 *
	 * @var Transaction
	 */
	protected $transaction;

	/**
	 * @dbname CreatedAt
	 *
	 * @var DateTime
	 */
	protected $createdAt;

	/**
	 * @dbname CommitedAt
	 * @nullable
	 *
	 * @var DateTime
	 */
	protected $commitedAt;

	/**
	 *
	 * @param Transaction $transaction        	
	 * @return \Google\Analytics\Entity\TransactionCommit
	 */
	public function setTransaction(Transaction $transaction) {
		$this->transaction = $transaction;
		return $this;
	}

	/**
	 *
	 * @return \Google\Analytics\Entity\Transaction
	 */
	public function getTransaction() {
		return $this->transaction;
	}

	/**
	 *
	 * @see \Solo\Db\Entity\AbstractEntity::getValues()
	 */
	public function getValues() {
		$out = parent::getValues();
		$out['TransactionContent'] = json_encode($this->getTransaction()->toArray(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		return $out;
	}

	/**
	 *
	 * @see \Solo\Db\Entity\AbstractEntity::exchangeArray()
	 */
	public function exchangeArray($data) {
		parent::exchangeArray($data);
		if (isset($data['TransactionContent'])) {
			$data['TransactionContent'] = str_replace('\\\\', '\\', $data['TransactionContent']);
			$transaction = new Transaction();
			$transaction->fromArray(json_decode($data['TransactionContent'], true));
			$this->setTransaction($transaction);
		}
	}

}

?>