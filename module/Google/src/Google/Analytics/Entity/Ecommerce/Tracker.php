<?php

namespace Google\Analytics\Entity\Ecommerce;

class Tracker {

	/**
	 *
	 * @var string
	 */
	private $pageType;

	/**
	 *
	 * @var string
	 */
	private $clientId;

	/**
	 *
	 * @var string
	 */
	private $userId;

	/**
	 *
	 * @var boolean
	 */
	private $isAuth;

	/**
	 *
	 * @var string
	 *
	 */
	private $cityName;

	/**
	 *
	 * @var array
	 */
	private $attributes = [];

	/**
	 *
	 * @var array
	 */
	private $impressions = [];

	/**
	 *
	 * @var array
	 */
	private $basketProducts = [];

	/**
	 *
	 * @var TrackerOrderInfo
	 */
	private $orderInfo;

	/**
	 *
	 * @var array
	 */
	private $orderProducts = [];

	/**
	 *
	 * @var array
	 */
	private $promoActions = [];

	/**
	 *
	 * @return string
	 */
	public function getPageType() {
		return $this->pageType;
	}

	/**
	 *
	 * @param string $pageType        	
	 */
	public function setPageType($pageType) {
		$this->pageType = $pageType;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getClientId() {
		return $this->clientId;
	}

	/**
	 *
	 * @param string $clientId        	
	 */
	public function setClientId($clientId) {
		$this->clientId = $clientId;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getUserId() {
		return $this->userId;
	}

	/**
	 *
	 * @param string $userId        	
	 */
	public function setUserId($userId) {
		$this->userId = $userId;
		return $this;
	}

	/**
	 *
	 * @param boolean $isAuth        	
	 * @throws \InvalidArgumentException
	 * @return boolean | \Google\Analytics\Entity\Ecommerce\Tracker
	 */
	public function isAuth($isAuth = null) {
		if (null === $isAuth) {
			return $this->isAuth;
		} else {
			if (!is_bool($isAuth)) {
				throw new \InvalidArgumentException('Is auth must be boolean');
			}
			$this->isAuth = $isAuth;
			return $this;
		}
	}

	/**
	 *
	 * @return string
	 */
	public function getCityName() {
		return $this->cityName;
	}

	/**
	 *
	 * @param string $cityName        	
	 */
	public function setCityName($cityName) {
		$this->cityName = $cityName;
		return $this;
	}

	/**
	 *
	 * @param TrackerImpression $impression        	
	 */
	public function addImpression(TrackerImpression $impression) {
		$this->impressions[] = $impression;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasImpressions() {
		return (0 < count($this->impressions));
	}

	/**
	 *
	 * @return array
	 */
	public function getImpressions() {
		return $this->impressions;
	}

	/**
	 *
	 * @param string $name        	
	 * @param string $value        	
	 */
	public function addAttribute($name, $value) {
		$this->attributes[$name] = $value;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasAttributes() {
		return (0 < count($this->attributes));
	}

	/**
	 *
	 * @return array
	 */
	public function getAttributes() {
		return $this->attributes;
	}

	/**
	 *
	 * @param string $name        	
	 * @return mixed
	 */
	public function getAttribute($name) {
		$value = null;
		if (array_key_exists($name, $this->attributes)) {
			$value = $this->attributes[$name];
		}
		return $value;
	}

	/**
	 *
	 * @param TrackerBasketProduct $product        	
	 */
	public function addBasketProduct(TrackerBasketProduct $product) {
		$this->basketProducts[] = $product;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasBasketProducts() {
		return (0 < count($this->basketProducts));
	}

	/**
	 *
	 * @return array
	 */
	public function getBasketProducts() {
		return $this->basketProducts;
	}

	/**
	 *
	 * @param TrackerOrderProduct $product        	
	 */
	public function addOrderProduct(TrackerOrderProduct $product) {
		if (!$product->getPosition()) {
			$product->setPosition(count($this->orderProducts) + 1);
		}
		$this->orderProducts[] = $product;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasOrderProducts() {
		return (0 < count($this->orderProducts));
	}

	/**
	 *
	 * @return array
	 */
	public function getOrderProducts() {
		return $this->orderProducts;
	}

	/**
	 *
	 * @param TrackerPromoAction $promoAction        	
	 */
	public function addPromoAction(TrackerPromoAction $promoAction) {
		if (!$promoAction->getPosition()) {
			$promoAction->setPosition(count($this->promoActions) + 1);
		}
		$this->promoActions[] = $promoAction;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasPromoActions() {
		return (0 < count($this->promoActions));
	}

	/**
	 *
	 * @return array
	 */
	public function getPromoActions() {
		return $this->promoActions;
	}

	/**
	 *
	 * @return \Google\Analytics\Entity\Ecommerce\TrackerOrderInfo
	 */
	public function orderInfo() {
		if (null === $this->orderInfo) {
			$this->orderInfo = new TrackerOrderInfo();
		}
		return $this->orderInfo;
	}

	public function toJson() {
		$out = [
			'ecommerce' => [] 
		];
		if (false && $this->hasImpressions()) {
			$out['ecommerce']['impressions'] = [];
			foreach ($this->getImpressions() as $impression) {
				if (15 > count($out['ecommerce']['impressions'])) {
					$out['ecommerce']['impressions'][] = [
						'id' => $impression->getId(),
						'name' => $impression->getName(),
						'category' => $impression->getCategoryName(),
						'price' => $impression->getPrice(),
						'position' => $impression->getPosition(),
						'list' => $impression->getListName() 
					];
				}
			}
		}
		switch ($this->getPageType()) {
			case 'product':
				$out['ecommerce']['detail'] = [
					'actionField' => [
						'list' => 'ProductPage' 
					],
					'products' => [
						[
							'id' => $this->getAttribute('product-id'),
							'name' => $this->getAttribute('product-name'),
							'category' => $this->getAttribute('product-category'),
							'brand' => $this->getAttribute('product-brand'),
							'price' => $this->getAttribute('product-price') 
						] 
					] 
				];
				break;
			case 'basket':
				$out['ecommerce']['checkout'] = [
					'actionField' => ['step' => 1],
					'products' => [] 
				];
				foreach ($this->getBasketProducts() as $product) {
					$out['ecommerce']['checkout']['products'][] = [
						'id' => $product->getId(),
						'name' => $product->getName(),
						'category' => $product->getCategoryName(),
						'price' => $product->getPrice(),
						'quantity' => $product->getQuantity() 
					];
				}
				break;
		}
		return json_encode($out, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
	}

}

?>