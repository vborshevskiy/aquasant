<?php

namespace Google\Analytics\Entity\Ecommerce;

class TrackerPromoAction {

	/**
	 *
	 * @var string
	 */
	private $id;

	/**
	 *
	 * @var string
	 */
	private $name;

	/**
	 *
	 * @var string
	 */
	private $creative;

	/**
	 *
	 * @var integer
	 */
	private $position = 0;

	/**
	 *
	 * @return string
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param string $id        	
	 */
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 *
	 * @param string $name        	
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getCreative() {
		return $this->creative;
	}

	/**
	 *
	 * @param string $creative        	
	 */
	public function setCreative($creative) {
		$this->creative = $creative;
		return $this;
	}

	/**
	 *
	 * @return number
	 */
	public function getPosition() {
		return $this->position;
	}

	/**
	 *
	 * @param number $position        	
	 */
	public function setPosition($position) {
		$this->position = $position;
		return $this;
	}

}

?>