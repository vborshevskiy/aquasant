<?php

namespace Google\Analytics\Entity\Ecommerce;

class TrackerImpression {

	/**
	 *
	 * @var string
	 */
	private $id;

	/**
	 *
	 * @var string
	 */
	private $name;

	/**
	 *
	 * @var string
	 */
	private $categoryName;

	/**
	 *
	 * @var float
	 */
	private $price;

	/**
	 *
	 * @var integer
	 */
	private $position;

	/**
	 *
	 * @var string
	 */
	private $listName;

	/**
	 *
	 * @var string
	 */
	private $brandName;

	/**
	 *
	 * @return string
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param string $id        	
	 */
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 *
	 * @param string $name        	
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getCategoryName() {
		return $this->categoryName;
	}

	/**
	 *
	 * @param string $categoryName        	
	 */
	public function setCategoryName($categoryName) {
		$this->categoryName = $categoryName;
		return $this;
	}

	/**
	 *
	 * @return number
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 *
	 * @param number $price        	
	 */
	public function setPrice($price) {
		$this->price = $price;
		return $this;
	}

	/**
	 *
	 * @return number
	 */
	public function getPosition() {
		return $this->position;
	}

	/**
	 *
	 * @param number $position        	
	 */
	public function setPosition($position) {
		$this->position = $position;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getListName() {
		return $this->listName;
	}

	/**
	 *
	 * @param string $listName        	
	 */
	public function setListName($listName) {
		$this->listName = $listName;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getBrandName() {
		return $this->brandName;
	}

	/**
	 *
	 * @param string $brandName        	
	 */
	public function setBrandName($brandName) {
		$this->brandName = $brandName;
		return $this;
	}

}

?>