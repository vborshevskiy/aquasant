<?php

namespace Google\Analytics\Entity\Ecommerce;

class TrackerBasketProduct extends TrackerImpression {

	/**
	 *
	 * @var integer
	 */
	private $quantity;

	/**
	 *
	 * @return number
	 */
	public function getQuantity() {
		return $this->quantity;
	}

	/**
	 *
	 * @param number $quantity        	
	 */
	public function setQuantity($quantity) {
		$this->quantity = $quantity;
		return $this;
	}

}

?>