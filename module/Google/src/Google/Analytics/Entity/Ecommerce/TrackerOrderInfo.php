<?php

namespace Google\Analytics\Entity\Ecommerce;

class TrackerOrderInfo {

	/**
	 *
	 * @var string
	 */
	private $id;

	/**
	 *
	 * @var float
	 */
	private $revenue = 0.00;

	/**
	 *
	 * @var float
	 */
	private $tax = 0.00;

	/**
	 *
	 * @var float
	 */
	private $shipping = 0.00;

	/**
	 *
	 * @var string
	 */
	private $coupon = '';

	/**
	 *
	 * @var string
	 */
	private $purchaseType;

	/**
	 *
	 * @return string
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param string $id        	
	 */
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	/**
	 *
	 * @return number
	 */
	public function getRevenue() {
		return $this->revenue;
	}

	/**
	 *
	 * @param number $revenue        	
	 */
	public function setRevenue($revenue) {
		$this->revenue = $revenue;
		return $this;
	}

	/**
	 *
	 * @return number
	 */
	public function getTax() {
		return $this->tax;
	}

	/**
	 *
	 * @param number $tax        	
	 */
	public function setTax($tax) {
		$this->tax = $tax;
		return $this;
	}

	/**
	 *
	 * @return number
	 */
	public function getShipping() {
		return $this->shipping;
	}

	/**
	 *
	 * @param number $shipping        	
	 */
	public function setShipping($shipping) {
		$this->shipping = $shipping;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getCoupon() {
		return $this->coupon;
	}

	/**
	 *
	 * @param string $coupon        	
	 */
	public function setCoupon($coupon) {
		$this->coupon = $coupon;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getPurchaseType() {
		return $this->purchaseType;
	}

	/**
	 *
	 * @param string $purchaseType        	
	 */
	public function setPurchaseType($purchaseType) {
		$this->purchaseType = $purchaseType;
		return $this;
	}

}

?>