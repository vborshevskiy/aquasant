<?php

namespace Google\Analytics\Entity;

use Solo\Stdlib\ArrayConvertible;

class CustomDimension implements ArrayConvertible {

	/**
	 *
	 * @var integer
	 */
	private $position;

	/**
	 *
	 * @var mixed
	 */
	private $value;

	/**
	 *
	 * @param integer $position        	
	 * @param mixed $value        	
	 */
	public function __construct($position = null, $value = null) {
		if (null !== $position) {
			$this->setPosition($position);
		}
		if (null !== $value) {
			$this->setValue($value);
		}
	}

	/**
	 *
	 * @return integer
	 */
	public function getPosition() {
		return $this->position;
	}

	/**
	 *
	 * @param integer $position        	
	 * @throws \InvalidArgumentException
	 * @return \Google\Analytics\Entity\CustomDimension
	 */
	public function setPosition($position) {
		if (!is_numeric($position)) {
			throw new \InvalidArgumentException('Position must be numeric');
		}
		$position = intval($position);
		if (0 >= $position) {
			throw new \InvalidArgumentException('Position must be greater than zero');
		}
		if (200 < $position) {
			throw new \InvalidArgumentException('Position must be less than 200');
		}
		$this->position = $position;
		return $this;
	}

	/**
	 *
	 * @return \Google\Analytics\Entity\mixed
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 *
	 * @param mixed $value        	
	 * @return \Google\Analytics\Entity\CustomDimension
	 */
	public function setValue($value) {
		$value = strval($value);
		if (150 < mb_strlen($value)) {
			throw new \InvalidArgumentException('Value must be less than 150 chars');
		}
		$this->value = $value;
		return $this;
	}

	/**
	 *
	 * @see \Solo\Stdlib\ArrayConvertible::toArray()
	 */
	public function toArray() {
		return [
			'position' => $this->getPosition(),
			'value' => $this->getValue() 
		];
	}

	/**
	 *
	 * @see \Solo\Stdlib\ArrayConvertible::fromArray()
	 */
	public function fromArray(array $data) {
		if (isset($data['position'])) $this->setPosition($data['position']);
		if (isset($data['value'])) $this->setValue($data['value']);
	}

}

?>