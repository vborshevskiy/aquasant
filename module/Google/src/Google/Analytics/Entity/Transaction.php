<?php

namespace Google\Analytics\Entity;

class Transaction {

	/**
	 *
	 * @var string
	 */
	private $id;

	/**
	 *
	 * @var string
	 */
	private $affiliation;

	/**
	 *
	 * @var float
	 */
	private $revenue;

	/**
	 *
	 * @var float
	 */
	private $shipping;

	/**
	 *
	 * @var array
	 */
	private $customDimensions = [];

	/**
	 *
	 * @var float
	 */
	private $tax;

	/**
	 *
	 * @var string
	 */
	private $uid;

	/**
	 *
	 * @var string
	 */
	private $promocode;

	/**
	 *
	 * @var multitype:TransactionItem
	 */
	private $items = [];

	/**
	 *
	 * @return string
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param string $id        	
	 * @throws \InvalidArgumentException
	 * @return \Google\Analytics\Entity\Transaction
	 */
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getAffiliation() {
		return $this->affiliation;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasAffiliation() {
		return (null !== $this->affiliation);
	}

	/**
	 *
	 * @param string $affiliation        	
	 * @return \Google\Analytics\Entity\Transaction
	 */
	public function setAffiliation($affiliation) {
		$this->affiliation = $affiliation;
		return $this;
	}

	/**
	 *
	 * @return float
	 */
	public function getRevenue() {
		return $this->revenue;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasRevenue() {
		return (null !== $this->revenue);
	}

	/**
	 *
	 * @param float $revenue        	
	 * @throws \InvalidArgumentException
	 * @return \Google\Analytics\Entity\Transaction
	 */
	public function setRevenue($revenue) {
		if (!is_numeric($revenue)) {
			throw new \InvalidArgumentException('Revenue must be numeric');
		}
		$this->revenue = floatval($revenue);
		return $this;
	}

	/**
	 *
	 * @return float
	 */
	public function getShipping() {
		return $this->shipping;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasShipping() {
		return (null !== $this->shipping);
	}

	/**
	 *
	 * @param float $shipping        	
	 * @throws \InvalidArgumentException
	 * @return \Google\Analytics\Entity\Transaction
	 */
	public function setShipping($shipping) {
		if (!is_numeric($shipping)) {
			throw new \InvalidArgumentException('Shipping must be numeric');
		}
		$this->shipping = floatval($shipping);
		return $this;
	}

	/**
	 *
	 * @return float
	 */
	public function getTax() {
		return $this->tax;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasTax() {
		return (null !== $this->tax);
	}

	/**
	 *
	 * @param float $tax        	
	 * @throws \InvalidArgumentException
	 * @return \Google\Analytics\Entity\Transaction
	 */
	public function setTax($tax) {
		if (!is_numeric($tax)) {
			throw new \InvalidArgumentException('Tax must be numeric');
		}
		$this->tax = floatval($tax);
		return $this;
	}

	/**
	 *
	 * @param TransactionItem $item        	
	 * @return TransactionItem
	 */
	public function addItem(TransactionItem $item) {
		$this->items[] = $item;
		$item->setTransaction($this);
		return $item;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasItems() {
		return (0 < count($this->items));
	}

	/**
	 *
	 * @return multitype:TransactionItem
	 */
	public function getItems() {
		return $this->items;
	}

	/**
	 *
	 * @param CustomDimension $dimension        	
	 * @return CustomDimension
	 */
	public function setCustomDimension(CustomDimension $dimension) {
		$this->customDimensions[$dimension->getPosition()] = $dimension;
		return $dimension;
	}

	/**
	 *
	 * @param integer $position        	
	 * @return CustomDimension
	 */
	public function getCustomDimension($position) {
		if (array_key_exists($position, $this->customDimensions)) {
			return $this->customDimensions[$position];
		}
		return null;
	}

	/**
	 *
	 * @return array
	 */
	public function getCustomDimensions() {
		return $this->customDimensions;
	}

	/**
	 *
	 * @return array
	 */
	public function toArray() {
		$out = [
			'id' => $this->getId() 
		];
		if ($this->hasAffiliation()) {
			$out['affiliation'] = $this->getAffiliation();
		}
		if ($this->hasRevenue()) {
			$out['revenue'] = $this->getRevenue();
		}
		if ($this->hasShipping()) {
			$out['shipping'] = $this->getShipping();
		}
		if ($this->hasTax()) {
			$out['tax'] = $this->getTax();
		}
		if ($this->getUid()) {
			$out['uid'] = $this->getUid();
		}
		if ($this->getPromocode()) {
			$out['promocode'] = $this->getPromocode();
		}
		
		$out['items'] = [];
		foreach ($this->getItems() as $item) {
			$out['items'][] = $item->toArray();
		}
		
		$out['customDimensions'] = [];
		foreach ($this->getCustomDimensions() as $dimension) {
			$out['customDimensions'][] = $dimension->toArray();
		}
		
		return $out;
	}

	/**
	 *
	 * @param array $data        	
	 */
	public function fromArray(array $data) {
		if (isset($data['id'])) $this->setId($data['id']);
		if (isset($data['affiliation'])) $this->setAffiliation($data['affiliation']);
		if (isset($data['revenue'])) $this->setRevenue($data['revenue']);
		if (isset($data['tax'])) $this->setTax($data['tax']);
		if (isset($data['uid'])) $this->setUid($data['uid']);
		if (isset($data['promocode'])) $this->setPromocode($data['promocode']);
		if (isset($data['items']) && is_array($data['items'])) {
			foreach ($data['items'] as $item) {
				$obj = new TransactionItem();
				$obj->fromArray($item);
				$this->addItem($obj);
			}
		}
		if (isset($data['customDimensions']) && is_array($data['customDimensions'])) {
			foreach ($data['customDimensions'] as $customDimension) {
				$obj = new CustomDimension();
				$obj->fromArray($customDimension);
				$this->setCustomDimension($obj);
			}
		}
	}

	/**
	 *
	 * @return string
	 */
	public function toJson() {
		return json_encode($this->toArray(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
	}

	/**
	 *
	 * @return string
	 */
	public function getUid() {
		return $this->uid;
	}

	/**
	 *
	 * @param string $uid        	
	 */
	public function setUid($uid) {
		$this->uid = $uid;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getPromocode() {
		return $this->promocode;
	}

	/**
	 *
	 * @param string $promocode        	
	 */
	public function setPromocode($promocode) {
		$this->promocode = $promocode;
		return $this;
	}

}

?>