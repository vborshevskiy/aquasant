<?php

namespace Google\Analytics\Controller\Plugin;

use Solo\Mvc\Controller\Plugin\AbstractControllerPlugin;

class AnalyticsPlugin extends AbstractControllerPlugin {

	/**
	 *
	 * @return \Google\Analytics\Service\Factory\EcommerceService
	 */
	public function ecommerce() {
		return $this->getServiceLocator()->get('Google\Analytics\Service\EcommerceService');
	}

}

?>