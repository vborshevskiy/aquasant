<?php

namespace Google\Analytics\Service\Factory;

use Solo\ServiceManager\AbstractServiceFactory;
use Google\Analytics\Service\EcommerceService;

class EcommerceServiceFactory extends AbstractServiceFactory {

	/**
	 *
	 * @see \Solo\ServiceManager\AbstractFactory::create()
	 */
	protected function create() {
		$service = new EcommerceService();
		
		// config
		if ($this->hasConfig('solo-google.analytics')) {
			$config = $this->getConfig('solo-google.analytics');
			if (isset($config['account'])) {
				$service->setAccountNumber($config['account']);
			} else {
				throw new \RuntimeException('Not set account parameter');
			}
		}
		// END config
		
		return $service;
	}

}

?>