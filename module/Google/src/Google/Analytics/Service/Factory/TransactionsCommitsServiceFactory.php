<?php

namespace Google\Analytics\Service\Factory;

use Solo\ServiceManager\AbstractServiceFactory;
use Google\Analytics\Service\TransactionsCommitsService;

class TransactionsCommitsServiceFactory extends AbstractServiceFactory {

	/**
	 *
	 * @see \Solo\ServiceManager\AbstractFactory::create()
	 */
	protected function create() {
		$commitsTable = $this->getServiceLocator()->get('Google\Analytics\Data\TransactionsCommitsTable');
		return new TransactionsCommitsService($commitsTable);
	}

}

?>