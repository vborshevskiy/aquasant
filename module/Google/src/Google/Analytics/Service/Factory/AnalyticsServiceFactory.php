<?php

namespace Google\Analytics\Service\Factory;

use Solo\ServiceManager\AbstractServiceFactory;
use Google\Analytics\Service\AnalyticsService;

class AnalyticsServiceFactory extends AbstractServiceFactory {

	/**
	 *
	 * @see \Solo\ServiceManager\AbstractFactory::create()
	 */
	protected function create() {
		$service = new AnalyticsService();
		
		if ($this->hasConfig('solo-google.analytics')) {
			$config = $this->getConfig('solo-google.analytics');
			foreach ($config as $name => $value) {
				$service->setOption($name, $value);
			}
		}
		
		return $service;
	}

}

?>