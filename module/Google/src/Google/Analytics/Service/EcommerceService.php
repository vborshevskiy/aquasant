<?php

namespace Google\Analytics\Service;

use Solo\ServiceManager\AbstractService;
use Zend\Session\Container;
use Google\Analytics\Entity\Transaction;
use Zend\Http\Client as HttpClient;
use Zend\Http\Request;
use Zend\Http\Response;

class EcommerceService extends AbstractService {

	private $accountNumber = '';

	/**
	 *
	 * @return \Google\Analytics\Service\TransactionsCommitsService
	 */
	public function commits() {
		return $this->getServiceLocator()->get('Google\Analytics\Service\TransactionsCommitsService');
	}

	/**
	 *
	 * @param string $accountNumber        	
	 * @return \Google\Analytics\Service\EcommerceService
	 */
	public function setAccountNumber($accountNumber) {
		$this->accountNumber = $accountNumber;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getAccountNumber() {
		return $this->accountNumber;
	}

	/**
	 *
	 * @return string
	 */
	public function jsOutput(array $transactions) {
		$container = new Container('google');
		
		$lines = [];
		
		if (0 < count($transactions)) {
			$lines[] = "ga('require', 'ecommerce', 'ecommerce.js');";
			
			foreach ($transactions as $transaction) {
				$key = sprintf('tr_%d', $transaction->getId());
				$isTransactionShown = (isset($container[$key]) && (1 == $container[$key]));
				
				if (!$isTransactionShown) {
					$lines[] = "ga('ecommerce:addTransaction', " . $transaction->toJson() . ");";
					foreach ($transaction->getItems() as $item) {
						$lines[] = "ga('ecommerce:addItem', " . $item->toJson() . ");";
					}
					
					$container[$key] = 1;
				}
			}
			
			$lines[] = "ga('ecommerce:send');";
			$lines[] = '';
		}
		
		return implode("\n", $lines);
	}

	/**
	 *
	 * @param string $clientId        	
	 * @param Transaction $transaction        	
	 */
	public function sendTransaction($clientId, Transaction $transaction) {
		$client = new HttpClient();
		$client->setEncType($client::ENC_URLENCODED);
		
		// transaction
		$request = new Request();
		$request->setMethod(Request::METHOD_POST);
		$request->setUri('http://www.google-analytics.com/collect');
		$request->getPost()->set('v', 1);
		$request->getPost()->set('tid', $this->getAccountNumber());
		$request->getPost()->set('cid', $clientId);
		$request->getPost()->set('t', 'transaction');
		$request->getPost()->set('ti', $transaction->getId());
		if ($transaction->hasAffiliation()) {
			$request->getPost()->set('ta', $transaction->getAffiliation());
		}
		$request->getPost()->set('tr', $transaction->getRevenue());
		if ($transaction->hasShipping() && (0 < $transaction->getShipping())) {
			$request->getPost()->set('ts', $transaction->getShipping());
		}
		if ($transaction->hasTax() && (0 < $transaction->getTax())) {
			$request->getPost()->set('tt', $transaction->getTax());
		}
		foreach ($transaction->getCustomDimensions() as $customDimension) {
			$request->getPost()->set(sprintf('cd%d', $customDimension->getPosition()), $customDimension->getValue());
		}
		
		$response = $client->send($request);
		if (Response::STATUS_CODE_200 !== $response->getStatusCode()) {
			return false;
		}
		// END transaction
		
		// items
		foreach ($transaction->getItems() as $item) {
			$request = new Request();
			$request->setMethod(Request::METHOD_POST);
			$request->setUri('http://www.google-analytics.com/collect');
			$request->getPost()->set('v', 1);
			$request->getPost()->set('tid', $this->getAccountNumber());
			$request->getPost()->set('cid', $clientId);
			$request->getPost()->set('t', 'item');
			$request->getPost()->set('ti', $transaction->getId());
			$request->getPost()->set('in', $item->getName());
			$request->getPost()->set('ip', $item->getPrice());
			$request->getPost()->set('iq', $item->getQuantity());
			$request->getPost()->set('ic', $item->getSku());
			$request->getPost()->set('iv', $item->getCategory());
			
			$response = $client->send($request);
			if (Response::STATUS_CODE_200 !== $response->getStatusCode()) {
				return false;
			}
		}
		// END items
		
		return true;
	}

	public function sendExtendedTransaction($clientId, Transaction $transaction) {
		$client = new HttpClient();
		$client->setEncType($client::ENC_URLENCODED);
		
		// transaction
		$request = new Request();
		$request->setMethod(Request::METHOD_POST);
		$request->setUri('http://www.google-analytics.com/collect');
		$request->getPost()->set('v', 1);
		$request->getPost()->set('t', 'pageview');
		$request->getPost()->set('tid', 'UA-122145320-1');
		$request->getPost()->set('cid', $clientId);
		$request->getPost()->set('dh', 'santehbaza.ru');
		$request->getPost()->set('dp', '/virtual-page/order/');
		$request->getPost()->set('ti', $transaction->getId());
		if ($transaction->hasAffiliation()) {
			$request->getPost()->set('ta', $transaction->getAffiliation());
		}
		$request->getPost()->set('tr', $transaction->getRevenue());
		if ($transaction->hasShipping() && (0 < $transaction->getShipping())) {
			$request->getPost()->set('ts', $transaction->getShipping());
		}
		if ($transaction->hasTax() && (0 < $transaction->getTax())) {
			$request->getPost()->set('tt', $transaction->getTax());
		}
		if ($transaction->getPromocode()) {
			$request->getPost()->set('tcc', $transaction->getPromocode());
		}
		$request->getPost()->set('pa', 'purchase');
		foreach ($transaction->getCustomDimensions() as $customDimension) {
			$request->getPost()->set(sprintf('cd%d', $customDimension->getPosition()), $customDimension->getValue());
		}
		
		// items
		$position = 1;
		foreach ($transaction->getItems() as $item) {
			$request->getPost()->set(sprintf('pr1id', $position), $item->getSku());
			$request->getPost()->set(sprintf('pr1nm', $position), $item->getName());
			$request->getPost()->set(sprintf('pr1ca', $position), $item->getCategory());
			$request->getPost()->set(sprintf('pr1pr', $position), $item->getPrice());
			$request->getPost()->set(sprintf('pr1qt', $position), $item->getQuantity());
			$request->getPost()->set(sprintf('pr1br', $position), $item->getBrand());
			$request->getPost()->set(sprintf('pr1va', $position), $item->getVariant());
			
			$position++;
		}
		// END items
		
		$response = $client->send($request);
		mail('rgoryanin@it-solo.com', 'santehbaza.ru: ga #1', print_r($request, true) . '<br />Response: ' . $response->getStatusCode());
		if (Response::STATUS_CODE_200 !== $response->getStatusCode()) {
			return false;
		}
		// END transaction
		
		return true;
	}

	/**
	 * 
	 * @param string $clientId
	 * @param string $clientCode
	 * @return boolean
	 */
	public function sendCallOrder($clientId, $clientCode) {
		$client = new HttpClient();
		$client->setEncType($client::ENC_URLENCODED);
		
		// transaction
		$request = new Request();
		$request->setMethod(Request::METHOD_POST);
		$request->setUri('http://www.google-analytics.com/collect');
		$request->getPost()->set('v', 1);
		$request->getPost()->set('t', 'event');
		$request->getPost()->set('tid', 'UA-122145320-1');
		$request->getPost()->set('cid', $clientId);
		$request->getPost()->set('ec', 'Ecommerce');
		$request->getPost()->set('el', 'CallOrder');
		$request->getPost()->set('ea', $clientCode);
		
		$response = $client->send($request);
		mail('rgoryanin@it-solo.com', 'santehbaza.ru: ga #2', print_r($request, true) . '<br />Response: ' . $response->getStatusCode());
		if (Response::STATUS_CODE_200 !== $response->getStatusCode()) {
			return false;
		}
		// END transaction
		
		return true;
	}

	/**
	 *
	 * @param Request $request        	
	 */
	private function debugTransactionRequest(Request $request) {
		$debugRequest = clone $request;
		$debugRequest->setUri('https://www.google-analytics.com/debug/collect');
		
		$client = new HttpClient();
		$client->setEncType($client::ENC_URLENCODED);
		$client->setOptions([
			'sslverifypeer' => false 
		]);
		
		$response = $client->send($debugRequest);
		print_r($response->getBody());
	}

}

?>