<?php

namespace Google\Analytics\Service;

use Solo\ServiceManager\AbstractService;
use Solo\Cookie\Cookie;
use Solo\ServiceManager\ProvidesOptions;

class AnalyticsService extends AbstractService {
	
	use ProvidesOptions;

	/**
	 *
	 * @return \Google\Analytics\Service\EcommerceService
	 */
	public function ecommerce() {
		return $this->getServiceLocator()->get('Google\Analytics\Service\EcommerceService');
	}

	/**
	 *
	 * @return string
	 */
	public function getClientId() {
		if (Cookie::exists('_gaClientId') && !empty(Cookie::get('_gaClientId'))) {
			return Cookie::get('_gaClientId');
		} elseif (Cookie::exists('_ga') && !empty(Cookie::get('_ga'))) {
			$ga = Cookie::get('_ga');
			if ($ga) {
				$gaParts = explode('.', $ga);
				if (2 < count($gaParts)) {
					$idParts = [
						$gaParts[count($gaParts) - 2],
						$gaParts[count($gaParts) - 1] 
					];
					return implode('.', $idParts);
				}
			}
		} elseif ($this->hasOption('defaultClientId')) {
			return $this->getOption('defaultClientId');
		} else {
			return '';
		}
	}

}

?>