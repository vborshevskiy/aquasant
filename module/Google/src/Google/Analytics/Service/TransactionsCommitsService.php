<?php

namespace Google\Analytics\Service;

use Solo\ServiceManager\AbstractService;
use Google\Analytics\Data\TransactionsCommitsTableInterface;
use Google\Analytics\Entity\TransactionCommit;
use Solo\Db\TableGateway\TableGateway;
use Solo\Stdlib\IteratorByProperty;

class TransactionsCommitsService extends AbstractService {

	/**
	 *
	 * @var TransactionsCommitsTableInterface
	 */
	protected $commitsTable;

	/**
	 *
	 * @param TransactionsCommitsTableInterface $commitsTable        	
	 */
	public function __construct(TransactionsCommitsTableInterface $commitsTable) {
		$this->commitsTable = $commitsTable;
	}

	/**
	 *
	 * @param TransactionCommit $transactionCommit        	
	 */
	public function save(TransactionCommit $transactionCommit) {
		$this->commitsTable->insert($transactionCommit, TableGateway::INSERT_UPDATE_ON_DUPLICATE);
	}

	/**
	 *
	 * @param integer $reserveId        	
	 * @return TransactionCommit
	 */
	public function findByReserveId($reserveId) {
		return $this->commitsTable->findPrimary($reserveId);
	}

	/**
	 *
	 * @param integer $siteId        	
	 * @return \Solo\Stdlib\IteratorByProperty
	 */
	public function findAllUncommited($siteId) {
		$commits = $this->commitsTable->findAllUncommited($siteId);
		return new IteratorByProperty($commits, 'reserveId');
	}

}

?>