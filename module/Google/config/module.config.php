<?php
return [
	'controller_plugins' => [
		'invokables' => [
			'google' => 'Google\Controller\Plugin\GooglePlugin' 
		] 
	],
	'service_manager' => [
		'factories' => [
			'Google\Analytics\Service\AnalyticsService' => 'Google\Analytics\Service\Factory\AnalyticsServiceFactory',
			'Google\Analytics\Service\EcommerceService' => 'Google\Analytics\Service\Factory\EcommerceServiceFactory',
			'Google\Analytics\Service\TransactionsCommitsService' => 'Google\Analytics\Service\Factory\TransactionsCommitsServiceFactory',
			
			'Google\Analytics\Data\TransactionsCommitsTable' => 'Google\Analytics\Data\MySql\Factory\TransactionsCommitsTableFactory' 
		],
		'invokables' => [
			'Google\Analytics\Entity\TransactionCommit' => 'Google\Analytics\Entity\TransactionCommit' 
		],
		'shared' => [
			'Google\Analytics\Entity\TransactionCommit' => false 
		] 
	]
];
