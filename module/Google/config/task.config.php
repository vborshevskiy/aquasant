<?php
return [
	'tasks' => [
		'ga_transactions_update' => [
			'class' => 'Google\Analytics\Task\Factory\TransactionsUpdaterFactory' 
		] 
	],
	'task_plugins' => [
		'invokables' => [
			'google' => 'Google\Task\Plugin\GooglePlugin' 
		] 
	] 
];