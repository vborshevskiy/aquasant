<?php

use Solo\Db\TableGateway\TableGateway;
use SoloSettings\Data\TriggersTable;
use SoloSettings\Data\ConstantsTable;
use SoloSettings\Data\ReplicatorsLastUpdatesTable;

return [
    'controller_plugins' => [
        'invokables' => [
            'constants' => 'SoloSettings\Controller\Plugin\Constants',
            'const' => 'SoloSettings\Controller\Plugin\Constants',
            'triggers' => 'SoloSettings\Controller\Plugin\Triggers',
            'replicatorsLastUpdates' => 'SoloSettings\Controller\Plugin\ReplicatorsLastUpdates',
        ]
    ],
    'service_manager' => [
        'aliases' => [
            'triggers' => 'SoloSettings\Service\TriggersService',
            'constants' => 'SoloSettings\Service\ConstantsService',
            'replicatorsLastUpdates' => 'SoloSettings\Service\ReplicatorsLastUpdatesService',
        ],
        'factories' => [
            'SoloSettings\Data\TriggersTable' => function ($sm) {
                $gateway = new TableGateway('triggers', [
                    'TriggerName',
                    'State'
                ]);
                $table = new TriggersTable($gateway);
                return $table;
            },
            'SoloSettings\Data\ConstantsTable' => function ($sm) {
                $gateway = new TableGateway('constants', [
                    'ConstantName',
                    'SiteId'
                ]);
                $table = new ConstantsTable($gateway);
                return $table;
            },
            'SoloSettings\Data\ReplicatorsLastUpdatesTable' => function ($sm) {
                $gateway = new TableGateway('replicators_last_updates', [
                    'ReplicatorName',
                    'WebMethodName',
                ]);
                $table = new ReplicatorsLastUpdatesTable($gateway);
                return $table;
            },
            'SoloSettings\Service\TriggersService' => 'SoloSettings\Service\TriggersServiceFactory',
            'SoloSettings\Service\ConstantsService' => 'SoloSettings\Service\ConstantsServiceFactory',
            'SoloSettings\Service\ReplicatorsLastUpdatesService' => 'SoloSettings\Service\ReplicatorsLastUpdatesServiceFactory',
        ]
    ]
];
