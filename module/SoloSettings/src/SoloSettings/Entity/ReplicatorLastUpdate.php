<?php

namespace SoloSettings\Entity;

class ReplicatorLastUpdate {

	/**
	 *
	 * @var string
	 */
	private $replicatorName;

	/**
	 *
	 * @var string
	 */
	private $webMethodName;
    
    /**
	 *
	 * @var string
	 */
	private $tableName;

	/**
	 *
	 * @var string
	 */
	private $date;

	/**
	 *
	 * @var boolean
	 */
	private $changed = false;

	/**
	 *
	 * @param string $replicatorName
	 * @param string $webMethodName
	 * @param string $date
	 */
	public function __construct($replicatorName = null, $webMethodName = null, $date = null, $tableName = null) {
		if (null !== $replicatorName) $this->setReplicatorName($replicatorName);
		if (null !== $webMethodName) $this->setWebMethodName($webMethodName);
		if (null !== $date) $this->setDate($date);
        if (null !== $tableName) $this->setTableName($tableName);
		$this->acceptChanges();
	}

	/**
	 *
	 * @return string
	 */
	public function getReplicatorName() {
		return $this->replicatorName;
	}

	/**
	 *
	 * @param string $replicatorName        	
	 * @return \SoloSettings\Entity\ReplicatorLastUpdate
	 */
	public function setReplicatorName($replicatorName) {
		$this->replicatorName = $replicatorName;
		$this->setChanged();
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getWebMethodName() {
		return $this->webMethodName;
	}

	/**
	 *
	 * @param string $webMethodName        	
	 * @return \SoloSettings\Entity\ReplicatorLastUpdate
	 */
	public function setWebMethodName($webMethodName) {
		$this->webMethodName = $webMethodName;
		$this->setChanged();
		return $this;
	}
    
    /**
	 *
	 * @return string
	 */
	public function getTableName() {
		return $this->tableName;
	}

	/**
	 *
	 * @param string $tableName        	
	 * @return \SoloSettings\Entity\ReplicatorLastUpdate
	 */
	public function setTableName($tableName) {
		$this->tableName = $tableName;
		$this->setChanged();
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 *
	 * @param string $date
	 * @return \SoloSettings\Entity\ReplicatorLastUpdate
	 */
	public function setDate($date) {
		$this->date = $date;
		$this->setChanged();
		return $this;
	}

	/**
	 * Mark object as changed
	 */
	public function setChanged() {
		$this->changed = true;
	}

	/**
	 * Checks is object has changes
	 * 
	 * @return boolean
	 */
	public function hasChanges() {
		return $this->changed;
	}

	/**
	 * Mark object as original
	 */
	public function acceptChanges() {
		$this->changed = false;
	}

}

?>