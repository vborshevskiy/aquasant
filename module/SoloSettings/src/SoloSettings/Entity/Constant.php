<?php

namespace SoloSettings\Entity;

class Constant {

	/**
	 *
	 * @var string
	 */
	private $name;

	/**
	 *
	 * @var string
	 */
	private $value;

	/**
	 *
	 * @var integer
	 */
	private $siteId;

	/**
	 *
	 * @var boolean
	 */
	private $changed = false;

	/**
	 *
	 * @param string $name        	
	 * @param string $value        	
	 * @param integer $siteId        	
	 */
	public function __construct($name = null, $value = null, $siteId = null) {
		if (null !== $name) $this->setName($name);
		if (null !== $value) $this->setValue($value);
		if (null !== $siteId) $this->setSiteId($siteId);
		$this->acceptChanges();
	}

	/**
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 *
	 * @param string $name        	
	 * @return \SoloSettings\Entity\Constant
	 */
	public function setName($name) {
		$this->name = $name;
		$this->setChanged();
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 *
	 * @param string $value        	
	 * @return \SoloSettings\Entity\Constant
	 */
	public function setValue($value) {
		$this->value = $value;
		$this->setChanged();
		return $this;
	}

	/**
	 *
	 * @return integer
	 */
	public function getSiteId() {
		return $this->siteId;
	}

	/**
	 *
	 * @param integer $siteId        	
	 * @throws \InvalidArgumentException
	 * @return \SoloSettings\Entity\Constant
	 */
	public function setSiteId($siteId) {
		if (!is_integer($siteId)) {
			throw new \InvalidArgumentException('Site id must be integer');
		}
		$this->siteId = $siteId;
		$this->setChanged();
		return $this;
	}

	/**
	 * Mark object as changed
	 */
	public function setChanged() {
		$this->changed = true;
	}

	/**
	 * Checks is object has changes
	 * 
	 * @return boolean
	 */
	public function hasChanges() {
		return $this->changed;
	}

	/**
	 * Mark object as original
	 */
	public function acceptChanges() {
		$this->changed = false;
	}

}

?>