<?php

namespace SoloSettings\Entity;

use SoloSettings\Triggers;

class Trigger {

	/**
	 *
	 * @var string
	 */
	private $name;

	/**
	 *
	 * @var array
	 */
	private $tables = [];

	/**
	 * Initialize trigger with specified name
	 * 
	 * @param string $name        	
	 */
	public function __construct($name) {
		$this->name = $name;
	}

	/**
	 * Gets trigger name
	 * 
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Add table with specified state
	 * 
	 * @param string $state        	
	 * @param string $tableName        	
	 * @return Trigger
	 */
	public function addTable($state, $tableName) {
		$this->tables[$state] = $tableName;
		return $this;
	}

	/**
	 * Gets table for specified state
	 * 
	 * @param string $state        	
	 * @throws \RuntimeException
	 * @return string
	 */
	public function getTable($state = 'active') {
		if (!isset($this->tables[$state])) {
			throw new \RuntimeException(sprintf('Failed to get table with state \'%s\'', $state));
		}
		return $this->tables[$state];
	}

	/**
	 * Swaps tables between specified states
	 * 
	 * @param string $fromState        	
	 * @param string $toState        	
	 * @return Triggers
	 */
	public function swap($fromState = 'active', $toState = 'passive') {
		$from = $this->getTable($fromState);
		$to = $this->getTable($toState);
		$this->tables[$fromState] = $to;
		$this->tables[$toState] = $from;
		return $this;
	}

}

?>