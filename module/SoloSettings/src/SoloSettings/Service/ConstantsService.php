<?php

namespace SoloSettings\Service;

use SoloSettings\Entity\Constant;
use Solo\ServiceManager\ProvidesOptions;
use SoloSettings\Data\ConstantsTableInterface;

class ConstantsService {
	
	use ProvidesOptions;

	/**
	 *
	 * @var ConstantsTableInterface
	 */
	private $constantsTable;

	/**
	 *
	 * @var boolean
	 */
	private $isLoaded = false;

	/**
	 *
	 * @var array
	 */
	private $items = [];

	/**
	 *
	 * @param ConstantsTableInterface $constantsTable        	
	 */
	public function __construct(ConstantsTableInterface $constantsTable) {
		$this->constantsTable = $constantsTable;
	}

	/**
	 *
	 * @param boolean $force        	
	 * @param string $name        	
	 */
	private function reload($force = false) {
		if (!$this->isLoaded || $force) {
			if ($this->hasOption('siteId')) {
				$rows = $this->constantsTable->findBySiteId(intval($this->getOption('siteId')));
			} else {
				$rows = $this->constantsTable->findAll();
			}
			foreach ($rows as $row) {
				$constant = new Constant($row->ConstantName, $row->ConstantValue, intval($row->SiteId));
				$this->items[$constant->getName()] = $constant;
			}
			$this->isLoaded = true;
		}
	}

	/**
	 *
	 * @param string $name        	
	 * @return boolean
	 */
	public function has($name) {
		$this->reload();
		return array_key_exists($name, $this->items);
	}

	/**
	 *
	 * @param string $name        	
	 * @return string | NULL
	 */
	public function get($name) {
		$this->reload();
		if (array_key_exists($name, $this->items)) {
			return $this->items[$name]->getValue();
		}
		return null;
	}

	/**
	 *
	 * @param string $name        	
	 * @param string $value        	
	 * @param integer $siteId        	
	 * @return \SoloSettings\Service\ConstantsService
	 */
	public function set($name, $value, $siteId = null) {
		if (!$this->has($name)) {
			if (null === $siteId) {
				$siteId = ($this->hasOption('siteId')) ? intval($this->getOption('siteId')) : 0;
			}
			$constant = new Constant($name, $value, $siteId);
			$this->items[$constant->getName()] = $constant;
			$constant->setChanged();
		} else {
			$this->items[$name]->setValue($value);
		}
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasChanges() {
		foreach ($this->items as $item) {
			if ($item->hasChanges()) return true;
		}
		return false;
	}

	/**
	 *
	 * @return array
	 */
	private function getChangedItems() {
		$items = [];
		foreach ($this->items as $item) {
			if ($item->hasChanges()) $items[] = $item;
		}
		return $items;
	}

	/**
	 * Save constants with changed values
	 */
	public function saveChanges() {
		$changedItems = $this->getChangedItems();
		foreach ($changedItems as $item) {
			$this->constantsTable->saveConstant($item);
			$item->acceptChanges();
		}
	}

}

?>