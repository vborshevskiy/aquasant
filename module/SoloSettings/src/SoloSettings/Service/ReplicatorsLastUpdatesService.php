<?php

namespace SoloSettings\Service;

use SoloSettings\Entity\ReplicatorLastUpdate;
use SoloSettings\Data\ReplicatorsLastUpdatesTableInterface;

class ReplicatorsLastUpdatesService {

    /**
     *
     * @var ReplicatorsLastUpdatesTableInterface
     */
    private $replicatorsLastUpdatesTable;

    /**
     *
     * @var boolean
     */
    private $isLoaded = false;

    /**
     *
     * @var array
     */
    private $items = [];

    /**
     *
     * @param ReplicatorsLastUpdatesTableInterface $replicatorsLastUpdatesTable  	
     */
    public function __construct(ReplicatorsLastUpdatesTableInterface $replicatorsLastUpdatesTable) {
        $this->replicatorsLastUpdatesTable = $replicatorsLastUpdatesTable;
    }

    /**
     *
     * @param boolean $force
     */
    private function reload($force = false) {
        if (!$this->isLoaded || $force) {
            $rows = $this->replicatorsLastUpdatesTable->findAll();
            foreach ($rows as $row) {
                $lastUpdate = new ReplicatorLastUpdate($row->ReplicatorName, $row->WebMethodName, $row->LastUpdate, $row->TableName);
                $this->items[$row->ReplicatorName][$row->WebMethodName][$row->TableName] = $lastUpdate;
            }
            $this->isLoaded = true;
        }
    }

    /**
     *
     * @param string $replicatorName
     * @param string $methodName
     * @return boolean
     */
    public function has($replicatorName, $methodName, $tableName) {
        $this->reload();
        if (array_key_exists($replicatorName, $this->items)) {
            if (array_key_exists($methodName, $this->items[$replicatorName])) {
                return array_key_exists($tableName, $this->items[$replicatorName][$methodName]);
            }
        }
        return false;
    }

    /**
     *
     * @param string $name        	
     * @return string | NULL
     */
    public function get($replicatorName, $methodName, $tableName) {
        $this->reload();
        if ($this->has($replicatorName, $methodName, $tableName)) {
            return $this->items[$replicatorName][$methodName][$tableName]->getDate();
        }
        return null;
    }

    /**
     *
     * @param string $replicatorName        	
     * @param string $methodName        	
     * @param string $date
     * @return \SoloSettings\Service\ReplicatorsLastUpdatesService
     */
    public function set($replicatorName, $methodName, $date, $tableName) {
        if (!$this->has($replicatorName, $methodName, $tableName)) {
            $lastUpdate = new ReplicatorLastUpdate($replicatorName, $methodName, $date, $tableName);
            $this->items[$lastUpdate->getReplicatorName()][$lastUpdate->getWebMethodName()][$lastUpdate->getTableName()] = $lastUpdate;
            $lastUpdate->setChanged();
        } else {
            $this->items[$replicatorName][$methodName][$tableName]->setDate($date);
        }
        return $this;
    }

    public function saveChanges() {
        $changedItems = $this->getChangedItems();
        foreach ($changedItems as $item) {
            $this->replicatorsLastUpdatesTable->saveReplicatorLastUpdateDate($item);
            $item->acceptChanges();
        }
    }

    /**
     *
     * @return array
     */
    private function getChangedItems() {
        $items = [];
        foreach ($this->items as $replicatorsItem) {
            foreach ($replicatorsItem as $methodItem) {
                foreach ($methodItem as $tableItem) {
                    if ($tableItem->hasChanges()) {
                        $items[] = $tableItem;
                    }
                }
            }
        }
        return $items;
    }

    /**
     *
     * @return boolean
     */
    public function hasChanges() {
        foreach ($this->items as $replicatorsItem) {
            foreach ($replicatorsItem as $item) {
                if ($item->hasChanges()) {
                    return true;
                }
            }
        }
        return false;
    }

}

?>