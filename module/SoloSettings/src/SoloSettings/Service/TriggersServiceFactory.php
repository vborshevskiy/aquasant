<?php

namespace SoloSettings\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class TriggersServiceFactory extends AbstractServiceFactory {

	protected function create() {
		$service = new TriggersService($this->getServiceLocator()->get('SoloSettings\\Data\\TriggersTable'));
		return $service;
	}

}

?>