<?php

namespace SoloSettings\Service;

use SoloSettings\Entity\Trigger;
use SoloSettings\Data\TriggersTableInterface;

class TriggersService {

    /**
     *
     * @var TriggersTableInterface
     */
    private $triggersTable;

    /**
     * 
     * @var boolean
     */
    private $isLoaded = false;

    /**
     * 
     * @var array
     */
    private $items = [];

    /**
     * 
     * @param TriggersTableInterface $triggersTable
     */
    public function __construct(TriggersTableInterface $triggersTable) {
        $this->triggersTable = $triggersTable;
    }

    /**
     * 
     * @param boolean $force
     * @param string $name
     */
    public function reload($force = false, $name = null) {
        if (!$this->isLoaded || $force) {
            if (null !== $name) {
                $rows = $this->triggersTable->findByTriggerName($name);
            } else {
                $rows = $this->triggersTable->findAll();
            }
            foreach ($rows as $row) {
                if (!array_key_exists($row->TriggerName, $this->items)) {
                    $trigger = new Trigger($row->TriggerName);
                    $this->items[$row->TriggerName] = $trigger;
                }
                $this->items[$row->TriggerName]->addTable($row->State, $row->TableName);
            }

            $this->isLoaded = true;
        }
    }

    /**
     *
     * @param string $name        	
     * @return boolean
     */
    public function has($name) {
        $this->reload();
        return array_key_exists($name, $this->items);
    }

    /**
     *
     * @param string $name        	
     * @throws \RuntimeException
     * @return Trigger
     */
    public function get($name) {
        $this->reload();
        if (!$this->has($name)) {
            $this->reload(true, $name);
        }
        if (!$this->has($name)) {
            throw new \RuntimeException(sprintf('Trigger "%s" not exists or failed to load', $name));
        }
        return $this->items[$name];
    }

    /**
     * 
     * @param string $name
     * @return string
     */
    public function active($name) {
        return $this->get($name)->getTable('active');
    }

    /**
     * 
     * @param string $name
     * @return string
     */
    public function passive($name) {
        return $this->get($name)->getTable('passive');
    }

    /**
     * 
     * @throws Exception
     */
    public function swap() {
        foreach (func_get_args() as $arg) {
            if (is_string($arg)) {
                $trigger = $this->get($arg);
                $trigger->swap();
                $this->triggersTable->saveTrigger($trigger);
            } elseif (is_array($arg)) {
                foreach ($arg as $triggerName) {
                    $this->swap($triggerName);
                }
            }
        }
    }

    /**
     * 
     * @param string $state
     * @return boolean
     */
    public function isValidState($state) {
        return in_array(trim($state), [
            'active',
            'passive'
        ]);
    }

}

?>