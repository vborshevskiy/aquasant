<?php

namespace SoloSettings\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class ReplicatorsLastUpdatesServiceFactory extends AbstractServiceFactory {

	protected function create() {
		$service = new ReplicatorsLastUpdatesService($this->getServiceLocator()->get('SoloSettings\\Data\\ReplicatorsLastUpdatesTable'));
		return $service;
	}

}

?>