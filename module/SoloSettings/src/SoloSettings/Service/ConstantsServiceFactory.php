<?php

namespace SoloSettings\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class ConstantsServiceFactory extends AbstractServiceFactory {

	protected function create() {
		$service = new ConstantsService($this->getServiceLocator()->get('SoloSettings\\Data\\ConstantsTable'));
		if ($this->hasConfig('sites.id')) {
			$service->setOption('siteId', $this->getConfig('sites.id'));
		}
		return $service;
	}

}

?>