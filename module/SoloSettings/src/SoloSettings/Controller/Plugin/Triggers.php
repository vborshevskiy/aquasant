<?php

namespace SoloSettings\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use SoloSettings\Service\TriggersService;

class Triggers extends AbstractPlugin {

	/**
	 *
	 * @return TriggersService
	 */
	public function __invoke() {
		return $this->getController()->getServiceLocator()->get('\\SoloSettings\\Service\\TriggersService');
	}

}

?>