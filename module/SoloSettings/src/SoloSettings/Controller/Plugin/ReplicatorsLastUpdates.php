<?php

namespace SoloSettings\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class ReplicatorsLastUpdates extends AbstractPlugin {

	/**
	 *
	 * @param string $name        	
	 * @return \SoloSettings\Service\ReplicatorsLastUpdatesService | mixed
	 */
	public function __invoke($name = null) {
		$serv = $this->getController()->getServiceLocator()->get('\\SoloSettings\\Service\\ReplicatorsLastUpdatesService');
		if (null !== $name) {
			return $serv->get($name);
		}
		return $serv;
	}

}

?>