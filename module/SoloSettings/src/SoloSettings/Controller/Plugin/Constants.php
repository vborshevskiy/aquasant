<?php

namespace SoloSettings\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class Constants extends AbstractPlugin {

	/**
	 *
	 * @param string $name        	
	 * @return \SoloSettings\Service\ConstantsService | mixed
	 */
	public function __invoke($name = null) {
		$serv = $this->getController()->getServiceLocator()->get('\\SoloSettings\\Service\\ConstantsService');
		if (null !== $name) {
			return $serv->get($name);
		}
		return $serv;
	}

}

?>