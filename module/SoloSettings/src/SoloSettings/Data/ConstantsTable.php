<?php

namespace SoloSettings\Data;

use Solo\Db\TableGateway\AbstractTable;
use SoloSettings\Entity\Constant;
use Zend\Db\Sql\Predicate\Predicate;

class ConstantsTable extends AbstractTable implements ConstantsTableInterface {

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \SoloSettings\Data\ConstantsTableInterface::findAll()
	 */
	public function findAll() {
		$select = $this->createSelect();
		$select->order('SiteId ASC');
		return $this->tableGateway->selectWith($select);
	}

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \SoloSettings\Data\ConstantsTableInterface::findBySiteId()
	 */
	public function findBySiteId($siteId) {
		$select = $this->createSelect();
		$select->where([
			'SiteId' => $siteId 
		]);
		$select->where([
			'SiteId' => 0 
		], Predicate::OP_OR);
		$select->order('SiteId ASC');
		return $this->tableGateway->selectWith($select);
	}

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \SoloSettings\Data\ConstantsTableInterface::saveConstant()
	 */
	public function saveConstant(Constant $constant) {
		$sql = "INSERT INTO constants (`ConstantName`, `ConstantValue`, `SiteId`)
				VALUES ('" . $constant->getName() . "', '" . $constant->getValue() . "', " . $constant->getSiteId() . ")
				ON DUPLICATE KEY
				UPDATE ConstantValue = '" . $constant->getValue() . "'";
		$this->query($sql);
	}

}

?>