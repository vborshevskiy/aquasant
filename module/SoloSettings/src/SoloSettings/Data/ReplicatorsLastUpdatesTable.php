<?php

namespace SoloSettings\Data;

use Solo\Db\TableGateway\AbstractTable;
use SoloSettings\Entity\ReplicatorLastUpdate;

class ReplicatorsLastUpdatesTable extends AbstractTable implements ReplicatorsLastUpdatesTableInterface {

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \SoloSettings\Data\ReplicatorsLastUpdatesTableInterface::findAll()
	 */
	public function findAll() {
		$select = $this->createSelect();
		return $this->tableGateway->selectWith($select);
	}

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \SoloSettings\Data\ReplicatorsLastUpdatesTableInterface::saveReplicatorLastUpdateDate()
	 */
	public function saveReplicatorLastUpdateDate(ReplicatorLastUpdate $replicatorLastUpdate) {
		$sql = "INSERT INTO
                        replicators_last_updates (ReplicatorName, WebMethodName, LastUpdate, TableName)
				VALUES (
                        '" . $replicatorLastUpdate->getReplicatorName() . "',
                        '" . $replicatorLastUpdate->getWebMethodName() . "',
                        '" . $replicatorLastUpdate->getDate() . "',
                        '" . $replicatorLastUpdate->getTableName()."')
				ON DUPLICATE KEY
				UPDATE
                        LastUpdate = '" . $replicatorLastUpdate->getDate() . "'";
		$this->query($sql);
	}

}

?>