<?php

namespace SoloSettings\Data;

use SoloSettings\Entity\Constant;

interface ConstantsTableInterface {

	/**
	 * Enum all constants
	 * 
	 * @return \Zend\Db\ResultSet\ResultSet
	 */
	public function findAll();

	/**
	 *
	 * @param integer $siteId        	
	 * @return \Zend\Db\ResultSet\ResultSet
	 */
	public function findBySiteId($siteId);

	/**
	 *
	 * @param Constant $constant        	
	 * @return \Zend\Db\Adapter\Driver\ResultInterface
	 */
	public function saveConstant(Constant $constant);

}

?>