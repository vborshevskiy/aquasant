<?php

namespace SoloSettings\Data;

use Solo\Db\TableGateway\AbstractTable;
use SoloSettings\Entity\Trigger;

class TriggersTable extends AbstractTable implements TriggersTableInterface {

	public function findByTriggerName($name) {
		return $this->tableGateway->select([
			'TriggerName' => $name 
		]);
	}
	
	public function findAll() {
		return $this->tableGateway->select();
	}
	
	public function saveTrigger(Trigger $trigger) {
		$this->tableGateway->beginTransaction();
		try {
			$this->tableGateway->update([
				'TableName' => $trigger->getTable('active')
				], [
				'TriggerName' => $trigger->getName(),
				'State' => 'active'
				]);
			$this->tableGateway->update([
				'TableName' => $trigger->getTable('passive')
				], [
				'TriggerName' => $trigger->getName(),
				'State' => 'passive'
				]);
			$this->tableGateway->commit();
		} catch (\Exception $e) {
			$this->tableGateway->rollback();
			throw $e;
		}
	}

}

?>