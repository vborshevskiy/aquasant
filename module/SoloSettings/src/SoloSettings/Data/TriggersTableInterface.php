<?php

namespace SoloSettings\Data;

use Zend\Db\ResultSet\ResultSet;
use SoloSettings\Entity\Trigger;

interface TriggersTableInterface {

	/**
	 *
	 * @param string $name        	
	 * @return ResultSet
	 */
	public function findByTriggerName($name);
	
	/**
	 * @return ResultSet
	 */
	public function findAll();
	
	public function saveTrigger(Trigger $trigger);

}

?>