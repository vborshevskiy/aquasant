<?php

namespace SoloSettings\Data;

use SoloSettings\Entity\ReplicatorLastUpdate;

interface ReplicatorsLastUpdatesTableInterface {

	/**
	 * 
	 * @return \Zend\Db\ResultSet\ResultSet
	 */
	public function findAll();

	/**
	 *
	 * @param ReplicatorLastUpdate $replicatorLastUpdate
	 * @return \Zend\Db\Adapter\Driver\ResultInterface
	 */
	public function saveReplicatorLastUpdateDate(ReplicatorLastUpdate $replicatorLastUpdate);

}

?>