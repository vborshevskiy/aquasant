<?php

use Zend\Log\Logger;

return [
    'solo_log' => [
        'common' => [
            'writers' => [
                'file' => [
                    'type' => 'stream',
                    'priority' => null,
                    'options' => [
                        'stream' => 'data/ws_err.log'
                    ]
                ]
            ]
        ],
        'webservice' => [
            'writers' => [
                'success' => [
                    'type' => 'stream',
                    'priority' => null,
                    'options' => [
                        'stream' => '/data/ws_succes.log'
                    ],
                    'filters' => [
                        [
                            'type' => 'priority',
                            'value' => [
                                'priority' => Logger::NOTICE,
                                'operator' => '>='
                            ]
                        ]
                    ]
                ],
                'failure' => [
                    'type' => 'stream',
                    'priority' => null,
                    'options' => [
                        'stream' => 'd:/webservice_failure.log'
                    ],
                    'filters' => [
                        [
                            'type' => 'priority',
                            'value' => Logger::WARN
                        ]
                    ]
                ],
                'info' => [
                    'type' => 'stream',
                    'priority' => null,
                    'options' => [
                        'stream' => 'd:/webservice_info.log'
                    ],
                    'filters' => [
                        [
                            'type' => 'priority',
                            'value' => Logger::INFO
                        ]
                    ]
                ]
            ]
        ],
        'reserve_draft_removal' => [
            'writers' => [
                'file' => [
                    'type' => 'stream',
                    'priority' => null,
                    'options' => [
                        'stream' => 'data/log/reserve_draft_removal.log'
                    ]
                ]
            ]
        ],
        'ws_logger' => [
            'writers' => [
                'file' => [
                    'type' => 'stream',
                    'priority' => null,
                    'options' => [
                        'stream' => dirname(__DIR__).'/../../data/log/exc/'.date('d-m-Y').'.log'
                    ]
                ]
            ]
        ],
        'orders_logger' => [
            'writers' => [
                'file' => [
                    'type' => 'stream',
                    'priority' => null,
                    'options' => [
                        'stream' => dirname(__DIR__).'/../../data/log/orders.log'
                    ]
                ]
            ]
        ],
        'auth_logger' => [
            'writers' => [
                'file' => [
                    'type' => 'stream',
                    'priority' => null,
                    'options' => [
                        'stream' => dirname(__DIR__).'/../../data/log/auth.log'
                    ]
                ]
            ]
        ],
        'yandex_kassa_aviso' => [
            'writers' => [
                'file' => [
                    'type' => 'stream',
                    'priority' => null,
                    'options' => [
                        'stream' => dirname(__DIR__).'/../../data/log/yandex_kassa_aviso.log'
                    ]
                ]
            ]
        ],
        'yandex_kassa_check' => [
            'writers' => [
                'file' => [
                    'type' => 'stream',
                    'priority' => null,
                    'options' => [
                        'stream' => dirname(__DIR__).'/../../data/log/yandex_kassa_check.log'
                    ]
                ]
            ]
        ],
        'yandex_kassa_success' => [
            'writers' => [
                'file' => [
                    'type' => 'stream',
                    'priority' => null,
                    'options' => [
                        'stream' => dirname(__DIR__).'/../../data/log/yandex_kassa_success.log'
                    ]
                ]
            ]
        ],
        'yandex_kassa_fail' => [
            'writers' => [
                'file' => [
                    'type' => 'stream',
                    'priority' => null,
                    'options' => [
                        'stream' => dirname(__DIR__).'/../../data/log/yandex_kassa_fail.log'
                    ]
                ]
            ]
        ],
        'pdf_convertation' => [
            'writers' => [
                'file' => [
                    'type' => 'stream',
                    'priority' => null,
                    'options' => [
                        'stream' => dirname(__DIR__).'/../../data/log/pdf_convertation.log'
                    ]
                ]
            ]
        ],
        'yandex-market' => [
            'writers' => [
                'file' => [
                    'type' => 'stream',
                    'priority' => null,
                    'options' => [
                        'stream' => dirname(__DIR__).'/../../data/log/yandex-market.log'
                    ]
                ]
            ]
        ],
        'ws' => [
            'writers' => [
                'file' => [
                    'type' => 'stream',
                    'priority' => null,
                    'options' => [
                        'stream' => dirname(__DIR__).'/../../data/log/ws.log'
                    ]
                ]
            ]
        ],
    ]
];
