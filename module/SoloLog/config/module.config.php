<?php
use SoloLog\Service\LogManager;

return array(
	'controller_plugins' => array(
		'invokables' => array(
			'logger' => 'SoloLog\Controller\Plugin\Logger' 
		) 
	),
	'service_manager' => array(
		'factories' => array(
			'SoloLog\Service\LogManager' => function ($sm) {
				$config = $sm->get('Config');
				if (!isset($config['solo_log'])) {
					throw new \RuntimeException('Failed to create logger, no configuration for initializing');
				}
				return new LogManager($config['solo_log']);
			} 
		) 
	) 
);
