<?php

namespace SoloLog;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Solo\ModuleManager\MultiConfigModule;
use SoloLog\Service\ProvidesLogger;
use Zend\Mvc\MvcEvent;

class Module extends MultiConfigModule implements AutoloaderProviderInterface {

	public function __construct() {
		parent::__construct(__DIR__);
	}

	public function getAutoloaderConfig() {
		return [
			'Zend\Loader\ClassMapAutoloader' => [
				__DIR__ . DIRECTORY_SEPARATOR . 'autoload_classmap.php' 
			],
			'Zend\Loader\StandardAutoloader' => [
				'namespaces' => [
					__NAMESPACE__ => __DIR__ . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . str_replace('\\', '/', __NAMESPACE__) 
				] 
			] 
		];
	}

	public function onBootstrap(MvcEvent $e) {
		$sm = $e->getApplication()->getServiceManager();
		$logManager = $sm->get(__NAMESPACE__ . '\\Service\\LogManager');
		ProvidesLogger::setStaticLogManager($logManager);
	}

}