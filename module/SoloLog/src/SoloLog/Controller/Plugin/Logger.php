<?php

namespace SoloLog\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Log\Logger as ZendLogger;

class Logger extends AbstractPlugin {

	/**
	 *
	 * @param string $name        	
	 * @return ZendLogger
	 */
	public function __invoke($name = 'common') {
		$lm = $this->getController()->getServiceLocator()->get('\\SoloLog\\Service\\LogManager');
		return $lm->create($name);
	}

}
?>