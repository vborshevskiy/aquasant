<?php

namespace SoloLog\Service;

trait ProvidesLogger {

	/**
	 *
	 * @var LogManager
	 */
	private $logManager = null;

	/**
	 *
	 * @var LogManager
	 */
	private static $staticLogManager = null;

	/**
	 *
	 * @param LogManager $logManager        	
	 */
	public function setLogManager(LogManager $logManager) {
		$this->logManager = $logManager;
	}

	/**
	 *
	 * @return \SoloLog\Service\LogManager NULL
	 */
	public function getLogManager() {
		if (null !== $this->logManager) {
			return $this->logManager;
		}
		if (null !== self::$staticLogManager) {
			return self::getStaticLogManager();
		}
		return null;
	}

	/**
	 *
	 * @param LogManager $logManager        	
	 */
	public static function setStaticLogManager(LogManager $logManager) {
		self::$staticLogManager = $logManager;
	}

	/**
	 *
	 * @return \SoloLog\Service\LogManager
	 */
	public static function getStaticLogManager() {
		return self::$staticLogManager;
	}

	/**
	 *
	 * @return \SoloLog\Service\LogManager
	 */
	public function logger() {
		return $this->getLogManager();
	}

}

?>