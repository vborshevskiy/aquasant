<?php

namespace SoloLog\Service;

use Zend\Log\Logger;
use Zend\Log\Filter\Priority;

class LogManager {

	/**
	 *
	 * @var array
	 */
	private $instances = [];

	/**
	 *
	 * @var array
	 */
	private $config = null;

	/**
	 *
	 * @param array $config        	
	 * @throws \InvalidArgumentException
	 */
	public function __construct($config) {
		if (!is_array($config)) {
			throw new \InvalidArgumentException('Config must be array');
		}
		$this->config = $config;
	}

	/**
	 *
	 * @param string $name        	
	 * @throws \InvalidArgumentException
	 * @throws \RuntimeException
	 * @return \Zend\Log\Logger
	 */
	public function create($name) {
		if (empty($name)) {
			throw new \InvalidArgumentException('Name can\'t be empty');
		}
		if (isset($this->instances[$name])) {
			return $this->instances[$name];
		} else {
			if (!isset($this->config[$name])) {
				throw new \RuntimeException('Failed to create logger \'' . $name . '\'. Configuration doesn\'t defined');
			}
			$logger = new Logger();
			foreach ($this->config[$name]['writers'] as $wrInfo) {
				$writer = $logger->writerPlugin($wrInfo['type'], $wrInfo['options']);
				if (isset($wrInfo['filters'])) {
					foreach ($wrInfo['filters'] as $fInfo) {
						if ('priority' == $fInfo['type']) {
							$filter = new Priority($fInfo['value']);
							$writer->addFilter($filter);
						}
					}
				}
				if (null !== $wrInfo['priority']) {
					$logger->addWriter($writer, $wrInfo['priority']);
				} else {
					$logger->addWriter($writer);
				}
			}
			$this->instances[$name] = $logger;
			return $logger;
		}
	}

}

?>