<?php

use SoloCache\Task\ExpiredCacheCleaner;

return [
    'tasks' => [
        'clear_expired_cache' => 'SoloCache\Task\ExpiredCacheCleaner'
    ],
    'service_manager' => [
        'factories' => [
            'SoloCache\Task\ExpiredCacheCleaner' => function ($sm) {
                $cacheService = $sm->get('SoloCache\Service\CacheService');
                $task = new ExpiredCacheCleaner($cacheService);
                return $task;
            },
        ],
    ],
];
