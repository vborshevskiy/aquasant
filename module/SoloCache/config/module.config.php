<?php
use Zend\Cache\StorageFactory;
use SoloCache\Service\CacheService;

return [
	'controller_plugins' => [
		'invokables' => [
			'cache' => 'SoloCache\Controller\Plugin\Cache' 
		] 
	],
	'service_manager' => [
		'aliases' => [
			'cache' => 'SoloCache\Service\CacheService' 
		],
		'factories' => [
			'SoloCache\Service\CacheService' => function ($sm) {
				$config = $sm->get('Config');
				if (isset($config['solo_cache'])) {
					$settings = $config['solo_cache'];
					$storage = StorageFactory::factory($settings['storage']);
					$serv = new CacheService($storage);
					$serv->helper()->setTriggers($sm->get('triggers'));
					return $serv;
				}
				return null;
			} 
		] 
	] 
];
