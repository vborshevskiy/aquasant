<?php
return [
	'solo_cache' => [
		'storage' => [
			'adapter' => [
				'name' => 'filesystem',
				'options' => [
					'ttl' => 200,
					'cache_dir' => __DIR__ . "/../../../data/cache",
					'file_permission' => 0777,
					'dir_permission' => 0777 
				] 
			],
			'plugins' => [
				'exception_handler' => [
					'throw_exceptions' => true 
				],
				'serializer' 
			] 
		] 
	] 
];