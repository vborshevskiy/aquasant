<?php

namespace SoloCache\Service;

use SoloCache\Service\CacheService;
use SoloCache\Entity\MethodCacher;

trait ProvidesCache {

	/**
	 *
	 * @var CacheService
	 */
	private $cacheService = null;

	/**
	 *
	 * @var CacheService
	 */
	private static $staticCacheService = null;

	/**
	 *
	 * @param CacheService $cacheService        	
	 */
	public function setCacheService(CacheService $cacheService) {
		$this->cacheService = $cacheService;
	}

	/**
	 *
	 * @return \SoloCache\Service\CacheService | NULL
	 */
	protected function getCacheService() {
		if (null !== $this->cacheService) {
			return $this->cacheService;
		}
		if (null !== self::$staticCacheService) {
			return self::getStaticCacheService();
		}
		return null;
	}

	/**
	 *
	 * @param CacheService $cacheService        	
	 */
	public static function setStaticCacheService(CacheService $cacheService) {
		self::$staticCacheService = $cacheService;
	}

	/**
	 *
	 * @return \SoloCache\Service\CacheService
	 */
	public static function getStaticCacheService() {
		return self::$staticCacheService;
	}

	/**
	 *
	 * @return \SoloCache\Service\CacheService | NULL
	 */
	protected function cache() {
		return $this->getCacheService();
	}
	
	/**
	 * 
	 * @param string $method
	 * @param array $arguments
	 * @return \SoloCache\Entity\MethodCacher
	 */
	protected function createMethodCacher($method, $arguments) {
		$cacher = new MethodCacher($method, $arguments);
		$cacher->setCacheService($this->getCacheService());
		return $cacher;
	}

}

?>