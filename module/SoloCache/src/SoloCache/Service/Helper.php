<?php

namespace SoloCache\Service;

use SoloSettings\Service\TriggersService;

class Helper {

	/**
	 *
	 * @var TriggersService
	 */
	private $triggers;

	/**
	 *
	 * @param TriggersService $triggers        	
	 */
	public function setTriggers(TriggersService $triggers) {
		$this->triggers = $triggers;
	}

	/**
	 *
	 * @return array
	 */
	public function extractTagsFromMethods() {
		if ((1 == func_num_args()) && is_array(func_get_args(0))) {
			$dependecies = [];
			foreach (func_get_arg(0) as $item) {
				$method = $item[0];
				$class = (isset($item[1]) ? $item[1] : null);
				$dependecies = array_merge($dependecies, $this->tagsFromMethod($method, $class));
			}
			return array_unique($dependecies);
		} elseif ((2 == func_num_args()) && is_string(func_get_arg(0)) && (is_object(func_get_arg(1)) || is_string(func_get_arg(1)))) {
			$method = func_get_arg(0);
			$class = func_get_arg(1);
			return $this->tagsFromMethod($method, $class);
		} else {
			return [];
		}
	}

	/**
	 *
	 * @param mixed $methodInfoOrName        	
	 * @param string $class        	
	 * @throws \InvalidArgumentException
	 * @return array
	 */
	public function tagsFromMethod($methodInfoOrName, $class = null) {
		$method = null;
		if (null !== $class) {
			$className = null;
			if (is_string($class)) {
				$className = $class;
			}
			if (is_object($class)) {
				$className = get_class($class);
			}
			if (null === $className) {
				throw new \InvalidArgumentException('Specified class must be object or string');
			}
			$method = new \ReflectionMethod($className, $methodInfoOrName);
		} else {
			if (is_object($methodInfoOrName) && ($methodInfoOrName instanceof \ReflectionMethod)) {
				$method = $methodInfoOrName;
			} else {
				$method = new \ReflectionMethod($methodInfoOrName);
			}
		}
		$docComment = $method->getDocComment();
		if (false !== $docComment) {
			$dependencies = [];
			$docLines = explode("\n", $docComment);
			foreach ($docLines as $line) {
				if (false !== ($offset = strpos($line, '@dependency_table'))) {
					$value = trim(substr($line, $offset + 18));
					list($name, $state) = explode(':', $value);
					if (empty($state)) {
						$dependencies[] = $name;
					} else {
						if ($this->triggers->isValidState($state)) {
							$dependencies[] = $this->triggers->get($name)->getTable($state);
						}
					}
				} elseif (false !== ($offset = strpos($line, '@dependency'))) {
					$value = substr($line, $offset + 12);
					$dependencies[] = trim($value);
				}
			}
			return $dependencies;
		}
		return [];
	}

}

?>