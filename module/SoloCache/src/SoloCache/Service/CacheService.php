<?php

namespace SoloCache\Service;

use Zend\Cache\Storage\StorageInterface;
use Zend\Cache\Storage\TaggableInterface;
use Zend\Cache\Storage\FlushableInterface;
use SoloCache\Entity\Tags;

class CacheService {

    /**
     *
     * @var StorageInterface
     */
    private $storage;

    /**
     *
     * @var Helper
     */
    private $helper = null;

    /**
     *
     * @param StorageInterface $storage        	
     */
    public function __construct(StorageInterface $storage) {
        $this->storage = $storage;
    }

    /**
     *
     * @param string $name        	
     */
    public function get($name) {
        return $this->storage->getItem($name);
    }

    /**
     *
     * @param string $name        	
     * @param mixed $value        	
     * @param string[] $tags        	
     */
    public function set($name, $value, $tags = null) {
        $this->storage->setItem($name, $value);
        if ((null !== $tags) && ($this->storage instanceof TaggableInterface)) {
            if ($tags instanceof \Solo\Collection\Collection) {
                $tags = $tags->toArray();
            }
            $this->storage->setTags($name, (array) $tags);
        }
    }

    /**
     *
     * @param string $name        	
     * @return boolean
     */
    public function has($name) {
        return $this->storage->hasItem($name);
    }

    /**
     *
     * @param string[] $tags        	
     * @param boolean $disjunction        	
     * @return boolean
     */
    public function clearByTags($tags, $disjunction = false) {
        if ($this->storage instanceof TaggableInterface) {
            return $this->storage->clearByTags($tags, $disjunction);
        }
        return false;
    }

    /**
     *
     * @param string $tag        	
     * @param boolean $disjunction        	
     * @return boolean
     */
    public function clearByTag($tag, $disjunction = false) {
        return $this->clearByTags([
                    $tag
                        ], $disjunction);
    }

    /**
     *
     * @param string $name        	
     * @return boolean
     */
    public function clearByName($name) {
        if ($this->storage->hasItem($name)) {
            return $this->storage->removeItem($name);
        }
        return false;
    }

    /**
     *
     * @return boolean
     */
    public function clearAll() {
        if ($this->storage instanceof FlushableInterface) {
            return $this->storage->flush();
        }
        return false;
    }

    /**
     *
     * @return Helper
     */
    public function helper() {
        if (null === $this->helper) {
            $this->helper = new Helper();
        }
        return $this->helper;
    }

    public function __call($name, $arguments) {
        if (0 == sizeof($arguments)) {
            return $this->get($name);
        } else {
            $this->set($name, $arguments[0]);
        }
    }

    /**
     *
     * @param array $tags        	
     * @return \SoloCache\Entity\Tags
     */
    public function prepareTags(array $tags = []) {
        return new Tags($tags);
    }
    
    /**
     * Remove expired items
     *
     * @return bool
     */
    public function clearExpired() {
        $this->storage->clearExpired();
    }

}
