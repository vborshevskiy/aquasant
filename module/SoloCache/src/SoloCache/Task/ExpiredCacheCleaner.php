<?php

namespace SoloCache\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloCache\Service\CacheService;

class ExpiredCacheCleaner extends AbstractReplicationTask {

    /**
     *
     * @var CacheService
     */
    protected $cacheService;

    /**
     *
     * @param CacheService $cacheService        	
     */
    public function __construct(CacheService $cacheService) {
        $this->cacheService = $cacheService;
        $this->scriptNotRespondingTimeInMinutes = 60 * 24;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SoloReplication\Service\TaskInterface::process()
     */
    public function process() {
        $this->cacheService->clearExpired();
    }

}
