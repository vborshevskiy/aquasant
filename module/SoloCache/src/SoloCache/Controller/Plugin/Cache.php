<?php

namespace SoloCache\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class Cache extends AbstractPlugin {

	/**
	 *
	 * @param string $name        	
	 * @param string $value        	
	 * @return \Zend\Cache\Storage\mixed \SoloCache\Service\CacheService
	 */
	public function __invoke($name = null, $value = null) {
		$cache = $this->getController()->getServiceLocator()->get('\\SoloCache\\Service\\CacheService');
		if (null !== $name) {
			if (null !== $value) {
				$cache->set($name, $value);
			} else {
				return $cache->get($name);
			}
		} else {
			return $cache;
		}
	}

	/**
	 *
	 * @param string $name        	
	 * @param string $arguments        	
	 * @throws \BadMethodCallException
	 * @return mixed
	 */
	public function __call($name, $arguments) {
		$cache = $this->getCache();
		if (!method_exists($cache, $name)) {
			throw new \BadMethodCallException('Invalid cache method: ' . $name);
		}
		return call_user_func_array(array(
			$cache,
			$name 
		), $arguments);
	}

}

?>