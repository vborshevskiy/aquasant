<?php

namespace SoloCache\Entity;

use Solo\Collection\Collection;

class Tags extends Collection {

	/**
	 * 
	 * @param mixed $tags
         * @param string|null $key        	
	 */
	public function add($tags, $key = null) {
		if (is_array($tags)) {
			foreach ($tags as $tag) {
				$this->add($tag);
			}
		} else {
			parent::add($tags, $tags);
		}
	}

}

?>