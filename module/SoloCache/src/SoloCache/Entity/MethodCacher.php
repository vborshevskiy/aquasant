<?php

namespace SoloCache\Entity;

use SoloCache\Service\CacheService;

class MethodCacher {

	/**
	 *
	 * @var \ReflectionMethod
	 */
	private $methodInfo;

	/**
	 *
	 * @var string
	 */
	private $cacheKey = null;

	/**
	 *
	 * @var CacheService
	 */
	private $cacheService = null;

	/**
	 *
	 * @param string $methodName        	
	 * @param array $methodArguments        	
	 * @throws \InvalidArgumentException
	 */
	public function __construct($methodName, $methodArguments) {
		if (empty($methodName)) {
			throw new \InvalidArgumentException('Method name can\'t be empty');
		}
		if (!is_array($methodArguments)) {
			throw new \InvalidArgumentException('Method arguments must be array');
		}
		$this->methodInfo = new \ReflectionMethod($methodName);
		$this->cacheKey = $this->createCacheKey($methodName, $methodArguments);
	}

	/**
	 *
	 * @param CacheService $cacheService        	
	 */
	public function setCacheService(CacheService $cacheService) {
		$this->cacheService = $cacheService;
	}

	/**
	 *
	 * @param array $arguments        	
	 * @return array
	 */
	private function createCacheKey($method, $arguments) {
		$method = str_replace(['::', '\\'], ['+', '_'], $method);
		if (0 == sizeof($arguments)) {
			return $method;
		} else {
			$parameters = [];
			$methodParameters = $this->methodInfo->getParameters();
			foreach ($methodParameters as $methodParameter) {
				$name = $methodParameter->getName();
				$argument = $arguments[$methodParameter->getPosition()];
				if (is_array($argument)) {
					$argument = implode('_', $argument);
				}
				$parameters[] = $name . '+' . $argument;
			}                     
			return $method . '_' . md5(implode('--', $parameters));
		}
	}

	/**
	 * 
	 * @throws \InvalidArgumentException
	 * @return boolean
	 */
	public function has() {
		if (null === $this->cacheService) {
			throw new \InvalidArgumentException('Cache service can\'t be null');
		}
		return $this->cacheService->has($this->cacheKey);
	}

	/**
	 * 
	 * @throws \InvalidArgumentException
	 * @return \Zend\Cache\Storage\mixed
	 */
	public function get() {
		if (null === $this->cacheService) {
			throw new \InvalidArgumentException('Cache service can\'t be null');
		}
		return $this->cacheService->get($this->cacheKey);
	}

	/**
	 * 
	 * @param mixed $content
	 * @throws \InvalidArgumentException
	 */
	public function set($content) {
		if (null === $this->cacheService) {
			throw new \InvalidArgumentException('Cache service can\'t be null');
		}
		$this->cacheService->set($this->cacheKey, $content, $this->cacheService->helper()->tagsFromMethod($this->methodInfo));
	}

}

?>