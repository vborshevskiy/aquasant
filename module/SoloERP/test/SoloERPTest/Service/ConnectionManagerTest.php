<?php
namespace SoloERPTest\Service;
use SoloERP\Service\ProvidesWebservice;

/**
 * Description of ConnectionManagerTest
 *
 * @author slava
 */
class ConnectionManagerTest extends \PHPUnit_Framework_TestCase { 
    
    /**
     *
     * @var \SoloERP\Service\ConnectionManager
     */
    protected $connectionManager = null;
    
    public function setUp() {
        $this->connectionManager = ProvidesWebservice::getStaticConnectionManager();
    }
    
    public function testConnection() {
        $connection = $this->connectionManager->getDefaultConnection();
        try {
            $connection->getFunctions();
        } catch (\SoapFault $ex) {
            $this->fail('Failed SOAP Connection to ERP');
        }
    }
    
}

?>
