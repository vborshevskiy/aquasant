<?php

return array(
    'router' => array(
        'routes' => array(
            'erpstaff' => array(
                'type' => 'Segment',
                'priority' => 1000,
                'options' => array(
                    'route' => '/erpstaff[/][:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'SoloERP\Controller\Staff',
                        'action' => 'index'
                    )
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'SoloERP\Controller\Staff' => 'SoloERP\Controller\StaffController'
        )
    )
);
