<?php

namespace SoloERP\Data\Method;

use SoloERP\Client\ClientInterface;
use SoloERP\WebService\Method\MethodInterface;
use SoloERP\WebService\Response\UltimaResponse;

class GetPreliminaryDelivery implements MethodInterface
{

    private $connection = null;

    private $values = null;

    public function __construct(ClientInterface $connection)
    {
        $this->connection = $connection;
        $this->values = array();
    }

    public function call($params = array())
    {
        foreach ($params as $key => $value) {
        	$this->addPar($key, $value);
        }
        $response = $this->connection->__call('GetPreliminaryDelivery', $this->values);
        return new UltimaResponse($response);
    }

    public function addPar($key, $par)
    {
        if (!isset($key) || !isset($par)) {
        	throw new \InvalidArgumentException('Empty property or value');
        }
        $this->values[$key] = $par;
    }

    public function removePar($key)
    {
        if (!$this->hasPar($key)) {
        	throw new \InvalidArgumentException('Unknown parameter');
        }
        unset($this->values[$key]);
    }

    public function hasPar($key)
    {
        return array_key_exists($key, $this->values);
    }

    public function getPar($key)
    {
        if ($this->hasPar($key)) {
        	return $this->values[$key];
        }
        throw new \InvalidArgumentException('Unknown parameter');
    }

    public function getPars()
    {
        return $this->values;
    }


}

