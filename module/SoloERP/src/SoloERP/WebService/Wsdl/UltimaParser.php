<?php

namespace SoloERP\WebService\Wsdl;

use Solo\WebService\Wsdl\Parser\Method;
use Solo\WebService\Wsdl\Parser;

class UltimaParser extends Parser{

	/**
	 *
	 * @var \SoapClient
	 */
	private $client;

	public function __construct($wsdl) {
		$this->client = new \SoapClient($wsdl);
	}

	/**
	 * 
	 * @return array
	 */
	public function getMethods() {
		$methods = array();
		foreach ($this->client->__getFunctions() as $func) {
			preg_match("/^[\\w]{1,}\\s([\\w]+)\\((.*)\\)$/ui", $func, $matches);
			$methodName = $matches[1];
			$methods[$methodName] = new Method($methodName);
			if (0 < strlen($matches[2])) {
				$p = explode(",", $matches[2]);
				foreach ($p as $v) {
					preg_match("#(^\\w+)\\s.*?(\\w+)$#", trim($v), $par);
					$propertyType = ($par[1] == 'UNKNOWN' ? 'array' : $par[1]);
					$propertyName = $par[2];
					$methods[$methodName]->addProperty($propertyName, $propertyType);
				}
			}
		}
		return $methods;
	}

}

?>