<?php

namespace SoloERP\WebService\Reader;

final class UltimaCsvListReader extends UltimaListReader {

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \SoloERP\WebService\Reader\UltimaListReader::getRow()
	 */
	protected function getRow($position) {
		$row = explode('||', $this->result[$position]);
		$out = [];
		for ($i = 0; $i < $this->definitionSize; $i++) {
			$out[$this->definition[$i]] = $row[$i];
		}
		return new \ArrayObject($out);
	}

}

?>