<?php

namespace SoloERP\WebService\Reader;

use SoloERP\WebService\Response\UltimaResponse;

final class UltimaJsonListReader extends UltimaListReader {

    /**
     *
     * @param UltimaResponse $response        	
     */
    public function __construct(UltimaResponse $response) {
        $this->result = $response->getData();
    }

    /**
     *
     * @return boolean
     */
    public function isEmpty() {
        return empty($this->result);
    }

    /**
     * (non-PHPdoc)
     * 
     * @see \SoloERP\WebService\Reader\UltimaListReader::getRow()
     */
    protected function getRow($position) {
        return new \ArrayObject((array) $this->result[$position]);
    }

    public function getValue($val) {
        $response = new \ArrayObject((array) $this->result);
        if (array_key_exists($val, $response)) {
            return $response[$val];
        }
        return null;
    }

}
