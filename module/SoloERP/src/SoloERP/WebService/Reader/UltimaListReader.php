<?php

namespace SoloERP\WebService\Reader;

use Solo\WebService\Reader\AbstractReader;
use SoloERP\WebService\Response\UltimaResponse;

class UltimaListReader extends AbstractReader {

	/**
	 *
	 * @var array
	 */
	protected $definition;

	/**
	 *
	 * @var integer
	 */
	protected $definitionSize;

	/**
	 *
	 * @param UltimaResponse $response        	
	 */
	public function __construct(UltimaResponse $response) {
		$data = $response->getResponse();
		if ((2 == sizeof($data)) && (2 == sizeof($data[0])) && in_array('data', $data[0]) && in_array('errorCode', $data[0])) {
			$this->definition = $data[1][0][0];
			$this->definitionSize = sizeof($this->definition);
			parent::__construct($data[1][0][1]);
		} else {
			if (2 == sizeof($data)) {
				$this->definition = $data[0];
				$this->definitionSize = sizeof($this->definition);
				parent::__construct($data[1]);
			}
		}
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \Solo\WebService\Reader\AbstractReader::getRow()
	 */
	protected function getRow($position) {
		$row = $this->result[$position];
		$out = [];
		for ($i = 0; $i < $this->definitionSize; $i++) {
			$val = $row[$i];
			if (is_string($val)) {
				$val = trim($val);
			} elseif (is_integer($val)) {
				$val = intval($val);
			} elseif (is_float($val)) {
				$val = floatval($val);
			}
			$out[$this->definition[$i]] = $val;
		}
		return new \ArrayObject($out);
	}

}

?>