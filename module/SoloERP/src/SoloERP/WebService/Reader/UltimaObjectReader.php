<?php

namespace SoloERP\WebService\Reader;

use SoloERP\WebService\Response\UltimaResponse;

class UltimaObjectReader implements \ArrayAccess {

	/**
	 *
	 * @var UltimaResponse
	 */
	private $response;

	/**
	 *
	 * @var array
	 */
	private $values = [];

	/**
	 *
	 * @var boolean
	 */
	private $initialized = false;

	/**
	 *
	 * @param UltimaResponse $response        	
	 */
	public function __construct(UltimaResponse $response) {
		$this->response = $response;
	}

	private function init() {
		$data = $this->response->getData();
		if ((2 == sizeof($data)) && (0 < sizeof($data[1]))) {
			$this->values = array_combine($data[0], $data[1][0]);
		}
		$this->initialized = true;
	}

	/**
	 *
	 * @return array
	 */
	public function toArray() {
		if (!$this->initialized) {
			$this->init();
		}
		return $this->values;
	}

	/**
	 *
	 * @param string $name        	
	 * @throws \RuntimeException
	 * @return mixed
	 */
	public function __get($name) {
		if (!$this->initialized) {
			$this->init();
		}
		if (!array_key_exists($name, $this->values)) {
			throw new \RuntimeException('Undefined property \'' . $name . '\' in ' . __CLASS__);
		}
		return $this->values[$name];
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see ArrayAccess::offsetGet()
	 */
	public function offsetGet($offset) {
		if (!$this->initialized) {
			$this->init();
		}
		if (!array_key_exists($offset, $this->values)) {
			throw new \RuntimeException('Undefined property \'' . $offset . '\' in ' . __CLASS__);
		}
		return $this->values[$offset];
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see ArrayAccess::offsetSet()
	 */
	public function offsetSet($offset, $value) {
		throw new \BadMethodCallException('Write methods disallows for ' . __CLASS__);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see ArrayAccess::offsetExists()
	 */
	public function offsetExists($offset) {
		if (!$this->initialized) {
			$this->init();
		}
		return array_key_exists($offset, $this->values);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see ArrayAccess::offsetUnset()
	 */
	public function offsetUnset($offset) {
		throw new \BadMethodCallException('Write methods disallows for ' . __CLASS__);
	}

}

?>