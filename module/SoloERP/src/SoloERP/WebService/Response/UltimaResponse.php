<?php

namespace SoloERP\WebService\Response;

use Solo\WebService\Response\Response;
use Zend\Http\Header\GenericHeader;

class UltimaResponse extends Response {

    const HTTP_STATUS_OK = 200;

    /**
     *
     * @var multitype:
     */
    private $data = null;

    /**
     *
     * @param mixed $response        	
     */
    public function setResponse($response) {
        parent::setResponse($response);
        if ($response->getStatusCode() === self::HTTP_STATUS_OK) {
            $this->data = json_decode($response->getContent());
        } else {
            $this->error = $response->getStatusCode();
        }
    }

    /**
     *
     * @return mixed
     */
    public function getData() {
        return $this->data;
    }

    /**
     *
     * @return boolean
     */
    public function hasData() {
        return (null !== $this->data);
    }
    
    /**
     * 
     * @return boolean
     */
    public function hasReadableErrorMessage() {
    	if ($this->getResponse() instanceof \Zend\Http\Response) {
	    	foreach ($this->getResponse()->getHeaders() as $header) {
	    		if ($header instanceof GenericHeader) {
	    			if ('UltimaUserReadableError' == $header->getFieldName()) {
	    				if ('true' == $header->getFieldValue()) {
	    					return true;
	    				} else {
	    					return false;
	    				}
	    			}
	    		}
	    	}
    	}
    	return false;
    }
    
    /**
     * 
     * @return string
     */
    public function getErrorMessage() {
    	$message = '';
    	if ($this->getResponse() instanceof \Zend\Http\Response) {
	    	foreach ($this->getResponse()->getHeaders() as $header) {
	    		if ($header instanceof GenericHeader) {
	    			if ('UltimaErrorText' == $header->getFieldName()) {
	    				$message = urldecode($header->getFieldValue());
	    			}
	    		}
	    	}
    	}
    	return $message;
    }

}
