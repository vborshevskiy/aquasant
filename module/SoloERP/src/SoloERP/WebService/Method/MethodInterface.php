<?php

namespace SoloERP\WebService\Method;

use Solo\WebService\Method\Response;

interface MethodInterface {

    /**
     *
     * @param array $params        	
     * @return Response
     */
    public function call($params = []);
}
