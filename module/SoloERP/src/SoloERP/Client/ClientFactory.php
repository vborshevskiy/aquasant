<?php

namespace SoloERP\Client;

use Solo\Inflector\Inflector;

abstract class ClientFactory {

    /**
     *
     * @param string $provider        	
     * @param array $options        	
     * @throws \InvalidArgumentException
     * @return ClientInterface
     */
    public static function factory($provider, $options) {
        if (empty($provider)) {
            throw new \InvalidArgumentException('Provider can\'t be empty');
        }
        if (!is_array($options)) {
            throw new \InvalidArgumentException('Options must be array');
        }
        $className = __NAMESPACE__ . '\\' . Inflector::camelize($provider) . 'Client';
        return new $className($options);
    }

}
