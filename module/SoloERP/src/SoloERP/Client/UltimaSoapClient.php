<?php

namespace SoloERP\Client;

use Zend\Soap\Client;

class UltimaSoapClient extends Client implements ClientInterface {

    private $port = null;

    use ProvidesLogger;

    /**
     *
     * @param array $options        	
     */
    public function __construct($options) {
        $wsdl = (isset($options['wsdl']) ? $options['wsdl'] : null);
        $options = (isset($options['options']) ? $options['options'] : null);
        if ($wsdl !== null) {
            $url = parse_url($wsdl);
            if (is_array($url) && array_key_exists('port', $url) && $url['port']) {
                $this->port = $url['port'];
            }
        }
        parent::__construct($wsdl, $options);
    }

    public function _doRequest(Client\Common $client, $request, $location, $action, $version, $one_way = null) {
        $parts = parse_url($location);
        if (null !== $this->port) {
            $parts['port'] = $this->port;
        }
        $location = $this->buildPath($parts);
        return parent::_doRequest($client, $request, $location, $action, $version, $one_way);
    }

    public function buildPath($parts = array()) {
        $location = '';

        if (isset($parts['scheme'])) {
            $location .= $parts['scheme'] . '://';
        }
        if (isset($parts['user']) || isset($parts['pass'])) {
            $location .= $parts['user'] . ':' . $parts['pass'] . '@';
        }
        $location .= $parts['host'];
        if (isset($parts['port'])) {
            $location .= ':' . $parts['port'];
        }
        $location .= $parts['path'];
        if (isset($parts['query'])) {
            $location .= '?' . $parts['query'];
        }

        return $location;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SoloERP\Client\ClientInterface::getProviderName()
     */
    public function getProviderName() {
        return 'UltimaSoap';
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SoloERP\Client\ClientInterface::getOption()
     */
    public function getOption($name) {
        if (empty($name)) {
            throw new \InvalidArgumentException('Name can\'t be empty');
        }
        $options = $this->getOptions();
        if (!array_key_exists($name, $options)) {
            throw new \RuntimeException('Option \'' . $name . '\' doesn\'t exists');
        }
        return $options[$name];
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Zend\Soap\Client::call()
     */
    public function call($method, $params = []) {
        if (empty($method)) {
            throw new \InvalidArgumentException('Method can\'t be empty');
        }
        if (!is_array($params)) {
            throw new \InvalidArgumentException('Parameters must be array');
        }
        if (!$this->isLoggerEmpty())
            $this->getLogger()->info('Start call method: \'' . $method . '\'');
        $response = parent::call($method, $params);
        if (!$this->isLoggerEmpty())
            $this->getLogger()->info('End call method: \'' . $method . '\'');
        return $response;
    }

}

?>