<?php

namespace SoloERP\Client;

interface ClientInterface {

    /**
     * identify client engine name
     *
     * @return string
     */
    public function getProviderName();

    /**
     * Returns option for current connection
     *
     * @param string $name        	
     * @return mixed
     */
    public function getOption($name);
}
