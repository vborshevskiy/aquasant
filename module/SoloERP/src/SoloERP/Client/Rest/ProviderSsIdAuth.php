<?php

namespace SoloERP\Client\Rest;

trait ProviderSsIdAuth {
    
    use \SoloRedis\Service\ProvidesRedis;
    
    /**
     *
     * @var \Zend\Session\Container
     */
    private $session;
    
    /**
     *
     * @var string
     */
    private $defaultSsId;
    
    /**
     *
     * @var string
     */
    private $yandexProxySsId;
    
    /**
     *
     * @var boolean
     */
    protected $isYandexProxyMethod = false;
    
    /**
     * 
     * @return string
     */
    protected function getSsId() {
        if ($this->isYandexProxyMethod) {
            return $this->getYandexProxySsId();
        }
        return $this->getDefaultSsId();
    }
    
    /**
     * 
     * @param string $ssId
     * @return string
     */
    protected function setSsId($ssId) {
        if ($this->isYandexProxyMethod) {
            return $this->setYandexProxySsId($ssId);
        }
        return $this->setDefaultSsId($ssId);
    }
    
    /**
     * 
     * @return boolean
     */
    protected function hasSsId() {
        if ($this->isYandexProxyMethod) {
            return $this->hasYandexProxySsId();
        }
        return $this->hasDefaultSsId();
    }
    
    /**
     * 
     * @param \Zend\Stdlib\MessageInterface $response
     * @return string|null
     */
    protected function getSsIdFromCookie(\Zend\Stdlib\MessageInterface $response) {
    	$result = null;
        foreach ($response->getCookie() as $cookie) {
            if ($cookie->getName() === 'UltimaAuthCookie') {
                $result = $cookie->getValue();
            }
        }
        //$data = base64_decode($result, false);
        //var_dump($data); exit();
        return $result;
    }

    /**
     * 
     * @return string
     */
    private function getDefaultSsId() {
        if (is_null($this->session)) {
            $this->session = new \Zend\Session\Container('erp_ssid');
        }
        if (is_null($this->defaultSsId)) {
            $this->defaultSsId = $this->session['ssId'];
        }
        return $this->defaultSsId;
    }

    /**
     * 
     * @param string $ssId
     * @return string
     */
    private function setDefaultSsId($ssId) {
        if (is_null($this->session)) {
            $this->session = new \Zend\Session\Container('erp_ssid');
        }
        $this->session['ssId'] = $ssId;
        $this->defaultSsId = $ssId;
        return $ssId;
    }
    
    /**
     * 
     * @return boolean
     */
    private function hasDefaultSsId() {
        if (is_null($this->session)) {
            $this->session = new \Zend\Session\Container('erp_ssid');
        }
        if (is_null($this->defaultSsId)) {
            $this->defaultSsId = $this->session['ssId'];
        }
        return !!$this->defaultSsId;
    }
    
    /**
     * 
     * @return string
     */
    private function getYandexProxySsId() {
        if (is_null($this->yandexProxySsId)) {
            $this->yandexProxySsId = $this->redis()->get('yandexProxySsId');
        }
        return $this->yandexProxySsId;
    }

    /**
     * 
     * @param string $ssId
     * @return string
     */
    private function setYandexProxySsId($ssId) {
        $this->redis()->set('yandexProxySsId', $ssId);
        $this->yandexProxySsId = $ssId;
        return $ssId;
    }
    
    /**
     * 
     * @return boolean
     */
    private function hasYandexProxySsId() {
        if (is_null($this->yandexProxySsId)) {
            $this->yandexProxySsId = $this->redis()->get('yandexProxySsId');
        }
        return !!$this->yandexProxySsId;
    }
    
}
