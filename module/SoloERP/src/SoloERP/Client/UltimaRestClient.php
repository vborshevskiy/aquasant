<?php

namespace SoloERP\Client;

use Zend\Http\Client;

class UltimaRestClient extends Client implements ClientInterface {

    use ProvidesLogger;
    use Rest\ProviderSsIdAuth;
    
    /**
     *
     * @var string
     */
    private $path = null;
    
    /**
     *
     * @var string
     */
    private $login = null;
    
    /**
     *
     * @var string
     */
    private $pass = null;
    
    /**
     *
     * @var boolean
     */
    private $lastAttempt = false;
    
    /**
     *
     * @var boolean
     */
    private $useSsId = true;

    /**
     *
     * @param array $config        	
     */
    public function __construct($config) {
        if (isset($config['path'])) {
            $this->path = $config['path'];
        }
        if (isset($config['login'])) {
            $this->login = $config['login'];
        }
        if (isset($config['pass'])) {
            $this->pass = $config['pass'];
        }
        $config = (isset($config['config']) ? $config['config'] : null);
        if (!is_null($config) && !empty($config)) {
            $this->setOptions($config);
        }
        parent::__construct(null, $config);
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SoloERP\Client\ClientInterface::getProviderName()
     */
    public function getProviderName() {
        return 'UltimaRest';
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SoloERP\Client\ClientInterface::getConfig()
     */
    public function getOption($name) {
        if (empty($name)) {
            throw new \InvalidArgumentException('Name can\'t be empty');
        }
        $config = $this->getConfig();
        if (!array_key_exists($name, $config)) {
            throw new \RuntimeException('Config option \'' . $name . '\' doesn\'t exists');
        }
        return $config[$name];
    }

    /**
     * (non-PHPdoc)
     *
     * @see \Zend\Http\Client::send()
     */
    public function __call($method, $params = []) {
        if (empty($method)) {
            throw new \InvalidArgumentException('Method can\'t be empty');
        }
        if (!is_array($params)) {
            throw new \InvalidArgumentException('Parameters must be array');
        }
        if (empty($this->path)) {
            throw new \InvalidArgumentException('Path can\'t be empty');
        }
        $this->isYandexProxyMethod = $method === 'ProcessYandexMarketOrder';
        
        $this->setAdapter('Zend\Http\Client\Adapter\Curl');
        $this->setUri($this->path . $method);
        if ($this->useSsId && $this->hasSsId()) {
            if (!$this->isLoggerEmpty()) {
                $this->getLogger()->info('Method: ' . $method . ', ssid: ' . $this->getSsId() . ', yandexProxy: ' . (int)$this->isYandexProxyMethod);
            }
            $this->addCookie('ss-id', $this->getSsId());
            $this->addCookie('UltimaAuthCookie', $this->getSsId());
            //mail('rgoryanin@it-solo.com', 'santbaza: '.$method, $this->getSsId());
        } elseif (!is_null($this->login) || !is_null($this->pass)) {
            if (!$this->isLoggerEmpty()) {
                $this->getLogger()->info('Method: ' . $method . ', auth: ' . $this->login . '::' . $this->pass . ', yandexProxy: ' . (int)$this->isYandexProxyMethod);
            }
            $this->setAuth($this->login, $this->pass);
        }
        if (count($params) > 0) {
            $this->getRequest()->setContent(json_encode($params));
            $this->setMethod(\Zend\Http\Request::METHOD_POST);
            $this->getRequest()->getHeaders()->addHeaders(array(
                'Content-Type: application/json',
                'Content-Length: ' .  strlen(json_encode($params)),
            ));
        }
        try {
            $response = $this->send();
            if ($response->getStatusCode() === \Zend\Http\PhpEnvironment\Response::STATUS_CODE_401 || $response->getStatusCode() === \Zend\Http\PhpEnvironment\Response::STATUS_CODE_500) {
                if ($this->useSsId && !$this->lastAttempt) {
                    $this->lastAttempt = true;
                    $this->useSsId = false;
                    return $this->__call($method, $params);
                }
            }
            if ((!$this->useSsId || !$this->hasSsId()) && $response->getCookie() && $response->getStatusCode() === \Zend\Http\PhpEnvironment\Response::STATUS_CODE_200) {
                $ssId = $this->getSsIdFromCookie($response);
                //mail('rgoryanin@it-solo.com', 'santbaza: first extract ss id', $ssId);
                $this->setSsId($ssId);
            }
            $ssId = $this->getSsIdFromCookie($response);
            $this->setSsId($ssId);
        } catch (\Exception $e) {
            throw new \Exception('Error sending a request to the ERP', 500);
        }
        return $response;
    }

}
