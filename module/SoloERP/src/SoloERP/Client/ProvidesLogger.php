<?php

namespace SoloERP\Client;

use Zend\Log\Logger;

trait ProvidesLogger {

    /**
     *
     * @var Logger
     */
    private $logger = null;

    /**
     * Sets logger instance
     *
     * @param Logger $logger        	
     */
    public function setLogger(Logger $logger) {
        $this->logger = $logger;
    }

    /**
     * Checks is logger instance has inialized
     *
     * @return boolean
     */
    public function isLoggerEmpty() {
        return (null === $this->logger);
    }

    /**
     * Gets logger instance
     *
     * @throws \RuntimeException
     * @return \Zend\Log\Logger
     */
    public function getLogger() {
        if ($this->isLoggerEmpty()) {
            throw new \RuntimeException('Logger instance not inialized');
        }
        return $this->logger;
    }

}
