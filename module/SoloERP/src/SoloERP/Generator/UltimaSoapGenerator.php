<?php

namespace SoloERP\Generator;

use SoloERP\WebService\Wsdl\UltimaParser;
use Zend\Code\Generator\FileGenerator;
use Zend\Code\Generator\ClassGenerator;
use Zend\Code\Generator\PropertyGenerator;
use Zend\Code\Generator\MethodGenerator;
use Zend\Code\Generator\ParameterGenerator;
use Solo\Inflector\Inflector;
use SoloERP\Client\ClientInterface;

class UltimaSoapGenerator implements GeneratorInterface {

	/**
	 *
	 * @var Parser
	 */
	private $parser;

	/**
	 *
	 * @param ClientInterface $connection        	
	 */
	public function __construct(ClientInterface $connection) {
		$this->parser = new UltimaParser($connection->getOption('wsdl'));
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \SoloERP\Generator\GeneratorInterface::writeMethods()
	 */
	public function writeMethods($savePath, $namespace) {
		if (empty($savePath)) {
			throw new \InvalidArgumentException('Save path can\'t be empty');
		}
		if (empty($namespace)) {
			throw new \InvalidArgumentException('Namespace can\'t be empty');
		}
		
		$methods = $this->parser->getMethods();
		foreach ($methods as $method) {
			$file = new FileGenerator();
			$file->setNamespace($namespace);
			$file->setFilename($method->getName());
			$file->setUse('SoloERP\\Client\\ClientInterface');
			$file->setUse('SoloERP\\WebService\\Method\\MethodInterface');
//			if ($method->hasProperties()) {
//				$file->setUse('Solo\\WebService\\Method\\ParamValidator');
//				$file->setUse('Solo\\Inflector\\Inflector');
//			}
			$file->setUse('SoloERP\\WebService\\Response\\UltimaResponse');
			
			$class = new ClassGenerator();
			$class->setFinal(true);
			$class->setName($method->getName());
			$class->setImplementedInterfaces([
				'MethodInterface' 
			]);
			
			$clientProperty = new PropertyGenerator();
			$clientProperty->setFlags($clientProperty::FLAG_PRIVATE);
			$clientProperty->setName('connection');
			$class->addPropertyFromGenerator($clientProperty);
			
			$valuesProperty = new PropertyGenerator();
			$valuesProperty->setFlags($valuesProperty::FLAG_PRIVATE);
			$valuesProperty->setName('values');
			$class->addPropertyFromGenerator($valuesProperty);
			
			// constructor
			$constructor = new MethodGenerator();
			$constructor->setFlags($constructor::FLAG_PUBLIC);
			$constructor->setName('__construct');
			$constructor->setParameter(new ParameterGenerator('connection', 'ClientInterface'));
			$values = [];
//			foreach ($method->getProperties() as $property) {
//				$values[] = '\'' . $property->getName() . '\' => null';
//			}
			$constructor->setBody(implode("\n", [
				'$this->connection = $connection;',
				'$this->values = array(' . implode(', ', $values) . ');' 
			]));
			$class->addMethodFromGenerator($constructor);
			
//			foreach ($method->getProperties() as $property) {
//				$setMethod = new MethodGenerator();
//				$setMethod->setFlags($setMethod::FLAG_PUBLIC);
//				$setMethod->setName('set' . Inflector::camelize($property->getName()));
//				$setMethod->setParameter(new ParameterGenerator($property->getName()));
//				$setMethod->setBody(implode("\n", [
//					'if ((null !== $' . $property->getName() . ') && (!ParamValidator::isValid(\'' . $property->getType() . '\', $' . $property->getName() . '))) {',
//					"\t" . 'throw new \\InvalidArgumentException(\'Invalid type for value: ' . $property->getName() . '\');',
//					'}',
//					'$this->values[\'' . $property->getName() . '\'] = $' . $property->getName() . ';' 
//				]));
//				$class->addMethodFromGenerator($setMethod);
//				
//				$getMethod = new MethodGenerator();
//				$getMethod->setFlags($getMethod::FLAG_PUBLIC);
//				$getMethod->setName('get' . Inflector::camelize($property->getName()));
//				$getMethod->setBody('return $this->values[\'' . $property->getName() . '\'];');
//				$class->addMethodFromGenerator($getMethod);
//			}
			
			$callMethod = new MethodGenerator();
			$callMethod->setFlags($callMethod::FLAG_PUBLIC);
			$callMethod->setName('call');
			$callMethod->setParameter(new ParameterGenerator('params', null, array()));
			$body = [];
			if ($method->hasProperties()) {
				$availableProperties = [];
				foreach ($method->getProperties() as $property) {
					$availableProperties[] = $property->getName();
				}
				$body[] = 'foreach ($params as $key => $value) {';
				$body[] = "\t" . '$this->addPar($key, $value);';
				$body[] = '}';
			}
			$body[] = '$response = $this->connection->__call(\'' . $method->getName() . '\', $this->values);';
			$body[] = 'return new UltimaResponse($response);';
			$callMethod->setBody(implode("\n", $body));
			$class->addMethodFromGenerator($callMethod);
            
            $callMethod = new MethodGenerator();
			$callMethod->setFlags($callMethod::FLAG_PUBLIC);
			$callMethod->setName('addPar');
            $callMethod->setParameter(new ParameterGenerator('key', 'string'));
			$callMethod->setParameter(new ParameterGenerator('par'));
			$body = [];
			$body[] = 'if (!isset($key) || !isset($par)) {';
            $body[] = "\t" . 'throw new \InvalidArgumentException(\'Empty property or value\');';
            $body[] = '}';
            $body[] = '$this->values[$key] = $par;';
			$callMethod->setBody(implode("\n", $body));
			$class->addMethodFromGenerator($callMethod);
            
            $callMethod = new MethodGenerator();
			$callMethod->setFlags($callMethod::FLAG_PUBLIC);
			$callMethod->setName('removePar');
            $callMethod->setParameter(new ParameterGenerator('key', 'string'));
			$body = [];
			$body[] = 'if (!$this->hasPar($key)) {';
            $body[] = "\t" . 'throw new \InvalidArgumentException(\'Unknown parameter\');';
            $body[] = '}';
            $body[] = 'unset($this->values[$key]);';
			$callMethod->setBody(implode("\n", $body));
			$class->addMethodFromGenerator($callMethod);
            
            $callMethod = new MethodGenerator();
			$callMethod->setFlags($callMethod::FLAG_PUBLIC);
			$callMethod->setName('hasPar');
            $callMethod->setParameter(new ParameterGenerator('key', 'string'));
			$body = [];
            $body[] = 'return array_key_exists($key, $this->values);';
			$callMethod->setBody(implode("\n", $body));
			$class->addMethodFromGenerator($callMethod);
            
            $callMethod = new MethodGenerator();
			$callMethod->setFlags($callMethod::FLAG_PUBLIC);
			$callMethod->setName('getPar');
            $callMethod->setParameter(new ParameterGenerator('key', 'string'));
			$body = [];
            $body[] = 'if ($this->hasPar($key)) {';
            $body[] = "\t" . 'return $this->values[$key];';
            $body[] = '}';
            $body[] = 'throw new \InvalidArgumentException(\'Unknown parameter\');';
			$callMethod->setBody(implode("\n", $body));
			$class->addMethodFromGenerator($callMethod);
            
            $callMethod = new MethodGenerator();
			$callMethod->setFlags($callMethod::FLAG_PUBLIC);
			$callMethod->setName('getPars');
			$body = [];
            $body[] = 'return $this->values;';
			$callMethod->setBody(implode("\n", $body));
			$class->addMethodFromGenerator($callMethod);
			
			$file->setClass($class);
			
			$content = $file->generate();
			file_put_contents($savePath . '/' . $file->getFilename() . '.php', $content);
		}
		return sizeof($methods);
	}

}

?>
