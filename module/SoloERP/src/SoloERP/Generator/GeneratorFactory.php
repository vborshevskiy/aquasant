<?php

namespace SoloERP\Generator;

use SoloERP\Client\ClientInterface;

abstract class GeneratorFactory {

	/**
	 *
	 *
	 *
	 * Create generator instance
	 *
	 * @param string $name        	
	 * @param ClientInterface $connection        	
	 * @throws \InvalidArgumentException
	 * @return GeneratorInterface
	 */
	public static function factory($name, ClientInterface $connection) {
		if (empty($name)) {
			throw new \InvalidArgumentException('Name can\'t be empty');
		}
		$className = __NAMESPACE__ . '\\' . $connection->getProviderName() . 'Generator';
		return new $className($connection);
	}

}

?>