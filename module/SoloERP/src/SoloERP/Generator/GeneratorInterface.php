<?php

namespace SoloERP\Generator;

use SoloERP\Client\ClientInterface;

interface GeneratorInterface {

	/**
	 *
	 * @param ClientInterface $connection        	
	 */
	public function __construct(ClientInterface $connection);

	/**
	 * Generate and write method wrappers in specified folder
	 * Returns count of generated methods
	 *
	 * @param string $savePath        	
	 * @param string $namespace        	
	 * @return integer
	 */
	public function writeMethods($savePath, $namespace);

}

?>