<?php

namespace SoloERP\Controller;

use Solo\Mvc\Controller\ActionController;
use Solo\Inflector\Inflector;
use SoloERP\Generator\GeneratorFactory;
use Solo\Io\Directory;
use SoloERP\Service\ProvidesWebservice;

class StaffController extends ActionController {

	public function indexAction() {
		print 'nothing';
		exit();
	}

	public function generateAction() {
		$connectionManager = ProvidesWebservice::getStaticConnectionManager();
        if ($connectionManager->hasConnection('webMethodsGenerator')) {
            if ($connectionManager->hasMethodsDefaultDir()) {
                $namespace = $connectionManager->getMethodsDefaultDirNamespace();
                $savePath = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Data' . DIRECTORY_SEPARATOR . Inflector::camelize(Inflector::singularize($connectionManager->getMethodsDefaultDir()));
            } else {
                $namespace = $connectionManager->getConnectionNamespace('webMethodsGenerator');
                $savePath = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Data' . DIRECTORY_SEPARATOR . Inflector::camelize(Inflector::singularize('webMethodsGenerator'));
            }
            $this->createWebMethods('webMethodsGenerator', $connectionManager->getConnection('webMethodsGenerator'), $namespace, $savePath);
        } else {
            foreach ($connectionManager as $connectionName => $connection) {
                if ($connectionManager->hasMethodsDefaultDir()) {
                    $namespace = $connectionManager->getMethodsDefaultDirNamespace();
                    $savePath = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Data' . DIRECTORY_SEPARATOR . Inflector::camelize(Inflector::singularize($connectionManager->hasMethodsDefaultDir()));
                } else {
                    $savePath = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Data' . DIRECTORY_SEPARATOR . Inflector::camelize(Inflector::singularize($connectionName));
                }
                $this->createWebMethods($connectionName, $connection, $namespace, $savePath);
            }
        }
		exit();
	}
    
    private function createWebMethods($connectionName, $connection, $namespace, $savePath) {
        Directory::remove($savePath);
        Directory::create($savePath, 0777, true);

        $generator = GeneratorFactory::factory($connectionName, $connection);
        $countMethods = $generator->writeMethods($savePath, $namespace);
        print $connectionName . '<br />';
        print 'Total generated = ' . $countMethods . '<br /><br />';
    }

}
