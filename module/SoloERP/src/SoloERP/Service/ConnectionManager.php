<?php

namespace SoloERP\Service;

use SoloERP\Client\ClientInterface;
use Solo\Stdlib\Exception\NotSupportedException;
use Solo\Inflector\Inflector;

final class ConnectionManager implements \Iterator, \ArrayAccess {

	/**
	 *
	 * @var string
	 */
	private $defaultConnectionName = null;
    
    /**
	 *
	 * @var string
	 */
	private $defaultDir = null;
    
    /**
	 *
	 * @var string
	 */
	private $defaultDirNamespace = null;

	/**
	 *
	 * @var array
	 */
	private $connections = [];

	/**
	 *
	 * @param string $name        	
	 * @param ClientInterface $connection        	
	 * @throws \InvalidArgumentException
	 */
	public function setConnection($name, ClientInterface $connection) {
		if (empty($name)) {
			throw new \InvalidArgumentException('Name can\'t be empty');
		}
		$this->connections[$name] = $connection;
	}

	/**
	 * Returns connection instance by name
	 *
	 * @param string $name        	
	 * @throws \RuntimeException
	 * @return ClientInterface
	 */
	public function getConnection($name) {
		if (empty($name) && $this->hasDefaultConnection()) {
			return $this->getDefaultConnection();
		}
		if (!array_key_exists($name, $this->connections)) {
			throw new \RuntimeException('Specifed connection ' . $name . ' doesn\' exists');
		}
		return $this->connections[$name];
	}
    
    /**
	 * Checks is connection specified
	 *
	 * @param string $name        	
	 * @return boolean
	 */
	public function hasConnection($name) {
		return (array_key_exists($name, $this->connections));
	}

	/**
	 *
	 * @param string $name        	
	 * @throws \InvalidArgumentException
	 * @return boolean
	 */
	public function removeConnection($name) {
		if (empty($name)) {
			throw new \InvalidArgumentException('Name can\'t be empty');
		}
		if (array_key_exists($name, $this->connections)) {
			unset($this->connections[$name]);
			return true;
		}
		return false;
	}

	/**
	 * Initialize default connection by name from already added connections in manager
	 *
	 * @param string $name        	
	 * @throws \InvalidArgumentException
	 * @throws \RuntimeException
	 */
	public function setDefaultConnection($name) {
		if (empty($name)) {
			throw new \InvalidArgumentException('Name can\'t be empty');
		}
		if (!array_key_exists($name, $this->connections)) {
			throw new \RuntimeException('Specified connection ' . $name . ' doesn\' exists');
		}
		$this->defaultConnectionName = $name;
	}
    
    /**
	 * Initialize default methods directory
	 *
	 * @param string $dir        	
	 * @throws \InvalidArgumentException
     * @return ClientInterface
	 */
	public function setMethodsDefaultDir($dir) {
		if (empty($dir)) {
			throw new \InvalidArgumentException('Default dir can\'t be empty');
		}
		$this->defaultDir = $dir;
        return $this;
	}
    
    /**
	 *
	 * @throws \RuntimeException
	 * @return string
	 */
	public function getMethodsDefaultDir() {
		if (!$this->hasMethodsDefaultDir()) {
			throw new \RuntimeException('Default dir doesn\'t exists');
		}
		return $this->defaultDir;
	}
    
    /**
	 * Initialize default methods directory namespace
	 *
	 * @param string $namespace        	
	 * @throws \InvalidArgumentException
     * @return ClientInterface
	 */
	public function setMethodsDefaultDirNamespace($namespace) {
		if (empty($namespace)) {
			throw new \InvalidArgumentException('Default dir namespace can\'t be empty');
		}
		$this->defaultDirNamespace = $namespace;
        return $this;
	}
    
    /**
	 *
	 * @throws \RuntimeException
	 * @return string
	 */
	public function getMethodsDefaultDirNamespace() {
		if ($this->defaultDirNamespace === null) {
			throw new \RuntimeException('Default dir namespace doesn\'t exists');
		}
		return $this->defaultDirNamespace;
	}
    
    /**
	 * Checks is default methods directory exists
	 *
	 * @return boolean
	 */
	public function hasMethodsDefaultDir() {
		return (null !== $this->defaultDir);
	}

	/**
	 * Checks is default connection specified
	 *
	 * @return boolean
	 */
	public function hasDefaultConnection() {
		return (null !== $this->defaultConnectionName);
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return ClientInterface
	 */
	public function getDefaultConnection() {
		if (!$this->hasDefaultConnection()) {
			throw new \RuntimeException('Default connection doesn\' specified');
		}
		return $this->connections[$this->defaultConnectionName];
	}

	/**
	 * Gets namespace for specified connection name
	 * uses for generating web methods classes
	 *
	 * @param string $name        	
	 * @throws \InvalidArgumentException
	 * @return string
	 */
	public function getConnectionNamespace($name) {
		if (empty($name) && $this->hasDefaultConnection()) {
			$name = $this->defaultConnectionName;
		}
		return 'SoloERP\\Data\\' . Inflector::camelize(Inflector::singularize($name));
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Iterator::rewind()
	 */
	public function rewind() {
		reset($this->connections);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Iterator::current()
	 */
	public function current() {
		return current($this->connections);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Iterator::key()
	 */
	public function key() {
		return key($this->connections);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Iterator::next()
	 */
	public function next() {
		next($this->connections);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Iterator::valid()
	 */
	public function valid() {
		return isset($this->connections[key($this->connections)]);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see ArrayAccess::offsetGet()
	 */
	public function offsetGet($offset) {
		return $this->getConnection($offset);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see ArrayAccess::offsetSet()
	 */
	public function offsetSet($offset, $value) {
		throw new NotSupportedException(__CLASS__ . '::' . __METHOD__ . ' not supported');
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see ArrayAccess::offsetExists()
	 */
	public function offsetExists($offset) {
		return array_key_exists($offset, $this->connections);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see ArrayAccess::offsetUnset()
	 */
	public function offsetUnset($offset) {
		return $this->removeConnection($offset);
	}

}

?>