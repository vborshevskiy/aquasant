<?php

namespace SoloERP\Service;

trait ProvidesWebservice {

    /**
     * ConnectionManager instance with highest priority
     *
     * @var ConnectionManager
     */
    private $connectionManager = null;

    /**
     * ConnectionManager instance with lowest priority
     *
     * @var ConnectionManager
     */
    private static $staticConnectionManager = null;

    /**
     * Connection name in ConnectionManager for default using
     *
     * @var string
     */
    private $defaultConnectionName = null;

    /**
     * Sets ConnectionManager instance for use in highest priority
     *
     * @param ConnectionManager $connectionManager        	
     */
    public function setConnectionManager(ConnectionManager $connectionManager) {
        $this->connectionManager = $connectionManager;
    }

    /**
     * Sets ConnectionManager instance for use in lowest priority
     *
     * @param ConnectionManager $connectionManager        	
     */
    public static function setStaticConnectionManager(ConnectionManager $connectionManager) {
        self::$staticConnectionManager = $connectionManager;
    }

    /**
     * Returns ConnectionManager instance fro use in lowest priority
     *
     * @return \SoloERP\Service\ConnectionManager
     */
    public static function getStaticConnectionManager() {
        return self::$staticConnectionManager;
    }

    /**
     * Sets default connection name in ConnectionManager for default using
     *
     * @param string $name        	
     */
    public function setDefaultConnectionName($name) {
        $this->defaultConnectionName = $name;
    }

    /**
     * Gets ConnectionManager instance using instances priorities
     *
     * @return ConnectionManager NULL
     */
    protected function getConnectionManager() {
        if (null !== $this->connectionManager) {
            return $this->connectionManager;
        }
        if (null !== self::$staticConnectionManager) {
            return self::$staticConnectionManager;
        }
        return null;
    }

    /**
     *
     * @param string $name        	
     * @param string $connectionName        	
     * @throws \RuntimeException
     * @return SoloERP\WebService\Method\MethodInterface
     */
    protected function getWebMethod($name, $connectionName = null) {
        if ((null === $connectionName) && (null !== $this->defaultConnectionName)) {
            $connectionName = $this->defaultConnectionName;
        }
        $connection = $this->getConnectionManager()->getConnection($connectionName);
        if (null === $connection) {
            throw new \RuntimeException('Failed to get connection \'' . $connectionName . '\'');
        }
        if ($this->getConnectionManager()->hasMethodsDefaultDir()) {
            $namespace = $this->getConnectionManager()->getMethodsDefaultDirNamespace();
        } else {
            $namespace = $this->getConnectionManager()->getConnectionNamespace($connectionName);
        }
        $methodClass = $namespace . '\\' . $name;
        $method = new $methodClass($connection);
        return $method;
    }

    /**
     *
     * @param string $name        	
     * @param array $params        	
     * @param string $connectionName        	
     * @return Solo\WebService\Response\Response
     */
    protected function callWebMethod($name, $params = [], $connectionName = null) {
        return $this->getWebMethod($name, $connectionName)->call($params);
    }

}

?>