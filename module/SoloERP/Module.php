<?php

namespace SoloERP;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Solo\ModuleManager\MultiConfigModule;
use Zend\Mvc\MvcEvent;
use SoloERP\Service\ConnectionManager;
use SoloERP\Client\ClientFactory;
use SoloERP\Service\ProvidesWebservice;

class Module extends MultiConfigModule implements AutoloaderProviderInterface {

    public function __construct() {
        parent::__construct(__DIR__);
    }

    public function getAutoloaderConfig() {
        return [
            'Zend\Loader\ClassMapAutoloader' => [
                __DIR__ . DIRECTORY_SEPARATOR . 'autoload_classmap.php'
            ],
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . str_replace('\\', '/', __NAMESPACE__)
                ]
            ]
        ];
    }

    public function onBootstrap(MvcEvent $e) {
        $this->loadConnectionManager($e);
    }

    /**
     *
     * @todo move connection manager inizializing in config
     * @param MvcEvent $e        	
     */
    private function loadConnectionManager(MvcEvent $e) {
        $sm = $e->getApplication()->getServiceManager();
        $config = $sm->get('Config');
        $connectionManager = new ConnectionManager();
        
        $logManager = $sm->get('SoloLog\\Service\\LogManager');
        $logger = $logManager->create('ws');
        
        if (isset($config['solo_erp'])) {
            if (isset($config['solo_erp']['connections'])) {
                foreach ($config['solo_erp']['connections'] as $connectionName => $connectionParams) {
                    $connection = ClientFactory::factory($connectionParams['provider'], $connectionParams['options']);
                    $connection->setLogger($logger);
                    $connectionManager->setConnection($connectionName, $connection);
                }
            }
            if (isset($config['solo_erp']['default'])) {
                $connectionManager->setDefaultConnection($config['solo_erp']['default']);
            }
            if (isset($config['solo_erp']['default_dir'])) {
                $connectionManager->setMethodsDefaultDir($config['solo_erp']['default_dir']);
                $connectionManager->setMethodsDefaultDirNamespace(__NAMESPACE__ . '\\' . 'Data' . '\\' . $config['solo_erp']['default_dir']);
            }
        }
        ProvidesWebservice::setStaticConnectionManager($connectionManager);
    }

}
