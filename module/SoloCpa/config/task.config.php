<?php
return [
	'tasks' => [
		'repl_cpa_campaigns' => 'SoloCpa\Task\Factory\CampaignsReplicatorFactory',
		'cpa_reserves_statuses_update' => 'SoloCpa\Task\Factory\ReservesStatusesUpdaterFactory',
		'cpa_reserves_statuses_sync' => [
			'class' => 'SoloCpa\Task\Factory\ReservesStatusesSynchronizerFactory',
			'options' => [
				'sync_done_reserves_in_days' => 7 
			] 
		] 
	],
	'task_plugins' => [
		'invokables' => [
			'cpa' => 'SoloCpa\Task\Plugin\CpaPlugin' 
		] 
	],
	'console' => [
		'router' => [
			'routes' => [
				'cpa_reserves_statuses_sync' => [
					'options' => [
						'route' => 'tasks cpa_reserves_statuses_sync [--reserves=]',
						'defaults' => [
							'controller' => 'SoloTasks\Controller\Tasks',
							'action' => 'runConsole',
							'name' => 'cpa_reserves_statuses_sync' 
						] 
					] 
				] 
			] 
		] 
	] 
];