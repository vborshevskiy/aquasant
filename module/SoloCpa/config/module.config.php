<?php
return [
	'router' => [
		'routes' => [
			'cpa' => [
				'type' => 'Segment',
				'priority' => 1000,
				'options' => [
					'route' => '/api/cpa/[:action]/',
					'constraints' => [
						'action' => '[0-9a-zA-Z_]*' 
					],
					'defaults' => [
						'controller' => 'SoloCpa\Controller\Cpa',
						'action' => 'index' 
					] 
				] 
			] 
		] 
	],
	'console' => [
		'router' => [
			'routes' => [
				'cpa' => [
					'options' => [
						'route' => 'cpa [<action>]',
						'defaults' => [
							'controller' => 'SoloCpa\Controller\Cpa' 
						] 
					] 
				]
			] 
		] 
	],
	'controllers' => [
		'invokables' => [
			'SoloCpa\Controller\Cpa' => 'SoloCpa\Controller\CpaController' 
		] 
	],
	'controller_plugins' => [
		'invokables' => [
			'cpa' => 'SoloCpa\Controller\Plugin\CpaPlugin' 
		] 
	],
	'service_manager' => [
		'factories' => [
			'SoloCpa\Data\CampaignSourcesTable' => 'SoloCpa\Data\MySql\Factory\CampaignSourcesTableFactory',
			'SoloCpa\Data\CampaignSourcesReplicationTable' => 'SoloCpa\Data\MySql\Factory\CampaignSourcesReplicationTableFactory',
			'SoloCpa\Data\CpaReservesTable' => 'SoloCpa\Data\MySql\Factory\CpaReservesTableFactory',
			
			'SoloCpa\Service\CpaService' => 'SoloCpa\Service\Factory\CpaServiceFactory',
			'SoloCpa\Service\TrackerService' => 'SoloCpa\Service\Factory\TrackerServiceFactory' 
		],
		'invokables' => [
			'SoloCpa\Entity\CampaignSource' => 'SoloCpa\Entity\CampaignSource',
			'SoloCpa\Entity\CpaReserve' => 'SoloCpa\Entity\CpaReserve' 
		],
		'shared' => [
			'SoloCpa\Entity\CampaignSource' => false,
			'SoloCpa\Entity\CpaReserve' => false 
		] 
	] 
];
