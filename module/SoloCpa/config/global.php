<?php
return [
	'solo-cpa' => [
		'cookie_expired' => '180d',
		'partners' => [
			'actionpay' => [
				'campaignId' => 101,
				'className' => 'SoloCpa\Service\Provider\Actionpay',
				'trackMode' => 'postback',
				'options' => [
					'code' => 5814,
					'multiplier' => 1,
					'secret_key' => 'sfw342basd6HGjgasd1',
					'postback_key' => '1ILnFOjxuRB6Pc2b',
					'ip' => '138.201.201.49' 
				],
				'enabled' => true 
			],
			'pandapay' => [
				'campaignId' => 999,
				'className' => 'SoloCpa\Service\Provider\Pandapay',
				'trackMode' => 'postback',
				'options' => [
					'code' => 5814 
				],
				'enabled' => true 
			],
			'admitad' => [
				'campaignId' => 81,
				'className' => 'SoloCpa\Service\Provider\Admitad',
				'trackMode' => 'postback',
				'options' => [
					'code' => '8f1d03ca20',
					'postback_key' => '0A3b4AfaBd4cA5ca76b398180A2D9557' 
				],
				'enabled' => true 
			],
			'postview' => [
				'campaignId' => 81,
				'utm_source' => 'postview',
				'className' => 'SoloCpa\Service\Provider\Admitad',
				'trackMode' => 'postback',
				'options' => [
					'code' => '8f1d03ca20',
					'postback_key' => '0A3b4AfaBd4cA5ca76b398180A2D9557' 
				],
				'enabled' => true 
			],
			'criteo' => [
				'campaignId' => 161,
				'trackMode' => 'none',
				'className' => 'SoloCpa\Service\Provider\Criteo',
				'enabled' => true 
			],
			'gdeslon' => [
				'campaignId' => 301,
				'className' => 'SoloCpa\Service\Provider\GdeSlon',
				'trackMode' => 'report',
				'options' => [
					'merchant_id' => 70597 
				],
				'enabled' => true 
			],
			'mixmarket' => [
				'campaignId' => 242,
				'className' => 'SoloCpa\Service\Provider\MixMarket',
				'trackMode' => 'postback',
				'options' => [
					'password' => 'XaEYy3KHdz',
					'new_reserves' => [
						'id' => '1294939191',
						'hash' => '591a46fbd10d4061489ae254eae5f75b' 
					],
					'paid_reserves' => [
						'id' => '1294939191',
						'hash' => '591a46fbd10d4061489ae254eae5f75b' 
					],
					'cancelled_reserves' => [
						'id' => '1294939190',
						'hash' => '631173de8d7ee535e56464f16e047945' 
					] 
				],
				'enabled' => false 
			],
			'cityads' => [
				'campaignId' => 221,
				'trackMode' => 'postback',
				'className' => 'SoloCpa\Service\Provider\CityAds',
				'options' => [
					'campaign' => 23865 
				],
				'enabled' => true 
			],
			'yandex' => [
				'campaignId' => 282,
				'trackMode' => 'none',
				'className' => 'SoloCpa\Service\Provider\YandexOrganic',
				'enabled' => true 
			],
			'google' => [
				'campaignId' => 283,
				'trackMode' => 'none',
				'className' => 'SoloCpa\Service\Provider\GoogleOrganic',
				'enabled' => true 
			],
			'mailru' => [
				'campaignId' => 284,
				'trackMode' => 'none',
				'className' => 'SoloCpa\Service\Provider\MailruOrganic',
				'enabled' => true 
			],
			'typein' => [
				'campaignId' => 281,
				'trackMode' => 'none',
				'className' => 'SoloCpa\Service\Provider\TypeIn',
				'enabled' => true 
			],
			'ad1' => [
				'campaignId' => 341,
				'trackMode' => 'none',
				'className' => 'SoloCpa\Service\Provider\Ad1',
				'enabled' => false 
			],
			'market-cpc' => [
				'campaignId' => 1,
				'trackMode' => 'none',
				'className' => 'SoloCpa\Service\Provider\MarketCpc',
				'enabled' => true 
			],
			'market-cpa' => [
				'campaignId' => 361,
				'trackMode' => 'none',
				'className' => 'SoloCpa\Service\Provider\MarketCpa',
				'enabled' => true 
			],
			'linkprofit' => [
				'campaignId' => 401,
				'className' => 'SoloCpa\Service\Provider\Linkprofit',
				'trackMode' => 'postback',
				'options' => [
					'accountId' => 'default1',
					'campaignId' => '3f4931d8',
					'campaignUid' => '123ru' 
				],
				'enabled' => true 
			],
			'advcake' => [
				'campaignId' => 501,
				'className' => 'SoloCpa\Service\Provider\Advcake',
				'trackMode' => 'report',
				'options' => [
					'cookie_expire' => '30d' 
				],
				'enabled' => true 
			],
			'get4click' => [
				'campaignId' => 441,
				'className' => 'SoloCpa\Service\Provider\Get4Click',
				'trackMode' => 'report',
				'options' => [],
				'enabled' => true 
			] 
		],
		'trackers' => [
			'aprt' => [
				'className' => 'SoloCpa\Service\Tracker\Aprt',
				'enabled' => false,
				'options' => [
					'code' => '123shop' 
				] 
			],
			'admitad' => [
				'className' => 'SoloCpa\Service\Tracker\Admitad',
				'enabled' => false,
				'options' => [
					'mainpageCode' => '9ce8886eeb',
					'categoryCode' => '9ce8886ee4',
					'productCode' => '9ce8886ee5',
					'basketCode' => '9ce8886ee6',
					'orderCode' => '9ce8886ee6',
					'orderThankyouCode' => '9ce8886ee7' 
				] 
			],
			'adriver' => [
				'className' => 'SoloCpa\Service\Tracker\Adriver',
				'enabled' => false,
				'options' => [
					'sid' => 193811,
					'bt' => 62 
				] 
			],
			'criteo' => [
				'className' => 'SoloCpa\Service\Tracker\Criteo',
				'enabled' => true,
				'options' => [
					'account' => 16192,
					'siteType' => 'd' 
				] 
			],
			'gtm' => [
				'className' => 'SoloCpa\Service\Tracker\Gtm',
				'enabled' => true,
				'options' => [
					'id' => 'GTM-KBL5ZV' 
				] 
			],
			'ga' => [
				'className' => 'SoloCpa\Service\Tracker\Ga',
				'enabled' => false,
				'options' => [
					'id' => 'UA-25538461-2' 
				] 
			],
			'flocktory' => [
				'className' => 'SoloCpa\Service\Tracker\Flocktory',
				'enabled' => true,
				'options' => [
					'site_id' => 66 
				] 
			],
			'retailrocket' => [
				'className' => 'SoloCpa\Service\Tracker\RetailRocket',
				'enabled' => true,
				'options' => [] 
			],
			'gdeslon' => [
				'className' => 'SoloCpa\Service\Tracker\GdeSlon',
				'enabled' => false,
				'options' => [
					'merchant_id' => 70597 
				] 
			],
			'mixmarket' => [
				'className' => 'SoloCpa\Service\Tracker\MixMarket',
				'enabled' => false,
				'options' => [
					'id' => '1294939189',
					'retargetingId' => 1294930976 
				] 
			],
			'tpm' => [
				'className' => 'SoloCpa\Service\Tracker\Tpm',
				'enabled' => false 
			],
			'metrika' => [
				'className' => 'SoloCpa\Service\Tracker\YandexMetrika',
				'enabled' => true,
				'options' => [
					'id' => 2207821,
					'webvisor' => true,
					'clickmap' => true,
					'trackLinks' => true,
					'accurateTrackBounce' => true,
					'async' => true,
					'ecommerce' => 'dataLayerYa' 
				] 
			],
			'get4click' => [
				'className' => 'SoloCpa\Service\Tracker\Get4Click',
				'enabled' => true,
				'options' => [
					'shopId' => 473,
					'bannerId' => 1045 
				] 
			],
			'advcake' => [
				'className' => 'SoloCpa\Service\Tracker\AdvCake',
				'enabled' => true 
			],
			'migcredit' => [
				'className' => 'SoloCpa\Service\Tracker\MigCredit',
				'enabled' => true 
			],
			'adblender' => [
				'className' => 'SoloCpa\Service\Tracker\Adblender',
				'enabled' => true,
				'options' => [
					'account' => '123ru' 
				] 
			],
			'diginetica' => [
				'className' => 'SoloCpa\Service\Tracker\Diginetica',
				'enabled' => true,
				'options' => [
					'siteId' => '123' 
				] 
			],
			'kraken' => [
				'className' => 'SoloCpa\Service\Tracker\Kraken',
				'enabled' => true,
				'options' => [
					'projectId' => 2802743 
				] 
			] 
		] 
	] 
];