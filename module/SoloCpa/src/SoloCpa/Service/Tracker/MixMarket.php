<?php

namespace SoloCpa\Service\Tracker;

use SoloCpa\Entity\Tracker\TrackerInterface;
use Solo\Stdlib\StringUtils;

class MixMarket extends AbstractTracker {

	/**
	 *
	 * @param TrackerInterface $tracker        	
	 * @return string
	 */
	public function getCode(TrackerInterface $tracker) {
		$this->validateOptions();
		
		$out = '';
		switch ($tracker->getType()) {
			case TrackerInterface::TYPE_BASKET:
			case TrackerInterface::TYPE_ORDER:
				$out = StringUtils::format(
					'<script>document.write(\'<img src="//mixmarket.biz/uni/tev.php?id={id}&r=\'+escape(document.referrer)+\'&t=\'+(new Date()).getTime()+\'" width="1" height="1"/>\');</script><noscript><img src="http://mixmarket.biz/uni/tev.php?id=' . $this->getOption(
						'id') . '" width="1" height="1"/></noscript>', 
					[
						'id' => $this->getOption('id') 
					]);
				break;
		}
		$params = [];
		$params['uAdvArId'] = $this->getOption('retargetingId');
		if (TrackerInterface::TYPE_PRODUCT == $tracker->getType()) {
			$params['skulist'] = $tracker->getId();
		}
		$out .= '<script type="text/javascript">window.__mixm__ = window.__mixm__ || [];';
		foreach ($params as $name => $value) {
			$out .= sprintf('window.__mixm__.push([\'%s\', %s]);', $name, $value);
		}
		
		$out .= '(function(){function t(){if(!e){e=1;var t=0,a=0,n=0;for(i=0;o.__mixm__.length>i;i++){if("trId"==o.__mixm__[i][0]){t="t"+o.__mixm__[i][1];break}"uAdvArId"==o.__mixm__[i][0]?a="u"+o.__mixm__[i][1]:"mAdvId"==o.__mixm__[i][0]&&(n="m"+o.__mixm__[i][1])}var m=t?t:a?a:n?n:"def",r=document.createElement("script");r.type="text/javascript",r.async=!0,r.src=("https:"==document.location.protocol?"https://":"http://")+"js.mixmarket.biz/a"+m+".js?t="+(new Date).getTime();var d=document.getElementsByTagName("script")[0];d.parentNode.insertBefore(r,d)}}var e=0,a=document,n=a.documentElement,o=window;"complete"==a.readyState||"loaded"==a.readyState||"interactive"==a.readyState?t():a.addEventListener?a.addEventListener("DOMContentLoaded",t,!1):a.attachEvent?(n.doScroll&&o==o.top&&function(){try{n.doScroll("left")}catch(e){return setTimeout(arguments.callee,0),void 0}t()}(),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t()})):o.onload=t})();</script>';
		
		return $out;
	}

	/**
	 *
	 * @throws \RuntimeException
	 */
	private function validateOptions() {
		$checkCodes = [
			'id',
			'retargetingId' 
		];
		foreach ($checkCodes as $checkCode) {
			$optionName = $checkCode;
			if (!$this->hasOption($optionName) || empty($this->getOption($optionName))) {
				throw new \RuntimeException(sprintf('Not specified "%s" option', $optionName));
			}
		}
	}

}

?>