<?php

namespace SoloCpa\Service\Tracker;

use SoloCpa\Entity\Tracker\TrackerInterface;

class YandexMetrika extends AbstractTracker {

	/**
	 *
	 * @param TrackerInterface $tracker        	
	 * @return string
	 */
	public function getCode(TrackerInterface $tracker) {
		$this->validateOptions();
		
		$out = [];
		
		$out[] = '<!-- Yandex.Metrika counter -->
		<script type="text/javascript">
		(function (d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {';
		
		// ecommerce
		if ($this->hasOption('ecommerce')) {
			$out[] = 'window.' . $this->getOption('ecommerce') . ' = window.' . $this->getOption('ecommerce') . ' || [];';
			$eobj = [
				'currencyCode' => 'RUB' 
			];
			
			switch ($tracker->getType()) {
				case $tracker::TYPE_PRODUCT:
					$product = [
						'id' => $tracker->getId(),
						'name' => $tracker->getName(),
						'price' => $tracker->getPrice() 
					];
					if ($tracker->hasVendorName()) {
						$product['brand'] = $tracker->getVendorName();
					}
					if ($tracker->hasCategory()) {
						$product['category'] = $tracker->getCategory()->getName();
					}
					
					$eobj['detail'] = [
						'products' => [
							$product 
						] 
					];
					break;
				case $tracker::TYPE_ORDER_THANKYOU:
					$products = [];
					
					foreach ($tracker->getPurchasedProducts() as $product) {
						$product = [
							'id' => $product->getId(),
							'name' => $product->getName(),
							'price' => $product->getPrice(),
							'quantity' => $product->getQuantity() 
						];
						$products[] = $product;
					}
					
					$eobj['purchase'] = [
						'actionField' => [
							'id' => $tracker->orderInfo()->getNumber() 
						],
						'products' => $products 
					];
					
					break;
			}
			
			$out[] = $this->getOption('ecommerce').'.push({ "ecommerce": ' . json_encode($eobj, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT) . ' });';
		}
		// END ecommerce
		
		$initParams = [];
		$initParams['id'] = $this->getOption('id');
		if ($this->hasOption('webvisor')) {
			$initParams['webvisor'] = ((true == $this->getOption('webvisor')) ? 'true' : 'false');
		}
		if ($this->hasOption('clickmap')) {
			$initParams['clickmap'] = ((true == $this->getOption('clickmap')) ? 'true' : 'false');
		}
		if ($this->hasOption('trackLinks')) {
			$initParams['trackLinks'] = ((true == $this->getOption('trackLinks')) ? 'true' : 'false');
		}
		if ($this->hasOption('accurateTrackBounce')) {
			$initParams['accurateTrackBounce'] = ((true == $this->getOption('accurateTrackBounce')) ? 'true' : 'false');
		}
		if ($this->hasOption('ecommerce')) {
			$initParams['ecommerce'] = $this->getOption('ecommerce');
		}
		
		$out[] = 'w.yaCounter' . $this->getOption('id') . ' = new Ya.Metrika(' . json_encode($initParams, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT) . ');
				} catch(e) { }
			});';
		
		$out[] = 'var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
				s.type = "text/javascript";';
		$out[] = 's.async = true;';
		$out[] = 's.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";';
		
		$out[] = 'if (w.opera == "[object Opera]") {
					d.addEventListener("DOMContentLoaded", f, false);
				} else { f(); }
		})(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript><div><img src="//mc.yandex.ru/watch/' . $this->getOption('id') . '" style="position:absolute; left:-9999px;" alt="" /></div></noscript>';
		
		return implode("\n", $out);
	}

	/**
	 *
	 * @throws \RuntimeException
	 */
	private function validateOptions() {
		$checkCodes = [
			'id' 
		];
		foreach ($checkCodes as $checkCode) {
			$optionName = $checkCode;
			if (!$this->hasOption($optionName) || empty($this->getOption($optionName))) {
				throw new \RuntimeException(sprintf('Not specified "%s" option', $optionName));
			}
		}
	}

}

?>