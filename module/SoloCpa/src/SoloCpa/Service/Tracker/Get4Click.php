<?php

namespace SoloCpa\Service\Tracker;

use Solo\Stdlib\StringUtils;
use SoloCpa\Entity\Tracker\TrackerInterface;

class Get4Click extends AbstractTracker {

	/**
	 *
	 * @param TrackerInterface $tracker        	
	 * @return string
	 */
	public function getCode(TrackerInterface $tracker) {
		$this->validateOptions();
		
		$params = [];
		
		switch ($tracker->getType()) {
			case $tracker::TYPE_ORDER_THANKYOU:
				$params['_shopId'] = $this->getOption('shopId');
				$params['_bannerId'] = $this->getOption('bannerId');
				$params['_customerFirstName'] = $tracker->userInfo()->getName();
				$params['_customerLastName'] = '';
				$params['_customerEmail'] = $tracker->userInfo()->getEmail();
				$params['_customerPhone'] = ($tracker->userInfo()->hasPhone() ? $tracker->userInfo()->getPhone()->toString('+7{code}{number}') : '');
				$params['_customerGender'] = '';
				$params['_orderId'] = $tracker->orderInfo()->getNumber();
				$params['_orderValue'] = $tracker->orderInfo()->getAmount() . '.00';
				$params['_orderCurrency'] = 'RUB';
				$params['_usedPromoCode'] = $tracker->orderInfo()->getPromocode();
				
				break;
		}
		
		$out = '';
		if (0 < count($params)) {
			$out = StringUtils::format(
				'<script type="text/javascript">
var _iPromoBannerObj = function() {
this.htmlElementId = \'promocode-element-container\';
this.params = {params};
			
this.lS=function(s){document.write(\'<sc\'+\'ript type="text/javascript" src="\'+s+\'" async="true"></scr\'+\'ipt>\');},
this.gc=function(){return document.getElementById(this.htmlElementId);};
var r=[];for(e in this.params){if(typeof(e)===\'string\'){r.push(e+\'=\'+encodeURIComponent(this.params[e]));}}r.push(\'method=main\');r.push(\'jsc=iPromoCpnObj\');this.lS((\'https:\'==document.location.protocol ? \'https://\':\'http://\')+\'get4click.ru/wrapper.php?\'+r.join(\'&\'));};
			
var iPromoCpnObj = new _iPromoBannerObj();
</script>', 
				[
					'params' => json_encode($params, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT) 
				]);
		}
		return $out;
	}

	/**
	 *
	 * @throws \RuntimeException
	 */
	private function validateOptions() {
		$checkCodes = [
			'shopId',
			'bannerId' 
		];
		foreach ($checkCodes as $checkCode) {
			$optionName = $checkCode;
			if (!$this->hasOption($optionName) || empty($this->getOption($optionName))) {
				throw new \RuntimeException(sprintf('Not specified "%s" option', $optionName));
			}
		}
	}

}

?>