<?php

namespace SoloCpa\Service\Tracker;

use SoloCpa\Entity\Tracker\TrackerInterface;

class Aprt extends AbstractTracker {

	/**
	 *
	 * @param TrackerInterface $tracker        	
	 * @return string
	 */
	public function getCode(TrackerInterface $tracker) {
		$this->validateOptions();
		
		$data = [];
		switch ($tracker->getType()) {
			case $tracker::TYPE_MAINPAGE:
				$data['pageType'] = 1;
				break;
			
			case $tracker::TYPE_CATEGORY:
				$data['pageType'] = 3;
				$data['currentCategory'] = [
					'id' => $tracker->getId(),
					'name' => $tracker->getName() 
				];
				if ($tracker->hasParentCategories()) {
					$data['parentCategories'] = [];
					foreach ($tracker->getParentCategories() as $parentCategory) {
						$data['parentCategories'][] = [
							'id' => $parentCategory->getId(),
							'name' => $parentCategory->getName() 
						];
					}
				}
				if ($tracker->hasChildCategories()) {
					$data['childCategories'] = [];
					foreach ($tracker->getChildCategories() as $childCategory) {
						$data['childCategories'][] = [
							'id' => $childCategory->getId(),
							'name' => $childCategory->getName() 
						];
					}
				}
				break;
			
			case $tracker::TYPE_PRODUCT:
				$data['pageType'] = 2;
				$data['currentProduct'] = [
					'id' => $tracker->getId(),
					'name' => $tracker->getName(),
					'price' => $tracker->getPrice() 
				];
				if ($tracker->hasCategory()) {
					$data['currentCategory'] = [
						'id' => $tracker->getCategory()->getId(),
						'name' => $tracker->getCategory()->getName() 
					];
				}
				break;
			
			case $tracker::TYPE_BASKET:
				$data['pageType'] = 4;
				break;
			
			case $tracker::TYPE_ORDER:
				$data['pageType'] = 5;
				break;
			
			case $tracker::TYPE_ORDER_THANKYOU:
				$data['pageType'] = 6;
				$data['purchasedProducts'] = [];
				foreach ($tracker->getPurchasedProducts() as $product) {
					$data['purchasedProducts'][] = [
						'id' => $product->getId(),
						'name' => $product->getName(),
						'price' => $product->getPrice(),
						'quantity' => $product->getQuantity() 
					];
				}
				$data['orderInfo'] = [
					'id' => $tracker->orderInfo()->getNumber(),
					'totalPrice' => $tracker->orderInfo()->getAmount() 
				];
				break;
			
			default:
				$data['pageType'] = 0;
				break;
		}
		if ($tracker->hasBasketProducts()) {
			$data['basketProducts'] = [];
			foreach ($tracker->getBasketProducts() as $basketProduct) {
				$data['basketProducts'][] = [
					'id' => $basketProduct->getId(),
					'name' => $basketProduct->getName(),
					'price' => $basketProduct->getPrice(),
					'quantity' => $basketProduct->getQuantity() 
				];
			}
		}
		
		$out = [];
		$out[] = '<script type="text/javascript">
(function() {
	var s = document.createElement(\'script\');
	s.type = \'text/javascript\';
	s.async = s.defer = true;
	s.src = \'//aprtx.com/code/' . $this->getOption('code') . '/\';
	var p = document.getElementsByTagName(\'body\')[0] || document.getElementsByTagName(\'head\')[0];
	if (p) p.appendChild(s);
})();
</script>';
		$out[] = '<script type="text/javascript">';
		$out[] = 'window.APRT_DATA = ' . json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT) . ';';
		$out[] = '</script>';
		
		return implode("\n", $out);
	}

	/**
	 *
	 * @throws \RuntimeException
	 */
	private function validateOptions() {
		if (!$this->hasOption('code') || empty($this->getOption('code'))) {
			throw new \RuntimeException('Not specified "code" option');
		}
	}

}

?>