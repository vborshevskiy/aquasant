<?php

namespace SoloCpa\Service\Tracker;

use SoloCpa\Entity\Tracker\TrackerInterface;

class Criteo extends AbstractTracker {

	/**
	 *
	 * @param TrackerInterface $tracker        	
	 * @return string
	 */
	public function getCode(TrackerInterface $tracker) {
		$this->validateOptions();
		
		$data = [];
		$data[] = [
			'event' => 'setAccount',
			'account' => $this->getOption('account') 
		];
		$data[] = [
			'event' => 'setSiteType',
			'type' => $this->getOption('siteType') 
		];
		if ($tracker->userInfo()->hasEmail()) {
			$data[] = [
				'event' => 'setHashedEmail',
				'email' => md5($tracker->userInfo()->getEmail()) 
			];
		}
		
		switch ($tracker->getType()) {
			case $tracker::TYPE_MAINPAGE:
				$data[] = [
					'event' => 'viewHome' 
				];
				break;
			
			case $tracker::TYPE_CATEGORY:
			case $tracker::TYPE_SEARCH:
				$products = [];
				if ($tracker->hasProducts()) {
					$i = 0;
					foreach ($tracker->getProducts() as $product) {
						if (3 > $i) {
							$products[] = sprintf('%s.%s', $tracker->getOption('criteo.product_prefix'), $product->getId());
						}
						$i++;
					}
				}
				$data[] = [
					'event' => 'viewList',
					'item' => $products 
				];
				break;
			
			case $tracker::TYPE_PRODUCT:
				$data[] = [
					'event' => 'viewItem',
					'item' => sprintf('%s.%s', $tracker->getOption('criteo.product_prefix'), $tracker->getId()),
					'user_segment' => $tracker->hasOption('criteo.user_segment') ? $tracker->getOption('criteo.user_segment') : 0
				];
				break;
			
			case $tracker::TYPE_BASKET:
			case $tracker::TYPE_ORDER:
				$items = [];
				foreach ($tracker->getBasketProducts() as $product) {
					$items[] = [
						'id' => sprintf('%s.%s', $tracker->getOption('criteo.product_prefix'), $product->getId()),
						'price' => $product->getPrice(),
						'quantity' => $product->getQuantity() 
					];
				}
				$data[] = [
					'event' => 'viewBasket',
					'item' => $items 
				];
				break;
			
			case $tracker::TYPE_ORDER_THANKYOU:
				$items = [];
				foreach ($tracker->getPurchasedProducts() as $product) {
					$items[] = [
						'id' => sprintf('%s.%s', $tracker->getOption('criteo.product_prefix'), $product->getId()),
						'price' => $product->getPrice(),
						'quantity' => $product->getQuantity() 
					];
				}
				$data[] = [
					'event' => 'trackTransaction',
					'id' => $tracker->orderInfo()->getNumber(),
					'new_customer' => 1,
					//'deduplication' => (($tracker->hasCpaProviderUid() && ('criteo' == $tracker->getCpaProviderUid())) ? 1 : 0),
					'item' => $items 
				];
				break;
		}
		
		$out = [];
		$out[] = '<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"></script>
<script type="text/javascript">
window.criteo_q = window.criteo_q || [];
window.criteo_q.push(';
		for ($i = 0, $size = count($data); $i < $size; $i++) {
			$out[] = json_encode($data[$i]) . (($i < ($size - 1)) ? ',' : '');
		}
		$out[] = ');
</script>';
		
		return implode("\n", $out);
	}

	/**
	 *
	 * @throws \RuntimeException
	 */
	private function validateOptions() {
		$checkCodes = [
			'account',
			'siteType' 
		];
		foreach ($checkCodes as $checkCode) {
			$optionName = $checkCode;
			if (!$this->hasOption($optionName) || empty($this->getOption($optionName))) {
				throw new \RuntimeException(sprintf('Not specified "%s" option', $optionName));
			}
		}
	}

}

?>