<?php

namespace SoloCpa\Service\Tracker;

use SoloCpa\Entity\Tracker\TrackerInterface;
use Solo\Stdlib\ArrayHelper;

class GdeSlon extends AbstractTracker {

	/**
	 *
	 * @param TrackerInterface $tracker        	
	 * @return string
	 */
	public function getCode(TrackerInterface $tracker) {
		$this->validateOptions();
		
		$params = [
			'mid' => $this->getOption('merchant_id') 
		];
		$mode = '';
		
		switch ($tracker->getType()) {
			case $tracker::TYPE_MAINPAGE:
				$mode = 'main';
				break;
			
			case $tracker::TYPE_CATEGORY:
				$mode = 'list';
				$params['cat_id'] = $tracker->getId();
				$codes = [];
				foreach ($tracker->getProducts() as $product) {
					$codes[] = sprintf('%d:%d', $product->getId(), $product->getPrice());
				}
				$params['codes'] = implode(',', $codes);
				break;
			
			case $tracker::TYPE_SEARCH:
				$mode = 'list';
				$codes = [];
				foreach ($tracker->getProducts() as $product) {
					$codes[] = sprintf('%d:%d', $product->getId(), $product->getPrice());
				}
				$params['codes'] = implode(',', $codes);
				break;
			
			case $tracker::TYPE_PRODUCT:
				$mode = 'card';
				$params['codes'] = sprintf('%d:%d', $tracker->getId(), $tracker->getPrice());
				break;
			
			case $tracker::TYPE_BASKET:
			case $tracker::TYPE_ORDER:
				$mode = 'basket';
				$codes = [];
				foreach ($tracker->getBasketProducts() as $product) {
					for ($i = 0; $i < $product->getQuantity(); $i++) {
						$codes[] = sprintf('%d:%d', $product->getId(), $product->getPrice());
					}
				}
				$params['codes'] = implode(',', $codes);
				break;
			
			case $tracker::TYPE_ORDER_THANKYOU:
				$mode = 'thanks';
				$params['order_id'] = $tracker->orderInfo()->getNumber();
				$codes = [];
				foreach ($tracker->getPurchasedProducts() as $product) {
					for ($i = 0; $i < $product->getQuantity(); $i++) {
						$codes[] = sprintf('%d:%d', $product->getId(), $product->getPrice());
					}
				}
				$params['codes'] = implode(',', $codes);
				break;
			
			case $tracker::TYPE_EMPTY:
			default:
				$mode = 'other';
				break;
		}
		
		$params['mode'] = $mode;
		
		$out = '<script async="true" type="text/javascript" src="https://www.gdeslon.ru/landing.js?' . implode('&', ArrayHelper::joinKeyValue($params)) . '"></script>';
		return $out;
	}

	/**
	 *
	 * @throws \RuntimeException
	 */
	private function validateOptions() {
		$checkCodes = [
			'merchant_id' 
		];
		foreach ($checkCodes as $optionName) {
			if (!$this->hasOption($optionName) || empty($this->getOption($optionName))) {
				throw new \RuntimeException(sprintf('Not specified "%s" option', $optionName));
			}
		}
	}

}

?>