<?php

namespace SoloCpa\Service\Tracker;

use SoloCpa\Entity\Tracker\TrackerInterface;

class Gtm extends AbstractTracker {

	/**
	 *
	 * @return string
	 */
	public function getPreheader() {
		$this->validateOptions();
		
		$out = [];
		$out[] = "<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push(
{'gtm.start': new Date().getTime(),event:'gtm.js'}
);var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','" . $this->getOption('id') . "');</script>
<!-- End Google Tag Manager -->";
		return implode("/", $out);
	}

	/**
	 *
	 * @param TrackerInterface $tracker        	
	 * @return string
	 */
	public function getCode(TrackerInterface $tracker) {
		$this->validateOptions();
		
		$params = [];
		
		$totalvalue = 0;
		$prodid = 0;
		
		switch ($tracker->getType()) {
			case $tracker::TYPE_PRODUCT:
				$totalvalue = $tracker->getPrice();
				$prodid = $tracker->getId();
				break;
			
			case $tracker::TYPE_ORDER_THANKYOU:
				$totalvalue = $tracker->orderInfo()->getAmount();
				$prodid = [];
				foreach ($tracker->getPurchasedProducts() as $product) {
					$prodid[] = $product->getId();
				}
				break;
		}
		
		$params['ecomm_totalvalue'] = $totalvalue;
		$params['ecomm_prodid'] = $prodid;
		
		$out = [];
		$out[] = '<script type="text/javascript">';
		$out[] = 'dataLayer = dataLayer || [];';
		$out[] = 'dataLayer.push(' . json_encode($params, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT) . ');';
		$out[] = '</script>';
		$out[] = '<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=' . $this->getOption('id') . '"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->';
		return implode("\n", $out);
	}

	/**
	 *
	 * @throws \RuntimeException
	 */
	private function validateOptions() {
		$checkCodes = [
			'id' 
		];
		foreach ($checkCodes as $checkCode) {
			$optionName = $checkCode;
			if (!$this->hasOption($optionName) || empty($this->getOption($optionName))) {
				throw new \RuntimeException(sprintf('Not specified "%s" option', $optionName));
			}
		}
	}

}

?>