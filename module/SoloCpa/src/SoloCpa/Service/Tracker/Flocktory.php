<?php

namespace SoloCpa\Service\Tracker;

use SoloCpa\Entity\Tracker\TrackerInterface;

class Flocktory extends AbstractTracker {

	/**
	 *
	 * @param TrackerInterface $tracker        	
	 * @return string
	 */
	public function getCode(TrackerInterface $tracker) {
		$this->validateOptions();
		
		$out = [];
		$out[] = '<script type="text/javascript" src="//api.flocktory.com/v2/loader.js?site_id=' . $this->getOption('site_id') . '" async="async"></script>';
		
		if ($tracker->userInfo()->hasName() && $tracker->userInfo()->hasEmail()) {
			$out[] = '<div class="i-flocktory" data-fl-user-name="' . $tracker->userInfo()->getName() . '" data-fl-user-email="' . $tracker->userInfo()->getEmail() . '"></div>';
		}
		
		switch ($tracker->getType()) {
			case $tracker::TYPE_CATEGORY:
				$out[] = '<div class="i-flocktory" data-fl-action="track-category-view" data-fl-category-id="' . $tracker->getId() . '"></div>';
				break;
			
			case $tracker::TYPE_PRODUCT:
				$out[] = '<div class="i-flocktory" data-fl-action="track-item-view" data-fl-item-id="' . $tracker->getId() . '"></div>';
				break;
			
			case $tracker::TYPE_ORDER_THANKYOU:
				$out[] = '<div class="i-flocktory" data-fl-action="exchange" data-fl-user-name="' . $tracker->userInfo()->getName() . '" data-fl-user-email="' . $tracker->userInfo()->getEmail() . '" data-fl-spot="thankyou"></div>';
				
				$products = [];
				foreach ($tracker->getPurchasedProducts() as $product) {
					$item = [
						'id' => $product->getId(),
						'title' => $product->getName(),
						'price' => $product->getPrice(),
						'count' => $product->getQuantity() 
					];
					if ($product->hasImageUrl()) {
						$item['image'] = $product->getImageUrl();
					}
					$products[] = $item;
				}
				
				$out[] = '<script type="text/javascript">
    window.flocktory = window.flocktory || [];
    window.flocktory.push([\'postcheckout\', {
        user: {
            name: \'' . $tracker->userInfo()->getName() . '\',
            email: \'' . $tracker->userInfo()->getEmail() . '\'
        },
        order: {
            id: \'' . $tracker->orderInfo()->getNumber() . '\',
            price: ' . $tracker->orderInfo()->getAmount() . ',
            items: ' . json_encode($products, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) . '
        }
    }]);
</script>';
				break;
		}
		
		return implode("\n", $out);
	}

	/**
	 *
	 * @throws \RuntimeException
	 */
	private function validateOptions() {
		$checkCodes = [
			'site_id' 
		];
		foreach ($checkCodes as $optionName) {
			if (!$this->hasOption($optionName) || empty($this->getOption($optionName))) {
				throw new \RuntimeException(sprintf('Not specified "%s" option', $optionName));
			}
		}
	}

}

?>