<?php

namespace SoloCpa\Service\Tracker;

use SoloCpa\Entity\Tracker\TrackerInterface;

class Tpm extends AbstractTracker {

	/**
	 * 
	 * @param TrackerInterface $tracker
	 * @return string
	 */
	public function getCode(TrackerInterface $tracker) {
		$params = [];
		switch ($tracker->getType()) {
			case $tracker::TYPE_PRODUCT:
				$params['xcnt_product_id'] = sprintf("'%d'", $tracker->getId());
				break;
			case $tracker::TYPE_BASKET:
			case $tracker::TYPE_ORDER:
				$productIds = [];
				$quantities = [];
				foreach ($tracker->getBasketProducts() as $product) {
					$productIds[] = $product->getId();
					$quantities[] = $product->getQuantity();
				}
				$params['xcnt_basket_products'] = sprintf("'%s'", implode(',', $productIds));
				$params['xcnt_basket_quantity'] = sprintf("'%s'", implode(',', $quantities));
				break;
			case $tracker::TYPE_ORDER_THANKYOU:
				$productIds = [];
				$quantities = [];
				foreach ($tracker->getPurchasedProducts() as $product) {
					$productIds[] = $product->getId();
					$quantities[] = $product->getQuantity();
				}
				$params['xcnt_order_products'] = sprintf("'%s'", implode(',', $productIds));
				$params['xcnt_order_quantity'] = sprintf("'%s'", implode(',', $quantities));
				$params['xcnt_order_id'] = sprintf("'%s'", $tracker->orderInfo()->getNumber());
				$params['xcnt_order_total'] = sprintf("'%s'", $tracker->orderInfo()->getAmount());
				$params['xcnt_order_currency'] = "'RUB'";
				if ($tracker->userInfo()->getEmail()) {
					$params['xcnt_user_email'] = sprintf("'%s'", $tracker->userInfo()->getEmail());
					$params['xcnt_user_email_hash'] = sprintf("'%s'", md5($tracker->userInfo()->getEmail()));
				}
				if ($tracker->userInfo()->getId()) {
					$params['xcnt_user_id'] = sprintf("'%s'", $tracker->userInfo()->getId());
				}
				break;
		}
		
		$out = [];
		$out[] = '<script id="xcntmyAsync" type="text/javascript">';
		foreach ($params as $name => $value) {
			$out[] = sprintf('var %s = %s;', $name, $value);
		}
		$out[] = '(function(d){';
		$out[] = 'var xscr = d.createElement(\'script\'); xscr.async = 1;';
		$out[] = 'xscr.src = \'//x.cnt.my/async/track/?r=\' + Math.random();';
		$out[] = 'var x = d.getElementById(\'xcntmyAsync\');';
		$out[] = 'x.parentNode.insertBefore(xscr, x);';
		$out[] = '})(document);';
		$out[] = '</script>';
		
		return implode("\n", $out);
	}

}

?>