<?php

namespace SoloCpa\Service\Tracker;

use SoloCpa\Entity\Tracker\TrackerInterface;

class Ga extends AbstractTracker {

	/**
	 *
	 * @var array
	 */
	private $events = [];

	/**
	 *
	 * @param TrackerInterface $tracker        	
	 * @return string
	 */
	public function getCode(TrackerInterface $tracker) {
		$this->validateOptions();
		
		$params = [];
		if ($tracker->userInfo()->hasId()) {
			$params['dimension1'] = $tracker->userInfo()->getId();
		}
		if ($tracker->userInfo()->hasInitialDate()) {
			$params['dimension2'] = $tracker->userInfo()->getInitialDate()->format('d.m.Y');
		}
		
		$out = [];
		$out[] = '<script>
		(function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');
		ga(\'create\', \'' . $this->getOption('id') . '\', \'auto\');';
		foreach ($params as $name => $value) {
			$out[] = 'ga(\'set\', \'' . $name . '\', \'' . $value . '\');';
		}
		$out[] = 'ga(\'send\', \'pageview\');';
		foreach ($this->events as $event) {
			$send = ['hitType' => 'event', 'eventCategory' => $event['category'], 'eventAction' => $event['action'], 'eventLabel' => $event['label']];
			if (isset($event['value'])) {
				$send['eventValue'] = $event['value'];
			}
			$out[] = 'ga(\'send\', '.json_encode($send, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE).');';
		}
		$out[] = 'ga(function(tracker) { clientId = tracker.get(\'clientId\'); var date = new Date(new Date().getTime() + (60 * 60 * 24 * 365 * 2 * 1000)); document.cookie = "_gaClientId="+clientId+"; path=/; expires="+date.toUTCString(); });';
		$out[] = '</script>';
		
		return implode("\n", $out);
	}

	/**
	 *
	 * @param string $category        	
	 * @param string $action        	
	 * @param string $label        	
	 * @param mixed $value        	
	 */
	public function addEvent($category, $action, $label, $value = null) {
		$event = [
			'category' => $category,
			'action' => $action,
			'label' => $label
		];
		if (null !== $value) {
			$event['value'] = $value;
		}
		$this->events[] = $event;
	}

	/**
	 *
	 * @throws \RuntimeException
	 */
	private function validateOptions() {
		$checkCodes = [
			'id' 
		];
		foreach ($checkCodes as $checkCode) {
			$optionName = $checkCode;
			if (!$this->hasOption($optionName) || empty($this->getOption($optionName))) {
				throw new \RuntimeException(sprintf('Not specified "%s" option', $optionName));
			}
		}
	}

}

?>