<?php

namespace SoloCpa\Service\Tracker;

use SoloCpa\Entity\Tracker\TrackerInterface;

class Admitad extends AbstractTracker {

	/**
	 *
	 * @param TrackerInterface $tracker        	
	 * @return string
	 */
	public function getCode(TrackerInterface $tracker) {
		$this->validateOptions();
		
		$level = null;
		$code = null;
		$params = [];
		
		switch ($tracker->getType()) {
			case $tracker::TYPE_CATEGORY:
				$level = 1;
				$code = $this->getOption('categoryCode');
				$params['ad_category'] = $tracker->getId();
				break;
			
			case $tracker::TYPE_PRODUCT:
				$level = 2;
				$code = $this->getOption('productCode');
				$params['ad_product'] = [
					'id' => $tracker->getId(),
					'price' => $tracker->getPrice(),
					'url' => $tracker->getUrl(),
					'name' => $tracker->getName() 
				];
				if ($tracker->hasVendorName()) {
					$params['ad_product']['vendor'] = $tracker->getVendorName();
				}
				if ($tracker->hasImageUrl()) {
					$params['ad_product']['picture'] = $tracker->getImageUrl();
				}
				if ($tracker->hasCategory()) {
					$params['ad_product']['category'] = $tracker->getCategory()->getId();
				}
				break;
			
			case $tracker::TYPE_BASKET:
				$level = 3;
				$code = $this->getOption('basketCode');
				$params['ad_products'] = [];
				foreach ($tracker->getBasketProducts() as $product) {
					$params['ad_product'][] = [
						'id' => $product->getId(),
						'number' => $product->getQuantity() 
					];
				}
				break;
			
			case $tracker::TYPE_ORDER:
				$level = 3;
				$code = $this->getOption('orderCode');
				$params['ad_products'] = [];
				foreach ($tracker->getBasketProducts() as $product) {
					$params['ad_products'][] = [
						'id' => $product->getId(),
						'number' => $product->getQuantity() 
					];
				}
				break;
			
			case $tracker::TYPE_ORDER_THANKYOU:
				$level = 4;
				$code = $this->getOption('orderThankyouCode');
				$params['ad_order'] = $tracker->orderInfo()->getNumber();
				$params['ad_amount'] = $tracker->orderInfo()->getAmount() . '.00';
				$params['ad_products'] = [];
				foreach ($tracker->getPurchasedProducts() as $product) {
					$params['ad_products'][] = [
						'id' => $product->getId(),
						'number' => $product->getQuantity() 
					];
				}
				break;
				
			case $tracker::TYPE_MAINPAGE:
			default:
				$level = 0;
				$code = $this->getOption('mainpageCode');
				break;
		}
		
		$out = [];
		$out[] = '<script type="text/javascript">';
		foreach ($params as $paramName => $paramValue) {
			$out[] = 'window.' . $paramName . ' = ' . json_encode($paramValue, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT) . ';';
		}
		$out[] = 'window._retag = window._retag || [];
window._retag.push({code: "' . $code . '", level: ' . $level . '});
(function () {
	var id = "admitad-retag";
	if (document.getElementById(id)) {return;}
	var s = document.createElement("script");
	s.async = true; s.id = id;
	var r = (new Date).getTime();
	s.src = (document.location.protocol == "https:" ? "https:" : "http:") + "//cdn.trmit.com/static/js/retag.min.js?r="+r;
	var a = document.getElementsByTagName("script")[0]
	a.parentNode.insertBefore(s, a);
})()
</script>';
		return implode("\n", $out);
	}

	/**
	 *
	 * @throws \RuntimeException
	 */
	private function validateOptions() {
		$checkCodes = [
			'mainpage',
			'category',
			'product',
			'basket',
			'order',
			'orderThankyou' 
		];
		foreach ($checkCodes as $checkCode) {
			$optionName = $checkCode . 'Code';
			if (!$this->hasOption($optionName) || empty($this->getOption($optionName))) {
				throw new \RuntimeException(sprintf('Not specified "%s" option', $optionName));
			}
		}
	}

}

?>