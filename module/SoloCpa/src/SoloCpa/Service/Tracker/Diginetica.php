<?php

namespace SoloCpa\Service\Tracker;

use Solo\Stdlib\StringUtils;
use SoloCpa\Entity\Tracker\TrackerInterface;
use Solo\DateTime\DateTime;

class Diginetica extends AbstractTracker {

	private $alreadyShown = false;

	/**
	 *
	 * @param TrackerInterface $tracker        	
	 * @return string
	 */
	public function getCode(TrackerInterface $tracker) {
		$out = [];
		
		if (!$this->alreadyShown) {
			$out[] = StringUtils::format('<script src="//cdn.diginetica.net/{siteId}/client.js?ts={timestamp}" defer
async></script>', [
				'siteId' => $this->getOption('siteId'),
				'timestamp' => DateTime::now()->getTimestamp() 
			]);
			$out[] = StringUtils::format(
				'<script type="text/javascript">
var digiScript = document.createElement(\'script\');
digiScript.src = \'//cdn.diginetica.net/{siteId}/client.js?ts=\' + Date.now();
digiScript.defer = true;
digiScript.async = true;
document.body.appendChild(digiScript);
</script>', 
				[
					'siteId' => $this->getOption('siteId') 
				]);
			$this->alreadyShown = true;
		}
		
		return implode("\n", $out);
	}

}

?>