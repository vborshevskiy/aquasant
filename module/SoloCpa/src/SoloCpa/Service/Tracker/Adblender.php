<?php

namespace SoloCpa\Service\Tracker;

use SoloCpa\Entity\Tracker\TrackerInterface;

class Adblender extends AbstractTracker {

	/**
	 *
	 * @param TrackerInterface $tracker        	
	 * @return string
	 */
	public function getCode(TrackerInterface $tracker) {
		$out = [];
		$out[] = '<script type="text/javascript">
(function() {
    var ra = document.createElement(\'script\'); ra.type = \'text/javascript\'; ra.async = true;
    ra.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'bn.adblender.ru/c/' . $this->getOption('account') . '/all.js?\' + Math.floor((new Date()).valueOf()/1000/3600);
    var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ra, s);
})();';
		
		$data = [];
		
		switch ($tracker->getType()) {
			
			case $tracker::TYPE_BASKET:
				$out[] = 'window.Adblender = window.Adblender || [];
Adblender.push({type:\'basketView\'});';
				break;
			
			case $tracker::TYPE_ORDER_THANKYOU:
				$out[] = 'window.Adblender = window.Adblender || [];
Adblender.push([\'' . $tracker->orderInfo()->getNumber() . '\', ' . $tracker->orderInfo()->getAmount() . ']);';
				break;
		}
		
		$out[] = '</script>';
		
		return implode("\n", $out);
	}

}

?>