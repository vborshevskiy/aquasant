<?php

namespace SoloCpa\Service\Tracker;

use SoloCpa\Entity\Tracker\TrackerInterface;

class RetailRocket extends AbstractTracker {

	/**
	 *
	 * @param TrackerInterface $tracker        	
	 * @return string
	 */
	public function getCode(TrackerInterface $tracker) {
		$out = '';
		
		switch ($tracker->getType()) {
			case $tracker::TYPE_SEARCH:
				$out = '<script type="text/javascript">
rrApiOnReady.push(function() {
    try { rrApi.search("' . $tracker->getQueryPhrase() . '"); } 
    catch(e) {}
})
</script>';
				break;
			
			case $tracker::TYPE_ORDER_THANKYOU:
				$data = [
					'transaction' => $tracker->orderInfo()->getNumber(),
					'items' => [] 
				];
				foreach ($tracker->getPurchasedProducts() as $product) {
					$data['items'][] = [
						'id' => $product->getTag('bitrixId'),
						'qnt' => $product->getQuantity(),
						'price' => $product->getPrice() 
					];
				}
				
				$out = '<script type="text/javascript">
rrApiOnReady.push(function () { rrApi.setEmail("' . $tracker->userInfo()->getEmail() . '"); });
rrApiOnReady.push(function() {
try {
	rrApi.order(' . json_encode(
					$data, 
					JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT) . ');
} catch(e) {}
});
</script>';
				
				break;
		}
		
		return $out;
	}

}

?>