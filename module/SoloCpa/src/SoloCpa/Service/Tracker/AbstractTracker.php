<?php

namespace SoloCpa\Service\Tracker;

use Solo\ServiceManager\ProvidesOptions;

abstract class AbstractTracker {
	
	use ProvidesOptions;

	/**
	 *
	 * @var boolean
	 */
	protected $enabled = false;

	/**
	 *
	 * @param boolean $enabled        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCpa\Service\Tracker\AbstractTracker
	 */
	public function enabled($enabled = null) {
		if (null !== $enabled) {
			if (!is_bool($enabled)) {
				throw new \InvalidArgumentException('Enabled must be boolean');
			}
			$this->enabled = $enabled;
			return $this;
		} else {
			return $this->enabled;
		}
	}

}

?>