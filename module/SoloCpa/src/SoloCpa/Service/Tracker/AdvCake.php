<?php

namespace SoloCpa\Service\Tracker;

use SoloCpa\Entity\Tracker\TrackerInterface;

class AdvCake extends AbstractTracker {

	/**
	 *
	 * @param TrackerInterface $tracker        	
	 * @return string
	 */
	public function getCode(TrackerInterface $tracker) {
		$out = [];
		$out[] = '<script type="text/javascript" id="advcakeAsync">';
		/*$out[] = '(function ( a ) {
var b = a.createElement("script");
b.async = 1;
b.src = "//server.adv-cake.ru/int/?r=" + Math.random();
a.body.appendChild( b );
})(document);';*/
		
		$data = [];
		
		switch ($tracker->getType()) {
			case $tracker::TYPE_MAINPAGE:
				$data['pageType'] = 1;
				break;
				
			case $tracker::TYPE_PRODUCT:
				$data['pageType'] = 2;
				$data['currentProduct'] = ['id' => $tracker->getId(), 'name' => $tracker->getName(), 'price' => $tracker->getPrice()];
				if ($tracker->hasCategory()) {
					$data['currentCategory'] = ['id' => $tracker->getCategory()->getId(), 'name' => $tracker->getCategory()->getName()];
				}
				break;
			
			case $tracker::TYPE_CATEGORY:
				$data['pageType'] = 3;
				$data['currentCategory'] = ['id' => $tracker->getId(), 'name' => $tracker->getName()];
				$data['products'] = [];
				foreach ($tracker->getProducts() as $product) {
					$data['products'][] = ['id' => $product->getId(), 'name' => $product->getName()];
				}
				break;
			
			case $tracker::TYPE_BASKET:
				$data['pageType'] = 4;
				break;
				
			case $tracker::TYPE_ORDER:
				$data['pageType'] = 5;
				break;
			
			case $tracker::TYPE_ORDER_THANKYOU:
				$data['pageType'] = 6;
				$data['basketProducts'] = [];
				foreach ($tracker->getPurchasedProducts() as $product) {
					$data['basketProducts'][] = ['id' => $product->getId(), 'name' => $product->getName(), 'price' => $product->getPrice(), 'quantity' => $product->getQuantity()];
				}
				$data['orderInfo'] = ['id' => $tracker->orderInfo()->getNumber(), 'totalPrice' => $tracker->orderInfo()->getAmount()];
				break;
		}
		
		if (0 < count($data)) {
			if ($tracker->userInfo()->hasEmail()) {
				$data['user'] = [
					'email' => $tracker->userInfo()->getEmail() 
				];
			}
			if ($tracker->hasBasketProducts()) {
				$data['basketProducts'] = [];
				foreach ($tracker->getBasketProducts() as $basketProduct) {
					$data['basketProducts'][] = [
						'id' => $basketProduct->getId(),
						'name' => $basketProduct->getName(),
						'price' => $basketProduct->getPrice(),
						'quantity' => $basketProduct->getQuantity() 
					];
				}
			}
			
			$out[] = 'window.advcake_push_data = window.advcake_push_data || function(advcake_data_array){};';
			$out[] = 'window.advcake_data = ' . json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) . ';';
			$out[] = 'window.advcake_push_data(window.advcake_data);';
		}
		
		$out[] = '</script>';
		
		return implode("\n", $out);
	}

}

?>