<?php

namespace SoloCpa\Service\Tracker;

use SoloCpa\Entity\Tracker\TrackerInterface;

class MigCredit extends AbstractTracker {

	/**
	 *
	 * @param TrackerInterface $tracker        	
	 * @return string
	 */
	public function getCode(TrackerInterface $tracker) {
		$out = [];
		
		switch ($tracker->getType()) {
			case $tracker::TYPE_ORDER_THANKYOU:
				$out['IDOrder'] = $tracker->orderInfo()->getNumber();
				$out['city'] = 'Москва';
				$out['order'] = [];
				foreach ($tracker->getPurchasedProducts() as $product) {
					$categoryName = $product->getCategoryName();
					if ($product->hasTag('migcreditCode')) {
						$categoryName = $product->getTag('migcreditCode');
					}
					$out['order'][] = [
						'category' => $categoryName,
						'artikul' => $product->getId(),
						'name' => $product->getName(),
						'price' => $product->getPrice(),
						'count' => $product->getQuantity() 
					];
				}
				break;
		}
		
		$code = '<script type="text/javascript">' . PHP_EOL;
		$code .= 'var migc__order = ' . json_encode($out, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) . ';' . PHP_EOL;
		$code .= '</script>' . PHP_EOL;
		
		return $code;
	}

}

?>