<?php

namespace SoloCpa\Service\Provider;

use SoloCpa\Entity\CpaReserve;
use Solo\Stdlib\StringUtils;
use Solo\Stdlib\ArrayHelper;
use Zend\Http\Client as HttpClient;
use Zend\Http\Request;

class CityAds extends AbstractProvider {

	/**
	 *
	 * @see \SoloCpa\Service\Provider\ProviderInterface::getUid()
	 */
	public function getUid() {
		return 'cityads';
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::initialize()
	 */
	public function initialize() {
		if ($this->request) {
			if ($this->request->getQuery('aip')) {
				$this->setTarget($this->request->getQuery('aip'));
			}
			if ($this->request->getQuery('click_id')) {
				$this->setSource($this->request->getQuery('click_id'));
			}
		}
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::hasSpecialHttpGetParameters()
	 */
	public function hasSpecialHttpGetParameters() {
		$result = false;
		
		if ($this->request && ($this->request->getQuery('aip') || $this->request->getQuery('click_id'))) {
			$result = true;
		}
		
		return $result;
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::getSuccessCode()
	 */
	public function getSuccessCode(CpaReserve $reserve) {
		$params = [];
		$params['click_id'] = $reserve->getPartnerSource();
		$params['customer_type'] = 'E';
		$params['payment_method'] = $reserve->getOrderInfo()->getPaymentMethod();
		$params['order_total'] = $reserve->getAmount();
		$params['currency'] = 'RUR';
		$params['commission'] = $reserve->getReward();
		if ($reserve->getOrderInfo()->getCoupon()) {
			$params['coupon'] = $reserve->getOrderInfo()->getCoupon();
		}
		
		$basket = [];
		foreach ($reserve->getProducts() as $product) {
			$basket[] = [
				'pid' => $product->getSku(),
				'pn' => str_replace('&', 'and', $product->getName()),
				'up' => $product->getPrice(),
				'pc' => str_replace('&', 'and', $product->getCategoryName()),
				'qty' => $product->getQuantity(),
				'pd' => $product->getDiscount() 
			];
		}
		$params['basket'] = json_encode($basket, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		$params['md'] = 2;
		
		$out = [];
		$out[] = StringUtils::format(
			'<script type="text/javascript" async="async" src=\'https://cityadspix.com/track/{orderNumber}/ct/q1/c/{campaignCode}', 
			[
				'orderNumber' => $reserve->getReserveNumber(),
				'campaignCode' => $this->getOption('campaign') 
			]);
		if (0 < count($params)) {
			$out[] = '?' . implode('&', ArrayHelper::joinKeyValue($params));
		}
		$out[] = '\'></script>';
		
		$out[] = '<noscript>';
		$out[] = StringUtils::format(
			'<img src=\'https://cityadspix.com/track/{orderNumber}/ct/q1/c/{campaignCode}', 
			[
				'orderNumber' => $reserve->getReserveNumber(),
				'campaignCode' => $this->getOption('campaign') 
			]);
		if (0 < count($params)) {
			$out[] = '?' . implode('&', ArrayHelper::joinKeyValue($params));
		}
		$out[] = '\' width="1" height="1">';
		$out[] = '</noscript>';
		
		return implode('', $out);
	}

	/**
	 *
	 * @param array $reserves        	
	 * @return string
	 */
	public function formatReportReserves(array $reserves) {
		$writer = xmlwriter_open_memory();
		xmlwriter_start_document($writer, '1.0', 'utf-8');
		
		// items
		xmlwriter_start_element($writer, 'items');
		
		foreach ($reserves as $item) {
			$reserve = $item['reserve'];
			$status = 'New';
			if (isset($item['status'])) $status = $item['status'];
			if (isset($item['Status'])) $status = $item['Status'];
			switch ($item['status']) {
				case CpaReserve::STATUS_INPROGRESS:
					$status = 'New';
					break;
				case CpaReserve::STATUS_DONE:
					$status = 'Done';
					break;
				case CpaReserve::STATUS_CANCELLED:
					$status = 'Cancelled';
					break;
			}
			
			// item
			xmlwriter_start_element($writer, 'item');
			
			xmlwriter_write_element($writer, 'order_id', $reserve->getReserveNumber());
			xmlwriter_write_element($writer, 'click_id', $reserve->getPartnerSource());
			xmlwriter_write_element($writer, 'commission', $reserve->getReward());
			xmlwriter_write_element($writer, 'status', $status);
			xmlwriter_write_element($writer, 'date', $item['UpdateDate']);
			xmlwriter_write_element($writer, 'order_total', $reserve->getAmount());
			xmlwriter_write_element($writer, 'currency', 'RUR');
			xmlwriter_write_element($writer, 'customer_type', 'E');
			xmlwriter_write_element($writer, 'payment_method', $reserve->getOrderInfo()->getPaymentMethod());
			xmlwriter_write_element($writer, 'coupon', $reserve->getOrderInfo()->getCoupon());
			
			// basket
			xmlwriter_start_element($writer, 'basket');
			foreach ($reserve->getProducts() as $product) {
				xmlwriter_write_element($writer, 'pid', $product->getSku());
				xmlwriter_write_element($writer, 'pn', $product->getName());
				xmlwriter_write_element($writer, 'up', $product->getPrice());
				xmlwriter_write_element($writer, 'pc', $product->getCategoryName());
				xmlwriter_write_element($writer, 'qty', $product->getQuantity());
				xmlwriter_write_element($writer, 'pd', $product->getDiscount());
			}
			xmlwriter_end_element($writer);
			// END basket
			
			xmlwriter_end_element($writer);
			// END item
		}
		
		xmlwriter_end_element($writer);
		// END items
		
		return xmlwriter_output_memory($writer);
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::sendPostback()
	 */
	public function sendPostback(CpaReserve $reserve) {
		$client = new HttpClient();
		$client->setEncType($client::ENC_URLENCODED);
		
		if ($reserve->getPartnerSource()) {
			$status = 'New';
			switch ($reserve->getStatusCode()) {
				case CpaReserve::STATUS_INPROGRESS:
					$status = 'New';
					break;
				case CpaReserve::STATUS_DONE:
					$status = 'Done';
					break;
				case CpaReserve::STATUS_CANCELLED:
					$status = 'cancel';
					break;
			}
			
			$params = [
				'order_id' => $reserve->getReserveNumber(),
				'click_id' => $reserve->getPartnerSource(),
				'commission' => $reserve->getReward(),
				'status' => $status,
				'date' => $reserve->getStatusUpdatedAt()->getTimestamp(),
				'order_total' => $reserve->getAmount(),
				'currency' => 'RUR',
				'customer_type' => 'E',
				'payment_method' => $reserve->getOrderInfo()->getPaymentMethod(),
				'coupon' => $reserve->getOrderInfo()->getCoupon() 
			];
			
			$basket = [];
			foreach ($reserve->getProducts() as $product) {
				$basket[] = [
					'pid' => $product->getSku(),
					'pn' => str_replace('&', 'and', $product->getName()),
					'up' => $product->getPrice(),
					'pc' => str_replace('&', 'and', $product->getCategoryName()),
					'qty' => $product->getQuantity(),
					'pd' => $product->getDiscount() 
				];
			}
			$params['basket'] = json_encode($basket, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
			
			$request = new Request();
			$request->setMethod(Request::METHOD_GET);
			$request->setUri('http://cityads.ru/service/postback/');
			foreach ($params as $name => $value) {
				$request->getQuery()->set($name, $value);
			}
			// print $request->getUri().'?'.implode('&', ArrayHelper::joinKeyValue($params))."\n";
			print $request->getUri() . '?' . http_build_query($params) . "\n";
			$response = $client->send($request);
			$responseXml = new \SimpleXMLElement($response->getBody());
			$code = strval($responseXml->code);
			if ('OK' !== $code) {
				return false;
			}
		} else {
			return false;
		}
		
		return true;
	}

}

?>