<?php

namespace SoloCpa\Service\Provider;

use SoloCpa\Entity\CpaReserve;

class Get4Click extends AbstractProvider {

	/**
	 *
	 * @see \SoloCpa\Service\Provider\ProviderInterface::getUid()
	 */
	public function getUid() {
		return 'get4click';
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::initialize()
	 */
	public function initialize() {
		if ($this->request) {
			if ($this->request->getQuery('g4c_distr')) {
				$this->setTarget($this->request->getQuery('g4c_distr'));
			}
			if ($this->request->getQuery('utm_campaign')) {
				$this->setSource($this->request->getQuery('utm_campaign'));
			}
		}
	}
	
	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::hasSpecialHttpGetParameters()
	 */
	public function hasSpecialHttpGetParameters() {
		$result = false;
	
		if ($this->request && $this->request->getQuery('g4c_distr')) {
			$result = true;
		}
	
		return $result;
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::getSuccessCode()
	 */
	public function getSuccessCode(CpaReserve $reserve) {
		return '';
	}
	
	public function formatReportReserves(array $reserves) {
		$out = [];
		
		foreach ($reserves as $item) {
			$line = [];
			
			$reserve = $item['reserve'];
			$statusTitle = '';
			$status = null;
			if (isset($item['status'])) $status = $item['status'];
			if (isset($item['Status'])) $status = $item['Status'];
			switch ($status) {
				case CpaReserve::STATUS_INPROGRESS:
					$statusTitle = 'в обработке';
					break;
				case CpaReserve::STATUS_DONE:
					$statusTitle = 'принят';
					break;
				case CpaReserve::STATUS_CANCELLED:
					$statusTitle = 'отклонен';
					break;
			}
			
			$line[] = $reserve->getReserveNumber();
			$line[] = $reserve->getCreatedAt()->format('m/d/Y H:i');
			$line[] = $reserve->getAmount();
			$line[] = $reserve->getReward();
			$line[] = $statusTitle;
			$line[] = $reserve->getOrderInfo()->getCoupon();
			$line[] = $reserve->getEntryPointUrl();
			
			$out[] = implode(';', $line);
		}
		
		return implode("\n", $out);
	}

}

?>