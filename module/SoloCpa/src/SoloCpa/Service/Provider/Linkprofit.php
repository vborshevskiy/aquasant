<?php

namespace SoloCpa\Service\Provider;

use SoloCpa\Entity\CpaReserve;
use Zend\Http\Client as HttpClient;
use Zend\Http\Request;
use Zend\Http\Response;
use Solo\Stdlib\StringUtils;

class Linkprofit extends AbstractProvider {

	/**
	 *
	 * @see \SoloCpa\Service\Provider\ProviderInterface::getUid()
	 */
	public function getUid() {
		return 'linkprofit';
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::initialize()
	 */
	public function initialize() {
		if ($this->request && $this->request->getQuery('click_hash') && $this->request->getQuery('refid')) {
			$this->setTarget($this->request->getQuery('refid'));
			$this->setSource($this->request->getQuery('click_hash'));
		}
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::hasSpecialHttpGetParameters()
	 */
	public function hasSpecialHttpGetParameters() {
		$result = false;
		
		if ($this->request && ($this->request->getQuery('click_hash') || $this->request->getQuery('refid'))) {
			$result = true;
		}
		
		return $result;
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::getSuccessCode()
	 */
	public function getSuccessCode(CpaReserve $reserve) {
		return '';
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::sendPostback()
	 */
	public function sendPostback(CpaReserve $reserve) {
		$client = new HttpClient();
		$client->setEncType($client::ENC_URLENCODED);
		$client->setOptions([
			'sslverifypeer' => false 
		]);
		
		// send only newly created reserves
		if (CpaReserve::STATUS_INPROGRESS == $reserve->getStatusCode()) {
			$params = [
				'TotalCost' => str_replace(',', '.', $reserve->getReward()),
				'OrderID' => $reserve->getReserveNumber(),
				'CampaignID' => $this->getOption('campaignId'),
				'AffiliateID' => $reserve->getPartnerTarget(),
				'ClickHash' => $reserve->getPartnerSource() 
			];
			
			$request = new Request();
			$request->setMethod(Request::METHOD_GET);
			$request->setUri('https://cpa.linkprofit.ru/sale');
			foreach ($params as $name => $value) {
				$request->getQuery()->set($name, $value);
			}
			$response = $client->send($request);
			if (Response::STATUS_CODE_200 !== $response->getStatusCode()) {
				return false;
			}
		} else {
			$statuses = [
				CpaReserve::STATUS_DONE => 'A',
				CpaReserve::STATUS_CANCELLED => 'D' 
			];
			$url = StringUtils::format(
				'http://s.linkprofit.ru/postback/{campaignUid}/update/{orderId}', 
				[
					'campaignUid' => $this->getOption('campaignUid'),
					'orderId' => $reserve->getReserveNumber() 
				]);
			
			$params = [
				'Status' => $statuses[$reserve->getStatusCode()] 
			];
			
			$request = new Request();
			$request->setMethod(Request::METHOD_GET);
			$request->setUri($url);
			foreach ($params as $name => $value) {
				$request->getQuery()->set($name, $value);
			}
			$response = $client->send($request);
			if (Response::STATUS_CODE_200 !== $response->getStatusCode()) {
				return false;
			}
		}
		
		return true;
	}

}

?>