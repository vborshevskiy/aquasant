<?php

namespace SoloCpa\Service\Provider;

use SoloCpa\Entity\CpaReserve;
use Zend\Http\Client as HttpClient;
use Zend\Http\Request;
use Zend\Http\Response;

class Ad1 extends AbstractProvider {

	/**
	 *
	 * @see \SoloCpa\Service\Provider\ProviderInterface::getUid()
	 */
	public function getUid() {
		return 'ad1';
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::initialize()
	 */
	public function initialize() {
		if ($this->request) {
			if ($this->request->getQuery('utm_content')) {
				$this->setTarget($this->request->getQuery('utm_content'));
			}
			if ($this->request->getQuery('utm_term')) {
				$this->setSource($this->request->getQuery('utm_term'));
			}
		}
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::getSuccessCode()
	 */
	public function getSuccessCode(CpaReserve $reserve) {
		$params = [
			'order_id' => $reserve->getReserveNumber(),
			'n_c' => $reserve->getPartnerSource(),
			'summ' => $reserve->getReward() 
		];
		$code = 'http://t.zm-trk.com/inbox.php?' . http_build_query($params);
		return $code;
	}

	/**
	 *
	 * @param array $reserves        	
	 */
	public function formatReportReserves(array $reserves) {
		$writer = xmlwriter_open_memory();
		xmlwriter_start_document($writer, '1.0', 'utf-8');
		
		// items
		xmlwriter_start_element($writer, 'items');
		
		foreach ($reserves as $item) {
			$reserve = $item['reserve'];
			$status = 'New';
			if (isset($item['status'])) $status = $item['status'];
			if (isset($item['Status'])) $status = $item['Status'];
			switch ($item['status']) {
				case CpaReserve::STATUS_INPROGRESS:
					$status = '2';
					break;
				case CpaReserve::STATUS_DONE:
					$status = '1';
					break;
				case CpaReserve::STATUS_CANCELLED:
					$status = '3';
					break;
			}
			
			// item
			xmlwriter_start_element($writer, 'item');
			
			xmlwriter_write_element($writer, 'id', $reserve->getReserveNumber());
			xmlwriter_write_element($writer, 'status', $status);
			xmlwriter_write_element($writer, 'tid', $reserve->getPartnerSource());
			xmlwriter_write_element($writer, 'price', $reserve->getReward());
			
			xmlwriter_end_element($writer);
			// END item
		}
		
		xmlwriter_end_element($writer);
		// END items
		
		return xmlwriter_output_memory($writer);
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::sendPostback()
	 */
	public function sendPostback(CpaReserve $reserve) {
		$client = new HttpClient();
		$client->setOptions([
			'sslverifypeer' => false 
		]);
		$client->setEncType($client::ENC_URLENCODED);
		
		if ($this->hasOption('code') && $this->hasOption('postback_key')) {
			$i = 1;
			foreach ($reserve->getProducts() as $product) {
				$params[] = [
					'postback' => 1,
					'postback_key' => $this->getOption('postback_key'),
					'campaign_code' => $this->getOption('code'),
					'uid' => $reserve->getPartnerTarget(),
					'action_code' => 8,
					'tariff_code' => ($product->getPartnerCode() ? $product->getPartnerCode() : 0),
					'order_id' => $reserve->getReserveNumber(),
					'price' => $product->getPrice(),
					'position_count' => count($reserve->getProducts()),
					'position_id' => $i,
					'quantity' => $product->getQuantity(),
					'product_id' => $product->getId(),
					'payment_type' => 'sale',
					'client_id' => $reserve->getAgentId(),
					'currency_code' => 'RUB' 
				];
				
				$request = new Request();
				$request->setMethod(Request::METHOD_GET);
				$request->setUri('https://ad.admitad.com/r');
				foreach ($params as $name => $value) {
					$request->getQuery()->set($name, $value);
				}
				$response = $client->send($request);
				if (Response::STATUS_CODE_200 !== $response->getStatusCode()) {
					return false;
				}
				
				$i++;
			}
		}
		
		return true;
	}

}

?>