<?php

namespace SoloCpa\Service\Provider;

use SoloCpa\Entity\CpaReserve;
use Solo\Stdlib\StringUtils;
use Zend\Http\Client as HttpClient;
use Zend\Http\Request;
use Zend\Http\Response;

class Actionpay extends AbstractProvider {

	/**
	 *
	 * @see \SoloCpa\Service\Provider\ProviderInterface::getUid()
	 */
	public function getUid() {
		return 'actionpay';
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::initialize()
	 */
	public function initialize() {
		if ($this->request && $this->request->getQuery('actionpay')) {
			$parts = explode('.', $this->request->getQuery('actionpay'));
			if (0 < count($parts)) {
				$target = trim($parts[0]);
				if (!empty($target)) {
					$this->setTarget($target);
				}
				if (isset($parts[1])) {
					$source = trim($parts[1]);
					if (!empty($source)) {
						$this->setSource($source);
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * @see \SoloCpa\Service\Provider\AbstractProvider::hasSpecialHttpGetParameters()
	 */
	public function hasSpecialHttpGetParameters() {
		$result = false;
		
		if ($this->request && $this->request->getQuery('actionpay')) {
			$result = true;
		}
		
		return $result;
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::getSuccessCode()
	 */
	public function getSuccessCode(CpaReserve $reserve) {
		$out = '';
		if (($reserve->getCampaignId() == $this->getId()) && $this->hasOption('code')) {
			$code = $this->getOption('code');
			$multiplier = ($this->hasOption('multiplier') ? $this->getOption('multiplier') : 1);
			foreach ($reserve->getPartnerCodes() as $partnerCode) {
				$out .= StringUtils::format(
					'<img src="http://apypxl.com/ok/{code}.png?actionpay={target}.{source}&apid={reserveId}&price={price}" height="1" width="1" />', 
					[
						'code' => $code,
						'target' => $this->getTarget(),
						'source' => $this->getSource(),
						'reserveId' => $reserve->getReserveNumber(),
						'price' => $this->calculateReward($reserve->getReward()) 
					]);
			}
		}
		return $out;
	}

	/**
	 *
	 * @param array $reserves        	
	 * @return string
	 */
	public function formatReportReserves(array $reserves) {
		$writer = xmlwriter_open_memory();
		xmlwriter_start_document($writer, '1.0', 'utf-8');
		
		// items
		xmlwriter_start_element($writer, 'items');
		
		foreach ($reserves as $item) {
			$reserve = $item['reserve'];
			$statusCode = 4;
			$status = null;
			if (isset($item['status'])) $status = $item['status'];
			if (isset($item['Status'])) $status = $item['Status'];
			switch ($status) {
				case CpaReserve::STATUS_INPROGRESS:
					$statusCode = 2;
					break;
				case CpaReserve::STATUS_DONE:
					$statusCode = 1;
					break;
				case CpaReserve::STATUS_CANCELLED:
					$statusCode = 3;
					break;
			}
			
			// item
			xmlwriter_start_element($writer, 'item');
			
			xmlwriter_write_element($writer, 'id', $reserve->getReserveNumber());
			xmlwriter_write_element($writer, 'click', $reserve->getPartnerTarget());
			xmlwriter_write_element($writer, 'source', $reserve->getPartnerSource());
			xmlwriter_write_element($writer, 'price', $this->calculateReward($reserve->getReward()));
			xmlwriter_write_element($writer, 'status', $statusCode);
			xmlwriter_write_element($writer, 'date', $reserve->getCreatedAt()->format('d.m.Y H:i:s'));
			xmlwriter_write_element($writer, 'aim', $this->getOption('code'));
			
			xmlwriter_end_element($writer);
			// END item
		}
		
		xmlwriter_end_element($writer);
		// END items
		
		return xmlwriter_output_memory($writer);
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::sendPostback()
	 */
	public function sendPostback(CpaReserve $reserve) {
		$client = new HttpClient();
		$client->setEncType($client::ENC_URLENCODED);
		
		if (CpaReserve::STATUS_INPROGRESS == $reserve->getStatusCode()) {
			$params = [
				'actionpay' => sprintf('%s.%s', $reserve->getPartnerTarget(), $reserve->getPartnerSource()),
				'apid' => $reserve->getReserveNumber(),
				'price' => $this->calculateReward($reserve->getReward()),
				'ip' => $this->getOption('ip') 
			];
			
			$request = new Request();
			$request->setMethod(Request::METHOD_GET);
			$request->setUri(StringUtils::format('http://x.actionpay.ru/ok/{aim}.png', [
				'aim' => $this->getOption('code') 
			]));
			foreach ($params as $name => $value) {
				$request->getQuery()->set($name, $value);
			}
			$response = $client->send($request);
			if (Response::STATUS_CODE_200 !== $response->getStatusCode()) {
				return false;
			}
		} else {
			if ($this->hasOption('code') && $this->hasOption('postback_key')) {
				$status = '';
				if (CpaReserve::STATUS_DONE == $reserve->getStatusCode()) {
					$status = 'accept';
				} elseif (CpaReserve::STATUS_CANCELLED == $reserve->getStatusCode()) {
					$status = 'reject';
				}
				
				if ($status) {
					$params = [
						'key' => $this->getOption('postback_key'),
						'aim' => $this->getOption('code'),
						'status' => $status,
						'actionpay' => sprintf('%s.%s', $reserve->getPartnerTarget(), $reserve->getPartnerSource()),
						'apid' => $reserve->getReserveNumber(),
						'price' => $this->calculateReward($reserve->getReward()),
						'ip' => $this->getOption('ip') 
					];
					
					$request = new Request();
					$request->setMethod(Request::METHOD_GET);
					$request->setUri('http://n.actionpay.ru/status/');
					foreach ($params as $name => $value) {
						$request->getQuery()->set($name, $value);
					}
					$response = $client->send($request);
					if (Response::STATUS_CODE_200 !== $response->getStatusCode()) {
						return false;
					}
				} else {
					return false;
				}
			}
		}
		
		return true;
	}

	/**
	 *
	 * @param float $rewardAmount        	
	 * @return float
	 */
	private function calculateReward($rewardAmount) {
		$multiplier = ($this->hasOption('multiplier') ? $this->getOption('multiplier') : 1);
		return round($rewardAmount * $multiplier, 2);
	}

}

?>