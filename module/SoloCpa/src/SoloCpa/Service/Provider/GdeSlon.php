<?php

namespace SoloCpa\Service\Provider;

use SoloCpa\Entity\CpaReserve;
use Solo\Stdlib\StringUtils;
use Solo\DateTime\DateTime;

class GdeSlon extends AbstractProvider {

	/**
	 *
	 * @see \SoloCpa\Service\Provider\ProviderInterface::getUid()
	 */
	public function getUid() {
		return 'gdeslon';
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::initialize()
	 */
	public function initialize() {
		if ($this->request && $this->request->getQuery('gsaid')) {
			$this->setTarget($this->request->getQuery('gsaid'));
		}
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::hasSpecialHttpGetParameters()
	 */
	public function hasSpecialHttpGetParameters() {
		$result = false;
		
		if ($this->request && $this->request->getQuery('gsaid')) {
			$result = true;
		}
		
		return $result;
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::getSuccessCode()
	 */
	public function getSuccessCode(CpaReserve $reserve) {
		$codes = [];
		foreach ($reserve->getProducts() as $product) {
			for ($i = 0; $i < $product->getQuantity(); $i++) {
				$codes[] = sprintf('%d:%d', $product->getId(), $product->getPrice());
			}
		}
		$codes[] = sprintf('001:%s', str_replace(',', '.', strval($reserve->getReward())));
		$out = StringUtils::format(
			'<script type="text/javascript" src="https://www.gdeslon.ru/thanks.js?codes={codes}&order_id={order_id}&merchant_id={merchant_id}"></script>', 
			[
				'codes' => implode(',', $codes),
				'order_id' => $reserve->getReserveNumber(),
				'merchant_id' => $this->getOption('merchant_id') 
			]);
		return $out;
	}

	/**
	 *
	 * @param array $reserves        	
	 */
	public function formatReportReserves(array $reserves) {
		$writer = xmlwriter_open_memory();
		xmlwriter_start_document($writer, '1.0', 'utf-8');
		
		// sales_report
		xmlwriter_start_element($writer, 'sales_report');
		xmlwriter_write_attribute($writer, 'date', DateTime::now()->format('Y-m-d H:i:s'));
		
		foreach ($reserves as $item) {
			$reserve = $item['reserve'];
			switch ($item['status']) {
				case CpaReserve::STATUS_INPROGRESS:
					$statusCode = 2;
					break;
				case CpaReserve::STATUS_DONE:
					$statusCode = 0;
					break;
				case CpaReserve::STATUS_CANCELLED:
					$statusCode = 1;
					break;
			}
			
			// sale
			foreach ($reserve->getProducts() as $product) {
				xmlwriter_start_element($writer, 'sale');
				
				xmlwriter_write_element($writer, 'order_id', $reserve->getReserveNumber());
				xmlwriter_write_element($writer, 'article', ($product->hasSku() ? $product->getSku() : $product->getId()));
				xmlwriter_write_element($writer, 'date', $reserve->getCreatedAt()->format('Y-m-d H:i:s'));
				xmlwriter_write_element($writer, 'status', $statusCode);
				xmlwriter_write_element($writer, 'price', $product->getPrice());
				if ($reserve->getPartnerTarget()) {
					xmlwriter_write_element($writer, 'affiliate_id', $reserve->getPartnerTarget());
				}
				xmlwriter_write_element($writer, 'cancel_reason', '');
				
				xmlwriter_end_element($writer);
			}
			// END sale
			
			// reward
			xmlwriter_start_element($writer, 'sale');
			
			xmlwriter_write_element($writer, 'order_id', $reserve->getReserveNumber());
			xmlwriter_write_element($writer, 'article', '001');
			xmlwriter_write_element($writer, 'date', $reserve->getCreatedAt()->format('Y-m-d H:i:s'));
			xmlwriter_write_element($writer, 'status', $statusCode);
			xmlwriter_write_element($writer, 'price', $reserve->getReward());
			if ($reserve->getPartnerTarget()) {
				xmlwriter_write_element($writer, 'affiliate_id', $reserve->getPartnerTarget());
			}
			xmlwriter_write_element($writer, 'cancel_reason', '');
			
			xmlwriter_end_element($writer);
			// END reward
		}
		
		xmlwriter_end_element($writer);
		// END sales_report
		
		return xmlwriter_output_memory($writer);
	}

}

?>