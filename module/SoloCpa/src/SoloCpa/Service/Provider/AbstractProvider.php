<?php

namespace SoloCpa\Service\Provider;

use Solo\DateTime\DateTime;
use Zend\Http\Request;
use SoloCpa\Entity\CpaReserve;
use Solo\ServiceManager\ProvidesOptions;

abstract class AbstractProvider implements ProviderInterface {
	
	use ProvidesOptions;

	/**
	 *
	 * @var integer
	 */
	protected $id;

	/**
	 *
	 * @var string
	 */
	protected $source;

	/**
	 *
	 * @var string
	 */
	protected $target;

	/**
	 *
	 * @var DateTime
	 */
	protected $createdAt;

	/**
	 *
	 * @var string
	 */
	protected $uid;

	/**
	 *
	 * @var array
	 */
	protected $utmParameters = [];

	/**
	 *
	 * @var string
	 */
	private $trackUid;

	/**
	 *
	 * @var string
	 */
	private $entryPointUrl;

	/**
	 *
	 * @var Request
	 */
	protected $request;

	/**
	 *
	 * @see \SoloCpa\Service\Provider\ProviderInterface::setRequest()
	 */
	public function setRequest(Request $request) {
		$this->request = $request;
		if ($request) {
			$this->initializeUtmParameters($request);
			$this->initializeTrackUid($request);
			$this->initializeEntryPointUrl($request);
		}
		return $this;
	}

	/**
	 *
	 * @param Request $request        	
	 */
	private function initializeUtmParameters(Request $request) {
		$this->utmParameters['source'] = $this->request->getQuery('utm_source');
		$this->utmParameters['medium'] = $this->request->getQuery('utm_medium');
		$this->utmParameters['term'] = $this->request->getQuery('utm_term');
		$this->utmParameters['content'] = $this->request->getQuery('utm_content');
		$this->utmParameters['campaign'] = $this->request->getQuery('utm_campaign');
	}

	/**
	 *
	 * @param Request $request        	
	 */
	private function initializeTrackUid(Request $request) {
		if ($request->getQuery('utm_source')) {
			$this->setTrackUid(md5(uniqid('', true)));
		}
	}

	/**
	 *
	 * @param Request $request        	
	 */
	private function initializeEntryPointUrl(Request $request) {
		if ($request->getQuery('utm_source')) {
			$this->setEntryPointUrl($request->getUri()->toString());
		}
	}

	/**
	 *
	 * @param string $name        	
	 * @return boolean
	 */
	protected function hasUtmParameter($name) {
		return array_key_exists($name, $this->utmParameters);
	}

	/**
	 *
	 * @param string $name        	
	 * @return string
	 */
	protected function getUtmParameter($name) {
		$value = '';
		if ($this->hasUtmParameter($name)) {
			$value = $this->utmParameters[$name];
		}
		return $value;
	}

	/**
	 *
	 * @param string $uid        	
	 * @return \SoloCpa\Service\Provider\AbstractProvider
	 */
	public function setUid($uid) {
		$this->uid = $uid;
		return $this;
	}

	/**
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param integer $id        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCpa\Service\Provider\AbstractProvider
	 */
	public function setId($id) {
		if (!is_numeric($id)) {
			throw new \InvalidArgumentException('Id must be numeric');
		}
		$this->id = intval($id);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getSource() {
		return $this->source;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasSource() {
		return !empty($this->source);
	}

	/**
	 *
	 * @param string $source        	
	 * @return \SoloCpa\Service\Provider\AbstractProvider
	 */
	public function setSource($source) {
		$this->source = $source;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getTarget() {
		return $this->target;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasTarget() {
		return !empty($this->target);
	}

	/**
	 *
	 * @param string $target        	
	 * @return \SoloCpa\Service\Provider\AbstractProvider
	 */
	public function setTarget($target) {
		$this->target = $target;
		return $this;
	}

	/**
	 *
	 * @return \Solo\DateTime\DateTime
	 */
	public function getCreatedAt() {
		return $this->createdAt;
	}

	/**
	 *
	 * @param DateTime $createdAt        	
	 * @return \SoloCpa\Service\Provider\AbstractProvider
	 */
	public function setCreatedAt(DateTime $createdAt) {
		$this->createdAt = $createdAt;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getTrackUid() {
		return $this->trackUid;
	}

	/**
	 *
	 * @param string $trackUid        	
	 */
	public function setTrackUid($trackUid) {
		$this->trackUid = $trackUid;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getEntryPointUrl() {
		return $this->entryPointUrl;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasEntryPointUrl() {
		return !empty($this->entryPointUrl);
	}

	/**
	 *
	 * @param string $entryPointUrl        	
	 */
	public function setEntryPointUrl($entryPointUrl) {
		$this->entryPointUrl = $entryPointUrl;
		return $this;
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\ProviderInterface::toString()
	 */
	public function toString() {
		$parts = [];
		$parts[] = $this->getUid();
		if ($this->hasSource()) {
			$parts[] = sprintf('s:%s', $this->getSource());
		}
		if ($this->hasTarget()) {
			$parts[] = sprintf('t:%s', $this->getTarget());
		}
		if ($this->getCreatedAt()) {
			$parts[] = sprintf('c:%d', $this->getCreatedAt()->getTimestamp());
		}
		foreach ($this->utmParameters as $paramName => $paramValue) {
			$parts[] = sprintf('utm_%s:%s', $paramName, $paramValue);
		}
		if ($this->getTrackUid()) {
			$parts[] = sprintf('tu:%s', $this->getTrackUid());
		}
		return implode('|', $parts);
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\ProviderInterface::fromString()
	 */
	public function fromString($serialized) {
		$parts = explode('|', $serialized);
		foreach ($parts as $part) {
			if (!empty($part)) {
				$pos = stripos($part, ':');
				if (false !== $pos) {
					$prefix = substr($part, 0, $pos);
					$value = substr($part, $pos + 1);
					if (!empty($value)) {
						switch ($prefix) {
							case 's':
								$this->setSource($value);
								break;
							case 't':
								$this->setTarget($value);
								break;
							case 'c':
								$this->setCreatedAt(DateTime::createFromTimestamp($value));
								break;
							case 'tu':
								$this->setTrackUid($value);
								break;
							default:
								if (false !== stripos($prefix, 'utm_')) {
									$utmParamName = trim(substr($prefix, strlen('utm_')));
									if (!empty($utmParamName)) {
										$this->utmParameters[$utmParamName] = $value;
									}
								}
								break;
						}
					}
				} else {
					$this->setUid($part);
				}
			}
		}
	}

	/**
	 *
	 * @param CpaReserve $reserve        	
	 * @throws \RuntimeException
	 * @return boolean
	 */
	public function sendPostback(CpaReserve $reserve) {
		throw new \RuntimeException('Method not implemented');
	}

	/**
	 * Initialize provider
	 */
	abstract public function initialize();

	/**
	 *
	 * @param CpaReserve $reserve        	
	 * @return string
	 */
	abstract public function getSuccessCode(CpaReserve $reserve);

	/**
	 *
	 * @return boolean
	 */
	public function hasSpecialHttpGetParameters() {
		return false;
	}

}

?>