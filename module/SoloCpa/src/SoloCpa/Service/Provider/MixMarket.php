<?php

namespace SoloCpa\Service\Provider;

use SoloCpa\Entity\CpaReserve;
use Solo\Stdlib\StringUtils;
use Zend\Http\Client as HttpClient;
use Zend\Http\Request;

class MixMarket extends AbstractProvider {

	/**
	 *
	 * @see \SoloCpa\Service\Provider\ProviderInterface::getUid()
	 */
	public function getUid() {
		return 'mixmarket';
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::initialize()
	 */
	public function initialize() {
		// do nothing
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::getSuccessCode()
	 */
	public function getSuccessCode(CpaReserve $reserve) {
		$out = StringUtils::format(
			'<script>
var univar1=\'{reserveNumber}\';
var univar2=\'{reserveAmount}\';
document.write(\'<img src="//mixmarket.biz/uni/tev.php?id=1294939192&r=\'+escape(document.referrer)+\'&t=\'+(new Date()).getTime()+\'&a1={reserveNumber}&a2={reserveAmount}" width="1" height="1"/>\');</script>
<noscript><img src="//mixmarket.biz/uni/tev.php?id=1294939192&a1={reserveNumber}&a2={reserveAmount}" width="1" height="1"/></noscript>', 
			[
				'reserveNumber' => $reserve->getReserveNumber(),
				'reserveAmount' => $reserve->getAmount() 
			]);
		return $out;
	}

	/**
	 *
	 * @param array $reserves        	
	 * @return string
	 */
	private function formatReportReserves(array $reserves, $conditionId) {
		$writer = xmlwriter_open_memory();
		xmlwriter_start_document($writer, '1.0', 'windows-1251');
		
		// uni
		xmlwriter_start_element($writer, 'uni');
		xmlwriter_write_attribute($writer, 'version', '1.0');
		
		xmlwriter_start_element($writer, 'condition');
		xmlwriter_write_attribute($writer, 'id', $conditionId);
		
		foreach ($reserves as $reserve) {
			// object
			xmlwriter_start_element($writer, 'object');
			
			xmlwriter_write_element($writer, 'id', $reserve->getReserveNumber());
			xmlwriter_write_element($writer, 'price', $reserve->getReward());
			
			xmlwriter_end_element($writer);
			// END object
		}
		
		xmlwriter_end_element($writer);
		xmlwriter_end_element($writer);
		// END uni
		
		return xmlwriter_output_memory($writer);
	}

	public function sendPostback(CpaReserve $reserve) {
		switch ($reserve->getStatusCode()) {
			case CpaReserve::STATUS_INPROGRESS:
				$this->trackNewReserves([$reserve]);
				break;
			case CpaReserve::STATUS_DONE:
				$this->trackPaidReserves([$reserve]);
				break;
			case CpaReserve::STATUS_CANCELLED:
				$this->trackCancelledReserves([$reserve]);
				break;
		}
	}

	/**
	 *
	 * @param array $reserves        	
	 */
	public function trackNewReserves(array $reserves) {
		$options = $this->getOption('new_reserves');
		$xml = $this->formatReportReserves($reserves, $options['id']);
		
		$request = new Request();
		$request->setUri(
			StringUtils::format(
				'http://mixmarket.biz/uni/gate.php?cid={id}&hash1={hash}&pass_={password}&e=ask', 
				[
					'id' => $options['id'],
					'hash' => $options['hash'],
					'password' => $this->getOption('password') 
				]));
		$request->setMethod($request::METHOD_POST);
		$request->getPost()->set('msg', $xml);
		print "Request:\n";
		print $request->getUri()->toString()."\n";
		print $xml."\n";
		print "\n";
		
		$client = new HttpClient();
		$client->setEncType($client::ENC_URLENCODED);
		$response = $client->send($request);
		$responseXml = new \SimpleXMLElement($response->getContent());
		print "Response:\n";
		print $response->getContent()."\n";
		print "\n";
		return ($responseXml && $responseXml->status && ('ok' == strval($responseXml->status)));
	}

	/**
	 *
	 * @param array $reserves        	
	 */
	public function trackPaidReserves(array $reserves) {
		$options = $this->getOption('paid_reserves');
		$xml = $this->formatReportReserves($reserves, $options['id']);
		
		$request = new Request();
		$request->setUri(
			StringUtils::format(
				'http://mixmarket.biz/uni/gate.php?cid={id}&hash1={hash}&pass_={password}&e=send', 
				[
					'id' => $options['id'],
					'hash' => $options['hash'],
					'password' => $this->getOption('password') 
				]));
		$request->setMethod($request::METHOD_POST);
		$request->getPost()->set('msg', $xml);
		
		$client = new HttpClient();
		$client->setEncType($client::ENC_URLENCODED);
		$response = $client->send($request);
		$responseXml = new \SimpleXMLElement($response->getContent());
		return ($responseXml && $responseXml->status && ('ok' == strval($responseXml->status)));
	}

	/**
	 *
	 * @param array $reserves        	
	 */
	public function trackCancelledReserves(array $reserves) {
		$options = $this->getOption('cancelled_reserves');
		$xml = $this->formatReportReserves($reserves, $options['id']);
		
		$request = new Request();
		$request->setUri(
			StringUtils::format(
				'http://mixmarket.biz/uni/gate.php?cid={id}&hash1={hash}&pass_={password}&e=send', 
				[
					'id' => $options['id'],
					'hash' => $options['hash'],
					'password' => $this->getOption('password') 
				]));
		$request->setMethod($request::METHOD_POST);
		$request->getPost()->set('msg', $xml);
		
		$client = new HttpClient();
		$client->setEncType($client::ENC_URLENCODED);
		$response = $client->send($request);
		$responseXml = new \SimpleXMLElement($response->getContent());
		return ($responseXml && $responseXml->status && ('ok' == strval($responseXml->status)));
	}

}

?>