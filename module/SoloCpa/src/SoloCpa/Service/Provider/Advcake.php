<?php

namespace SoloCpa\Service\Provider;

use SoloCpa\Entity\CpaReserve;
use Solo\Cookie\Cookie;
use Solo\DateTime\DateTime;

class Advcake extends AbstractProvider {
	
	/**
	 *
	 * @var integer
	 */
	private $updateDoneReservesInDays = 7;
	
	/**
	 *
	 * @see \SoloCpa\Service\Provider\ProviderInterface::getUid()
	 */
	public function getUid() {
		return 'advcake';
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::initialize()
	 */
	public function initialize() {
		if ($this->request) {
			$expire = $this->getOption('cookie_expire', '30d');
			Cookie::set('advcake_trackid', $this->getTrackUid(), $expire);
			
			$this->setSource('advcake');
			$this->setTarget('advcake');
		}
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::getSuccessCode()
	 */
	public function getSuccessCode(CpaReserve $reserve) {
		return '';
	}

	/**
	 *
	 * @param array $reserves        	
	 * @return string
	 */
	public function formatReportReserves(array $reserves) {
		$writer = xmlwriter_open_memory();
		xmlwriter_start_document($writer, '1.0', 'utf-8');
		
		// orders
		xmlwriter_start_element($writer, 'orders');
		
		foreach ($reserves as $item) {
			$reserve = $item['reserve'];
			
			if ($reserve instanceof CpaReserve) {
				$status = -1;
				switch ($item['status']) {
					case CpaReserve::STATUS_INPROGRESS:
						$status = 1;
						break;
					case CpaReserve::STATUS_DONE:
						$diff = $reserve->getStatusUpdatedAt()->diff(DateTime::now(), true);
						if (false && $this->updateDoneReservesInDays > $diff->days) {
							$status = 1;
						} else {
							$status = 2;
						}
						break;
					case CpaReserve::STATUS_CANCELLED:
						$status = 3;
						break;
				}
				
				// order
				xmlwriter_start_element($writer, 'order');
				
				xmlwriter_write_element($writer, 'orderId', $reserve->getReserveNumber());
				xmlwriter_write_element($writer, 'orderPrice', $reserve->getAmount());
				xmlwriter_write_element($writer, 'orderStatus', $status);
				
				// orderBasket
				$basket = [];
				foreach ($reserve->getProducts() as $product) {
					$categoryIds = [];
					$categoryNames = [];
					foreach ($product->getCategories() as $category) {
						$categoryIds[] = $category['id'];
						$categoryNames[] = str_replace(',', '', $category['name']);
					}
					
					$basket[] = [
						'id' => $product->getSku(),
						'name' => $product->getName(),
						'price' => $product->getPrice(),
						'quantity' => $product->getQuantity(),
						'marketing_cat' => ($product->getPartnerCode() ? $product->getPartnerCode() : 0),
						'cat_ids' => implode(',', $categoryIds),
						'cat_names' => implode(',', $categoryNames)
					];
				}
				xmlwriter_write_element($writer, 'orderBasket', json_encode($basket, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
				// END orderBasket
				
				xmlwriter_write_element($writer, 'orderTrackid', $reserve->getTrackUid());
				xmlwriter_write_element($writer, 'coupon', $reserve->getOrderInfo()->getCoupon());
				xmlwriter_write_element($writer, 'marketing_cat', implode(',', $reserve->getPartnerCodes()));
				xmlwriter_write_element($writer, 'dateCreate', $reserve->getCreatedAt()->format('Y-m-d H:i'));
				xmlwriter_write_element($writer, 'dateLastChange', $reserve->getStatusUpdatedAt()->format('Y-m-d H:i'));
				xmlwriter_write_element($writer, 'url', $reserve->getEntryPointUrl());
				
				xmlwriter_end_element($writer);
			}
			// END order
		}
		
		xmlwriter_end_element($writer);
		// END orders
		
		return xmlwriter_output_memory($writer);
	}

}

?>