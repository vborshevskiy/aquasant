<?php

namespace SoloCpa\Service\Provider;

use Zend\Http\Request;

interface ProviderInterface {

	/**
	 *
	 * @return string
	 */
	public function getUid();

	/**
	 *
	 * @param Request $request        	
	 * @return ProviderInterface
	 */
	public function setRequest(Request $request);

	/**
	 *
	 * @return string
	 */
	public function toString();

	/**
	 *
	 * @param string $serialized        	
	 * @return ProviderInterface
	 */
	public function fromString($serialized);

}

?>