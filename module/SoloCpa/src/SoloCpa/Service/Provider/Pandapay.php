<?php

namespace SoloCpa\Service\Provider;

use SoloCpa\Entity\CpaReserve;
use Zend\Http\Client as HttpClient;
use Zend\Http\Request;
use Zend\Http\Response;

class Pandapay extends AbstractProvider {

	/**
	 *
	 * @see \SoloCpa\Service\Provider\ProviderInterface::getUid()
	 */
	public function getUid() {
		return 'pandapay';
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::initialize()
	 */
	public function initialize() {
		// don nothing
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::getSuccessCode()
	 */
	public function getSuccessCode(CpaReserve $reserve) {
		return '';
	}

	public function checkPromocode($promocode) {
		$client = new HttpClient();
		$client->setEncType($client::ENC_URLENCODED);
		
		$params = [
			'code' => $promocode 
		];
		
		$request = new Request();
		$request->setMethod(Request::METHOD_GET);
		$request->setUri('http://pandapay.ru/api/promocode/check/');
		foreach ($params as $name => $value) {
			$request->getQuery()->set($name, $value);
		}
		$response = $client->send($request);
		if (Response::STATUS_CODE_200 !== $response->getStatusCode()) {
			return false;
		}
		$responseJson = json_decode($response->getBody(), true);
		if (isset($responseJson['success']) && (true === $responseJson['success'])) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @see \SoloCpa\Service\Provider\AbstractProvider::sendPostback()
	 */
	public function sendPostback(CpaReserve $reserve) {
		$client = new HttpClient();
		$client->setEncType($client::ENC_URLENCODED);
		
		$params = [
			'code' => $reserve->getOrderInfo()->getCoupon(),
			'shop' => $this->getOption('shop'),
			'aim' => $this->getOption('code'),
			'price' => $reserve->getAmount(),
			'apid' => $reserve->getReserveId() 
		];
		
		$request = new Request();
		$request->setMethod(Request::METHOD_GET);
		$request->setUri('http://pandapay.ru/api/promocode/activate/');
		foreach ($params as $name => $value) {
			$request->getQuery()->set($name, $value);
		}
		$response = $client->send($request);
		if (Response::STATUS_CODE_200 !== $response->getStatusCode()) {
			return false;
		}
		$responseJson = json_decode($response->getBody(), true);
		if (isset($responseJson['success']) && (true === $responseJson['success'])) {
			return true;
		}
		return false;
	}

}

?>