<?php

namespace SoloCpa\Service\Provider;

use SoloCpa\Entity\CpaReserve;
use Zend\Http\Client as HttpClient;
use Zend\Http\Request;
use Zend\Http\Response;

class Admitad extends AbstractProvider {

	/**
	 *
	 * @see \SoloCpa\Service\Provider\ProviderInterface::getUid()
	 */
	public function getUid() {
		return 'admitad';
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::initialize()
	 */
	public function initialize() {
		if ($this->request) {
			if ($this->request->getQuery('admitad_uid')) {
				$this->setTarget($this->request->getQuery('admitad_uid'));
			}
			if ($this->request->getQuery('utm_campaign')) {
				$this->setSource($this->request->getQuery('utm_campaign'));
			}
		}
	}
	
	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::hasSpecialHttpGetParameters()
	 */
	public function hasSpecialHttpGetParameters() {
		$result = false;
	
		if ($this->request && $this->request->getQuery('admitad_uid')) {
			$result = true;
		}
	
		return $result;
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::getSuccessCode()
	 */
	public function getSuccessCode(CpaReserve $reserve) {
		$out = [];
		if ($this->hasOption('code')) {
			$prefix = '_admitad';
			if ($this->hasUtmParameter('source') && (0 == strcasecmp('postview', $this->getUtmParameter('source')))) {
				$prefix = '_pv';
			}
			
			$out[] = '<script type="text/javascript">
(function (d, w) {
w.'.$prefix.'Pixel = {
response_type: \'img\',
action_code: \'7\',
campaign_code: \'' . $this->getOption('code') . '\',
payment_type: \'sale\'
};
w.'.$prefix.'Positions = w.'.$prefix.'Positions || [];';
			
			$products = [];
			$i = 1;
			foreach ($reserve->getProducts() as $product) {
				$products[] = [
					'uid' => $this->getTarget(),
					'order_id' => $reserve->getReserveNumber(),
					'position_id' => $i,
					'client_id' => $reserve->getAgentId(),
					'tariff_code' => ($product->getPartnerCode() ? $product->getPartnerCode() : 0),
					'currency_code' => 'RUB',
					'position_count' => count($reserve->getProducts()),
					'price' => $product->getPrice(),
					'quantity' => $product->getQuantity(),
					'product_id' => $product->getId(),
					'payment_type' => 'sale',
					'payment' => $product->getReward(),
					'screen' => '',
					'tracking' => '' 
				];
				$i++;
			}
			foreach ($products as $product) {
				$out[] = 'w.'.$prefix.'Positions.push(' . json_encode($product, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT) . ');';
			}
			
			$out[] = 'var id = \''.$prefix.'-pixel\';
if (d.getElementById(id)) { return; }
var s = d.createElement(\'script\');
s.id = id;
var r = (new Date).getTime();
var protocol = (d.location.protocol === \'https:\' ? \'https:\' : \'http:\');
s.src = protocol + \'//cdn.asbmit.com/static/js/'.($this->hasUtmParameter('source') && (0 == strcasecmp('postview', $this->getUtmParameter('source'))) ? 'pvpixel.min.js' : 'ad/pixel.min.js').'?r=\' + r;
d.head.appendChild(s);
})(document, window)
</script>';
			
			// images
			$out[] = '<noscript>';
			foreach ($products as $product) {
				$query = [];
				$query['uid'] = $this->getTarget();
				$query['campaign_code'] = $this->getOption('code');
				$query['action_code'] = implode(',', $reserve->getPartnerCodes());
				$query['response_type'] = 'img';
				$query['payment_type'] = $product['payment_type'];
				$query['order_id'] = $product['order_id'];
				$query['tariff_code'] = $product['tariff_code'];
				$query['currency_code'] = $product['currency_code'];
				$query['price'] = $product['price'];
				$query['quantity'] = $product['quantity'];
				$query['product_id'] = $product['product_id'];
				$query['position_id'] = $product['position_id'];
				$query['client_id'] = $product['client_id'];
				$query['position_count'] = $product['position_count'];
				
				$out[] = '<img src="//ad.admitad.com/r?' . implode('&', $query) . '" width="1" height="1" alt="" />';
			}
			$out[] = '</noscript>';
			// END images
		}
		return implode("\n", $out);
	}

	/**
	 *
	 * @param array $reserves        	
	 * @return string
	 */
	public function formatReportReserves(array $reserves) {
		$writer = xmlwriter_open_memory();
		xmlwriter_start_document($writer, '1.0', 'utf-8');
		
		// payments
		xmlwriter_start_element($writer, 'Payments');
		xmlwriter_write_attribute($writer, 'xmlns', 'http://admitad.com/payments-revision');
		
		foreach ($reserves as $item) {
			$reserve = $item['reserve'];
			
			// Payment
			if (in_array($item['status'], [1, 2])) {
				xmlwriter_start_element($writer, 'Payment');
				
				xmlwriter_write_element($writer, 'OrderID', $reserve->getReserveNumber());
				xmlwriter_write_element($writer, 'OrderAmount', $reserve->getAmount());
				xmlwriter_write_element($writer, 'Reward', $reserve->getReward());
				xmlwriter_write_element($writer, 'Status', $item['status']);
				
				xmlwriter_end_element($writer);
			}
			// END Payment
		}
		
		xmlwriter_end_element($writer);
		// END payments
		
		return xmlwriter_output_memory($writer);
	}

	/**
	 *
	 * @see \SoloCpa\Service\Provider\AbstractProvider::sendPostback()
	 */
	public function sendPostback(CpaReserve $reserve) {
		$client = new HttpClient();
		$client->setOptions([
			'sslverifypeer' => false 
		]);
		$client->setEncType($client::ENC_URLENCODED);
		
		$actionCode = 7;
		if ($reserve->getOrderInfo()->getCoupon()) {
			$actionCode = 9;
		}
		if ($this->hasOption('code') && $this->hasOption('postback_key')) {
			$i = 1;
			foreach ($reserve->getProducts() as $product) {
				$params = [
					'postback' => 1,
					'postback_key' => $this->getOption('postback_key'),
					'campaign_code' => $this->getOption('code'),
					'uid' => $reserve->getPartnerTarget(),
					'action_code' => $actionCode,
					'tariff_code' => ($product->getPartnerCode() ? $product->getPartnerCode() : 0),
					'order_id' => $reserve->getReserveNumber(),
					'price' => $product->getPrice(),
					'position_count' => count($reserve->getProducts()),
					'position_id' => $i,
					'quantity' => $product->getQuantity(),
					'product_id' => $product->getId(),
					'payment_type' => 'sale',
					'client_id' => $reserve->getAgentId(),
					'currency_code' => 'RUB',
					'payment' => $product->getReward()
				];
				
				$request = new Request();
				$request->setMethod(Request::METHOD_GET);
				$request->setUri('https://ad.admitad.com/r');
				foreach ($params as $name => $value) {
					$request->getQuery()->set($name, $value);
				}
				$response = $client->send($request);
				if (Response::STATUS_CODE_200 !== $response->getStatusCode()) {
					return false;
				}
				
				$i++;
			}
		}
		
		return true;
	}

}

?>