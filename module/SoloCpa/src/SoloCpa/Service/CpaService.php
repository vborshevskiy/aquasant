<?php

namespace SoloCpa\Service;

use Solo\ServiceManager\AbstractService;
use Zend\Http\Request;
use SoloCpa\Data\CampaignSourcesTableInterface;
use SoloCpa\Entity\CampaignSource;
use Solo\DateTime\DateTime;
use SoloCpa\Service\Provider\ProviderInterface;
use Zend\Session\Container;
use Solo\Cookie\Cookie;
use Solo\ServiceManager\ProvidesOptions;
use SoloCpa\Data\CpaReservesTableInterface;
use SoloCpa\Entity\CpaReserve;
use Solo\Db\TableGateway\TableGateway;
use SoloErp\WebService\Reader\UltimaObjectReader;
use SoloErp\WebService\Reader\UltimaListReader;
use Zend\Stdlib\RequestInterface;
use SoloCpa\Service\Provider\GenericProvider;
use Zend\Http\Header\Referer;
use SoloCabinet\Entity\Ultima\Reserve;
use Solo\Stdlib\ArrayHelper;

class CpaService extends AbstractService {
	
	use ProvidesOptions;

	/**
	 *
	 * @var CampaignSourcesTableInterface
	 */
	private $campaignSourcesTable;

	/**
	 *
	 * @var CpaReservesTableInterface
	 */
	private $cpaReservesTable;

	/**
	 *
	 * @var array
	 */
	private $partnersSettings = [];

	/**
	 *
	 * @var Provider\ProviderInterface
	 */
	private $currentProvider = null;

	/**
	 *
	 * @param CampaignSourcesTableInterface $campaignSourcesTable        	
	 */
	public function setCampaignSourcesDataObjects(CampaignSourcesTableInterface $campaignSourcesTable) {
		$this->campaignSourcesTable = $campaignSourcesTable;
	}

	/**
	 *
	 * @param CpaReservesTableInterface $cpaReservesTable        	
	 */
	public function setCpaReservesDataObjects(CpaReservesTableInterface $cpaReservesTable) {
		$this->cpaReservesTable = $cpaReservesTable;
	}

	/**
	 *
	 * @param string $uid        	
	 * @param array $settings        	
	 */
	public function addPartnerSettings($uid, $settings) {
		$this->partnersSettings[$uid] = [
			'campaignId' => $settings['campaignId'],
			'uid' => $uid,
			'className' => $settings['className'],
			'trackMode' => $settings['trackMode'],
			'options' => (isset($settings['options']) ? $settings['options'] : []),
			'utm_source' => (isset($settings['utm_source']) ? trim($settings['utm_source']) : '') 
		];
	}

	/**
	 *
	 * @return array
	 */
	public function getPartnersSettings() {
		return $this->partnersSettings;
	}

	/**
	 *
	 * @param ProviderInterface $provider        	
	 * @return \SoloCpa\Service\CpaService
	 */
	public function setProvider(ProviderInterface $provider) {
		$this->currentProvider = $provider;
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasProvider() {
		return (null !== $this->currentProvider);
	}

	/**
	 *
	 * @return \SoloCpa\Service\Provider\ProviderInterface
	 */
	public function getProvider() {
		return $this->currentProvider;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasCampaignId() {
		if ($this->hasProvider()) return true;
		if (Cookie::exists('adcampaign')) return true;
		return false;
	}

	/**
	 *
	 * @return integer
	 */
	public function getCampaignId() {
		if ($this->hasProvider()) {
			return $this->getProvider()->getId();
		} elseif (Cookie::exists('adcampaign')) {
			return Cookie::get('adcampaign');
		} else {
			return '';
		}
	}

	/**
	 *
	 * @param RequestInterface $request        	
	 */
	public function initialize(RequestInterface $request) {
		if ($request instanceof Request) {
			$this->initializeCampaign($request);
			$this->initializeProvider($request);
		}
	}

	/**
	 *
	 * @param Request $request        	
	 */
	private function initializeCampaign(Request $request) {
		$campaignId = 0;
		
		$query = $request->getQuery()->toString();
		if (!empty($query)) {
			foreach ($this->campaignSourcesTable->findAll() as $source) {
				if (empty($campaignId) && (false !== stripos($query, $source->getQueryPattern()))) {
					$campaignId = $source->getCampaignId();
				}
			}
		}
		
		// @todo activities
		
		Cookie::set('adcampaign', $campaignId, '30w', '/', $_SERVER['HTTP_HOST'], false, true);
	}

	/**
	 *
	 * @param Request $request        	
	 */
	private function initializeProvider(Request $request) {
		// from request
		if ($request->getQuery('utm_source')) {
			$isSkipSource = false;
			if ($request->getQuery('skip_source_until')) {
				$skipSourceUntilTimestamp = $request->getQuery('skip_source_until');
				if (is_numeric($skipSourceUntilTimestamp) && (0 < $skipSourceUntilTimestamp)) {
					$skipSourceUntil = DateTime::createFromTimestamp($skipSourceUntilTimestamp);
					if (DateTime::now()->lowerThan($skipSourceUntil)) {
						$isSkipSource = true;
					}
				}
			}
			
			if (!$isSkipSource) {
				$source = $this->findSourceByUtmSource($request->getQuery('utm_source'));
				if ($source) {
					$provider = $this->createProvider($source->getCampaignId());
					if (!$provider) {
						$provider = new GenericProvider();
						$provider->setId($source->getCampaignId());
						$provider->setUid($source->getUtmSource());
					}
					$provider->setRequest($request);
					$provider->initialize();
					
					$provider->setCreatedAt(DateTime::now());
					
					$this->memorizeProvider($provider);
					$this->setProvider($provider);
				}
				// @todo exclude this shit
				$restrictedUtmSources = [
					'email',
					'123' 
				];
				if (!$this->hasProvider() && !in_array($request->getQuery('utm_source'), $restrictedUtmSources)) {
					$this->forgetProvider();
				}
			}
		}
		// END from request
		
		// from session
		if (!$this->hasProvider()) {
			$session = new Container('cpa');
			if (isset($session->serialized) && !empty($session->serialized) && isset($session->campaignId) && !empty($session->campaignId)) {
				$campaignId = intval($session->campaignId);
				$provider = $this->createProvider($campaignId);
				if (!$provider) {
					$source = $this->findSourceByCampaignId($campaignId);
					if ($source) {
						$provider = new GenericProvider();
						$provider->setId($source->getCampaignId());
						$provider->setUid($source->getUtmSource());
					}
				}
				if ($provider) {
					$provider->fromString($session->serialized);
					if (isset($session->entryPointUrl)) {
						$provider->setEntryPointUrl($session->entryPointUrl);
					}
					
					$this->memorizeProvider($provider);
					$this->setProvider($provider);
				}
			}
		}
		// END from session
		
		// from cookie
		if (!$this->hasProvider() && Cookie::exists('cpa')) {
			$parts = explode('|', Cookie::get('cpa'));
			if ((0 < count($parts)) && !empty($parts[0])) {
				$utmSource = trim($parts[0]);
				$source = $this->findSourceByUtmSource($utmSource);
				if ($source) {
					$provider = $this->createProvider($source->getCampaignId());
					if (!$provider) {
						$provider = new GenericProvider();
						$provider->setId($source->getCampaignId());
						$provider->setUid($source->getUtmSource());
					}
					if ($provider) {
						$provider->fromString(Cookie::get('cpa'));
						if (Cookie::exists('cpa_entry_url')) {
							$provider->setEntryPointUrl(Cookie::get('cpa_entry_url'));
						}
						
						$this->memorizeProvider($provider);
						$this->setProvider($provider);
					}
				}
			}
		}
		// END from cookie
		
		// organic
		try {
			if (!$this->hasProvider() && $request->getHeader('referer')) {
				$referer = $request->getHeader('referer');
				if ($referer instanceof Referer) {
					$provider = null;
					if (false !== stripos($referer->uri()->getHost(), 'yandex.ru')) {
						$provider = $this->createProvider('yandex');
					} elseif (false !== stripos($referer->uri()->getHost(), 'google.com')) {
						$provider = $this->createProvider('google');
					} elseif (false !== stripos($referer->uri()->getHost(), 'google.ru')) {
						$provider = $this->createProvider('google');
					} elseif (false !== stripos($referer->uri()->getHost(), 'go.mail.ru')) {
						$provider = $this->createProvider('mailru');
					}
					
					if ($provider) {
						$this->memorizeProvider($provider);
						$this->setProvider($provider);
					}
				}
			}
		} catch (\Zend\Uri\Exception\InvalidArgumentException $ex) {
			// do nothing
		} catch (\Exception $ex) {
			throw $ex;
		}
		// END organic
		
		// type-in
		try {
			if (!$this->hasProvider() && $request->getHeader('referer')) {
				$referer = $request->getHeader('referer');
				if ($referer instanceof Referer) {
					$provider = null;
					if (false !== stripos($referer->uri()->getHost(), '123.ru')) {
						$provider = $this->createProvider('typein');
					}
					
					if ($provider) {
						$this->memorizeProvider($provider);
						$this->setProvider($provider);
					}
				}
			}
		} catch (\Zend\Uri\Exception\InvalidArgumentException $ex) {
			// do nothing
		} catch (\Exception $ex) {
			throw $ex;
		}
		// END type-in
		
		// typein
		try {
			if (!$this->hasProvider() && !$request->getHeader('referer') && $request->getUri() && ('/' == $request->getUri()->getPath())) {
				$provider = $this->createProvider('typein');
				if ($provider) {
					$this->memorizeProvider($provider);
					$this->setProvider($provider);
				}
			}
		} catch (\Zend\Uri\Exception\InvalidArgumentException $ex) {
			// do nothing
		} catch (\Exception $ex) {
			throw $ex;
		}
		// END typein
	}

	/**
	 *
	 * @return boolean
	 */
	public function isEmpty() {
		return (!$this->hasCampaignId() && !$this->hasProvider());
	}

	/**
	 *
	 * @param integer $reserveId        	
	 * @return string
	 */
	public function getSuccessCode($reserveId) {
		if (true || $this->hasProvider() && !$this->isSuccessCodeAlreadyShown($reserveId)) {
			$reserve = $this->cpaReservesTable->findPrimary($this->getProvider()->getId(), $reserveId);
			if ($reserve) {
				$this->trackSuccessCodeView($reserveId);
				return $this->getProvider()->getSuccessCode($reserve);
			}
		}
		return '';
	}

	/**
	 *
	 * @param integer $reserveId        	
	 */
	private function trackSuccessCodeView($reserveId) {
		$session = new Container('cpa');
		if (!isset($session->successCodeViews)) {
			$session->successCodeViews = [];
		}
		$views = $session->successCodeViews;
		$views[$reserveId] = true;
		$session->successCodeViews = $views;
	}

	/**
	 *
	 * @param integer $reserveId        	
	 * @return boolean
	 */
	private function isSuccessCodeAlreadyShown($reserveId) {
		$session = new Container('cpa');
		if (isset($session->successCodeViews) && isset($session->successCodeViews[$reserveId])) {
			return true;
		}
		return false;
	}

	/**
	 *
	 * @param integer $reserveId        	
	 * @param integer $campaignId        	
	 */
	public function saveReserveCampaign($reserveId, $campaignId) {
		$wm = $this->erp()->site()->CallHandlerFunction();
		$wm->setName('SetReserveAdvertisingCampaign');
		$wm->setParam(sprintf('%d|%d', $reserveId, $campaignId));
		$wm->call();
		
		// @todo db tracking
	}

	/**
	 *
	 * @param CpaReserve $reserve        	
	 */
	public function saveReserve(CpaReserve $reserve) {
		$this->saveReserveInLog($reserve);
		
		// in erp
		if ($this->getProvider()->hasSource() || $this->getProvider()->hasTarget()) {
			$wm = $this->erp()->site()->SetReserveCpaPartnerSettings();
			$wm->setResereveID($reserve->getReserveId());
			$wm->setIdentity($this->getProvider()->hasSource() ? $this->getProvider()->getSource() : '');
			$wm->setIdentityAlt($this->getProvider()->hasTarget() ? $this->getProvider()->getTarget() : '');
			$response = $wm->call();
			if (!$response->hasError()) {
				$rdr = new UltimaObjectReader($response);
				if (isset($rdr->partnerCodes) && is_array($rdr->partnerCodes)) {
					$reserve->setPartnerCodes($rdr->partnerCodes);
				}
				$reserve->setReward(isset($rdr->reward) ? $rdr->reward : 0);
				
				// products partner codes
				if (isset($rdr->productsPartnerCodes) && is_array($rdr->productsPartnerCodes) && (2 == count($rdr->productsPartnerCodes))) {
					$productsPartnerCodes = array_combine($rdr->productsPartnerCodes[0], $rdr->productsPartnerCodes[1]);
					foreach ($reserve->getProducts() as $product) {
						if (isset($productsPartnerCodes[$product->getId()])) {
							$product->setPartnerCode($productsPartnerCodes[$product->getId()]);
						}
					}
				}
				// END products partner codes
				
				// products rewards
				if (isset($rdr->productsRewards) && is_array($rdr->productsRewards) && (2 == count($rdr->productsRewards))) {
					$productsRewards = array_combine($rdr->productsRewards[0], $rdr->productsRewards[1]);
					foreach ($reserve->getProducts() as $product) {
						if (isset($productsRewards[$product->getId()])) {
							$product->setReward($productsRewards[$product->getId()]);
						}
					}
				}
				// END products rewards
				
				$reserve->isSuccess(true);
				$this->cpaReservesTable->update($reserve);
			}
		}
	}

	/**
	 *
	 * @param CpaReserve $reserve        	
	 */
	public function saveReserveInLog(CpaReserve $reserve) {
		$this->cpaReservesTable->insert($reserve, TableGateway::INSERT_UPDATE_ON_DUPLICATE);
	}

	/**
	 *
	 * @param CpaReserve $reserve        	
	 */
	public function updateReserveInLog(CpaReserve $reserve) {
		$this->cpaReservesTable->update($reserve);
	}

	/**
	 *
	 * @param integer $campaignId        	
	 * @param array $reserveIds        	
	 * @param DateTime $createdDate        	
	 * @return array
	 */
	public function findReportReserves($campaignId, array $reserveIds = [], DateTime $createdDate = null) {
		$reserves = [];
		
		$erpReserves = $this->findReservesFromErp($campaignId, $reserveIds, $createdDate);
		$siteReserves = $this->findReservesFromSite($campaignId, $reserveIds, $createdDate);
		
		$checkReserveIds = array_unique(array_merge($reserveIds, array_keys($erpReserves)));
		foreach ($checkReserveIds as $reserveId) {
			if (isset($siteReserves[$reserveId])) {
				$siteReserve = $siteReserves[$reserveId];
				$erpReserve = (isset($erpReserves[$reserveId]) ? $erpReserves[$reserveId] : null);
				
				$status = ($erpReserve ? $erpReserve->Status : CpaReserve::STATUS_INPROGRESS);
				
				// check in progress status
				if (CpaReserve::STATUS_INPROGRESS == $status) {
					$agentId = $this->cabinet()->activeReserves()->getAgentIdByReserveId($reserveId);
					if ($agentId) {
						$reserve = $this->cabinet()->activeReserves()->getReserveInfo($agentId, $reserveId);
						if ($reserve->success()) {
							$reserve = $reserve->reserve;
							if (in_array($reserve->getStatusId(), [
								2676 
							]) && !$reserve->isCompleted()) {
								$status = CpaReserve::STATUS_CANCELLED;
							} elseif ($reserve->isCompleted()) {
								$status = CpaReserve::STATUS_DONE;
							}
						}
					}
				}
				// END check in progress status
				
				$reserves[] = [
					'id' => $reserveId,
					'status' => $status,
					'reserve' => $siteReserve 
				];
			}
		}
		
		return $reserves;
	}

	/**
	 *
	 * @param integer $campaignId        	
	 * @param array $reserveIds        	
	 * @param DateTime $createdDate        	
	 * @return multitype:\SoloErp\WebService\ResultRow
	 */
	private function findReservesFromErp($campaignId, array $reserveIds = [], DateTime $createdDate = null) {
		$reserves = [];
		
		$wm = null;
		if (0 < count($reserveIds)) {
			$wm = $this->erp()->site()->GetCpaPartnersReservesByIDs();
			$wm->setReserves($reserveIds);
		} elseif (null !== $createdDate) {
			$wm = $this->erp()->site()->GetCpaPartnersReservesByCreatedDate();
			$wm->setCreatedDate($createdDate->format('Y-m-d'));
		}
		$wm->setSiteID($this->sites()->currentSite()->getId());
		// $wm->setSiteID(120);
		$wm->setCampaignID($campaignId);
		
		$response = $wm->call();
		if (!$response->hasError()) {
			$rdr = new UltimaListReader($response);
			foreach ($rdr as $row) {
				$reserves[$row->ID] = $row;
			}
		}
		
		return $reserves;
	}

	/**
	 *
	 * @param integer $campaignId        	
	 * @param array $reserveIds        	
	 * @return multitype:\SoloErp\WebService\ResultRow
	 */
	public function findReservesByIds($campaignId, array $reserveIds) {
		return $this->findReservesFromErp($campaignId, $reserveIds);
	}

	/**
	 *
	 * @param integer $campaignId        	
	 * @param array $reserveIds        	
	 * @param DateTime $createdDate        	
	 * @return array
	 */
	private function findReservesFromSite($campaignId, array $reserveIds = [], DateTime $createdDate = null) {
		$reserves = [];
		
		$rows = [];
		if (0 < count($reserveIds)) {
			$rows = $this->cpaReservesTable->findAllSuccessByReserveIds($campaignId, $reserveIds);
		} elseif (null !== $createdDate) {
			$rows = $this->cpaReservesTable->findAllSuccessByCampaignIdAndCreatedAt($campaignId, $createdDate->getDayBegin(), $createdDate->getDayEnd());
		}
		foreach ($rows as $row) {
			$reserves[$row->getReserveId()] = $row;
		}
		
		return $reserves;
	}

	/**
	 *
	 * @param integer $campaignId        	
	 * @param DateTime $fromDate        	
	 * @param DateTime $toDate        	
	 * @return array
	 */
	public function findCampaignReportReservesByDateRange($campaignId, DateTime $fromDate, DateTime $toDate) {
		$reserves = [];
		
		$erpReserves = $this->findReservesByDateRangeFromErp($campaignId, $fromDate, $toDate);
		$siteReserves = $this->findCampaignReservesByDateRangeFromSite($campaignId, $fromDate, $toDate);
		
		foreach ($siteReserves as $reserveId => $siteReserve) {
			if (isset($erpReserves[$reserveId])) {
				$status = CpaReserve::STATUS_INPROGRESS;
				$erpReserve = $erpReserves[$reserveId];
				$status = $erpReserve->Status;
				
				$agentId = $siteReserve->getAgentId();
				$reserve = $this->cabinet()->activeReserves()->getReserveInfo($agentId, $reserveId);
				if ($reserve->success()) {
					$reserve = $reserve->reserve;
					
					$siteReserve->setAmount($reserve->getAmount());
					foreach ($siteReserve->getProducts() as $product) {
						//$product->dump(); exit();
					}
					//print $siteReserve->getReward().' => '.floatval($erpReserve['RewardAmount']); exit();
					$siteReserve->setReward($erpReserve['RewardAmount']);
					
					$status = $this->normalizeReportReserveStatus($status, $reserve);
				}
				
				$reserves[] = [
					'id' => $reserveId,
					'status' => $status,
					'reserve' => $siteReserve 
				];
			}
		}
		
		return $reserves;
	}

	/**
	 *
	 * @param DateTime $fromDate        	
	 * @param DateTime $toDate        	
	 * @return array
	 */
	public function findAllReportReservesByDateRange(DateTime $fromDate, DateTime $toDate) {
		$reserves = [];
		
		$siteReserves = $this->findAllReservesByDateRangeFromSite($fromDate, $toDate);
		
		$campaings = [];
		foreach ($siteReserves as $siteReserve) {
			$campaignId = $siteReserve->getCampaignId();
			if (!isset($campaings[$campaignId])) {
				$campaings[$campaignId] = [];
			}
			$campaings[$campaignId][] = $siteReserve->getReserveId();
		}
		
		$erpReserves = [];
		foreach ($campaings as $campaignId => $reserveIds) {
			$campaignErpReserves = $this->findReservesByIds($campaignId, $reserveIds);
			$erpReserves = ArrayHelper::merge($erpReserves, $campaignErpReserves, true);
		}
		
		foreach ($siteReserves as $reserveId => $siteReserve) {
			if (isset($erpReserves[$reserveId])) {
				$status = CpaReserve::STATUS_INPROGRESS;
				$erpReserve = $erpReserves[$reserveId];
				$status = $erpReserve->Status;
				
				$agentId = $siteReserve->getAgentId();
				$reserve = $this->cabinet()->activeReserves()->getReserveInfo($agentId, $reserveId);
				if ($reserve->success()) {
					$reserve = $reserve->reserve;
					
					$siteReserve->setAmount($reserve->getAmount());
					$siteReserve->setReward($erpReserve['RewardAmount']);
					
					$status = $this->normalizeReportReserveStatus($status, $reserve);
				}
				
				$reserves[] = [
					'id' => $reserveId,
					'status' => $status,
					'reserve' => $siteReserve 
				];
			}
		}
		
		return $reserves;
	}
	
	/**
	 *@param array $promocodes 
	 * @param DateTime $fromDate
	 * @param DateTime $toDate
	 * @return array
	 */
	public function findPromocodesReportReservesByDateRange(array $promocodes, DateTime $fromDate, DateTime $toDate) {
		$reserves = [];
	
		$siteReserves = $this->findAllReservesByDateRangeFromSite($fromDate, $toDate);
		$siteReserves = array_filter($siteReserves, function(CpaReserve $siteReserve) use($promocodes) {
			$promocode = $siteReserve->getOrderInfo()->getCoupon();
			return (!empty($promocode) && in_array($promocode, $promocodes));
		});
	
		$campaings = [];
		foreach ($siteReserves as $siteReserve) {
			$campaignId = $siteReserve->getCampaignId();
			if (!isset($campaings[$campaignId])) {
				$campaings[$campaignId] = [];
			}
			$campaings[$campaignId][] = $siteReserve->getReserveId();
		}
	
		$erpReserves = [];
		foreach ($campaings as $campaignId => $reserveIds) {
			$campaignErpReserves = $this->findReservesByIds($campaignId, $reserveIds);
			$erpReserves = ArrayHelper::merge($erpReserves, $campaignErpReserves, true);
		}
	
		foreach ($siteReserves as $reserveId => $siteReserve) {
			if (isset($erpReserves[$reserveId])) {
				$status = CpaReserve::STATUS_INPROGRESS;
				$erpReserve = $erpReserves[$reserveId];
				$status = $erpReserve->Status;
	
				$agentId = $siteReserve->getAgentId();
				$reserve = $this->cabinet()->activeReserves()->getReserveInfo($agentId, $reserveId);
				if ($reserve->success()) {
					$reserve = $reserve->reserve;
						
					$siteReserve->setAmount($reserve->getAmount());
					$siteReserve->setReward($erpReserve['RewardAmount']);
						
					$status = $this->normalizeReportReserveStatus($status, $reserve);
				}
	
				$reserves[] = [
				'id' => $reserveId,
				'status' => $status,
				'reserve' => $siteReserve
				];
			}
		}
	
		return $reserves;
	}

	/**
	 *
	 * @param string $status        	
	 * @param Reserve $reserve        	
	 * @return string
	 */
	private function normalizeReportReserveStatus($status, Reserve $reserve) {
		if (CpaReserve::STATUS_INPROGRESS == $status) {
			if (in_array($reserve->getStatusId(), [
				2676 
			]) && !$reserve->isCompleted()) {
				$status = CpaReserve::STATUS_CANCELLED;
			} elseif (in_array($reserve->getStatusId(), [
				1419,
				1571 
			]) && $reserve->isCompleted()) {
				$status = CpaReserve::STATUS_DONE;
			} elseif (in_array($reserve->getStatusId(), [
				1501 
			]) && $reserve->isCompleted()) {
				$hasRefund = false;
				if ($reserve->getComments() && (false !== mb_stripos($reserve->getComments(), 'возврат'))) {
					$hasRefund = true;
				}
				if ($hasRefund) {
					$status = CpaReserve::STATUS_CANCELLED;
				} else {
					$status = CpaReserve::STATUS_DONE;
				}
			}
		}
		return $status;
	}

	/**
	 *
	 * @param integer $campaignId        	
	 * @param DateTime $fromDate        	
	 * @param DateTime $toDate        	
	 * @return array
	 */
	private function findReservesByDateRangeFromErp($campaignId, DateTime $fromDate, DateTime $toDate) {
		$reserves = [];
		
		$wm = $this->erp()->site()->GetCpaPartnersReservesByCreatedDateRange();
		$wm->setSiteID($this->sites()->currentSite()->getId());
		$wm->setCampaignID($campaignId);
		$wm->setCreatedDateFrom($fromDate->format('Y-m-d'));
		$wm->setCreatedDateTo($toDate->format('Y-m-d'));
		
		$response = $wm->call();
		if (!$response->hasError()) {
			$rdr = new UltimaListReader($response);
			foreach ($rdr as $row) {
				$reserves[$row->ID] = $row;
			}
		}
		
		return $reserves;
	}

	/**
	 *
	 * @param integer $campaignId        	
	 * @param DateTime $fromDate        	
	 * @param DateTime $toDate        	
	 * @return array
	 */
	public function findReservesByUpdateDateRange($campaignId, DateTime $fromDate, DateTime $toDate) {
		$reserves = [];
		
		$wm = $this->erp()->site()->GetCpaPartnersReservesByUpdatedDateRange();
		$wm->setSiteId($this->sites()->currentSite()->getId());
		$wm->setCampaignId($campaignId);
		$wm->setUpdatedDateFrom($fromDate->format('Y-m-d'));
		$wm->setUpdatedDateTo($toDate->format('Y-m-d'));
		
		$response = $wm->call();
		if (!$response->hasError()) {
			$rdr = new UltimaListReader($response);
			foreach ($rdr as $row) {
				$reserves[$row->ID] = $row;
			}
		}
		
		return $reserves;
	}

	/**
	 *
	 * @param integer $campaignId        	
	 * @param DateTime $fromDate        	
	 * @param DateTime $toDate        	
	 * @return array
	 */
	private function findCampaignReservesByDateRangeFromSite($campaignId, DateTime $fromDate, DateTime $toDate) {
		$reserves = [];
		
		$rows = $this->cpaReservesTable->findAllSuccessByCampaignIdAndCreatedAt($campaignId, $fromDate->getDayBegin(), $toDate->getDayEnd());
		foreach ($rows as $row) {
			$reserves[$row->getReserveId()] = $row;
		}
		
		return $reserves;
	}

	/**
	 *
	 * @param DateTime $fromDate        	
	 * @param DateTime $toDate        	
	 * @return array
	 */
	private function findAllReservesByDateRangeFromSite(DateTime $fromDate, DateTime $toDate) {
		$reserves = [];
		
		$rows = $this->cpaReservesTable->findAllSuccessByCreatedAt($fromDate->getDayBegin(), $toDate->getDayEnd());
		foreach ($rows as $row) {
			$reserves[$row->getReserveId()] = $row;
		}
		
		return $reserves;
	}

	/**
	 *
	 * @param string $utmSource        	
	 * @return CampaignSource
	 */
	public function findSourceByUtmSource($utmSource) {
		$source = null;
		
		$rows = $this->campaignSourcesTable->findOneByUtmSource($utmSource);
		if (1 == $rows->count()) {
			$source = $rows->current();
		} else {
			foreach ($this->partnersSettings as $settings) {
				if (empty($source) && isset($settings['utm_source']) && (0 == strcasecmp($utmSource, $settings['utm_source'])) && isset($settings['campaignId'])) {
					$source = $this->findSourceByCampaignId($settings['campaignId']);
				}
			}
		}
		
		return $source;
	}

	/**
	 *
	 * @param integer $campaignId        	
	 * @return CampaignSource
	 */
	private function findSourceByCampaignId($campaignId) {
		$rows = $this->campaignSourcesTable->findOneByCampaignId($campaignId);
		if (1 == $rows->count()) {
			return $rows->current();
		} else {
			return null;
		}
	}

	/**
	 *
	 * @param string|integer $uidOrCampaignId        	
	 * @return Provider\ProviderInterface
	 */
	public function createProvider($uidOrCampaignId) {
		$settings = null;
		
		foreach ($this->partnersSettings as $partnerSettings) {
			if (empty($settings)) {
				if (is_numeric($uidOrCampaignId)) {
					if ($partnerSettings['campaignId'] == intval($uidOrCampaignId)) {
						$settings = $partnerSettings;
					}
				} else {
					if (0 == strcasecmp($uidOrCampaignId, $partnerSettings['uid'])) {
						$settings = $partnerSettings;
					}
				}
			}
		}
		
		if ($settings && isset($settings['className'])) {
			$className = $settings['className'];
			if (class_exists($className, true)) {
				$provider = new $className();
				$provider->setOptions($settings['options']);
				$provider->setId($settings['campaignId']);
				return $provider;
			}
		}
		return null;
	}

	/**
	 *
	 * @param ProviderInterface $provider        	
	 */
	private function memorizeProvider(ProviderInterface $provider) {
		$serialized = $provider->toString();
		
		// session
		$session = new Container('cpa');
		$session->serialized = $serialized;
		$session->campaignId = $provider->getId();
		$session->entryPointUrl = $provider->getEntryPointUrl();
		
		// cookie
		$host = $_SERVER['HTTP_HOST'];
		$domain = substr($host, stripos($host, '.'));
		$expired = ($this->hasOption('cookie_expired') ? $this->getOption('cookie_expired') : '30d');
		Cookie::killCookie('cpa');
		Cookie::killCookie('cpa', $host);
		Cookie::killCookie('cpa', $domain);
		Cookie::set('cpa', $serialized, $expired, '/', $domain, false, true);
		if (!Cookie::exists('cpa_entry_url') || $provider->getEntryPointUrl()) {
			Cookie::killCookie('cpa_entry_url');
			Cookie::killCookie('cpa_entry_url', $host);
			Cookie::killCookie('cpa_entry_url', $domain);
			if (isset($_GET['utm_source']) && ('advcake' == $_GET['utm_source'])) {
				Cookie::set('cpa_entry_url', $provider->getEntryPointUrl(), $expired, '/', $domain, false, true);
			}
		}
	}

	/**
	 */
	private function forgetProvider() {
		$session = new Container('cpa');
		if (isset($session->serialized)) {
			unset($session->serialized);
		}
		if (isset($session->campaignId)) {
			unset($session->campaignId);
		}
		if (isset($session->entryPointUrl)) {
			unset($session->entryPointUrl);
		}
		
		$host = $_SERVER['HTTP_HOST'];
		$domain = substr($host, stripos($host, '.'));
		Cookie::killCookie('cpa');
		Cookie::killCookie('cpa', $host);
		Cookie::killCookie('cpa', $domain);
		
		Cookie::killCookie('cpa_entry_url');
		Cookie::killCookie('cpa_entry_url', $host);
		Cookie::killCookie('cpa_entry_url', $domain);
	}

	/**
	 *
	 * @param integer $campaignId        	
	 * @param integer $reserveId        	
	 * @return CpaReserve
	 */
	public function getCampaignReserve($campaignId, $reserveId) {
		return $this->cpaReservesTable->findPrimary($campaignId, $reserveId);
	}

	/**
	 *
	 * @param array $reserveNumbers        	
	 * @return \Zend\Db\ResultSet\ResultSet
	 */
	public function findSiteReservesByNumbers(array $reserveNumbers) {
		$rows = [];
		if (0 < count($reserveNumbers)) {
			$rows = $this->cpaReservesTable->findAllByReserveNumbers($reserveNumbers);
		}
		return $rows;
	}

}

?>