<?php

namespace SoloCpa\Service;

use Solo\ServiceManager\AbstractService;
use SoloCpa\Service\Tracker\AbstractTracker;

class TrackerService extends AbstractService {

	/**
	 *
	 * @var array
	 */
	private $trackerSettings = [];

	/**
	 *
	 * @var array
	 */
	private $trackers = [];

	/**
	 *
	 * @param string $name        	
	 * @param string $className        	
	 * @param array $options        	
	 * @return \SoloCpa\Service\TrackerService
	 */
	public function addTrackerSettings($name, $className, array $options, $enabled = true) {
		$this->trackerSettings[$name] = [
			'className' => $className,
			'options' => $options,
			'enabled' => $enabled 
		];
		return $this;
	}

	/**
	 *
	 * @param string $name        	
	 * @throws \RuntimeException
	 * @return AbstractTracker
	 */
	public function createTracker($name) {
		if (!isset($this->trackers[$name])) {
			if (!isset($this->trackerSettings[$name])) {
				throw new \RuntimeException(sprintf('Failed to create tracker "%s", not specified config', $name));
			}
			$settings = $this->trackerSettings[$name];
			$className = $settings['className'];
			if (class_exists($className, true)) {
				$tracker = new $className();
				if ($tracker instanceof AbstractTracker) {
					$tracker->setOptions($settings['options']);
					$tracker->enabled($settings['enabled']);
				}
				$this->trackers[$name] = $tracker;
			} else {
				throw new \RuntimeException(sprintf('Failed to create tracker "%s", class "%s" not found', $name, $className));
			}
		}
		return $this->trackers[$name];
	}

	/**
	 * Disable all trackers
	 */
	public function disableAllTrackers() {
		foreach ($this->trackerSettings as $name => &$settings) {
			$this->trackerSettings[$name]['enabled'] = false;
		}
		foreach ($this->trackers as $name => $tracker) {
			$tracker->enabled(false);
		}
	}

}

?>