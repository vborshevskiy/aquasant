<?php

namespace SoloCpa\Service\Factory;

use Solo\ServiceManager\AbstractServiceFactory;
use SoloCpa\Service\CpaService;

class CpaServiceFactory extends AbstractServiceFactory {

	/**
	 *
	 * @see \Solo\ServiceManager\AbstractFactory::create()
	 */
	protected function create() {
		$service = new CpaService();
		
		$service->setCampaignSourcesDataObjects($this->getServiceLocator()->get('SoloCpa\Data\CampaignSourcesTable'));
		$service->setCpaReservesDataObjects($this->getServiceLocator()->get('SoloCpa\Data\CpaReservesTable'));
		
		// settings
		if ($this->hasConfig('solo-cpa.partners')) {
			$partners = $this->getConfig('solo-cpa.partners');
			foreach ($partners as $uid => $settings) {
				$service->addPartnerSettings($uid, $settings);
			}
		}
		// END settings
		
		// options
		if ($this->hasConfig('solo-cpa.cookie_expired')) {
			$service->setOption('cookie_expired', $this->getConfig('solo-cpa.cookie_expired'));
		}
		// END options
		
		return $service;
	}

}

?>