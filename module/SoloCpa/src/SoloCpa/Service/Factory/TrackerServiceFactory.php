<?php

namespace SoloCpa\Service\Factory;

use Solo\ServiceManager\AbstractServiceFactory;
use SoloCpa\Service\TrackerService;

class TrackerServiceFactory extends AbstractServiceFactory {

	/**
	 *
	 * @see \Solo\ServiceManager\AbstractFactory::create()
	 */
	protected function create() {
		$service = new TrackerService();
		
		// config
		if ($this->hasConfig('solo-cpa.trackers')) {
			$config = $this->getConfig('solo-cpa.trackers');
			foreach ($config as $name => $tracker) {
				$className = $tracker['className'];
				$enabled = ((isset($tracker['enabled']) && $tracker['enabled']) || !isset($tracker['enabled']));
				$options = (isset($tracker['options']) ? $tracker['options'] : []);
				$service->addTrackerSettings($name, $className, $options, $enabled);
			}
		}
		// END config
		
		return $service;
	}

}

?>