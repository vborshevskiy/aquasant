<?php

namespace SoloCpa\Entity\CpaReserve;

use Solo\Mvc\Entity\AbstractEntity;

class OrderInfo extends AbstractEntity {

	/**
	 * @getter-setter
	 * 
	 * @var string
	 */
	protected $paymentMethod = '';

	/**
	 * @getter-setter
	 * 
	 * @var string
	 */
	protected $coupon = '';

}

?>