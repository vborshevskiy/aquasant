<?php

namespace SoloCpa\Entity\CpaReserve;

use Solo\Mvc\Entity\AbstractEntity;

class Product extends AbstractEntity {

	public function __construct() {
		if (isset($_GET['clearCache'])) {
			$this->__resetCache = true;
		}
	}

	/**
	 * @getter-setter
	 *
	 * @var integer
	 */
	protected $id;

	/**
	 * @getter-setter
	 * @nullable
	 *
	 * @var string
	 */
	protected $sku;

	/**
	 * @getter-setter
	 * @nullable
	 *
	 * @var string
	 */
	protected $name;

	/**
	 * @getter-setter
	 * @nullable
	 *
	 * @var string
	 */
	protected $categoryName;

	/**
	 *
	 * @var array
	 */
	protected $categoriesPath = [];

	/**
	 * @getter-setter
	 *
	 * @var integer
	 */
	protected $quantity;

	/**
	 * @getter-setter
	 *
	 * @var float
	 */
	protected $price;

	/**
	 * @getter-setter
	 *
	 * @var string
	 */
	protected $partnerCode = '';

	/**
	 * @getter-setter
	 *
	 * @var float
	 */
	protected $discount = 0;

	/**
	 * @getter-setter
	 *
	 * @var float
	 */
	protected $reward = 0;

	/**
	 *
	 * @param integer $id        	
	 * @param string $name        	
	 * @param integer $index        	
	 */
	public function addCategory($id, $name, $index = null) {
		if (null === $index) {
			$index = count($this->categoriesPath);
		}
		$this->categoriesPath[$index] = [
			'id' => $id,
			'name' => $name 
		];
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasCategories() {
		return (0 < count($this->categoriesPath));
	}

	/**
	 *
	 * @return array
	 */
	public function getCategories() {
		return $this->categoriesPath;
	}

}

?>