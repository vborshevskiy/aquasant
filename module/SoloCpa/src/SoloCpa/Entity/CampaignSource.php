<?php

namespace SoloCpa\Entity;

use Solo\Db\Entity\AbstractEntity;

class CampaignSource extends AbstractEntity {

	/**
	 * @dbname CampaignSourceID
	 * 
	 * @var integer
	 */
	protected $id;

	/**
	 * @dbname CampaignID
	 * 
	 * @var integer
	 */
	protected $campaignId;

	/**
	 * @dbname QueryPattern
	 * 
	 * @var string
	 */
	protected $queryPattern;

	/**
	 * @dbname UtmSource
	 * 
	 * @var string
	 */
	protected $utmSource;

}

?>