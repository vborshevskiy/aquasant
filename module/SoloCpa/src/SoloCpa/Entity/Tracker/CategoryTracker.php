<?php

namespace SoloCpa\Entity\Tracker;

class CategoryTracker extends AbstractTracker {

	/**
	 *
	 * @var integer
	 */
	private $id;

	/**
	 *
	 * @var string
	 */
	private $name;

	/**
	 *
	 * @var array
	 */
	private $parentCategories = [];

	/**
	 *
	 * @var array
	 */
	private $childCategories = [];

	/**
	 *
	 * @var array
	 */
	private $products = [];

	/**
	 *
	 * @see \SoloCpa\Entity\Tracker\TrackerInterface::getType()
	 */
	public function getType() {
		return TrackerInterface::TYPE_CATEGORY;
	}

	/**
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param integer $id        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCpa\Entity\Tracker\CategoryTracker
	 */
	public function setId($id) {
		if (!is_numeric($id)) {
			throw new \InvalidArgumentException('Id must be numeric');
		}
		$this->id = intval($id);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 *
	 * @param string $name        	
	 * @return \SoloCpa\Entity\Tracker\CategoryTracker
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	/**
	 *
	 * @param Category $category        	
	 * @return Category
	 */
	public function addParentCategory(Category $category) {
		$this->parentCategories[$category->getId()] = $category;
		return $category;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasParentCategories() {
		return (0 < count($this->parentCategories));
	}

	/**
	 *
	 * @return array
	 */
	public function getParentCategories() {
		return $this->parentCategories;
	}

	/**
	 *
	 * @param Category $category        	
	 * @return Category
	 */
	public function addChildCategory(Category $category) {
		$this->childCategories[$category->getId()] = $category;
		return $category;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasChildCategories() {
		return (0 < count($this->childCategories));
	}

	/**
	 *
	 * @return array
	 */
	public function getChildCategories() {
		return $this->childCategories;
	}

	/**
	 *
	 * @param Product $product        	
	 * @return Product
	 */
	public function addProduct(Product $product) {
		$this->products[$product->getId()] = $product;
		return $product;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasProducts() {
		return (0 < count($this->products));
	}

	/**
	 *
	 * @return array
	 */
	public function getProducts() {
		return $this->products;
	}

}

?>