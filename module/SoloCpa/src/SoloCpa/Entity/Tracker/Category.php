<?php

namespace SoloCpa\Entity\Tracker;

class Category {

	/**
	 *
	 * @var integer
	 */
	private $id;

	/**
	 *
	 * @var string
	 */
	private $name;

	/**
	 *
	 * @param integer $id        	
	 * @param string $name        	
	 */
	public function __construct($id, $name = null) {
		$this->setId($id);
		if (null !== $name) $this->setName($name);
	}

	/**
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param integer $id        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCpa\Entity\Tracker\Category
	 */
	public function setId($id) {
		if (!is_numeric($id)) {
			throw new \InvalidArgumentException('Id must be numeric');
		}
		$this->id = intval($id);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 *
	 * @param string $name        	
	 * @return \SoloCpa\Entity\Tracker\Category
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

}

?>