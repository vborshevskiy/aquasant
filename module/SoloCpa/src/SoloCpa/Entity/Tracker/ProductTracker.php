<?php

namespace SoloCpa\Entity\Tracker;

use Zend\Validator\Uri;

class ProductTracker extends AbstractTracker {

	/**
	 *
	 * @var integer
	 */
	private $id;

	/**
	 *
	 * @var string
	 */
	private $name;

	/**
	 *
	 * @var float
	 */
	private $price;

	/**
	 *
	 * @var Category
	 */
	private $category;

	/**
	 *
	 * @var string
	 */
	private $vendorName;

	/**
	 *
	 * @var string
	 */
	private $url;

	/**
	 *
	 * @var string
	 */
	private $imageUrl;

	/**
	 *
	 * @param integer $id        	
	 * @param string $name        	
	 * @param float $price        	
	 */
	public function __construct($id, $name = null, $price = null) {
		$this->setId($id);
		if (null !== $name) $this->setName($name);
		if (null !== $price) $this->setPrice($price);
	}

	/**
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param integer $id        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCpa\Entity\Tracker\ProductTracker
	 */
	public function setId($id) {
		if (!is_numeric($id)) {
			throw new \InvalidArgumentException('Id must be numeric');
		}
		if (empty($id)) {
			throw new \InvalidArgumentException('Id can\'t be empty');
		}
		$this->id = intval($id);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 *
	 * @param string $name        	
	 * @return \SoloCpa\Entity\Tracker\ProductTracker
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	/**
	 *
	 * @return float
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 *
	 * @param float $price        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCpa\Entity\Tracker\ProductTracker
	 */
	public function setPrice($price) {
		if (!is_numeric($price)) {
			throw new \InvalidArgumentException('Price must be numeric');
		}
		if (empty($price)) {
			throw new \InvalidArgumentException('Price can\'t be empty');
		}
		$this->price = floatval($price);
		return $this;
	}

	public function getCategory() {
		return $this->category;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasCategory() {
		return (null !== $this->category);
	}

	/**
	 *
	 * @param Category $category        	
	 * @return Category
	 */
	public function setCategory(Category $category) {
		$this->category = $category;
		return $category;
	}

	/**
	 *
	 * @return string
	 */
	public function getVendorName() {
		return $this->vendorName;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasVendorName() {
		return (null !== $this->vendorName);
	}

	/**
	 *
	 * @param string $vendorName        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCpa\Entity\Tracker\ProductTracker
	 */
	public function setVendorName($vendorName) {
		if (empty($vendorName)) {
			throw new \InvalidArgumentException('Vendor name can\'t be empty');
		}
		$this->vendorName = $vendorName;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 *
	 * @param string $url        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCpa\Entity\Tracker\ProductTracker
	 */
	public function setUrl($url) {
		if (empty($url)) {
			throw new \InvalidArgumentException('Url can\'t be empty');
		}
		if (!(new Uri())->isValid($url)) {
			throw new \InvalidArgumentException('Url has invalid format');
		}
		$this->url = $url;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getImageUrl() {
		return $this->imageUrl;
	}
	
	/**
	 *
	 * @return boolean
	 */
	public function hasImageUrl() {
		return (null !== $this->imageUrl);
	}
	

	/**
	 *
	 * @param string $imageUrl        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCpa\Entity\Tracker\ProductTracker
	 */
	public function setImageUrl($imageUrl) {
		if (empty($imageUrl)) {
			throw new \InvalidArgumentException('Image url can\'t be empty');
		}
		if (!(new Uri())->isValid($imageUrl)) {
			throw new \InvalidArgumentException('Image url has invalid format');
		}
		$this->imageUrl = $imageUrl;
		return $this;
	}

	/**
	 *
	 * @see \SoloCpa\Entity\Tracker\TrackerInterface::getType()
	 */
	public function getType() {
		return TrackerInterface::TYPE_PRODUCT;
	}

}

?>