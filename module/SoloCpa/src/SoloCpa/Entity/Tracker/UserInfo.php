<?php

namespace SoloCpa\Entity\Tracker;

use Zend\Validator\EmailAddress;
use Solo\DateTime\DateTime;
use SoloCore\Entity\Phone;

class UserInfo {

	/**
	 *
	 * @var integer
	 */
	private $id;

	/**
	 *
	 * @var string
	 */
	private $name;

	/**
	 *
	 * @var string
	 */
	private $email;

	/**
	 *
	 * @var Phone
	 */
	private $phone;

	/**
	 *
	 * @var DateTime
	 */
	private $initialDate;

	/**
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasId() {
		return (null !== $this->id);
	}

	/**
	 *
	 * @param integer $id        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCpa\Entity\Tracker\UserInfo
	 */
	public function setId($id) {
		if (!is_numeric($id)) {
			throw new \InvalidArgumentException('Id must be numeric');
		}
		if (empty($id)) {
			throw new \InvalidArgumentException('Id can\'t be empty');
		}
		$this->id = intval($id);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasName() {
		return (null !== $this->name);
	}

	/**
	 *
	 * @param string $name        	
	 * @return \SoloCpa\Entity\Tracker\UserInfo
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasEmail() {
		return (null !== $this->email);
	}

	/**
	 *
	 * @param string $email        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCpa\Entity\Tracker\UserInfo
	 */
	public function setEmail($email) {
		if (empty($email)) {
			throw new \InvalidArgumentException('Email can\'t be empty');
		}
		if (!(new EmailAddress())->isValid($email)) {
			throw new \InvalidArgumentException('Email has invalid format');
		}
		$this->email = $email;
		return $this;
	}

	/**
	 *
	 * @return \Solo\DateTime\DateTime
	 */
	public function getInitialDate() {
		return $this->initialDate;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasInitialDate() {
		return (null !== $this->initialDate);
	}

	/**
	 *
	 * @param DateTime $initialDate        	
	 * @return \SoloCpa\Entity\Tracker\UserInfo
	 */
	public function setInitialDate(DateTime $initialDate) {
		$this->initialDate = $initialDate;
		return $this;
	}
	
	/**
	 * 
	 * @return \SoloCore\Entity\Phone
	 */
	public function getPhone() {
		return $this->phone;
	}
	
	/**
	 * 
	 * @return boolean
	 */
	public function hasPhone() {
		return ((null !== $this->phone) && ($this->phone instanceof Phone) && !$this->phone->isEmpty());
	}
	
	/**
	 * 
	 * @param Phone $phone
	 * @return \SoloCpa\Entity\Tracker\UserInfo
	 */
	public function setPhone(Phone $phone) {
		$this->phone = $phone;
		return $this;
	}

}

?>