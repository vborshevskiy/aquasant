<?php

namespace SoloCpa\Entity\Tracker;

interface TrackerInterface {

	const TYPE_MAINPAGE = 1;

	const TYPE_CATEGORY = 2;
	
	const TYPE_SEARCH = 3;

	const TYPE_PRODUCT = 4;

	const TYPE_BASKET = 5;

	const TYPE_ORDER = 6;

	const TYPE_ORDER_THANKYOU = 7;
	
	const TYPE_EMPTY = 8;

	/**
	 *
	 * @return integer
	 */
	public function getType();

}

?>