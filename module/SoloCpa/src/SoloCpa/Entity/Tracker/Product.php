<?php

namespace SoloCpa\Entity\Tracker;

class Product {

	/**
	 *
	 * @var string
	 */
	private $id;

	/**
	 *
	 * @var integer
	 */
	private $quantity;

	/**
	 *
	 * @var float
	 */
	private $price;

	/**
	 *
	 * @var string
	 */
	private $name;

	/**
	 *
	 * @var string
	 */
	private $imageUrl;

	/**
	 *
	 * @var string
	 */
	private $categoryName;

	/**
	 *
	 * @var array
	 */
	private $tags = [];

	/**
	 *
	 * @param string $id        	
	 * @param integer $quantity        	
	 * @param float $price        	
	 */
	public function __construct($id, $quantity = null, $price = null) {
		$this->setId($id);
		if (null !== $quantity) $this->setQuantity($quantity);
		if (null !== $price) $this->setPrice($price);
	}

	/**
	 *
	 * @return string
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param string $id        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCpa\Entity\Tracker\BasketProduct
	 */
	public function setId($id) {
		if (empty($id)) {
			throw new \InvalidArgumentException('Id can\'t be empty');
		}
		$this->id = $id;
		return $this;
	}

	/**
	 *
	 * @return integer
	 */
	public function getQuantity() {
		return $this->quantity;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasQuantity() {
		return (null !== $this->quantity);
	}

	/**
	 *
	 * @param integer $quantity        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCpa\Entity\Tracker\BasketProduct
	 */
	public function setQuantity($quantity) {
		if (!is_numeric($quantity)) {
			throw new \InvalidArgumentException('Quantity must be numeric');
		}
		if (empty($quantity)) {
			throw new \InvalidArgumentException('Quantity can\'t be empty');
		}
		$this->quantity = intval($quantity);
		return $this;
	}

	/**
	 *
	 * @return float
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasPrice() {
		return (null !== $this->price);
	}

	/**
	 *
	 * @param float $price        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCpa\Entity\Tracker\BasketProduct
	 */
	public function setPrice($price) {
		if (!is_numeric($price)) {
			throw new \InvalidArgumentException('Price must be numeric');
		}
		if (empty($price)) {
			throw new \InvalidArgumentException('Price can\'t be empty');
		}
		$this->price = floatval($price);
		return $this;
	}

	public function getName() {
		return $this->name;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasName() {
		return (null !== $this->name);
	}

	/**
	 *
	 * @param string $name        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCpa\Entity\Tracker\BasketProduct
	 */
	public function setName($name) {
		if (empty($name)) {
			throw new \InvalidArgumentException('Name can\'t be empty');
		}
		$this->name = $name;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getImageUrl() {
		return $this->imageUrl;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasImageUrl() {
		return (null !== $this->imageUrl);
	}

	/**
	 *
	 * @param string $imageUrl        	
	 * @return \SoloCpa\Entity\Tracker\Product
	 */
	public function setImageUrl($imageUrl) {
		$this->imageUrl = $imageUrl;
		return $this;
	}

	/**
	 *
	 * @param string $name        	
	 * @param string $value        	
	 * @return \SoloCpa\Entity\Tracker\Product
	 */
	public function setTag($name, $value) {
		$this->tags[$name] = $value;
		return $this;
	}

	/**
	 *
	 * @param string $name        	
	 * @return boolean
	 */
	public function hasTag($name) {
		return array_key_exists($name, $this->tags);
	}

	/**
	 *
	 * @param string $name        	
	 * @return mixed
	 */
	public function getTag($name) {
		if (array_key_exists($name, $this->tags)) {
			return $this->tags[$name];
		} else {
			return null;
		}
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasCategoryName() {
		return (null !== $this->categoryName);
	}

	/**
	 *
	 * @return string
	 */
	public function getCategoryName() {
		return $this->categoryName;
	}

	/**
	 *
	 * @param string $categoryName        	
	 */
	public function setCategoryName($categoryName) {
		$this->categoryName = $categoryName;
		return $this;
	}

}

?>