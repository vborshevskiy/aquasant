<?php

namespace SoloCpa\Entity\Tracker;

class OrderThankyouTracker extends AbstractTracker {

	/**
	 *
	 * @var array
	 */
	private $purchasedProducts = [];

	/**
	 *
	 * @var OrderInfo
	 */
	private $orderInfo;

	/**
	 *
	 * @see \SoloCpa\Entity\Tracker\TrackerInterface::getType()
	 */
	public function getType() {
		return TrackerInterface::TYPE_ORDER_THANKYOU;
	}

	/**
	 *
	 * @param Product $product        	
	 * @return Product
	 */
	public function addPurchasedProduct(Product $product) {
		$this->purchasedProducts[$product->getId()] = $product;
		return $product;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasPurchasedProducts() {
		return (0 < count($this->purchasedProducts));
	}

	/**
	 *
	 * @return array
	 */
	public function getPurchasedProducts() {
		return $this->purchasedProducts;
	}

	/**
	 *
	 * @return \SoloCpa\Entity\Tracker\OrderInfo
	 */
	public function orderInfo() {
		if (null === $this->orderInfo) {
			$this->orderInfo = new OrderInfo();
		}
		return $this->orderInfo;
	}

}

?>