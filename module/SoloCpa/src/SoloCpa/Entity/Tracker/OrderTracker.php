<?php

namespace SoloCpa\Entity\Tracker;

class OrderTracker extends AbstractTracker {

	/**
	 *
	 * @see \SoloCpa\Entity\Tracker\TrackerInterface::getType()
	 */
	public function getType() {
		return TrackerInterface::TYPE_ORDER;
	}

}

?>