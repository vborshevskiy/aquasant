<?php

namespace SoloCpa\Entity\Tracker;

class BasketTracker extends AbstractTracker {

	/**
	 *
	 * @see \SoloCpa\Entity\Tracker\TrackerInterface::getType()
	 */
	public function getType() {
		return TrackerInterface::TYPE_BASKET;
	}

}

?>