<?php

namespace SoloCpa\Entity\Tracker;

use Solo\ServiceManager\ProvidesOptions;

abstract class AbstractTracker implements TrackerInterface {
	
	use ProvidesOptions;

	/**
	 *
	 * @var array
	 */
	protected $basketProducts = [];

	/**
	 *
	 * @var UserInfo
	 */
	private $userInfo;

	/**
	 *
	 * @var string
	 */
	private $cpaProviderUid;

	/**
	 *
	 * @param Product $product        	
	 * @return Product
	 */
	public function addBasketProduct(Product $product) {
		$this->basketProducts[$product->getId()] = $product;
		return $product;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasBasketProducts() {
		return (0 < count($this->basketProducts));
	}

	/**
	 *
	 * @return array
	 */
	public function getBasketProducts() {
		return $this->basketProducts;
	}

	/**
	 *
	 * @return \SoloCpa\Entity\Tracker\UserInfo
	 */
	public function userInfo() {
		if (null === $this->userInfo) {
			$this->userInfo = new UserInfo();
		}
		return $this->userInfo;
	}

	/**
	 *
	 * @return string
	 */
	public function getCpaProviderUid() {
		return $this->cpaProviderUid;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasCpaProviderUid() {
		return (null !== $this->cpaProviderUid);
	}

	/**
	 *
	 * @param string $cpaProviderUid        	
	 * @return \SoloCpa\Entity\Tracker\AbstractTracker
	 */
	public function setCpaProviderUid($cpaProviderUid) {
		$this->cpaProviderUid = $cpaProviderUid;
		return $this;
	}

}

?>