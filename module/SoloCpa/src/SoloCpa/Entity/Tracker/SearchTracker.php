<?php

namespace SoloCpa\Entity\Tracker;

class SearchTracker extends AbstractTracker {

	/**
	 *
	 * @var string
	 */
	private $queryPhrase;

	/**
	 *
	 * @var array
	 */
	private $products = [];

	/**
	 *
	 * @param string $queryPhrase        	
	 */
	public function __construct($queryPhrase) {
		$this->setQueryPhrase($queryPhrase);
	}

	/**
	 *
	 * @see \SoloCpa\Entity\Tracker\TrackerInterface::getType()
	 */
	public function getType() {
		return TrackerInterface::TYPE_SEARCH;
	}

	/**
	 *
	 * @return string
	 */
	public function getQueryPhrase() {
		return $this->queryPhrase;
	}

	/**
	 *
	 * @param string $queryPhrase        	
	 * @return \SoloCpa\Entity\Tracker\SearchTracker
	 */
	public function setQueryPhrase($queryPhrase) {
		$this->queryPhrase = $queryPhrase;
		return $this;
	}

	/**
	 *
	 * @param Product $product        	
	 * @return Product
	 */
	public function addProduct(Product $product) {
		$this->products[$product->getId()] = $product;
		return $product;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasProducts() {
		return (0 < count($this->products));
	}

	/**
	 *
	 * @return array
	 */
	public function getProducts() {
		return $this->products;
	}

}

?>