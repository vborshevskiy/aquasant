<?php

namespace SoloCpa\Entity\Tracker;

class OrderInfo {

	/**
	 *
	 * @var string
	 */
	private $number;

	/**
	 *
	 * @var float
	 */
	private $amount;

	/**
	 *
	 * @var string
	 */
	private $promocode;

	/**
	 *
	 * @return string
	 */
	public function getNumber() {
		return $this->number;
	}

	/**
	 *
	 * @param string $number        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCpa\Entity\Tracker\OrderInfo
	 */
	public function setNumber($number) {
		if (empty($number)) {
			throw new \InvalidArgumentException('Id can\'t be empty');
		}
		$this->number = $number;
		return $this;
	}

	/**
	 *
	 * @return float
	 */
	public function getAmount() {
		return $this->amount;
	}

	/**
	 *
	 * @param float $amount        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCpa\Entity\Tracker\OrderInfo
	 */
	public function setAmount($amount) {
		if (!is_numeric($amount)) {
			throw new \InvalidArgumentException('Amount must be numeric');
		}
		$this->amount = floatval($amount);
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getPromocode() {
		return $this->promocode;
	}

	/**
	 *
	 * @param string $promocode        	
	 * @return \SoloCpa\Entity\Tracker\OrderInfo
	 */
	public function setPromocode($promocode) {
		$this->promocode = $promocode;
		return $this;
	}

}

?>