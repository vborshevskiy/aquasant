<?php

namespace SoloCpa\Entity;

use Solo\Db\Entity\AbstractEntity;
use Solo\DateTime\DateTime;
use Solo\Db\ExchangeableInterface;

class CpaReserve extends AbstractEntity {
	
	const STATUS_INPROGRESS = 0;

	const STATUS_DONE = 1;

	const STATUS_CANCELLED = 2;

	/**
	 * @dbname CampaignID
	 *
	 * @var integer
	 */
	protected $campaignId;

	/**
	 * @dbname ReserveID
	 *
	 * @var integer
	 */
	protected $reserveId;

	/**
	 * @dbname ReserveNumber
	 *
	 * @var string
	 */
	protected $reserveNumber;

	/**
	 * @dbname AgentID
	 *
	 * @var integer
	 */
	protected $agentId;

	/**
	 *
	 * @var array
	 */
	protected $partnerCodes = [];

	/**
	 * @dbname ReserveAmount
	 *
	 * @var float
	 */
	protected $amount;

	/**
	 * @dbname ReserveReward
	 *
	 * @var float
	 */
	protected $reward;

	/**
	 * @dbname CreatedAt
	 *
	 * @var DateTime
	 */
	protected $createdAt;

	/**
	 * @dbname PartnerSource
	 *
	 * @var string
	 */
	protected $partnerSource;

	/**
	 * @dbname PartnerTarget
	 *
	 * @var string
	 */
	protected $partnerTarget;

	/**
	 *
	 * @var array
	 */
	protected $products = [];

	/**
	 * @dbname IsSuccess
	 *
	 * @var boolean
	 */
	protected $isSuccess = false;

	/**
	 *
	 * @var CpaReserve\OrderInfo
	 */
	protected $orderInfo;

	/**
	 * @dbname StatusCode
	 *
	 * @var integer
	 */
	protected $statusCode;

	/**
	 * @dbname StatusUpdatedAt
	 *
	 * @var DateTime
	 */
	protected $statusUpdatedAt;

	/**
	 * @dbname StatusSyncedAt
	 *
	 * @var DateTime
	 */
	protected $statusSyncedAt;

	/**
	 * @dbname TrackUID
	 *
	 * @var string
	 */
	protected $trackUid;

	/**
	 * @dbname EntryPointURL
	 * 
	 * @var string
	 */
	protected $entryPointUrl;

	/**
	 *
	 * @param array $codes        	
	 * @return \SoloCpa\Entity\CpaReserve
	 */
	public function setPartnerCodes(array $codes) {
		$this->partnerCodes = $codes;
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasPartnerCodes() {
		return (0 < count($this->partnerCodes));
	}

	/**
	 *
	 * @return array
	 */
	public function getPartnerCodes() {
		return $this->partnerCodes;
	}

	/**
	 *
	 * @param CpaReserve\Product $product        	
	 * @return \CpaReserve\Product
	 */
	public function addProduct(CpaReserve\Product $product) {
		$this->products[$product->getId()] = $product;
		return $product;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasProducts() {
		return (0 < count($this->products));
	}

	/**
	 *
	 * @param integer $productId        	
	 * @return boolean
	 */
	public function hasProduct($productId) {
		return array_key_exists($productId, $this->products);
	}

	/**
	 *
	 * @return array
	 */
	public function getProducts() {
		return $this->products;
	}

	/**
	 *
	 * @param integer $productId        	
	 * @return CpaReserve\Product
	 */
	public function getProduct($productId) {
		if (isset($this->products[$productId])) {
			return $this->products[$productId];
		} else {
			return null;
		}
	}

	/**
	 *
	 * @return \SoloCpa\Entity\CpaReserve\OrderInfo
	 */
	public function getOrderInfo() {
		if (null === $this->orderInfo) {
			$this->orderInfo = new CpaReserve\OrderInfo();
		}
		return $this->orderInfo;
	}

	/**
	 *
	 * @see \Solo\Db\Entity\AbstractEntity::getValues()
	 */
	public function getValues() {
		$data = parent::getValues();
		$data['PartnerCodes'] = implode(',', $this->getPartnerCodes());
		if ($this->hasProducts()) {
			$products = [];
			foreach ($this->getProducts() as $product) {
				$productValues = $product->getValues();
				$productValues['__className'] = get_class($product);
				$products[] = $productValues;
			}
			$data['Products'] = json_encode($products, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		}
		$data['OrderInfo'] = json_encode($this->getOrderInfo()->getValues(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		return $data;
	}

	/**
	 *
	 * @see \Solo\Db\Entity\AbstractEntity::exchangeArray()
	 */
	public function exchangeArray($data) {
		parent::exchangeArray($data);
		if (isset($data['PartnerCodes']) && !empty($data['PartnerCodes'])) $this->setPartnerCodes(explode(',', $data['PartnerCodes']));
		if (isset($data['Products']) && !empty($data['Products'])) {
			$products = json_decode(str_replace('\\\\"', '\\"', $data['Products']), true);
			foreach ($products as $product) {
				$className = (isset($product['__className']) ? str_replace('\\\\', '\\', $product['__className']) : __NAMESPACE__ . '\Product');
				if (class_exists($className, true)) {
					$productObj = new $className();
					if ($productObj instanceof ExchangeableInterface) {
						$productObj->exchangeArray($product);
					}
					$this->addProduct($productObj);
				}
			}
		}
		if (isset($data['OrderInfo']) && !empty($data['OrderInfo'])) {
			$this->getOrderInfo()->exchangeArray(json_decode($data['OrderInfo'], true));
		}
	}

}

?>