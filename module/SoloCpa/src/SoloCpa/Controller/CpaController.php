<?php

namespace SoloCpa\Controller;

use Application\Controller\AbstractController;
use Solo\DateTime\DateTime;
use SoloCpa\Service\Provider\ProviderInterface;
use Zend\Http\Response;
use SoloCpa\Entity\CpaReserve;
use SoloCatalog\Entity\Category;

class CpaController extends AbstractController {

	/**
	 *
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function indexAction() {
		exit();
	}

	/**
	 * ActionPay reserves
	 */
	public function actionpayAction() {
		$provider = $this->cpa()->createProvider('actionpay');
		
		// validate credentials
		if (false) {
			if (!$this->getRequest()->isPost() || !$this->getRequest()->getPost('pass') || !$provider->hasOption('secret_key') || (md5(
				$provider->getOption('secret_pass') !== $this->getRequest()->getPost('pass')))) {
				print '<?xml version="1.0" encoding="UTF-8"?><error>Неверный пароль</error>';
				exit();
			}
		}
		// END validate credentials
		
		// input
		$reserveIds = [];
		$createdDate = null;
		if (true || !$this->core()->isDev()) {
			if ($this->getRequest()->getPost('date')) {
				$createdDate = DateTime::createFromString($this->getRequest()->getPost('date'));
			} elseif ($this->getRequest()->getPost('xml')) {
				$reserveNumbers = [];
				$rdr = new \SimpleXMLElement(urldecode($this->getRequest()->getPost('xml')));
				foreach ($rdr->item as $item) {
					$reserveNumbers[] = strval($item);
				}
				
				$reserveIds = [];
				$siteReserves = $this->cpa()->findSiteReservesByNumbers($reserveNumbers);
				foreach ($siteReserves as $siteReserve) {
					$reserveIds[] = $siteReserve->getReserveId();
				}
			}
			// END input
		} else {
			$xml = '<?xml version="1.0" encoding="UTF-8"?>
<items>
<item>Z477847</item>
</items>';
			$rdr = new \SimpleXMLElement($xml);
			foreach ($rdr->item as $item) {
				$reserveNumber = strval($item);
				$reserveId = $this->cabinet()->activeReserves()->getReserveIdByNumber($reserveNumber);
				if ($reserveId) {
					$reserveIds[] = $reserveId;
				}
			}
		}
		if ($this->core()->isDev()) {
			$rdr = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?> <items><item>Z494099</item></items>');
			$reserveNumbers = [];
			foreach ($rdr->item as $item) {
				$reserveNumbers[] = strval($item);
			}
			
			$reserveIds = [];
			$siteReserves = $this->cpa()->findSiteReservesByNumbers($reserveNumbers);
			foreach ($siteReserves as $siteReserve) {
				$reserveIds[] = $siteReserve->getReserveId();
			}
		}
		
		$reserves = $this->cpa()->findReportReserves($provider->getId(), $reserveIds, $createdDate);
		
		$this->getResponse()->getHeaders()->addHeaders([
			'Content-Type' => 'text/xml' 
		]);
		return $this->getResponse()->setContent($provider->formatReportReserves($reserves));
	}

	/**
	 * Admitad report
	 */
	public function admitadAction() {
		set_time_limit(360);
		$provider = $this->cpa()->createProvider('admitad');
		
		// validate credentials
		if (false) {
			// validate by passwd file
		}
		// END validate credentials
		
		// input
		$reserveIds = [];
		$toDate = DateTime::now()->getDayEnd();
		$fromDate = DateTime::now()->addDays(-40)->getDayBegin();
		// END input
		
		$reserves = $this->cpa()->findCampaignReportReservesByDateRange($provider->getId(), $fromDate, $toDate);
		$this->getResponse()->getHeaders()->addHeaders([
			'Content-Type' => 'text/xml' 
		]);
		return $this->getResponse()->setContent($provider->formatReportReserves($reserves));
	}

	/**
	 * GdeSlon report
	 */
	public function gdeslonAction() {
		set_time_limit(360);
		$provider = $this->cpa()->createProvider('gdeslon');
		
		// input
		$reserveIds = [];
		$fromDate = null;
		$toDate = null;
		
		if ($this->getRequest()->isPost()) {
			$data = $this->getRequest()->getContent();
			$data = json_decode($data, true);
			if ($data) {
				if (isset($data['order_id']) && !empty($data['order_id'])) {
					$reserveNumbers = [];
					foreach (explode(',', $data['order_id']) as $reserveNumber) {
						$reserveNumbers[] = $reserveNumber;
					}
					$reserveIds = [];
					$reserveIds = [];
					$siteReserves = $this->cpa()->findSiteReservesByNumbers($reserveNumbers);
					foreach ($siteReserves as $siteReserve) {
						$reserveIds[] = $siteReserve->getReserveId();
					}
				} elseif (isset($data['date_interval']) && !empty($data['date_interval'])) {
					list($fromDateString, $toDateString) = explode('-', $data['date_interval']);
					$fromDate = DateTime::createFromFormat('Ymd', $fromDateString);
					$toDate = DateTime::createFromFormat('Ymd', $toDateString);
					if (!$fromDate || !$toDate || !$fromDate->lowerOrEquals($toDate)) {
						$fromDate = null;
						$toDate = null;
					}
				}
			}
		} else {
			$toDate = DateTime::now()->getDayEnd()->addDays(1);
			$fromDate = DateTime::now()->getDayEnd()->addDays(-5)->getDayBegin();
		}
		// END input
		
		if ($this->core()->isDev()) {
			$fromDate = DateTime::createFromFormat('Ymd', '20170201');
			$toDate = DateTime::createFromFormat('Ymd', '20170331');
		}
		
		$reserves = [];
		if (0 < count($reserveIds)) {
			$reserves = $this->cpa()->findReportReserves($provider->getId(), $reserveIds);
		} else {
			$reserves = $this->cpa()->findCampaignReportReservesByDateRange($provider->getId(), $fromDate, $toDate);
		}
		$this->getResponse()->getHeaders()->addHeaders([
			'Content-Type' => 'text/xml' 
		]);
		return $this->getResponse()->setContent($provider->formatReportReserves($reserves));
	}

	/**
	 * Send mixmarket auto invoice
	 */
	public function mixmarketAction() {
		$provider = $this->cpa()->createProvider('mixmarket');
		
		$fromDate = DateTime::now()->dayBegin();
		$toDate = DateTime::now()->dayEnd();
		
		$reserves = $this->cpa()->findCampaignReportReservesByDateRange($provider->getId(), $fromDate, $toDate);
		
		$paidReserves = [];
		$cancelledReserves = [];
		foreach ($reserves as $reserve) {
			if (CpaReserve::STATUS_DONE == $reserve['status']) {
				$paidReserves[] = $reserve;
			} elseif (CpaReserve::STATUS_CANCELLED == $reserve['status']) {
				$cancelledReserves[] = $reserve;
			}
		}
		
		if (0 < count($paidReserves)) {
			printf('Found %d paid reserves<br />' . PHP_EOL, count($paidReserves));
			
			print 'send: ';
			if ($provider->trackPaidReserves($paidReserves)) {
				print 'SUCCESS';
			} else {
				print 'FAILED';
			}
			print '<br /><br />' . PHP_EOL;
		}
		if (0 < count($cancelledReserves)) {
			printf('Found %d cancelled reserves<br />' . PHP_EOL, count($cancelledReserves));
			
			print 'send: ';
			if ($provider->trackCancelledReserves($cancelledReserves)) {
				print 'SUCCESS';
			} else {
				print 'FAILED';
			}
			print '<br /><br />' . PHP_EOL;
		}
		
		exit();
	}

	/**
	 * CityAds xml reporting
	 */
	public function cityadsAction() {
		// auth
		$pass = 'sd_safG2Lh__3';
		if (!$this->core()->isDev() && (!$this->params()->fromQuery('pkey') || (0 !== strcasecmp($pass, $this->params()->fromQuery('pkey'))))) {
			$this->getResponse()->setStatusCode(Response::STATUS_CODE_401);
			return $this->getResponse()->setContent('<?xml version="1.0" encoding="UTF-8"?><error>401</error>');
		}
		// END auth
		
		$dateFrom = null;
		$dateTo = null;
		if ($this->params()->fromPost('xml')) {
			$rdr = new \SimpleXMLElement($this->params()->fromPost('xml'));
			if ($rdr->date_from) {
				$dateFrom = DateTime::createFromTimestamp($rdr->date_from);
			}
			if ($rdr->date_to) {
				$dateTo = DateTime::createFromTimestamp($rdr->date_to);
			}
		}
		if (!$dateFrom && !$dateTo && $this->core()->isDev()) {
			$dateFrom = DateTime::now()->addDays(-1)->getDayBegin();
			$dateTo = DateTime::now()->getDayEnd();
		}
		// validate
		if (!$dateFrom || !$dateTo) {
			$this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
			return $this->getResponse()->setContent('<?xml version="1.0" encoding="UTF-8"?><error>500</error>');
		}
		// END validate
		
		$provider = $this->cpa()->createProvider('cityads');
		$reserves = $this->cpa()->findCampaignReportReservesByDateRange($provider->getId(), $dateFrom, $dateTo);
		$this->getResponse()->getHeaders()->addHeaders([
			'Content-Type' => 'text/xml' 
		]);
		return $this->getResponse()->setContent($provider->formatReportReserves($reserves));
	}

	/**
	 * AD1 xml reporting
	 */
	public function ad1Action() {
		$password = 'b072de9e3843170ec7f1ca77d8ec0f54';
		if (!$this->params()->fromPost('pass') || ($password != $this->params()->fromPost('pass'))) {
			$this->getResponse()->setStatusCode(Response::STATUS_CODE_401);
			return $this->getResponse()->setContent('<?xml version="1.0" encoding="UTF-8"?><error>401</error>');
		}
		
		$reserveIds = [];
		if ($this->params()->fromPost('xml')) {
			$rdr = new \SimpleXMLElement($this->params()->fromPost('xml'));
			foreach ($rdr->item as $item) {
				$reserveIds[] = strval($item);
			}
		}
		
		$provider = $this->cpa()->createProvider('ad1');
		$reserves = [];
		if (0 < count($reserveIds)) {
			$reserves = $this->cpa()->findReportReserves($provider->getId(), $reserveIds);
		}
		$this->getResponse()->getHeaders()->addHeaders([
			'Content-Type' => 'text/xml' 
		]);
		return $this->getResponse()->setContent($provider->formatReportReserves($reserves));
	}

	/**
	 * Advcake xml report
	 */
	public function advcakeAction() {
		set_time_limit(360);
		
		$dateTo = DateTime::now()->addDays(1)->getDayBegin();
		$dateFrom = DateTime::now()->addDays(-30)->getDayBegin();
		if ($this->params()->fromQuery('from') && $this->params()->fromQuery('to')) {
			$dateFrom = DateTime::createFromFormat('Y-m-d', $this->params()->fromQuery('from'))->getDayBegin();
			$dateTo = DateTime::createFromFormat('Y-m-d', $this->params()->fromQuery('to'))->getDayEnd();
		}
		
		$provider = $this->cpa()->createProvider('advcake');
		$reserves = $this->cpa()->findCampaignReportReservesByDateRange($provider->getId(), $dateFrom, $dateTo);
		
		// fill products properties
		$productIds = [];
		foreach ($reserves as $item) {
			$reserve = $item['reserve'];
			foreach ($reserve->getProducts() as $product) {
				$productIds[] = $product->getId();
			}
		}
		if (0 < count($productIds)) {
			$allCategories = $this->catalog()->categories()->getAllCategories();
			$products = $this->catalog()->products()->findAllByProductIds($productIds);
			foreach ($reserves as $item) {
				$reserve = $item['reserve'];
				foreach ($reserve->getProducts() as $cpaProduct) {
					$productId = $cpaProduct->getId();
					if (isset($products[$productId])) {
						$product = $products[$productId];
						$categoryId = $product->getCategoryId();
						if (isset($allCategories[$categoryId])) {
							$category = $allCategories[$categoryId];
							foreach ($category->getPathAsArray() as $parentCategoryId) {
								$parentCategory = $allCategories[$parentCategoryId];
								if ($parentCategory) {
									$cpaProduct->addCategory($parentCategory->getId(), $parentCategory->getName());
								}
							}
						}
					}
				}
			}
		}
		// END fill products properties
		
		$this->getResponse()->getHeaders()->addHeaders([
			'Content-Type' => 'text/xml' 
		]);
		return $this->getResponse()->setContent($provider->formatReportReserves($reserves));
	}

	/**
	 * Get4Click xml report
	 */
	public function get4clickAction() {
		set_time_limit(360);
		
		$dateTo = DateTime::now()->addDays(1)->getDayBegin();
		$dateFrom = DateTime::now()->addDays(-30)->getDayBegin();
		if ($this->params()->fromQuery('from') && $this->params()->fromQuery('to')) {
			$dateFrom = DateTime::createFromFormat('Y.m.d', $this->params()->fromQuery('from'))->getDayBegin();
			$dateTo = DateTime::createFromFormat('Y.m.d', $this->params()->fromQuery('to'))->getDayEnd();
		}
		
		$provider = $this->cpa()->createProvider('get4click');
		$reserves = $this->cpa()->findCampaignReportReservesByDateRange($provider->getId(), $dateFrom, $dateTo);
		
		$this->getResponse()->getHeaders()->addHeaders([
			'Content-Type' => 'text/csv' 
		]);
		header('Content-disposition: attachment;filename=get4click_report.csv');
		return $this->getResponse()->setContent($provider->formatReportReserves($reserves));
	}

	/**
	 * Advcake promocodes xml report
	 */
	public function advcakepromocodesAction() {
		set_time_limit(360);
		
		$promocodes = [
			'3P123',
			'3p123',
			'500P123',
			'500p123',
			'1KP123',
			'1kp123' 
		];
		
		$dateTo = DateTime::now()->addDays(1)->getDayBegin();
		$dateFrom = DateTime::now()->addDays(-7)->getDayBegin();
		if ($this->params()->fromQuery('from') && $this->params()->fromQuery('to')) {
			$dateFrom = DateTime::createFromFormat('Y-m-d', $this->params()->fromQuery('from'))->getDayBegin();
			$dateTo = DateTime::createFromFormat('Y-m-d', $this->params()->fromQuery('to'))->getDayEnd();
		}
		
		// validate date period
		if (30 < $dateFrom->diff($dateTo)->getDays()) {
			$this->getResponse()->setStatusCode(Response::STATUS_CODE_401);
			return $this->getResponse()->setContent('<?xml version="1.0" encoding="UTF-8"?><error>Specified too long date period. Max diff equals 7 days</error>');
		}
		
		$provider = $this->cpa()->createProvider('advcake');
		$reserves = $this->cpa()->findPromocodesReportReservesByDateRange($promocodes, $dateFrom, $dateTo);
		
		// fill products properties
		$productIds = [];
		foreach ($reserves as $item) {
			$reserve = $item['reserve'];
			foreach ($reserve->getProducts() as $product) {
				$productIds[] = $product->getId();
			}
		}
		if (0 < count($productIds)) {
			$allCategories = $this->catalog()->categories()->getAllCategories();
			$products = $this->catalog()->products()->findAllByProductIds($productIds);
			foreach ($reserves as $item) {
				$reserve = $item['reserve'];
				foreach ($reserve->getProducts() as $cpaProduct) {
					$productId = $cpaProduct->getId();
					if (isset($products[$productId])) {
						$product = $products[$productId];
						$categoryId = $product->getCategoryId();
						if (isset($allCategories[$categoryId])) {
							$category = $allCategories[$categoryId];
							foreach ($category->getPathAsArray() as $parentCategoryId) {
								$parentCategory = $allCategories[$parentCategoryId];
								if ($parentCategory) {
									$cpaProduct->addCategory($parentCategory->getId(), $parentCategory->getName());
								}
							}
						}
					}
				}
			}
		}
		// END fill products properties
		
		$this->getResponse()->getHeaders()->addHeaders([
			'Content-Type' => 'text/xml' 
		]);
		return $this->getResponse()->setContent($provider->formatReportReserves($reserves));
	}

}

?>