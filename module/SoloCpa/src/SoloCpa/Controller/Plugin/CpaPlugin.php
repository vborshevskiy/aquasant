<?php

namespace SoloCpa\Controller\Plugin;

use Solo\Mvc\Controller\Plugin\AbstractControllerPlugin;

class CpaPlugin extends AbstractControllerPlugin {

	/**
	 *
	 * @param string $name        	
	 * @param string $arguments        	
	 * @throws \BadMethodCallException
	 * @return mixed
	 */
	public function __call($name, $arguments) {
		$service = $this->getServiceLocator()->get('SoloCpa\Service\CpaService');
		if (!method_exists($service, $name)) {
			throw new \BadMethodCallException('Invalid cpa method: ' . $name);
		}
		return call_user_func_array(array(
			$service,
			$name 
		), $arguments);
	}

	/**
	 *
	 * @param string $name        	
	 * @return \SoloCpa\Service\Tracker\AbstractTracker
	 */
	public function tracker($name = null) {
		$service = $this->getServiceLocator()->get('SoloCpa\Service\TrackerService');
		if (null !== $name) {
			return $service->createTracker($name);
		} else {
			return $service;
		}
	}

}

?>