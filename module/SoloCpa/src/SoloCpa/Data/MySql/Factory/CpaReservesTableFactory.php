<?php

namespace SoloCpa\Data\MySql\Factory;

use Solo\ServiceManager\AbstractFactory;
use Solo\Db\TableGateway\TableGateway;
use SoloCpa\Data\MySql\CpaReservesTable;

class CpaReservesTableFactory extends AbstractFactory {

	/**
	 *
	 * @see \Solo\ServiceManager\AbstractFactory::create()
	 */
	protected function create() {
		$tableGateway = new TableGateway('cpa_reserves', [
			'CampaignID',
			'ReserveID' 
		]);
		$tableGateway->setResultSetPrototype($this->getServiceLocator()->get('SoloCpa\Entity\CpaReserve'));
		return new CpaReservesTable($tableGateway);
	}

}

?>