<?php

namespace SoloCpa\Data\MySql\Factory;

class CampaignSourcesReplicationTableFactory extends CampaignSourcesTableFactory {

	/**
	 *
	 * @see \SoloCpa\Data\MySql\Factory\CampaignSourcesTableFactory::create()
	 */
	protected function create() {
		$table = parent::create();
		$table->getTableGateway()->setTableName('cpa_campaign_sources:passive');
		return $table;
	}

}

?>