<?php

namespace SoloCpa\Data\MySql\Factory;

use Solo\ServiceManager\AbstractFactory;
use Solo\Db\TableGateway\TableGateway;
use SoloCpa\Data\MySql\CampaignSourcesTable;

class CampaignSourcesTableFactory extends AbstractFactory {

	/**
	 *
	 * @see \Solo\ServiceManager\AbstractFactory::create()
	 */
	protected function create() {
		$tableGateway = new TableGateway('cpa_campaign_sources', 'CampaignSourceID', 'active-passive');
		$tableGateway->setResultSetPrototype($this->getServiceLocator()->get('SoloCpa\Entity\CampaignSource'));
		return new CampaignSourcesTable($tableGateway);
	}

}

?>