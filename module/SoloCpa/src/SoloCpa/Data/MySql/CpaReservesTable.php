<?php

namespace SoloCpa\Data\MySql;

use Solo\Db\Table\AbstractTable;
use SoloCpa\Data\CpaReservesTableInterface;
use Solo\DateTime\DateTime;

class CpaReservesTable extends AbstractTable implements CpaReservesTableInterface {

	/**
	 *
	 * @see \SoloCpa\Data\CpaReservesTableInterface::findAllSuccessByCampaignIdAndCreatedAt()
	 */
	public function findAllSuccessByCampaignIdAndCreatedAt($campaignId, DateTime $dateFrom, DateTime $dateTo) {
		$select = $this->createSelect();
		$select->where($this->where()->between('CreatedAt', $dateFrom->getTimestamp(), $dateTo->getTimestamp()));
		$select->where([
			'CampaignID' => $campaignId,
			'IsSuccess' => 1 
		]);
		return $this->selectWith($select);
	}

	/**
	 *
	 * @see \SoloCpa\Data\CpaReservesTableInterface::findAllSuccessByCreatedAt()
	 */
	public function findAllSuccessByCreatedAt(DateTime $dateFrom, DateTime $dateTo) {
		$select = $this->createSelect();
		$select->where($this->where()->between('CreatedAt', $dateFrom->getTimestamp(), $dateTo->getTimestamp()));
		$select->where([
			'IsSuccess' => 1 
		]);
		return $this->selectWith($select);
	}

	/**
	 *
	 * @see \SoloCpa\Data\CpaReservesTableInterface::findAllSuccessByReserveIds()
	 */
	public function findAllSuccessByReserveIds($campaignId, array $reserveIds) {
		$select = $this->createSelect();
		$select->where($this->where()->in('ReserveID', $reserveIds));
		$select->where([
			'CampaignID' => $campaignId,
			'IsSuccess' => 1 
		]);
		return $this->selectWith($select);
	}

	/**
	 *
	 * @see \SoloCpa\Data\CpaReservesTableInterface::findAllNotSynchronized()
	 */
	public function findAllNotSynchronized($campaignId) {
		$sql = "SELECT
					*
				FROM " . $this->getTableGateway()->getTable() . "
				WHERE
					CampaignID = {$this->quoteValue($campaignId)}
					AND StatusUpdatedAt IS NOT NULL
					AND (
						StatusSyncedAt IS NULL
						OR StatusSyncedAt < StatusUpdatedAt
					)";
		return $this->query($sql);
	}

	/**
	 *
	 * @see \SoloCpa\Data\CpaReservesTableInterface::findAllByReserveIds()
	 */
	public function findAllByReserveIds(array $reserveIds) {
		$select = $this->createSelect();
		$select->where($this->where()->in('ReserveID', $reserveIds));
		return $this->selectWith($select);
	}

	/**
	 *
	 * @see \SoloCpa\Data\CpaReservesTableInterface::findAllByReserveNumbers()
	 */
	public function findAllByReserveNumbers(array $reserveNumbers) {
		$select = $this->createSelect();
		$select->where($this->where()->in('ReserveNumber', $reserveNumbers));
		return $this->selectWith($select);
	}

}

?>