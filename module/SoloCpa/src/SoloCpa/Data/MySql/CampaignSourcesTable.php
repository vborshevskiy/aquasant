<?php

namespace SoloCpa\Data\MySql;

use Solo\Db\Table\AbstractTable;
use SoloCpa\Data\CampaignSourcesTableInterface;

class CampaignSourcesTable extends AbstractTable implements CampaignSourcesTableInterface {

	/**
	 *
	 * @see \SoloCpa\Data\CampaignSourcesTableInterface::findOneByUtmSource()
	 */
	public function findOneByUtmSource($utmSource) {
		$select = $this->createSelect();
		$select->where([
			'UtmSource' => $utmSource 
		]);
		$select->limit(1);
		return $this->selectWith($select);
	}

	/**
	 *
	 * @see \SoloCpa\Data\CampaignSourcesTableInterface::findOneByCampaignId()
	 */
	public function findOneByCampaignId($campaignId) {
		$select = $this->createSelect();
		$select->where([
			'CampaignID' => $campaignId 
		]);
		$select->limit(1);
		return $this->selectWith($select);
	}

}

?>