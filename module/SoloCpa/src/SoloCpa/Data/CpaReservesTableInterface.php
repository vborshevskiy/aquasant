<?php

namespace SoloCpa\Data;

use Solo\Db\Table\TableInterface;
use Solo\DateTime\DateTime;
use Zend\Db\ResultSet\ResultSet;

interface CpaReservesTableInterface extends TableInterface {

	/**
	 *
	 * @param integer $campaignId        	
	 * @param DateTime $dateFrom        	
	 * @param DateTime $dateTo        	
	 * @return ResultSet
	 */
	public function findAllSuccessByCampaignIdAndCreatedAt($campaignId, DateTime $dateFrom, DateTime $dateTo);
	
	/**
	 *
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @return ResultSet
	 */
	public function findAllSuccessByCreatedAt(DateTime $dateFrom, DateTime $dateTo);

	/**
	 *
	 * @param integer $campaignId        	
	 * @param array $reserveIds        	
	 * @return ResultSet
	 */
	public function findAllSuccessByReserveIds($campaignId, array $reserveIds);

	/**
	 *
	 * @param integer $campaignId        	
	 * @return ResultSet
	 */
	public function findAllNotSynchronized($campaignId);

	/**
	 *
	 * @param array $reserveIds        	
	 * @return ResultSet
	 */
	public function findAllByReserveIds(array $reserveIds);

	/**
	 *
	 * @param array $reserveNumbers        	
	 * @return ResultSet
	 */
	public function findAllByReserveNumbers(array $reserveNumbers);

}

?>