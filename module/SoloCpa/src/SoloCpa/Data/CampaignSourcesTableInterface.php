<?php

namespace SoloCpa\Data;

use Solo\Db\Table\TableInterface;
use Zend\Db\ResultSet\ResultSet;

interface CampaignSourcesTableInterface extends TableInterface {

	/**
	 *
	 * @param string $utmSource        	
	 * @return ResultSet
	 */
	public function findOneByUtmSource($utmSource);

	/**
	 *
	 * @param integer $campaignId        	
	 * @return ResultSet
	 */
	public function findOneByCampaignId($campaignId);

}

?>