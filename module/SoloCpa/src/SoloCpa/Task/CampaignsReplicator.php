<?php

namespace SoloCpa\Task;

use SoloTasks\Service\AbstractReplicationTask;
use SoloCpa\Data\CampaignSourcesTableInterface;
use SoloErp\WebService\Reader\UltimaCsvListReader;
use SoloTasks\Entity\DataSet;
use SoloTasks\Entity\DataMapper;

class CampaignsReplicator extends AbstractReplicationTask {

	/**
	 *
	 * @var CampaignSourcesTableInterface
	 */
	private $campaignSourcesTable;

	/**
	 *
	 * @param CampaignSourcesTableInterface $campaignSourcesTable        	
	 */
	public function __construct(CampaignSourcesTableInterface $campaignSourcesTable) {
		$this->campaignSourcesTable = $campaignSourcesTable;
	}

	/**
	 *
	 * @see \SoloTasks\Service\AbstractReplicationTask::beginWrite()
	 */
	public function beginWrite() {
		$this->getLog()->info('Begin update campaigns');
	}

	/**
	 *
	 * @see \SoloTasks\Service\TaskInterface::process()
	 */
	public function process() {
		$this->processSources();
	}

	/**
	 *
	 * @see \SoloTasks\Service\AbstractReplicationTask::endWrite()
	 */
	public function endWrite() {
		$this->triggers()->swap([
			$this->campaignSourcesTable->getTableGateway()->getTableBaseName() 
		]);
		$this->getLog()->info('End update campaigns');
	}

	/**
	 * Update campaign source table
	 */
	private function processSources() {
		$wm = $this->erp()->replication()->GetAdvertisingSources();
		$rdr = new UltimaCsvListReader($wm->call());
		if (!$rdr->isEmpty()) {
			$this->campaignSourcesTable->myisam()->startBulkLoad();
			$this->campaignSourcesTable->truncate();
			
			$dataSet = new DataSet();
			$mapper = $this->createSourcesDataMapper();
			foreach ($rdr as $row) {
				$dataSet->add($mapper->convert($row));
			}
			if (0 < count($dataSet)) {
				$this->campaignSourcesTable->insertSet($dataSet);
			}
			
			$this->campaignSourcesTable->myisam()->stopBulkLoad();
			
			$this->getLog()->info(sprintf('Inserted %d campaign sources', $rdr->count()));
		} else {
			$this->getLog()->info('Nothing to update');
		}
	}

	/**
	 *
	 * @return \SoloTasks\Entity\DataMapper
	 */
	private function createSourcesDataMapper() {
		$mapper = new DataMapper([
			'CampaignSourceID' => 'int: ID',
			'CampaignID' => 'int: CampaignID',
			'QueryPattern' => 'Url' 
		]);
		$mapper->setMapping(
			'UtmSource', 
			'Url', 
			function ($field) {
				foreach (explode('&', $field) as $part) {
					list($name, $value) = explode('=', $part);
					if ('utm_source' == $name) {
						return $value;
					}
				}
				return null;
			});
		return $mapper;
	}

}

?>