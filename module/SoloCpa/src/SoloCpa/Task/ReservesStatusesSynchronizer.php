<?php

namespace SoloCpa\Task;

use SoloTasks\Service\AbstractReplicationTask;
use SoloCpa\Data\CpaReservesTableInterface;
use Solo\DateTime\DateTime;
use SoloCpa\Entity\CpaReserve;

class ReservesStatusesSynchronizer extends AbstractReplicationTask {

	const MODE_ALL = 'all';

	const MODE_RESERVES = 'reserves';

	/**
	 *
	 * @var CpaReservesTableInterface
	 */
	protected $cpaReservesTable;

	/**
	 *
	 * @var string
	 */
	protected $mode = self::MODE_ALL;

	/**
	 *
	 * @var array
	 */
	private $reserves = [];

	/**
	 *
	 * @var integer
	 */
	private $updateDoneReservesInDays = 7;

	/**
	 *
	 * @param CpaReservesTableInterface $cpaReservesTable        	
	 */
	public function __construct(CpaReservesTableInterface $cpaReservesTable) {
		$this->cpaReservesTable = $cpaReservesTable;
	}

	/**
	 *
	 * @param array $reserves        	
	 */
	public function addReserves(array $reserves) {
		$this->mode = self::MODE_RESERVES;
		foreach ($reserves as $reserve) {
			$this->reserves[] = $reserve;
		}
	}

	/**
	 *
	 * @see \SoloTasks\Service\TaskInterface::process()
	 */
	public function process() {
		switch ($this->mode) {
			case self::MODE_RESERVES:
				$this->processReserves();
				break;
			case self::MODE_ALL:
			default:
				$this->processAllNotSynced();
				break;
		}
	}

	private function processAllNotSynced() {
		$minCheckDate = DateTime::createFromTimestamp(mktime(0, 0, 0, 5, 13, 2017));
		$campaignIds = [
			341,
			401,
			81,
			101,
			221,
			242 
		];
		
		foreach ($this->cpa()->getPartnersSettings() as $partnerSettings) {
			if ('postback' == $partnerSettings['trackMode']) {
				$campaignId = $partnerSettings['campaignId'];
				if (in_array($campaignId, $campaignIds)) {
					$this->getLog()->info(sprintf('Synchronize campaign %d (%s)', $campaignId, $partnerSettings['uid']));
					
					$provider = $this->cpa()->createProvider($campaignId);
					
					$reserves = $this->cpaReservesTable->findAllNotSynchronized($campaignId);
					$this->getLog()->info(sprintf('Found %d reserves to sync', count($reserves)));
					foreach ($reserves as $reserve) {
						if ($reserve->getStatusUpdatedAt()->greaterThan($minCheckDate)) {
							$canSync = true;
							
							// check for delayed payed reserves
							if (CpaReserve::STATUS_DONE == $reserve->getStatusCode()) {
								$diff = $reserve->getStatusUpdatedAt()->diff(DateTime::now(), true);
								if (7 > $diff->days) {
									$canSync = false;
									if (in_array($reserve->getReserveNumber(), ['Z856633'])) {
										$canSync = true;
									}
								}
							}
							// END check for delayed payed reserves
							
							if ($canSync) {
								$this->getLog()->info(sprintf('Reserve %d', $reserve->getReserveId()));
								if ($provider->sendPostback($reserve)) {
									$reserve->setStatusSyncedAt(DateTime::now());
									$this->cpa()->updateReserveInLog($reserve);
									$this->getLog()->info('success');
								} else {
									$this->getLog()->info('failure');
								}
							}
						}
					}
				}
			}
		}
	}

	private function processReserves() {
		$campaignIds = [
			341,
			401,
			81,
			101,
			221,
			242 
		];
		// $campaignIds = [242];
		$reserves = $this->cpaReservesTable->findAllByReserveIds($this->reserves);
		$this->getLog()->info(sprintf('Found %d reserves to sync', count($reserves)));
		foreach ($reserves as $reserve) {
			$campaignId = $reserve->getCampaignId();
			if (in_array($campaignId, $campaignIds)) {
				$this->getLog()->info(sprintf('Reserve %d', $reserve->getReserveId()));
				
				$provider = $this->cpa()->createProvider($campaignId);
				
				if ($provider->sendPostback($reserve)) {
					$reserve->setStatusSyncedAt(DateTime::now());
					$this->cpa()->updateReserveInLog($reserve);
					$this->getLog()->info('success');
				} else {
					$this->getLog()->info('failure');
				}
			}
		}
	}

}

?>