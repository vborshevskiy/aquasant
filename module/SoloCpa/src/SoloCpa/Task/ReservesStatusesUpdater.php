<?php

namespace SoloCpa\Task;

use SoloTasks\Service\AbstractReplicationTask;
use Solo\DateTime\DateTime;

class ReservesStatusesUpdater extends AbstractReplicationTask {

	/**
	 *
	 * @see \SoloTasks\Service\TaskInterface::process()
	 */
	public function process() {
		$campaignIds = [
			341,
			401,
			81,
			101,
			221,
			242 
		];
		
		foreach ($this->cpa()->getPartnersSettings() as $partnerSettings) {
			if ('postback' == $partnerSettings['trackMode']) {
				$campaignId = $partnerSettings['campaignId'];
				
				if (in_array($campaignId, $campaignIds)) {
					$this->getLog()->info(sprintf('Update campaign %d (%s)', $campaignId, $partnerSettings['uid']));
					
					$fromDate = DateTime::now()->addDays(-45);
					$minCheckDate = DateTime::createFromTimestamp(mktime(0, 0, 0, 5, 13, 2017));
					if ($fromDate->lowerThan($minCheckDate)) {
						$fromDate = $minCheckDate;		
					}
					$toDate = DateTime::now()->addDays(1);
					
					$statuses = $this->cpa()->findReservesByUpdateDateRange($campaignId, $fromDate, $toDate);
					$this->getLog()->info(sprintf('Found %d reserves to update', count($statuses)));
					$affectedRowsCount = 0;
					foreach ($statuses as $status) {
						$cpaReserve = $this->cpa()->getCampaignReserve($campaignId, $status->ID);
						if ($cpaReserve) {
							$statusCode = intval($status->Status);
							if ($cpaReserve->getStatusCode() !== $statusCode) {
								$cpaReserve->setReward(floatval($status->RewardAmount));
								$cpaReserve->setStatusCode($statusCode);
								$cpaReserve->setStatusUpdatedAt(DateTime::now());
								$this->cpa()->updateReserveInLog($cpaReserve);
								
								$affectedRowsCount++;
								$this->getLog()->info(sprintf('#%d: %d status', $status->ID, $statusCode));
							}
						}
					}
					$this->getLog()->info(sprintf('Updated %d reserves', $affectedRowsCount));
				}
			}
		}
	}

}

?>