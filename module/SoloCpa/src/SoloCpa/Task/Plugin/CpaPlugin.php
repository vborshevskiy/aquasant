<?php

namespace SoloCpa\Task\Plugin;

use SoloTasks\Service\AbstractTaskPlugin;

class CpaPlugin extends AbstractTaskPlugin {

	/**
	 *
	 * @param string $name        	
	 * @param string $arguments        	
	 * @throws \BadMethodCallException
	 * @return mixed
	 */
	public function __call($name, $arguments) {
		$service = $this->getServiceLocator()->get('SoloCpa\Service\CpaService');
		if (!method_exists($service, $name)) {
			throw new \BadMethodCallException('Invalid cpa method: ' . $name);
		}
		return call_user_func_array(array(
			$service,
			$name 
		), $arguments);
	}

}

?>