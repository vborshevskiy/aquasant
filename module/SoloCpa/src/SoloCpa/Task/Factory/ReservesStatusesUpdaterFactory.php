<?php

namespace SoloCpa\Task\Factory;

use Solo\ServiceManager\AbstractFactory;
use SoloCpa\Task\ReservesStatusesUpdater;

class ReservesStatusesUpdaterFactory extends AbstractFactory {

	/**
	 *
	 * @see \Solo\ServiceManager\AbstractFactory::create()
	 */
	protected function create() {
		$task = new ReservesStatusesUpdater();
		
		$task->isLockable(false);
		
		return $task;
	}

}

?>