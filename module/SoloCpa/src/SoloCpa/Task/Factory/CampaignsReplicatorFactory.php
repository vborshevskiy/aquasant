<?php

namespace SoloCpa\Task\Factory;

use Solo\ServiceManager\AbstractFactory;
use SoloCpa\Task\CampaignsReplicator;

class CampaignsReplicatorFactory extends AbstractFactory {

	/**
	 *
	 * @see \Solo\ServiceManager\AbstractFactory::create()
	 */
	protected function create() {
		$campaignSourcesTable = $this->getServiceLocator()->get('SoloCpa\Data\CampaignSourcesReplicationTable');
		$task = new CampaignsReplicator($campaignSourcesTable);
		return $task;
	}

}

?>