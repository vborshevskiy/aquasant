<?php

namespace SoloCpa\Task\Factory;

use Solo\ServiceManager\AbstractFactory;
use SoloCpa\Task\ReservesStatusesSynchronizer;

class ReservesStatusesSynchronizerFactory extends AbstractFactory {

	/**
	 *
	 * @see \Solo\ServiceManager\AbstractFactory::create()
	 */
	protected function create() {
		$cpaReservesTable = $this->getServiceLocator()->get('SoloCpa\Data\CpaReservesTable');
		$task = new ReservesStatusesSynchronizer($cpaReservesTable);
		
		// fill settings
		$request = $this->service('Request');
		if ($request->getParam('reserves')) {
			$reserves = explode(',', $request->getParam('reserves'));
			$task->addReserves($reserves);
		}
		
		$task->isLockable(false);
		
		return $task;
	}

}

?>