<?php

use SoloSearch\Service\SearchManager;

return array(
    'service_manager' => array(
        'aliases' => array(
            'search_manager' => 'SoloSearch\Service\SearchManager',
            'searchLogger' => 'SoloSearch\Service\SearchLoggerService'
        ),
        'search_indexes' => [
            'goods' => 'SoloSearch\Data\GoodsIndexLocator',
    		'all_goods' => 'SoloSearch\Data\GoodsIndexLocator',
            'categories' => 'SoloSearch\Data\CategoriesIndexLocator'
        ],
        'factories' => array(
            'SphinxConnection' => 'SoloSearch\Service\ConnectionServiceFactory',
            'SoloSearch\Service\SearchManager' => function ($sm) {
                $serv = new SearchManager();
                $serv->setConfigTemplatePath($sm->get('Config')['search']['config_tpl']);
                $serv->setTmpConfigPath($sm->get('Config')['search']['tmp_config_path']);
                return $serv;
            },
            'SoloSearch\Service\SearchLoggerService' => function ($sm) {
                $config = $sm->get('Config')['search']['logger'];
                if (!$config) {
                    $config['enabled'] = false;
                }
                return new \SoloSearch\Service\SearchLoggerService($config);
            }
        )
    ),
    'router' => array(
        'routes' => array(
            'searchlogger' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'priority' => 1000,
                'options' => array(
                    'route' => '/searchlogger',
                    'defaults' => array(
                        'controller' => 'SoloSearch\Controller\SearchLogger',
                        'action' => 'index'
                    )
                )
            )
        )
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
                'consoleSearchlogger' => array(
                    'options' => array(
                        'route' => 'searchlogger',
                        'defaults' => array(
                            'controller' => 'SoloSearch\Controller\SearchLogger',
                            'action' => 'console'
                        )
                    )
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'SoloSearch\Controller\SearchLogger' => 'SoloSearch\Controller\SearchLoggerController'
        )
    )
);


