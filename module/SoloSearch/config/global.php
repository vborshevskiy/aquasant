<?php
use Solo\Search\Adapter\Platform\Sphinx;
use Zend\EventManager\Event;

return [
	'search' => [
		'connection' => [
			'driver' => 'Pdo',
                        'dsn' => 'mysql:host=127.0.0.1;port=9306',
			'username' => 'user',
			'password' => '123',
		],
		'default' => [
			'ranker' => 'sph04' 
		],
		'logger' => [
			'enabled' => true 
		],
		'formatters' => [
			'simple_sphinx' => function (Event $e) {
				$locator = $e->getTarget();
				$expression = $e->getParam('expression');
				if (empty($expression)) {
					$locator->options()->apply(function ($value) use($locator) {
						$value->setValue(Sphinx::escape(trim($value->getValue(), '*')));
					});
				} else {
					$expression = Sphinx::escape(trim($expression, '*'));
				}
				$e->setParam('expression', $expression);
			} 
		],
        'config_tpl' => '/home/aqua/web/aquaweb.supportit.ru/aquasant/module/SoloSearch/config/sphinx/sphinx.conf',
		'tmp_config_path' => '/home/aqua/web/aquaweb.supportit.ru/aquasant/module/SoloSearch/config/sphinx/sphinx.conf',
	] 
];