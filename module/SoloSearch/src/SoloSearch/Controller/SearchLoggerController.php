<?php

namespace SoloSearch\Controller;

use Solo\Mvc\Controller\ActionController;

class SearchLoggerController extends ActionController {

	public function indexAction() {
		echo $this->appendSearchHistory();
		exit();
	}

	public function consoleAction() {
		print $this->appendSearchHistory();
		exit();
	}

	private function appendSearchHistory() {
		try {
			$logger = $this->service('searchLogger');
			$pRedis = $this->service('PredisClient');
			$logger->appendSearchHistory($pRedis);
			return 'success';
		} catch (\Exception $e) {
			return 'exception: ' . $e->getMessage();
		}
	}

}
