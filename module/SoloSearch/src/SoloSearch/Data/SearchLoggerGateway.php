<?php

namespace SoloSearch\Data;

use Solo\Db\QueryGateway\QueryGateway;

class SearchLoggerGateway extends QueryGateway {

	/**
	 *
	 * @param
	 *        	array of SearchKey
	 */
	public function appendSearchHistory($data) {
		if ($data) {
			$sql = 'Insert Into search_logger (QueryText, QueryTimeStamp, IsAutoComplete) Values ';
			$values = [];
			foreach ($data as $key) {
				$values[] = "('" . $key->getQuery() . "'," . $key->getTime() . "," . ($key->getAutoComplete() == true ? 1 : 0) . ")";
			}
			$sql .= implode(",", $values);
			return $this->query($sql);
		}
	}

}

?>