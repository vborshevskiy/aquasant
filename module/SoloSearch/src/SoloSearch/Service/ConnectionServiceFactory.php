<?php

namespace SoloSearch\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Adapter\Adapter;

class ConnectionServiceFactory implements FactoryInterface {

	/**
	 * Creates database adapter
	 * 
	 * @param ServiceLocator $serviceLocator        	
	 * @return Adapter
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		$config = $serviceLocator->get('Config')['search'];
		if (isset($config['connection'])) {
			$settings = $config['connection'];
			return new Adapter($settings);
		}
	}

}

?>