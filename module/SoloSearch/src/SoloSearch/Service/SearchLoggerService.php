<?php

namespace SoloSearch\Service;

use SoloSearch\Entity\SearchKey;
use SoloSearch\Data\SearchLoggerGateway;

/**
 * SearchService
 *
 * @author Slava Tutrinov
 */
class SearchLoggerService {

	/**
	 *
	 * @var array of SearchKey
	 */
	private $cache;

	/**
	 * will not work if false
	 *
	 * @var boolean
	 */
	private $enabled = true;

	public function __construct($settings) {
		if ($settings['enabled'] == false) {
			$this->enabled = false;
		}
	}

	public function addKey($pRedis, $query, $autoComplete = false) {
		if (!$this->enabled) return;
		$key = uniqid('SH:');
		$searchKey = new SearchKey($key, $query, $autoComplete);
		$pRedis->set($key, serialize($searchKey));
		$pRedis->sadd('SH:keys', $key);
	}

	public function removeKeys($pRedis, $keys) {
		if (!$this->enabled) return;
		$pipe = $pRedis->multiExec();
		foreach ($keys as $key) {
			$pRedis->srem('SH:keys', $key);
		}
		$pRedis->del($keys);
		$pipe->execute();
	}

	public function getCache($pRedis) {
		$this->getKeys($pRedis);
		return $this->cache;
	}

	public function getKeys($pRedis) {
		if (!$this->enabled) return;
		$keys = $pRedis->smembers('SH:keys');
		$data = $pRedis->mget($keys);
		foreach ($data as $key) {
			$this->cache[] = unserialize($key);
		}
		return $keys;
	}

	public function appendSearchHistory($pRedis) {
		if (!$this->enabled) return;
		$keys = $this->getKeys($pRedis);
		$queryGateway = new SearchLoggerGateway();
		$queryGateway->appendSearchHistory($this->cache);
		$this->removeKeys($pRedis, $keys);
	}

}

?>
