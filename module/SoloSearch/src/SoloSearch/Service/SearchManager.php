<?php

namespace SoloSearch\Service;

use Solo\ServiceManager\ServiceLocatorAwareService;
use Solo\Inflector\Inflector;
use Solo\Search\IndexLocator\IndexLocatorInterface;

class SearchManager extends ServiceLocatorAwareService {

	private $configTemplatePath = null;

	private $tmpConfigPath = null;

	/**
	 *
	 * @param string $name        	
	 * @throws \RuntimeException
	 * @return IndexLocatorInterface
	 */
	public function factoryLocator($name) {
		$locator = null;		
		$locator = $this->factoryLocatorByAliase($name);
		
		if (null === $locator) {
			$locator = $this->factoryLocatorByFullyQualifiedName($name);
		}
		
		if (null === $locator) {
			throw new \RuntimeException(sprintf('Failed to create locator %s', $locator));
		}
		
		return $locator;
	}

	/**
	 *
	 * @param string $name        	
	 * @return IndexLocatorInterface
	 */
	private function factoryLocatorByAliase($name) {
		$config = $this->getServiceLocator()->get('Config');
		if (isset($config['search']['indexes'])) {                      
			$settings = $config['search']['indexes'];                       
			if (is_array($settings) && array_key_exists($name, $settings)) {
				$fullName = $settings[$name]['name'];            
				if ($this->getServiceLocator()->has($fullName)) {
					return $this->getServiceLocator()->get($fullName);
				}
			}
		}
		return null;
	}

	/**
	 *
	 * @param string $name        	
	 * @return IndexLocatorInterface
	 */
	private function factoryLocatorByFullyQualifiedName($name) {           
		$fullName = '\\SoloCatalog\\Search\\' . Inflector::camelize($name) . 'IndexLocator';                       
		if ($this->getServiceLocator()->has($fullName)) {
			return $this->getServiceLocator()->get($fullName);
		}
		return null;
	}

	public function getConfigTemplatePath() {
		return $this->configTemplatePath;
	}

	public function setConfigTemplatePath($configTemplatePath) {
		$this->configTemplatePath = $configTemplatePath;
	}

	public function getTmpConfigPath() {
		return $this->tmpConfigPath;
	}

	public function setTmpConfigPath($tmpConfigPath) {
		$this->tmpConfigPath = $tmpConfigPath;
	}

}

?>
