<?php

namespace SoloSearch\Entity;

class SearchKey {

	private $key;

	private $query;

	private $time;

	private $autoComplete;

	public function __construct($key, $query, $autoComplete) {
		$this->key = $key;
		$this->query = $query;
		$this->autoComplete = $autoComplete;
		$this->time = time();
	}

	public function getKey() {
		return $this->key;
	}

	public function getTime() {
		return $this->time;
	}

	public function getQuery() {
		return $this->query;
	}

	public function getAutoComplete() {
		return $this->autoComplete;
	}

}

?>