<?php

namespace SoloSearch\Entity;

class AutocompleteResult {

	private $groups;
        private $query;

	public function __construct($query) {
		$this->groups = [];
                $this->query=$query;
	}

	public function addGroup($GroupName) {
		$group = new AutocompleteResultGroup();
		$this->groups[$GroupName] = $group;
		return $group;
	}

	public function highlightQuery($query) {
		foreach ($this->groups as $group) {
			$group->highlightQuery($query);
		}
	}

	public function getResult() {
		$results['query'] = $this->query;
                foreach($this->groups as $groupName=>$group)
                {
                   $results[$groupName]=$group->getResult();
                }                		
		return json_encode($results);
	}

	public function getItems() {
	    $result = [];

	    $items = $this->groups['items'];

	    if ($items) {
		    foreach ($items->getResult() as $item) {
		        $result[] = ['context' => $item['text'], 'href' => $item['href']];
	        }
	    }

        return $result;
    }

}

?>