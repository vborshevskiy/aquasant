<?php

namespace SoloSearch\Entity;

class AutocompleteResultGroup {

	private $items;
        private $name;

	public function __construct() {
		$this->items = [];              
	}

	public function addItem($title, $url) {
		$this->items[] = [
			'text' => $title,
			'href' => $url 
		];
	}
        
	public function highlightQuery($query) {
		for ($i = 0, $size = sizeof($this->items); $i < $size; $i++) {
			$this->items[$i]['text'] = preg_replace("/(" . $query . ")/iu", '@$1@', $this->items[$i]['text']);
		}
	}        
	public function getResult() {  
                          
                return $this->items;            
	}      

}

?>