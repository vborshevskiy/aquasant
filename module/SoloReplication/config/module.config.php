<?php

use Solo\Db\QueryGateway\QueryGateway;
use SoloReplication\Data\YandexYmlMapper;

use SoloReplication\Service\Kladr;
use SoloReplication\src\SoloReplication\Service\DatabaseBalancing;

return array(
    'router' => array(
        'routes' => array(
            'replication2' => array(
                'type' => 'Segment',
                'priority' => 1001,
                'options' => array(
                    'route' => '/replication/tasks/[:type]',
                    'constraints' => array(
                        'type' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'SoloReplication\Controller\Starter',
                        'action' => 'manager'
                    )
                )
            ),
            'replication' => array(
                'type' => 'Segment',
                'priority' => 1000,
                'options' => array(
                    'route' => '/replication[/][:type]',
                    'constraints' => array(
                        'type' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'SoloReplication\Controller\Starter',
                        'action' => 'index'
                    )
                )
            )
        )
    ),
    'service_manager' => array(
        'factories' => array(
            'SoloReplication\Service\Kladr' => function ($sm) {
                $serv = new Kladr();
                $serv->setSearch($sm->get('search_manager'));
                return $serv;
            },
            'SoloReplication\Service\DatabaseBalancing' => function ($sm) {
                return new DatabaseBalancing($sm->get('Solo\\Db\\BalancingManager\\BalancingObserver'));
            },
            'SoloReplication\Data\YandexYmlMapper' => function ($sm) {
                return new YandexYmlMapper(new QueryGateway());
            },
            'SoloReplication\Service\TaskManager' => 'SoloReplication\Service\TaskManagerFactory',
            'SoloReplication\Service\ProcessManager' => 'SoloReplication\Service\ProcessManagerFactory'
        )
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
                'replication' => array(
                    'options' => array(
                        'route' => 'repl [<name>]',
                        'defaults' => array(
                            'controller' => 'SoloReplication\Controller\Starter',
                            'action' => 'console'
                        )
                    )
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'SoloReplication\Controller\Starter' => 'SoloReplication\Controller\StarterController'
        )
    )
);
