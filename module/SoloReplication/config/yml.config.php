<?php

return [
    'yml' => [
        'shopName' => 'Сантехбаза.ru',
        'company' => 'Сантехбаза.ru - гипермаркет сантехники в Москве',
        'siteUrl' => 'https://www.'.DOMAIN_NAME.'/',
        'currencies' => [
            [
                'id' => 'RUR',
                'rate' => 1,
            ],
        ],
        'cities' => [
            2 => 'msk'
        ],
        'citiesSubdomains' => [
            2 => ''
        ],
        'mainDomain' => DOMAIN_NAME,
    ],
];