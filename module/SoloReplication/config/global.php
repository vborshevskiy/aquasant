<?php
return [
	'solo-replication' => [
		'log_path' => 'data/log/',
		'locks_path' => 'data/lock/',
		'img_path' => dirname(__FILE__) . '/../../../' . 'public/img/',
		'plugins' => [
			'availGoods' => 'SoloReplication\Service\Plugin\AvailGoods',
			'availWarehouses' => 'SoloReplication\Service\Plugin\AvailWarehouses' 
		] 
	] 
]
;