<?php

namespace SoloReplication\Model;

use SoloSettings\Triggers;
use Solo\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;

class GenericModel extends TableGateway {

	/**
	 *
	 * @var string
	 */
	protected $basename = null;

	/**
	 *
	 * @var Triggers
	 */
	private $triggers;

	/**
	 *
	 * @param string $basename        	
	 * @param mixed $primaryKeys        	
	 * @param Triggers $triggers        	
	 */
	public function __construct($basename, $primaryKeys, Triggers $triggers) {
		$this->basename = $basename;
		$this->triggers = $triggers;
		parent::__construct($this->triggers->passive($this->basename), $primaryKeys);
	}

	/**
	 *
	 * @param string $type        	
	 */
	private function initTableNameByType($type) {
		$name = ((0 == strcasecmp('active', $type)) ? $this->triggers->active($this->basename) : $this->triggers->passive($this->basename));
		$this->setTable($name);
	}

	/**
	 *
	 * @param array $data        	
	 * @param string $type        	
	 */
	public function insertSet($data, $type, $updateOnDuplicate = false) {
		$this->initTableNameByType($type);
		if (0 < sizeof($data)) {
			reset($data);
			$firstRow = current($data);
			$cols = array_keys($firstRow);
			
			$cmd = "INSERT INTO " . $this->quoteIdentifier($this->getTable()) . " (`" . implode('`, `', $cols) . "`) VALUES (%s)";
			if ($updateOnDuplicate) {
				foreach ($data as $row) {
					$values = [];
					foreach ($row as $key => $val) {
						if (null === $val) {
							$values[$key] = 'NULL';
						} else {
							$values[$key] = $this->quoteValue(str_replace("\\", "\\\\", $val));
						}
					}
					$sql = sprintf($cmd, implode(', ', $values));
					
					foreach ($values as $col => $value) {
						$onDuplicateSql = [];
						if (!in_array($col, $this->primaryKeys)) {
							$onDuplicateSql[] = $col.'='.$value;
						} 
					}
					$sql .= " ON DUPLICATE KEY UPDATE ".implode(', ', $onDuplicateSql);
					$this->getAdapter()->query($sql, Adapter::QUERY_MODE_EXECUTE);
				}
			} else {
				$values = [];
				foreach ($data as $row) {
					$valueRow = [];
					foreach ($row as $val) {
						if (null === $val) {
							$valueRow[] = 'NULL';
						} else {
							$valueRow[] = $this->quoteValue(str_replace("\\", "\\\\", $val));
						}
					}
					$values[] = implode(', ', $valueRow);
				}
				
				$sql = sprintf($cmd, implode('), (', $values));
				$this->getAdapter()->query($sql, Adapter::QUERY_MODE_EXECUTE);
			}
		}
	}

	/**
	 *
	 * @param array $data        	
	 */
	public function insertPassive($data) {
		$this->initTableNameByType('passive');
		return $this->insert($data);
	}

	/**
	 *
	 * @param array $data        	
	 */
	public function insertActive($data) {
		$this->initTableNameByType('active');
		return $this->insert($data);
	}

	/**
	 *
	 * @param array $data        	
	 */
	public function insertSetActive($data, $updateOnDuplicate = false) {
		return $this->insertSet($data, 'active', $updateOnDuplicate);
	}

	/**
	 *
	 * @param array $data        	
	 */
	public function insertSetPassive($data, $updateOnDuplicate = false) {
		return $this->insertSet($data, 'passive', $updateOnDuplicate);
	}
	
	/**
	 * Clears all data in active table
	 */
	public function clearActive() {
		$this->initTableNameByType('active');
		$this->clearTable();
	}
	
	/**
	 * Clears all data in passive table
	 */
	public function clearPassive() {
		$this->initTableNameByType('passive');
		$this->clearTable();
	}

}

?>