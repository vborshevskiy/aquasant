<?php

namespace SoloReplication\Service;

use Solo\EventManager\ProvidesEvents;
use Zend\Log\Logger;

abstract class AbstractTask implements TaskInterface {
	
	use ProvidesEvents;

	/**
	 *
	 * @var string
	 */
	private $name = null;

	/**
	 *
	 * @var Logger
	 */
	protected $log = null;

	/**
	 * 
	 * @param string $name
	 * @return \SoloReplication\Service\AbstractTask
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return string
	 */
	public function getName() {
		if (null === $this->name) {
			throw new \RuntimeException(sprintf('Not set name for task in %s', __CLASS__));
		}
		return $this->name;
	}
	
	/**
	 * 
	 * @param Logger $logger
	 * @return \SoloReplication\Service\AbstractTask
	 */
	public function setLog(Logger $logger) {
		$this->log = $logger;
		return $this;
	}
	
	/**
	 * 
	 * @return \Zend\Log\Logger
	 */
	public function getLog() {
		return $this->log;
	}

}

?>