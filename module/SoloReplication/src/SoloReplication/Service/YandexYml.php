<?php

namespace SoloReplication\Service;

use SoloReplication\Data\YandexYmlMapperInterface;
use Solo\Yandex\Yml\Catalog;
use Solo\Yandex\Yml\Currency;
use Solo\Yandex\Yml\Shop;
use Solo\Yandex\Yml\Category;
use Solo\Yandex\Yml\DeliveryOption;
use Solo\Yandex\Yml\Offer;
use Solo\Db\QueryGateway\QueryGateway;
use Solo\Google\Feed\Feed;
use Solo\Google\Feed\FeedItem;
use Solo\Google\Feed\Writer\FeedWriter;

trait YandexYml {

	/**
	 *
	 * @var YandexYmlMapperInterface
	 */
	protected $yandexYmlMapper;

	/**
	 *
	 * @var array
	 */
	protected $onlyDeliveryCitiesIds = [];

	/**
	 *
	 * @var array
	 */
	protected $citiesSubdomains = [];

	/**
	 *
	 * @var array
	 */
	protected $shipppingPossibleDates = [];

	/**
	 *
	 * @param \SoloCatalog\Task\Service\YandexYmlMapperInterface $yandexYmlMapper        	
	 */
	public function setYandexYmlMapper(YandexYmlMapperInterface $yandexYmlMapper) {
		$this->yandexYmlMapper = $yandexYmlMapper;
	}

	/**
	 *
	 * @param array $onlyDeliveryCitiesIds        	
	 */
	public function setOnlyDeliveryCitiesIds($onlyDeliveryCitiesIds) {
		if (is_array($onlyDeliveryCitiesIds)) {
			$this->onlyDeliveryCitiesIds = $onlyDeliveryCitiesIds;
		}
	}

	/**
	 *
	 * @param array $citiesSubdomains        	
	 */
	public function setCitiesSubdomains($citiesSubdomains) {
		if (is_array($citiesSubdomains)) {
			$this->citiesSubdomains = $citiesSubdomains;
		}
	}

	/**
	 *
	 * @param array $dates        	
	 */
	public function setShipppingPossibleDates(array $dates) {
		$this->shipppingPossibleDates = $dates;
	}

	/**
	 *
	 * @param
	 *        	array | string $tables
	 */
	protected function setPassiveTables($tables) {
		if (is_array($tables)) {
			foreach ($tables as $table) {
				$this->yandexYmlMapper->addPassiveTable($table);
			}
		} elseif (is_string($tables)) {
			$this->yandexYmlMapper->addPassiveTable($tables);
		}
	}

	protected function createYandexYml() {
		$this->setPassiveTables($this->getSwapTables());
		$this->log->info('Start creating YML');
		$cityIds = $this->yandexYmlMapper->getCities();
		if (array_key_exists('siteUrl', $this->getYmlSettings())) {
			$siteUrl = $this->getYmlSettings()['siteUrl'];
		} else {
			$this->log->err('Setting "siteUrl" is not valid');
			return;
		}
		if (array_key_exists('citiesSubdomains', $this->getYmlSettings())) {
			$citiesSubdomains = $this->getYmlSettings()['citiesSubdomains'];
		} else {
			$this->log->err('Setting "citiesSubdomains" can\'t be empty');
			return;
		}
		if (array_key_exists('mainDomain', $this->getYmlSettings())) {
			$mainDomain = $this->getYmlSettings()['mainDomain'];
		} else {
			$this->log->err('Setting "citiesMainDomain" can\'t be empty');
			return;
		}
		$goodsInfo = [];
		
		$holidaysInfo = $this->yandexYmlMapper->getHolidays();
		$holidays = [];
		foreach ($holidaysInfo as $holidayInfo) {
			$holidays[] = \DateTime::createFromFormat('Y-m-d H:i:s', $holidayInfo);
		}
		
		foreach ($cityIds as $cityId => $officeIds) {
			$this->buildXml($officeIds, $goodsInfo, $siteUrl, $cityId, $citiesSubdomains, $mainDomain, $holidays);
			if (false && $cityId === \SoloDelivery\Service\DeliveryService::MOSCOW_ID) {
				$this->buildXml($officeIds, $goodsInfo, $siteUrl, $cityId, $citiesSubdomains, $mainDomain, $holidays, true);
			}
		}
		foreach ($cityIds as $cityId => $officeIds) {
			$this->buildAvailXml($officeIds, $goodsInfo, $siteUrl, $cityId, $citiesSubdomains, $mainDomain, $holidays);
		}
		foreach ($cityIds as $cityId => $officeIds) {
			$this->buildAvailXml($officeIds, $goodsInfo, $siteUrl, $cityId, $citiesSubdomains, $mainDomain, $holidays, false, 'google');
		}
		
		foreach ($cityIds as $cityId => $officeIds) {
			if (2 == $cityId) {
				$this->buildGoogleXml($officeIds, $goodsInfo, $siteUrl, $cityId, $citiesSubdomains, $mainDomain, $holidays, false, 'google');
			}
		}
		
		$this->log->info('Stop creating YML');
	}

	/**
	 *
	 * @param array $officeIds        	
	 * @param array $goodsInfo        	
	 * @param string $siteUrl        	
	 * @param integer $cityId        	
	 * @param array $citiesSubdomains        	
	 * @param string $mainDomain        	
	 * @param array $holidays        	
	 * @param boolean $isRegionPrice        	
	 * @param \DateTime $regionDeliveryShippingDate        	
	 * @return type
	 */
	protected function buildXml($officeIds, &$goodsInfo, $siteUrl, $cityId, $citiesSubdomains, $mainDomain, $holidays, $isRegionPrice = false, $format = 'yml') {
		$queryGateway = new QueryGateway();
		
		if (array_key_exists($cityId, $citiesSubdomains)) {
			$subdomain = $citiesSubdomains[$cityId];
		} else {
			$this->log->err('Setting "Subdomain" for city ' . $cityId . ' is empty');
			return;
		}
		$onlyDelivery = in_array($cityId, $this->onlyDeliveryCitiesIds);
		$this->log->info('City id = ' . $cityId);
		$ymlCatalog = new Catalog();
		$this->setShopSettings($ymlCatalog->shop());
		$availGoods = $this->yandexYmlMapper->getAvailGoods($officeIds);
		$this->log->info('Avail goods = ' . count($availGoods));
		$providerSuborderGoods = $this->yandexYmlMapper->getSuborderGoods($officeIds);
		$this->log->info('Provider suborder goods = ' . count($providerSuborderGoods->avail));
		$mainWarehouseSuborderGoods = $this->yandexYmlMapper->getMainWarehouseSuborderGoods($officeIds);
		$this->log->info('Main warehouse suborder goods = ' . count($mainWarehouseSuborderGoods->avail));
		$suborderGoods = $this->mergeProviderAndMainWarehouseSuborder($providerSuborderGoods, $mainWarehouseSuborderGoods);
		$this->log->info('Suborder goods = ' . count($suborderGoods->avail));
		$deliveryGoods = $this->yandexYmlMapper->getDeliveryGoods($cityId);
		$this->log->info('Delivery goods = ' . count($deliveryGoods));
		$categoriesIds = array_unique(array_merge($availGoods, $suborderGoods->categories));
		$allCategories = $this->yandexYmlMapper->getCategories();
		$menu = $this->createMenu($categoriesIds, $allCategories);
		foreach ($menu as $category) {
			$ymlCatalog->shop()->categories()->add(new Category($category['id'], $category['name'], $category['parentId']));
		}
		
		if (!$isRegionPrice) {
			$ymlCatalog->shop()->deliveryOptions()->add(new DeliveryOption());
		}
		
		$goodIds = array_unique(array_merge(array_keys($availGoods), array_keys($suborderGoods->avail)));
		$infoGoodIds = array_diff($goodIds, array_keys($goodsInfo));
		$goodIdsChunks = array_chunk($infoGoodIds, 500);
		foreach ($goodIdsChunks as $goodIdsChunk) {
			$goodsInfo += $this->yandexYmlMapper->getGoodsInfo($goodIdsChunk);
		}
		$this->log->info('Goods info = ' . count($goodsInfo));
		
		$priceCategoryId = ($isRegionPrice ? \SoloCatalog\Service\Helper\PricesHelper::DEFAULT_REGION_PRICE_COLUMN_ID : \SoloCatalog\Service\Helper\PricesHelper::DEFAULT_PRICE_COLUMN_ID);
		$this->log->info('Price column: ' . $priceCategoryId);
		$goodsPrices = $this->yandexYmlMapper->getGoodsPrices($cityId, $goodIds, $priceCategoryId);
		$this->log->info('Goods prices = ' . count($goodsPrices));
		$vendors = $this->yandexYmlMapper->getVendorsByGoodIds($goodIds);
		$goodsDeliveryPrice = $this->yandexYmlMapper->getGoodsDeliveryPrice($cityId, $goodIds);
		$this->log->info('Goods delivery prices = ' . count($goodsDeliveryPrice));
		$goodsImages = $this->yandexYmlMapper->getGoodsImages($goodIds);
		$goodsReserveAvailDates = $this->yandexYmlMapper->getReserveAvailDates($cityId, $goodIds);
		$this->log->info('Goods reserve avail dates = ' . count($goodsReserveAvailDates));
		
		foreach ($goodIds as $goodId) {
			// если нет даты доставки, то товар нельзя доставить через ТК
			if ($isRegionPrice && !isset($goodsReserveAvailDates[$goodId])) {
				continue;
			}
			// определяем available
			// true, если в наличии и дата доставки в пределах двух дней
			// true, если под заказ и дата доставки в пределах двух дней
			// иначе false
			if (isset($goodsReserveAvailDates[$goodId])) {
				$pickupDate = new \DateTime($goodsReserveAvailDates[$goodId]['PickupDate']);
				$deliveryDate = \SoloCatalog\Service\Helper\DateHelper::getMinDate(
					[
						new \DateTime($goodsReserveAvailDates[$goodId]['AvailDeliveryDate']),
						new \DateTime($goodsReserveAvailDates[$goodId]['SuborderDeliveryDate']) 
					]);
				$nearestDeliveryShippingDate = $this->getNearestDeliveryShippingDate($deliveryDate);
			} else {
				$pickupDate = null;
				$deliveryDate = null;
				$nearestDeliveryShippingDate = null;
			}
			
			if ($isRegionPrice) {
				$available = (!is_null($nearestDeliveryShippingDate) && \SoloCatalog\Service\Helper\DateHelper::isFutureDate($nearestDeliveryShippingDate) && \SoloCatalog\Service\Helper\DateHelper::isInWorkDaysIntervalDate(
					$nearestDeliveryShippingDate, 
					2, 
					$holidays));
			} else {
				if (!is_null($pickupDate) && \SoloCatalog\Service\Helper\DateHelper::isFutureDate($pickupDate) && \SoloCatalog\Service\Helper\DateHelper::isInWorkDaysIntervalDate($pickupDate, 2, [])) {
					$available = true;
				} else {
					$available = false;
				}
				if (!is_null($deliveryDate) && \SoloCatalog\Service\Helper\DateHelper::isFutureDate($deliveryDate)) {
					$localDeliveryDays = \SoloCatalog\Service\Helper\DateHelper::getWorkDaysInterval($deliveryDate, $holidays);
				} else {
					$localDeliveryDays = null;
				}
			}
			
			// disable suborder
			if (isset($availGoods[$goodId]) && (0 == $availGoods[$goodId]['AvailQauntity']) && (0 < $availGoods[$goodId]['SuborderQuantity'])) {
				continue;
			}
			
			$offer = new Offer();
			if (isset($goodsInfo[$goodId])) {
				if (isset($availGoods[$goodId])) {
					$softCategoryId = $availGoods[$goodId];
				} elseif (isset($suborderGoods->categories[$goodId])) {
					$softCategoryId = $suborderGoods->categories[$goodId];
				} else {
					$this->log->warn('Loading category id for good: ' . $goodId);
					continue;
				}
				$offer->setId((int)$goodsInfo[$goodId]['GoodID']);
				$offer->setVolume((float)$goodsInfo[$goodId]['Volume']);
				$offer->setWeight((float)$goodsInfo[$goodId]['Weight']);
				
				// url
				$url = 'https://www.' . $mainDomain . '/' . $menu[$softCategoryId]['uid'] . '/' . $goodId . '_' . trim($goodsInfo[$goodId]['GoodEngName']) . '/';
				$url .= '?utm_source=ya_market&amp;utm_medium=cpc&amp;utm_term=' . $goodId . '&amp;utm_campaign=msk';
				$offer->setUrl($url);
				
				$offer->setVendorCode(htmlspecialchars(trim($goodsInfo[$goodId]['VendorCode'])));
				$offer->setModel(htmlspecialchars(trim($goodsInfo[$goodId]['Model'])));
				if (isset($goodsPrices[$goodId])) {
					$price = (float)$goodsPrices[$goodId]['Price'];
					$offer->setPrice($price);
					
					$prevPrice = (float)$goodsPrices[$goodId]['PrevPrice'];
					if ($price < $prevPrice) {
						$minOldPrice = $price + intval($price * 0.01 * 5);
						$maxOldPrice = $price + intval($price * 0.01 * 95);
						if (($minOldPrice <= $prevPrice) && ($prevPrice <= $maxOldPrice)) {
							$offer->setOldPrice($prevPrice);
						}
					}
				} else {
					$this->log->warn('Loading price for good failed: ' . $goodId);
					continue;
				}
				$offer->setCurrencyId('RUR');
				$offer->setCategoryId($menu[$softCategoryId]['id']);
				if (isset($goodsImages[$goodId])) {
					$offer->setPicture($siteUrl . 'img/450x600/' . $goodsImages[$goodId]);
				}
				$offer->setAvailable($available);
				$offer->setAvailable(true);
				if (!$isRegionPrice && !is_null($localDeliveryDays)) {
					$offer->setLocalDeliveryDays($localDeliveryDays);
				}
				
				// store
				$offer->setStore(false);
				if (isset($availGoods[$goodId]) && !$onlyDelivery && !$isRegionPrice) {
					$availQuantity = $availGoods[$goodId]['AvailQuantity'];
					if (0 < $availQuantity) {
						$offer->setStore(true);
					}
				}
				
				if (isset($deliveryGoods[$goodId]) || $isRegionPrice) {
					$offer->setDelivery(true);
				} else {
					$offer->setDelivery(false);
				}
				// для мск pickup делаем true
				if ((isset($availGoods[$goodId]) || isset($suborderGoods->avail[$goodId])) && !$isRegionPrice /*&& !$onlyDelivery*/) {
					$offer->setPickup(true);
				} else {
					$offer->setPickup(false);
				}
				if (isset($goodsDeliveryPrice[$goodId]) && !$isRegionPrice) {
					$offer->setLocalDeliveryCost((float)$goodsDeliveryPrice[$goodId]);
				} elseif (!$isRegionPrice) {
					$offer->setLocalDeliveryCost(990.00);
				}
				if (isset($goodsInfo[$goodId])) {
					$offer->setName(htmlspecialchars($goodsInfo[$goodId]['GoodName']));
					$offer->setDescription(strip_tags(htmlspecialchars($goodsInfo[$goodId]['GoodDescription'])));
					$offer->setManufacturerWarranty((bool)$goodsInfo[$goodId]['BrandWarranty']);
				} else {
					continue;
				}
				if (isset($vendors[$goodsInfo[$goodId]['BrandID']])) {
					$offer->setVendor(htmlspecialchars($vendors[$goodsInfo[$goodId]['BrandID']]['BrandName']));
				}
				
				$offer->setSalesNotes('выставочный зал 3000 м2');
				
				// properties
				$sql = "SELECT
                            p.PropertyName AS Name,
                            pu.UnitName AS Unit,
                            CASE
                                    WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueText <> '' THEN gpv.ValueText
                                    WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueString <> '' THEN gpv.ValueString
                                    WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueNumeric IS NOT NULL AND gpv.ValueNumeric > 0 THEN gpv.ValueNumeric
                                    WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueBoolean = 1 THEN 'Да'
                                    #WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueBoolean = 0 THEN 'Нет'
                                    WHEN gpv.PropertyValueID > 1 THEN GROUP_CONCAT(pv.PropertyValue SEPARATOR ',\n')
                            END AS Value
                    FROM
                            #goods_to_propvalues:active# gpv
                            
                    INNER JOIN
                            #all_goods:active# ag
                            ON
                                ag.GoodID = " . $goodId . "

                    INNER JOIN
                            #props:active# p
                            ON
                                p.PropertyID = gpv.PropertyID
                                AND p.KindID IN (1,2)
                                AND p.IsHidden = 0

                    LEFT OUTER JOIN
                            #prop_values:active# pv
                            ON
                                pv.PropertyValueID = gpv.PropertyValueID

                    LEFT JOIN
                            #prop_units:active# pu
                            ON
                                pu.PropertyUnitID = p.PropertyUnitID

                    WHERE
                            gpv.GoodID = " . $goodId . "
                    GROUP BY
                            gpv.PropertyID";
				$rows = $queryGateway->query($sql);
				foreach ($rows as $row) {
					$offer->addParam($row['Name'], $row['Value'], $row['Unit']);
				}
				// END properties
			}
			$checkResult = $offer->check($onlyDelivery, $isRegionPrice, true);
			if ($checkResult === true) {
				$ymlCatalog->shop()->offers()->add($offer);
			} else {
				// $this->log->warn('Loading info for good: ' . $goodId . ' failed, message "' . $checkResult . '"');
				continue;
			}
		}
		
		$path = 'public/yml/';
		if (false && !$isRegionPrice && $cityId === \SoloDelivery\Service\DeliveryService::MOSCOW_ID && $ymlCatalog->shop()->offers()->count() < 10000) {
			$fileName = 'bad-price-' . uniqid() . '.xml';
			$this->notify()->send('Количество товаров в московском прайсе менее 10000', 'Прайс сохранен ' . $fileName);
		} else {
			if (2 == $cityId) {
				$fileName = 'yml_msk.xml';
			} else {
				$fileName = ($isRegionPrice ? 0 : $cityId) . '.xml';
			}
		}
		$ymlCatalog->saveYml($path, $fileName);
	}

	/**
	 *
	 * @param array $officeIds        	
	 * @param array $goodsInfo        	
	 * @param string $siteUrl        	
	 * @param integer $cityId        	
	 * @param array $citiesSubdomains        	
	 * @param string $mainDomain        	
	 * @param array $holidays        	
	 * @param boolean $isRegionPrice        	
	 * @param \DateTime $regionDeliveryShippingDate        	
	 * @return type
	 */
	protected function buildAvailXml($officeIds, &$goodsInfo, $siteUrl, $cityId, $citiesSubdomains, $mainDomain, $holidays, $isRegionPrice = false, $format = 'yml') {
		$queryGateway = new QueryGateway();
		
		if (array_key_exists($cityId, $citiesSubdomains)) {
			$subdomain = $citiesSubdomains[$cityId];
		} else {
			$this->log->err('Setting "Subdomain" for city ' . $cityId . ' is empty');
			return;
		}
		$onlyDelivery = in_array($cityId, $this->onlyDeliveryCitiesIds);
		$this->log->info('City id = ' . $cityId);
		$ymlCatalog = new Catalog();
		$this->setShopSettings($ymlCatalog->shop());
		$availGoods = $this->yandexYmlMapper->getAvailGoods($officeIds);
		$this->log->info('Avail goods = ' . count($availGoods));
		$deliveryGoods = $this->yandexYmlMapper->getDeliveryGoods($cityId);
		$this->log->info('Delivery goods = ' . count($deliveryGoods));
		$categoriesIds = $availGoods;
		$allCategories = $this->yandexYmlMapper->getCategories();
		$menu = $this->createMenu($categoriesIds, $allCategories);
		foreach ($menu as $category) {
			$ymlCatalog->shop()->categories()->add(new Category($category['id'], $category['name'], $category['parentId']));
		}
		
		if (!$isRegionPrice) {
			$ymlCatalog->shop()->deliveryOptions()->add(new DeliveryOption());
		}
		
		$goodIds = array_keys($availGoods);
		$infoGoodIds = array_diff($goodIds, array_keys($goodsInfo));
		$goodIdsChunks = array_chunk($infoGoodIds, 500);
		foreach ($goodIdsChunks as $goodIdsChunk) {
			$goodsInfo += $this->yandexYmlMapper->getGoodsInfo($goodIdsChunk);
		}
		$this->log->info('Goods info = ' . count($goodsInfo));
		
		$priceCategoryId = ($isRegionPrice ? \SoloCatalog\Service\Helper\PricesHelper::DEFAULT_REGION_PRICE_COLUMN_ID : \SoloCatalog\Service\Helper\PricesHelper::DEFAULT_PRICE_COLUMN_ID);
		$this->log->info('Price column: ' . $priceCategoryId);
		$goodsPrices = $this->yandexYmlMapper->getGoodsPrices($cityId, $goodIds, $priceCategoryId);
		$this->log->info('Goods prices = ' . count($goodsPrices));
		$vendors = $this->yandexYmlMapper->getVendorsByGoodIds($goodIds);
		$goodsDeliveryPrice = $this->yandexYmlMapper->getGoodsDeliveryPrice($cityId, $goodIds);
		$this->log->info('Goods delivery prices = ' . count($goodsDeliveryPrice));
		$goodsImages = $this->yandexYmlMapper->getGoodsImages($goodIds);
		$goodsReserveAvailDates = $this->yandexYmlMapper->getReserveAvailDates($cityId, $goodIds);
		$this->log->info('Goods reserve avail dates = ' . count($goodsReserveAvailDates));
		
		foreach ($goodIds as $goodId) {
			// если нет даты доставки, то товар нельзя доставить через ТК
			if ($isRegionPrice && !isset($goodsReserveAvailDates[$goodId])) {
				continue;
			}
			// определяем available
			// true, если в наличии и дата доставки в пределах двух дней
			// true, если под заказ и дата доставки в пределах двух дней
			// иначе false
			if (isset($goodsReserveAvailDates[$goodId])) {
				$pickupDate = new \DateTime($goodsReserveAvailDates[$goodId]['PickupDate']);
				$deliveryDate = \SoloCatalog\Service\Helper\DateHelper::getMinDate(
					[
						new \DateTime($goodsReserveAvailDates[$goodId]['AvailDeliveryDate']),
						new \DateTime($goodsReserveAvailDates[$goodId]['SuborderDeliveryDate']) 
					]);
				$nearestDeliveryShippingDate = $this->getNearestDeliveryShippingDate($deliveryDate);
			} else {
				$pickupDate = null;
				$deliveryDate = null;
				$nearestDeliveryShippingDate = null;
			}
			
			if ($isRegionPrice) {
				$available = (!is_null($nearestDeliveryShippingDate) && \SoloCatalog\Service\Helper\DateHelper::isFutureDate($nearestDeliveryShippingDate) && \SoloCatalog\Service\Helper\DateHelper::isInWorkDaysIntervalDate(
					$nearestDeliveryShippingDate, 
					2, 
					$holidays));
			} else {
				if (!is_null($pickupDate) && \SoloCatalog\Service\Helper\DateHelper::isFutureDate($pickupDate) && \SoloCatalog\Service\Helper\DateHelper::isInWorkDaysIntervalDate($pickupDate, 2, [])) {
					$available = true;
				} else {
					$available = false;
				}
				if (!is_null($deliveryDate) && \SoloCatalog\Service\Helper\DateHelper::isFutureDate($deliveryDate)) {
					$localDeliveryDays = \SoloCatalog\Service\Helper\DateHelper::getWorkDaysInterval($deliveryDate, $holidays);
				} else {
					$localDeliveryDays = null;
				}
			}
			
			// disable suborder
			if (isset($availGoods[$goodId]) && (0 == $availGoods[$goodId]['AvailQauntity']) && (0 < $availGoods[$goodId]['SuborderQuantity'])) {
				continue;
			}
			
			$offer = new Offer();
			if (isset($goodsInfo[$goodId])) {
				$isPackage = (1 == $goodsInfo[$goodId]['IsPackage']);
				$viewMode = $goodsInfo[$goodId]['ViewMode'];
				
				if (false && $isPackage && (1 != $viewMode)) {
					continue;
				}
				
				if (isset($availGoods[$goodId])) {
					$softCategoryId = $availGoods[$goodId];
				} elseif (isset($suborderGoods->categories[$goodId])) {
					$softCategoryId = $suborderGoods->categories[$goodId];
				} else {
					$this->log->warn('Loading category id for good: ' . $goodId);
					continue;
				}
				$offer->setId((int)$goodsInfo[$goodId]['GoodID']);
				$offer->setVolume((float)$goodsInfo[$goodId]['Volume']);
				$offer->setWeight((float)$goodsInfo[$goodId]['Weight']);
				
				// url
				$url = 'https://www.' . $mainDomain . '/' . $menu[$softCategoryId]['uid'] . '/' . $goodId . '_' . trim($goodsInfo[$goodId]['GoodEngName']) . '/';
				$url .= '?utm_source=ya_market&amp;utm_medium=cpc&amp;utm_term=' . $goodId . '&amp;utm_campaign=msk';
				$offer->setUrl($url);
				
				$offer->setVendorCode(htmlspecialchars(trim($goodsInfo[$goodId]['VendorCode'])));
				$offer->setModel(htmlspecialchars(trim($goodsInfo[$goodId]['Model'])));
				if (isset($goodsPrices[$goodId])) {
					$price = (float)$goodsPrices[$goodId]['Price'];
					if ('yml' == $format) {
						$offer->setPrice($price);
					} elseif ('google' == $format) {
						$offer->setPrice(sprintf('%d.00 RUB', $price));
					}
					
					$prevPrice = (float)$goodsPrices[$goodId]['PrevPrice'];
					if ($price < $prevPrice) {
						$minOldPrice = $price + intval($price * 0.01 * 5);
						$maxOldPrice = $price + intval($price * 0.01 * 95);
						if (($minOldPrice <= $prevPrice) && ($prevPrice <= $maxOldPrice)) {
							$offer->setOldPrice($prevPrice);
						}
					}
				} else {
					$this->log->warn('Loading price for good failed: ' . $goodId);
					continue;
				}
				$offer->setCurrencyId('RUR');
				$offer->setCategoryId($menu[$softCategoryId]['id']);
				if (isset($goodsImages[$goodId])) {
					$offer->setPicture($siteUrl . 'img/450x600/' . $goodsImages[$goodId]);
				}
				$offer->setAvailable($available);
				$offer->setAvailable(true);
				if (!$isRegionPrice && !is_null($localDeliveryDays)) {
					$offer->setLocalDeliveryDays($localDeliveryDays);
				}
				
				// store
				$offer->setStore(false);
				if (isset($availGoods[$goodId]) && !$onlyDelivery && !$isRegionPrice) {
					$availQuantity = $availGoods[$goodId]['AvailQuantity'];
					if (0 < $availQuantity) {
						$offer->setStore(true);
					}
				}
				
				if (isset($deliveryGoods[$goodId]) || $isRegionPrice) {
					$offer->setDelivery(true);
				} else {
					$offer->setDelivery(false);
				}
				// для мск pickup делаем true
				if ((isset($availGoods[$goodId]) || isset($suborderGoods->avail[$goodId])) && !$isRegionPrice /*&& !$onlyDelivery*/) {
					$offer->setPickup(true);
				} else {
					$offer->setPickup(false);
				}
				if (isset($goodsDeliveryPrice[$goodId]) && !$isRegionPrice) {
					$offer->setLocalDeliveryCost((float)$goodsDeliveryPrice[$goodId]);
				} elseif (!$isRegionPrice) {
					$offer->setLocalDeliveryCost(990.00);
				}
				if (isset($goodsInfo[$goodId])) {
					$offer->setName(htmlspecialchars($goodsInfo[$goodId]['GoodName']));
					$offer->setDescription(strip_tags(htmlspecialchars($goodsInfo[$goodId]['GoodDescription'])));
					$offer->setManufacturerWarranty((bool)$goodsInfo[$goodId]['BrandWarranty']);
				} else {
					continue;
				}
				if (isset($vendors[$goodsInfo[$goodId]['BrandID']])) {
					$offer->setVendor(htmlspecialchars($vendors[$goodsInfo[$goodId]['BrandID']]['BrandName']));
				}
				
				$offer->setSalesNotes('выставочный зал 3000 м2');
				
				// properties
				$sql = "SELECT
                            p.PropertyName AS Name,
                            pu.UnitName AS Unit,
                            CASE
                                    WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueText <> '' THEN gpv.ValueText
                                    WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueString <> '' THEN gpv.ValueString
                                    WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueNumeric IS NOT NULL AND gpv.ValueNumeric > 0 THEN gpv.ValueNumeric
                                    WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueBoolean = 1 THEN 'Да'
                                    #WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueBoolean = 0 THEN 'Нет'
                                    WHEN gpv.PropertyValueID > 1 THEN GROUP_CONCAT(pv.PropertyValue SEPARATOR ',\n')
                            END AS Value
                    FROM
                            #goods_to_propvalues:active# gpv
    
                    INNER JOIN
                            #all_goods:active# ag
                            ON
                                ag.GoodID = " . $goodId . "
    
                    INNER JOIN
                            #props:active# p
                            ON
                                p.PropertyID = gpv.PropertyID
                                AND p.KindID IN (1,2)
                                AND p.IsHidden = 0
    
                    LEFT OUTER JOIN
                            #prop_values:active# pv
                            ON
                                pv.PropertyValueID = gpv.PropertyValueID
    
                    LEFT JOIN
                            #prop_units:active# pu
                            ON
                                pu.PropertyUnitID = p.PropertyUnitID
    
                    WHERE
                            gpv.GoodID = " . $goodId . "
                    GROUP BY
                            gpv.PropertyID";
				$rows = $queryGateway->query($sql);
				foreach ($rows as $row) {
					$offer->addParam($row['Name'], $row['Value'], $row['Unit']);
				}
				// END properties
			}
			$checkResult = $offer->check($onlyDelivery, $isRegionPrice, true);
			if ($checkResult === true) {
				$ymlCatalog->shop()->offers()->add($offer);
			} else {
				// $this->log->warn('Loading info for good: ' . $goodId . ' failed, message "' . $checkResult . '"');
				continue;
			}
		}
		
		$path = 'public/yml/';
		if (false && !$isRegionPrice && $cityId === \SoloDelivery\Service\DeliveryService::MOSCOW_ID && $ymlCatalog->shop()->offers()->count() < 10000) {
			$fileName = 'bad-price-' . uniqid() . '.xml';
			$this->notify()->send('Количество товаров в московском прайсе менее 10000', 'Прайс сохранен ' . $fileName);
		} else {
			if (2 == $cityId) {
				$fileName = sprintf('%s_msk_avail.xml', $format);
			} else {
				$fileName = ($isRegionPrice ? 0 : $cityId) . '_avail.xml';
			}
		}
		$ymlCatalog->saveYml($path, $fileName);
	}

	/**
	 *
	 * @param \Solo\Yandex\Yml\Shop $shop        	
	 */
	protected function setShopSettings(Shop &$shop) {
		$settings = $this->getYmlSettings();
		if (array_key_exists('shopName', $settings)) $shop->setName($settings['shopName']);
		if (array_key_exists('company', $settings)) $shop->setCompany($settings['company']);
		if (array_key_exists('platform', $settings)) $shop->setPlatform($settings['platform']);
		if (array_key_exists('version', $settings)) $shop->setVersion($settings['version']);
		if (array_key_exists('agency', $settings)) $shop->setAgency($settings['agency']);
		if (array_key_exists('email', $settings)) $shop->setEmail($settings['email']);
		if (array_key_exists('siteUrl', $settings)) $shop->setUrl($settings['siteUrl']);
		if (array_key_exists('currencies', $settings)) {
			foreach ($settings['currencies'] as $currencySetting) {
				if (array_key_exists('id', $currencySetting) && array_key_exists('rate', $currencySetting)) $shop->currencies()->add(new Currency($currencySetting['id'], $currencySetting['rate']));
			}
		}
	}

	/**
	 *
	 * @param array $categoriesIds        	
	 * @param array $allCategories        	
	 * @return array
	 */
	protected function createMenu($categoriesIds, $allCategories) {
		$menu = [];
		foreach ($categoriesIds as $categoryId) {
			if (!array_key_exists($categoryId, $allCategories)) {
				continue;
			}
			$currentCategory = $allCategories[$categoryId];
			$parents = \Solo\Stdlib\ArrayHelper::findAll(
				$allCategories, 
				function ($value) use($currentCategory) {
					return (($value['cLeft'] < $currentCategory['cLeft']) && ($value['cRight'] > $currentCategory['cRight']) && ($value['cLevel'] < $currentCategory['cLevel']));
				});
			usort(
				$parents, 
				function ($a, $b) {
					if ($a['cLevel'] > $b['cLevel']) return 1;
					if ($a['cLevel'] < $b['cLevel']) return -1;
					return 0;
				});
			$categoriesChain = $parents;
			$categoriesChain[] = $currentCategory;
			
			foreach ($categoriesChain as $category) {
				if (!array_key_exists($category['SoftCategoryID'], $menu)) {
					$menu[$category['SoftCategoryID']] = [
						'id' => (int)$category['SoftCategoryID'],
						'parentId' => (int)$category['ParentID'],
						'name' => $category['CategoryName'],
						'uid' => $category['CategoryUID'] 
					];
				}
			}
		}
		return $menu;
	}

	/**
	 *
	 * @param array $providerSuborderGoods        	
	 * @param array $mainWarehouseSuborderGoods        	
	 * @return \stdClass
	 */
	protected function mergeProviderAndMainWarehouseSuborder($providerSuborderGoods, $mainWarehouseSuborderGoods) {
		$result = new \stdClass();
		$result->avail = [];
		$result->categories = [];
		$result->categories = $providerSuborderGoods->categories + $mainWarehouseSuborderGoods->categories;
		$result->avail = $providerSuborderGoods->avail;
		foreach ($mainWarehouseSuborderGoods->avail as $goodId => $avail) {
			if ((array_key_exists($goodId, $result->avail) && !$result->avail[$goodId]) || !array_key_exists($goodId, $result->avail)) {
				$result->avail[$goodId] = $avail;
			}
		}
		return $result;
	}

	/**
	 *
	 * @param \DateTime $suborderDate        	
	 * @return \DateTime
	 */
	public function getNearestDeliveryShippingDate(\DateTime $suborderDate) {
		foreach ($this->shipppingPossibleDates as $shipppingPossibleDate) {
			if (\SoloCatalog\Service\Helper\DateHelper::isFirstDateMore($shipppingPossibleDate, $suborderDate)) {
				return clone $shipppingPossibleDate;
			}
		}
		return null;
	}

	/**
	 *
	 * @param array $officeIds        	
	 * @param array $goodsInfo        	
	 * @param string $siteUrl        	
	 * @param integer $cityId        	
	 * @param array $citiesSubdomains        	
	 * @param string $mainDomain        	
	 * @param array $holidays        	
	 * @param boolean $isRegionPrice        	
	 * @param \DateTime $regionDeliveryShippingDate        	
	 * @return type
	 */
	protected function buildGoogleXml($officeIds, &$goodsInfo, $siteUrl, $cityId, $citiesSubdomains, $mainDomain, $holidays, $isRegionPrice = false, $format = 'yml') {
		$queryGateway = new QueryGateway();
		
		if (array_key_exists($cityId, $citiesSubdomains)) {
			$subdomain = $citiesSubdomains[$cityId];
		} else {
			$this->log->err('Setting "Subdomain" for city ' . $cityId . ' is empty');
			return;
		}
		$onlyDelivery = in_array($cityId, $this->onlyDeliveryCitiesIds);
		$this->log->info('City id = ' . $cityId);
		
		$feed = new Feed();
		$feed->setTitle('santehbaza.ru');
		$feed->setLink('https://www.santehbaza.ru');
		$feed->setDescription('Оптово-розничная база сантехники Аквасант — вся сантехника в одном месте');
		$feed->setDescription('santehbaza.ru');
		
		$availGoods = $this->yandexYmlMapper->getAvailGoods($officeIds);
		$this->log->info('Avail goods = ' . count($availGoods));
		$providerSuborderGoods = $this->yandexYmlMapper->getSuborderGoods($officeIds);
		$this->log->info('Provider suborder goods = ' . count($providerSuborderGoods->avail));
		$mainWarehouseSuborderGoods = $this->yandexYmlMapper->getMainWarehouseSuborderGoods($officeIds);
		$this->log->info('Main warehouse suborder goods = ' . count($mainWarehouseSuborderGoods->avail));
		$suborderGoods = $this->mergeProviderAndMainWarehouseSuborder($providerSuborderGoods, $mainWarehouseSuborderGoods);
		$this->log->info('Suborder goods = ' . count($suborderGoods->avail));
		$deliveryGoods = $this->yandexYmlMapper->getDeliveryGoods($cityId);
		$this->log->info('Delivery goods = ' . count($deliveryGoods));
		$categoriesIds = array_unique(array_merge($availGoods, $suborderGoods->categories));
		$allCategories = $this->yandexYmlMapper->getCategories();
		$menu = $this->createMenu($categoriesIds, $allCategories);
		
		$goodIds = array_unique(array_merge(array_keys($availGoods), array_keys($suborderGoods->avail)));
		$infoGoodIds = array_diff($goodIds, array_keys($goodsInfo));
		$goodIdsChunks = array_chunk($infoGoodIds, 500);
		foreach ($goodIdsChunks as $goodIdsChunk) {
			$goodsInfo += $this->yandexYmlMapper->getGoodsInfo($goodIdsChunk);
		}
		$this->log->info('Goods info = ' . count($goodsInfo));
		
		$priceCategoryId = ($isRegionPrice ? \SoloCatalog\Service\Helper\PricesHelper::DEFAULT_REGION_PRICE_COLUMN_ID : \SoloCatalog\Service\Helper\PricesHelper::DEFAULT_PRICE_COLUMN_ID);
		$this->log->info('Price column: ' . $priceCategoryId);
		$goodsPrices = $this->yandexYmlMapper->getGoodsPrices($cityId, $goodIds, $priceCategoryId);
		$this->log->info('Goods prices = ' . count($goodsPrices));
		$vendors = $this->yandexYmlMapper->getVendorsByGoodIds($goodIds);
		$goodsDeliveryPrice = $this->yandexYmlMapper->getGoodsDeliveryPrice($cityId, $goodIds);
		$this->log->info('Goods delivery prices = ' . count($goodsDeliveryPrice));
		$goodsImages = $this->yandexYmlMapper->getGoodsImages($goodIds);
		$goodsReserveAvailDates = $this->yandexYmlMapper->getReserveAvailDates($cityId, $goodIds);
		$this->log->info('Goods reserve avail dates = ' . count($goodsReserveAvailDates));
		
		foreach ($goodIds as $goodId) {
			// если нет даты доставки, то товар нельзя доставить через ТК
			if ($isRegionPrice && !isset($goodsReserveAvailDates[$goodId])) {
				continue;
			}
			// определяем available
			// true, если в наличии и дата доставки в пределах двух дней
			// true, если под заказ и дата доставки в пределах двух дней
			// иначе false
			if (isset($goodsReserveAvailDates[$goodId])) {
				$pickupDate = new \DateTime($goodsReserveAvailDates[$goodId]['PickupDate']);
				$deliveryDate = \SoloCatalog\Service\Helper\DateHelper::getMinDate(
					[
						new \DateTime($goodsReserveAvailDates[$goodId]['AvailDeliveryDate']),
						new \DateTime($goodsReserveAvailDates[$goodId]['SuborderDeliveryDate']) 
					]);
				$nearestDeliveryShippingDate = $this->getNearestDeliveryShippingDate($deliveryDate);
			} else {
				$pickupDate = null;
				$deliveryDate = null;
				$nearestDeliveryShippingDate = null;
			}
			
			if ($isRegionPrice) {
				$available = (!is_null($nearestDeliveryShippingDate) && \SoloCatalog\Service\Helper\DateHelper::isFutureDate($nearestDeliveryShippingDate) && \SoloCatalog\Service\Helper\DateHelper::isInWorkDaysIntervalDate(
					$nearestDeliveryShippingDate, 
					2, 
					$holidays));
			} else {
				if (!is_null($pickupDate) && \SoloCatalog\Service\Helper\DateHelper::isFutureDate($pickupDate) && \SoloCatalog\Service\Helper\DateHelper::isInWorkDaysIntervalDate($pickupDate, 2, [])) {
					$available = true;
				} else {
					$available = false;
				}
				if (!is_null($deliveryDate) && \SoloCatalog\Service\Helper\DateHelper::isFutureDate($deliveryDate)) {
					$localDeliveryDays = \SoloCatalog\Service\Helper\DateHelper::getWorkDaysInterval($deliveryDate, $holidays);
				} else {
					$localDeliveryDays = null;
				}
			}
			
			// disable suborder
			if (isset($availGoods[$goodId]) && (0 == $availGoods[$goodId]['AvailQauntity']) && (0 < $availGoods[$goodId]['SuborderQuantity'])) {
				continue;
			}
			
			$feedItem = new FeedItem();
			
			if (isset($goodsInfo[$goodId])) {
				if (isset($availGoods[$goodId])) {
					$softCategoryId = $availGoods[$goodId];
				} elseif (isset($suborderGoods->categories[$goodId])) {
					$softCategoryId = $suborderGoods->categories[$goodId];
				} else {
					$this->log->warn('Loading category id for good: ' . $goodId);
					continue;
				}
				
				if (150 < mb_strlen($goodsInfo[$goodId]['GoodName'])) {
					$goodsInfo[$goodId]['GoodName'] = substr($goodsInfo[$goodId]['GoodName'], 0, 150);
				}
				
				$feedItem->setId((int)$goodsInfo[$goodId]['GoodID']);
				$feedItem->setAvailability($feedItem::AVAILABILITY_IN_STOCK);
				$feedItem->setCategory($goodsInfo[$goodId]['GoogleCategoryName']);
				$feedItem->setProductType($menu[$softCategoryId]['name']);
				
				// url
				$url = 'https://www.' . $mainDomain . '/' . $menu[$softCategoryId]['uid'] . '/' . $goodId . '_' . trim($goodsInfo[$goodId]['GoodEngName']) . '/';
				$feedItem->setLink($url);
				
				if (isset($goodsPrices[$goodId])) {
					$price = (float)$goodsPrices[$goodId]['Price'];
					$feedItem->setPrice($price);
					
					$prevPrice = (float)$goodsPrices[$goodId]['PrevPrice'];
					if ($price < $prevPrice) {
						$minOldPrice = $price + intval($price * 0.01 * 5);
						$maxOldPrice = $price + intval($price * 0.01 * 95);
						if (($minOldPrice <= $prevPrice) && ($prevPrice <= $maxOldPrice)) {
							$feedItem->setSalePrice($prevPrice);
						}
					}
				} else {
					$this->log->warn('Loading price for good failed: ' . $goodId);
					continue;
				}
				if (isset($goodsImages[$goodId])) {
					$feedItem->setImageLink($siteUrl . 'img/450x600/' . $goodsImages[$goodId]);
				} else {
					continue;
				}
				
				if (isset($goodsInfo[$goodId])) {
					$feedItem->setTitle(htmlspecialchars($goodsInfo[$goodId]['GoodName']));
					$feedItem->setDescription(strip_tags(htmlspecialchars($goodsInfo[$goodId]['GoodDescription'])));
					if (!$feedItem->getDescription()) {
						$feedItem->setDescription($feedItem->getTitle());
					}
				} else {
					continue;
				}
				if (isset($vendors[$goodsInfo[$goodId]['BrandID']])) {
					$feedItem->setBrand(htmlspecialchars($vendors[$goodsInfo[$goodId]['BrandID']]['BrandName']));
				}
			}

			if (empty($feedItem->getTitle())) {
				continue;
			}
			
			$feed->addItem($feedItem);
		}
		
		$path = 'public/yml/';
			if (2 == $cityId) {
				$fileName = 'google_msk.xml';
			} else {
				$fileName = ($isRegionPrice ? 0 : $cityId) . '.xml';
			}
			
			$writer = new FeedWriter($feed);
			$writer->writeToFile($path.'/'.$fileName);
	}

}
