<?php

namespace SoloReplication\Service;

class DataMapper {
	
	/**
	 * 
	 * @var array
	 */
	private $mapHash = [];
	
	/**
	 * 
	 * @param array $mappings
	 */
	public function __construct(array $mappings = []) {
		if (0 < sizeof($mappings)) {
			$this->setMappings($mappings);
		}
	}
	
	/**
	 * 
	 * @param string $resultField
	 * @param string $sourceField
	 * @param callable $converter
	 * @throws \InvalidArgumentException
	 * @return \SoloReplication\Service\DataMapper
	 */
	public function setMapping($resultField, $sourceField, callable $converter = null) {
		$resultField = trim($resultField);
		$sourceField = trim($sourceField);
		
		if (empty($resultField)) {
			throw new \InvalidArgumentException('Not specified result field');
		}
		if (empty($sourceField) && (null === $converter)) {
			throw new \InvalidArgumentException('Must set source field or converter function');
		}
		
		$this->mapHash[$resultField] = [
			'sourceField' => $sourceField,
			'resultField' => $resultField,
			'converter' => $converter
		];
		return $this;
	}
	
	/**
	 * 
	 * @param array $mappings
	 * @return \SoloReplication\Service\DataMapper
	 */
	public function setMappings(array $mappings) {
		foreach ($mappings as $resultField => $sourceField) {
			$this->setMapping($resultField, $sourceField);
		}
		return $this;
	}
	
	/**
	 * 
	 * @param array | \ArrayAccess $row
	 * @throws \RuntimeException
	 * @return array
	 */
	public function convert($row) {
		if (!is_array($row) && !$row instanceof \ArrayAccess) {
			$rowType = (is_object($row) ? get_class($row) : gettype($row));
			throw new \RuntimeException(sprintf('Row for convertation not array or not implements ArrayAccess. Type of row is %s', $rowType));
		}
		$out = [];
		foreach ($this->mapHash as $map) {
			$out[$map['resultField']] = $this->convertField($row, $map);
		}
		return $out;
	}
	
	/**
	 * 
	 * @param array | \ArrayAccess $row
	 * @param array $map
	 * @return string
	 */
	private function convertField($row, $map) {
		$sourceField = $map['sourceField'];
		$converter = $map['converter'];
		
		$result = '';
		
		if (!empty($sourceField)) {
			if (false !== stripos($sourceField, ':')) {
				$sourceParts = explode(':', $sourceField);
				$expression = trim($sourceParts[0]);
				$fieldName = trim($sourceParts[1]);
				$result = sprintf($expression, $row[$fieldName]);
			} else {
				$result = $row[$sourceField];
			}
		}
		
		if (null !== $converter) {
			$result = $converter($row[$sourceField], $row);
		}
		
		return $result;
	}

}

?>