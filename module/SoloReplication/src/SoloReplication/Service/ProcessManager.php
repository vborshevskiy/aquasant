<?php

namespace SoloReplication\Service;

use SoloSettings\Service\TriggersService;

class ProcessManager {

    /**
     *
     * @var LockManager
     */
    private $lockManager = null;

    /**
     *
     * @var TriggersService
     */
    private $triggers = null;

    /**
     *
     * @param LockManager $lockManager        	
     */
    public function setLockManager(LockManager $lockManager) {
        $this->lockManager = $lockManager;
    }

    /**
     *
     * @throws \RuntimeException
     * @return \SoloReplication\Service\LockManager
     */
    public function getLockManager() {
        if (null === $this->lockManager) {
            throw new \RuntimeException(sprintf('Not set lock manager in %s', __CLASS__));
        }
        return $this->lockManager;
    }

    /**
     *
     * @param TriggersService $triggers        	
     */
    public function setTriggers(TriggersService $triggers) {
        $this->triggers = $triggers;
    }

    /**
     *
     * @throws \RuntimeException
     * @return \SoloSettings\Service\TriggersService
     */
    public function getTriggers() {
        if (null === $this->triggers) {
            throw new \RuntimeException(sprintf('Not set triggers in %s', __CLASS__));
        }
        return $this->triggers;
    }

    public function run(TaskInterface $task) {
        if ($this->getLockManager()->canRun($task)) {
            $this->getLockManager()->lock($task);

            $this->startTask($task);
            $task->run();
            $this->stopTask($task);

            $this->getLockManager()->unlock($task);
        }
    }

    private function startTask(TaskInterface $task) {
        $args = $task->getEventManager()->prepareArgs([]);
        $task->getEventManager()->trigger('start.pre', $task, $args);
        $task->getEventManager()->trigger('start.post', $task, $args);
    }

    private function stopTask(TaskInterface $task) {
        $args = $task->getEventManager()->prepareArgs([]);
        $task->getEventManager()->trigger('stop.pre', $task, $args);
        $task->getEventManager()->trigger('stop.post', $task, $args);
    }

    private function swapTables(TaskInterface $task) {
        if ($task instanceof AbstractReplicationTask) {
            $swapTables = $task->getSwapTables();
            if (0 < sizeof($swapTables)) {
                $args = $task->events()->prepareArgs([
                    'swapTables' => $swapTables
                ]);
                $task->events()->trigger('swapTables.pre', $task, $args);

                $this->getTriggers()->swap($swapTables);

                $task->events()->trigger('swapTables.post', $task, $args);
            }
        }
    }

}

?>