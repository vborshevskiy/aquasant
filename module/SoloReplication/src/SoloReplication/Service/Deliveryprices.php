<?php

namespace SoloReplication\Service;

use SoloERP\WebService\Reader\UltimaListReader;

class Deliveryprices extends AbstractReplicator {

	protected $logFilename = 'delivery_prices.log';

	protected $lockFilename = 'delivery_prices.lock';

	protected $swapTables = [
		'kladr_costs' 
	];

	public function process() {
		$this->processDeliveryprices();
	}

	private function processDeliveryprices() {
		$this->clearPassiveTable('kladr_costs');
		
		// level 3
		$res = $this->queryGateway->query("SELECT a.code3, a.code2, a.code1, a.post_index
            FROM " . $this->triggers->active('kladr_level3') . " a
            WHERE a.code1 in (50,77)");
		
		$queryParts = [];
		while (false !== ($item = $res->current())) {
			
			$params = [
				"postIndex" => (int)$item['post_index'],
				"priceCategory" => 195,
				"goodsVolume" => '0.1',
				"goodsWeight" => '0.1',
				"deliveryTime" => 412006,
				"deliveryDate" => strtotime("+ 1 day"),
				"hasActionGoods" => false,
				"agentID" => 1381300,
				"goods" => array(
					190622 
				),
				"quantity" => array(
					1 
				) 
			];
			
			try {
				$rdr = new UltimaListReader($this->callMethod('GetDeliveryInfo', $params));
			} catch (\Exception $e) {
				/**
				 * это для ошибки, код у неё - 0, а текст очень длинный поэтому так пока
				 * Для индекса 143571 не найден сектор доставки
				 */
				continue;
			}
			
			if (!empty($rdr[0]['Price'])) {
				$light = $rdr[0]['Price'];
			} else {
				continue;
			}
			
			$params = [
				"postIndex" => (int)$item['post_index'],
				"priceCategory" => 195,
				"goodsVolume" => 50,
				"goodsWeight" => 3,
				"deliveryTime" => 412006,
				"deliveryDate" => strtotime("+ 1 day"),
				"hasActionGoods" => false,
				"agentID" => 1381300,
				"goods" => array(
					197046 
				),
				"quantity" => array(
					1 
				) 
			];
			
			try {
				$rdr = new UltimaListReader($this->callMethod('GetDeliveryInfo', $params));
			} catch (\Exception $e) {
				/**
				 * это для ошибки, код у неё - 0, а текст очень длинный поэтому так пока
				 * Для индекса 143571 не найден сектор доставки
				 */
				continue;
			}
			
			if (!empty($rdr[0]['Price'])) {
				$hard = $rdr[0]['Price'];
			} else {
				continue;
			}
			
			$queryParts[] = "(" . $this->queryGateway->quoteValue($item['code1']) . "," . $this->queryGateway->quoteValue($item['code2']) . "," . $this->queryGateway->quoteValue($item['code3']) . "," . $this->queryGateway->quoteValue(
				$light) . "," . $this->queryGateway->quoteValue($hard) . ")";
		}
		
		$this->queryGateway->query("INSERT " . $this->triggers->passive('kladr_costs') . " (code1, code2, code3, light, hard) VALUES " . implode(',', $queryParts));
		// /level3
		
		// level 4
		$res = $this->queryGateway->query(
			"SELECT a.code4, a.code3, a.code2, a.code1, a.post_index
            FROM " . $this->triggers->active('kladr_level4') . " a
            WHERE a.code1 in (50,77)");
		
		$queryParts = [];
		while (false !== ($item = $res->current())) {
			
			$params = [
				"postIndex" => (int)$item['post_index'],
				"priceCategory" => 195,
				"goodsVolume" => '0.1',
				"goodsWeight" => '0.1',
				"deliveryTime" => 412006,
				"deliveryDate" => strtotime("+ 1 day"),
				"hasActionGoods" => false,
				"agentID" => 1381300,
				"goods" => array(
					190622 
				),
				"quantity" => array(
					1 
				) 
			];
			
			try {
				$rdr = new UltimaListReader($this->callMethod('GetDeliveryInfo', $params));
			} catch (\Exception $e) {
				/**
				 * это для ошибки, код у неё - 0, а текст очень длинный поэтому так пока
				 * Для индекса 143571 не найден сектор доставки
				 */
				continue;
			}
			
			if (!empty($rdr[0]['Price'])) {
				$light = $rdr[0]['Price'];
			} else {
				continue;
			}
			
			$params = [
				"postIndex" => (int)$item['post_index'],
				"priceCategory" => 195,
				"goodsVolume" => 50,
				"goodsWeight" => 3,
				"deliveryTime" => 412006,
				"deliveryDate" => strtotime("+ 1 day"),
				"hasActionGoods" => false,
				"agentID" => 1381300,
				"goods" => array(
					197046 
				),
				"quantity" => array(
					1 
				) 
			];
			
			try {
				$rdr = new UltimaListReader($this->callMethod('GetDeliveryInfo', $params));
			} catch (\Exception $e) {
				/**
				 * это для ошибки, код у неё - 0, а текст очень длинный поэтому так пока
				 * Для индекса 143571 не найден сектор доставки
				 */
				continue;
			}
			
			if (!empty($rdr[0]['Price'])) {
				$hard = $rdr[0]['Price'];
			} else {
				continue;
			}
			
			$queryParts[] = "(" . $this->queryGateway->quoteValue($item['code1']) . "," . $this->queryGateway->quoteValue($item['code2']) . "," . $this->queryGateway->quoteValue($item['code3']) . "," . $this->queryGateway->quoteValue(
				$item['code4']) . "," . $this->queryGateway->quoteValue($light) . "," . $this->queryGateway->quoteValue($hard) . ")";
		}
		
		$this->queryGateway->query("INSERT " . $this->triggers->passive('kladr_costs') . " (code1, code2, code3, code4, light, hard) VALUES " . implode(',', $queryParts));
		// /level4
	} // function ends
}
