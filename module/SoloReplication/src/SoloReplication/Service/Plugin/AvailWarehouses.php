<?php

namespace SoloReplication\Service\Plugin;

use SoloReplication\Model\GenericModel;

class AvailWarehouses extends AbstractPlugin {

	public function getDependecyTables() {
		return [
			'avail_warehouses' 
		];
	}

	public function process(InputParameters $params = null) {
		if (null === $params) {
			$params = new InputParameters();
		}
		$params->assertParam('cityId');
		
		$availWarehouses = new GenericModel('avail_warehouses', [
			'GoodID',
			'WarehouseID' 
		], $this->triggers);
		$availWarehouses->clearPassive();
		
		$sql = "SELECT
					gr.GoodID,
					gr.WarehouseID,
					(gr.Remains - gr.Reserve) AS AvailQuantity,
					alg.CategoryID
				FROM
					" . $params->getTableOr('goods_remains', $this->getTriggers()->active('goods_remains')) . " gr
				INNER JOIN
					" . $params->getTableOr('all_goods', $this->getTriggers()->active('all_goods')) . " alg
					ON alg.GoodID = gr.GoodID
				WHERE
					gr.CityID = " . $params->cityId;
		$aw = [];
		$rows = $this->getQueryGateway()->query($sql);
		foreach ($rows as $row) {
			if (!array_key_exists($row->GoodID . '_' . $row->WarehouseID, $aw)) {
				foreach ($this->getWarehouses()->enumAll() as $warehouse) {
					$aw[$row->GoodID . '_' . $warehouse->getWarehouseID()] = array(
						'GoodID' => $row->GoodID,
						'WarehouseID' => $warehouse->getWarehouseID(),
						'AvailQuantity' => 0,
						'CategoryID' => $row->CategoryID 
					);
				}
			}
			$aw[$row->GoodID . '_' . $row->WarehouseID]['AvailQuantity'] = $row->AvailQuantity;
			$aw[$row->GoodID . '_' . $row->WarehouseID]['CategoryID'] = $row->CategoryID;
		}
		$awChunks = array_chunk($aw, 500);
		foreach ($awChunks as $chunk) {
			$availWarehouses->insertSetPassive($chunk);
		}
		
		return sizeof($aw);
	}

}

?>