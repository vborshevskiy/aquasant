<?php

namespace SoloReplication\Service\Plugin;

class InputParameters implements \ArrayAccess {

	/**
	 *
	 * @var array
	 */
	private $params = [];

	/**
	 *
	 * @var array
	 */
	private $tables = [];

	/**
	 *
	 * @param string $name        	
	 * @param mixed $value        	
	 * @return \SoloReplication\Service\Plugin\InputParameters
	 */
	public function setParam($name, $value) {
		$this->params[$name] = $value;
		return $this;
	}

	/**
	 *
	 * @param string $name        	
	 * @throws \InvalidArgumentException
	 * @return mixed
	 */
	public function getParam($name) {
		if (!$this->hasParam($name)) {
			throw new \InvalidArgumentException(sprintf('Parameter %s not exists', $name));
		}
		return $this->params[$name];
	}

	/**
	 *
	 * @param string $name        	
	 * @return boolean
	 */
	public function hasParam($name) {
		return array_key_exists($name, $this->params);
	}

	/**
	 *
	 * @param string $name        	
	 * @param string $value        	
	 * @return \SoloReplication\Service\Plugin\InputParameters
	 */
	public function setTable($name, $value) {
		$this->tables[$name] = $value;
		return $this;
	}

	/**
	 *
	 * @param string $name        	
	 * @throws \InvalidArgumentException
	 * @return string
	 */
	public function getTable($name) {
		if (!$this->hasTable($name)) {
			throw new \InvalidArgumentException(sprintf('Table %s not exists', $name));
		}
		return $this->tables[$name];
	}
	
	/**
	 * 
	 * @param string $name
	 * @param string $otherValue
	 * @return string
	 */
	public function getTableOr($name, $otherValue) {
		if (!$this->hasTable($name)) {
			return $otherValue;
		}
		return $this->tables[$name];
	}

	/**
	 *
	 * @param string $name        	
	 * @return boolean
	 */
	public function hasTable($name) {
		return array_key_exists($name, $this->tables);
	}

	/**
	 * 
	 * @param string $name
	 * @param boolean $throw
	 * @throws \RuntimeException
	 * @return boolean
	 */
	public function assertParam($name, $throw = true) {
		if (!$this->hasParam($name)) {
			if ($throw) {
				throw new \RuntimeException(sprintf('Parameter %s must be set', $name));
			} else {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ArrayAccess::offsetGet()
	 */
	public function offsetGet($offset) {
		return $this->getParam($offset);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ArrayAccess::offsetSet()
	 */
	public function offsetSet($offset, $value) {
		$this->setParam($offset, $value);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ArrayAccess::offsetExists()
	 */
	public function offsetExists($offset) {
		return $this->hasParam($offset);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ArrayAccess::offsetUnset()
	 */
	public function offsetUnset($offset) {
		if ($this->hasParam($offset)) {
			unset($this->params[$offset]);
			return true;
		}
		return false;
	}

}

?>