<?php

namespace SoloReplication\Service\Plugin;

interface PluginInterface {

	public function process(InputParameters $params = null);
	
}

?>