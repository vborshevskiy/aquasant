<?php

namespace SoloReplication\Service\Plugin;

class RebuildSearchGoodsIndex extends AbstractPlugin {

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \SoloReplication\Service\Plugin\PluginInterface::process()
	 */
	public function process(InputParameters $params = null) {
		if (null === $params) {
			$params = new InputParameters();
		}
		
		$output = [];
		$cmd = 'indexer justGoods_' . $this->triggers->active('all_goods') . '_' . $this->triggers->active('avail_goods') . ' --rotate';
		exec($cmd, $output);
		
		array_unshift($output, 'Command: ' . $cmd);
		
		return $output;
	}

}

?>