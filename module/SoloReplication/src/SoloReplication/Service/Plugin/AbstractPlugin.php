<?php

namespace SoloReplication\Service\Plugin;

use SoloReplication\Service\AbstractReplicator;

abstract class AbstractPlugin implements PluginInterface {

	/**
	 *
	 * @var AbstractReplicator
	 */
	private $replicator = null;

	/**
	 *
	 * @param AbstractReplicator $replicator        	
	 */
	public function setReplicator(AbstractReplicator $replicator) {
		$this->replicator = $replicator;
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return \SoloReplication\Service\AbstractReplicator
	 */
	public function getReplicator() {
		if (null === $this->replicator) {
			throw new \RuntimeException(sprintf('Replicator not set in %s', get_class($this)));
		}
		return $this->replicator;
	}
	
	/**
	 * 
	 * @return \Solo\Db\QueryGateway\QueryGateway
	 */
	public function getQueryGateway() {
		return $this->getReplicator()->getQueryGateway();
	}
	
	/**
	 * 
	 * @return \SoloSettings\Triggers
	 */
	public function getTriggers() {
		return $this->getReplicator()->getTriggers();
	}
	
	/**
	 * 
	 * @return \SoloSettings\Service\ConstantsService
	 */
	public function getConstants() {
		return $this->getReplicator()->getConstants();
	}
	
	/**
	 * 
	 * @return \SoloWarehouse\Service\WarehouseService
	 */
	public function getWarehouses() {
		return $this->getReplicator()->getWarehouses();
	}
	
	/**
	 * 
	 * @return array
	 */
	public function getDependecyTables() {
		return [];
	}

}

?>