<?php

namespace SoloReplication\Service\Plugin;

use SoloReplication\Service\Plugin\AbstractPlugin;
use SoloReplication\Model\GenericModel;
use SoloReplication\Service\Plugin\InputParameters;

class AvailGoods extends AbstractPlugin {

	public function getDependencyTables() {
		return [
			'avail_goods' 
		];
	}

	public function process(InputParameters $params = null) {
		if (null === $params) {
			$params = new InputParameters();
		}
		$params->assertParam('cityId');
		
		$availGoods = new GenericModel('avail_goods', [
			'GoodID' 
		], $this->getTriggers());
		
		$availGoods->clearPassive();
		
		$sql = "SELECT
					gr.GoodID,
					gr.WarehouseID,
					(gr.Remains - gr.Reserve) AS AvailQuantity,
					gr.ArrivalDate,
					alg.CategoryID AS HardCategoryID
				FROM
                                        #goods_remains# gr
				INNER JOIN
					#all_goods alg
					ON alg.GoodID = gr.GoodID
				WHERE
					gr.CityID = " . $params->getParam('cityId');
		$remains = [];
		$rows = $this->getQueryGateway()->query($sql);
		foreach ($rows as $row) {
			$warehouseInfo = $this->getWarehouses()->getById($row->WarehouseID);
			if (!array_key_exists($row->GoodID, $remains)) {
				$remains[$row->GoodID] = [
					'GoodID' => $row->GoodID,
					'HardCategoryID' => $row->HardCategoryID,
					'CityID' => $row->CityID,
					'AvailQuantity' => 0,
					'SuborderQuantity' => 0,
					'DeliveryQuantity' => 0,
					'ArrivalDate' => null 
				];
			}
			if (0 < $row->AvailQuantity) {
				$remains[$row->GoodID]['AvailQuantity'] += $row->AvailQuantity;
			}
			if (!$warehouseInfo->hasSuborder()) {
				$remains[$row->GoodID]['SuborderQuantity'] += $row->AvailQuantity;
			}
			if ($warehouseInfo->getCanDelivery()) {
				$remains[$row->GoodID]['DeliveryQuantity'] += $row->AvailQuantity;
			}
			if (!empty($row->ArrivalDate)) {
				$remains[$row->GoodID]['ArrivalDate'] = $row->ArrivalDate;
			}
		}
		foreach ($remains as $goodId => $remain) {
			if (0 == $remain['AvailQuantity']) {
				unset($remains[$goodId]);
			}
		}
		$remainsChunks = array_chunk($remains, 500);
		foreach ($remainsChunks as $chunk) {
			$availGoods->insertSetPassive($chunk);
		}
		
		return sizeof($remains);
	}

}

?>