<?php

namespace SoloReplication\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class TaskManagerFactory implements FactoryInterface {

    /**
     * (non-PHPdoc)
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $serviceLocator) {
        $config = $serviceLocator->get('Config');       
        if (!isset($config['tasks']) || !is_array($config['tasks'])) {
            throw new \RuntimeException('Not set configuration for tasks set');
        }
        $taskManager = new TaskManager();
        if ($taskManager instanceof ServiceLocatorAwareInterface) {
            $taskManager->setServiceLocator($serviceLocator);
        }
        foreach ($config['tasks'] as $name => $settings) {
            $className = (is_string($settings) ? $settings : $settings['className']);
            $taskManager->addTaskSettings($name, $className);
        }
        foreach ($config['solo-replication'] as $name => $settings) {
            $taskManager->addReplicationSettings($name, $settings);
        }
        $taskManager->addReplicationSettings('siteId', $config['site']['id']);
        $taskManager->addReplicationSettings('yml',$config['yml']);
        $taskManager->addReplicationSettings('sitemap',$config['sitemap']);
        return $taskManager;
    }

}

?>