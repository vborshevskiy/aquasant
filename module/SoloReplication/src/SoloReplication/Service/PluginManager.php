<?php

namespace SoloReplication\Service;

use Zend\ServiceManager\AbstractPluginManager;
use Zend\ServiceManager\ConfigInterface;
use SoloReplication\Service\Plugin\PluginInterface;

class PluginManager extends AbstractPluginManager {

	/**
	 * Default set of replicator plugins
	 *
	 * @var array
	 */
	protected $invokableClasses = [
		'availgoods' => 'SoloReplication\Service\Plugin\AvailGoods',
		'availwarehouses' => 'SoloReplication\Service\Plugin\AvailWarehouses',
		'rebuildsearchgoodsindex' => 'SoloReplication\Service\Plugin\RebuildSearchGoodsIndex'
	];

	/**
	 * Whether or not share by default; default true
	 *
	 * @var boolean
	 */
	protected $shareByDefault = true;

	/**
	 *
	 * @var AbstractReplicator
	 */
	private $replicator = null;

	/**
	 *
	 * @param ConfigInterface $config        	
	 */
	public function __construct(ConfigInterface $config = null) {
		parent::__construct($config);
		$this->addInitializer([
			$this,
			'injectReplicator' 
		]);
	}

	public function injectReplicator($plugin) {
		if (!is_object($plugin)) {
			return;
		}
		if (!($plugin instanceof PluginInterface)) {
			return;
		}
		$plugin->setReplicator($this->getReplicator());
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \Zend\ServiceManager\AbstractPluginManager::validatePlugin()
	 */
	public function validatePlugin($plugin) {
		if (!($plugin instanceof PluginInterface)) {
			throw new \RuntimeException(sprintf('Plugin of type %s is invalid; must implement %s\ReplicatorInterface', (is_object($plugin) ? get_class($plugin) : gettype($plugin)), __NAMESPACE__));
		}
	}

	/**
	 *
	 * @param AbstractReplicator $replicator        	
	 * @return \SoloReplication\Service\PluginManager
	 */
	public function setReplicator(AbstractReplicator $replicator) {
		$this->replicator = $replicator;
		return $this;
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return \SoloReplication\Service\AbstractReplicator
	 */
	public function getReplicator() {
		if (null === $this->replicator) {
			throw new \RuntimeException(sprintf('Replicator not set in %s', get_class($this)));
		}
		return $this->replicator;
	}

}

?>