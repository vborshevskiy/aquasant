<?php

namespace SoloReplication\Service;

use SoloWarehouse\Entity\WarehouseTypes;

class Shopsales extends AbstractReplicator {

	protected $logFilename = 'shop_sales.log';

	protected $lockFilename = 'shop_sales.lock';

	protected $swapTables = [
		'goods_sales' 
	];

	public function process() {
		$this->processSales();
	}

	private function processSales() {
		$this->clearPassiveTable('goods_sales');
		
		$types = [
			WarehouseTypes::MAIN,
			WarehouseTypes::OUTPOST 
		];
		$res = $this->queryGateway->query(
			"SELECT w.WarehouseID as shopId
			FROM " . $this->triggers->active('warehouses') . " w
			INNER JOIN warehouse_settings ws ON w.WarehouseID = ws.WarehouseID
			WHERE w.WarehouseTypeID IN (" . implode(', ', $types) . ")
			ORDER BY ws.WarehouseNumber ASC");
		
		$salesPeriond = $this->constants->get('replication_shopsales_days');
		if (!$salesPeriond || !preg_match("~^[0-9]+$~", $salesPeriond)) {
			$salesPeriond = 7;
		}
		
		$salesSet = [];
		while ($shopId = $res->current()->shopId) {
			
			$params = [
				'shopsID' => array(
					intval($shopId) 
				),
				'dateFrom' => date("Y-m-d", strtotime("- " . $salesPeriond . " day")),
				'dateTo' => date("Y-m-d") 
			];
			
			// call erp
			$resSales = $this->callMethod('GetShopSales', $params)->getResponse();
			
			// собираем массив продаж по магазину
			if (!empty($resSales[1][0][0]) && $resSales[1][0][1]) {
				$sales = array_combine($resSales[1][0][0], $resSales[1][0][1]);
			} else {
				continue;
			}
			
			// наполняем общий массив продаж
			foreach ($sales as $id => $quant) {
				
				if (isset($salesSet[$id])) {
					$salesSet[$id] += intval($quant);
				} else {
					$salesSet[$id] = intval($quant);
				}
			}
		}
		
		// делим на кусочки для ускорения загрузки в бд
		$chunks = array_chunk($salesSet, 500, true);
		
		foreach ($chunks as $chunk) {
			$queryParts = [];
			foreach ($chunk as $goodId => $quant) {
				$queryParts[] = "(" . $goodId . ", " . $quant . ")";
			}
			
			$sql = "INSERT " . $this->triggers->passive('goods_sales') . " VALUES " . implode(', ', $queryParts);
			$this->queryGateway->query($sql);
		}
		
		$this->log->info('Insered sales to goods_sales');
	}

}
