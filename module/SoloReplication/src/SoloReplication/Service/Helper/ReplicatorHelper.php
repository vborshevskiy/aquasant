<?php

namespace SoloReplication\Service\Helper;

use Solo\Db\QueryGateway\QueryGateway;

class ReplicatorHelper {

    /**
     * 
     * @var QueryGateway
     */
    private $queryGateway = null;
    /**
     * 
     * @var array
     */

    /**
     *
     * @param string $str        	
     * @return string
     */
    public function translit($str) {
        setlocale(LC_ALL, 'ru_RU.UTF-8');

        $trans = array(
            "а" => "a",
            "б" => "b",
            "в" => "v",
            "г" => "g",
            "д" => "d",
            "е" => "e",
            "ё" => "e",
            "ж" => "j",
            "з" => "z",
            "и" => "i",
            "й" => "y",
            "к" => "k",
            "л" => "l",
            "м" => "m",
            "н" => "n",
            "о" => "o",
            "п" => "p",
            "р" => "r",
            "с" => "s",
            "т" => "t",
            "у" => "u",
            "ф" => "f",
            "х" => "h",
            "ц" => "c",
            "ч" => "ch",
            "ш" => "sh",
            "щ" => "sh",
            "ы" => "i",
            "э" => "e",
            "ю" => "u",
            "я" => "ya",
            "А" => "A",
            "Б" => "B",
            "В" => "V",
            "Г" => "G",
            "Д" => "D",
            "Е" => "E",
            "Ё" => "E",
            "Ж" => "J",
            "З" => "Z",
            "И" => "I",
            "Й" => "Y",
            "К" => "K",
            "Л" => "L",
            "М" => "M",
            "Н" => "N",
            "О" => "O",
            "П" => "P",
            "Р" => "R",
            "С" => "S",
            "Т" => "T",
            "У" => "U",
            "Ф" => "F",
            "Х" => "H",
            "Ц" => "C",
            "Ч" => "Ch",
            "Ш" => "Sh",
            "Щ" => "Sh",
            "Ы" => "I",
            "Э" => "E",
            "Ю" => "U",
            "Я" => "Ya",
            "ь" => "",
            "Ь" => "",
            "ъ" => "",
            "Ъ" => ""
        );

        $str = strtr($str, $trans);

        return $str;
    }

    /**
     *
     * @param string $str        	
     * @return string
     */
    public function translitUrl($str) {
        $url = preg_replace('#[^а-яА-я\s\w]#ui', '_', $str);
        $url = str_replace(' ', '_', $this->translit($url));
        $url = str_replace(' ','_',$url);
        $url = preg_replace('#\_{2,}#u', '_', $url);
        $url = strtolower($url);
        $url = trim($url, '_');
        $url = trim($url);

        return $url;
    }

    /**
     *
     * @param string $urlName        	
     * @return string
     */
    public function editUrlName($urlName) {
        $urlName = str_replace(' ', '_', $urlName);
        $urlName = strtolower($urlName);

        return $urlName;
    }

    /**
     *
     * @param mixed $val        	
     * @throws \InvalidArgumentException
     * @return integer
     */
    public function parseInt($val) {
        if (!is_numeric($val)) {
            throw new \InvalidArgumentException('Input variable in parseInt must be numeric');
        }
        return intval($val);
    }

    /**
     *
     * @param string $val        	
     * @throws \InvalidArgumentException
     * @return string
     */
    public function parseString($val) {
        if (null === $val) {
            throw new \InvalidArgumentException('Input variable in parseString can\'t be null');
        }
        return trim($val);
    }

    /**
     *
     * @param mixed $val        	
     * @throws \InvalidArgumentException
     * @return float
     */
    public function parseFloat($val) {
        if (!is_numeric($val)) {
            throw new \InvalidArgumentException('Input variable in parseFloat must be numeric');
        }
        return floatval($val);
    }

    /**
     *
     * @param string $val        	
     * @return integer
     */
    public function parseBoolean($val) {
        if (is_bool($val)) {
            return ($val === true ? 1 : 0);
        }
        return ((0 == strcasecmp('true', $val)) ? 1 : 0);
    }
    
    /**
     *
     * @param string $val
     * @return DateTime
     */
    public function parseDate($val) {
        $date = \DateTime::createFromFormat('d.m.Y H:i:s', $val);
        return $date->format('Y-m-d H:i:s');
    }
    
    /**
     *
     * @param string $val
     * @return DateTime
     */
    public function getDateTime($val) {
        return \DateTime::createFromFormat('d.m.Y H:i:s', $val);
    }
    
    /**
     *
     * @param string $val
     * @return DateTime
     */
    public function parseTime($val) {
        $date = \DateTime::createFromFormat('H:i', $val);
        return $date->format('H:i:s');
    }
    
     /**
     *
     * @param string $val
     * @return DateTime
     */
    public function parseJSONDate($val) {
    	$dt = new \DateTime();
    	$dt->setTimestamp(strtotime($val));
    	return $dt->format('Y-m-d H:i:s');
    	
        preg_match('/(\d{10})(\d{3})([\+\-]\d{4})/', $val, $matches);
        // Get the timestamp as the TS tring / 1000
        $ts = (int) $matches[1];

        // Get the timezone name by offset
        $tz = (int) $matches[3];
        $tz = timezone_name_from_abbr("", $tz / 100 * 3600, false);
        $tz = new \DateTimeZone($tz);

        // Create a new DateTime, set the timestamp and the timezone
        $dt = new \DateTime();
        $dt->setTimestamp($ts);
        //$dt->setTimezone($tz);
        // Echo the formatted value         
        return $dt->format('Y-m-d H:i:s');
    }

    public function setQueryGateway(QueryGateway $queryGateway) {
        $this->queryGateway = $queryGateway;
    }
    
    public function getSiteUrl() {
        return 'http://'.$_SERVER['SERVER_NAME'];
    }
    
    /**
     * 
     * @param integer $value
     * @param string $jsonDate
     */
    public function changeJsonDate($value, $jsonDate) {
        preg_match('/(\d{10})(\d{3})([\+\-]\d{4})/', $jsonDate, $matches);
        if (count($matches) == 4) {
            $time = (int)$matches[1];
            $changedTime = $time + $value;
            $changesJsonDate = '/Date(' . $changedTime . $matches[2] . $matches[3] . ')/';
            return $changesJsonDate;
        }
        return false;
    }

}

?>