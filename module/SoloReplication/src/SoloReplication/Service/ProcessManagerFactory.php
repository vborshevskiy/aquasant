<?php

namespace SoloReplication\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ProcessManagerFactory implements FactoryInterface {

	public function createService(ServiceLocatorInterface $serviceLocator) {
		$config = $serviceLocator->get('Config');
		$processManager = new ProcessManager();
		
		$lockManager = $serviceLocator->get('SoloReplication\Service\LockManager');
		$processManager->setLockManager($lockManager);
		
		$triggers = $serviceLocator->get('triggers');
		$processManager->setTriggers($triggers);
		
		return $processManager; 
	}

}

?>