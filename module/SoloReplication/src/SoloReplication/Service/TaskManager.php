<?php

namespace SoloReplication\Service;

use Solo\ServiceManager\ServiceLocatorAwareService;

class TaskManager extends ServiceLocatorAwareService {

    private $taskSettings = [];
    
    private $replicationSettings = [];

    /**
     * 
     * @param string $name
     * @throws \RuntimeException
     * @return TaskInterface
     */
    public function create($name) {
        if (!array_key_exists($name, $this->taskSettings)) {
            throw new \RuntimeException(sprintf('Task %s not exists', $name));
        }
        $className = $this->taskSettings[$name]['className'];
        $task = $this->getServiceLocator()->get($className);
        if ($task instanceof AbstractTask) {
            $task->setName($name);
        }
        $task->setTriggers($this->getServiceLocator()->get('triggers'));
        $task->setCache($this->getServiceLocator()->get('cache'));
        $task->setHelper($this->getServiceLocator()->get('SoloReplication\Service\Helper\ReplicatorHelper'));
        $task->setImageService($this->getServiceLocator()->get('SoloCatalog\Service\ImageService'));
        $task->setStoresService($this->getServiceLocator()->get('SoloFacility\Service\StoresService'));
        $task->setLastUpdate($this->getServiceLocator()->get('replicatorsLastUpdates'));
        $task->setLogPath($this->replicationSettings['log_path'] . $task->getName() . '.log');
        $task->setLockPath($this->replicationSettings['locks_path'] . $task->getName() . '.lock');       
        $task->setPredisClient($this->getServiceLocator()->get('PredisClient'));
        $task->setImgPath($this->replicationSettings['img_path']);
        $task->setSiteId($this->replicationSettings['siteId']);
        $task->setYmlSettings($this->replicationSettings['yml']);
        $task->setSitemapSettings($this->replicationSettings['sitemap']);
        $task->setFilterService($this->getServiceLocator()->get('SoloCatalog\Service\FiltersService'));
        $task->setNotifyService($this->getServiceLocator()->get('SoloNotify\Service\NotifyService'));
        if (in_array('SoloReplication\Service\YandexYml',class_uses($task))) {
            $task->setYandexYmlMapper($this->getServiceLocator()->get('SoloReplication\Data\YandexYmlMapper'));
            $task->setOnlyDeliveryCitiesIds($this->getServiceLocator()->get('Config')['catalog']['onlyDeliveryCitiesIds']);
            $task->setCitiesSubdomains($this->getServiceLocator()->get('Config')['catalog']['citiesSubdomains']);
            $logisticCompaniesSchedule = $this->getServiceLocator()->get('SoloDelivery\Service\LogisticCompaniesService')->getSchedule();
            $shipppingPossibleDates = [];
            foreach ($logisticCompaniesSchedule as $companySchedule) {
                foreach ($companySchedule as $shippingDate) {
                    $shipppingPossibleDates[] = $shippingDate['shipping'];
                }
            }
            usort($shipppingPossibleDates, function($a, $b) {
                return (\SoloCatalog\Service\Helper\DateHelper::isFirstDateMore($a, $b));
            });
            $task->setShipppingPossibleDates($shipppingPossibleDates);
        }
        return $task;
    }

    /**
     * 
     * @param string $name
     * @throws \RuntimeException
     * @return string
     */
    public function getClassName($name) {
        if (!array_key_exists($name, $this->taskSettings)) {
            throw new \RuntimeException(sprintf('Task %s not exists', $name));
        }
        return $this->taskSettings[$name]['className'];
    }

    /**
     *
     * @param string $name
     * @param string $className
     */
    public function addTaskSettings($name, $className) {
        $this->taskSettings[$name] = [
            'name' => $name,
            'className' => $className
        ];
    }
    
    /**
     *
     * @param string $name
     * @param string $settings
     */
    public function addReplicationSettings($name, $settings) {
        $this->replicationSettings[$name] = $settings;
    }

}

?>