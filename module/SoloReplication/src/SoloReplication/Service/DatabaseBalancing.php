<?php

namespace SoloReplication\src\SoloReplication\Service;

use SoloReplication\Service\AbstractReplicator;
use Solo\Db\BalancingManager\BalancingObserver;

class DatabaseBalancing extends AbstractReplicator {

	protected $logFilename = 'database_balancing.log';

	protected $lockFilename = 'database_balancing.lock';

	/**
	 *
	 * @var BalancingObserver
	 */
	private $balancingObserver;

	/**
	 *
	 * @param BalancingObserver $balancingObserver        	
	 */
	public function __construct(BalancingObserver $balancingObserver) {
		$this->balancingObserver = $balancingObserver;
	}

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \SoloReplication\Service\AbstractReplicator::process()
	 */
	public function process() {
		$this->balancingObserver->saveSlaveMetrics();
	}

}

?>