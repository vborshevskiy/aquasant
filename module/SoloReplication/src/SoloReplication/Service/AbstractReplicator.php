<?php

namespace SoloReplication\Service;

use Zend\Log\Logger;
use Solo\Benchmark\Benchmark;
use Zend\Log\Writer\Stream;
use SoloSettings\Triggers;
use SoloSettings\Service\ConstantsService;
use Solo\Db\QueryGateway\QueryGateway;
use SoloWarehouse\Service\WarehouseService;
use Zend\EventManager\EventManagerAwareInterface;
use SoloERP\Service\ProvidesWebservice;
use Solo\EventManager\ProvidesEvents;
use SoloCache\Service\ProvidesCache;
use SoloReplication\Service\Helper\ReplicatorHelper;

abstract class AbstractReplicator implements TaskInterface, EventManagerAwareInterface {
	
	use ProvidesWebservice;
	use ProvidesEvents;
	use ProvidesCache;

	/**
	 * name of replicator
	 *
	 * @var string
	 */
	private $name = null;

	/**
	 *
	 * @var resource
	 */
	private $lockFile = null;

	/**
	 *
	 * @var Logger
	 */
	protected $log = null;

	/**
	 * path to log file
	 *
	 * @var string
	 */
	protected $logFilename = null;

	/**
	 * folder to save log
	 *
	 * @var string
	 */
	protected $logFolder = null;

	/**
	 * path to lock file
	 *
	 * @var string
	 */
	protected $lockFilename = null;

	/**
	 * folder to store lock files
	 *
	 * @var string
	 */
	protected $lockFolder = null;

	/**
	 *
	 * @var array
	 */
	protected $swapTables = [];

	/**
	 *
	 * @var Benchmark
	 */
	protected $benchmark = null;

	/**
	 *
	 * @var QueryGateway
	 */
	protected $queryGateway = null;

	/**
	 *
	 * @var Triggers
	 */
	protected $triggers = null;

	/**
	 *
	 * @var Constants
	 */
	protected $constants = null;

	/**
	 *
	 * @var WarehouseService
	 */
	protected $warehouses = null;

	/**
	 *
	 * @var ReplicatorHelper
	 */
	private $helper = null;

	/**
	 *
	 * @var PluginManager
	 */
	private $plugins = null;

	public function __construct() {
		$this->initName();
		$this->initBenchmark();
		$this->init();
	}

	/**
	 *
	 * @param QueryGateway $queryGateway        	
	 */
	public function setQueryGateway(QueryGateway $queryGateway) {
		$this->queryGateway = $queryGateway;
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return \Solo\Db\QueryGateway\QueryGateway
	 */
	public function getQueryGateway() {
		if (null === $this->queryGateway) {
			throw new \RuntimeException(sprintf('Query gateway not set in %s', get_class($this)));
		}
		return $this->queryGateway;
	}

	/**
	 *
	 * @param Triggers $triggers        	
	 */
	public function setTriggers(Triggers $triggers) {
		$this->triggers = $triggers;
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return \SoloSettings\Triggers
	 */
	public function getTriggers() {
		if (null === $this->triggers) {
			throw new \RuntimeException(sprintf('Triggers not set in %s', get_class($this)));
		}
		return $this->triggers;
	}

	/**
	 *
	 * @param Constants $constants        	
	 */
	public function setConstants(Constants $constants) {
		$this->constants = $constants;
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return \SoloSettings\Constants
	 */
	public function getConstants() {
		if (null === $this->constants) {
			throw new \RuntimeException(sprintf('Constants not set in %s', get_class($this)));
		}
		return $this->constants;
	}

	/**
	 *
	 * @param WarehouseService $warehouses        	
	 */
	public function setWarehouses(WarehouseService $warehouses) {
		$this->warehouses = $warehouses;
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return \SoloWarehouse\Service\WarehouseService
	 */
	public function getWarehouses() {
		if (null === $this->warehouses) {
			throw new \RuntimeException(sprintf('Warehouses not set in %s', get_class($this)));
		}
		return $this->warehouses;
	}

	/**
	 *
	 * @param string $folder        	
	 * @throws \InvalidArgumentException
	 */
	public function setLogFolder($folder) {
		if (empty($folder)) {
			throw new \InvalidArgumentException('Folder can\'t be empty');
		}
		$this->logFolder = $folder;
	}

	/**
	 *
	 * @return string
	 */
	public function getLogPath() {
		return $this->logFolder . '/' . $this->logFilename;
	}

	/**
	 *
	 * @param string $folder        	
	 * @throws \InvalidArgumentException
	 */
	public function setLockFolder($folder) {
		if (empty($folder)) {
			throw new \InvalidArgumentException('Folder can\'t be empty');
		}
		$this->lockFolder = $folder;
	}

	/**
	 *
	 * @return string
	 */
	public function getLockPath() {
		return $this->lockFolder . '/' . $this->lockFilename;
	}

	private function initName() {
		$this->name = get_class($this);
	}

	private function initLog() {
		$this->log = new Logger();
		
		// file
		$stream = @fopen($this->getLogPath(), 'a', false);
		if (!$stream) {
			throw new \RuntimeException('Failed to open stream for log writting');
		}
		$file = new Stream($stream);
		$this->log->addWriter($file);
		
		// output
		$output = new Stream('php://output');
		$this->log->addWriter($output);
	}

	private function initBenchmark() {
		$this->benchmark = new Benchmark('time', 'memory');
	}

	/**
	 * Lock replicator
	 *
	 * @return boolean
	 */
	protected function lock() {
		$this->lockFile = @fopen($this->getLockPath(), 'w+');
		if (!$this->lockFile) return false;
		if (!flock($this->lockFile, LOCK_EX | LOCK_NB)) {
			return false;
		}
		return true;
	}

	/**
	 * Unlock replicator
	 */
	protected function unlock() {
		flock($this->lockFile, LOCK_UN);
		fclose($this->lockFile);
	}

	/**
	 * Start replication
	 */
	public function start() {
		$this->initLog();
		
		$args = $this->events()->prepareArgs(['name' => $this->name, 'swapTables' => $this->swapTables]);
		$this->events()->trigger(__FUNCTION__ . '.pre', $this, $args);
		
		if ($this->lock()) {
			$this->log->info($this->name . ' started');
			$this->benchmark->start();
			$this->process();
		} else {
			$this->log->err('Failed to start replicator \'' . $this->name . '\'');
			exit();
		}
		
		$this->events()->trigger(__FUNCTION__ . '.post', $this, $args);
	}

	/**
	 * Stop replication
	 */
	public function stop() {
		$args = $this->events()->prepareArgs(['name' => $this->name, 'swapTables' => $this->swapTables]);
		$this->events()->trigger(__FUNCTION__ . '.pre', $this, $args);
		
		$this->triggers->swap($this->swapTables);
		
		$this->unlock();
		$this->log->info($this->name . ' stopped');
		$this->benchmark->stop();
		
		$this->events()->trigger(__FUNCTION__ . '.post', $this, $args);
		
		$this->log->info('Time spent: ' . $this->benchmark->getResult('time') . ' seconds');
		$this->log->info('Memory used: ' . $this->benchmark->getResult('memory') . ' bytes (' . round(($this->benchmark->getResult('memory') / 1024), 2) . ' Kb)' . "\n");
	}

	/**
	 * Quick launch replication in one call
	 */
	public function launch() {
		$this->start();
		$this->stop();
	}

	/**
	 * Clears specified table in database
	 *
	 * @param string $name        	
	 */
	protected function clearTable($name) {
		$this->queryGateway->clearTable($name);
	}

	/**
	 *
	 * @param string $name        	
	 * @throws \InvalidArgumentException
	 */
	protected function clearPassiveTable($name) {
		if (empty($name)) {
			throw new \InvalidArgumentException('Name can\'t be empty');
		}
		$this->clearTable($this->triggers->passive($name));
	}

	/**
	 *
	 * @param string $name        	
	 * @throws \InvalidArgumentException
	 */
	protected function clearActiveTable($name) {
		if (empty($name)) {
			throw new \InvalidArgumentException('Name can\'t be empty');
		}
		$this->clearTable($this->triggers->active($name));
	}

	/**
	 *
	 * @return \SoloReplication\Service\Helper\ReplicatorHelper
	 */
	protected function helper() {
		if (null === $this->helper) {
			$this->helper = new ReplicatorHelper();
			$this->helper->setQueryGateway($this->queryGateway);
			$this->helper->setTriggers($this->getTriggers());
		}
		return $this->helper;
	}

	/**
	 *
	 * @param PluginManager $pluginManager        	
	 * @return \SoloReplication\Service\AbstractReplicator
	 */
	public function setPluginManager(PluginManager $pluginManager) {
		$this->plugins = $pluginManager;
		$this->plugins->setReplicator($this);
		
		return $this;
	}

	/**
	 *
	 * @return \SoloReplication\Service\PluginManager
	 */
	public function getPluginManager() {
		if (null === $this->plugins) {
			$this->setPluginManager(new PluginManager());
		}
		return $this->plugins;
	}

	/**
	 *
	 * @param string $method        	
	 * @param array $params        	
	 * @return mixed | PluginInterface
	 */
	public function __call($method, $params) {
		$plugin = $this->getPluginManager()->get(strtolower($method), $params);
		if (is_callable($plugin)) {
			return call_user_func_array($plugin, $params);
		}
		return $plugin;
	}

	protected function init() {}

}

?>