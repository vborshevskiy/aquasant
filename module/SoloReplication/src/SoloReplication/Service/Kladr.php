<?php

namespace SoloReplication\Service;

use SoloReplication\Model\GenericModel;

set_time_limit(600);
ini_set('memory_limit', '256M');
class Kladr extends AbstractReplicator {

	protected $logFilename = 'kladr.log';

	protected $lockFilename = 'kladr.lock';

	protected $search;

	protected $swapTables = [
		'kladr_abbrs',
		'kladr_level1',
		'kladr_level2',
		'kladr_level3',
		'kladr_level4',
		'kladr_level5',
		'kladr_level6' 
	];

	public function setSearch($search) {
		$this->search = $search;
	}

	public function process() {
		$this->processKladr();
	}

	private function processKladr() {
		
		/*
		 * Первые 4 таблицы простые, все ответы сервиса помещаются на одну страницу, записей не много, до 5.000, решаем в лоб
		 */
		
		// список шагов
		$steps = array(
			'GetKladrAbbrsCSV' => array(
				'table' => 'kladr_abbrs' 
			),
			'GetKladrLevel1CSV' => array(
				'table' => 'kladr_level1' 
			),
			'GetKladrLevel2CSV' => array(
				'table' => 'kladr_level2' 
			),
			'GetKladrLevel3CSV' => array(
				'table' => 'kladr_level3' 
			) 
		);
		
		foreach ($steps as $remoteCall => $stepParams) {
			$res = $this->callMethod($remoteCall, [])->getResponse();
			
			// собираем названия полей из описания в результате запроса
			// приводим к нижнему регистру, под названия полей в соотв. таблицах БД
			$addrFields = [];
			if (!empty($res[0])) {
				foreach ($res[0] as $key) {
					$addrFields[] = strtolower($key);
				}
			}
			
			// собираем массив данных для загрузки
			$items = [];
			if (!empty($addrFields) && !empty($res[1]) && is_array($res[1])) {
				// бежим по массиву результатов
				foreach ($res[1] as $string) {
					$values = explode('||', $string);
					$items[] = array_combine($addrFields, $values);
				}
			}
			
			// стандартный механизм обновления
			if (!empty($items)) {
				$this->clearPassiveTable($stepParams['table']);
				$sites = new GenericModel($stepParams['table'], '', $this->triggers);
				$sites->insertSetPassive($items);
			}
			
			$this->log->info('Insered to ' . $stepParams['table']);
		}
		
		/*
		 * теперь немного сложнее для 4, 5 и 6 по сути тоже самое, но придется обрабатывать постранично много-много данных до 800.000 тысяч записей, на странице примерно по 30.000 записей (не ровно, бывает меньше) будем инкрементировать страницы до тех пор пока не получим пустую работает это все МЕДЛЕННО, возможно придется как-то делить/выносить
		 */
		
		// список шагов
		$steps = array(
			'GetKladrLevel4CSV' => array(
				'table' => 'kladr_level4' 
			),
			'GetKladrLevel5CSV' => array(
				'table' => 'kladr_level5' 
			),
			'GetKladrLevel6CSV' => array(
				'table' => 'kladr_level6' 
			) 
		);
		
		foreach ($steps as $remoteCall => $stepParams) {
			
			// чистим пассивную таблицу перед наполнением
			$this->clearPassiveTable($stepParams['table']);
			
			$page = 1;
			do {
				$res = $this->callMethod($remoteCall, array(
					'rows' => $page 
				))->getResponse();
				
				// собираем названия полей из описания в результате запроса
				// приводим к нижнему регистру, под названия полей в соотв. таблицах БД
				$addrFields = [];
				if (!empty($res[0])) {
					foreach ($res[0] as $key) {
						$addrFields[] = strtolower($key);
					}
				}
				
				// собираем массив данных для загрузки
				$items = [];
				if (!empty($addrFields) && !empty($res[1]) && is_array($res[1])) {
					// бежим по массиву результатов
					foreach ($res[1] as $string) {
						$values = explode('||', $string);
						
						// экранируем и собираем параметры
						array_walk($values, function (&$val, $key) use($values) {
							$val = $this->queryGateway->quoteValue($val);
						});
						
						// заготовка под инсерты блоками по N записей
						$items[] = '(' . implode(',', $values) . ')';
					}
				}
				
				// НЕстандартный механизм обновления
				// будем загружать в таблицу кусками
				$chunks = array_chunk($items, 1000);
				
				foreach ($chunks as $chunk) {
					$sql = "INSERT " . $this->triggers->passive($stepParams['table']) . " VALUES " . implode(', ', $chunk);
					
					$this->queryGateway->query($sql);
				}
				
				// переворачиваем страницу
				$page++;
			} while (!empty($res[1]));
			
			$this->log->info('Insered to ' . $stepParams['table']);
		}
		
		// ///////////////////
		// все что нужно отреплицировали, теперь надо все проиндексировать для поиска
		// ///////////////////
		$vars = array(
			'kladr_level2' => $this->triggers->active('kladr_level2'),
			'kladr_level3' => $this->triggers->active('kladr_level3'),
			'kladr_level4' => $this->triggers->active('kladr_level4'),
			'kladr_level5' => $this->triggers->active('kladr_level5'),
			'kladr_abbrs' => $this->triggers->active('kladr_abbrs'),
			'kladr_costs' => $this->triggers->active('kladr_costs') 
		);
		
		/**
		 * Cтатический метод process делает всё и сразу:
		 * - устанавливает откуда и куда писать конфиг
		 * - рендерит свежий конфиг
		 * - сохраняет конфиг
		 *
		 * В классе так же есть отдельные методы для каждой операции
		 */
		$config = \Solo\Sphinx\Config::process($this->search->getConfigTemplatePath(), $this->search->getTmpConfigPath(), $vars);
		
		// собссно индексер, который строит индекс
		$indexer = new \Solo\Sphinx\Indexer();
		
		// опция на перерисовку индексов без перезагрузки демона поиска
		$indexer->addOption('rotate');
		
		// откуда читаем свежий конфиг, грубо говоря, это то же самое, что и $searchService->getTmpConfigPath()
		$indexer->setParam('config', $config->getSavePath());
		
		// тут командой exec запускается индексатор (индексируем улицы)
		$out = $indexer->setIndex('streets')->run();
		var_dump($out);
		
		// индексируем города
		$out = $indexer->setIndex('moscowRegionTowns')->run();
		var_dump($out);
	}

}
