<?php

namespace SoloReplication\Service;

use SoloSettings\Service\TriggersService;
use SoloCache\Service\CacheService;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use SoloReplication\Service\Helper\ReplicatorHelper;
use SoloSettings\Service\ReplicatorsLastUpdatesService;
use SoloCatalog\Service\ImageService;
use SoloFacility\Service\StoresService;
use SoloCatalog\Service\FiltersService;
use SoloNotify\Service\NotifyService;

abstract class AbstractReplicationTask extends AbstractTask {

    /**
     *
     * @var array
     */
    protected $swapTables = [];

    /**
     *
     * @var TriggersService
     */
    protected $triggersService = null;

    /**
     *
     * @var CacheService
     */
    protected $cacheService = null;

    /**
     *
     * @var string
     */
    protected $logPath = null;

    /**
     *
     * @var string
     */
    protected $imgPath = null;

    /**
     *
     * @var string
     */
    protected $lockPath = null;

    /**
     *
     * @var string
     */
    protected $eventName = null;

    /**
     *
     * @var ReplicatorHelper
     */
    protected $helper;

    /**
     *
     * @var PredisClient
     */
    protected $predisClient = null;

    /**
     *
     * @var ReplicatorsLastUpdatesService
     */
    protected $replicatorLastUpdate;

    /**
     *
     * @var ImageService
     */
    protected $imageService;

    /**
     *
     * @var StoresService
     */
    protected $storesService;

    /**
     *
     * @var array
     */
    protected $ymlSettings = [];

    /**
     *
     * @var array
     */
    protected $sitemapSettings = [];

    /**
     *
     * @var FiltersService
     */
    protected $filterService;

    /**
     *
     * @var NotifyService
     */
    protected $notifyService;

    /**
     *
     * @var integer
     */
    protected $scriptNotRespondingTimeInMinutes = 60;

    /**
     *
     * @var siteId
     */
    protected $siteId;

    public function run() {
        $this->initLog();
        if ($this->lock()) {
            $this->processTriggerHandler();
            $this->process();
            $this->events()->trigger($this->eventName, null, []);
        } else {
            $this->log->info('Replication ' . $this->getName() . ' is already running');
        }
    }

    protected function processTriggerHandler() {
        $triggersService = $this->triggersService;
        $this->events()->attach(
                $this->eventName, function ($e) use ($triggersService) {
                $parameters = $e->getParams($parameters);
                $this->log->info('Tables swapping start');
                $tags = [];
                foreach ($this->getSwapTables() as $tbl) {
                    $tags[] = $this->triggersService->swap($tbl);
                    $this->log->info('Table ' . $tbl . ' swapped');
                }
                $this->getCache()->clearByTags($tags);
                $this->log->info('Tables swapping end');
            }
        );
    }

    /**
     * Add one or several table base names for swaping after task procession
     */
    public function addSwapTable() {
        foreach (func_get_args() as $tableName) {
            if (is_string($tableName) && !in_array($tableName, $this->swapTables)) {
                $this->swapTables[] = $tableName;
            }
            if (is_array($tableName)) {
                foreach ($tableName as $name) {
                    $this->addSwapTable($name);
                }
            }
        }
    }

    public function removeSwapTable() {
        foreach (func_get_args() as $tableName) {
            if (is_string($tableName) && in_array($tableName, $this->swapTables)) {
                $key = array_search($tableName, $this->swapTables);
                unset($this->swapTables[$key]);
            }
        }
    }

    public function isExistSwapTable() {
        foreach (func_get_args() as $tableName) {
            if (is_string($tableName) && !in_array($tableName, $this->swapTables)) {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @return array
     */
    public function getSwapTables() {
        return $this->swapTables;
    }

    public function setTriggers(TriggersService $triggersService) {
        $this->triggersService = $triggersService;
    }

    public function getTriggers() {
        return $this->triggersService;
    }

    public function setCache(CacheService $cacheService) {
        $this->cacheService = $cacheService;
    }

    public function getCache() {
        return $this->cacheService;
    }

    protected function initLog() {
        $this->log = new Logger();

        // output
        $output = new Stream('php://output');
        $this->log->addWriter($output);

        // file
        try {
            $stream = @fopen($this->getLogPath(), 'a', false);
            if (!$stream) {
                throw new \RuntimeException('Failed to open stream for log writting');
            }
            $file = new Stream($stream);
            $this->log->addWriter($file);
        } catch (\Exception $e) {
            $this->log->warn('Warning: wrong log file path: ' . $this->getLogPath());
        }
    }

    public function setLogPath($logPath) {
        $this->logPath = $logPath;
    }

    public function getLogPath() {
        return $this->logPath;
    }

    public function getImgPath() {
        return $this->imgPath;
    }

    public function setImgPath($imgPath) {
        $this->imgPath = $imgPath;
    }

    public function setLockPath($lockPath) {
        $this->lockPath = $lockPath;
    }

    public function getLockPath() {
        return $this->lockPath;
    }

    public function setSiteId($siteId) {
        $this->siteId = $siteId;
    }

    public function getSiteId() {
        return $this->siteId;
    }

    /**
     * Lock replicator
     *
     * @return boolean
     */
    protected function lock() {
        $this->lockFile = @fopen($this->getLockPath(), 'w+');
        if (!$this->lockFile) {
            return false;
        }
        @fwrite($this->lockFile, (string)$this->scriptNotRespondingTimeInMinutes);
        if (!flock($this->lockFile, LOCK_EX | LOCK_NB)) {
            return false;
        }
        return true;
    }

    /**
     * Unlock replicator
     */
    protected function unlock() {
        flock($this->lockFile, LOCK_UN);
        fclose($this->lockFile);
    }

    /*
     * @param ReplicatorHelper $helper
     */
    public function setHelper(ReplicatorHelper $helper) {
        $this->helper = $helper;
    }

    public function helper() {
        return $this->helper;
    }

    public function setLastUpdate(ReplicatorsLastUpdatesService $replicatorLastUpdate) {
        $this->replicatorLastUpdate = $replicatorLastUpdate;
    }

    public function setPredisClient($predisClient) {
        $this->predisClient = $predisClient;
    }

    public function lastUpdate() {
        return $this->replicatorLastUpdate;
    }

    /*
     * @param ImageService $imageService
     */
    public function setImageService(ImageService $imageService) {
        $this->imageService = $imageService;
    }

    /*
     * @return ImageService
     */
    public function imageService() {
        return $this->imageService;
    }

    /*
     * @param StoresService $imageService
     */
    public function setStoresService(StoresService $storesService) {
        $this->storesService = $storesService;
    }

    /*
     * @return StoresService
     */
    public function storesService() {
        return $this->storesService;
    }

    /**
     *
     * @param array $ymlSettings
     */
    public function setYmlSettings($ymlSettings) {
        $this->ymlSettings = $ymlSettings;
    }

    /**
     *
     * @return array
     */
    public function getYmlSettings() {
        return $this->ymlSettings;
    }

    /**
     *
     * @param array $sitemapSettings
     */
    public function setSitemapSettings($sitemapSettings) {
        $this->sitemapSettings = $sitemapSettings;
    }

    /**
     *
     * @return array
     */
    public function getSitemapSettings() {
        return $this->sitemapSettings;
    }

    /**
     *
     * @param FiltersService $filterService
     */
    public function setFilterService($filterService) {
        $this->filterService = $filterService;
    }

    /**
     *
     * @return FiltersService
     */
    public function filters() {
        return $this->filterService;
    }

    /**
     *
     * @param NotifyService $notifyService
     */
    public function setNotifyService(NotifyService $notifyService) {
        $this->notifyService = $notifyService;
    }

    /**
     *
     * @return NotifyService
     */
    public function notify() {
        return $this->notifyService;
    }

    /**
     *
     * @param integer $timeInMinutes
     */
    public function setScriptNotRespondingTimeInMinutes($timeInMinutes) {
        $this->scriptNotRespondingTimeInMinutes = $timeInMinutes;
    }

    /**
     * @return string
     */
    public function getNotifyMessage($className, $methodName, $erpMethod = null, $params = null) {
        $message = '';
        $message .= 'Репликатор: ' . $className . "\n";
        $message .= 'Метод: ' . $methodName . "\n";
        if (!is_null($erpMethod)) {
            $message .= 'Метод ERP: ' . $erpMethod . "\n";
        }
        if (!is_null($params)) {
            $message .= 'Параметры: ' . json_encode($params) . "\n";
        }
        return $message;
    }

}
