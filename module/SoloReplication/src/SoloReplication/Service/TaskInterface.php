<?php

namespace SoloReplication\Service;

use Zend\EventManager\EventManagerAwareInterface;

interface TaskInterface extends EventManagerAwareInterface {

	public function process();

}

?>