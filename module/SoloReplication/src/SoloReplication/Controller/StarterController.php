<?php

namespace SoloReplication\Controller;

use Solo\Mvc\Controller\ActionController;
use SoloReplication\Service\AbstractReplicator;
use SoloCache\Service\ProvidesCache;

class StarterController extends ActionController {

    use ProvidesCache;

    public function managerAction() {
        $taskName = $this->params('type');

        $taskManager = $this->service('SoloReplication\Service\TaskManager');
        $task = $taskManager->create($taskName);
        $task->setTriggers($this->service('triggers'));
        $task->setCache($this->cache());
        $task->setHelper($this->service('SoloReplication\Service\Helper\ReplicatorHelper'));

        $processManager = $this->service('SoloReplication\Service\ProcessManager');
        $processManager->run($task);

        exit();
    }

    public function indexAction() {
        $type = $this->params()->fromRoute('type');
        if (empty($type)) {
            print 'Failed to start replication on empty type';
            exit();
        }
        $replicator = $this->createReplicator($type);
        if (empty($replicator)) {
            print 'Failed to create replicator: ' . $type;
            exit();
        }
        $replicator->launch();
        $this->otherActions($type);
        exit();
    }

    public function consoleAction() {
        $name = $this->getRequest()->getParam('name');
        if (!empty($name)) {
            $replicator = $this->createReplicator($name);
            if (empty($replicator)) {
                return 'Failed to create replicator: ' . $name;
            }
            $processManager = $this->service('SoloReplication\Service\ProcessManager');
            $processManager->run($replicator);
            return '';
        } else {
            return 'Type name of replicator';
        }
    }

    /**
     *
     * @param string $type        	
     * @return AbstractReplicator
     */
    private function createReplicator($type) {
        $taskManager = $this->service('SoloReplication\Service\TaskManager');
        $service = $taskManager->create($type);
        return $service;
    }

}
