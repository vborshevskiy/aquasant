<?php

namespace SoloReplication\Data;

use Solo\Db\Mapper\AbstractMapper;

class YandexYmlMapper extends AbstractMapper implements YandexYmlMapperInterface {
    
    /**
     *
     * @var array
     */
    protected $passiveTables = [];
    
    /**
     * 
     * @param string $tableName
     * @return string
     */
    public function getTableActivity($tableName) {
        if (in_array($tableName, $this->passiveTables)) {
            return 'passive';
        }
        return 'active';
    }
    
    /**
     * 
     * @param string $tableName
     */
    public function addPassiveTable($tableName) {
        if (!in_array($tableName, $this->passiveTables)) {
            $this->passiveTables[] = $tableName;
        }
    }
    
    public function clearPassiveTables() {
        $this->passiveTables = [];
    }

    /**
     *
     * @return array
     */
    public function getCities() {
        $sql = 'SELECT
                        of.OfficeID,
                        cit.CityID
                FROM
                        #offices:'.$this->getTableActivity('offices').'# of
                INNER JOIN
                        #cities:'.$this->getTableActivity('cities').'# cit
                        ON cit.CityID = of.OfficeLocationId
                WHERE
                        of.Hide = 0';
        $rows = $this->query($sql)->toArray();
        $result = [];
        foreach ($rows as $row) {
            $result[(int)$row['CityID']][] = (int)$row['OfficeID'];
        }
        return $result;
    }
    
    /**
     *
     * @param array $officeIds
     * @return array
     */
    public function getAvailGoods($officeIds) {
        if (!is_array($officeIds) || count($officeIds) == 0) {
            return [];
        }
        $sql = 'SELECT
                        ag.GoodID,
                        sc.SoftCategoryID,
        				ag.AvailQuantity,
        				ag.SuborderQuantity,
        				ag.WindowQuantity
                FROM
                        #avail_goods:'.$this->getTableActivity('avail_goods').'# ag
                INNER JOIN
                        #goods_to_soft_categories:'.$this->getTableActivity('goods_to_soft_categories').'# gtsc
                        ON gtsc.GoodID = ag.GoodID
                INNER JOIN
                        #soft_categories:'.$this->getTableActivity('soft_categories').'# sc
                        ON sc.SoftCategoryID = gtsc.SoftCategoryID
                WHERE
                        ag.AvailQuantity > 0 OR ag.SuborderQuantity > 0';
        $rows = $this->query($sql, true)->toArray();
        $result = [];
        foreach ($rows as $row) {
            $result[(int)$row['GoodID']] = (int)$row['SoftCategoryID'];
        }
        return $result;
    }
    
    /**
     *
     * @param array $officeIds
     * @return array
     */
    public function getSuborderGoods($officeIds) {
        if (!is_array($officeIds) || count($officeIds) == 0) {
            return [];
        }
        // в субботу прибавляем 4 дня (не учитываем)
        $sql = 'SELECT DISTINCT
                        arg.GoodID,
                        CASE
                            WHEN arg.ArrivalDate < ADDDATE(NOW(),
                                INTERVAL
                                    CASE
                                        WHEN WEEKDAY(NOW()) = 3 THEN 4
                                        WHEN WEEKDAY(NOW()) = 4 THEN 4
                                        WHEN WEEKDAY(NOW()) = 5 THEN 3
                                        ELSE 2
                                    END
                                DAY)
                            THEN 1
                            ELSE 0
                        END AS Avail,
                        sc.SoftCategoryID
                FROM
                        #arrival_goods:'.$this->getTableActivity('arrival_goods').'# arg
                INNER JOIN
                        #all_goods:'.$this->getTableActivity('all_goods').'# ag
                        ON ag.GoodID = arg.GoodID
                INNER JOIN
                        #goods_to_soft_categories:'.$this->getTableActivity('goods_to_soft_categories').'# gtsc
                        ON gtsc.GoodID = ag.GoodID
                INNER JOIN
                        #soft_categories:'.$this->getTableActivity('soft_categories').'# sc
                        ON sc.SoftCategoryID = gtsc.SoftCategoryID
                INNER JOIN
                        #offices:'.$this->getTableActivity('offices').'# of
                        ON of.OfficeID = arg.OfficeID
                        AND of.IsPickup = 1
                WHERE
                        arg.OfficeID IN (' . implode(',',$officeIds) . ')
                        AND arg.SuborderQuantity > 0
                        AND ag.OriginalGoodID = 0';
        $rows = $this->query($sql, true)->toArray();
        $result = new \stdClass();
        $result->avail = [];
        $result->categories = [];
        foreach ($rows as $row) {
            $result->avail[(int)$row['GoodID']] = (bool)$row['Avail'];
            $result->categories[(int)$row['GoodID']] = (int)$row['SoftCategoryID'];
        }
        return $result;
    }
    
    /**
     *
     * @param array $officeIds
     * @return array
     */
    public function getMainWarehouseSuborderGoods($officeIds) {
        if (!is_array($officeIds) || count($officeIds) == 0) {
            return [];
        }
        // в субботу прибавляем 4 дня (не учитываем)
        $sql = 'SELECT DISTINCT
                        aw.GoodID,
                        CASE
                            WHEN grad.PickupDate < ADDDATE(NOW(),
                                INTERVAL
                                    CASE
                                        WHEN WEEKDAY(NOW()) = 3 THEN 4
                                        WHEN WEEKDAY(NOW()) = 4 THEN 4
                                        WHEN WEEKDAY(NOW()) = 5 THEN 3
                                        ELSE 2
                                    END
                                DAY)
                            THEN 1
                            ELSE 0
                        END AS Avail,
                        sc.SoftCategoryID
                FROM
                        #avail_warehouses:'.$this->getTableActivity('avail_warehouses').'# aw
                INNER JOIN
                        #all_goods:'.$this->getTableActivity('all_goods').'# ag
                        ON ag.GoodID = aw.GoodID
                INNER JOIN
                        #goods_to_soft_categories:'.$this->getTableActivity('goods_to_soft_categories').'# gtsc
                        ON gtsc.GoodID = ag.GoodID
                INNER JOIN
                        #soft_categories:'.$this->getTableActivity('soft_categories').'# sc
                        ON sc.SoftCategoryID = gtsc.SoftCategoryID
                INNER JOIN
                        #offices:'.$this->getTableActivity('offices').'# of
                        ON of.OfficeID = aw.WarehouseID
                INNER JOIN
                        #good_reserve_avail_date:'.$this->getTableActivity('good_reserve_avail_date').'# grad
                        ON grad.GoodID = aw.GoodID
                        AND grad.CityID = of.OfficeLocationId
                WHERE
                        aw.WarehouseID IN (' . implode(',',$officeIds) . ')
                        AND aw.MainWarehouseSuborderQuantity > 0
                        AND ag.OriginalGoodID = 0
                        ';
        $rows = $this->query($sql, true)->toArray();
        $result = new \stdClass();
        $result->avail = [];
        $result->categories = [];
        foreach ($rows as $row) {
            $result->avail[(int)$row['GoodID']] = (bool)$row['Avail'];
            $result->categories[(int)$row['GoodID']] = (int)$row['SoftCategoryID'];
        }
        return $result;
    }
    
    /**
     * 
     * @return array
     */
    public function getCategories() {
        $sql = 'SELECT
                        sc.SoftCategoryID,
                        sc.ParentID,
                        sc.cLeft,
                        sc.cRight,
                        sc.cLevel,
                        sc.CategoryName,
                        sc.CategoryUID
                FROM
                        #soft_categories:'.$this->getTableActivity('soft_categories').'# sc';
        $rows = $this->query($sql, true)->toArray();
        $result = [];
        foreach ($rows as $row) {
            $result[$row['SoftCategoryID']] = $row;
        }
        return $result;
    }
    
    /**
     * 
     * @param array $goodIds
     * @return array
     */
    public function getGoodsInfo($goodIds) {
        if (!is_array($goodIds) || count($goodIds) == 0) {
            return [];
        }
        $sql = 'SELECT
                        alg.GoodID,
                        alg.GoodDescription,
                        alg.BrandWarranty,
                        alg.GoodName,
                        alg.BrandID,
                        alg.GoodEngName,
                        alg.Weight,
                        alg.Volume,
                        alg.Model,
                        alg.VendorCode,
        				alg.IsPackage,
        				alg.ViewMode,
        				alg.GoogleCategoryName
                FROM
                        #all_goods:'.$this->getTableActivity('all_goods').'# alg
                WHERE
                        alg.GoodID IN ('.implode(',', $goodIds).')';
        $rows = $this->query($sql, true)->toArray();
        $result = [];
        foreach ($rows as $row) {
            $result[$row['GoodID']] = $row;
        }
        return $result;
    }
    
    /**
     * 
     * @param integer $cityId
     * @param array $goodIds
     * @param integer $categoryId
     * @return array
     */
    public function getGoodsPrices($cityId, $goodIds, $categoryId = 1) {
        if (!is_array($goodIds) || count($goodIds) == 0) {
            return [];
        }
        $sql = 'SELECT
                        gp.GoodID,
                        gp.Value as Price,
                        gp.PrevValue as PrevPrice
                FROM
                        #goods_prices:'.$this->getTableActivity('goods_prices').'# gp
                INNER JOIN
                        #locations:'.$this->getTableActivity('locations').'# loc
                        ON loc.LocationID = '.(int)$cityId.'
                        AND loc.LocationZoneID = gp.ZoneID
                WHERE
                        gp.GoodID IN ('.implode(',', $goodIds).')
                        AND gp.CategoryID = ' . $categoryId;
        $rows = $this->query($sql, true)->toArray();
        $result = [];
        foreach ($rows as $row) {
            $result[$row['GoodID']] = $row;
        }
        return $result;
    }
    
    /**
     * 
     * @param array $goodIds
     * @return array
     */
    public function getVendorsByGoodIds($goodIds) {
        if (!is_array($goodIds) || count($goodIds) == 0) {
            return [];
        }
        $sql = 'SELECT DISTINCT
                        br.*
                FROM
                        #all_goods:'.$this->getTableActivity('all_goods').'# alg
                INNER JOIN
                        #brands:'.$this->getTableActivity('brands').'# br
                        ON br.BrandID = alg.BrandID
                WHERE
                        alg.GoodID IN ('.implode(',', $goodIds).')';
        $rows = $this->query($sql, true)->toArray();
        $result = [];
        foreach ($rows as $row) {
            $result[$row['BrandID']] = $row;
        }
        return $result;
    }
    
    /**
     * 
     * @param integer $cityId
     * @param array $goodIds
     * @return array
     */
    public function getGoodsDeliveryPrice($cityId, $goodIds) {
        if (!is_array($goodIds) || count($goodIds) == 0) {
            return [];
        }
        $sql = 'SELECT
                        dpr.GoodID,
                        dpr.DeliveryPrice
                FROM
                        #delivery_prices:'.$this->getTableActivity('delivery_prices').'# dpr
                WHERE
                        dpr.CityID = '.(int)$cityId.'
                        AND dpr.GoodID IN ('.implode(',', $goodIds).')';
        $rows = $this->query($sql, true)->toArray();
        $result = [];
        foreach ($rows as $row) {
            $result[$row['GoodID']] = $row['DeliveryPrice'];
        }
        return $result;
    }
    
    /*
     * @param array $goodIds
     * @return array
     */
    public function getGoodsImages($goodIds) {
        $sql = 'SELECT
                        gi.GoodID,
                        gi.MiniatureUrl
                FROM
                        #goods_images:'.$this->getTableActivity('goods_images').'# gi
                INNER JOIN
                        #goods_images_views:'.$this->getTableActivity('goods_images_views').'# giv
                        ON giv.ViewID = gi.ViewID
                WHERE
                        gi.GoodID IN ('.implode(',', $goodIds).')
                ORDER BY
                       	giv.SortIndex ASC,
                        gi.SortIndex ASC';
        $rows = $this->query($sql, true)->toArray();
        $result = [];
        foreach ($rows as $row) {
        	if (!isset($result[$row['GoodID']])) {
            	$result[$row['GoodID']] = $row['MiniatureUrl'];
        	}
        }
        return $result;
    }
    
    /**
     * 
     * @param integer $cityId
     * @param array $goodIds
     * @return array
     */
    public function getReserveAvailDates($cityId, $goodIds) {
        $sql = 'SELECT
                        grad.GoodID,
                        grad.PickupDate,
                        grad.AvailDeliveryDate,
                        grad.SuborderDeliveryDate
                FROM
                        #good_reserve_avail_date:'.$this->getTableActivity('good_reserve_avail_date').'# grad
                WHERE
                        grad.GoodID IN ('.implode(',', $goodIds).')
                        AND grad.CityID = '.$cityId;
        $rows = $this->query($sql, true)->toArray();
        $result = [];
        foreach ($rows as $row) {
            $result[$row['GoodID']] = $row;
        }
        return $result;
    }
    
    /**
     * 
     * @param integer $cityId
     * @return array
     */
    public function getDeliveryGoods($cityId) {
        $sql = 'SELECT DISTINCT
                        avg.GoodID,
                        grad.AvailDeliveryDate
                FROM
                        #avail_goods:'.$this->getTableActivity('avail_goods').'# avg
                INNER JOIN
                        #good_reserve_avail_date:'.$this->getTableActivity('good_reserve_avail_date').'# grad
                        ON grad.GoodID = avg.GoodID
                        AND grad.CityID = '.$cityId.'
                WHERE
                        avg.DeliveryQuantity > 0
                        AND avg.CityID = '.$cityId;
        $rows = $this->query($sql, true)->toArray();
        $result = [];
        foreach ($rows as $row) {
            $result[$row['GoodID']] = new \DateTime($row['AvailDeliveryDate']);
        }
        return $result;
    }
    
    /**
     * @return array
     */
    public function getHolidays() {
        $sql = 'SELECT DISTINCT
                        h.*
                FROM
                        #holidays:'.$this->getTableActivity('holidays').'# h';
        return \Solo\Stdlib\ArrayHelper::enumOneColumn($this->query($sql, true)->toArray(), 'HolidayDate');
    }
}
