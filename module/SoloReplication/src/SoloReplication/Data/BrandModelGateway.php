<?php

namespace SoloReplication\Data;

use Solo\Db\QueryGateway\QueryGateway;

class BrandModelGateway extends QueryGateway {

	public function getBrandsAndModels() {
		$sql = "SELECT
                                gc.GoodID as GoodId,
                                CASE
                                        WHEN gc.ValueText <> '' THEN gc.ValueText
                                        WHEN gc.ValueString <> '' THEN gc.ValueString
                                        WHEN gc.PropertyValueID > 0 THEN group_concat(cv.PropertyValue SEPARATOR ',\n')
                                END AS value,
                                c.PropertyName AS name,
                                pu.UnitName AS uname,
                                pu.PropertyUnitID AS id
                        FROM
                                #all_goods# ag
                        INNER JOIN
                                #goods_to_propvalues# gc
                                ON ag.GoodID = gc.GoodID
                        INNER JOIN
                                #props# c
                                ON c.PropertyID = gc.PropertyID
                        LEFT OUTER JOIN
                                #prop_values# cv
                                ON cv.PropertyValueID = gc.PropertyValueID
                        INNER JOIN
                                #props_to_templates# ptt
                                ON (
                                        ptt.PropertyID = gc.PropertyID
                                        AND ag.PropertyTemplateID = ptt.PropertyTemplateID
                                )
                        INNER JOIN
                                #prop_units# pu
                                ON pu.PropertyUnitID = c.PropertyUnitID
                        WHERE
                                (
                                        c.PropertyName = 'Бренд'
                                        OR c.PropertyName = 'Модель'
                                )
                                AND ptt.IsHideOnWebsite = 0
                                AND ptt.IsHide <> 1
                                AND ptt.IsShowInSfOnly <> 1
                                AND (
                                        cv.PropertyValue <> ''
                                        OR gc.PropertyValueID < 2
                                )
                        GROUP BY
                                gc.GoodID,
                                gc.PropertyID,
                                c.PropertyName
                        ORDER BY
                                gc.GoodID ASC";
		return $this->query($sql)->toArray();
	}

}

?>