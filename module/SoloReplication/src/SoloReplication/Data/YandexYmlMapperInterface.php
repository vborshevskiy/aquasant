<?php

namespace SoloReplication\Data;

interface YandexYmlMapperInterface {

    /**
     *
     * @return array
     */
    public function getCities();
    
    /**
     *
     * @param array $officeIds
     * @return array
     */
    public function getAvailGoods($officeIds);
    
    /**
     *
     * @param array $officeIds
     * @return array
     */
    public function getSuborderGoods($officeIds);
    
    /**
     * 
     * @return array
     */
    public function getCategories();
    
    /**
     * 
     * @param array $goodIds
     * @return array
     */
    public function getGoodsInfo($goodIds);
    
    /**
     * 
     * @param array $goodIds
     * @return array
     */
    public function getVendorsByGoodIds($goodIds);
    
    /**
     * 
     * @param integer $cityId
     * @param array $goodIds
     * @return array
     */
    public function getGoodsDeliveryPrice($cityId, $goodIds);
    
    /*
     * @param array $goodIds
     * @return array
     */
    public function getGoodsImages($goodIds);
    
    /**
     * 
     * @param integer $cityId
     * @param array $goodIds
     * @return array
     */
    public function getReserveAvailDates($cityId, $goodIds);
    
    /**
     *
     * @param array $officeIds
     * @return array
     */
    public function getMainWarehouseSuborderGoods($officeIds);
    
    /**
     * 
     * @param integer $cityId
     * @return array
     */
    public function getDeliveryGoods($cityId);
    
    /**
     * @return array
     */
    public function getHolidays();
}
