<?php

return [
    'cabinet' => [
        'all_reserves' => [
            'default_sorting' => [
                'column' => 'document_no',
                'direction' => 'desc'
            ],
            'items_per_page' => 20
        ],
        'active_reserves' => [
            'default_sorting' => [
                'column' => 'document_no',
                'direction' => 'desc'
            ],
            'items_per_page' => 20
        ],
        'finished_reserves' => [
            'default_sorting' => [
                'column' => 'document_no',
                'direction' => 'desc'
            ],
            'items_per_page' => 20
        ],
        'processed_reserves' => [
            'default_sorting' => [
                'column' => 'document_no',
                'direction' => 'desc'
            ],
            'items_per_page' => 20
        ],
        'drafts' => [
            'default_sorting' => [
                'column' => 'document_no',
                'direction' => 'desc'
            ],
            'items_per_page' => 20
        ],
        'balance' => [
            'default_sorting' => [
                'column' => 'dt_process',
                'direction' => 'desc'
            ],
            'items_per_page' => 20
        ],
        'bonus' => [
            'default_sorting' => [
                'column' => 'dt_process',
                'direction' => 'desc'
            ],
            'items_per_page' => 20
        ],
        'invoices' => [
            'download_options' => [
                [
                    'invoiceId' => 33,
                    'printFormId' => 34289,
                    'fileType' => 'pdf',
                ],
            ]
        ],
        'payments' => [
            'default_sorting' => [
                'column' => 'document_no',
                'direction' => 'desc'
            ],
            'items_per_page' => 20,
            'download_options' => [
                [
                    'printFormId' => 3638,
                    'fileType' => 'pdf',
                    'type' => 'default'
                ]
            ]
        ],
        'contracts' => [
            'default_sorting' => [
                'column' => 'dt_activation',
                'direction' => 'desc'
            ],
            'items_per_page' => 20,
            'download_options' => [
                [
                    'printFormId' => 2018,
                    'fileType' => 'pdf',
                    'type' => 'default'
                ]
            ]
        ],
        'shipping_invoices' => [
            'default_sorting' => [
                'column' => 'document_no',
                'direction' => 'desc'
            ],
            'items_per_page' => 20,
            'download_options' => [
                [
                    'printFormId' => 5478,
                    'fileType' => 'pdf',
                    'type' => 'default'
                ]
            ]
        ]
    ]
];
