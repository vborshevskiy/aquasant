<?php

use SoloCabinet\Service\Ultima\AllReservesService;
use SoloCabinet\Service\Ultima\InvoicesService;
use SoloCabinet\Service\Ultima\BonusService;
use SoloCabinet\Entity\Ultima\DocumentDownloadOptions;
use SoloCabinet\Service\Ultima\ReservesService;
use SoloCabinet\Service\Ultima\CabinetService;
use SoloCabinet\Data\GoodInfoTable;
use SoloCabinet\Entity\GoodInfo;

return array(
    'controller_plugins' => array(
        'invokables' => array(
            'cabinet' => '\SoloCabinet\Controller\Plugin\Cabinet',
            'notify' => '\SoloCabinet\Controller\Plugin\Notify'
        )
    ),
    'service_manager' => array(
        'aliases' => array(
            'cabinet_service' => '\SoloCabinet\Service\CabinetService',
            'docs-service' => '\SoloCabinet\Service\DocsService',
            'notify' => '\SoloCabinet\Service\NotifyService'
        ),
        'factories' => array(
            '\SoloCabinet\Service\CabinetService' => function ($sm) {
                return new CabinetService();
            },
            '\SoloCabinet\Entity\Reserve' => function ($sm) {
                return new \SoloCabinet\Entity\Ultima\Reserve();
            },
            '\SoloCabinet\Entity\ReserveGood' => function ($sm) {
                return new \SoloCabinet\Entity\Ultima\ReserveGood();
            },
            '\SoloCabinet\Data\GoodInfoTable' => function ($sm) {
                $gateway = new GoodInfoTable();
                $gateway->setResultSetPrototype(new GoodInfo());
                return $gateway;
            },
            '\SoloCabinet\Service\AllReservesService' => function ($sm) {
                return new AllReservesService();
            },
            '\SoloCabinet\Service\ReservesService' => function ($sm) {
                $serv = new ReservesService();
                return $serv;
            },
            '\SoloCabinet\Service\InvoicesService' => function ($sm) {
                $serv = new InvoicesService();
                $config = $sm->get('Config');
                if (isset($config['cabinet']) && isset($config['cabinet']['invoices'])) {
                    $settings = $config['cabinet']['invoices'];
                    if (isset($settings['download_options']) && is_array($settings['download_options'])) {
                        foreach ($settings['download_options'] as $option) {
                            $options = new DocumentDownloadOptions($option['printFormId'], $option['fileType']);
                            $type = (int)(isset($option['invoiceId']) ? trim($option['invoiceId']) : 0);
                            $serv->setDownloadOptions($options, $type);
                        }
                    }
                }
                return $serv;
            },
            '\SoloCabinet\Service\BonusService' => function ($sm) {
                $serv = new BonusService();
                return $serv;
            },
        ),
        'shared' => [
            '\SoloCabinet\Entity\Reserve' => false,
            '\SoloCabinet\Entity\ReserveGood' => false
        ]
    )
);
