<?php

namespace SoloCabinet\Service;

use SoloCabinet\Entity\ListSorting;
use Solo\Cookie\Cookie;

trait ProvidesListing {

	/**
	 *
	 * @var ListSorting
	 */
	private $defaultListSorting = null;

	/**
	 *
	 * @var integer
	 */
	private $defaultItemsPerPage = 0;

	/**
	 *
	 * @param ListSorting $sorting        	
	 */
	public function setDefaultListSorting(ListSorting $sorting) {
		$this->defaultListSorting = $sorting;
	}

	/**
	 *
	 * @param integer $itemsPerPage        	
	 * @throws \InvalidArgumentException
	 */
	public function setDefaultItemsPerPage($itemsPerPage) {
		if (!is_integer($itemsPerPage)) {
			throw new \InvalidArgumentException('Items per page must be integer');
		}
		$this->defaultItemsPerPage = $itemsPerPage;
	}

	public function getDefaultItemsPerPage() {
		return $this->defaultItemsPerPage;
	}

	/**
	 *
	 * @param string $column        	
	 * @param string $direction        	
	 * @return \SoloCabinet\Entity\ListSorting
	 */
	public function createListSorting($column = null, $direction = null) {
		if (null !== $this->defaultListSorting) {
			$sorting = $this->defaultListSorting;
		} else {
			$sorting = new ListSorting();
		}
		if (null !== $column) {
			$sorting->setColumn($column);
			$sorting->setDirection($direction);
		}
		return $sorting;
	}

	/**
	 *
	 * @return \SoloCabinet\Entity\ListSorting
	 */
	public function getListSorting() {
		$sorting = $this->createListSorting();
		if (Cookie::exists($this->getListSortingCookieName())) {
			$sorting = unserialize(Cookie::get($this->getListSortingCookieName()));
		}
		return $sorting;
	}

	/**
	 *
	 * @param ListSorting $sorting        	
	 */
	public function setListSorting(ListSorting $sorting) {
		Cookie::set($this->getListSortingCookieName(), serialize($sorting));
	}

	/**
	 *
	 * @return string
	 */
	private function getListSortingCookieName() {
		return get_called_class() . '\\Sorting';
	}

}

?>