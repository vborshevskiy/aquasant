<?php

namespace SoloCabinet\Service\Ultima;

use Solo\ServiceManager\ServiceLocatorAwareService;
use Solo\ServiceManager\Result;
use SoloERP\WebService\Reader\UltimaObjectReader;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloCabinet\Entity\Ultima\Reserve;
use SoloCabinet\Service\Ultima\Result\DeleteReserveResult;
use SoloCabinet\Service\Ultima\Result\JoinReservesResult;
use SoloERP\WebService\Reader\UltimaListReader;
use SoloCabinet\Service\Ultima\ProvidesReserves;
use SoloCabinet\Entity\DocumentsFilterOptions;
use SoloCabinet\Entity\Ultima\JoinReserveOptions;
use SoloCabinet\Entity\Ultima\ReserveGood;
use SoloIdentity\Entity\Ultima\DeliveryAddresses;
use SoloCabinet\Entity\Ultima\ReserveChildDocument;

class ReservesService extends ServiceLocatorAwareService {
	
	use ProvidesReserves;
	
	use\SoloCache\Service\ProvidesCache;

	public function getDeliveryAdresses($securityKey, $agentId = null) {
		$wm = $this->getWebMethod('GetDeliveryAddresses');
		$wm->addPar('SecurityKey', $securityKey);
		// $wm->addPar('AgentId', $agentId);
		$response = $wm->call();
		
		$result = new Result();
		
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaJsonListReader($response);
			//print_r($response->getContent());
			foreach ($rdr as $adress) {
				$item = new DeliveryAddresses();
				$item->setId(intval($adress['Id']));
				$item->setAdress($adress['Adress']);
				$item->setLatitude($adress['Latitude']);
				$item->setLongitude($adress['Longitude']);
				
				$items[$adress->Id] = $item;
			}
			$result->items = $items;
		}
		return $result;
	}

	/**
	 *
	 * @param integer $userId        	
	 * @param integer $agentId        	
	 * @param integer $documentId        	
	 * @param string $priceAgentId        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getReserve($documentId, $agentId = null) {
		$wm = $this->getWebMethod('GetReserveInfo');
		$wm->addPar('Id', $documentId);
		if ($agentId > 0) {
			$wm->addPar('AgentId', $agentId);
		}
		// print json_encode($wm->getPars()); exit();
		$response = $wm->call();
		//print_r($response); exit();
		if (isset($_GET['debug'])) {
			echo '<pre>', print_r($response), '</pre>';
		}
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaJsonListReader($response);
			$item = $this->newReserve();
			$item->setDocumentNumber(intval($documentId));
			$item->setAmount(intval($rdr->getValue("Amount")));
			$item->setCreationDate(trim($rdr->getValue("CreationDate")));
			$item->setDeadReserveDateTime(trim($rdr->getValue("DeadDate")));
			$item->setGroup(trim($rdr->getValue("Group")));
			$item->setIsDelivery('delivery' == $rdr->getValue("ObtainMethod"));
			if ($rdr->getValue('ObtainMethod') == 'delivery') {
				$item->setDeliveryAddressID(intval($rdr->getValue('Delivery')->AddressId));
				$item->setDeliveryComments(trim($rdr->getValue('Delivery')->Comments));
				$item->setDeliveryDate(trim($rdr->getValue('Delivery')->Date));
				$item->setDeliveryTimeId(intval($rdr->getValue('Delivery')->TimeId));
				$item->setDeliveryAddressDescription(trim($rdr->getValue('Delivery')->DeliveryAddress));
				if ($rdr->getValue('Delivery')->Option == 'hybrid') {
					$item->setDeliveryCost(floatval($rdr->getValue('Delivery')->LogisticCompanyCost));
				} else {
					$deliveryCost = floatval($rdr->getValue('Delivery')->Cost);
					if (!$deliveryCost && $rdr->getValue('Delivery')) {
						$deliveryCost = $this->getDeliveryCost(intval($documentId), '', $rdr->getValue('Delivery'));
					}
					$item->setDeliveryCost($deliveryCost);
				}
				$item->setDeliveryNeedLifting(intval($rdr->getValue('Delivery')->NeedLifting));
				$item->isUndefinedDeliveryArea((bool)$rdr->getValue('Delivery')->UndefinedDeliveryArea);
			}
			if ($rdr->getValue('ChildDocuments') && is_array($rdr->getValue('ChildDocuments'))) {
				foreach ($rdr->getValue('ChildDocuments') as $childDocumentData) {
					$childDocument = new ReserveChildDocument();
					$childDocument->setId($childDocumentData->ID);
					$childDocument->setTypeId($childDocumentData->TypeID);
					$childDocument->setTypeName($childDocumentData->TypeName);
					$item->addChildDocument($childDocument);
				}
			}
			$item->setStoreId(intval($rdr->getValue('ReserveStoreId')));
			$item->setChargedBonusAmount($rdr->getValue('ChargedBonusAmount'));
			$item->setUsedBonusAmount(intval($rdr->getValue('UsedBonusAmount')));
			if ($rdr->getValue('PaymentTypeId')) {
				$item->setPayTypeId($rdr->getValue('PaymentTypeId'));
			}
			$item->isPayed($rdr->getValue('IsPayed'));
			$item->setComissionPercent($rdr->getValue('Percent'));
			$item->setStatusId($rdr->getValue('StatusId'));
			$item->setComment($rdr->getValue('Comment'));
			// $item->isDeliveryByTransportCompany($rdr->getValue('IsTK'));
			
			$item->setManagerId($rdr->getValue('ManagerId'));
			if ($rdr->getValue('ContactPhone')) {
				$item->setContactPhone($rdr->getValue('ContactPhone'));
			}
			if ($rdr->getValue('ContactEmail')) {
				$item->setContactEmail($rdr->getValue('ContactEmail'));
			}
			if ($rdr->getValue('ContactName')) {
				$item->setContactName($rdr->getValue('ContactName'));
			}
			
			$wm = $this->getWebMethod('GetReserveArticles');
			$wm->addPar('Id', $documentId);
			if ($agentId > 0) {
				$wm->addPar('AgentId', $agentId);
			}
			$response = $wm->call();
			if ($response->hasError()) {
				$result->setError($response->getError());
			} else {
				$resInfo = new UltimaJsonListReader($response);
				if (isset($_GET['debug'])) {
					echo '<pre>', print_r($resInfo), '</pre>';
					die();
				}
				foreach ($resInfo as $data) {
					
					$good = new ReserveGood();
					$good->setId(intval($data['Id']));
					$good->setQuantity(intval($data['Quantity']));
					$good->setStoreQuantity(intval($data['StoreQuantity']));
					$good->setPrice(intval($data['Price']));
					$good->setAmount(intval($data['Amount']));
					$item->addGood($good);
				}
			}
			
			$result->item = $item;
		}
		return $result;
	}

	/**
	 *
	 * @param integer $orderId        	
	 * @param string $securityKey        	
	 * @param array $deliveryInfo        	
	 * @return \SoloDelivery\Service\Ultima\Result\GetDeliveryCostResult
	 */
	public function getDeliveryCost($orderId, $securityKey, $deliveryInfo) {
		$result = 0;
		
		$wm = $this->getWebMethod('GetDeliveryCost');
		$wm->addPar('ReserveId', $orderId);
		$wm->addPar('SecurityKey', $securityKey);
		$wm->addPar('Delivery', $deliveryInfo);
		$response = $wm->call();
		if (!$response->hasError()) {
			$row = new UltimaJsonListReader($response);
			$result = floatval($row->getValue('Value'));
		}
		
		return $result;
	}

	/**
	 *
	 * @param integer $userId        	
	 * @param integer $agentId        	
	 * @param integer $documentId        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getReserveSimple($userId, $agentId, $documentId) {
		$filterOptions = new DocumentsFilterOptions();
		$filterOptions->setDocumentNumber($documentId);
		$filterOptions->setPage(1);
		$filterOptions->setItemsPerPage(1);
		$enumResult = $this->enumGenericReserves($userId, $agentId, Reserve::TYPE_ALL, $filterOptions);
		
		$result = new Result();
		if (!$enumResult->success()) {
			$result->setError($enumResult->getErrorCode());
		} else {
			$result->item = $enumResult->items[0];
		}
		return $result;
	}

	/**
	 *
	 * @param int $userId        	
	 * @param int $agentId        	
	 * @param \SoloCabinet\Entity\Ultima\Reserve $reserve        	
	 * @return \SoloCabinet\Service\Ultima\Result\JoinReservesResult
	 */
	public function updateReserve($securityKey, $agentId, $reserveId, $articles, $reserveStoreId, $bonusAmount, $delivery) {
		$wm = $this->getWebMethod("UpdateReserve");
		$wm->addPar('SecurityKey', $securityKey);
		$wm->addPar('Id', $reserveId);
		$wm->addPar('ReserveStoreId', $reserveStoreId);
		if (!is_null($delivery)) {
			$wm->addPar('ObtainMethod', \SoloOrder\Entity\Order\Ultima\Order::DELIVERY_OBTAIN_METHOD);
			$wm->addPar('Delivery', $delivery);
		}
		if ($agentId) {
			$wm->addPar('AgentId', $agentId);
		}
		if ($bonusAmount > 0) {
			$wm->addPar('BonusAmount', $bonusAmount);
		}
		$wm->addPar('Articles', $articles);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
			$result->setData($response->getData());
		}
		return $result;
	}

	public function updateReserveAgent($securityKey, $reserveId, $newAgentId) {
		$wm = $this->getWebMethod("UpdateReserveAgent");
		$wm->addPar('SecurityKey', $securityKey);
		$wm->addPar('Id', $reserveId);
		$wm->addPar('NewAgentId', $newAgentId);
		
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
			$result->setData($response->getData());
		}
		return $result;
	}

	/**
	 *
	 * @param integer $oldUserId        	
	 * @param integer $oldAgentId        	
	 * @param integer $userId        	
	 * @param integer $agentId        	
	 * @param integer $documentId        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function updateReserveOwner($oldUserId, $oldAgentId, $userId, $agentId, $documentId) {
		$wm = $this->getWebMethod("UpdateJBReserveAgent");
		$wm->setOldUserId($oldUserId);
		$wm->setOldAgentId($oldAgentId);
		$wm->setUserId($userId);
		$wm->setAgentId($agentId);
		$wm->setDocId($documentId);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		}
		return $result;
	}

	/**
	 *
	 * @param integer $userId        	
	 * @param integer $agentId        	
	 * @param mixed $docIds        	
	 * @return \SoloCabinet\Service\Ultima\Result\DeleteReserveResult
	 */
	public function removeReserve($securityKey, $agentId, $docIds) {
		$wm = $this->getWebMethod('DeleteReserve');
		$wm->addPar('SecurityKey', $securityKey); // var_dump($securityKey);
		$wm->addPar('Id', $docIds);
		
		$response = $wm->call();
		// var_dump($response); die;
		$result = new DeleteReserveResult();
		if ($response->hasError()) {
			$result->setError($response->getError());
		}
		return $result;
	}

	/**
	 *
	 * @param array $docIds        	
	 * @param JoinReserveOptions $options        	
	 * @throws \RuntimeException
	 * @return \SoloCabinet\Service\Ultima\Result\JoinReservesResult
	 */
	public function joinReserves(array $docIds, JoinReserveOptions $options) {
		if (!in_array($options->getPaymentDocumentId(), $docIds)) {
			throw new \RuntimeException('Specified payment document id not exists in joined documents');
		}
		if (!in_array($options->getDeliveryDocumentId(), $docIds)) {
			throw new \RuntimeException('Specified delivery document id not exists in joined documents');
		}
		
		$wm = $this->getWebMethod("UniteJBReserve");
		$wm->setUserId($options->getUserId());
		$wm->setAgentId($options->getAgentId());
		$wm->setDocIds($docIds);
		$wm->setPDoc($options->getPaymentDocumentId());
		$wm->setDDoc($options->getDeliveryDocumentId());
		$wm->setIsOldJur($options->noBonuses());
		$wm->setNoBonuses($options->isOldJur());
		$response = $wm->call();
		
		$result = new JoinReservesResult();
		if ($response->hasError()) {
			$result->setError($response->getError());
			$errorData = $response->getData();
			if (is_array($errorData) && sizeof($errorData) == 2 && is_array($errorData[0]) && is_array($errorData[1])) {
				$rdr = new UltimaListReader($response);
				$items = [];
				foreach ($rdr as $row) {
					$item = new \ArrayObject();
					$item->goodId = intval($row['GoodID']);
					$item->barcodeId = intval($row['BarcodeID']);
					$item->availQty = intval($row['AvailQuantity']);
					
					$items[$item->goodId] = $item;
				}
				$result->items = $items;
			} else {
				$result->setData($errorData);
			}
		} else {
			$rdr = new UltimaObjectReader($response);
			$result->documentId = intval($rdr->Doc);
			$result->agentId = intval($rdr->Agent);
		}
		return $result;
	}

	/**
	 *
	 * @return \SoloCabinet\Entity\Ultima\Reserve;
	 */
	public function newReserve() {
		return $this->getServiceLocator()->get('\SoloCabinet\Entity\Reserve');
	}

	/**
	 *
	 * @return \SoloCabinet\Entity\Ultima\ReserveGood
	 */
	public function newGood() {
		return $this->getServiceLocator()->get('\SoloCabinet\Entity\ReserveGood');
	}

	public function getReserveStatuses() {
		$wm = $this->getWebMethod('GetReserveStatuses');
		
		$response = $wm->call();
		
		$result = new Result();
		
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaJsonListReader($response);
			
			$result->statuses = $rdr->getValue('Statuses');
		}
		
		return $result;
	}

	/**
	 *
	 * @param integer $agentId        	
	 * @param integer $statusId        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getReservesCount($agentId = null, $statusId = null) {
		$wm = $this->getWebMethod('GetReservesCount');
		
		if ($agentId) {
			$wm->addPar('AgentId', $agentId);
		}
		
		if ($statusId) {
			$wm->addPar('StatusId', $statusId);
		}
		
		$response = $wm->call();
		
		$result = new Result();
		
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaJsonListReader($response);
			
			$result->reservesCount = $rdr->getValue('ReservesCount');
		}
		
		return $result;
	}

	/**
	 *
	 * @param integer $managerId        	
	 * @param array $statusIds        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getManagerReservesCount($managerId, array $statusIds = []) {
		$wm = $this->getWebMethod('GetManagerReservesCount');
		$wm->addPar('ManagerID', $managerId);
		$wm->addPar('StatusIds', $statusIds);
		
		$response = $wm->call();
		
		$result = new Result();
		
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaJsonListReader($response);
			
			$result->reservesCount = $rdr->getValue('ReservesCount');
		}
		
		return $result;
	}

	/**
	 *
	 * @param integer $reserveId        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getReserveDocuments($reserveId) {
		$wm = $this->getWebMethod('GetReservePrintForms');
		
		$wm->addPar('IdDoc', $reserveId);
		
		$response = $wm->call();
		
		$result = new Result();
		
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaJsonListReader($response);
			
			$result->documents = $rdr->getValue('PrintForms');
		}
		
		return $result;
	}

	public function getReservePrintForm($reserveId, $printFormId, $agentId = null) {
		$wm = $this->getWebMethod('GetReservePrintFormData');
		
		$wm->addPar('IdDoc', $reserveId);
		$wm->addPar('IdForm', $printFormId);
		
		if ($agentId) {
			$wm->addPar('AgentId', $agentId);
		}
		
		$response = $wm->call();
		
		$result = new Result();
		
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaJsonListReader($response);
			
			$result->printForm = $rdr->getValue('PrintFormData');
		}
		
		return $result;
	}

}

?>