<?php

namespace SoloCabinet\Service\Ultima;

use Solo\ServiceManager\ServiceLocatorAwareService;
use SoloCabinet\Service\Ultima\Result\WarrantyStatusResult;
use SoloERP\WebService\Reader\UltimaObjectReader;
use SoloCabinet\Entity\Ultima\WarrantyStatus;

class WarrantyService extends ServiceLocatorAwareService {

	/**
	 *
	 * @param string $serial        	
	 * @return \SoloCabinet\Service\Ultima\Result\WarrantyStatusResult
	 */
	public function getStatusBySerial($serial) {
		$wm = $this->getWebMethod('GetWarrantyStatusBySerial');
		$wm->setSerialNumber($serial);
		$response = $wm->call();
		$result = new WarrantyStatusResult();
		if ($response->hasError()) {
			$result->setError($response->getError());
			$result->setData($response->getData());
		} else {
			$rdr = new UltimaObjectReader($response);
			
			$item = new WarrantyStatus();
			$item->setDocumentID(intval($rdr->DocumentID));
			$item->setDocumentNumber(intval($rdr->DocumentNo));
			$item->setGoodId(intval($rdr->GoodID));
			$item->setGoodName(trim($rdr->GoodName));
			$item->setGoodStatus(trim($rdr->GoodStatus));
			$item->setAgentId(intval($rdr->AgentID));
			
			$dateOnly = explode('T', $rdr->DocumentDate);
			$dateOnlyArray = explode('-', $dateOnly[0]);
			$documentDate = trim($dateOnlyArray[2] . '.' . $dateOnlyArray[1] . '.' . $dateOnlyArray[0]);
			if ('..' != $documentDate) {
				$item->setDocumentDate($documentDate);
			}
			
			$result->warranty = $item;
		}
		return $result;
	}

}

?>