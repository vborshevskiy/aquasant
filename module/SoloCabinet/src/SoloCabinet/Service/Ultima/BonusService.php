<?php

namespace SoloCabinet\Service\Ultima;

use Solo\ServiceManager\ServiceLocatorAwareService;
use SoloCabinet\Service\ProvidesListing;
use SoloCabinet\Entity\DocumentsFilterOptions;
use Solo\ServiceManager\Result;
use SoloERP\WebService\Reader\UltimaObjectReader;
use SoloCabinet\Entity\DocumentsStatistics;
use SoloERP\Service\ProvidesWebservice;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloCabinet\Entity\Ultima\BonusHistory;

final class BonusService extends ServiceLocatorAwareService {
	
//	use ProvidesListing {
//		setDefaultListSorting as public setHistoryDefaultListSorting;
//		createListSorting as public createHistoryListSorting;
//		getListSorting as public getHistoryListSorting;
//		setListSorting as public setHistoryListSorting;
//		setDefaultItemsPerPage as public setHistoryDefaultItemsPerPage;
//		getDefaultItemsPerPage as public setHistoryDefaultItemsPerPage;
//	}
	use ProvidesWebservice;

	/**
     * Gets history for bonus movements
     *
     * @param string $securityKey        	
     * @return \Solo\ServiceManager\Result
     */
    public function getBonusHistory($securityKey) {
        $wm = $this->getWebMethod('GetBonusHistory');
        $wm->addPar('SecurityKey', $securityKey);
        $response = $wm->call();

        $result = new Result();
        if ($response->hasError()) {
            $result->setError($response->getError());
        } else {
            $rdr = new UltimaJsonListReader($response);
            $items = [];
            foreach ($rdr as $row) {
                $item = new BonusHistory();
                $item->setId(intval($row->offsetGet('Id')));
                $item->setInAmount(floatval($row->offsetGet('IncomeBonus')));
                $item->setOutAmount(floatval($row->offsetGet('OutcomeBonus')));
                $item->setAgentId(intval($row->offsetGet('AgentId')));
                $item->setDate(\SoloCatalog\Service\Helper\DateHelper::parseJsonDate($row->offsetGet('DocumentDate')));
                
                $items[$item->getId()] = $item;
            }
            $result->items = $items;
        }
        return $result;
    }

    /**
	 *
	 * @param integer $userId        	
	 * @param integer $agentId        	
	 * @param DocumentsFilterOptions $filterOptions        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getStatistics($userId, $agentId, DocumentsFilterOptions $filterOptions) {
		$wm = $this->getWebMethod("GetGeneralBonusInfo");
		$wm->setUserId($userId);
		$wm->setAgentId($agentId);
		if ($filterOptions->hasCostFrom()) {
			$wm->setCostFrom($filterOptions->getCostFrom());
		}
		if ($filterOptions->hasCostTo()) {
			$wm->setCostTo($filterOptions->getCostTo());
		}
		if ($filterOptions->hasDateFrom()) {
			$wm->setDateFrom($filterOptions->getDateFrom());
		}
		if ($filterOptions->hasDateTo()) {
			$wm->setDateTo($filterOptions->getDateTo());
		}
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaObjectReader($response);
			$stats = new DocumentsStatistics();
			$stats->setTotalCount(intval($rdr->ALL_NUM));
			$stats->setDateLimits(trim($rdr->MIN_DATE), trim($rdr->MAX_DATE));
			$stats->setCostLimits(floatval($rdr->MIN_COST), floatval($rdr->MAX_COST));
			
			$result->stats = $stats;
		}
		return $result;
	}

}

?>