<?php

namespace SoloCabinet\Service\Ultima;

use Solo\ServiceManager\ServiceLocatorAwareService;
use Solo\ServiceManager\Result;
use SoloCabinet\Service\Ultima\ProvidesDocumentDownloads;
use SoloERP\Service\ProvidesWebservice;
use SoloERP\WebService\Reader\UltimaJsonListReader;

final class InvoicesService extends ServiceLocatorAwareService {

    const SBERBANK_INVOICE_ID = 33;

    use ProvidesDocumentDownloads;

    use ProvidesWebservice;

    public function getInvoice($securityKey, $agentId, $documentNumber, $invoiceId) {
        $wm = $this->getWebMethod('GetDocumentPrintFormData');
        $downloadOptions = $this->getDownloadOptions($invoiceId);
        $wm->addPar('SecurityKey', $securityKey);
        if ($agentId > 0) {
            $wm->addPar('AgentId', $agentId);
        }
        $wm->addPar('DocumentId', $documentNumber);
        $wm->addPar('DataType', $downloadOptions->getFileType());
        $wm->addPar('PrintFormId', $downloadOptions->getPrintFormId());
        $wm->addPar('Params', [
            [
                'Key' => 'tst',
                'Value' => 'text',
            ],
        ]);
        $response = $wm->call();

        $result = new Result();
        if ($response->hasError()) {
            $result->setError($response->getError());
        } else {
            $rdr = new UltimaJsonListReader($response);
            $result->document = base64_decode($rdr->getValue('Data'));
        }
        return $result;
    }

}
