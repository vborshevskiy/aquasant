<?php

namespace SoloCabinet\Service\Ultima;

use SoloERP\Service\ProvidesWebservice;
use Solo\ServiceManager\ServiceLocatorAwareService;
use Solo\ServiceManager\Result;
use SoloERP\WebService\Reader\UltimaListReader;
use SoloERP\WebService\Reader\UltimaObjectReader;
use SoloCabinet\Entity\Ultima\NotifyEvent;
use SoloCabinet\Entity\Ultima\NotifierPhone;
use SoloCabinet\Entity\Ultima\NotifierEmail;
use SoloCabinet\Entity\Ultima\GoodsIncomeNotifier;
use SoloCabinet\Entity\Ultima\IncomeGood;

class NotifyService extends ServiceLocatorAwareService {
	
	use ProvidesWebservice;

	/**
	 *
	 * @var \SoloCabinet\Data\GoodInfoTable
	 */
	private $goodInfoTable = null;

	public function getGoodInfoTable() {
		return $this->goodInfoTable;
	}

	public function setGoodInfoTable($goodInfoTable) {
		$this->goodInfoTable = $goodInfoTable;
	}

	/**
	 *
	 * @return \Solo\ServiceManager\Result
	 */
	public function getNotifyEvents() {
		$wm = $this->getWebMethod('GetAgentNotifyEventsJB');
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$items = [];
			$rdr = new UltimaListReader($response);
			foreach ($rdr as $row) {
				$item = new NotifyEvent();
				$item->setId(intval($row->ID));
				$item->setName(trim($row->Name));
				
				$items[$item->getId()] = $item;
			}
			$result->events = $items;
		}
		return $result;
	}

	/**
	 *
	 * @param integer $agentId        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getNotifyPhones($agentId) {
		$wm = $this->getWebMethod('GetAgentNotifyPhonesJB');
		$wm->setAgentId($agentId);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$items = [];
			$rdr = new UltimaListReader($response);
			foreach ($rdr as $row) {
				$notifier = new NotifierPhone();
				$notifier->setId(intval($row->ID));
				$notifier->setPhone(trim($row->Phone));
				$notifier->setPhonePrefix(trim($row->PhonePrefix));
				
				$notifyEvent = new NotifyEvent();
				$notifyEvent->setId(intval($row->NotifyEventID));
				$notifyEvent->setName(trim($row->NotifyEventName));
				$notifier->setNotifyEvent($notifyEvent);
				
				$items[$notifyEvent->getId()] = $notifier;
			}
			$result->notifiers = $items;
		}
		return $result;
	}

	/**
	 *
	 * @param integer $agentId        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getNotifyEmails($agentId) {
		$wm = $this->getWebMethod('GetAgentNotifyEmailsJB');
		$wm->setAgentId($agentId);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$items = [];
			$rdr = new UltimaListReader($response);
			foreach ($rdr as $row) {
				$notifier = new NotifierEmail();
				$notifier->setId(intval($row->ID));
				$notifier->setEmail(trim($row->Email));
				
				$notifyEvent = new NotifyEvent();
				$notifyEvent->setId(intval($row->NotifyEventID));
				$notifyEvent->setName(trim($row->NotifyEventName));
				$notifier->setNotifyEvent($notifyEvent);
				
				$items[$notifyEvent->getId()] = $notifier;
			}
			$result->emails = $items;
		}
		return $result;
	}

	/**
	 *
	 * @param integer $agentId        	
	 * @param integer $eventId        	
	 * @param string $phone        	
	 * @param string $phonePrefix        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function addPhoneNotifier($agentId, $eventId, $phone, $phonePrefix) {
		$wm = $this->getWebMethod('AddAgentNotifyPhoneJB');
		$wm->setAgentId($agentId);
		$wm->setNotifyEventId($eventId);
		$wm->setPhone($phone);
		$wm->setPhonePrefix($phonePrefix);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		}
		return $result;
	}

	/**
	 *
	 * @param integer $agentId        	
	 * @param integer $eventId        	
	 * @param string $email        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function addEmailNotifier($agentId, $eventId, $email) {
		$wm = $this->getWebMethod('AddAgentNotifyEmailJB');
		$wm->setAgentId($agentId);
		$wm->setNotifyEventId($eventId);
		$wm->setEmail($email);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		}
		return $result;
	}

	/**
	 *
	 * @param integer $agentId        	
	 * @param integer $phoneNotifierId        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function deletePhoneNotifier($agentId, $phoneNotifierId) {
		$wm = $this->getWebMethod('DeleteAgentNotifyPhoneJB');
		$wm->setAgentId($agentId);
		$wm->setNotifyPhoneId($phoneNotifierId);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		}
		return $result;
	}

	/**
	 *
	 * @param integer $agentId        	
	 * @param integer $emailNotifierId        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function deleteEmailNotifier($agentId, $emailNotifierId) {
		$wm = $this->getWebMethod('DeleteAgentNotifyEmailJB');
		$wm->setAgentId($agentId);
		$wm->setNotifyEmailId($emailNotifierId);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		}
		return $result;
	}

	/**
	 *
	 * @param integer $agentId        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function deleteAllPhoneNotifiers($agentId) {
		$wm = $this->getWebMethod('DeleteAllAgentNotifyPhonesJB');
		$wm->setAgentId($agentId);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		}
		return $result;
	}

	/**
	 *
	 * @param integer $agentId        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function deleteAllEmailNotifiers($agentId) {
		$wm = $this->getWebMethod('DeleteAllAgentNotifyEmailsJB');
		$wm->setAgentId($agentId);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		}
		return $result;
	}

	/**
	 *
	 * @param integer $agentId        	
	 * @param integer $eventId        	
	 * @param string $phone        	
	 * @param string $phonePrefix        	
	 * @param string $email        	
	 * @param array $goods        	
	 * @param array $warehouses        	
	 * @param array $quantities        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function addGoodsIncomeNotify($agentId, $eventId, $phone, $phonePrefix, $email, $goods, $warehouses, $quantities) {
		$wm = $this->getWebMethod('AddAgentNotifyGoodsIncomeJB');
		$wm->setAgentId($agentId);
		$wm->setEmail($email);
		$wm->setNotifyEventId($eventId);
		$wm->setPhone($phone);
		$wm->setPhonePrefix($phonePrefix);
		$wm->setWarehouses($warehouses);
		$wm->setGoods($goods);
		$wm->setQuantities($quantities);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		}
		return $result;
	}

	/**
	 *
	 * @param integer $agentId        	
	 * @param integer $eventId        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getGoodsIncomeNotifiers($agentId, $eventId) {
		$wm = $this->getWebMethod('GetAgentNotifyGoodsIncomeJB');
		$wm->setAgentId($agentId);
		$wm->setNotifyEventId($eventId);
		$response = $wm->call();
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$respObj = new UltimaObjectReader($response);
			$goodsIncomeNotifier = new GoodsIncomeNotifier();
			$goods = array_combine($respObj->Goods[0], $respObj->Goods[1]);
			if (sizeof($goods) > 0) {
				$goodIds = array_keys($goods);
				$goodRows = $this->goodInfoTable->getGoods($goodIds);
				foreach ($goodRows as $gr) {
					$incomeGood = new IncomeGood();
					$incomeGood->setId(intval($gr->getGoodId()));
					$incomeGood->setName(trim($gr->getGoodName()));
					$incomeGood->setEngName(trim($gr->getGoodEngName()));
					
					$warehouses = [];
					array_walk($goods[$gr->getGoodId()][0], function ($val, $key) use(&$warehouses) {
						$warehouses[$key] = intval($val);
					});
					
					$incomeGood->setWarehouses($warehouses);
					$incomeGood->setQuantity(intval($goods[$gr->getGoodId()][1][0]));
					$goodsIncomeNotifier->addGood($incomeGood);
				}
			}
			$goodsIncomeNotifier->setEmail($respObj->Email);
			$goodsIncomeNotifier->setPhonePostfix($respObj->Phone);
			$goodsIncomeNotifier->setPhonePrefix($respObj->PhonePrefix);
			$result->setData($goodsIncomeNotifier);
		}
		return $result;
	}

	/**
	 *
	 * @param integer $agentId        	
	 * @param integer $eventId        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function deleteAllGoodsIncomeNotifiers($agentId, $eventId) {
		$wm = $this->getWebMethod('DeleteAllAgentNotifyGoodsIncomeJB');
		$wm->setAgentId($agentId);
		$wm->setNotifyEventId($eventId);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		}
		return $result;
	}

	/**
	 *
	 * @param integer $agentId        	
	 * @param integer $notifyEventId        	
	 * @param array $goods        	
	 * @param array $warehouses        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function deleteGoodsIncomeNotify($agentId, $notifyEventId, $goods, $warehouses) {
		$wm = $this->getWebMethod('DeleteAgentNotifyGoodsIncomeJB');
		$wm->setAgentId($agentId);
		$wm->setGoods($goods);
		$wm->setNotifyEventId($notifyEventId);
		$wm->setWarehouses($warehouses);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		}
		return $result;
	}

	/**
	 *
	 * @param integer $agentId        	
	 * @param integer $eventId        	
	 * @param string $phone        	
	 * @param string $phonePrefix        	
	 * @param string $email        	
	 * @param array $goods        	
	 * @param array $prices        	
	 * @param array $warehouses        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function addGoodsPricesNotify($agentId, $eventId, $phone, $phonePrefix, $email, $goods, $prices, $warehouses) {
		$wm = $this->getWebMethod('AddAgentNotifyGoodsPricesJB');
		$wm->setAgentId($agentId);
		$wm->setEmail($email);
		$wm->setGoods($goods);
		$wm->setNotifyEventId($eventId);
		$wm->setPhone($phone);
		$wm->setPhonePrefix($phonePrefix);
		$wm->setPrices($prices);
		$wm->setWarehouses($warehouses);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		}
		return $result;
	}

	/**
	 *
	 * @param integer $agentId        	
	 * @param integer $eventId        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function deleteAllGoodsPricesNotifiers($agentId, $eventId) {
		$wm = $this->getWebMethod('DeleteAllAgentNotifyGoodsPricesJB');
		$wm->setAgentId($agentId);
		$wm->setNotifyEventId($eventId);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		}
		return $result;
	}

}

?>
