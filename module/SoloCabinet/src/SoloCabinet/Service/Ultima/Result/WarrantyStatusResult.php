<?php

namespace SoloCabinet\Service\Ultima\Result;

use Solo\ServiceManager\Result;

class WarrantyStatusResult extends Result {

	/**
	 *
	 * @var integer
	 */
	const ERROR_GOOD_NOT_FOUND = 1301;

}

?>