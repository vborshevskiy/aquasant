<?php

namespace SoloCabinet\Service\Ultima\Result;

use Solo\ServiceManager\Result;

final class JoinReservesResult extends Result {

	/**
	 *
	 * @var integer
	 */
	const ERROR_SUBORDER_RESERVES_EXISTS = 602;

}

?>