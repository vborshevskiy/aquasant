<?php

namespace SoloCabinet\Service\Ultima\Result;

use Solo\ServiceManager\Result;

final class DeleteReserveResult extends Result {

	/**
	 *
	 * @var integer
	 */
	const ERROR_INVALID_RESERVE_OWNER = 501;

	/**
	 *
	 * @var integer
	 */
	const ERROR_RESERVE_DONT_EXISTS = 502;

}

?>