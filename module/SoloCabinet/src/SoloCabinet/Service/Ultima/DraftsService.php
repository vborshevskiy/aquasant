<?php

namespace SoloCabinet\Service\Ultima;

use Solo\ServiceManager\ServiceLocatorAwareService;
use SoloCabinet\Service\ProvidesListing;
use SoloCabinet\Entity\DocumentsFilterOptions;
use Solo\ServiceManager\Result;
use SoloERP\WebService\Reader\UltimaListReader;
use SoloCabinet\Entity\Ultima\Draft;
use SoloCabinet\Entity\Ultima\Reserve;
use SoloERP\WebService\Reader\UltimaObjectReader;
use SoloCabinet\Entity\DocumentsStatistics;

final class DraftsService extends ServiceLocatorAwareService {
	
	use ProvidesListing;

	/**
	 *
	 * @param integer $userId        	
	 * @param integer $agentId        	
	 * @param integer $priceAgentId        	
	 * @param DocumentsFilterOptions $options        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function enum($userId, $agentId, $priceAgentId, DocumentsFilterOptions $options) {
		$wm = $this->getWebMethod('GetDraftList');
		$wm->setUserId($userId);
		$wm->setAgentId($agentId);
		$wm->setPriceAgentId($priceAgentId);
		if ($options->hasCostFrom()) {
			$wm->setCostFrom($options->getCostFrom());
		}
		if ($options->hasCostTo()) {
			$wm->setCostTo($options->getCostTo());
		}
		if ($options->hasDateFrom()) {
			$wm->setDateFrom($options->getDateFrom());
		}
		if ($options->hasDateTo()) {
			$wm->setDateTo($options->getDateTo());
		}
		if ($options->hasDocumentNumber()) {
			$wm->setDocumentNo($options->getDocumentNumber());
		}
		$wm->setPage($options->getPage());
		if ($options->hasItemsPerPage()) {
			$wm->setRecPerPage($options->getItemsPerPage());
		}
		if ($options->hasSortColumn()) {
			$wm->setSort($options->getSortColumn());
		}
		if ($options->hasSortDirection()) {
			$wm->setSortDir($options->getSortDirection());
		}
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaListReader($response);
			$items = [];
			foreach ($rdr as $row) {
				$item = new Draft();
				$item->setId(intval($row->ID));
				$item->setNumber(intval($row->DocumentNo));
				$item->setAmount(floatval($row->Amount));
				$item->setCreateDate(trim($row->CreateDate));
				$item->setIsDelivery(1 == intval($row->IsDelivery));
				$item->setWarehouseId(intval($row->WarehouseID));
				
				$items[$item->getId()] = $item;
			}
			$result->items = $items;
		}
		return $result;
	}

	/**
	 *
	 * @param integer $userId        	
	 * @param integer $agentId        	
	 * @param DocumentsFilterOptions $options        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getStatistics($userId, $agentId, DocumentsFilterOptions $options) {
		$wm = $this->getWebMethod("GetGeneralReserveInfo");
		$wm->setUserId($userId);
		$wm->setAgentId($agentId);
		if ($options->hasCostFrom()) {
			$wm->setCostFrom($options->getCostFrom());
		}
		if ($options->hasCostTo()) {
			$wm->setCostTo($options->getCostTo());
		}
		if ($options->hasDateFrom()) {
			$wm->setDateFrom($options->getDateFrom());
		}
		if ($options->hasDateTo()) {
			$wm->setDateTo($options->getDateTo());
		}
		$wm->setTypeReserve(Reserve::TYPE_DRAFT);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaObjectReader($response);
			$stats = new DocumentsStatistics();
			$stats->setTotalCount(intval($rdr->ALL_NUM));
			$stats->setDateLimits(trim($rdr->MIN_DATE), trim($rdr->MAX_DATE));
			$stats->setCostLimits(floatval($rdr->MIN_COST), floatval($rdr->MAX_COST));
			
			$result->stats = $stats;
		}
		return $result;
	}

	/**
	 *
	 * @param integer $userId        	
	 * @param integer $agentId        	
	 * @param integer $documentId        	
	 * @param string $priceAgentId        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getDraft($userId, $agentId, $documentId, $priceAgentId = null) {
		return $this->getServiceLocator()->get(__NAMESPACE__ . '\\ReserveService')->getReserve($userId, $agentId, $documentId, $priceAgentId);
	}

	/**
	 *
	 * @param integer $userId        	
	 * @param integer $agentId        	
	 * @param mixed $docsIds        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function removeDraft($userId, $agentId, $docsIds) {
		return $this->getServiceLocator()->get(__NAMESPACE__ . '\\ReserveService')->removeReserve($userId, $agentId, $docsIds);
	}

	/**
	 *
	 * @param integer $userId        	
	 * @param integer $agentId        	
	 * @param integer $docId        	
	 * @param array $goods        	
	 * @param array $quantities        	
	 * @param array $prices        	
	 * @param boolean $isDelivery        	
	 * @param integer $deliveryGoodID        	
	 * @param integer $timeId        	
	 * @param string $date        	
	 * @param integer $addressId        	
	 * @param integer $warehouse        	
	 * @param integer $outcomeWarehouse        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function updateDraft($userId, $agentId, $docId, $goods, $quantities, $prices = [], $isDelivery = false, $deliveryGoodID = -1, $timeId = null, $date = null, $addressId = null, $warehouse = null, $outcomeWarehouse = 1) {
		$wm = $this->getWebMethod("UpdateJBDraft");
		$wm->setUserId($userId);
		$wm->setAgentId($agentId);
		$wm->setDocId($docId);
		$wm->setGoods($goods);
		$wm->setQuantities($quantities);
		$wm->setDelivery($isDelivery);
		$wm->setDeliveryGoodId($deliveryGoodID);
		$wm->setTimeId($timeId);
		$wm->setDate($date);
		$wm->setAddressId($addressId);
		$wm->setWarehouseReserve($warehouse);
		$wm->setWarehouseOutcome($outcomeWarehouse);
		$wm->setPrices($prices);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
			$result->setData($response->getData());
		}
		return $result;
	}

}

?>