<?php

namespace SoloCabinet\Service\Ultima;

use Solo\ServiceManager\ServiceLocatorAwareService;
use SoloCabinet\Service\ProvidesListing;
use SoloCabinet\Entity\DocumentsFilterOptions;
use Solo\ServiceManager\Result;
use SoloERP\WebService\Reader\UltimaObjectReader;
use SoloCabinet\Entity\DocumentsStatistics;
use SoloERP\Service\ProvidesWebservice;
use SoloERP\WebService\Reader\UltimaListReader;
use SoloCabinet\Entity\Ultima\BalanceHistory;

final class BalanceService extends ServiceLocatorAwareService {
	
	use ProvidesListing {
		setDefaultListSorting as public setHistoryDefaultListSorting;
		createListSorting as public createHistoryListSorting;
		getListSorting as public getHistoryListSorting;
		setListSorting as public setHistoryListSorting;
		setDefaultItemsPerPage as public setHistoryDefaultItemsPerPage;
		getDefaultItemsPerPage as public setHistoryDefaultItemsPerPage;
	}
	
	use ProvidesWebservice;

	/**
	 *
	 * @param integer $userId        	
	 * @param integer $agentId        	
	 * @param DocumentsFilterOptions $filterOptions        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getHistory($userId, $agentId, DocumentsFilterOptions $filterOptions) {
		$wm = $this->getWebMethod("GetJBBalanceList");
		$wm->setUserId($userId);
		$wm->setAgentId($agentId);
		if ($filterOptions->hasCostFrom()) {
			$wm->setCostFrom($filterOptions->getCostFrom());
		}
		if ($filterOptions->hasCostTo()) {
			$wm->setCostTo($filterOptions->getCostTo());
		}
		if ($filterOptions->hasDateFrom()) {
			$wm->setDateFrom($filterOptions->getDateFrom());
		}
		if ($filterOptions->hasDateTo()) {
			$wm->setDateTo($filterOptions->getDateTo());
		}
		if ($filterOptions->hasDocumentNumber()) {
			$wm->setDocumentNo($filterOptions->getDocumentNumber());
		}
		$wm->setPage($filterOptions->getPage());
		if ($filterOptions->hasItemsPerPage()) {
			$wm->setRecPerPage($filterOptions->getItemsPerPage());
		}
		if ($filterOptions->hasSortColumn()) {
			$wm->setSort($filterOptions->getSortColumn());
		}
		if ($filterOptions->hasSortDirection()) {
			$wm->setSortDir($filterOptions->getSortDirection());
		}
		$response = $wm->call();
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaListReader($response);
			$items = [];
			foreach ($rdr as $row) {
				$item = new BalanceHistory();
				$item->setDescription(trim($row->DOCUMENT_DESCR));
				$item->setNumber(intval($row->DOCUMENT_NO));
				$item->setId(intval($row->DOCUMENT_ID));
				$item->setInAmount(floatval($row->AMOUNT_IN));
				$item->setIsExpense(1 == intval($row->IS_EXPENSE));
				$item->setOutAmount(floatval($row->AMOUNT_OUT));
				$item->setDate(strtotime($row->DOCUMENT_DATE));
				$item->setIsReserve(1 == intval($row->IS_RESERVE));
				
				$items[$item->getId()] = $item;
			}
			$result->items = $items;
		}
		return $result;
	}

	/**
	 *
	 * @param integer $userId        	
	 * @param integer $agentId        	
	 * @param DocumentsFilterOptions $filterOptions        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getStatistics($userId, $agentId, DocumentsFilterOptions $filterOptions) {
		$wm = $this->getWebMethod("GetGeneralBalanceInfo");
		$wm->setUserId($userId);
		$wm->setAgentId($agentId);
		if ($filterOptions->hasCostFrom()) {
			$wm->setCostFrom($filterOptions->getCostFrom());
		}
		if ($filterOptions->hasCostTo()) {
			$wm->setCostTo($filterOptions->getCostTo());
		}
		if ($filterOptions->hasDateFrom()) {
			$wm->setDateFrom($filterOptions->getDateFrom());
		}
		if ($filterOptions->hasDateTo()) {
			$wm->setDateTo($filterOptions->getDateTo());
		}
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaObjectReader($response);
			$stats = new DocumentsStatistics();
			$stats->setTotalCount(intval($rdr->ALL_NUM));
			$stats->setDateLimits(trim($rdr->MIN_DATE), trim($rdr->MAX_DATE));
			$stats->setCostLimits(floatval($rdr->MIN_COST), floatval($rdr->MAX_COST));
			
			$result->stats = $stats;
		}
		return $result;
	}

}

?>