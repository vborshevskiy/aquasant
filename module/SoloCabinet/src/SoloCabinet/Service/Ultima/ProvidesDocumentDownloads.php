<?php

namespace SoloCabinet\Service\Ultima;

use SoloCabinet\Entity\Ultima\DocumentDownloadOptions;

trait ProvidesDocumentDownloads {

    /**
     *
     * @var array
     */
    private $downloadOptions = [];

    /**
     *
     * @param DocumentDownloadOptions $options        	
     * @param integer $type        	
     */
    public function setDownloadOptions(DocumentDownloadOptions $options, $type) {
        $this->downloadOptions[$type] = $options;
    }

    /**
     * 
     * @param integer $type
     * @return DocumentDownloadOptions
     * @throws \RuntimeException
     */
    public function getDownloadOptions($type) {
        if (array_key_exists($type, $this->downloadOptions)) {
            return $this->downloadOptions[$type];
        } elseif (array_key_exists(0, $this->downloadOptions)) {
            return $this->downloadOptions[0];
        }
        throw new \RuntimeException('Can\'t find download options');
    }

}
