<?php

namespace SoloCabinet\Service\Ultima;

use Solo\ServiceManager\ServiceLocatorAwareService;
use SoloERP\Service\ProvidesWebservice;
use Solo\ServiceManager\Result;
use SoloCabinet\Entity\Ultima\SummaryStatistics;
use SoloERP\WebService\Reader\UltimaObjectReader;

class CabinetService extends ServiceLocatorAwareService {
	
	use ProvidesWebservice;

	/**
	 *
	 * @param integer $userId        	
	 * @param integer $agentId        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getGAgentDocsInfo($userId, $agentId) {
		$wm = $this->getWebMethod('GetGeneralAgentDocsInfo');
		$wm->setUserId($userId);
		$wm->setAgentId($agentId);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaObjectReader($response);
			$stats = new SummaryStatistics();
			$stats->setAllReservesCount(intval($rdr->AllNum));
			$stats->setActiveReservesCount(intval($rdr->ActiveNum));
			$stats->setBalance(floatval($rdr->Balance));
			$stats->setDraftsCount(intval($rdr->DraftsNum));
			$stats->setFinishedReservesCount(intval($rdr->FinishedNum));
			$stats->setProcessedReservesCount(intval($rdr->ProcessedNum));
			
			$result->stats = $stats;
		}
		return $result;
	}

}

?>