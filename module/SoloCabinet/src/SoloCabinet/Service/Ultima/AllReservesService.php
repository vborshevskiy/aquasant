<?php

namespace SoloCabinet\Service\Ultima;

use Solo\ServiceManager\ServiceLocatorAwareService;
use SoloCabinet\Service\ProvidesListing;
use SoloCabinet\Service\Ultima\ProvidesReserves;
use SoloCabinet\Entity\DocumentsFilterOptions;
use SoloCabinet\Entity\Ultima\Reserve;
use Solo\ServiceManager\Result;
use Solo\DateTime\DateTime;
use SoloCatalog\Service\Helper\DateHelper;
use SoloERP\WebService\Reader\UltimaJsonListReader;

final class AllReservesService extends ServiceLocatorAwareService {

	use ProvidesListing;
	use ProvidesReserves;

	/**
	 *
	 * @param int $agentId
	 * @return Result
	 */
	public function enum($agentId = 0, $statusId = null) {
		$enumResult = $this->enumGenericReserves($agentId, $statusId);
		$result = new Result();

		if ($enumResult->success() && $enumResult->items) {
			$result->items = $enumResult->items;
		} else {
			$result->setError($enumResult->getErrorCode());
		}
		return $result;
	}

	/**
	 *
	 * @param integer $managerId
	 * @param integer $statusId
	 * @param DateTime $createdFrom
	 * @param DateTime $createdTo
	 * @param string $searchQuery
	 * @return \Solo\ServiceManager\Result
	 */
	public function enumManagerReserves($managerId = null, $statusId = null, DateTime $createdFrom = null, DateTime $createdTo = null, $searchQuery = null) {
		$wm = $this->getWebMethod('GetManagerReserves');
		if (null !== $managerId) {
			$wm->addPar('ManagerID', $managerId);
		}

		if (!empty($statusId)) {
			$wm->addPar('StatusID', $statusId);
		}
		if (!empty($createdFrom) && !empty($createdTo)) {
			$wm->addPar('CreatedFrom', DateHelper::getSerializeJsonDate($createdFrom));
			$wm->addPar('CreatedTo', DateHelper::getSerializeJsonDate($createdTo));
		}
		if (!empty($searchQuery)) {
			$wm->addPar('SearchQuery', $searchQuery);
		}

		if ('output' == $_GET['debug']) {
			print '<pre>' . json_encode($wm->getPars(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT) . '</pre>';
		}

		$response = $wm->call();
		if ('output' == $_GET['debug']) {
			print '<pre>';
			print_r($response);
			print '</pre>';
			exit();
		}
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$items = [];
			$rdr = new UltimaJsonListReader($response);

			foreach ($rdr->getValue('Documents') as $row) {
				$item = new Reserve();
				$item->setDocumentId(intval($row->Id));
				$item->setAmount(intval($row->Amount));
				$item->setCreationDate(trim($row->CreationDate));
				$item->setDeadReserveDateTime(trim($row->DeadDate));
				$item->setDeliveryDate(trim($row->DeliveryDate));
				$item->setDeliveryTimeId($row->TimeId);
				$item->setArticles($row->Articles);
				$item->setGroup($row->Group);
				$item->isPayed($row->IsPayed);
				$item->setIsDelivery('delivery' == $row->ObtainMethod);
				$item->setWarehouseId(intval($row->ReserveStoreId));
				$item->setUsedBonusAmount(intval($row->UsedBonusAmount));
				$item->setAgentId(intval($row->AgentID));
				$item->setStatusId($row->StatusId);
				$item->setManagerId($row->ManagerId);
				if (is_numeric($row->PaymentTypeId)) {
					$item->setPayTypeId($row->PaymentTypeId);
				}
				$item->setSubTypeId($row->SubTypeId);

				$items[] = $item;
			}

			$result->items = $items;
		}

		return $result;
	}

	/**
	 *
	 * @param string $securityKey
	 * @param array $allReserves
	 * @return Result
	 */
	public function getArticles($securityKey, $allReserves) {
		$artResult = $this->artGenericReserves($securityKey, $allReserves);
		$result = new Result();

		if ($artResult->success() && $artResult->items) {
			$result->items = $artResult->items;
		} else {
			$result->setError($artResult->getErrorCode());
		}
		return $result;
	}

	/**
	 *
	 * @param string $securityKey
	 * @param integer $agentId
	 * @param DocumentsFilterOptions $options
	 * @return Result
	 */
	public function getStatistics($securityKey, $agentId, DocumentsFilterOptions $options) {
		$statResult = $this->getGenericStatistics($securityKey, $agentId, Reserve::TYPE_ALL, $options);
		$result = new Result();
		if ($statResult->success() && $statResult->stats) {
			$result->stats = $statResult->stats;
		} else {
			$result->setError($statResult->getErrorCode());
		}
		return $result;
	}

	/**
	 *
	 * @param integer $agentId
	 * @param integer $documentId
	 * @return integer
	 */
	public function countReserveArticles($agentId, $documentId) {
		$result = 0;

		$wm = $this->getWebMethod('GetReserveArticles');
		$wm->addPar('Id', $documentId);
		$wm->addPar('AgentId', $agentId);
		$response = $wm->call();
		if (!$response->hasError()) {
			$resInfo = new UltimaJsonListReader($response);
			foreach ($resInfo as $row) {
				$result += $row->offsetGet('Quantity');
			}
			// $result = $resInfo->count();
		}

		return $result;
	}

}
