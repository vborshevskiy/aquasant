<?php

namespace SoloCabinet\Service\Ultima;

use SoloERP\Service\ProvidesWebservice;
use SoloCabinet\Entity\DocumentsFilterOptions;
use SoloCabinet\Entity\Ultima\Reserve;
use SoloCabinet\Entity\Ultima\ReserveGood;
use Solo\ServiceManager\Result;
use SoloCabinet\Entity\DocumentsStatistics;
use SoloERP\WebService\Reader\UltimaJsonListReader;

trait ProvidesReserves {

    use ProvidesWebservice;

    /**
     * @param $agentId
     * @return Result
     */
    protected function enumGenericReserves($agentId, $statusId = null) {
        $wm = $this->getWebMethod('GetReserves');
        if ($agentId > 0) {
            $wm->addPar('AgentId', $agentId);
        }

        if ($statusId > 0) {
            $wm->addPar('StatusId', $statusId);
        }

        $response = $wm->call();
        $result = new Result();
        if ($response->hasError()) {
            $result->setError($response->getError());
        } else {
            $items = [];
            $rdr = new UltimaJsonListReader($response);

            foreach ($rdr->getValue('Documents') as $row) {
                $item = new Reserve();
                $item->setDocumentId(intval($row->Id));
                $item->setAmount(intval($row->Amount));
                $item->setCreationDate(trim($row->CreationDate));
                $item->setDeadReserveDateTime(trim($row->DeadDate));
                $item->setDeliveryDate(trim($row->DeliveryDate));
                $item->setDeliveryTimeId($row->TimeId);
                $item->setArticles($row->Articles);
                $item->setGroup($row->Group);
                $item->isPayed($row->IsPayed);
                $item->setIsDelivery('delivery' == $row->ObtainMethod);
                $item->setWarehouseId(intval($row->ReserveStoreId));
                $item->setUsedBonusAmount(intval($row->UsedBonusAmount));
                $item->setAgentId(intval($agentId));
                $item->setStatusId($row->StatusId);
                $item->setPayTypeId($row->PaymentTypeId);

                $items[] = $item;
            }

            $result->items = $items;
        }

        return $result;
    }

    /**
     * 
     * @param string $securityKey
     * @param array $allReserves
     * @return Result
     */
    protected function artGenericReserves($securityKey, $allReserves) {

        $result = new Result();

        foreach ($allReserves as $reserve) {

            $wm = $this->getWebMethod('GetReserveArticles');
            $wm->addPar('SecurityKey', $securityKey);
            $wm->addPar('Id', $reserve->getdocumentId());
            $response = $wm->call();
            if ($response->hasError()) {
                $result->setError($response->getError());
            } else {
                $resInfo = new UltimaJsonListReader($response);



                foreach ($resInfo as $data) {
                    $good = new ReserveGood();
                    $good->setId(intval($data['Id']));
                    $good->setQuantity(intval($data['Quantity']));
                    $good->setStoreQuantity(intval($data['StoreQuantity']));
                    $good->setPrice(floatval($data['Price']));
                    $good->setAmount(floatval($data['Amount']));
                    $reserve->addGood($good);
                }
            }
        }

        $result->items = $allReserves;

        return $result;
    }

    /**
     * 
     * @param string $securityKey
     * @param integer $agentId
     * @param type $type
     * @param DocumentsFilterOptions $options
     * @return Result
     */
    protected function getGenericStatistics($securityKey, $agentId, $type, DocumentsFilterOptions $options) {
        $wm = $this->getWebMethod("GetClientOrdersStatistics");
        $wm->addPar('SecurityKey', $securityKey);

        $response = $wm->call();

        $result = new Result();
        if ($response->hasError()) {
            $result->setError($response->getError());
        } else {
            $rdr = new UltimaJsonListReader($response);            //    var_dump($rdr); die;
            foreach ($rdr as $row) {
                $stats = new DocumentsStatistics();
                if ($row['GroupKey'] == 'reserves')
                    $stats->setReservesCount(intval($row['GroupKey']));
                if ($row['GroupKey'] == 'drafts')
                    $stats->setDraftsCount(intval($row['GroupKey']));
                if ($row['GroupKey'] == 'shipping')
                    $stats->setShippingCount(intval($row['GroupKey']));
                if ($row['GroupKey'] == 'shipped')
                    $stats->setShippedCount(intval($row['GroupKey']));
            }
            //$stats->setDateLimits(trim($rdr->getValue('CreationDateMin') ), trim($rdr->getValue('CreationDateMax')));
//			$stats->setCostLimits(floatval($rdr->getValue('AmountMin')), floatval($rdr->getValue('AmountMax')));

            $result->stats = $stats;
        }
        return $result;
    }

}
