<?php

namespace SoloCabinet\Service\Ultima;

use Solo\ServiceManager\ServiceLocatorAwareService;
use SoloCabinet\Service\ProvidesListing;
use SoloCabinet\Entity\DocumentsFilterOptions;
use Solo\ServiceManager\Result;
use SoloCabinet\Entity\Ultima\Contract;
use SoloERP\WebService\Reader\UltimaListReader;
use SoloCabinet\Entity\DocumentsStatistics;
use SoloERP\WebService\Reader\UltimaObjectReader;
use SoloCabinet\Entity\Ultima\DocumentDownloadOptions;
use SoloCabinet\Service\Ultima\ProvidesDocumentDownloads;

final class ContractsService extends ServiceLocatorAwareService {
	
	use ProvidesListing;
	use ProvidesDocumentDownloads;

	/**
	 *
	 * @param integer $userId        	
	 * @param integer $agentId        	
	 * @param DocumentsFilterOptions $filterOptions        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function enum($userId, $agentId, DocumentsFilterOptions $filterOptions) {
		$wm = $this->getWebMethod("GetJBContractsList");
		$wm->setUserId($userId);
		$wm->setAgentId($agentId);
		if ($filterOptions->hasCostFrom()) {
			$wm->setCostFrom($filterOptions->getCostFrom());
		}
		if ($filterOptions->hasCostTo()) {
			$wm->setCostTo($filterOptions->getCostTo());
		}
		if ($filterOptions->hasDateFrom()) {
			$wm->setDateFrom($filterOptions->getDateFrom());
		}
		if ($filterOptions->hasDateTo()) {
			$wm->setDateTo($filterOptions->getDateTo());
		}
		if ($filterOptions->hasDocumentNumber()) {
			$wm->setDocumentNo($filterOptions->getDocumentNumber());
		}
		$wm->setPage($filterOptions->getPage());
		if ($filterOptions->hasItemsPerPage()) {
			$wm->setRecPerPage($filterOptions->getItemsPerPage());
		}
		if ($filterOptions->hasSortColumn()) {
			$wm->setSort($filterOptions->getSortColumn());
		}
		if ($filterOptions->hasSortDirection()) {
			$wm->setSortDir($filterOptions->getSortDirection());
		}
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$items = [];
			$rdr = new UltimaListReader($response);
			foreach ($rdr as $row) {
				$item = new Contract();
				$item->setId(intval($row->ID));
				$item->setNumber(intval($row->ContractNo));
				$item->setActivationDate(trim($row->ActivationDate));
				$item->setAmount(floatval($row->Amount));
				$item->setPeriod(trim($row->Period));
				$item->setDirectorPersonName(trim($row->DirectorPersonName));
				$item->setFirmName(trim($row->FirmName));
				
				$items[$item->getId()] = $item;
			}
			$result->contracts = $items;
		}
		return $result;
	}

	/**
	 *
	 * @param integer $userId        	
	 * @param integer $agentId        	
	 * @param DocumentsFilterOptions $filterOptions        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getStatistics($userId, $agentId, DocumentsFilterOptions $filterOptions) {
		$wm = $this->getWebMethod("GetGeneralContractsInfo");
		$wm->setUserId($userId);
		$wm->setAgentId($agentId);
		if ($filterOptions->hasCostFrom()) {
			$wm->setCostFrom($filterOptions->getCostFrom());
		}
		if ($filterOptions->hasCostTo()) {
			$wm->setCostTo($filterOptions->getCostTo());
		}
		if ($filterOptions->hasDateFrom()) {
			$wm->setDateFrom($filterOptions->getDateFrom());
		}
		if ($filterOptions->hasDateTo()) {
			$wm->setDateTo($filterOptions->getDateTo());
		}
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaObjectReader($response);
			$stats = new DocumentsStatistics();
			$stats->setTotalCount(intval($rdr->ALL_NUM));
			$stats->setDateLimits(trim($rdr->MIN_DATE), trim($rdr->MAX_DATE));
			$stats->setCostLimits(floatval($rdr->MIN_COST), floatval($rdr->MAX_COST));
			
			$result->stats = $stats;
		}
		return $result;
	}

	/**
	 *
	 * @param integer $userId        	
	 * @param integer $agentId        	
	 * @param integer $docId        	
	 * @param DocumentDownloadOptions $options        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getContract($userId, $agentId, $docId, DocumentDownloadOptions $options = null) {
		$options = $this->prepareDownloadOptions($options);
		$wm = $this->getWebMethod("GetJBContract");
		$wm->setUserId($userId);
		$wm->setAgentId($agentId);
		$wm->setDocId($docId);
		$wm->setPrintFormID($options->getFileType());
		$wm->setDocFileType($options->getPrintFormId());
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$result->content = $response->getData();
		}
		return $result;
	}

}

?>