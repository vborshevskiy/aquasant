<?php

namespace SoloCabinet\Service\Ultima;

use Solo\ServiceManager\ServiceLocatorAwareService;
use SoloCabinet\Entity\DocumentsFilterOptions;
use SoloCabinet\Service\Ultima\ProvidesReserves;
use SoloCabinet\Entity\Ultima\Reserve;
use Solo\ServiceManager\Result;
use SoloCabinet\Service\ProvidesListing;

final class ActiveReservesService extends ServiceLocatorAwareService {
	
	use ProvidesListing;
	use ProvidesReserves;

	/**
	 *
	 * @param integer $userId        	
	 * @param integer $agentId        	
	 * @param DocumentsFilterOptions $options        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function enum($userId, $agentId, DocumentsFilterOptions $options) {
		$enumResult = $this->enumGenericReserves($userId, $agentId, Reserve::TYPE_ACTIVE, $options);
		$result = new Result();
		if ($enumResult->success() && $enumResult->items) {
			$result->items = $enumResult->items;
		} else {
			$result->setError($enumResult->getErrorCode());
		}
		return $result;
	}

	/**
	 *
	 * @param integer $userId        	
	 * @param integer $agentId        	
	 * @param DocumentsFilterOptions $options        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getStatistics($userId, $agentId, DocumentsFilterOptions $options) {
		$statResult = $this->getGenericStatistics($userId, $agentId, Reserve::TYPE_ACTIVE, $options);
		$result = new Result();
		if ($statResult->success() && $statResult->stats) {
			$result->stats = $statResult->stats;
		} else {
			$result->setError($statResult->getErrorCode());
		}
		return $result;
	}

}

?>