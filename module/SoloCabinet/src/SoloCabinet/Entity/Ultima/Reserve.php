<?php

namespace SoloCabinet\Entity\Ultima;

use Solo\Collection\Collection;

class Reserve {

	/**
	 *
	 * @var string
	 */
	const DELIVERY_TYPE_SHOP = 'shop';

	/**
	 *
	 * @var string
	 */
	const DELIVERY_TYPE_TRANSPORT = 'transport';

	/**
	 *
	 * @var integer
	 */
	const PAYMENT_TYPE_CASH = 3;

	/**
	 *
	 * @var integer
	 */
	const PAYMENT_TYPE_ONLINE = 7;

	/**
	 *
	 * @var integer
	 */
	const TYPE_ALL = 0;

	/**
	 *
	 * @var integer
	 */
	const TYPE_ACTIVE = 1;

	/**
	 *
	 * @var integer
	 */
	const TYPE_PURCHASING = 2;

	/**
	 *
	 * @var integer
	 */
	const TYPE_FINISHED = 3;

	/**
	 *
	 * @var integer
	 */
	const TYPE_DRAFT = 4;

	/**
	 *
	 * @var integer
	 */
	private $documentId = null;

	/**
	 *
	 * @var integer
	 */
	private $invoiceId = null;

	/**
	 *
	 * @var integer
	 */
	private $group = null;

	/**
	 *
	 * @var array
	 */
	private $articles = null;

	/**
	 *
	 * @var integer
	 */
	private $invoiceNumber = null;

	/**
	 *
	 * @var integer
	 */
	private $shippingInvoiceId = null;

	/**
	 *
	 * @var integer
	 */
	private $payTypeId = null;

	/**
	 *
	 * @var integer
	 */
	private $documentNumber = null;

	/**
	 *
	 * @var string
	 */
	private $createDate = null;

	/**
	 *
	 * @var string
	 */
	private $deadReserveDateTime = null;

	/**
	 *
	 * @var string
	 */
	private $dateTime = null;

	/**
	 *
	 * @var float
	 */
	private $amount = null;

	/**
	 *
	 * @var boolean
	 */
	private $isDelivery = false;

	/**
	 *
	 * @var integer
	 */
	private $warehouseId = null;

	/**
	 *
	 * @var integer
	 */
	private $outcomeWarehouseId = null;

	/**
	 *
	 * @var int
	 */
	private $warehouseReserveId = null;

	/**
	 *
	 * @var integer
	 */
	private $deliveryAddressId = null;

	/**
	 *
	 * @var string
	 */
	private $deliveryDate = null;

	/**
	 *
	 * @var integer
	 */
	private $deliveryTimeId = null;

	/**
	 *
	 * @var string
	 */
	private $deliveryComments = null;

	/**
	 *
	 * @var string
	 */
	private $deliveryAddressDescription = null;

	/**
	 *
	 * @var bool
	 */
	private $deliveryNeedLifting = null;
	
	/**
	 * 
	 * @var integer
	 */
	private $deliveryLiftType = null;

	/**
	 *
	 * @var int
	 */
	private $deliveryGoodId = null;

	/**
	 *
	 * @var integer
	 */
	private $isUndefinedDeliveryArea = false;

	/**
	 *
	 * @var string
	 */
	private $status = null;

	/**
	 *
	 * @var integer
	 */
	private $agentId = null;

	/**
	 *
	 * @var integer
	 */
	private $typeReserve = null;

	/**
	 *
	 * @var integer
	 */
	private $priceCategoryId = null;

	/**
	 *
	 * @var boolean
	 */
	private $isHasSuborder = false;

	/**
	 *
	 * @var Collection
	 */
	private $goods;

	/**
	 *
	 * @var float
	 */
	private $chargedBonusAmount = 0;

	/**
	 *
	 * @var float
	 */
	private $usedBonusAmount = 0;

	/**
	 *
	 * @var type integer
	 */
	private $storeId = 0;

	/**
	 *
	 * @var float
	 */
	private $deliveryCost = 0;

	/**
	 *
	 * @var boolean
	 */
	private $isPayed = false;

	/**
	 *
	 * @var integer
	 */
	private $statusId;

	/**
	 *
	 * @var float
	 */
	private $comissionPercent = 0;

	/**
	 *
	 * @var boolean
	 */
	private $isDeliveryByTransportCompany = false;

	/**
	 *
	 * @var array
	 */
	private $childDocuments = [];

	/**
	 *
	 * @var integer
	 */
	private $subTypeId;

	/**
	 *
	 * @var string
	 */
	private $comment;

	/**
	 *
	 * @var integer
	 */
	private $managerId;

	/**
	 *
	 * @var string
	 */
	private $contactPhone;

	/**
	 *
	 * @var string
	 */
	private $contactName;

	/**
	 *
	 * @var string
	 */
	private $contactEmail;

	/**
	 * Creates reserve instance
	 */
	public function __construct() {
		$this->goods = new Collection();
	}

	/**
	 *
	 * @return integer
	 */
	public function getDocumentId() {
		return $this->documentId;
	}

	/**
	 *
	 * @param integer $documentId        	
	 * @throws \InvalidArgumentException
	 */
	public function setDocumentId($documentId) {
		if (!is_integer($documentId)) {
			throw new \InvalidArgumentException('Document id must be integer');
		}
		$this->documentId = $documentId;
	}

	/**
	 *
	 * @return integer
	 */
	public function getArticles() {
		return $this->articles;
	}

	/**
	 *
	 * @param string $articles        	
	 */
	public function setArticles($articles) {
		$this->articles = explode(',', $articles);
	}

	/**
	 *
	 * @return integer
	 */
	public function getGroup() {
		return $this->group;
	}

	/**
	 *
	 * @param string $group        	
	 */
	public function setGroup($group) {
		$this->group = $group;
	}

	/**
	 *
	 * @return integer
	 */
	public function getInvoiceId() {
		return $this->invoiceId;
	}

	/**
	 *
	 * @param integer $invoiceId        	
	 * @throws \InvalidArgumentException
	 */
	public function setInvoiceId($invoiceId) {
		if (!is_integer($invoiceId)) {
			throw new \InvalidArgumentException('Invoice id must be integer');
		}
		$this->invoiceId = $invoiceId;
	}

	/**
	 *
	 * @return int
	 */
	public function getDeliveryGoodId() {
		return $this->deliveryGoodId;
	}

	/**
	 *
	 * @param int $deliveryGoodId        	
	 */
	public function setDeliveryGoodId($deliveryGoodId) {
		$this->deliveryGoodId = $deliveryGoodId;
	}

	/**
	 *
	 * @return int
	 */
	public function getWarehouseReserveId() {
		return $this->warehouseReserveId;
	}

	/**
	 *
	 * @param int $warehouseReserveId        	
	 */
	public function setWarehouseReserveId($warehouseReserveId) {
		$this->warehouseReserveId = $warehouseReserveId;
	}

	/**
	 *
	 * @return integer
	 */
	public function getInvoiceNumber() {
		return $this->invoiceNumber;
	}

	/**
	 *
	 * @param integer $invoiceNumber        	
	 * @throws \InvalidArgumentException
	 */
	public function setInvoiceNumber($invoiceNumber) {
		if (!is_integer($invoiceNumber)) {
			throw new \InvalidArgumentException('Invoice number must be integer');
		}
		$this->invoiceNumber = $invoiceNumber;
	}

	/**
	 *
	 * @return integer
	 */
	public function getPayTypeId() {
		return $this->payTypeId;
	}

	/**
	 *
	 * @param integer $payTypeId        	
	 * @throws \InvalidArgumentException
	 */
	public function setPayTypeId($payTypeId) {
		if (!is_integer($payTypeId)) {
			throw new \InvalidArgumentException('Payment type id must be integer');
		}
		
		$this->payTypeId = $payTypeId;
	}

	/**
	 *
	 * @return integer
	 */
	public function getDocumentNumber() {
		return $this->documentNumber;
	}

	/**
	 *
	 * @param integer $documentNumber        	
	 * @throws \InvalidArgumentException
	 */
	public function setDocumentNumber($documentNumber) {
		if (!is_integer($documentNumber)) {
			throw new \InvalidArgumentException('Document number must be integer');
		}
		$this->documentNumber = $documentNumber;
	}

	/**
	 *
	 * @return string
	 */
	public function getCreationDate() {
		return $this->createDate;
	}

	/**
	 *
	 * @param string $createDate        	
	 */
	public function setCreationDate($createDate) {
		$this->createDate = $createDate;
	}

	/**
	 *
	 * @return string
	 */
	public function getDeadReserveDateTime() {
		return $this->deadReserveDateTime;
	}

	/**
	 *
	 * @param string $deadReserveDateTime        	
	 */
	public function setDeadReserveDateTime($deadReserveDateTime) {
		$this->deadReserveDateTime = $deadReserveDateTime;
	}

	/**
	 *
	 * @return string
	 */
	public function getDateTime() {
		return $this->dateTime;
	}

	/**
	 *
	 * @param string $deadReserveDateTime        	
	 */
	public function setDateTime($dateTime) {
		$this->dateTime = $dateTime;
	}

	/**
	 *
	 * @return float
	 */
	public function getAmount() {
		return $this->amount;
	}

	/**
	 *
	 * @param float $amount        	
	 * @throws \InvalidArgumentException
	 */
	public function setAmount($amount) {
		if (!is_numeric($amount)) {
			throw new \InvalidArgumentException('Amount must be numeric');
		}
		$this->amount = $amount;
	}

	/**
	 *
	 * @return boolean
	 */
	public function getIsDelivery() {
		return $this->isDelivery;
	}

	/**
	 *
	 * @param boolean $isDelivery        	
	 */
	public function setIsDelivery($isDelivery) {
		if (!is_bool($isDelivery)) {
			throw new \InvalidArgumentException('Is delivery must be boolean');
		}
		$this->isDelivery = $isDelivery;
	}

	/**
	 *
	 * @param boolean $isDelivery        	
	 * @return boolean
	 */
	public function isDelivery($isDelivery = null) {
		if (null !== $isDelivery) {
			$this->setIsDelivery($isDelivery);
		} else {
			return $this->getIsDelivery();
		}
	}

	/**
	 *
	 * @return integer
	 */
	public function getWarehouseId() {
		return $this->warehouseId;
	}

	/**
	 *
	 * @param integer $warehouseId        	
	 * @throws \InvalidArgumentException
	 */
	public function setWarehouseId($warehouseId) {
		if (!is_integer($warehouseId)) {
			throw new \InvalidArgumentException('Warehouse id must integer');
		}
		$this->warehouseId = $warehouseId;
	}

	/**
	 *
	 * @return integer
	 */
	public function getDeliveryAddressId() {
		return $this->deliveryAddressId;
	}

	/**
	 *
	 * @param integer $deliveryAddressId        	
	 * @throws \InvalidArgumentException
	 */
	public function setDeliveryAddressId($deliveryAddressId) {
		if (!is_integer($deliveryAddressId)) {
			throw new \InvalidArgumentException('Delivery address id must be integer');
		}
		$this->deliveryAddressId = $deliveryAddressId;
	}

	/**
	 *
	 * @return string
	 */
	public function getStatus() {
		return $this->status;
	}

	/**
	 *
	 * @param string $status        	
	 */
	public function setStatus($status) {
		$this->status = $status;
	}

	/**
	 *
	 * @return integer
	 */
	public function getAgentId() {
		return $this->agentId;
	}

	/**
	 *
	 * @param integer $agentId        	
	 * @throws \InvalidArgumentException
	 */
	public function setAgentId($agentId) {
		if (!is_integer($agentId)) {
			throw new \InvalidArgumentException('Agent id must be integer');
		}
		$this->agentId = $agentId;
	}

	/**
	 *
	 * @return integer
	 */
	public function getTypeReserve() {
		return $this->typeReserve;
	}

	/**
	 *
	 * @param integer $typeReserve        	
	 * @throws \InvalidArgumentException
	 */
	public function setTypeReserve($typeReserve) {
		if (!is_integer($typeReserve)) {
			throw new \InvalidArgumentException('Type must be integer');
		}
		if (!in_array($typeReserve, [
			self::TYPE_ALL,
			self::TYPE_ACTIVE,
			self::TYPE_DRAFT,
			self::TYPE_FINISHED,
			self::TYPE_PURCHASING 
		])) {
			throw new \InvalidArgumentException('Type must in compatible types');
		}
		$this->typeReserve = $typeReserve;
	}

	/**
	 *
	 * @return integer
	 */
	public function getShippingInvoiceId() {
		return $this->shippingInvoiceId;
	}

	/**
	 *
	 * @param integer $shippingInvoiceId        	
	 * @throws \InvalidArgumentException
	 */
	public function setShippingInvoiceId($shippingInvoiceId) {
		if (!is_integer($shippingInvoiceId)) {
			throw new \InvalidArgumentException('Shipping invoice id must be integer');
		}
		$this->shippingInvoiceId = $shippingInvoiceId;
	}

	public function getOutcomeWarehouseId() {
		return $this->outcomeWarehouseId;
	}

	public function setOutcomeWarehouseId($outcomeWarehouseId) {
		$this->outcomeWarehouseId = $outcomeWarehouseId;
	}

	/**
	 *
	 * @return string
	 */
	public function getDeliveryDate() {
		return $this->deliveryDate;
	}

	/**
	 *
	 * @param string $deliveryDate        	
	 */
	public function setDeliveryDate($deliveryDate) {
		$this->deliveryDate = $deliveryDate;
	}

	/**
	 *
	 * @return integer
	 */
	public function getDeliveryTimeId() {
		return $this->deliveryTimeId;
	}

	/**
	 *
	 * @param integer $deliveryTimeId        	
	 * @throws \InvalidArgumentException
	 */
	public function setDeliveryTimeId($deliveryTimeId) {
		// if (!is_integer($deliveryTimeId)) {
		// throw new \InvalidArgumentException('Delivery time id must be integer');
		// }
		$this->deliveryTimeId = $deliveryTimeId;
	}

	/**
	 *
	 * @return string
	 */
	public function getDeliveryComments() {
		return $this->deliveryComments;
	}

	/**
	 *
	 * @param string $deliveryComments        	
	 */
	public function setDeliveryComments($deliveryComments) {
		$this->deliveryComments = $deliveryComments;
	}

	/**
	 *
	 * @return string
	 */
	public function getDeliveryAddressDescription() {
		return $this->deliveryAddressDescription;
	}

	/**
	 *
	 * @param bool $deliveryNeedLifting        	
	 */
	public function setDeliveryNeedLifting($deliveryNeedLifting) {
		$this->deliveryNeedLifting = $deliveryNeedLifting;
	}

	/**
	 *
	 * @return bool
	 */
	public function getDeliveryNeedLifting() {
		return $this->deliveryNeedLifting;
	}

	/**
	 *
	 * @param string $deliveryAddressDescription        	
	 */
	public function setDeliveryAddressDescription($deliveryAddressDescription) {
		$this->deliveryAddressDescription = $deliveryAddressDescription;
	}

	/**
	 *
	 * @return integer
	 */
	public function getPriceCategoryId() {
		return $this->priceCategoryId;
	}

	/**
	 *
	 * @param integer $priceCategoryId        	
	 * @throws \InvalidArgumentException
	 */
	public function setPriceCategoryId($priceCategoryId) {
		if (!is_integer($priceCategoryId)) {
			throw new \InvalidArgumentException('Price category id must be integer');
		}
		$this->priceCategoryId = $priceCategoryId;
	}

	/**
	 *
	 * @return boolean
	 */
	public function getIsHasSuborder() {
		return $this->isHasSuborder;
	}

	/**
	 *
	 * @param integer $isHasSuborder        	
	 * @throws \InvalidArgumentException
	 */
	public function setIsHasSuborder($isHasSuborder) {
		if (!is_bool($isHasSuborder)) {
			throw new \InvalidArgumentException('Is has suborder must be boolean');
		}
		$this->isHasSuborder = $isHasSuborder;
	}

	/**
	 *
	 * @param boolean $isHasSuborder        	
	 * @return boolean
	 */
	public function hasSuborder($isHasSuborder = null) {
		if (null !== $isHasSuborder) {
			$this->setIsHasSuborder($isHasSuborder);
		} else {
			return $this->getIsHasSuborder();
		}
	}

	/**
	 *
	 * @param ReserveGood $good        	
	 */
	public function addGood(ReserveGood $good) {
		$this->goods[$good->getId()] = $good;
	}

	/**
	 *
	 * @return Collection
	 */
	public function getGoods() {
		return $this->goods;
	}

	public function getGoodIds() {
		if ($this->getGoods() !== null) {
			return array_keys($this->getGoods()->toArray());
		}
		return null;
	}

	public function getGoodQuantities() {
		if ($this->getGoods() !== null) {
			$response = [];
			foreach ($this->getGoods() as $reserveGood) {
				$response[] = $reserveGood->getQuantity();
			}
			return $response;
		}
		return null;
	}

	/**
	 *
	 * @return boolean
	 */
	public function isCashless() {
		return (self::PAYMENT_TYPE_CASHLESS == $this->getPayTypeId());
	}

	/**
	 *
	 * @return boolean
	 */
	public function isCash() {
		return (self::PAYMENT_TYPE_CASH == $this->getPayTypeId());
	}

	/**
	 *
	 * @return boolean
	 */
	public function isActive() {
		return (self::TYPE_ACTIVE == $this->getTypeReserve());
	}

	/**
	 *
	 * @return boolean
	 */
	public function isProcessed() {
		return (self::TYPE_PURCHASING == $this->getTypeReserve());
	}

	/**
	 *
	 * @return boolean
	 */
	public function isFinished() {
		return (self::TYPE_FINISHED == $this->getTypeReserve());
	}

	/**
	 *
	 * @return float
	 */
	public function getTotalAmount() {
		$sum = 0;
		foreach ($this->goods as $good) {
			$sum += $good->getAmount();
		}
		return $sum;
	}

	/**
	 *
	 * @return float
	 */
	public function getChargedBonusAmount() {
		return $this->chargedBonusAmount;
	}

	/**
	 *
	 * @param float $chargedBonusAmount        	
	 */
	public function setChargedBonusAmount($chargedBonusAmount) {
		$this->chargedBonusAmount = $chargedBonusAmount;
	}

	/**
	 *
	 * @return float
	 */
	public function getUsedBonusAmount() {
		return $this->usedBonusAmount;
	}

	/**
	 *
	 * @param float $usedBonusAmount        	
	 */
	public function setUsedBonusAmount($usedBonusAmount) {
		$this->usedBonusAmount = $usedBonusAmount;
	}

	/**
	 *
	 * @return integer
	 */
	public function getStoreId() {
		return $this->storeId;
	}

	/**
	 *
	 * @param integer $storeId        	
	 */
	public function setStoreId($storeId) {
		$this->storeId = $storeId;
	}

	/**
	 *
	 * @return float
	 */
	public function getDeliveryCost() {
		return $this->deliveryCost;
	}

	/**
	 *
	 * @param float $cost        	
	 */
	public function setDeliveryCost($cost) {
		$this->deliveryCost = $cost;
	}

	public function isPayed($isPayed = null) {
		if (is_null($isPayed)) {
			return $this->isPayed;
		}
		$this->isPayed = (bool)$isPayed;
	}

	/**
	 *
	 * @return float
	 */
	public function getComissionPercent() {
		return $this->comissionPercent;
	}

	/**
	 *
	 * @param float $comissionPercent        	
	 */
	public function setComissionPercent($comissionPercent) {
		$this->comissionPercent = (float)$comissionPercent;
	}

	/**
	 *
	 * @param
	 *        	boolean | null $isDeliveryByTransportCompany
	 * @return boolean | void
	 */
	public function isDeliveryByTransportCompany($isDeliveryByTransportCompany = null) {
		if (is_null($isDeliveryByTransportCompany)) {
			return $this->isDeliveryByTransportCompany;
		}
		$this->isDeliveryByTransportCompany = (bool)$isDeliveryByTransportCompany;
	}

	/**
	 *
	 * @return int
	 */
	public function getStatusId() {
		return $this->statusId;
	}

	/**
	 *
	 * @param int $statusId        	
	 */
	public function setStatusId($statusId) {
		$this->statusId = $statusId;
	}

	/**
	 *
	 * @param ReserveChildDocument $document        	
	 */
	public function addChildDocument(ReserveChildDocument $document) {
		$this->childDocuments[] = $document;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasChildDocuments() {
		return (0 < count($this->childDocuments));
	}

	/**
	 *
	 * @return array
	 */
	public function getChildDocuments() {
		return $this->childDocuments;
	}

	/**
	 *
	 * @return ReserveChildDocument
	 */
	public function getFirstChildDocument() {
		$result = null;
		
		if (0 < count($this->childDocuments)) {
			$result = reset($this->childDocuments);
		}
		
		return $result;
	}

	/**
	 *
	 * @return string
	 */
	public function getComment() {
		return $this->comment;
	}

	/**
	 *
	 * @param string $comment        	
	 */
	public function setComment($comment) {
		$this->comment = $comment;
		return $this;
	}

	/**
	 *
	 * @return number
	 */
	public function getSubTypeId() {
		return $this->subTypeId;
	}

	/**
	 *
	 * @param number $subTypeId        	
	 */
	public function setSubTypeId($subTypeId) {
		$this->subTypeId = $subTypeId;
		return $this;
	}

	/**
	 *
	 * @return number
	 */
	public function getManagerId() {
		return $this->managerId;
	}

	/**
	 *
	 * @param number $managerId        	
	 */
	public function setManagerId($managerId) {
		$this->managerId = $managerId;
		return $this;
	}

	/**
	 *
	 * @param string $value        	
	 * @return boolean
	 */
	public function isUndefinedDeliveryArea($value = null) {
		if (null !== $value) {
			$this->isUndefinedDeliveryArea = $value;
		} else {
			return $this->isUndefinedDeliveryArea;
		}
	}

	/**
	 *
	 * @return string
	 */
	public function getContactPhone() {
		return $this->contactPhone;
	}

	/**
	 *
	 * @param string $contactPhone        	
	 */
	public function setContactPhone($contactPhone) {
		$this->contactPhone = $contactPhone;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getContactName() {
		return $this->contactName;
	}

	/**
	 *
	 * @param string $contactName        	
	 */
	public function setContactName($contactName) {
		$this->contactName = $contactName;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getContactEmail() {
		return $this->contactEmail;
	}

	/**
	 *
	 * @param string $contactEmail        	
	 */
	public function setContactEmail($contactEmail) {
		$this->contactEmail = $contactEmail;
		return $this;
	}
	/**
	 * @return number
	 */
	public function getDeliveryLiftType() {
		return $this->deliveryLiftType;
	}


	/**
	 * @param number $deliveryLiftType
	 */
	public function setDeliveryLiftType($deliveryLiftType) {
		$this->deliveryLiftType = $deliveryLiftType;
		return $this;
	}



}
