<?php

namespace SoloCabinet\Entity\Ultima;

class GoodsIncomeNotifier {

	/**
	 *
	 * @var string
	 */
	private $phonePostfix = null;

	/**
	 *
	 * @var string
	 */
	private $phonePrefix = null;

	/**
	 *
	 * @var string
	 */
	private $email = null;

	/**
	 *
	 * @var array
	 */
	private $goods = [];

	/**
	 *
	 * @return string
	 */
	public function getPhonePostfix() {
		return $this->phonePostfix;
	}

	/**
	 *
	 * @param string $phonePostfix        	
	 */
	public function setPhonePostfix($phonePostfix) {
		$this->phonePostfix = $phonePostfix;
	}

	/**
	 *
	 * @return string
	 */
	public function getPhonePrefix() {
		return $this->phonePrefix;
	}

	/**
	 *
	 * @param string $phonePrefix        	
	 */
	public function setPhonePrefix($phonePrefix) {
		$this->phonePrefix = $phonePrefix;
	}

	/**
	 *
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 *
	 * @param string $email        	
	 */
	public function setEmail($email) {
		$this->email = $email;
	}

	/**
	 *
	 * @return array
	 */
	public function getGoods() {
		return $this->goods;
	}

	/**
	 *
	 * @param IncomeGood $good        	
	 */
	public function addGood(IncomeGood $good) {
		$this->goods[$good->getId()] = $good;
	}

}

?>
