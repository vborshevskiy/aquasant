<?php

namespace SoloCabinet\Entity\Ultima;

class NotifierEmail {

	/**
	 *
	 * @var integer
	 */
	private $id = null;

	/**
	 *
	 * @var string
	 */
	private $email = null;

	/**
	 *
	 * @var NotifyEvent
	 */
	private $notifyEvent = null;

	/**
	 *
	 * @return number
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param integer $id        	
	 * @throws \InvalidArgumentException
	 */
	public function setId($id) {
		if (!is_integer($id)) {
			throw new \InvalidArgumentException('Id must be integer');
		}
		$this->id = $id;
	}

	/**
	 *
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 *
	 * @param string $email        	
	 */
	public function setEmail($email) {
		$this->email = $email;
	}

	/**
	 *
	 * @return NotifyEvent
	 */
	public function getNotifyEvent() {
		return $this->notifyEvent;
	}

	/**
	 *
	 * @param NotifyEvent $notifyEvent        	
	 */
	public function setNotifyEvent(NotifyEvent $notifyEvent) {
		$this->notifyEvent = $notifyEvent;
	}

}

?>