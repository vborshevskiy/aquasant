<?php

namespace SoloCabinet\Entity\Ultima;

class Payment {

	/**
	 *
	 * @var integer
	 */
	private $id = null;

	/**
	 *
	 * @var integer
	 */
	private $number = null;

	/**
	 *
	 * @var string
	 */
	private $createDate = null;

	/**
	 *
	 * @var float
	 */
	private $amount = null;

	/**
	 *
	 * @var float
	 */
	private $ndsAmount = null;

	/**
	 *
	 * @var integer
	 */
	private $invoiceNumber = null;

	/**
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param integer $id        	
	 * @throws \InvalidArgumentException
	 */
	public function setId($id) {
		if (!is_integer($id)) {
			throw new \InvalidArgumentException('Id must be integer');
		}
		$this->id = $id;
	}

	/**
	 *
	 * @return integer
	 */
	public function getNumber() {
		return $this->number;
	}

	/**
	 *
	 * @param integer $number        	
	 * @throws \InvalidArgumentException
	 */
	public function setNumber($number) {
		if (!is_integer($number)) {
			throw new \InvalidArgumentException('Number must be integer');
		}
		$this->number = $number;
	}

	/**
	 *
	 * @return string
	 */
	public function getCreateDate() {
		return $this->createDate;
	}

	/**
	 *
	 * @param string $createDate        	
	 */
	public function setCreateDate($createDate) {
		$this->createDate = $createDate;
	}

	/**
	 *
	 * @return float
	 */
	public function getAmount() {
		return $this->amount;
	}

	/**
	 *
	 * @param float $amount        	
	 * @throws \InvalidArgumentException
	 */
	public function setAmount($amount) {
		if (!is_numeric($amount)) {
			throw new \InvalidArgumentException('Amount must be numeric');
		}
		$this->amount = $amount;
	}

	/**
	 *
	 * @return float
	 */
	public function getNdsAmount() {
		return $this->ndsAmount;
	}

	/**
	 *
	 * @param float $ndsAmount        	
	 * @throws \InvalidArgumentException
	 */
	public function setNdsAmount($ndsAmount) {
		if (!is_numeric($ndsAmount)) {
			throw new \InvalidArgumentException('Nds amount must be numeric');
		}
		$this->ndsAmount = $ndsAmount;
	}

	/**
	 *
	 * @return integer
	 */
	public function getInvoiceNumber() {
		return $this->invoiceNumber;
	}

	/**
	 *
	 * @param integer $invoiceNumber        	
	 * @throws \InvalidArgumentException
	 */
	public function setInvoiceNumber($invoiceNumber) {
		if (!is_integer($invoiceNumber)) {
			throw new \InvalidArgumentException('Invoice number must be integer');
		}
		$this->invoiceNumber = $invoiceNumber;
	}

}

?>