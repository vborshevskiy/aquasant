<?php

namespace SoloCabinet\Entity\Ultima;

class ReserveGood {

	/**
	 *
	 * @var integer
	 */
	private $id = null;

	/**
	 *
	 * @var string
	 */
	private $name = null;
	/**
	 *
	 * @var string
	 */
	private $link = null;

	/**
	 *
	 * @var integer
	 */
	private $quantity = null;
	
        /**
	 *
	 * @var integer
	 */
	private $storeQuantity = null;

	/**
	 *
	 * @var float
	 */
	private $price = null;

	/**
	 *
	 * @var float
	 */
	private $amount = null;

	/**
	 *
	 * @throws \RuntimeException
	 * @return integer
	 */
	public function getId() {
		if (!$this->hasId()) {
			throw new \RuntimeException('Id not set');
		}
		return $this->id;
	}

	/**
	 *
	 * @param integer $id        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCabinet\Entity\Ultima\ReserveGood
	 */
	public function setId($id) {
		if (!is_integer($id)) {
			throw new \InvalidArgumentException('Id must be integer');
		}
		$this->id = $id;
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasId() {
		return (null !== $this->id);
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return string
	 */
	public function getName() {
		if (!$this->hasName()) {
			throw new \RuntimeException('Name not set');
		}
		return $this->name;
	}

	/**
	 *
	 * @param string $name        	
	 * @return \SoloCabinet\Entity\Ultima\ReserveGood
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasName() {
		return (null !== $this->name);
	}
	/**
	 *
	 * @throws \RuntimeException
	 * @return string
	 */
	public function getLink() {
		if (!$this->hasLink()) {
			throw new \RuntimeException('Link not set');
		}
		return $this->link;
	}

	/**
	 *
	 * @param string $link        	
	 * @return \SoloCabinet\Entity\Ultima\ReserveGood
	 */
	public function setLink($link) {
		$this->link = $link;
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasLink() {
		return (null !== $this->link);
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return integer
	 */
	public function getQuantity() {
		if (!$this->hasQuantity()) {
			throw new \RuntimeException('Quantity not set');
		}
		return $this->quantity;
	}

	/**
	 *
	 * @param integer $quantity        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCabinet\Entity\Ultima\ReserveGood
	 */
	public function setQuantity($quantity) {
		if (!is_integer($quantity)) {
			throw new \InvalidArgumentException('Quantity must be integer');
		}
		$this->quantity = $quantity;
		return $this;
	}
	/**
	 *
	 * @throws \RuntimeException
	 * @return integer
	 */
	public function getStoreQuantity() {
		if (!$this->hasStoreQuantity()) {
			throw new \RuntimeException('StoreQuantity not set');
		}
		return $this->storeQuantity;
	}

	/**
	 *
	 * @param integer $storeQuantity        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCabinet\Entity\Ultima\ReserveGood
	 */
	public function setStoreQuantity($storeQuantity) {
		if (!is_integer($storeQuantity)) {
			throw new \InvalidArgumentException('StoreQuantity must be integer');
		}
		$this->storeQuantity = $storeQuantity;
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasQuantity() {
		return (null !== $this->quantity);
	}
        
	/**
	 *
	 * @return boolean
	 */
	public function hasStoreQuantity() {
		return (null !== $this->storeQuantity);
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return float
	 */
	public function getPrice() {
		if (!$this->hasPrice()) {
			throw new \RuntimeException('Price not set');
		}
		return $this->price;
	}

	/**
	 *
	 * @param float $price        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCabinet\Entity\Ultima\ReserveGood
	 */
	public function setPrice($price) {
		if (!is_numeric($price)) {
			throw new \InvalidArgumentException('Price must be numeric');
		}
		$this->price = $price;
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasPrice() {
		return (null !== $this->price);
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return float
	 */
	public function getAmount() {
		if (!$this->hasAmount()) {
			throw new \RuntimeException('Amount not set');
		}
		return $this->amount;
	}

	/**
	 *
	 * @param float $amount        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCabinet\Entity\Ultima\ReserveGood
	 */
	public function setAmount($amount) {
		if (!is_numeric($amount)) {
			throw new \InvalidArgumentException('Amount must be numeric');
		}
		$this->amount = $amount;
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasAmount() {
		return (null !== $this->amount);
	}

}

?>