<?php

namespace SoloCabinet\Entity\Ultima;

class IncomeGood {

	/**
	 *
	 * @var integer
	 */
	private $id = null;

	/**
	 *
	 * @var string
	 */
	private $name = null;

	/**
	 *
	 * @var string
	 */
	private $engName = null;

	/**
	 *
	 * @var array
	 */
	private $warehouses = null;

	/**
	 *
	 * @var integer
	 */
	private $quantity = null;

	/**
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param integer $id        	
	 * @throws \InvalidArgumentException
	 */
	public function setId($id) {
		if (!is_integer($id)) {
			throw new \InvalidArgumentException('Id must be integer');
		}
		$this->id = $id;
	}

	/**
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 *
	 * @param string $name        	
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 *
	 * @return string
	 */
	public function getEngName() {
		return $this->engName;
	}

	/**
	 *
	 * @param string $engName        	
	 */
	public function setEngName($engName) {
		$this->engName = $engName;
	}

	/**
	 *
	 * @return array
	 */
	public function getWarehouses() {
		return $this->warehouses;
	}

	/**
	 *
	 * @param array $warehouses        	
	 */
	public function setWarehouses($warehouses) {
		if (!is_array($warehouses)) {
			throw new \InvalidArgumentException('Warehouses must be array');
		}
		$this->warehouses = $warehouses;
	}

	/**
	 *
	 * @return integer
	 */
	public function getQuantity() {
		return $this->quantity;
	}

	/**
	 *
	 * @param integer $quantity        	
	 * @throws \InvalidArgumentException
	 */
	public function setQuantity($quantity) {
		if (!is_integer($quantity)) {
			throw new \InvalidArgumentException('Quantity must be integer');
		}
		$this->quantity = $quantity;
	}

	/**
	 *
	 * @param integer $warehouseId        	
	 * @throws \InvalidArgumentException
	 */
	public function addWarehouse($warehouseId) {
		if (!is_integer($warehouseId)) {
			throw new \InvalidArgumentException('Warehouse id must be integer');
		}
		if (!in_array($warehouseId, $this->warehouses)) {
			$this->warehouses[] = $warehouseId;
		}
	}

}

?>
