<?php

namespace SoloCabinet\Entity\Ultima;

class BonusHistory {

    /**
     *
     * @var integer
     */
    private $id = null;

    /**
     *
     * @var integer
     */
    private $number = null;

    /**
     *
     * @var boolean
     */
    private $isExpense = null;

    /**
     *
     * @var float
     */
    private $inAmount = null;

    /**
     *
     * @var float
     */
    private $outAmount = null;

    /**
     *
     * @var string
     */
    private $description = null;

    /**
     *
     * @var \DateTime
     */
    private $date = null;

    /**
     *
     * @var boolean
     */
    private $isReserve = null;

    /**
     *
     * @var boolean
     */
    private $isRealItem = null;

    /**
     *
     * @var integer
     */
    private $agentId = 0;

    /**
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @param integer $id        	
     * @throws \InvalidArgumentException
     */
    public function setId($id) {
        if (!is_integer($id)) {
            throw new \InvalidArgumentException('Id must be integer');
        }
        $this->id = $id;
    }

    /**
     *
     * @return integer
     */
    public function getNumber() {
        return $this->number;
    }

    /**
     *
     * @param integer $number        	
     * @throws \InvalidArgumentException
     */
    public function setNumber($number) {
        if (!is_integer($number)) {
            throw new \InvalidArgumentException('Number must be integer');
        }
        $this->number = $number;
    }

    /**
     *
     * @return boolean
     */
    public function getIsExpense() {
        return $this->isExpense;
    }

    /**
     *
     * @param boolean $isExpense        	
     * @throws \InvalidArgumentException
     */
    public function setIsExpense($isExpense) {
        if (!is_bool($isExpense)) {
            throw new \InvalidArgumentException('Is expense must be boolean');
        }
        $this->isExpense = $isExpense;
    }

    /**
     *
     * @param boolean $isExpense        	
     * @return boolean
     */
    public function isExpense($isExpense = null) {
        if (null !== $isExpense) {
            $this->setIsExpense($isExpense);
        } else {
            return $this->getIsExpense();
        }
    }

    /**
     *
     * @return float
     */
    public function getInAmount() {
        return $this->inAmount;
    }

    /**
     *
     * @param float $inAmount        	
     * @throws \InvalidArgumentException
     */
    public function setInAmount($inAmount) {
        if (null === $inAmount) {
            throw new \InvalidArgumentException('In amount can\'t be null');
        }
        $this->inAmount = $inAmount;
    }

    /**
     *
     * @return float
     */
    public function getOutAmount() {
        return $this->outAmount;
    }

    /**
     *
     * @param float $outAmount        	
     * @throws \InvalidArgumentException
     */
    public function setOutAmount($outAmount) {
        if (!is_numeric($outAmount)) {
            throw new \InvalidArgumentException('Out amount must be numeric');
        }
        $this->outAmount = $outAmount;
    }

    /**
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     *
     * @param string $description        	
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     *
     * @return \DateTime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     *
     * @param \DateTime $date        	
     */
    public function setDate(\DateTime $date) {
        $this->date = $date;
    }

    /**
     *
     * @return boolean
     */
    public function getIsReserve() {
        return $this->isReserve;
    }

    /**
     *
     * @param boolean $isReserve        	
     * @throws \InvalidArgumentException
     */
    public function setIsReserve($isReserve) {
        if (!is_bool($isReserve)) {
            throw new \InvalidArgumentException('Is reserve must be boolean');
        }
        $this->isReserve = $isReserve;
    }

    /**
     *
     * @param boolean $isReserve        	
     * @return boolean
     */
    public function isReserve($isReserve = null) {
        if (null !== $isReserve) {
            $this->setIsReserve($isReserve);
        } else {
            return $this->getIsReserve();
        }
    }

    /**
     *
     * @return boolean
     */
    public function getIsRealItem() {
        return $this->isRealItem;
    }

    /**
     *
     * @param boolean $isRealItem        	
     * @throws \InvalidArgumentException
     */
    public function setIsRealItem($isRealItem) {
        if (!is_bool($isRealItem)) {
            throw new \InvalidArgumentException('Is real item must be boolean');
        }
        $this->isRealItem = $isRealItem;
    }

    /**
     *
     * @param boolean $isRealItem        	
     * @return boolean
     */
    public function isRealItem($isRealItem = null) {
        if (null !== $isRealItem) {
            $this->setIsRealItem($isRealItem);
        } else {
            return $this->getIsRealItem();
        }
    }
    
    /**
     * 
     * @param integer $agentId
     * @throws \InvalidArgumentException
     */
    public function setAgentId($agentId) {
        if (!is_int($agentId)) {
            throw new \InvalidArgumentException('Agent id must be integer');
        }
        $this->agentId = $agentId;
    }
    
    /**
     * 
     * @return integer
     */
    public function getAgentId() {
        return $this->agentId;
    }

}

?>