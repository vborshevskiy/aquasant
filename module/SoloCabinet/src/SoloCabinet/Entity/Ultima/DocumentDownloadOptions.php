<?php

namespace SoloCabinet\Entity\Ultima;

class DocumentDownloadOptions {

    /**
     *
     * @var integer
     */
    private $printFormId = null;

    /**
     *
     * @var string
     */
    private $fileType = null;

    /**
     *
     * @param integer $printFormId        	
     * @param string $fileType        	
     */
    public function __construct($printFormId = null, $fileType = null) {
        if (null !== $printFormId) {
            $this->setPrintFormId($printFormId);
        }
        if (null !== $fileType) {
            $this->setFileType($fileType);
        }
    }

    /**
     *
     * @return integer
     */
    public function getPrintFormId() {
        return $this->printFormId;
    }

    /**
     *
     * @param integer $printFormId        	
     * @throws \InvalidArgumentException
     */
    public function setPrintFormId($printFormId) {
        if (!is_integer($printFormId)) {
            throw new \InvalidArgumentException('Print form id must be integer');
        }
        $this->printFormId = $printFormId;
    }

    /**
     *
     * @return boolean
     */
    public function hasPrintFormId() {
        return (null !== $this->printFormId);
    }

    /**
     *
     * @return string
     */
    public function getFileType() {
        return $this->fileType;
    }

    /**
     *
     * @param string $fileType        	
     */
    public function setFileType($fileType) {
        $this->fileType = $fileType;
    }

    /**
     *
     * @return boolean
     */
    public function hasFileType() {
        return (null !== $this->fileType);
    }

}
