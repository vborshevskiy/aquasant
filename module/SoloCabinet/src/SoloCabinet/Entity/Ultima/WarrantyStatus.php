<?php

namespace SoloCabinet\Entity\Ultima;

class WarrantyStatus {

	/**
	 *
	 * @var integer
	 */
	private $documentID = null;

	/**
	 *
	 * @var integer
	 */
	private $documentNumber = null;

	/**
	 *
	 * @var string
	 */
	private $documentDate = null;

	/**
	 *
	 * @var integer
	 */
	private $goodId = null;

	/**
	 *
	 * @var string
	 */
	private $goodName = null;

	/**
	 *
	 * @var string
	 */
	private $goodStatus = null;

	/**
	 *
	 * @var integer
	 */
	private $agentId = null;

	/**
	 *
	 * @return integer
	 */
	public function getDocumentID() {
		return $this->documentID;
	}

	/**
	 *
	 * @param integer $documentID        	
	 * @throws \InvalidArgumentException
	 */
	public function setDocumentID($documentID) {
		if (!is_integer($documentID)) {
			throw new \InvalidArgumentException('Document id must be integer');
		}
		$this->documentID = $documentID;
	}

	/**
	 *
	 * @return integer
	 */
	public function getDocumentNumber() {
		return $this->documentNumber;
	}

	/**
	 *
	 * @param integer $documentNumber        	
	 * @throws \InvalidArgumentException
	 */
	public function setDocumentNumber($documentNumber) {
		if (!is_integer($documentNumber)) {
			throw new \InvalidArgumentException('Document number must be integer');
		}
		$this->documentNumber = $documentNumber;
	}

	/**
	 *
	 * @return string
	 */
	public function getDocumentDate() {
		return $this->documentDate;
	}

	/**
	 *
	 * @param string $documentDate        	
	 */
	public function setDocumentDate($documentDate) {
		$this->documentDate = $documentDate;
	}

	/**
	 *
	 * @return integer
	 */
	public function getGoodId() {
		return $this->goodId;
	}

	/**
	 *
	 * @param integer $goodId        	
	 * @throws \InvalidArgumentException
	 */
	public function setGoodId($goodId) {
		if (!is_integer($goodId)) {
			throw new \InvalidArgumentException('Good id must be integer');
		}
		$this->goodId = $goodId;
	}

	/**
	 *
	 * @return string
	 */
	public function getGoodName() {
		return $this->goodName;
	}

	/**
	 *
	 * @param string $goodName        	
	 */
	public function setGoodName($goodName) {
		$this->goodName = $goodName;
	}

	/**
	 *
	 * @return string
	 */
	public function getGoodStatus() {
		return $this->goodStatus;
	}

	/**
	 *
	 * @param string $goodStatus        	
	 */
	public function setGoodStatus($goodStatus) {
		$this->goodStatus = $goodStatus;
	}

	/**
	 *
	 * @return integer
	 */
	public function getAgentId() {
		return $this->agentId;
	}

	/**
	 *
	 * @param integer $agentId        	
	 * @throws \InvalidArgumentException
	 */
	public function setAgentId($agentId) {
		if (!is_integer($agentId)) {
			throw new \InvalidArgumentException('Agent id must be integer');
		}
		$this->agentId = $agentId;
	}

}

?>