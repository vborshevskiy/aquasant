<?php

namespace SoloCabinet\Entity\Ultima;

class JoinReserveOptions {

	/**
	 *
	 * @var integer
	 */
	private $userId = null;

	/**
	 *
	 * @var integer
	 */
	private $agentId = null;

	/**
	 *
	 * @var integer
	 */
	private $paymentDocumentId = null;

	/**
	 *
	 * @var integer
	 */
	private $deliveryDocumentId = null;

	/**
	 *
	 * @var boolean
	 */
	private $noBonuses = true;

	/**
	 *
	 * @var boolean
	 */
	private $isOldJur = false;

	/**
	 *
	 * @throws \RuntimeException
	 * @return integer
	 */
	public function getUserId() {
		if (!$this->hasUserId()) {
			throw new \RuntimeException('User id not set');
		}
		return $this->userId;
	}

	/**
	 *
	 * @param integer $userId        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCabinet\Entity\Ultima\JoinReserveOptions
	 */
	public function setUserId($userId) {
		if (!is_integer($userId)) {
			throw new \InvalidArgumentException('User id must be integer');
		}
		$this->userId = $userId;
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasUserId() {
		return (null !== $this->userId);
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return integer
	 */
	public function getAgentId() {
		if (!$this->hasAgentId()) {
			throw new \RuntimeException('Agent id not set');
		}
		return $this->agentId;
	}

	/**
	 *
	 * @param integer $agentId        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCabinet\Entity\Ultima\JoinReserveOptions
	 */
	public function setAgentId($agentId) {
		if (!is_integer($agentId)) {
			throw new \InvalidArgumentException('Agent id must be integer');
		}
		$this->agentId = $agentId;
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasAgentId() {
		return (null !== $this->agentId);
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return integer
	 */
	public function getPaymentDocumentId() {
		if (!$this->hasPaymentDocumentId()) {
			throw new \RuntimeException('Payment document id not set');
		}
		return $this->paymentDocumentId;
	}

	/**
	 *
	 * @param integer $paymentDocumentId        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCabinet\Entity\Ultima\JoinReserveOptions
	 */
	public function setPaymentDocumentId($paymentDocumentId) {
		if (!is_integer($paymentDocumentId)) {
			throw new \InvalidArgumentException('Payment document id must be integer');
		}
		$this->paymentDocumentId = $paymentDocumentId;
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasPaymentDocumentId() {
		return (null !== $this->paymentDocumentId);
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return integer
	 */
	public function getDeliveryDocumentId() {
		if (!$this->hasDeliveryDocumentId()) {
			throw new \RuntimeException('Delivery document id not set');
		}
		return $this->deliveryDocumentId;
	}

	/**
	 *
	 * @param integer $deliveryDocumentId        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCabinet\Entity\Ultima\JoinReserveOptions
	 */
	public function setDeliveryDocumentId($deliveryDocumentId) {
		if (!is_integer($deliveryDocumentId)) {
			throw new \InvalidArgumentException('Delivery document id must be integer');
		}
		$this->deliveryDocumentId = $deliveryDocumentId;
		return $this;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasDeliveryDocumentId() {
		return (null !== $this->deliveryDocumentId);
	}

	/**
	 *
	 * @return boolean
	 */
	public function getNoBonuses() {
		return $this->noBonuses;
	}

	/**
	 *
	 * @param boolean $noBonuses        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCabinet\Entity\Ultima\JoinReserveOptions
	 */
	public function setNoBonuses($noBonuses) {
		if (!is_bool($noBonuses)) {
			throw new \InvalidArgumentException('No bonuses must be boolean');
		}
		$this->noBonuses = $noBonuses;
		return $this;
	}

	/**
	 *
	 * @param boolean $noBonuses        	
	 * @return boolean
	 */
	public function noBonuses($noBonuses = null) {
		if (null !== $noBonuses) {
			$this->setNoBonuses($noBonuses);
		} else {
			return $this->getNoBonuses();
		}
	}

	/**
	 *
	 * @return boolean
	 */
	public function getIsOldJur() {
		return $this->isOldJur;
	}

	/**
	 *
	 * @param boolean $isOldJur        	
	 * @throws \InvalidArgumentException
	 * @return \SoloCabinet\Entity\Ultima\JoinReserveOptions
	 */
	public function setIsOldJur($isOldJur) {
		if (!is_bool($isOldJur)) {
			throw new \InvalidArgumentException('Is old jur must be boolean');
		}
		$this->isOldJur = $isOldJur;
		return $this;
	}

	/**
	 *
	 * @param boolean $isOldJur        	
	 * @return boolean
	 */
	public function isOldJur($isOldJur = null) {
		if (null !== $isOldJur) {
			$this->setIsOldJur($isOldJur);
		} else {
			return $this->getIsOldJur();
		}
	}

}

?>