<?php

namespace SoloCabinet\Entity\Ultima;

class ReserveChildDocument {

	/**
	 *
	 * @var integer
	 */
	private $id;

	/**
	 *
	 * @var integer
	 */
	private $typeId;

	/**
	 *
	 * @var string
	 */
	private $typeName;

	/**
	 *
	 * @return number
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param number $id        	
	 */
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	/**
	 *
	 * @return number
	 */
	public function getTypeId() {
		return $this->typeId;
	}

	/**
	 *
	 * @param number $typeId        	
	 */
	public function setTypeId($typeId) {
		$this->typeId = $typeId;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getTypeName() {
		return $this->typeName;
	}

	/**
	 *
	 * @param string $typeName        	
	 */
	public function setTypeName($typeName) {
		$this->typeName = $typeName;
		return $this;
	}

}

?>