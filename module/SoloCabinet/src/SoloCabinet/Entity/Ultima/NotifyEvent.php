<?php

namespace SoloCabinet\Entity\Ultima;

class NotifyEvent {

	/**
	 *
	 * @var integer
	 */
	const EVENT_GOODS_INCOME = 12874;

	/**
	 *
	 * @var integer
	 */
	private $id = null;

	/**
	 *
	 * @var string
	 */
	private $name = null;

	/**
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param integer $id        	
	 * @throws \InvalidArgumentException
	 */
	public function setId($id) {
		if (!is_integer($id)) {
			throw new \InvalidArgumentException('Id must be integer');
		}
		$this->id = $id;
	}

	/**
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 *
	 * @param string $name        	
	 */
	public function setName($name) {
		$this->name = $name;
	}

}

?>