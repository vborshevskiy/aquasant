<?php

namespace SoloCabinet\Entity\Ultima;

class Draft {

	/**
	 *
	 * @var integer
	 */
	private $id = null;

	/**
	 *
	 * @var integer
	 */
	private $number = null;

	/**
	 *
	 * @var string
	 */
	private $createDate = null;

	/**
	 *
	 * @var float
	 */
	private $amount = null;

	/**
	 *
	 * @var boolean
	 */
	private $isDelivery = false;

	/**
	 *
	 * @var integer
	 */
	private $warehouseId = null;

	/**
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param integer $id        	
	 * @throws \InvalidArgumentException
	 */
	public function setId($id) {
		if (!is_integer($id)) {
			throw new \InvalidArgumentException('Id must be integer');
		}
		$this->id = $id;
	}

	/**
	 *
	 * @return integer
	 */
	public function getNumber() {
		return $this->number;
	}

	/**
	 *
	 * @param integer $number        	
	 * @throws \InvalidArgumentException
	 */
	public function setNumber($number) {
		if (!is_integer($number)) {
			throw new \InvalidArgumentException('Number must be integer');
		}
		$this->number = $number;
	}

	/**
	 *
	 * @return string
	 */
	public function getCreateDate() {
		return $this->createDate;
	}

	/**
	 *
	 * @param string $createDate        	
	 */
	public function setCreateDate($createDate) {
		$this->createDate = $createDate;
	}

	/**
	 *
	 * @return float
	 */
	public function getAmount() {
		return $this->amount;
	}

	/**
	 *
	 * @param float $amount        	
	 * @throws \InvalidArgumentException
	 */
	public function setAmount($amount) {
		if (!is_numeric($amount)) {
			throw new \InvalidArgumentException('Amount must be numeric');
		}
		$this->amount = $amount;
	}

	/**
	 *
	 * @return boolean
	 */
	public function getIsDelivery() {
		return $this->isDelivery;
	}

	/**
	 *
	 * @param boolean $isDelivery        	
	 * @throws \InvalidArgumentException
	 */
	public function setIsDelivery($isDelivery) {
		if (!is_bool($isDelivery)) {
			throw new \InvalidArgumentException('Is delivery must be boolean');
		}
		$this->isDelivery = $isDelivery;
	}

	/**
	 *
	 * @param boolean $isDelivery        	
	 * @return boolean
	 */
	public function isDelivery($isDelivery = null) {
		if (null !== $isDelivery) {
			$this->setIsDelivery($isDelivery);
		} else {
			return $this->getIsDelivery();
		}
	}

	/**
	 *
	 * @return integer
	 */
	public function getWarehouseId() {
		return $this->warehouseId;
	}

	/**
	 *
	 * @param integer $warehouseId        	
	 * @throws \InvalidArgumentException
	 */
	public function setWarehouseId($warehouseId) {
		if (!is_integer($warehouseId)) {
			throw new \InvalidArgumentException('Warehouse id must be integer');
		}
		$this->warehouseId = $warehouseId;
	}

}

?>