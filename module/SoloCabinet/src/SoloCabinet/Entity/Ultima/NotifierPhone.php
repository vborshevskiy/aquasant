<?php

namespace SoloCabinet\Entity\Ultima;

class NotifierPhone {

	/**
	 *
	 * @var integer
	 */
	private $id = null;

	/**
	 *
	 * @var string
	 */
	private $phone = null;

	/**
	 *
	 * @var string
	 */
	private $phonePrefix = null;

	/**
	 *
	 * @var NotifyEvent
	 */
	private $notifyEvent = null;

	/**
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param integer $id        	
	 * @throws \InvalidArgumentException
	 */
	public function setId($id) {
		if (!is_integer($id)) {
			throw new \InvalidArgumentException('Id must be integer');
		}
		$this->id = $id;
	}

	/**
	 *
	 * @return string
	 */
	public function getPhone() {
		return $this->phone;
	}

	/**
	 *
	 * @param string $phone        	
	 */
	public function setPhone($phone) {
		$this->phone = $phone;
	}

	/**
	 *
	 * @return string
	 */
	public function getPhonePrefix() {
		return $this->phonePrefix;
	}

	/**
	 *
	 * @param string $phonePrefix        	
	 */
	public function setPhonePrefix($phonePrefix) {
		$this->phonePrefix = $phonePrefix;
	}

	/**
	 *
	 * @return \SoloCabinet\Entity\Ultima\NotifyEvent
	 */
	public function getNotifyEvent() {
		return $this->notifyEvent;
	}

	/**
	 *
	 * @param NotifyEvent $notifyEvent        	
	 */
	public function setNotifyEvent(NotifyEvent $notifyEvent) {
		$this->notifyEvent = $notifyEvent;
	}

}

?>