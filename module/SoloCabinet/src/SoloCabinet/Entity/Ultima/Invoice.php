<?php

namespace SoloCabinet\Entity\Ultima;

class Invoice {

	/**
	 * счет с печатью
	 *
	 * @var integer
	 */
	const PRINT_FORM_WITH_PRESS = 2018;

	/**
	 *
	 * @var integer
	 */
	const PRINT_FORM_FAKTURA_JUR = 4218;

	/**
	 * накладная на оплату для физиков
	 *
	 * @var integer
	 */
	const PRINT_FORM_CONSIGNMENT = 6084;

	/**
	 * товарная накладная для бух.расхода и перемещения
	 *
	 * @var integer
	 */
	const PRINT_FORM_WAYBILL = 5478;

	/**
	 * счёт фактура для бух.расходов
	 *
	 * @var unknown
	 */
	const PRINT_FORM_COST_BANG = 5498;

	/**
	 *
	 * @var integer
	 */
	private $id = null;

	/**
	 *
	 * @var integer
	 */
	private $number = null;

	/**
	 *
	 * @var float
	 */
	private $amount = null;

	/**
	 *
	 * @var string
	 */
	private $createDate = null;

	/**
	 *
	 * @var boolean
	 */
	private $isActual = false;

	/**
	 *
	 * @var boolean
	 */
	private $isDelivery = false;

	/**
	 *
	 * @var integer
	 */
	private $warehouseId = null;

	/**
	 *
	 * @var integer
	 */
	private $addressId = null;

	/**
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param integer $id        	
	 * @throws \InvalidArgumentException
	 */
	public function setId($id) {
		if (!is_integer($id)) {
			throw new \InvalidArgumentException('Id must be integer');
		}
		$this->id = $id;
	}

	/**
	 *
	 * @return integer
	 */
	public function getNumber() {
		return $this->number;
	}

	/**
	 *
	 * @param integer $number        	
	 * @throws \InvalidArgumentException
	 */
	public function setNumber($number) {
		if (!is_integer($number)) {
			throw new \InvalidArgumentException('Number must be integer');
		}
		$this->number = $number;
	}

	/**
	 *
	 * @return float
	 */
	public function getAmount() {
		return $this->amount;
	}

	/**
	 *
	 * @param float $amount        	
	 * @throws \InvalidArgumentException
	 */
	public function setAmount($amount) {
		if (!is_numeric($amount)) {
			throw new \InvalidArgumentException('Amount must be numeric');
		}
		$this->amount = $amount;
	}

	/**
	 *
	 * @return string
	 */
	public function getCreateDate() {
		return $this->createDate;
	}

	/**
	 *
	 * @param string $createDate        	
	 */
	public function setCreateDate($createDate) {
		$this->createDate = $createDate;
	}

	/**
	 *
	 * @return boolean
	 */
	public function getIsActual() {
		return $this->isActual;
	}

	/**
	 *
	 * @param boolean $isActual        	
	 * @throws \InvalidArgumentException
	 */
	public function setIsActual($isActual) {
		if (!is_bool($isActual)) {
			throw new \InvalidArgumentException('Is actual must be boolean');
		}
		$this->isActual = $isActual;
	}

	/**
	 *
	 * @param boolean $isActual        	
	 * @return boolean
	 */
	public function isActual($isActual = null) {
		if (null !== $isActual) {
			$this->setIsActual($isActual);
		} else {
			return $this->getIsActual();
		}
	}

	/**
	 *
	 * @return boolean
	 */
	public function getIsDelivery() {
		return $this->isDelivery;
	}

	/**
	 *
	 * @param boolean $isDelivery        	
	 * @throws \InvalidArgumentException
	 */
	public function setIsDelivery($isDelivery) {
		if (!is_bool($isDelivery)) {
			throw new \InvalidArgumentException('Is delivery must be boolean');
		}
		$this->isDelivery = $isDelivery;
	}

	/**
	 *
	 * @param boolean $isDelivery        	
	 * @return boolean
	 */
	public function isDelivery($isDelivery = null) {
		if (null !== $isDelivery) {
			$this->setIsDelivery($isDelivery);
		} else {
			return $this->getIsDelivery();
		}
	}

	/**
	 *
	 * @return integer
	 */
	public function getWarehouseId() {
		return $this->warehouseId;
	}

	/**
	 *
	 * @param integer $warehouseId        	
	 * @throws \InvalidArgumentException
	 */
	public function setWarehouseId($warehouseId) {
		if (!is_integer($warehouseId)) {
			throw new \InvalidArgumentException('Warehouse id must be integer');
		}
		$this->warehouseId = $warehouseId;
	}

	/**
	 *
	 * @return integer
	 */
	public function getAddressId() {
		return $this->addressId;
	}

	/**
	 *
	 * @param integer $addressId        	
	 * @throws \InvalidArgumentException
	 */
	public function setAddressId($addressId) {
		if (!is_integer($addressId)) {
			throw new \InvalidArgumentException('Address id must be integer');
		}
		$this->addressId = $addressId;
	}

}

?>