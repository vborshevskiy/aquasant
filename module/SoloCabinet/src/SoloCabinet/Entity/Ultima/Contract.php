<?php

namespace SoloCabinet\Entity\Ultima;

class Contract {

	/**
	 *
	 * @var integer
	 */
	private $id = null;

	/**
	 *
	 * @var integer
	 */
	private $number = null;

	/**
	 *
	 * @var string
	 */
	private $activationDate = null;

	/**
	 *
	 * @var float
	 */
	private $amount = null;

	/**
	 *
	 * @var string
	 */
	private $period = null;

	/**
	 *
	 * @var string
	 */
	private $directorPersonName = null;

	/**
	 *
	 * @var string
	 */
	private $firmName = null;

	/**
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param integer $id        	
	 * @throws \InvalidArgumentException
	 */
	public function setId($id) {
		if (!is_integer($id)) {
			throw new \InvalidArgumentException('Id must be integer');
		}
		$this->id = $id;
	}

	/**
	 *
	 * @return integer
	 */
	public function getNumber() {
		return $this->number;
	}

	/**
	 *
	 * @param integer $number        	
	 * @throws \InvalidArgumentException
	 */
	public function setNumber($number) {
		if (!is_integer($number)) {
			throw new \InvalidArgumentException('Number must be integer');
		}
		$this->number = $number;
	}

	/**
	 *
	 * @return string
	 */
	public function getActivationDate() {
		return $this->activationDate;
	}

	/**
	 *
	 * @param string $activationDate        	
	 * @throws \InvalidArgumentException
	 */
	public function setActivationDate($activationDate) {
		if (empty($activationDate)) {
			throw new \InvalidArgumentException('Activation date must be non-empty');
		}
		$this->activationDate = $activationDate;
	}

	/**
	 *
	 * @return float
	 */
	public function getAmount() {
		return $this->amount;
	}

	/**
	 *
	 * @param float $amount        	
	 * @throws \InvalidArgumentException
	 */
	public function setAmount($amount) {
		if (!is_numeric($amount)) {
			throw new \InvalidArgumentException('Amount must be numeric');
		}
		$this->amount = $amount;
	}

	/**
	 *
	 * @return string
	 */
	public function getPeriod() {
		return $this->period;
	}

	/**
	 *
	 * @param string $period        	
	 */
	public function setPeriod($period) {
		$this->period = $period;
	}

	/**
	 *
	 * @return string
	 */
	public function getDirectorPersonName() {
		return $this->directorPersonName;
	}

	/**
	 *
	 * @param string $directorPersonName        	
	 */
	public function setDirectorPersonName($directorPersonName) {
		$this->directorPersonName = $directorPersonName;
	}

	/**
	 *
	 * @return string
	 */
	public function getFirmName() {
		return $this->firmName;
	}

	/**
	 *
	 * @param string $firmName        	
	 */
	public function setFirmName($firmName) {
		$this->firmName = $firmName;
	}

}

?>