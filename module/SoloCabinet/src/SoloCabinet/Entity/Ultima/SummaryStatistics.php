<?php

namespace SoloCabinet\Entity\Ultima;

class SummaryStatistics {

	/**
	 *
	 * @var float
	 */
	private $balance = 0;

	/**
	 *
	 * @var integer
	 */
	private $allReservesCount = 0;

	/**
	 *
	 * @var integer
	 */
	private $activeReservesCount = 0;

	/**
	 *
	 * @var integer
	 */
	private $processedReservesCount = 0;

	/**
	 *
	 * @var integer
	 */
	private $finishedReservesCount = 0;

	/**
	 *
	 * @var integer
	 */
	private $draftsCount = 0;

	/**
	 *
	 * @return float
	 */
	public function getBalance() {
		return $this->balance;
	}

	/**
	 *
	 * @param float $balance        	
	 * @throws \InvalidArgumentException
	 */
	public function setBalance($balance) {
		if (!is_numeric($balance)) {
			throw new \InvalidArgumentException('Balance must be numeric');
		}
		$this->balance = $balance;
	}

	/**
	 *
	 * @return integer
	 */
	public function getAllReservesCount() {
		return $this->allReservesCount;
	}

	/**
	 *
	 * @param integer $allReservesCount        	
	 * @throws \InvalidArgumentException
	 */
	public function setAllReservesCount($allReservesCount) {
		if (!is_integer($allReservesCount)) {
			throw new \InvalidArgumentException('All reserves count must be integer');
		}
		$this->allReservesCount = $allReservesCount;
	}

	/**
	 *
	 * @return integer
	 */
	public function getActiveReservesCount() {
		return $this->activeReservesCount;
	}

	/**
	 *
	 * @param integer $activeReservesCount        	
	 * @throws \InvalidArgumentException
	 */
	public function setActiveReservesCount($activeReservesCount) {
		if (!is_integer($activeReservesCount)) {
			throw new \InvalidArgumentException('Active reserves count must be integer');
		}
		$this->activeReservesCount = $activeReservesCount;
	}

	/**
	 *
	 * @return integer
	 */
	public function getProcessedReservesCount() {
		return $this->processedReservesCount;
	}

	/**
	 *
	 * @param integer $processedReservesCount        	
	 * @throws \InvalidArgumentException
	 */
	public function setProcessedReservesCount($processedReservesCount) {
		if (!is_integer($processedReservesCount)) {
			throw new \InvalidArgumentException('Processed reserves count must be integer');
		}
		$this->processedReservesCount = $processedReservesCount;
	}

	/**
	 *
	 * @return integer
	 */
	public function getFinishedReservesCount() {
		return $this->finishedReservesCount;
	}

	/**
	 *
	 * @param integer $finishedReservesCount        	
	 * @throws \InvalidArgumentException
	 */
	public function setFinishedReservesCount($finishedReservesCount) {
		if (!is_integer($finishedReservesCount)) {
			throw new \InvalidArgumentException('Finished reserves count must be integer');
		}
		$this->finishedReservesCount = $finishedReservesCount;
	}

	/**
	 *
	 * @return integer
	 */
	public function getDraftsCount() {
		return $this->draftsCount;
	}

	/**
	 *
	 * @param integer $draftsCount        	
	 * @throws \InvalidArgumentException
	 */
	public function setDraftsCount($draftsCount) {
		if (!is_integer($draftsCount)) {
			throw new \InvalidArgumentException('Drafts count must be integer');
		}
		$this->draftsCount = $draftsCount;
	}

}

?>