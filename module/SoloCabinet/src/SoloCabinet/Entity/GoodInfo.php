<?php

namespace SoloCabinet\Entity;

/**
 * Description of GoodInfo
 *
 * @author slava
 */
class GoodInfo {

	private $goodId = null;

	private $goodName = null;

	private $goodEngName = null;

	public function getGoodId() {
		return $this->goodId;
	}

	public function setGoodId($goodId) {
		$this->goodId = $goodId;
	}

	public function getGoodName() {
		return $this->goodName;
	}

	public function setGoodName($goodName) {
		$this->goodName = $goodName;
	}

	public function getGoodEngName() {
		return $this->goodEngName;
	}

	public function setGoodEngName($goodEngName) {
		$this->goodEngName = $goodEngName;
	}

	public function exchangeArray($data) {
		if (isset($data['GoodID'])) $this->setGoodId(intval($data['GoodID']));
		if (isset($data['GoodName'])) $this->setGoodName($data['GoodName']);
		if (isset($data['GoodEngName'])) $this->setGoodEngName($data['GoodEngName']);
	}

}

?>
