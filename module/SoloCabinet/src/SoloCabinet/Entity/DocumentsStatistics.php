<?php

namespace SoloCabinet\Entity;

class DocumentsStatistics {

    /**
     *
     * @var integer
     */
    private $totalCount = null;

    /**
     *
     * @var integer
     */
    private $reservesCount = null;

    /**
     *
     * @var integer
     */
    private $draftsCount = null;

    /**
     *
     * @var integer
     */
    private $shippingCount = null;

    /**
     *
     * @var integer
     */
    private $shippedCount = null;

    /**
     *
     * @var string
     */
    private $maxDate = null;

    /**
     *
     * @var string
     */
    private $minDate = null;

    /**
     *
     * @var float
     */
    private $maxCost = null;

    /**
     *
     * @var float
     */
    private $minCost = null;

    /**
     *
     * @return integer
     */
    public function getTotalCount() {
        return $this->totalCount;
    }

    /**
     *
     * @return integer
     */
    public function getReservesCount() {
        return $this->reservesCount;
    }

    /**
     *
     * @return integer
     */
    public function getDraftsCount() {
        return $this->draftsCount;
    }

    /**
     *
     * @return integer
     */
    public function getShippingCount() {
        return $this->shippingCount;
    }

    /**
     *
     * @return integer
     */
    public function getShippedCount() {
        return $this->shippedCount;
    }

    /**
     *
     * @param integer $totalCount        	
     * @throws \InvalidArgumentException
     * @throws \OutOfBoundsException
     */
    public function setTotalCount($totalCount) {
        if (!is_integer($totalCount)) {
            throw new \InvalidArgumentException('Total count must be integer');
        }
        if (0 > $totalCount) {
            throw new \OutOfBoundsException('Total count can\'t be lower than zero');
        }
        $this->totalCount = $totalCount;
    }

    /**
     *
     * @param integer $reservesCount        	
     */
    public function setReservesCount($reservesCount) {
        $this->reservesCount = $reservesCount;
    }

    /**
     *
     * @param integer $draftsCount        	
     */
    public function setDraftsCount($draftsCount) {
        $this->$draftsCount = $draftsCount;
    }

    /**
     *
     * @param integer $shippingCount       	
     */
    public function setShippingCount($shippingCount) {
        $this->shippingCount = $shippingCount;
    }

    /**
     *
     * @param integer $shippedCount        	
     */
    public function setShippedCount($shippedCount) {
        $this->shippedCount = $shippedCount;
    }


    /**
     *
     * @return string
     */
    public function getMaxDate() {
        return $this->maxDate;
    }

    /**
     *
     * @param string $maxDate        	
     * @throws \InvalidArgumentException
     */
    public function setMaxDate($maxDate) {
        if (empty($maxDate)) {
            throw new \InvalidArgumentException('Max date must be non-empty');
        }
        $this->maxDate = $maxDate;
    }

    /**
     *
     * @return string
     */
    public function getMinDate() {
        return $this->minDate;
    }

    /**
     *
     * @param string $minDate        	
     * @throws \InvalidArgumentException
     */
    public function setMinDate($minDate) {
        if (empty($minDate)) {
            throw new \InvalidArgumentException('Min date must be non-empty');
        }
        $this->minDate = $minDate;
    }

    /**
     *
     * @return float
     */
    public function getMaxCost() {
        return $this->maxCost;
    }

    /**
     *
     * @param float $maxCost        	
     * @throws \InvalidArgumentException
     * @throws \OutOfBoundsException
     */
    public function setMaxCost($maxCost) {
        if (!is_numeric($maxCost)) {
            throw new \InvalidArgumentException('Max cost must be numeric');
        }
        if (0 > floatval($maxCost)) {
            throw new \OutOfBoundsException('Max cost can\'t be lower that zero');
        }
        $this->maxCost = floatval($maxCost);
    }

    /**
     *
     * @return float
     */
    public function getMinCost() {
        return $this->minCost;
    }

    /**
     *
     * @param float $minCost        	
     * @throws \InvalidArgumentException
     * @throws \OutOfBoundsException
     */
    public function setMinCost($minCost) {
        if (!is_numeric($minCost)) {
            throw new \InvalidArgumentException('Min cost must be numeric');
        }
        if (0 > floatval($minCost)) {
            throw new \OutOfBoundsException('Min cost can\'t be lower that zero');
        }
        $this->minCost = floatval($minCost);
    }

    /**
     *
     * @param string $min        	
     * @param string $max        	
     */
    public function setDateLimits($min, $max) {
        $this->setMinDate($min);
        $this->setMaxDate($max);
    }

    /**
     *
     * @param float $min        	
     * @param float $max        	
     */
    public function setCostLimits($min, $max) {
        $this->setMinCost($min);
        $this->setMaxCost($max);
    }

}

?>