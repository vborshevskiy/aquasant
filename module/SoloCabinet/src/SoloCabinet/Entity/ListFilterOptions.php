<?php

namespace SoloCabinet\Entity;

class ListFilterOptions {

	/**
	 *
	 * @var integer
	 */
	private $page = 0;

	/**
	 *
	 * @var integer
	 */
	private $itemsPerPage = null;

	/**
	 *
	 * @var string
	 */
	private $sortColumn = null;

	/**
	 *
	 * @var string
	 */
	private $sortDirection = null;

	/**
	 *
	 * @return integer
	 */
	public function getPage() {
		return $this->page;
	}

	/**
	 *
	 * @param integer $page        	
	 * @throws \InvalidArgumentException
	 * @throws \OutOfRangeException
	 */
	public function setPage($page) {
		if (!is_integer($page)) {
			throw new \InvalidArgumentException('Page must be integer');
		}
		if (0 > $page) {
			throw new \OutOfRangeException('Page can\'t be lower than zero');
		}
		$this->page = $page;
	}

	/**
	 * Resets page to its default value
	 */
	public function clearPage() {
		$this->page = 0;
	}

	/**
	 *
	 * @return integer
	 */
	public function getItemsPerPage() {
		return $this->itemsPerPage;
	}

	/**
	 *
	 * @param integer $itemsPerPage        	
	 * @throws \InvalidArgumentException
	 * @throws \OutOfRangeException
	 */
	public function setItemsPerPage($itemsPerPage) {
		if (!is_integer($itemsPerPage)) {
			throw new \InvalidArgumentException('Page must be integer');
		}
		if (0 >= $itemsPerPage) {
			throw new \OutOfRangeException('Page can\'t be lower or equals than zero');
		}
		$this->itemsPerPage = $itemsPerPage;
	}

	/**
	 * Resets items per page
	 */
	public function clearItemsPerPage() {
		$this->itemsPerPage = null;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasItemsPerPage() {
		return (null !== $this->itemsPerPage);
	}

	/**
	 *
	 * @return string
	 */
	public function getSortColumn() {
		return $this->sortColumn;
	}

	/**
	 *
	 * @param string $sortColumn        	
	 */
	public function setSortColumn($sortColumn) {
		$this->sortColumn = $sortColumn;
	}

	/**
	 * Resets sort column
	 */
	public function clearSortColumn() {
		$this->sortColumn = null;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasSortColumn() {
		return (null !== $this->sortColumn);
	}

	/**
	 *
	 * @return string
	 */
	public function getSortDirection() {
		return $this->sortDirection;
	}

	/**
	 *
	 * @param string $sortDirection        	
	 * @throws \InvalidArgumentException
	 */
	public function setSortDirection($sortDirection) {
		if (!in_array(strtoupper($sortDirection), [
			'ASC',
			'DESC' 
		])) {
			throw new \InvalidArgumentException('Sort direction must be ASC or DESC');
		}
		$this->sortDirection = $sortDirection;
	}

	/**
	 * Resets sort direction
	 */
	public function clearSortDirection() {
		$this->sortDirection = null;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasSortDirection() {
		return (null !== $this->sortDirection);
	}

	/**
	 *
	 * @param string $column        	
	 * @param string $direction        	
	 */
	public function setSorting($column, $direction) {
		$this->setSortColumn($column);
		$this->setSortDirection($direction);
	}

	/**
	 * Clear sort options to its default values
	 */
	public function clear() {
		$this->clearPage();
		$this->clearItemsPerPage();
		$this->clearSortColumn();
		$this->clearSortDirection();
	}

}

?>