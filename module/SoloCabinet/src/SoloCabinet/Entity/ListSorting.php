<?php

namespace SoloCabinet\Entity;

class ListSorting implements \Serializable {

	/**
	 * Ascending sort direction
	 *
	 * @var string
	 */
	const SORT_DIRECTION_ASC = 'ASC';

	/**
	 * Descending sort direction
	 *
	 * @var string
	 */
	const SORT_DIRECTION_DESC = 'DESC';

	/**
	 * Sort column name
	 *
	 * @var string
	 */
	protected $column = null;

	/**
	 * Direction of sorting
	 *
	 * @var string
	 */
	protected $direction = null;

	/**
	 *
	 * @param string $sortColumn        	
	 * @param string $sortDirection        	
	 */
	public function __construct($column = null, $direction = null) {
		if (null !== $column) {
			$this->setColumn($column);
		}
		if (null !== $direction) {
			$this->setDirection($direction);
		}
	}

	/**
	 *
	 * @param string $column        	
	 */
	public function setColumn($column) {
		$this->column = $column;
	}

	/**
	 *
	 * @return string
	 */
	public function getColumn() {
		return $this->column;
	}

	/**
	 *
	 * @param string $direction        	
	 * @throws \InvalidArgumentException
	 */
	public function setDirection($direction) {
		$direction = strtoupper($direction);
		if (!in_array($direction, [
			self::SORT_DIRECTION_ASC,
			self::SORT_DIRECTION_DESC 
		])) {
			throw new \InvalidArgumentException('Invalid sort direction \'' . $direction . '\'');
		}
		$this->direction = $direction;
	}

	/**
	 *
	 * @return string
	 */
	public function getDirection() {
		return $this->direction;
	}

	/**
	 * Switch sort direction
	 *
	 * @return string
	 */
	public function triggerDirection() {
		if ($this->direction == self::SORT_DIRECTION_ASC) {
			$this->setDirection(self::SORT_DIRECTION_DESC);
		} else {
			$this->setDirection(self::SORT_DIRECTION_ASC);
		}
		return $this->getDirection();
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Serializable::serialize()
	 */
	public function serialize() {
		return json_decode([
			'column' => $this->getColumn(),
			'direction' => $this->getDirection() 
		]);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see Serializable::unserialize()
	 */
	public function unserialize($serialized) {
		$unserialized = json_encode($serialized);
		$this->setColumn($unserialized['column']);
		$this->setDirection($unserialized['direction']);
	}

}

?>