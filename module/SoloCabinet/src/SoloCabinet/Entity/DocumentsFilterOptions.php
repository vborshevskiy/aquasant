<?php

namespace SoloCabinet\Entity;

use SoloCabinet\Entity\ListFilterOptions;

class DocumentsFilterOptions extends ListFilterOptions {

	/**
	 *
	 * @var string
	 */
	private $documentNumber = null;

	/**
	 *
	 * @var string
	 */
	private $dateFrom = null;

	/**
	 *
	 * @var string
	 */
	private $dateTo = null;

	/**
	 *
	 * @var float
	 */
	private $costFrom = null;

	/**
	 *
	 * @var float
	 */
	private $costTo = null;

	/**
	 *
	 * @return string
	 */
	public function getDocumentNumber() {
		return $this->documentNumber;
	}

	/**
	 *
	 * @param string $documentNumber        	
	 * @throws \InvalidArgumentException
	 */
	public function setDocumentNumber($documentNumber) {
		if (empty($documentNumber)) {
			throw new \InvalidArgumentException('Document number must be non-empty');
		}
		$this->documentNumber = $documentNumber;
	}

	/**
	 * Resets document number
	 */
	public function clearDocumentNumber() {
		$this->documentNumber = null;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasDocumentNumber() {
		return (null !== $this->documentNumber);
	}

	/**
	 *
	 * @return string
	 */
	public function getDateFrom() {
		return $this->dateFrom;
	}

	/**
	 *
	 * @param string $dateFrom        	
	 * @throws \InvalidArgumentException
	 */
	public function setDateFrom($dateFrom) {
		if (empty($dateFrom)) {
			throw new \InvalidArgumentException('Date from must be non-empty');
		}
		$this->dateFrom = $dateFrom;
	}

	/**
	 * Resets date from
	 */
	public function clearDateFrom() {
		$this->dateFrom = null;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasDateFrom() {
		return (null !== $this->dateFrom);
	}

	/**
	 *
	 * @return string
	 */
	public function getDateTo() {
		return $this->dateTo;
	}

	/**
	 *
	 * @param string $dateTo        	
	 * @throws \InvalidArgumentException
	 */
	public function setDateTo($dateTo) {
		if (empty($dateTo)) {
			throw new \InvalidArgumentException('Date to must be non-empty');
		}
		$this->dateTo = $dateTo;
	}

	/**
	 * Resets date to
	 */
	public function clearDateTo() {
		$this->dateTo = null;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasDateTo() {
		return (null !== $this->dateTo);
	}

	/**
	 *
	 * @return float
	 */
	public function getCostFrom() {
		return $this->costFrom;
	}

	/**
	 *
	 * @param float $costFrom        	
	 * @throws \InvalidArgumentException
	 */
	public function setCostFrom($costFrom) {
		if (empty($costFrom)) {
			throw new \InvalidArgumentException('Cost from must be non-empty');
		}
		if (!is_numeric($costFrom)) {
			;
			throw new \InvalidArgumentException('Cost from must be numeric');
		}
		$this->costFrom = $costFrom;
	}

	/**
	 * Resets cost from
	 */
	public function clearCostFrom() {
		$this->costFrom = null;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasCostFrom() {
		return (null !== $this->costFrom);
	}

	/**
	 *
	 * @return float
	 */
	public function getCostTo() {
		return $this->costTo;
	}

	/**
	 *
	 * @param float $costTo        	
	 * @throws \InvalidArgumentException
	 */
	public function setCostTo($costTo) {
		if (empty($costTo)) {
			throw new \InvalidArgumentException('Cost to must be non-empty');
		}
		if (!is_numeric($costTo)) {
			throw new \InvalidArgumentException('Cost to must be numeric');
		}
		$this->costTo = $costTo;
	}

	/**
	 * Resets cost to
	 */
	public function clearCostTo() {
		$this->costTo = null;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasCostTo() {
		return (null !== $this->costTo);
	}

	/**
	 *
	 * @param string $from        	
	 * @param string $to        	
	 */
	public function setDateRange($from, $to) {
		$this->setDateFrom($from);
		$this->setDateTo($to);
	}

	/**
	 *
	 * @param float $from        	
	 * @param float $to        	
	 */
	public function setCostRange($from, $to) {
		$this->setCostFrom($from);
		$this->setCostTo($to);
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \SoloCabinet\Entity\ListFilterOptions::clear()
	 */
	public function clear() {
		parent::clear();
		$this->clearDocumentNumber();
		$this->clearDateFrom();
		$this->clearDateTo();
		$this->clearCostFrom();
		$this->clearCostTo();
	}

}

?>