<?php

namespace SoloCabinet\Data;

use Solo\Db\QueryGateway\QueryGateway;

class GoodInfoTable extends QueryGateway {

	/**
	 * 
	 * @param array $ids
	 * @return \Zend\Db\ResultSet\ResultSet
	 */
	public function getGoods(array $ids) {
		$sql = "SELECT ag.GoodID, ag.GoodName, ag.GoodEngName FROM #all_goods# ag WHERE ag.GoodID IN (" . implode(',', $ids) . ")";
		return $this->query($sql);
	}

}

?>
