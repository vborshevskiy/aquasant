<?php

namespace SoloCabinet\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use SoloCabinet\Service\Ultima\AllReservesService;
use SoloCabinet\Service\Ultima\ReservesService;
use SoloCabinet\Service\Ultima\InvoicesService;
use SoloCabinet\Service\Ultima\BonusService;

class Cabinet extends AbstractPlugin {

    /**
     * Calls CabinetService methods
     *
     * @param string $name        	
     * @param array $arguments        	
     * @throws \BadMethodCallException
     * @return mixed
     */
    public function __call($name, $arguments) {
        $cabinet = $this->getController()->getServiceLocator()->get('\\SoloCabinet\\Service\\CabinetService');
        if (!method_exists($cabinet, $name)) {
            throw new \BadMethodCallException('Invalid cabinet method: ' . $name);
        }
        return call_user_func_array([
            $cabinet,
            $name
                ], $arguments);
    }

    /**
     *
     * @return AllReservesService
     */
    public function allReserves() {
        return $this->getController()->getServiceLocator()->get('\\SoloCabinet\\Service\\AllReservesService');
    }

    /**
     *
     * @return ReservesService
     */
    public function reserves() {
        return $this->getController()->getServiceLocator()->get('\\SoloCabinet\\Service\\ReservesService');
    }

    /**
     *
     * @return InvoicesService
     */
    public function invoices() {
        return $this->getController()->getServiceLocator()->get('\\SoloCabinet\\Service\\InvoicesService');
    }

    /**
     *
     * @return BonusService
     */
    public function bonus() {
        return $this->getController()->getServiceLocator()->get('\\SoloCabinet\\Service\\BonusService');
    }

}

