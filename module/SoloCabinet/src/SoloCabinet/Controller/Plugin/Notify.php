<?php

namespace SoloCabinet\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use SoloCabinet\Service\Ultima\NotifyService;

class Notify extends AbstractPlugin {

	/**
	 *
	 * @return NotifyService
	 */
	public function __invoke() {
		return $this->getController()->getServiceLocator()->get('\\SoloCabinet\\Service\\NotifyService');
	}

}

?>