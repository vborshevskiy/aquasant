<?php

namespace SoloDelivery\Controller\Plugin;

use Solo\Mvc\Controller\Plugin\AbstractPlugin;

class PostDelivery extends AbstractPlugin {

    /**
     * 
     * @param string $name
     * @param mixed $arguments
     * @return \SoloDelivery\Service\PostDeliveryService
     * @throws \BadMethodCallException
     */
    public function __call($name, $arguments) {
        $service = $this->getController()->getServiceLocator()->get('\\SoloDelivery\\Service\\PostDeliveryService');
        if (!method_exists($service, $name)) {
            throw new \BadMethodCallException('Invalid delivery method: ' . $name);
        }
        return call_user_func_array(array(
            $service,
            $name
        ), $arguments);
    }
    
    /**
     * 
     * @return \SoloDelivery\Service\PostDeliveryCalculatorService
     */
    public function calculator() {
        $service = $this->getController()->getServiceLocator()->get('\\SoloDelivery\\Service\\PostDeliveryCalculatorService');
        return $service;
    }

}
