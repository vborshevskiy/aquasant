<?php

namespace SoloDelivery\Controller\Plugin;

class Delivery extends \Zend\Mvc\Controller\Plugin\AbstractPlugin {

    /**
     * 
     * @param string $name
     * @param mixed $arguments
     * @return \SoloDelivery\Service\DeliveryService
     * @throws \BadMethodCallException
     */
    public function __call($name, $arguments) {
        $delivery = $this->getController()->getServiceLocator()->get('delivery_service');
        if (!method_exists($delivery, $name)) {
            throw new \BadMethodCallException('Invalid delivery method: ' . $name);
        }
        return call_user_func_array(array(
            $delivery,
            $name
        ), $arguments);
    }

    /**
     *
     * @return \SoloDelivery\Service\LogisticCompaniesService
     */
    public function logisticCompanies() {
        return $this->getController()->getServiceLocator()->get('\\SoloDelivery\\Service\\LogisticCompaniesService');
    }

    /**
     *
     * @return \SoloDelivery\Service\DeliveryTimeRangeService
     */
    public function deliveryTimeRange() {
        return $this->getController()->getServiceLocator()->get('\\SoloDelivery\\Service\\DeliveryTimeRangeService');
    }

}
