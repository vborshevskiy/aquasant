<?php

namespace SoloDelivery\Controller\Plugin;

use Solo\Mvc\Controller\Plugin\AbstractPlugin;

class PostPickup extends AbstractPlugin {
    
    /**
     * 
     * @return \SoloDelivery\Service\TkPekService
     */
    public function tkPek() {
        $service = $this->getController()->getServiceLocator()->get('\\SoloDelivery\\Service\\TkPekService');
        return $service;
    }
}
