<?php

namespace SoloDelivery\Controller\Plugin;

use Solo\Mvc\Controller\Plugin\AbstractPlugin;

class Region extends AbstractPlugin {

    /**
     * 
     * @param string $name
     * @param mixed $arguments
     * @return \SoloDelivery\Service\RegionService
     * @throws \BadMethodCallException
     */
    public function __call($name, $arguments) {
        $service = $this->getController()->getServiceLocator()->get('\\SoloDelivery\\Service\\RegionService');
        if (!method_exists($service, $name)) {
            throw new \BadMethodCallException('Invalid region method: ' . $name);
        }
        return call_user_func_array(array(
            $service,
            $name
        ), $arguments);
    }

}
