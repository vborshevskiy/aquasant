<?php

namespace SoloDelivery\Controller;

use Solo\Mvc\Controller\ActionController;

class ConsoleController extends ActionController {

    public function shippedAction() {
        $addingData = [];
        foreach ($this->region()->getShippedAddingRegions() as $region) {
            if ($region['UpdateShippedChance'] > rand(0,100)) {
                $addingData[(int)$region['RegionID']] = rand(1, $region['MaxShippedAdding']);
            }
        }
        $this->region()->addShipped($addingData);
        
        $rootAddingData = [];
        foreach ($this->region()->getRootShippedSummary() as $rootShipped) {
            $rootAddingData[(int)$rootShipped['ParentRegionID']] = (int)$rootShipped['ShippedSum'];
        }
        $this->region()->addShipped($rootAddingData);
        echo 'finished';
        exit;
    }
    
    public function deliveryDaysAction() {
        $config = $this->getServiceLocator()->get('config');
        $result = [];
        
        $regions = $this->region()->getAllRootRegions();
        foreach ($regions as $region) {
            sleep(1);
            echo 'Region: ' . $region->getEmsId() . "\r\n";
            $transportCompnanies[] = [
                'uid' => 'ems',
                'to' => $region->getEmsId(),
            ];

            $goodsParams = [
                [
                    'goodId' => 0,
                    'quantity' => 1,
                    'volume' => 0.001,
                    'weight' => 1,
                ],
            ];

            $arrivalDate = new \DateTime('now + 4 days');
            $additionalDaysCount = (isset($config['post_delivery_additional_days_count']) ? (int)$config['post_delivery_additional_days_count'] : 0);
            $arrivalDate->modify('+' . $additionalDaysCount . ' ' . 'days');

            $goodsPrice = 3000;
            $additionalTkDeliveryPricesByGoodIds = 0;

            $deliveryDays = $this->postDelivery()->calculator()->getPrice($transportCompnanies, $goodsParams, $arrivalDate, $goodsPrice, $additionalTkDeliveryPricesByGoodIds)->offsetGet('ems')->getDeliveryMinTerm();
            $result[$region->getId()] = $deliveryDays;
        }
        
        $this->region()->updateDeliveryDays($result);
        
    }

}
