<?php

namespace SoloDelivery\Provider;

use Solo\Inflector\Inflector;

abstract class ProviderFactory {

    /**
     *
     * @param string $provider        	
     * @param array $options        	
     * @throws \InvalidArgumentException
     * @return ProviderInterface
     */
    public static function factory($provider, $options) {
        if (empty($provider)) {
            throw new \InvalidArgumentException('Provider can\'t be empty');
        }
        if (!is_array($options)) {
            throw new \InvalidArgumentException('Options must be array');
        }
        $className = __NAMESPACE__ . '\\' . Inflector::camelize($provider) . 'Provider';
        $class = new $className($options);
        $class->setClient(new \Zend\Http\Client());
        return $class;
    }

}
