<?php

namespace SoloDelivery\Provider;

final class EmsProvider extends BaseProvider {

    /**
     * 
     * @param array $options
     */
    public function __construct($options) {
        if (isset($options['path'])) {
            $this->path = $options['path'];
        }
        if (isset($options['method'])) {
            $this->method = $options['method'];
        }
        if (isset($options['from'])) {
            $this->from = $options['from'];
        }
    }
    
    /**
     * 
     * @return array | null
     * @throws \InvalidArgumentException
     */
    public function call() {
        if (empty($this->path)) {
            throw new \InvalidArgumentException('Path can\'t be empty');
        }
        if (empty($this->method)) {
            throw new \InvalidArgumentException('Method can\'t be empty');
        }
        if (count($this->goods) > 0) {
            $this->client->setMethod('POST');
            $this->client->setAdapter('Zend\Http\Client\Adapter\Curl');
            $this->client->setOptions([
                'curloptions' => [
                    CURLOPT_ENCODING => 'gzip',
                ],
            ]);
            $uri = new \Zend\Uri\Http($this->path);
            $uri->setQuery([
                'method' => $this->method,
                'from' => $this->from,
                'to' => $this->to,
                'weight' => $this->getSumWeight(),
            ]);
            $this->client->setUri($uri);
            try {
                $response = $this->client->send();
            } catch (\Exception $e) {
                return null;
            }
            return $response->getContent();
        }
        return null;
    }

}