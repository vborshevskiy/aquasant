<?php

namespace SoloDelivery\Provider;

final class SdekProvider extends BaseProvider {
    
    /**
     *
     * @var string
     */
    private $login;
    
    /**
     *
     * @var string
     */
    private $pass;
    
    /**
     *
     * @var string
     */
    private $version = '1.0';
    
    /**
     *
     * @var integer
     */
    private $tariffId;

    /**
     * 
     * @param array $options
     */
    public function __construct($options) {
        if (isset($options['path'])) {
            $this->path = $options['path'];
        }
        if (isset($options['from'])) {
            $this->from = (int)$options['from'];
        }
        if (isset($options['login'])) {
            $this->login = $options['login'];
        }
        if (isset($options['pass'])) {
            $this->pass = $options['pass'];
        }
        if (isset($options['version'])) {
            $this->version = $options['version'];
        }
        if (isset($options['tariffId'])) {
            $this->tariffId = $options['tariffId'];
        }
    }
    
    /**
     * 
     * Через Client и Curl побороть api СДЭК в разумные сроки не удалось, используется стандартная библиотека php.
     * 
     * @return array
     * @throws \InvalidArgumentException
     */
//    public function call() {
//        if (empty($this->path)) {
//            throw new \InvalidArgumentException('Path can\'t be empty');
//        }
//        
//        $goods = [];
//        foreach ($this->goods as $good) {
//            $goods[] = new \ArrayObject([
//                'volume' => $good['volume'],
//                'weight' => $good['weight'],
//            ]);
//        }
//        $params = [
//            'goods' => $goods,
//            'version' => $this->version,
//            'senderCityId' => $this->from,
//            'receiverCityId' => $this->to,
//        ];
//        
//        $this->client->setAdapter('Zend\Http\Client\Adapter\Curl');
//        $this->client->getRequest()->getHeaders()->addHeaders([
//            'Content-Type: application/json;charset=utf-8',
//        ]);
//        $this->client->setOptions([
//            'curloptions' => [
//                CURLOPT_POST => true,
//                CURLOPT_RETURNTRANSFER => true,
//            ],
//        ]);
//        $uri = new \Zend\Uri\Http($this->path);
//        $this->client->setUri($uri);
//        $this->client->setParameterPost($params);
//        
//        try {
//            $response = $this->client->send();
//        } catch (\Exception $e) {
//            return null;
//        }
//        return $response->getContent();
//    }
    
    /**
     * 
     * @return array
     * @throws \InvalidArgumentException
     */
    public function call() {
        if (empty($this->path)) {
            throw new \InvalidArgumentException('Path can\'t be empty');
        }
        
        $goods = [];
        foreach ($this->goods as $good) {
            $goods[] = new \ArrayObject([
                'weight' => $good['weight'],
                'volume' => $good['volume'],
            ]);
        }
        $params = [
            'goods' => $goods,
            'version' => $this->version,
            'senderCityId' => $this->from,
            'authLogin' => $this->login,
            'secure' => md5($this->getArrivalDate()->format('Y-m-d') . '&' . $this->pass),
            'receiverCityId' => $this->to,
            'dateExecute' => $this->getArrivalDate()->format('Y-m-d'),
        ];
        if (is_array($this->tariffId)) {
            $tariffIds = [];
            array_reverse($this->tariffId);
            foreach ($this->tariffId as $priority => $tariffId) {
                $tariffIds[] = [
                    'priority' => $priority,
                    'id' => $tariffId,
                ];
            }
            $params['tariffList'] = $tariffIds;
        } elseif (is_int($this->tariffId)) {
            $params['tariffId'] = $this->tariffId;
        }
        
        $ch = curl_init();
        $curlOptions = [
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => json_encode($params),
            CURLOPT_URL => $this->path,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json; charset=utf-8',
            ],
        ];
        curl_setopt_array($ch, $curlOptions);
        
        try {
            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                return null;
            }
        } catch (\Exception $e) {
            return null;
        }
        curl_close($ch);
        return $result;
    }

}