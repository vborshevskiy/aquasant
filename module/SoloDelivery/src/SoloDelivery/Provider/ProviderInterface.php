<?php

namespace SoloDelivery\Provider;

interface ProviderInterface {
    
    /**
     * @return mixed
     */
    public function call();
    
}