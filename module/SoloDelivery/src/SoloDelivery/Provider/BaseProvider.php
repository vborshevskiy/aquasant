<?php

namespace SoloDelivery\Provider;

use Zend\Http\Client;

abstract class BaseProvider implements ProviderInterface {
    
    /**
     *
     * @var Client
     */
    protected $client = null;
    
    /**
     *
     * @var string
     */
    private $path = null;
    
    /**
     *
     * @var string
     */
    protected $method;
    
    /**
     *
     * @var array
     */
    protected $goods = [];

    /**
     *
     * @var mixed
     */
    protected $from;
    
    /**
     *
     * @var mixed
     */
    protected $to;
    
    /**
     *
     * @var float
     */
    protected $price = 0;
    
    /**
     *
     * @var \DateTime
     */
    protected $arrivalDate;
    
    public function __construct() {
        $this->arrivalDate = new \DateTime('tomorrow');
    }

    /**
     * 
     * @return Client
     */
    public function getClient() {
        return $this->client;
    }
    
    /**
     * 
     * @param Client $client
     * @return \SoloDelivery\Provider\BaseProvider
     */
    public function setClient(Client $client) {
        $this->client = $client;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getPath() {
        return $this->path;
    }
    
    /**
     * 
     * @param string $path
     * @return \SoloDelivery\Provider\BaseProvider
     */
    public function setPath($path) {
        $this->path = $path;
        return $this;
    }

    /**
     * 
     * @return string
     */
    public function getMethod() {
        return $this->method;
    }
    
    /**
     * 
     * @param string $method
     * @return \SoloDelivery\Provider\BaseProvider
     */
    public function setMethod($method) {
        $this->method = $method;
        return $this;
    }
    
    public function getGoods() {
        
    }
    
    /**
     * 
     * @param array $good
     */
    public function setGood(array $good) {
        if (isset($good['volume']) && $good['volume'] > 0 && isset($good['weight']) && $good['weight'] > 0) {
            $this->goods[] = $good;
        }
    }

    /**
     * 
     * @return mixed
     */
    public function getFrom() {
        return $this->from;
    }
    
    /**
     * 
     * @param mixed $from
     * @return \SoloDelivery\Provider\BaseProvider
     */
    public function setFrom($from) {
        $this->from = $from;
        return $this;
    }
    
    /**
     * 
     * @return mixed
     */
    public function getTo() {
        return $this->to;
    }
    
    /**
     * 
     * @param mixed $to
     * @return \SoloDelivery\Provider\BaseProvider
     */
    public function setTo($to) {
        $this->to = $to;
        return $this;
    }
    
    /**
     * 
     * @return float
     */
    public function getPrice() {
        return $this->price;
    }
    
    /**
     * 
     * @param float $price
     * @throws \InvalidArgumentException
     */
    public function setPrice($price) {
        if (!is_numeric($price)) {
            throw new \InvalidArgumentException('Price must be numeric');
        }
        $this->price = $price;
    }
    
    /**
     * 
     * @return integer
     */
    public function getSumWeight() {
        $weight = 0;
        foreach ($this->goods as $good) {
            $weight += $good['weight'];
        }
        return $weight;
    }
    
    /**
     * 
     * @param \DateTime $arrivalDate
     * @return \SoloDelivery\Provider\BaseProvider
     */
    public function setArrivalDate(\DateTime $arrivalDate) {
        $this->arrivalDate = $arrivalDate;
        return $this;
    }
    
    /**
     * 
     * @return \DateTime
     */
    public function getArrivalDate() {
        return $this->arrivalDate;
    }
    
}
