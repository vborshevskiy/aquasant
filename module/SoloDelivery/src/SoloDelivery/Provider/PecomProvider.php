<?php

namespace SoloDelivery\Provider;

final class PecomProvider extends BaseProvider {
    
    /**
     *
     * @var string
     */
    private $login;
    
    /**
     *
     * @var string
     */
    private $pass;
    
    /**
     *
     * @var boolean
     */
    private $isOpenCarSender = false;
    
    /**
     *
     * @var integer
     */
    private $senderDistanceType = 0;
    
    /**
     *
     * @var integer
     */
    private $receiverDistanceType = 0;
    
    /**
     *
     * @var boolean
     */
    private $isDayByDay = false;
    
    /**
     *
     * @var integer
     */
    private $isOpenCarReceiver = 0;
    
    /**
     *
     * @var boolean
     */
    private $isInsurance = false;
    
    /**
     *
     * @var boolean
     */
    private $isPickUp = false;
    
    /**
     *
     * @var boolean
     */
    private $isDelivery = false;
    
    /**
     *
     * @var boolean
     */
    private $isHyperMarket = false;
    
    /**
     *
     * @var string
     */
    private $sertificateFile = null;

    /**
     * 
     * @param array $options
     */
    public function __construct($options) {
        if (isset($options['path'])) {
            $this->path = $options['path'];
        }
        if (isset($options['method'])) {
            $this->method = $options['method'];
        }
        if (isset($options['from'])) {
            $this->from = (int)$options['from'];
        }
        if (isset($options['login'])) {
            $this->login = $options['login'];
        }
        if (isset($options['pass'])) {
            $this->pass = $options['pass'];
        }
        if (isset($options['isOpenCarSender'])) {
            $this->isOpenCarSender = (bool)$options['isOpenCarSender'];
        }
        if (isset($options['senderDistanceType'])) {
            $this->senderDistanceType = (int)$options['senderDistanceType'];
        }
        if (isset($options['receiverDistanceType'])) {
            $this->receiverDistanceType = (int)$options['receiverDistanceType'];
        }
        if (isset($options['isDayByDay'])) {
            $this->isDayByDay = (bool)$options['isDayByDay'];
        }
        if (isset($options['isInsurance'])) {
            $this->isInsurance = (bool)$options['isInsurance'];
        }
        if (isset($options['isPickUp'])) {
            $this->isPickUp = (bool)$options['isPickUp'];
        }
        if (isset($options['isDelivery'])) {
            $this->isDelivery = (bool)$options['isDelivery'];
        }
        if (isset($options['isOpenCarReceiver'])) {
            $this->isOpenCarReceiver = (bool)$options['isOpenCarReceiver'];
        }
        if (isset($options['isHyperMarket'])) {
            $this->isHyperMarket = (bool)$options['isHyperMarket'];
        }
        if (isset($options['sertificateFile'])) {
            $this->sertificateFile = $options['sertificateFile'];
        }
    }
    
    /**
     * 
     * Через Client и Curl побороть api ПЭК в разумные сроки не удалось, используется стандартная библиотека php.
     * 
     * @return array
     * @throws \InvalidArgumentException
     */
//    public function call() {
//        if (empty($this->path)) {
//            throw new \InvalidArgumentException('Path can\'t be empty');
//        }
//        if (empty($this->method)) {
//            throw new \InvalidArgumentException('Method can\'t be empty');
//        }
//        
//        $cargos = [];
//        foreach ($this->goods as $good) {
//            $cargos[] = new \ArrayObject([
//                'volume' => $good['volume'],
//                'isHP' => false,
//                'sealingPositionsCount' => 0,
//                'weight' => $good['weight'],
//                'overSize' => false,
//            ]);
//        }
//        $params = [
//            'senderCityId' => $this->from,
//            'receiverCityId' => $this->to,
//            'isOpenCarSender' =>  false,
//            'senderDistanceType' => $this->senderDistanceType,
//            'isDayByDay' => $this->isDayByDay,
//            'isOpenCarReceiver' => $this->isOpenCarReceiver,
//            'receiverDistanceType' => $this->receiverDistanceType,
//            'isHyperMarket' => $this->isHyperMarket,
//            'isInsurance' => $this->isInsurance,
//            'isPickUp' => $this->isPickUp,
//            'isDelivery' => $this->isDelivery,
//            'Cargos' => $cargos,
//        ];
//        
//        $this->client->setAdapter('Zend\Http\Client\Adapter\Curl');
//        $this->client->getRequest()->getHeaders()->addHeaders([
//            'Content-Type: application/json;charset=utf-8',
//        ]);
//        $this->client->setOptions([
//            'curloptions' => [
//                CURLOPT_POST => true,
//                CURLOPT_RETURNTRANSFER => true,
//                CURLOPT_SSL_VERIFYPEER => true,
//                CURLOPT_SSL_VERIFYHOST => 2,
//                CURLOPT_CAINFO => $this->sertificateFile,
//                CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
//                CURLOPT_ENCODING => 'gzip',
//                CURLOPT_USERPWD => sprintf('%s:%s', $this->login, $this->pass),
//                //CURLOPT_POSTFIELDS => json_encode($params),
//            ],
//        ]);
//        $uri = new \Zend\Uri\Http($this->path . $this->method);
//        $this->client->setUri($uri);
//        //$this->client->setParameterPost([$params]);
//        
//        //$this->client->getRequest()->setContent(json_encode($params));
//        try {
//            $response = $this->client->send();
//        } catch (\Exception $e) {
//            return null;
//        }
//        return $response->getContent();
//    }
    
    
    /**
     * 
     * @return array
     * @throws \InvalidArgumentException
     */
    public function call() {
        if (empty($this->path)) {
            throw new \InvalidArgumentException('Path can\'t be empty');
        }
        if (empty($this->method)) {
            throw new \InvalidArgumentException('Method can\'t be empty');
        }
        
        $cargos = [];
        foreach ($this->goods as $good) {
            $cargos[] = new \ArrayObject([
                'volume' => $good['volume'],
                'isHP' => true,
                'sealingPositionsCount' => 0,
                'weight' => $good['weight'],
                'overSize' => false,
            ]);
        }
        $params = [
            'senderCityId' => $this->from,
            'receiverCityId' => $this->to,
            'isOpenCarSender' =>  false,
            'senderDistanceType' => $this->senderDistanceType,
            'isDayByDay' => $this->isDayByDay,
            'isOpenCarReceiver' => $this->isOpenCarReceiver,
            'receiverDistanceType' => $this->receiverDistanceType,
            'isHyperMarket' => $this->isHyperMarket,
            'isPickUp' => $this->isPickUp,
            'isDelivery' => $this->isDelivery,
            'Cargos' => $cargos,
        ];
        if ($this->isInsurance) {
            $params['isInsurance'] = $this->isInsurance;
            $params['isInsurancePrice'] = $this->getPrice();
        }
        
        $ch = curl_init();
        $curlOptions = [
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_CAINFO => $this->sertificateFile,
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
            CURLOPT_ENCODING => 'gzip',
            CURLOPT_USERPWD => sprintf('%s:%s', $this->login, $this->pass),
            CURLOPT_POSTFIELDS => json_encode($params),
            CURLOPT_URL => $this->path . $this->method,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json; charset=utf-8',
            ],
        ];
        curl_setopt_array($ch, $curlOptions);
        
        try {
            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                return null;
            }
        } catch (\Exception $e) {
            return null;
        }
        curl_close($ch);
        return $result;
    }

}