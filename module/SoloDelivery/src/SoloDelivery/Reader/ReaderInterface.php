<?php

namespace SoloDelivery\Reader;

interface ReaderInterface {
    
    /**
     * @return array
     */
    public function parse();
    
}