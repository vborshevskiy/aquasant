<?php

namespace SoloDelivery\Reader;

final class EmsReader extends BaseReader {
    
    private $companyName = 'EMS';
    
    /**
     * 
     * @return array | null
     */
    public function parse() {
        $response = json_decode($this->response);
        if (isset($response->rsp->stat) && $response->rsp->stat === 'ok') {
            $result = [];
            $result['companyName'] = $this->companyName;
            $result['companyUid'] = $this->companyUid;
            $result['isPickup'] = $this->isPickup;
            $result['maxTerm'] = (int)$response->rsp->term->max + $this->getDaysQuantityPriorArrival();
            $result['minTerm'] = (int)$response->rsp->term->min + $this->getDaysQuantityPriorArrival();
            $result['price'] = (float)$response->rsp->price;
            return $result;
        }
        return null;
    }

}