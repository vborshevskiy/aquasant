<?php

namespace SoloDelivery\Reader;

abstract class BaseReader implements ReaderInterface {
    
    /**
     *
     * @var mixed
     */
    public $response;
    
    /**
     *
     * @var mixed
     */
    protected $to;
    
    /**
     *
     * @var string
     */
    protected $companyUid;
    
    /**
     *
     * @var boolean
     */
    protected $isPickup = false;
    
    /**
     *
     * @var \DateTime
     */
    protected $arrivalDate;
    
    public function __construct() {
        $this->arrivalDate = new \DateTime('tomorrow');
    }
    
    /**
     * 
     * @return mixed
     */
    public function getResponse() {
        return $this->response;
    }
    
    /**
     * 
     * @param mixed $response
     * @return \SoloDelivery\Reader\BaseReader
     */
    public function setResponse($response) {
        $this->response = $response;
        return $this;
    }
    
    /**
     * 
     * @return mixed
     */
    public function getTo() {
        return $this->to;
    }
    
    /**
     * 
     * @param mixed $to
     * @return \SoloDelivery\Provider\BaseProvider
     */
    public function setTo($to) {
        $this->to = $to;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getCompanyUid() {
        return $this->companyUid;
    }
    
    /**
     * 
     * @param string $uid
     * @return \SoloDelivery\Provider\BaseProvider
     */
    public function setCompanyUid($uid) {
        $this->companyUid = $uid;
        return $this;
    }
    
    /**
     * 
     * @param \DateTime $arrivalDate
     * @return \SoloDelivery\Provider\BaseProvider
     */
    public function setArrivalDate(\DateTime $arrivalDate) {
        $this->arrivalDate = $arrivalDate;
        return $this;
    }
    
    /**
     * 
     * @return \DateTime
     */
    public function getArrivalDate() {
        return $this->arrivalDate;
    }
    
    /**
     * 
     * Additional days quantity to estimated delivery time
     * 
     * @return integer
     */
    public function getDaysQuantityPriorArrival() {
        return (int)$this->getArrivalDate()->diff(new \DateTime('now'))->format('%d');
    }
    
    /**
     * 
     * @param boolean $isPickup
     * @return \SoloDelivery\Entity\PostDeliveryPrice
     */
    public function isPickup($isPickup = null) {
        if (is_null($isPickup)) {
            return $this->isPickup;
        }
        $this->isPickup = (bool)$isPickup;
        return $this;
    }
    
}
