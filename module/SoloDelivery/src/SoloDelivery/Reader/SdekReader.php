<?php

namespace SoloDelivery\Reader;

final class SdekReader extends BaseReader {
    
    private $companyName = 'СДЭК';
    
    /**
     * 
     * @return array | null
     */
    public function parse() {
        $response = json_decode($this->response);
        if (isset($response->result)) {
            $result = [];
            $result['companyName'] = $this->companyName;
            $result['companyUid'] = $this->companyUid;
            $result['isPickup'] = $this->isPickup;
            $result['maxTerm'] = (int)$response->result->deliveryPeriodMax + $this->getDaysQuantityPriorArrival();
            $result['minTerm'] = (int)$response->result->deliveryPeriodMin + $this->getDaysQuantityPriorArrival();
            $result['price'] = (float)ceil($response->result->price);
            return $result;
        }
        return null;
    }

}