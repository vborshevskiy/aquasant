<?php

namespace SoloDelivery\Reader;

use Solo\Inflector\Inflector;

abstract class ReaderFactory {

    /**
     *
     * @param string $provider        	
     * @param mixed $result        	
     * @throws \InvalidArgumentException
     * @return ReaderInterface
     */
    public static function factory($provider) {
        if (empty($provider)) {
            throw new \InvalidArgumentException('Provider can\'t be empty');
        }
        $className = __NAMESPACE__ . '\\' . Inflector::camelize($provider) . 'Reader';
        return new $className();
    }

}
