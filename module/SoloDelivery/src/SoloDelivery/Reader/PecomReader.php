<?php

namespace SoloDelivery\Reader;

final class PecomReader extends BaseReader {
    
    use \SoloDelivery\Service\ProvidesPostDeliveryTerms;
    
    const AUTO_TRANSFER = 1;
    
    const AVIA_TRANSFER = 2;
    
    private $companyName = 'ПЭК';
    
    /**
     * 
     * @return array | null
     */
    public function parse() {
        $response = json_decode($this->response);
        if (isset($response->hasError) && $response->hasError === false) {
            foreach ($response->transfers as $transfer) {
                if ($transfer->transportingType === self::AUTO_TRANSFER) {
                    $termsInfo = $this->postDeliveryTerms()->getTerms($this->getTo());
                    $result = [];
                    $result['companyName'] = $this->companyName;
                    $result['companyUid'] = $this->companyUid;
                    $result['isPickup'] = $this->isPickup;
                    $result['price'] = (int)ceil($transfer->costTotal);
                    $result['maxTerm'] = (isset($termsInfo['TermMax']) ? (int)$termsInfo['TermMax'] + $this->getDaysQuantityPriorArrival() : -1);
                    $result['minTerm'] = (isset($termsInfo['TermMin']) ? (int)$termsInfo['TermMin'] + $this->getDaysQuantityPriorArrival() : -1);
                    return $result;
                }
            }
        }
        return null;
    }

}