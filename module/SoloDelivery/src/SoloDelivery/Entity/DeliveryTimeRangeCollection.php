<?php

namespace SoloDelivery\Entity;

use Solo\Collection\Collection;
use SoloDelivery\Entity\DeliveryTimeRange;

class DeliveryTimeRangeCollection extends Collection {

    /**
     *
     * @param DeliveryTimeRange $timeRange
     * @return DeliveryTimeRange
     */
    public function add($timeRange, $key = null) {
        if (!($timeRange instanceof DeliveryTimeRange)) {
            throw new \RuntimeException('Delivery time range must be an instace of DeliveryTimeRange');
        }
        $key = (is_null($key) ? $timeRange->getId() : $key);
        parent::add($timeRange, $key);
        return $timeRange;
    }

}

?>