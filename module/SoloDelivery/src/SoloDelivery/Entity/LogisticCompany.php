<?php

namespace SoloDelivery\Entity;

class LogisticCompany {

    /**
     *
     * @var integer
     */
    private $id = 0;

    /**
     *
     * @var string
     */
    private $name = '';

    /**
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @param integer $id        	
     * @throws \InvalidArgumentException
     * @return \SoloDelivery\Entity\LogisticCompany
     */
    public function setId($id) {
        if (!is_integer($id)) {
            throw new \InvalidArgumentException('Id must be integer');
        }
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     *
     * @param string $name        	
     * @return \SoloDelivery\Entity\LogisticCompany
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

}

?>