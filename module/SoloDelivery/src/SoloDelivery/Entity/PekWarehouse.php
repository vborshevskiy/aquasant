<?php

namespace SoloDelivery\Entity;

class PekWarehouse {
    
    /**
     *
     * @var integer
     */
    private $warehouseID;
    
    /**
     *
     * @var string
     */
    private $warehousePekID;
    
    /**
     *
     * @var string
     */
    private $divisionPekID;
    
    /**
     *
     * @var string
     */
    private $warehouseName;
    
    /**
     *
     * @var string
     */
    private $warehouseAddress;
    
    /**
     *
     * @var boolean
     */
    private $isAcceptanceOnly;
    
    /**
     *
     * @var boolean
     */
    private $isFreightSurcharge;
    
    /**
     *
     * @var float
     */
    private $latitude;
    
    /**
     *
     * @var float
     */
    private $longitude;
    
    /**
     *
     * @var string
     */
    private $warehouseEmail;
    
    /**
     *
     * @var string
     */
    private $warehousePhone;
    
    /**
     *
     * @var boolean
     */
    private $isRestrictions;
    
    /**
     *
     * @var float
     */
    private $maxWeight;
    
    /**
     *
     * @var float
     */
    private $maxVolume;
    
    /**
     *
     * @var float
     */
    private $maxWeightPerPlace;
    
    /**
     *
     * @var float
     */
    private $maxDimention;
    
    /**
     *
     * @var string
     */
    private $cityName;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay0From;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay0To;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay0DinnerFrom;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay0DinnerTo;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay1From;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay1To;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay1DinnerFrom;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay1DinnerTo;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay2From;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay2To;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay2DinnerFrom;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay2DinnerTo;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay3From;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay3To;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay3DinnerFrom;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay3DinnerTo;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay4From;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay4To;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay4DinnerFrom;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay4DinnerTo;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay5From;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay5To;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay5DinnerFrom;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay5DinnerTo;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay6From;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay6To;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay6DinnerFrom;
    
    /**
     *
     * @var \DateTime
     */
    private $warehouseWorkTimeDay6DinnerTo;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay0From;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay0To;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay0DinnerFrom;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay0DinnerTo;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay1From;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay1To;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay1DinnerFrom;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay1DinnerTo;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay2From;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay2To;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay2DinnerFrom;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay2DinnerTo;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay3From;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay3To;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay3DinnerFrom;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay3DinnerTo;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay4From;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay4To;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay4DinnerFrom;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay4DinnerTo;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay5From;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay5To;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay5DinnerFrom;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay5DinnerTo;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay6From;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay6To;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay6DinnerFrom;
    
    /**
     *
     * @var \DateTime
     */
    private $divisionWorkTimeDay6DinnerTo;
    
    /**
     * 
     * @param string $name
     * @return mixed
     * @throws \UnexpectedValueException
     */
    public function __get($name) {
        if (property_exists($this, $name)) {
            return $this->$name;
        }
        throw new \UnexpectedValueException('Unexpected PekWarehouse property name, "' . $name . '" given');
    }
    
    /**
     * 
     * @return string
     */
    public function getCity() {
        return $this->cityName;
    }
    
    /**
     * 
     * @return string
     */
    public function getDetails() {
        $workTime = $this->getWorkTime();
        if (is_null($workTime)) {
            return 'Время работы уточняйте по телефону';
        }
        return 'Время работы:' . '<br />' . $workTime;
    }
    
    /**
     * 
     * @return string
     */
    public function getWorkTime() {
        $workTimeArray = [];
        $weekends = [];
        for ($i = 1; $i <= 7; $i++) {
            $dayNumber = ($i === 7 ? 0 : $i);
            $russianDayName = \SoloCatalog\Service\Helper\DateHelper::getRussianWeekdayByNumber($i);
            $workTime = '';
            if ($this->{'divisionWorkTimeDay'. $dayNumber . 'From'} !== false && $this->{'divisionWorkTimeDay'. $dayNumber . 'To'} !== false) {
                $workTime .= $this->{'divisionWorkTimeDay'. $dayNumber . 'From'}->format('H:i') . ' - ' . $this->{'divisionWorkTimeDay'. $dayNumber . 'To'}->format('H:i');
                if ($this->{'divisionWorkTimeDay'. $dayNumber . 'DinnerFrom'} !== false && $this->{'divisionWorkTimeDay'. $dayNumber . 'DinnerTo'} !== false) {
                    $workTime .= ', обед ' . $this->{'divisionWorkTimeDay'. $dayNumber . 'DinnerFrom'}->format('H:i') . ' - ' . $this->{'divisionWorkTimeDay'. $dayNumber . 'DinnerTo'}->format('H:i');
                }
                $workTimeArray[$workTime][] = $russianDayName;
            } else {
                $weekends[] = $russianDayName;
            }
        }
        
        if (count($workTimeArray) > 0) {
            $workTimeTextArray = [];
            foreach ($workTimeArray as $time => $days) {
                $workTimeTextArray[] = implode(', ', $days) . ': ' . $time;
            }
            if (count($weekends) > 0) {
                $workTimeTextArray[] = implode(', ', $weekends) . ': выходной';
            }
            return implode('<br />', $workTimeTextArray);
        }
        
        return null;
    }

    /**
     * 
     * @return string
     */
    public function getType() {
        return 'pek';
    }

    /**
     * 
     * @param array $data
     * @return \SoloDelivery\Entity\PekWarehouse
     * @throws \UnexpectedValueException
     */
    public function fillFromArray(array $data) {
        foreach ($data as $property => $value) {
            $propertyComment = (new \ReflectionClass($this))->getProperty($property)->getDocComment();
            $dataType = preg_match('/@var\s+([^\s]+)/', $propertyComment, $matches) ? $matches[1] : null;
            switch ($dataType) {
                case 'integer':
                    $this->$property = (int)$value;
                    break;
                case 'string':
                    $this->$property = (string)$value;
                    break;
                case 'float':
                    $this->$property = (float)$value;
                    break;
                case 'boolean':
                    $this->$property = (bool)$value;
                    break;
                case '\DateTime':
                    $this->$property = \DateTime::createFromFormat('H:i:s', $value);
                    break;
                default:
                    throw new \UnexpectedValueException('Unexpected data type, "' . $dataType . '" given');
            }
        }
        return $this;
    }
    
}