<?php

namespace SoloDelivery\Entity;

use SoloDelivery\Entity\TransportCompanyInterface\EmsInterface;
use SoloDelivery\Entity\TransportCompanyInterface\SdekInterface;
use SoloDelivery\Entity\TransportCompanyInterface\PekInterface;

class PostDeliveryRegion implements EmsInterface, SdekInterface, PekInterface {

	const REGION_MOSCOW_ID = 42;
	
	const REGION_SPB_ID = 59;
	
    /**
     *
     * @var integer
     */
    protected $id;
    
    /**
     *
     * @var integer
     */
    protected $parentId;
    
    /**
     *
     * @var integer
     */
    protected $erpCityId;
    
    /**
     *
     * @var boolean
     */
    protected $isTwinDelivery = false;
    
    /**
     *
     * @var string
     */
    protected $name;
    
    /**
     *
     * @var string|null
     */
    protected $shortName;

    /**
     *
     * @var integer
     */
    protected $sdekId;
    
    /**
     *
     * @var type 
     */
    protected $pekId;
    
    /**
     *
     * @var string
     */
    protected $emsId;
    
    /**
     *
     * @var boolean
     */
    protected $isEndGroup = false;
    
    /**
     *
     * @var string
     */
    protected $phone;
    
    /**
     *
     * @var float
     */
    protected $latitude;
    
    /**
     *
     * @var float
     */
    protected $longitude;
    
    /**
     *
     * @var integer
     */
    protected $shipped = 1;
    
    /**
     *
     * @var integer
     */
    protected $deliveryDays = 10;
    
    /**
     *
     * @var integer
     */
    protected $zoom = 10;

    /**
     * 
     * @param integer $id
     * @return \SoloDelivery\Entity\PostDeliveryCity
     * @throws \InvalidArgumentException
     */
    public function setId($id) {
        if (!is_int($id)) {
            throw new \InvalidArgumentException('City id must be integer');
        }
        $this->id = $id;
        return $this;
    }

    /**
     * 
     * @return integer
     */
    public function getId() {
        return $this->id;
    }
    
    /**
     * 
     * @param integer $parentId
     * @return \SoloDelivery\Entity\PostDeliveryCity
     */
    public function setParentId($parentId) {
        $this->parentId = $parentId;
        return $this;
    }

    /**
     * 
     * @return integer
     */
    public function getParentId() {
        return $this->parentId;
    }
    
    /**
     * 
     * @param integer $erpCityId
     * @return \SoloDelivery\Entity\PostDeliveryCity
     */
    public function setErpCityId($erpCityId) {
        $this->erpCityId = $erpCityId;
        return $this;
    }

    /**
     * 
     * @return integer
     */
    public function getErpCityId() {
        return $this->erpCityId;
    }
    
    /**
     * 
     * @param string $name
     * @return \SoloDelivery\Entity\PostDeliveryCity
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * 
     * @return string
     */
    public function getName() {
        return $this->name;
    }
    
    /**
     * 
     * @return string
     */
    public function getShortName() {
        if (is_null($this->shortName)) {
            $name = $this->getName();
            if (strpos($name, ',') !== false) {
                $name = substr($name, 0, strpos($name, ','));
            }
            if (strpos($name, '(') !== false) {
                $name = substr($name, 0, strpos($name, '('));
            }
            $this->shortName = trim($name);
        }
        return $this->shortName;
    }

    /**
     * 
     * @param integer $sdekId
     * @return \SoloDelivery\Entity\PostDeliveryCity
     * @throws \InvalidArgumentException
     */
    public function setSdekId($sdekId) {
        if (!is_int($sdekId)) {
            throw new \InvalidArgumentException('SDEK id must be integer');
        }
        $this->sdekId = $sdekId;
        return $this;
    }

    /**
     * 
     * @return integer
     */
    public function getSdekId() {
        return $this->sdekId;
    }
    
    /**
     * 
     * @param integer $pekId
     * @return \SoloDelivery\Entity\PostDeliveryCity
     * @throws \InvalidArgumentException
     */
    public function setPekId($pekId) {
        if (!is_int($pekId)) {
            throw new \InvalidArgumentException('PEK id must be integer');
        }
        $this->pekId = $pekId;
        return $this;
    }

    /**
     * 
     * @return integer
     */
    public function getPekId() {
        return $this->pekId;
    }
    
    /**
     * 
     * @param string $emsId
     * @return \SoloDelivery\Entity\PostDeliveryCity
     */
    public function setEmsId($emsId) {
        $this->emsId = $emsId;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getEmsId() {
        return $this->emsId;
    }
    
    /**
     * 
     * @param null | boolean $isEndGroup
     * @return \SoloDelivery\Entity\PostDeliveryRegion | boolean
     */
    public function isEndGroup($isEndGroup = null) {
        if (is_null($isEndGroup)) {
            return $this->isEndGroup;
        }
        $this->isEndGroup = (bool)$isEndGroup;
        return $this;
    }
    
    /**
     * 
     * @param null | boolean $isTwinDelivery
     * @return \SoloDelivery\Entity\PostDeliveryRegion | boolean
     */
    public function isTwinDelivery($isTwinDelivery = null) {
        if (is_null($isTwinDelivery)) {
            return $this->isTwinDelivery;
        }
        $this->isTwinDelivery = (bool)$isTwinDelivery;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }
    
    /**
     * 
     * @param string $phone
     * @return \SoloDelivery\Entity\PostDeliveryRegion
     */
    public function setPhone($phone) {
        $this->phone = $phone;
        return $this;
    }
    
    /**
     * 
     * @return float
     */
    public function getLatitude() {
        return $this->latitude;
    }
    
    /**
     * 
     * @param float $latitude
     * @return \SoloDelivery\Entity\PostDeliveryRegion
     */
    public function setLatitude($latitude) {
        $this->latitude = $latitude;
        return $this;
    }
    
    /**
     * 
     * @return float
     */
    public function getLongitude() {
        return $this->longitude;
    }
    
    /**
     * 
     * @param float $longitude
     * @return \SoloDelivery\Entity\PostDeliveryRegion
     */
    public function setLongitude($longitude) {
        $this->longitude = $longitude;
        return $this;
    }
    
    /**
     * 
     * @return integer
     */
    public function getShipped() {
        return $this->shipped;
    }
    
    /**
     * 
     * @param integer $shipped
     * @return \SoloDelivery\Entity\PostDeliveryRegion
     */
    public function setShipped($shipped) {
        $this->shipped = $shipped;
        return $this;
    }
    
    /**
     * 
     * @return integer
     */
    public function getDeliveryDays() {
        return $this->deliveryDays;
    }
    
    /**
     * 
     * @param integer $deliveryDays
     * @return \SoloDelivery\Entity\PostDeliveryRegion
     */
    public function setDeliveryDays($deliveryDays) {
        $this->deliveryDays = $deliveryDays;
        return $this;
    }
    
    /**
     * 
     * @return integer
     */
    public function getZoom() {
        return $this->zoom;
    }
    
    /**
     * 
     * @param integer $zoom
     */
    public function setZoom($zoom) {
        $this->zoom = $zoom;
    }

}
