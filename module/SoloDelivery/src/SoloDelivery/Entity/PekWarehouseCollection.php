<?php

namespace SoloDelivery\Entity;

use Solo\Collection\Collection;

class PekWarehouseCollection extends Collection {

    /**
     * 
     * @param \SoloDelivery\Entity\PekWarehouse $warehouse
     * @param mixed $key
     * @return \SoloDelivery\Entity\PekWarehouse
     * @throws \RuntimeException
     */
    public function add($warehouse, $key = null) {
        if (!($warehouse instanceof PekWarehouse)) {
            throw new \RuntimeException('Pek warehouse must be an instace of PekWarehouse');
        }
        $key = (is_null($key) ? $warehouse->warehouseID : $key);
        parent::add($warehouse, $key);
        return $warehouse;
    }

}
