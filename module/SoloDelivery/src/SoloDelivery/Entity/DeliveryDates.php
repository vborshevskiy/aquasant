<?php

namespace SoloDelivery\Entity;
use SoloCatalog\Service\Helper\DateHelper;
use SoloDelivery\Entity\DeliveryTimeRangeCollection;
use SoloDelivery\Service\Helper\DeliveryHelper;
use Solo\DateTime\DateTime;

class DeliveryDates {

    /**
     *
     * @var DeliveryHelper
     */
    protected $deliveryHelper;

    /**
     *
     * @var array
     */
    protected $goods = [];

    /**
     *
     * @var type integer
     */
    protected $daysCount = 5;

    /**
     *
     * @var array
     */
    protected $unavailDates = [];

    /**
     *
     * @var array
     */
    protected $availDays = [];

    /**
     *
     * @var array
     */
    protected $timeRanges = [];

    /**
     *
     * @param integer $daysCount
     */
    public function __construct($daysCount, DeliveryHelper $deliveryHelper) {
        $this->daysCount = $daysCount;
        $this->deliveryHelper = $deliveryHelper;
    }

    /**
     *
     * @param \SoloDelivery\Entity\DeliveryGood $good
     */
    public function addGood(DeliveryGood $good) {
        $this->goods[$good->getId()] = $good;
    }

    /**
     *
     * @param integer $goodId
     * @return boolean
     */
    public function hasGood($goodId) {
        return (array_key_exists($goodId, $this->goods));
    }

    /**
     *
     * @param integer $goodId
     */
    public function deleteGood($goodId) {
        if ($this->hasGood($goodId)) {
            unset($this->goods[$goodId]);
        }
    }

    /**
     *
     * @param DeliveryTimeRangeCollection $timeRanges
     * @return \SoloDelivery\Entity\DeliveryDates
     */
    public function setTimeRanges(DeliveryTimeRangeCollection $timeRanges) {
        $this->timeRanges = $timeRanges;
        return $this;
    }

    /**
     *
     * @return DeliveryTimeRangeCollection
     */
    public function getTimeRanges() {
        return $this->timeRanges;
    }

    /**
     *
     * @param array $unavailDates
     * @return \SoloDelivery\Entity\DeliveryDates
     */
    public function setUnavailDates(array $unavailDates) {
        $this->unavailDates = $unavailDates;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getUnavailDates() {
        return $this->unavailDates;
    }

    /**
     *
     * @param array $availDays
     * @return \SoloDelivery\Entity\DeliveryDates
     */
    public function setAvailDays(array $availDays) {
        $this->availDays = $availDays;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getAvailDays() {
        return $this->availDays;
    }

    /**
     *
     * @return \DateTime
     */
    public function getMinDeliveryDate() {
        $dates = [];
        foreach ($this->goods as $good) {
            $date = $good->getMinDeliveryDate();
            if (is_null($date)) {
                return null;
            }
            $dates[] = $date;
        }
        return DateHelper::getMaxDate($dates);
    }

    /**
     *
     * @return null|array
     */
    public function getPossibleDeliveryDates($daysCount = null, $minDeliveryDate = 0) {
        $days = (null !== $daysCount) ? $daysCount : $this->daysCount;
        $possibleDeliveryDates = [];
        $deliveryDate = $this->getMinDeliveryDate();

        if (is_null($deliveryDate)) {
            return null;
        }
        if ((0 < $minDeliveryDate) && ($minDeliveryDate > $deliveryDate->getTimestamp())) {
        	$deliveryDate = DateTime::createFromTimestamp($minDeliveryDate);
        }

        if (!empty($this->getAvailDays())) {
            for ($i = 0; $i < $days;) {
                $weekDayNumber = $deliveryDate->format('N');

                if (in_array($weekDayNumber, $this->getAvailDays()) && !DateHelper::isDateInArray($deliveryDate, $this->getUnavailDates())) {
                    $possibleDeliveryDates[] = clone $deliveryDate;

                    $i++;
                }
                $deliveryDate->add(new \DateInterval('P1D'));
            }
        }

        return $possibleDeliveryDates;
    }
}

?>