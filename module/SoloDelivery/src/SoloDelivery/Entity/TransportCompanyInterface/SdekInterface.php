<?php

namespace SoloDelivery\Entity\TransportCompanyInterface;

interface SdekInterface {

    /**
     * 
     * @return integer
     */
    public function getSdekId();
    
}
