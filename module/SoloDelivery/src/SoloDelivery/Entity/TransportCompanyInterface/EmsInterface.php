<?php

namespace SoloDelivery\Entity\TransportCompanyInterface;

interface EmsInterface {

    /**
     * 
     * @return string
     */
    public function getEmsId();
    
}
