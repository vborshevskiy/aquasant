<?php

namespace SoloDelivery\Entity;

class PostDeliveryPrice {

    /**
     *
     * @var string
     */
    private $companyUid = '';

    /**
     *
     * @var string
     */
    private $companyName = '';

    /**
     *
     * @var float
     */
    private $price = 0;
    
    /**
     *
     * @var float
     */
    private $twinLogisticPrice = 0;

    /**
     *
     * @var integer
     */
    private $deliveryMinTerm = 0;
    
    /**
     *
     * @var integer
     */
    private $deliveryMaxTerm = 0;
    
    /**
     *
     * @var boolean
     */
    private $isPickup = false;
    
    /**
     * 
     * @return string
     */
    public function getCompanyUid() {
        return $this->companyUid;
    }
    
    /**
     * 
     * @param string $uid
     * @return \SoloDelivery\Entity\PostDeliveryPrice
     */
    public function setCompanyUid($uid) {
        $this->companyUid = $uid;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getCompanyName() {
        return $this->companyName;
    }
    
    /**
     * 
     * @param string $name
     * @return \SoloDelivery\Entity\PostDeliveryPrice
     */
    public function setCompanyName($name) {
        $this->companyName = $name;
        return $this;
    }
    
    /**
     * 
     * @return float
     */
    public function getPrice() {
        return $this->price;
    }
    
    /**
     * 
     * @param float $price
     * @return \SoloDelivery\Entity\PostDeliveryPrice
     * @throws \InvalidArgumentException
     */
    public function setPrice($price) {
        if (!is_numeric($price)) {
            throw new \InvalidArgumentException('Price must be numeric');
        }
        $this->price = (float)$price;
        return $this;
    }
    
    /**
     * 
     * @return integer
     */
    public function getDeliveryMaxTerm() {
        return $this->deliveryMaxTerm;
    }
    
    /**
     * 
     * @param integer $max
     * @return \SoloDelivery\Entity\PostDeliveryPrice
     * @throws \InvalidArgumentException
     */
    public function setDeliveryMaxTerm($max) {
        if (!is_int($max)) {
            throw new \InvalidArgumentException('Days quantity must be integer');
        }
        $this->deliveryMaxTerm = $max;
        return $this;
    }
    
    /**
     * 
     * @return integer
     */
    public function getDeliveryMinTerm() {
        return $this->deliveryMinTerm;
    }
    
    /**
     * 
     * @param integer $min
     * @return \SoloDelivery\Entity\PostDeliveryPrice
     * @throws \InvalidArgumentException
     */
    public function setDeliveryMinTerm($min) {
        if (!is_int($min)) {
            throw new \InvalidArgumentException('Days quantity must be integer');
        }
        $this->deliveryMinTerm = $min;
        return $this;
    }
    
    /**
     * 
     * @param float $price
     * @return \SoloDelivery\Entity\PostDeliveryPrice
     * @throws \InvalidArgumentException
     */
    public function setTwinLogisticPrice($price) {
        if (!is_numeric($price)) {
            throw new \InvalidArgumentException('Price must be numeric');
        }
        $this->twinLogisticPrice = (float)$price;
        return $this;
    }
    
    /**
     * 
     * @return float
     */
    public function getTwinLogisticPrice() {
        return $this->twinLogisticPrice;
    }
    
    /**
     * 
     * @param boolean $isPickup
     * @return \SoloDelivery\Entity\PostDeliveryPrice
     */
    public function isPickup($isPickup = null) {
        if (is_null($isPickup)) {
            return $this->isPickup;
        }
        $this->isPickup = (bool)$isPickup;
        return $this;
    }
    
}
