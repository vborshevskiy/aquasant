<?php

namespace SoloDelivery\Entity;

class DeliveryTimeRange {

    /**
     *
     * @var integer
     */
    private $id = 0;

    /**
     *
     * @var string
     */
    private $name = '';
    
    /**
     *
     * @var DateTime
     */
    private $timeTo = null;
    
    /**
     *
     * @var DateTime
     */
    private $timeFrom = null;
    
    /**
     *
     * @var DateTime
     */
    private $dateTimeTo = null;
    
    /**
     *
     * @var DateTime
     */
    private $dateTimeFrom = null;

    /**
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @param integer $id        	
     * @throws \InvalidArgumentException
     * @return \SoloDelivery\Entity\DeliveryTimeRange
     */
    public function setId($id) {
        if (!is_integer($id)) {
            throw new \InvalidArgumentException('Id must be integer');
        }
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     *
     * @param string $name        	
     * @return \SoloDelivery\Entity\DeliveryTimeRange
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    
    /**
     *
     * @return DateTime
     */
    public function getTimeTo() {
        return $this->timeTo;
    }

    /**
     *
     * @param DateTime $time        	
     * @throws \InvalidArgumentException
     * @return \SoloDelivery\Entity\DeliveryTimeRange
     */
    public function setTimeTo($time) {
        if (!$time instanceof \DateTime) {
            throw new \InvalidArgumentException('Time to must be instance of DateTime');
        }
        $this->timeTo = $time;
        return $this;
    }
    
    /**
     *
     * @return DateTime
     */
    public function getTimeFrom() {
        return $this->timeFrom;
    }

    /**
     *
     * @param DateTime $time        	
     * @throws \InvalidArgumentException
     * @return \SoloDelivery\Entity\DeliveryTimeRange
     */
    public function setTimeFrom($time) {
        if (!$time instanceof \DateTime) {
            throw new \InvalidArgumentException('Time from must be instance of DateTime');
        }
        $this->timeFrom = $time;
        return $this;
    }
    
    /**
     *
     * @return DateTime
     */
    public function getDateTimeFrom() {
        return $this->dateTimeFrom;
    }

    /**
     *
     * @param DateTime $date        	
     * @throws \InvalidArgumentException
     * @return \SoloDelivery\Entity\DeliveryTimeRange
     */
    public function setDateTimeFrom($date) {
        if (!$date instanceof \DateTime) {
            throw new \InvalidArgumentException('DateTime from must be instance of DateTime');
        }
        $this->dateTimeFrom = $date;
        return $this;
    }
    
    /**
     *
     * @return DateTime
     */
    public function getDateTimeTo() {
        return $this->dateTimeTo;
    }

    /**
     *
     * @param DateTime $date        	
     * @throws \InvalidArgumentException
     * @return \SoloDelivery\Entity\DeliveryTimeRange
     */
    public function setDateTimeTo($date) {
        if (!$date instanceof \DateTime) {
            throw new \InvalidArgumentException('DateTime to must be instance of DateTime');
        }
        $this->dateTimeTo = $date;
        return $this;
    }

}

?>