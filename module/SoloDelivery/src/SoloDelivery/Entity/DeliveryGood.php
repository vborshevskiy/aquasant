<?php

namespace SoloDelivery\Entity;

class DeliveryGood {

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $quantity;

    /**
     *
     * @var integer
     */
    protected $availQuantity;

    /**
     *
     * @var integer
     */
    protected $suborderQuantity;

    /**
     *
     * @var \DateTime
     */
    //protected $availDeliveryDate;

    /**
     *
     * @var \DateTime
     */
    //protected $suborderDeliveryDate;

	/**
     *
     * @var \DateTime
     */
    protected $deliveryDate;

	/**
     *
     * @var \DateTime
     */
    protected $showcaseRetrieveDate;

    /**
     *
     * @param integer $id
     * @return \SoloDelivery\Entity\DeliveryGood
     * @throws \InvalidArgumentException
     */
    public function setId($id) {
        if (!is_int($id)) {
            throw new \InvalidArgumentException('Good id must be integer');
        }
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @param integer $quantity
     * @return \SoloDelivery\Entity\DeliveryGood
     * @throws \InvalidArgumentException
     */
    public function setQuantity($quantity) {
        if (!is_int($quantity)) {
            throw new \InvalidArgumentException('Quantity must be integer');
        }
        $this->quantity = $quantity;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getQuantity() {
        return $this->quantity;
    }

    /**
     *
     * @param integer $availQuantity
     * @return \SoloDelivery\Entity\DeliveryGood
     * @throws \InvalidArgumentException
     */
    public function setAvailQuantity($availQuantity) {
        if (!is_int($availQuantity)) {
            throw new \InvalidArgumentException('Avail quantity id must be integer');
        }
        $this->availQuantity = $availQuantity;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getAvailQuantity() {
        return $this->availQuantity;
    }

    /**
     *
     * @param integer $suborderQuantity
     * @return \SoloDelivery\Entity\DeliveryGood
     * @throws \InvalidArgumentException
     */
    public function setSuborderQuantity($suborderQuantity) {
        if (!is_int($suborderQuantity)) {
            throw new \InvalidArgumentException('Suborder quantity id must be integer');
        }
        $this->suborderQuantity = $suborderQuantity;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getSuborderQuantity() {
        return $this->suborderQuantity;
    }

    /**
     *
     * @param \DateTime $availDeliveryDate
     * @return \SoloDelivery\Entity\DeliveryGood
     * @throws \InvalidArgumentException
     */
    public function setAvailDeliveryDate($availDeliveryDate) {
        if (!($availDeliveryDate instanceof \DateTime)) {
            throw new \InvalidArgumentException('Avail quantity id must be instance of \DateTime');
        }
        $this->availDeliveryDate = $availDeliveryDate;
        return $this;
    }

    /**
     *
     * @return \DateTime
     */
    public function getAvailDeliveryDate() {
        return $this->availDeliveryDate;
    }

    /**
     *
     * @param \DateTime $suborderDeliveryDate
     * @return \SoloDelivery\Entity\DeliveryGood
     * @throws \InvalidArgumentException
     */
    public function setSuborderDeliveryDate($suborderDeliveryDate) {
        if (!($suborderDeliveryDate instanceof \DateTime)) {
            throw new \InvalidArgumentException('Suborder quantity id must be instance of \DateTime');
        }
        $this->suborderDeliveryDate = $suborderDeliveryDate;
        return $this;
    }

    /**
     *
     * @return \DateTime
     */
    public function getSuborderDeliveryDate() {
        return $this->suborderDeliveryDate;
    }

	/**
     *
     * @param \DateTime $deliveryDate
     * @return \SoloDelivery\Entity\DeliveryGood
     * @throws \InvalidArgumentException
     */
    public function setDeliveryDate($deliveryDate) {
        if (!($deliveryDate instanceof \DateTime)) {
            throw new \InvalidArgumentException('DeliveryDate must be instance of \DateTime');
        }
        $this->deliveryDate = $deliveryDate;
        return $this;
    }

    /**
     *
     * @return \DateTime
     */
    public function getDeliveryDate() {
        return $this->deliveryDate;
    }

	/**
     *
     * @param \DateTime $showcaseRetrieveDate
     * @return \SoloDelivery\Entity\DeliveryGood
     * @throws \InvalidArgumentException
     */
    public function setShowcaseRetrieveDate($showcaseRetrieveDate) {
        if (!($showcaseRetrieveDate instanceof \DateTime)) {
            throw new \InvalidArgumentException('ShowcaseRetrieveDate must be instance of \DateTime');
        }
        $this->showcaseRetrieveDate = $showcaseRetrieveDate;
        return $this;
    }

    /**
     *
     * @return \DateTime
     */
    public function getShowcaseRetrieveDate() {
        return $this->showcaseRetrieveDate;
    }

	/**
     *
     * @return \DateTime
     */
    public function getShowcaseDeliveryDate() {
		$result = null;

		if ($this->showcaseRetrieveDate) {
			$result = clone $this->showcaseRetrieveDate;
			$result->add(new \DateInterval('P1D'));
		}

		return $result;
    }

    /**
     *
     * @return boolean
     */
    public function hasSuborder() {
        return (!is_null($this->getSuborderQuantity()) && !is_null($this->getDeliveryDate()));
    }

    /**
     *
     * @return boolean
     */
    public function hasAvail() {
        return (!is_null($this->getAvailQuantity()) && (!is_null($this->getDeliveryDate()) || !is_null($this->showcaseRetrieveDate)));
    }

	/**
     *
     * @return boolean
     */
    public function hasShowcase() {
        return (!is_null($this->getAvailQuantity()) && !is_null($this->getShowcaseRetrieveDate()));
    }

    public function getSummaryAvailAndSuborderQuantity() {
        $quantity = 0;
        if ($this->hasAvail()) {
            $quantity += $this->getAvailQuantity();
        }
        if ($this->hasSuborder()) {
            $quantity += $this->getSuborderQuantity();
        }
        return $quantity;
    }

    /**
     *
     * @return \DateTime|null
     */
    public function getMinDate() {
        if ($this->hasAvail() && $this->hasSuborder() && $this->hasShowcase()) {
            return \SoloCatalog\Service\Helper\DateHelper::getMinDate([
                $this->getDeliveryDate(),
                $this->getShowcaseRetrieveDate(),
            ]);
        } elseif ($this->hasShowcase()) {
            return $this->getShowcaseRetrieveDate();
        } elseif ($this->hasSuborder()) {
            return $this->getDeliveryDate();
        } elseif ($this->hasAvail()) {
            return $this->getDeliveryDate();
        }
        return null;
    }

	/**
     *
     * @return \DateTime
     */
    public function getMinDeliveryDate() {
		$result = null;

        if ($this->getQuantity() <= $this->getSummaryAvailAndSuborderQuantity()) {
			if ($this->hasAvail() && $this->hasSuborder() && $this->hasShowcase()) {
				$result = \SoloCatalog\Service\Helper\DateHelper::getMinDate([
					$this->getDeliveryDate(),
					$this->getShowcaseDeliveryDate(),
				]);
			} elseif ($this->hasShowcase()) {
				$result = $this->getShowcaseDeliveryDate();
			} elseif ($this->hasSuborder()) {
				$result = $this->getDeliveryDate();
			} elseif ($this->hasAvail()) {
				$result = $this->getDeliveryDate();
			}
		}

        return $result;
    }

	/**
     * OLD Version
     * @return \DateTime
    public function getMinDeliveryDate() {
        if ($this->getQuantity() > $this->getSummaryAvailAndSuborderQuantity()) {
            return null;
        }
        if ($this->hasAvail() && $this->hasSuborder()) {
            if ($this->getMinDate() == $this->getAvailDeliveryDate()) {
                if ($this->getQuantity() > $this->getAvailQuantity()) {
                    return $this->getSuborderDeliveryDate();
                }
                return $this->getAvailDeliveryDate();
            } elseif ($this->getMinDate() == $this->getSuborderDeliveryDate()) {
                if ($this->getQuantity() > $this->getSuborderQuantity()) {
                    return $this->getAvailDeliveryDate();
                }
                return $this->getSuborderDeliveryDate();
            }
            return null;
        } elseif ($this->hasAvail()) {
            if ($this->getQuantity() > $this->getAvailQuantity()) {
                return null;
            }
            return $this->getAvailDeliveryDate();
        } elseif ($this->hasSuborder()) {
            if ($this->getQuantity() > $this->getSuborderQuantity()) {
                return null;
            }
            return $this->getSuborderDeliveryDate();
        }
    }*/

}

?>