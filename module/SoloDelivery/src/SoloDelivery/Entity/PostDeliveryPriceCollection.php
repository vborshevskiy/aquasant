<?php

namespace SoloDelivery\Entity;

use Solo\Collection\Collection;

class PostDeliveryPriceCollection extends Collection {

    const MAX_TERM = 14;
    
    const MIN_TERM = 1;
    
    /**
     * 
     * @param \SoloDelivery\Entity\PostDeliveryPrice $company
     * @param mixed $key
     * @return \SoloDelivery\Entity\PostDeliveryPrice
     * @throws \RuntimeException
     */
    public function add($company, $key = null) {
        if (!($company instanceof PostDeliveryPrice)) {
            throw new \RuntimeException('Post delivery price must be an instace of PostDeliveryPrice');
        }
        $key = (is_null($key) ? $company->getCompanyUid() : $key);
        parent::add($company, $key);
        return $company;
    }
    
    /**
     * 
     * @return void
     */
    public function recalcPekTerms(array $companyUids = ['pecom', 'pecom-pickup']) {
        foreach ($companyUids as $companyUid) {
            if ($this->offsetExists($companyUid)) {
                $pecom = $this->offsetGet($companyUid);
                if ($pecom->getDeliveryMaxTerm() < 0 || $pecom->getDeliveryMinTerm() < 0) {
                    /** var \SoloDelivery\Entity\PostDeliveryPrice $postDeliveryPrice */
                    foreach ($this as $postDeliveryPrice) {
                        if ($postDeliveryPrice->getCompanyUid() !== 'pecom' && $postDeliveryPrice->getCompanyUid() !== 'pecom-pickup') {
                            if ($pecom->getDeliveryMaxTerm() === -1 || $postDeliveryPrice->getDeliveryMaxTerm() > $pecom->getDeliveryMaxTerm()) {
                                $pecom->setDeliveryMaxTerm($postDeliveryPrice->getDeliveryMaxTerm());
                            }
                            if ($pecom->getDeliveryMinTerm() === -1 || $postDeliveryPrice->getDeliveryMinTerm() < $pecom->getDeliveryMinTerm()) {
                                $pecom->setDeliveryMinTerm($postDeliveryPrice->getDeliveryMinTerm());
                            }
                        }
                    }
                    if ($pecom->getDeliveryMaxTerm() < 0) {
                        $pecom->setDeliveryMaxTerm(self::MAX_TERM);
                    }
                    if ($pecom->getDeliveryMinTerm() < 0) {
                        $pecom->setDeliveryMinTerm(self::MIN_TERM);
                    }
                }
            }
        }
    }

}
