<?php

namespace SoloDelivery\Entity;

class PostDeliveryRegionCollection extends \Solo\Collection\Collection {

    /**
     * 
     * @param \SoloDelivery\Entity\PostDeliveryRegion $postDeliveryRegion
     * @param mixed $key
     * @return \SoloDelivery\Entity\PostDeliveryRegion
     * @throws \InvalidArgumentException
     */
    public function add($postDeliveryRegion, $key = null) {
        if (!($postDeliveryRegion instanceof PostDeliveryRegion)) {
            throw new \InvalidArgumentException('Post delivery region must be an instace of PostDeliveryRegion');
        }
        $key = (is_null($key) ? $postDeliveryRegion->getId() : $key);
        parent::add($postDeliveryRegion, $key);
        return $postDeliveryRegion;
    }

}
