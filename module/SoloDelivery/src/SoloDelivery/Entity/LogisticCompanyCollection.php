<?php

namespace SoloDelivery\Entity;

use Solo\Collection\Collection;
use SoloDelivery\Entity\LogisticCompany;

class LogisticCompanyCollection extends Collection {

    /**
     *
     * @param LogisticCompany $company
     * @return LogisticCompany
     */
    public function add($company, $key = null) {
        if (!($company instanceof LogisticCompany)) {
            throw new \RuntimeException('Logistic company must be an instace of LogisticCompany');
        }
        $key = (is_null($key) ? $company->getId() : $key);
        parent::add($company, $key);
        return $company;
    }

}

?>