<?php

namespace SoloDelivery\Service;

use SoloDelivery\Entity\LogisticCompanyCollection;
use SoloDelivery\Entity\LogisticCompany;
use SoloDelivery\Data\LogisticCompaniesMapperInterface;

class LogisticCompanyService {

    /**
     *
     * @var LogisticCompaniesMapperInterface
     */
    protected $logisticCompanyMapper;

    /**
     *
     * @param LogisticCompaniesMapperInterface $logisticCompanyMapper     	
     */
    public function __construct(LogisticCompaniesMapperInterface $logisticCompanyMapper) {
        $this->logisticCompanyMapper = $logisticCompanyMapper;
    }
    
    /**
     * 
     * @param array $logisticCompanyId
     * @return LogisticCompanyCollection
     */
    public function getLogisticCompanyById($logisticCompanyId) {
        if (!is_int($logisticCompanyId)) {
            throw new \InvalidArgumentException('Logistic company id must be integer');
        }
        $companyData = $this->logisticCompanyMapper->getLogisticCompanyById($logisticCompanyId);
        return $this->createLogisticCompany($companyData);
    }
    
    /**
     * 
     * @param integer $cityId
     * @param integer $shippingsCount
     * @return array
     */
    public function getSchedule($cityId = 2, $shippingsCount = 3) {
        $schedule = [];
        $scheduleRecords = $this->logisticCompanyMapper->getScheduleRecords($cityId);
        foreach ($scheduleRecords as $groupedScheduleRecords) {
            $logisticCompanyId = (int)$groupedScheduleRecords['LogisticCompanyID'];
            $shippingDates = [];
            $shippingDate = new \DateTime();
            $shippingDate->setTime($groupedScheduleRecords['ShippingHour'], $groupedScheduleRecords['ShippingMinute'], 0);
            $daysDiff = $groupedScheduleRecords['WeekDayID'] - $shippingDate->format('N');
            $daysDiff = ($daysDiff >= 0 ? $daysDiff : $daysDiff + 7);
            $shippingDate->modify('+' . $daysDiff . ' days');
            $arrivalDate = clone $shippingDate ;
            $arrivalDate->modify('+' . $groupedScheduleRecords['DaysInterval'] . ' days');
            $arrivalDate->setTime($groupedScheduleRecords['ArrivalHour'], $groupedScheduleRecords['ArrivalMinute'], 0);
            $shippingDates[] = [
                'shipping' => clone $shippingDate,
                'arrival' => clone $arrivalDate ,
            ];
            while (count($shippingDates) < $shippingsCount) {
                $shippingDates[] = [
                    'shipping' => clone $shippingDate->modify('+7 days'),
                    'arrival' => clone $arrivalDate->modify('+7 days'),
                ];
            }
            if (array_key_exists($logisticCompanyId, $schedule)) {
                $schedule[$logisticCompanyId] = array_merge($schedule[$logisticCompanyId], $shippingDates);
            } else {
                $schedule[$logisticCompanyId] = $shippingDates;
            }
        }
        return $schedule;
    }

    /**
     * 
     * @param ResultSet $companyData
     * @return LogisticCompany
     */
    private function createLogisticCompany($companyData) {
        $company = new LogisticCompany();
        $company->setId((int) $companyData['CompanyID']);
        $company->setName($companyData['CompanyName']);
        return $company;
    }

}
