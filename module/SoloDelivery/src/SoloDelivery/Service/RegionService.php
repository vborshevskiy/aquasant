<?php

namespace SoloDelivery\Service;

use SoloDelivery\Data\PostDeliveryMapperInterface;
use SoloDelivery\Entity\PostDeliveryRegion;
use SoloDelivery\Entity\PostDeliveryRegionCollection;

class RegionService {
    
    const MOSCOW_REGION = 42;
    
    const MO_REGION = 43;

    /**
     *
     * @var PostDeliveryMapperInterface
     */
    protected $postDeliveryMapper;

    /**
     * 
     * @param PostDeliveryMapperInterface $postDeliveryMapperInterface
     */
    public function __construct(PostDeliveryMapperInterface $postDeliveryMapperInterface) {
        $this->postDeliveryMapper = $postDeliveryMapperInterface;
    }
    
    /**
     * 
     * @param string $regionName
     * @return integer | null
     */
    public function getRegionIdByName($regionName) {
        $region = $this->postDeliveryMapper->getRegionIdByName($regionName);
        if (!is_null($region)) {
            return (int)$region->RegionID;
        }
        return null;
    }
    
    /**
     * 
     * @param string $query
     * @return integer | null
     */
    public function searchRegion($query) {
        $region = $this->postDeliveryMapper->searchRegion($query);
        if (!is_null($region)) {
            return (int)$region->RegionID;
        }
        return null;
    }
    
    /**
     * 
     * @param string $query
     * @return PostDeliveryRegionCollection
     */
    public function searchRegions($query) {
        $regionsInfo = $this->postDeliveryMapper->searchRegions($query);
        $regionCollection = $this->createRegionCollectionInstance();
        foreach ($regionsInfo as $regionInfo) {
            $regionCollection->add($this->createRegion($regionInfo));
        }
        return $regionCollection;
    }

    /**
     *
     * @param string $query
     * @return PostDeliveryRegionCollection
     */
    public function searchCities($query) {
        $regionsInfo = $this->postDeliveryMapper->searchCities($query);
        $regionCollection = $this->createRegionCollectionInstance();
        foreach ($regionsInfo as $regionInfo) {
            $regionCollection->add($this->createRegion($regionInfo));
        }
        return $regionCollection;
    }
    
    /**
     * 
     * @param string | null $query
     * @return array
     */
    public function getMoRegions($query = null) {
        $regionsInfo = $this->postDeliveryMapper->getRegionsByParentId(self::MO_REGION, $query);
        $regionCollection = $this->createRegionCollectionInstance();
        foreach ($regionsInfo as $regionInfo) {
            $regionCollection->add($this->createRegion($regionInfo));
        }
        return $regionCollection;
    }
    
    /**
     * 
     * @return PostDeliveryRegionCollection
     */
    public function getRootRegions() {
        $regionsInfo = $this->postDeliveryMapper->getRootRegions();
        $regionCollection = $this->createRegionCollectionInstance();
        foreach ($regionsInfo as $key =>$regionInfo) {
            $regionCollection->add($this->createRegion($regionInfo), $key);
        }
        return $regionCollection;
    }

    /**
     * 
     * @return PostDeliveryRegionCollection
     */
    public function getAllRootRegions() {
        $regionsInfo = $this->postDeliveryMapper->getAllRootRegions();
        $regionCollection = $this->createRegionCollectionInstance();
        foreach ($regionsInfo as $key =>$regionInfo) {
            $regionCollection->add($this->createRegion($regionInfo), $key);
        }
        return $regionCollection;
    }
    
    /**
     * 
     * @param integer $regionId
     * @return PostDeliveryRegion | null
     */
    public function getRegionById($regionId) {
        $regionInfo = $this->postDeliveryMapper->getRegionById($regionId);
        if (!is_null($regionInfo)) {
            return $this->createRegion($regionInfo);
        }
        return null;
    }
    
    /**
     * 
     * @param integer $parentRegionId
     * @return PostDeliveryRegion | null
     */
    public function getRegionsByParentId($parentRegionId) {
        $regionsInfo = $this->postDeliveryMapper->getRegionsByParentId($parentRegionId);
        $regionCollection = $this->createRegionCollectionInstance();
        foreach ($regionsInfo as $regionInfo) {
            $regionCollection->add($this->createRegion($regionInfo));
        }
        return $regionCollection;
    }

    /**
     * 
     * @param integer $regionId
     * @return boolean
     */
    public function isDropdownRegion($regionId) {
        return (bool)$this->postDeliveryMapper->isDropdownRegion($regionId);
    }
    
    /**
     * 
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getShippedAddingRegions() {
        return $this->postDeliveryMapper->getShippedAddingRegions();
    }
    
    /**
     * 
     * @param array $data
     * @return integer
     */
    public function addShipped(array $data) {
        return $this->postDeliveryMapper->addShipped($data);
    }
    
    /**
     * 
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getRootShippedSummary() {
        return $this->postDeliveryMapper->getRootShippedSummary();
    }
    
    /**
     * 
     * @param array $data
     * @return integer
     */
    public function updateDeliveryDays(array $data) {
        $this->postDeliveryMapper->updateDeliveryDays($data);
    }

    /**
     * 
     * @return PostDeliveryRegion
     */
    public function createRegionInstance() {
        return new PostDeliveryRegion();
    }
    
    /**
     * 
     * @return PostDeliveryRegionCollection
     */
    public function createRegionCollectionInstance() {
        return new PostDeliveryRegionCollection();
    }

    /**
     * 
     * @param array $regionInfo
     * @return PostDeliveryRegion
     */
    public function createRegion($regionInfo) {
        $region = $this->createRegionInstance();
        if (array_key_exists('RegionID', $regionInfo)) {
            $region->setId((int)$regionInfo['RegionID']);
        }
        if (array_key_exists('ParentRegionID', $regionInfo)) {
            $region->setParentId((int)$regionInfo['ParentRegionID']);
        }
        if (array_key_exists('ErpCityID', $regionInfo)) {
            $region->setErpCityId((int)$regionInfo['ErpCityID']);
        }
        if (array_key_exists('TwinDelivery', $regionInfo)) {
            $region->isTwinDelivery($regionInfo['TwinDelivery']);
        }
        if (array_key_exists('TkSdekRegionID', $regionInfo)) {
            $region->setSdekId((int)$regionInfo['TkSdekRegionID']);
        }
        if (array_key_exists('TkPekRegionID', $regionInfo)) {
            $region->setPekId((int)$regionInfo['TkPekRegionID']);
        }
        if (array_key_exists('TkEmsRegionID', $regionInfo)) {
            $region->setEmsId($regionInfo['TkEmsRegionID']);
        }
        if (array_key_exists('RegionName', $regionInfo)) {
            $region->setName($regionInfo['RegionName']);
        }
        if (array_key_exists('EndGroup', $regionInfo)) {
            $region->isEndGroup($regionInfo['EndGroup']);
        }
        if (array_key_exists('CityPhone', $regionInfo)) {
            $region->setPhone($regionInfo['CityPhone']);
        }
        if (array_key_exists('Latitude', $regionInfo)) {
            $region->setLatitude((float)$regionInfo['Latitude']);
        }
        if (array_key_exists('Longitude', $regionInfo)) {
            $region->setLongitude((float)$regionInfo['Longitude']);
        }
        if (array_key_exists('DeliveryDays', $regionInfo)) {
            $region->setDeliveryDays((int)$regionInfo['DeliveryDays']);
        }
        if (array_key_exists('Shipped', $regionInfo)) {
            $region->setShipped((int)$regionInfo['Shipped']);
        }
        if (array_key_exists('Zoom', $regionInfo)) {
            $region->setZoom((int)$regionInfo['Zoom']);
        }
        return $region;
    }
    
}
