<?php

namespace SoloDelivery\Service;

use SoloDelivery\Provider\ProviderInterface;
use SoloDelivery\Reader\ReaderInterface;

class PostDeliveryCalculatorService {
    
    use \SoloERP\Service\ProvidesWebservice;
    
    /**
     *
     * @var array
     */
    protected $config = [];
    
    /**
     *
     * @var array
     */
    protected $providers = [];

    /**
     * 
     * @param array $config
     */
    public function __construct($config) {
        $this->config = $config;
    }
    
    /**
     * 
     * @param array $companies {
     *     @option string "uid" Transport company uid
     *     @option mixed "to" Destination id
     * }
     * @param array $goods {
     *     @option float "volume" Good volume
     *     @option float "weight" Good weight
     *     @option integer "quantity" Good quantity
     *     @option integer "goodId" Good id
     * }
     * @param \DateTime $arrivalDate
     * @param float $price
     * @return \SoloDelivery\Entity\PostDeliveryPriceCollection
     * @throws \RuntimeException
     */
    public function getPrice(array $companies, array $goods, \DateTime $arrivalDate, $price, $additionalTkDeliveryPricesByGoodIds) {
        $postDeliveryCollection = $this->createPostDeliveryCollection();
        $arrivalToCompanyDate = $this->getTransportationToCompanyDate($companies, $arrivalDate);
        foreach ($companies as $company) {
            $twinLogisticPrice = $this->getTwinLogisticPrice($goods, $additionalTkDeliveryPricesByGoodIds);
            if (array_key_exists($company['uid'], $this->config) && array_key_exists($company['uid'], $arrivalToCompanyDate)) {
                $provider = $this->getProvider($company['uid']);
                if (!($provider instanceof ProviderInterface)) {
                    throw new \RuntimeException('Transport company must be instance of ProviderInterface');
                }
                foreach ($goods as $good) {
                    for ($i = 0; $i < $good['quantity']; $i++) {
                        $provider->setGood([
                            'volume' => $good['volume'],
                            'weight' => $good['weight'],
                        ]);
                    }
                }
                $provider->setTo($company['to']);
                $provider->setPrice($price);
                $provider->setArrivalDate($arrivalToCompanyDate[$company['uid']]);
                $reader = $this->getReader($company['uid'], $provider->call());
                $reader->setTo($company['to']);
                $reader->setCompanyUid($company['uid']);
                $reader->setArrivalDate($arrivalToCompanyDate[$company['uid']]);
                $reader->isPickup($this->config[$company['uid']]['pickup']);
                $postDeliveryData = $reader->parse();
                if (!is_null($postDeliveryData)) {
                    if ($this->config[$company['uid']]['addTwinLogisticPrice']) {
                        $wm = $this->getWebMethod('GetPreliminaryDeliveryToLogisticCompany');
                        $articles = [];
                        foreach ($goods as $good) {
                            $articles[] = [
                                'Id' => $good['goodId'],
                                'Quantity' => $good['quantity'],
                            ];
                        }
                        $wm->addPar('Longitude', $this->config[$company['uid']]['center_coords']['longitude']);
                        $wm->addPar('Latitude', $this->config[$company['uid']]['center_coords']['latitude']);
                        $wm->addPar('Articles', $articles);
                        $response = $wm->call();
                        $result = new Ultima\Result\GetPreliminaryDeliveryToLogisticCompanyResult();
                        $result->setData(json_decode($response->getResponse()->getContent()));
                        if (!$response->hasError()) {
                            $twinLogisticPrice += (int)$result->getData()->Cost;
                            $postDeliveryCollection->add($this->createPostDelivery($postDeliveryData, $twinLogisticPrice));
                        }
                    } else {
                        $postDeliveryCollection->add($this->createPostDelivery($postDeliveryData, $twinLogisticPrice));
                    }
                }
            }
        }
        $postDeliveryCollection->recalcPekTerms();
        return $postDeliveryCollection;
    }
    
    /**
     * 
     * @param array $companies {
     *     @option string "uid" Transport company uid
     *     @option mixed "to" Destination id
     * }
     * @param \DateTime $arrivalDate
     * @return array
     */
    public function getTransportationToCompanyDate($companies, $arrivalDate) {
        $result = [];
        foreach ($companies as $company) {
            if (array_key_exists($company['uid'], $this->config) && array_key_exists('schedule', $this->config[$company['uid']])) {
                $schedule = $this->config[$company['uid']]['schedule'];
                usort($schedule, function($a, $b) {
                    return (\SoloCatalog\Service\Helper\DateHelper::isFirstDateMore($a['shipping'], $b['shipping']));
                });
                foreach ($schedule as $scheduleDate) {
                    if (\SoloCatalog\Service\Helper\DateHelper::isFirstDateMore($scheduleDate['shipping'], $arrivalDate)) {
                        $result[$company['uid']] = $scheduleDate['arrival'];
                        break;
                    }
                }
            }
        }
        return $result;
    }

    /**
     * 
     * @return \SoloDelivery\Entity\PostDeliveryPriceCollection
     */
    public function createPostDeliveryCollection() {
        return new \SoloDelivery\Entity\PostDeliveryPriceCollection();
    }
    
    /**
     * 
     * @param array $data
     * @param integer $twinLogisticPrice
     * @return \SoloDelivery\Entity\PostDeliveryPrice
     */
    public function createPostDelivery($data, $twinLogisticPrice = 0) {
        $postDeliveryPrice = new \SoloDelivery\Entity\PostDeliveryPrice();
        $postDeliveryPrice->setCompanyName($data['companyName']);
        $postDeliveryPrice->setCompanyUid($data['companyUid']);
        $postDeliveryPrice->setDeliveryMaxTerm($data['maxTerm']);
        $postDeliveryPrice->setDeliveryMinTerm($data['minTerm']);
        $postDeliveryPrice->setPrice($data['price']);
        $postDeliveryPrice->setTwinLogisticPrice($twinLogisticPrice);
        $postDeliveryPrice->isPickup($data['isPickup']);
        return $postDeliveryPrice;
    }

    /**
     * 
     * @param string $companyName
     * @return ProviderInterface
     */
    protected function getProvider($companyName) {
        if (!array_key_exists($companyName, $this->config)) {
            throw new \RuntimeException('Can\'t find provider options for ' . $companyName . ' transport company');
        }
        if (!in_array($companyName, $this->providers)) {
            $provider = \SoloDelivery\Provider\ProviderFactory::factory($this->config[$companyName]['provider'], $this->config[$companyName]['options']);
            $this->providers[$companyName] = $provider;
        }
        return $this->providers[$companyName];
    }
    
    /**
     * 
     * @param string $companyName
     * @param array $result
     * @return ReaderInterface
     */
    protected function getReader($companyName, $result) {
        if (!in_array($companyName, $this->providers)) {
            $reader = \SoloDelivery\Reader\ReaderFactory::factory($this->config[$companyName]['provider']);
            $this->readers[$companyName] = $reader;
        }
        $reader->setResponse($result);
        return $this->readers[$companyName];
    }
    
    /**
     * 
     * @param array $goods
     * @param array $additionalTkDeliveryPricesByGoodIds
     * @return float
     */
    protected function getTwinLogisticPrice($goods, $additionalTkDeliveryPricesByGoodIds) {
        $result = 0;
        foreach ($goods as $good) {
            if (isset($additionalTkDeliveryPricesByGoodIds[$good['goodId']])) {
                $result += $additionalTkDeliveryPricesByGoodIds[$good['goodId']] * $good['quantity'];
            }
        }
        return $result;
    }
    
}
