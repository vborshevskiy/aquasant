<?php

namespace SoloDelivery\Service;

use SoloDelivery\Service\PostDeliveryTermsService;

trait ProvidesPostDeliveryTerms {

    /**
     *
     * @var PostDeliveryTermsService
     */
    private static $postDeliveryTermsService = null;

    /**
     *
     * @param PostDeliveryTermsService $postDeliveryTermsService        	
     */
    public static function setPostDeliveryTermsService(PostDeliveryTermsService $postDeliveryTermsService) {
        self::$postDeliveryTermsService = $postDeliveryTermsService;
    }

    /**
     *
     * @return PostDeliveryTermsService
     */
    public static function getPostDeliveryTermsService() {
        return self::$postDeliveryTermsService;
    }

    /**
     *
     * @return PostDeliveryTermsService|null
     */
    protected function postDeliveryTerms() {
        if (null !== self::$postDeliveryTermsService) {
            return self::getPostDeliveryTermsService();
        }
        return null;
    }

}

