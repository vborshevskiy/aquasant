<?php

namespace SoloDelivery\Service\Helper;

use SoloCatalog\Service\Helper\DateHelper;
use Solo\DateTime\DateTime;

class DeliveryHelper {

    /**
     * 
     * @param \DateTime $date
     * @return string
     */
    public function getTextDeliveryDate(\DateTime $date) {
        if (DateHelper::isFutureDate($date)) {
            if (DateHelper::isToday($date))
                return 'сегодня';
            if (DateHelper::isTomorrow($date))
                return 'завтра';
            if (DateHelper::isDayAfterTomorrow($date))
                return 'послезавтра';
            return $date->format('d') . ' ' . DateHelper::getMonthNameGenitive($date->format('m'));
        }
        return null;
    }

    /**
     *
     * @param string $time        	
     * @return DateTime
     */
    public function getTimeForRange($time) {
        $timeData = explode(':', $time);
        if (isset($timeData[0]) && isset($timeData[1])) {
            $hour = $timeData[0];
            $minute = $timeData[1];
            $date = new DateTime;
            return $date->setTime($hour, $minute);
        }
        return false;
    }

    public function getHoursMinutes($time) {
        $data = '';
        if (isset($time)) {
            $data .= date('H', $time->getInfo()[0]);
            $data .= ':';
            $data .= date('i', $time->getInfo()[0]);
            return $data;
        }
        return false;
    }

    /**
     *
     * @param string $date        	
     * @return DateTime
     */
    public function getDateForRange($date) {
        return new DateTime($date);
    }

}

?>