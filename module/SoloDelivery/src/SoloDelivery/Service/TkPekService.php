<?php

namespace SoloDelivery\Service;

use SoloDelivery\Data\TkPekMapperInterface;
use SoloDelivery\Entity\PekWarehouse;
use SoloDelivery\Entity\PekWarehouseCollection;

class TkPekService {
    
    /**
     *
     * @var TkPekMapperInterface
     */
    protected $tkPekMapper;

    /**
     * 
     * @param TkPekMapperInterface $tkPekMapper
     */
    public function __construct(TkPekMapperInterface $tkPekMapper) {
        $this->tkPekMapper = $tkPekMapper;
    }
    
    /**
     * 
     * @return PekWarehouseCollection
     */
    public function getPekWarehouses() {
        $pekWarehousesData = $this->tkPekMapper->getPekWarehouses();
        $pekWarehouseCollection = $this->createPekWarehouseCollection();
        foreach ($pekWarehousesData as $pekWarehouseData) {
            $pekWarehouse = $this->createPekWarehouse();
            $pekWarehouse->fillFromArray($pekWarehouseData);
            $pekWarehouseCollection->add($pekWarehouse);
        }
        return $pekWarehouseCollection;
    }
    
    /**
     * 
     * @param integer $regionId
     * @return PekWarehouseCollection
     */
    public function getPekWarehousesByRegionId($regionId) {
        if (!is_int($regionId)) {
            throw new \InvalidArgumentException('Region id must be integer');
        }
        $pekWarehousesData = $this->tkPekMapper->getPekWarehousesByRegionId($regionId);
        $pekWarehouseCollection = $this->createPekWarehouseCollection();
        foreach ($pekWarehousesData as $pekWarehouseData) {
            $pekWarehouse = $this->createPekWarehouse();
            $pekWarehouse->fillFromArray($pekWarehouseData);
            $pekWarehouseCollection->add($pekWarehouse);
        }
        return $pekWarehouseCollection;
    }
    
    /**
     * 
     * @return integer
     */
    public function getPekWarehousesCount() {
        return $this->tkPekMapper->getPekWarehousesCount();
    }
    
    /**
     * 
     * @param integer $pekWarehouseId
     * @return PekWarehouse
     */
    public function getWarehouseById($pekWarehouseId) {
        $pekWarehouseData = $this->tkPekMapper->getWarehouseById($pekWarehouseId);
        $pekWarehouse = $this->createPekWarehouse();
        return $pekWarehouse->fillFromArray((array)$pekWarehouseData);
    }

    /**
     * 
     * @return PekWarehouseCollection
     */
    public function createPekWarehouseCollection() {
        return new PekWarehouseCollection();
    }
    
    /**
     * 
     * @return PekWarehouse
     */
    public function createPekWarehouse() {
        return new PekWarehouse();
    }
    
}
