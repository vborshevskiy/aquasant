<?php

namespace SoloDelivery\Service;

use SoloDelivery\Entity\DeliveryTimeRangeCollection;
use SoloDelivery\Entity\DeliveryTimeRange;
use SoloDelivery\Data\DeliveryTimeRangesTableInterface;
use SoloDelivery\Service\Helper\DeliveryHelper;

class DeliveryTimeRangeService {

    /**
     *
     * @var DeliveryTimeRangesTableInterface
     */
    protected $deliveryTimeRangesTable;
    
    /**
     *
     * @var DeliveryHelper
     */
    protected $helper;

    /**
     *
     * @param DeliveryTimeRangesTableInterface $deliveryTimeRangesTable
     * @param DeliveryHelper $helper
     */
    public function __construct(DeliveryTimeRangesTableInterface $deliveryTimeRangesTable, DeliveryHelper $helper) {
        $this->deliveryTimeRangesTable = $deliveryTimeRangesTable;
        $this->helper = $helper;
    }
    
    public function helper() {
        return $this->helper;
    }

    /**
     * 
     * @return LogisticCompanyCollection
     */
    public function getDeliveryTimeRanges() {
        $deliveryTimeRangesData = $this->deliveryTimeRangesTable->getDeliveryTimeRanges();
        $deliveryTimeRangeCollection = new DeliveryTimeRangeCollection();
        foreach ($deliveryTimeRangesData as $deliveryTimeRangeData) {
            $deliveryTimeRangeCollection->add($this->createDeliveryTimeRange($deliveryTimeRangeData));
        }
        return $deliveryTimeRangeCollection;
    }

    /**
     * 
     * @param ResultSet $deliveryTimeRangeData
     * @return DeliveryTimeRange
     */
    private function createDeliveryTimeRange($deliveryTimeRangeData) {
        $time = new DeliveryTimeRange();
        $time->setId((int) $deliveryTimeRangeData['TimeRangeID']);
        $time->setName($deliveryTimeRangeData['TimeRangeName']);
        $time->setTimeFrom($this->helper()->getTimeForRange($deliveryTimeRangeData['TimeFrom']));
        $time->setTimeTo($this->helper()->getTimeForRange($deliveryTimeRangeData['TimeTo']));
        $time->setDateTimeFrom($this->helper()->getDateForRange($deliveryTimeRangeData['DateFrom']));
        $time->setDateTimeTo($this->helper()->getDateForRange($deliveryTimeRangeData['DateTo']));
        return $time;
    }

}

?>