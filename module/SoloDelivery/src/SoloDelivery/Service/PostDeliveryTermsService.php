<?php

namespace SoloDelivery\Service;

use SoloDelivery\Data\PostDeliveryTermsMapperInterface;

class PostDeliveryTermsService {
    
    /**
     *
     * @var PostDeliveryTermsMapperInterface
     */
    protected $postDeliveryTermsMapper;

    /**
     *
     * @param PostDeliveryTermsMapperInterface $postDeliveryTermsMapper     	
     */
    public function __construct(PostDeliveryTermsMapperInterface $postDeliveryTermsMapper) {
        $this->postDeliveryTermsMapper = $postDeliveryTermsMapper;
    }
    
    /**
     * 
     * @param integer $tkPekRegionId
     * @return array|null
     */
    public function getTerms($tkPekRegionId) {
        return $this->postDeliveryTermsMapper->getTerms($tkPekRegionId);
    }
    
}
