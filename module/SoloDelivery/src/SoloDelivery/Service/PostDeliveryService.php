<?php

namespace SoloDelivery\Service;

use Solo\Cookie\Cookie;

class PostDeliveryService {
    
    const DEFAULT_CITY_ID = 42;
    
    /**
     *
     * @var array
     */
    protected $transportCompaniesConfig = [];

    /**
     * 
     * @param array $transportCompaniesConfig
     */
    public function __construct(array $transportCompaniesConfig) {
        $this->transportCompaniesConfig = $transportCompaniesConfig;
    }
    
    /**
     * 
     * @param string $transportCompanyUid
     * @return mixed
     */
    public function getTransportCompanyErpIdByUid($transportCompanyUid) {
        if (isset($this->transportCompaniesConfig[$transportCompanyUid]['erp_id'])) {
            return $this->transportCompaniesConfig[$transportCompanyUid]['erp_id'];
        }
        return null;
    }
    
    /**
     * 
     * @return boolean
     */
    public function hasLocality() {
        return ($this->getUserCityId() > 0 || $this->getUserRegionId() > 0);
    }
    
    /**
     * 
     * @return type
     */
    public function getUserRegionId() {
        if ($this->hasUserRegionId()) {
            return (int)Cookie::get('post-delivery-region-id');
        }
        return null;
    }
    
    /**
     * 
     * @return boolean
     */
    public function hasUserRegionId() {
        return Cookie::exists('post-delivery-region-id');
    }

    /**
     * 
     * @param integer $regionId
     * @return integer
     * @throws \InvalidArgumentException
     */
    public function setUserRegionId($regionId = null) {
        if (!is_int($regionId)) {
            $regionId = self::DEFAULT_CITY_ID;
        }
        Cookie::set('post-delivery-region-id', $regionId, null, strtotime('+ 1 year'), '/');
        return $regionId;
    }
    
    /**
     * 
     * @param boolean | null $isConfirmed
     * @return boolean
     */
    public function isUserRegionConfirmed($isConfirmed = null) {
        if (is_null($isConfirmed)) {
            return (bool)Cookie::get('post-delivery-region-confirmed');
        }
        if ($isConfirmed) {
            Cookie::set('post-delivery-region-confirmed', 1, null, strtotime('+ 1 year'), '/');
        } else {
            Cookie::killCookie('post-delivery-city-confirmed');
        }
        return (bool)$isConfirmed;
    }
    
}
