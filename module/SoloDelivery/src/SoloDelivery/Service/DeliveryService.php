<?php

namespace SoloDelivery\Service;

use SoloERP\Service\ProvidesWebservice;
use SoloCatalog\Entity\Avail\AvailCollection;
use SoloCatalog\Service\Helper\DateHelper;
use SoloOrder\Entity\Order\GoodsCollection;
use SoloDelivery\Data\DeliveryMapperInterface;
use SoloDelivery\Service\Helper\DeliveryHelper;
use SoloDelivery\Entity\DeliveryTimeRangeCollection;
use SoloDelivery\Entity\DeliveryTimeRange;
use SoloDelivery\Entity\DeliveryGood;
use Solo\DateTime\DateTime;

class DeliveryService extends \Solo\ServiceManager\ServiceLocatorAwareService {

    use ProvidesWebservice;

    const MOSCOW_ID = 2;

    /**
     *
     * @var DeliveryMapperInterface
     */
    protected $deliveryMapper;

    /**
     *
     * @var DeliveryHelper
     */
    protected $helper;

    /**
     *
     * @param DeliveryMapperInterface $deliveryMapper
     */
    public function __construct(DeliveryMapperInterface $deliveryMapper, DeliveryHelper $helper) {
        $this->deliveryMapper = $deliveryMapper;
        $this->helper = $helper;
    }

    /**
     *
     * @return DeliveryHelper
     */
    public function helper() {
        return $this->helper;
    }

    /**
     *
     * @param integer $goodId
     * @param integer $cityId
     * @return \DateTime|null
     * @throws \InvalidArgumentException
     */
    public function getSingleGoodDeliveryDate($goodId, $cityId) {
        if (!is_integer($goodId)) {
            throw new \InvalidArgumentException('Good id must be integer');
        }
        if (!is_integer($cityId)) {
            throw new \InvalidArgumentException('City id must be integer');
        }
        $date = $this->deliveryMapper->getSingleGoodDeliveryDate($goodId, $cityId);
        return \DateTime::createFromFormat('Y-m-d H:i:s', $date);
    }

    public function getPossibleDeliveryDates($availabilities, GoodsCollection $goodsCollection, $cityId = self::MOSCOW_ID, $regionId = null, $minDeliveryDate = 0)
    {
    	
        $goodIds = array_keys($goodsCollection->toArray());
        $goodQuantities = $goodsCollection->getQuantitiesByGoodIds();
        $deliveryDates = $this->getDeliveryDatesInstance();

		$showcases = [];
		foreach ($goodsCollection->toArray() as $goodId => $good) {
			//print_r($good); exit();
			if ($good->allowShowcaseSale()) {
				$showcases[$goodId] = $goodId;
			}
		}

        /** OLD version
		$goodAvailDeliveryDates = $this->getAvailDeliveryDateByGoodIds($cityId, $goodIds);
        $goodSuborderDeliveryDates = $this->getSuborderDeliveryDateByGoodIds($cityId, $goodIds);
		*/
		$goodReserveAvailDates = $this->getReserveAvailDates($cityId, $goodIds);

        foreach ($goodQuantities as $goodId => $quantity) {
            $availQuantity = (int) $availabilities[$goodId]->AvailQuantity;
            $suborderQuantity = (int) $availabilities[$goodId]->SuborderQuantity;

            $deliveryGood = new DeliveryGood();
            $deliveryGood
                ->setId($goodId)
                ->setQuantity($quantity)
                ->setAvailQuantity($availQuantity)
                ->setSuborderQuantity($suborderQuantity);

			/** OLD version
            if (array_key_exists($goodId, $goodAvailDeliveryDates)) {
                $deliveryGood->setAvailDeliveryDate($goodAvailDeliveryDates[$goodId]);
            }
            if (array_key_exists($goodId, $goodSuborderDeliveryDates)) {
                $deliveryGood->setSuborderDeliveryDate($goodSuborderDeliveryDates[$goodId]);
            }
			*/

			if (array_key_exists($goodId, $goodReserveAvailDates) && isset($goodReserveAvailDates[$goodId]['DeliveryDate']) && $goodReserveAvailDates[$goodId]['DeliveryDate']) {
                $deliveryGood->setDeliveryDate($goodReserveAvailDates[$goodId]['DeliveryDate']);
            }
            if (array_key_exists($goodId, $showcases) && array_key_exists($goodId, $goodReserveAvailDates) && isset($goodReserveAvailDates[$goodId]['ShowcaseRetrieveDate']) && $goodReserveAvailDates[$goodId]['ShowcaseRetrieveDate']) {
                $deliveryGood->setShowcaseRetrieveDate($goodReserveAvailDates[$goodId]['ShowcaseRetrieveDate']);
            }

            $deliveryDates->addGood($deliveryGood);
        }
        
        $unavailDates = $this->getUnavailDays();
        if (59 == $regionId) {
        	$unavailDate = DateTime::createFromTimestamp(time());
        	if ((4 == $unavailDate->getWeekday()) && (19 <= $unavailDate->getHours())) {
        		$unavailDate->add(new \DateInterval('P2D'));
        		$unavailDates[] = clone $unavailDate;
        		$unavailDate->add(new \DateInterval('P1D'));
        		$unavailDates[] = clone $unavailDate;
        	}
        	if (5 == $unavailDate->getWeekday()) {
        		$unavailDate->add(new \DateInterval('P1D'));
        		$unavailDates[] = clone $unavailDate;
        		$unavailDate->add(new \DateInterval('P1D'));
        		$unavailDates[] = clone $unavailDate;
        	}
        	for ($i = 0; $i < 20; $i++) {
        		if (!in_array($unavailDate->getWeekday(), [6, 0])) {
        			$unavailDates[] = clone $unavailDate;
        		}
        		$unavailDate->add(new \DateInterval('P1D'));
        	}
        }

        $deliveryDates->setTimeRanges($this->getDeliveryTimeRange());
        $deliveryDates->setUnavailDates($unavailDates);
        $deliveryDates->setAvailDays($this->getAvailDayNumbers($cityId));
        
        $daysCount = 7;
        if (59 == $regionId) {
        	$daysCount = 6;
        }

        return $deliveryDates->getPossibleDeliveryDates($daysCount, $minDeliveryDate);
    }

	/**
     * @param integer $cityId
     * @param array $goodIds
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getReserveAvailDates($cityId, $goodIds) {
        if (!is_array($goodIds)) {
            throw new \InvalidArgumentException('Good ids must be array');
        }
        if (!is_int($cityId)) {
            throw new \InvalidArgumentException('City id must be integer');
        }
        $goodsReserveAvailDates = $this->deliveryMapper->getAvailDeliveryDateByGoodIds($cityId, $goodIds);
        $result = [];
        if (!is_null($goodsReserveAvailDates)) {
            foreach ($goodsReserveAvailDates as $goodsReserveAvailDate) {
                $result[$goodsReserveAvailDate->GoodID]['DeliveryDate'] = \DateTime::createFromFormat('Y-m-d H:i:s', $goodsReserveAvailDate->DeliveryDate);
				$result[$goodsReserveAvailDate->GoodID]['ShowcaseRetrieveDate'] = \DateTime::createFromFormat('Y-m-d H:i:s', $goodsReserveAvailDate->ShowcaseRetrieveDate);
            }
        }
        return $result;
    }

    /**
     * OLD version method
     * @param integer $cityId
     * @param array $goodIds
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getAvailDeliveryDateByGoodIds($cityId, $goodIds) {
        if (!is_array($goodIds)) {
            throw new \InvalidArgumentException('Good ids must be array');
        }
        if (!is_int($cityId)) {
            throw new \InvalidArgumentException('City id must be integer');
        }
        $goodsAvailDeliveryDate = $this->deliveryMapper->getAvailDeliveryDateByGoodIds($cityId, $goodIds);
        $result = [];
        if (!is_null($goodsAvailDeliveryDate)) {
            foreach ($goodsAvailDeliveryDate as $goodAvailDeliveryDate) {
                $result[$goodAvailDeliveryDate->GoodID] = \DateTime::createFromFormat('Y-m-d H:i:s', $goodAvailDeliveryDate->DeliveryDate);
            }
        }
        return $result;
    }

    public function getDeliveryDatesByGoodId($cityId, $goodId) {
        $result = $this->deliveryMapper->getDeliveryDatesByGoodId($cityId, $goodId);

        return $result->current();
    }

    /**
     * OLD version method
     * @param integer $cityId
     * @param array $goodIds
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getSuborderDeliveryDateByGoodIds($cityId, $goodIds) {
        if (!is_array($goodIds)) {
            throw new \InvalidArgumentException('Good ids must be array');
        }
        if (!is_int($cityId)) {
            throw new \InvalidArgumentException('City id must be integer');
        }
        $goodsSuborderDeliveryDate = $this->deliveryMapper->getSuborderDeliveryDateByGoodIds($cityId, $goodIds);
        $result = [];
        if (!is_null($goodsSuborderDeliveryDate)) {
            foreach ($goodsSuborderDeliveryDate as $goodSuborderDeliveryDate) {
                $result[$goodSuborderDeliveryDate->GoodID] = \DateTime::createFromFormat('Y-m-d H:i:s', $goodSuborderDeliveryDate->DeliveryDate);
            }
        }
        return $result;
    }

    /**
     *
     * @param integer $cityId
     * @param array $goodIds
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getMinDeliveryDateByGoodIds($cityId, $goodIds) {
        if (!is_array($goodIds)) {
            throw new \InvalidArgumentException('Good ids must be array');
        }
        if (!is_int($cityId)) {
            throw new \InvalidArgumentException('City id must be integer');
        }
        $goodsDeliveryDates = $this->deliveryMapper->getDeliveryDatesByGoodIds($cityId, $goodIds);
        $result = [];
        foreach ($goodsDeliveryDates as $goodsDeliveryDate) {
            $availDeliveryDate = \DateTime::createFromFormat('Y-m-d H:i:s', $goodsDeliveryDate['AvailDeliveryDate']);
            $suborderDeliveryDate = \DateTime::createFromFormat('Y-m-d H:i:s', $goodsDeliveryDate['SuborderDeliveryDate']);
            $minDeliveryDate = DateHelper::getMinDate([$availDeliveryDate,$suborderDeliveryDate]);
            $result[$goodsDeliveryDate->GoodID] = DateHelper::getMinDate([$availDeliveryDate,$suborderDeliveryDate]);
        }
        return $result;
    }

    /**
     *
     * @return DeliveryTimeRangeCollection
     */
    public function getDeliveryTimeRange() {
        $deliveryTimeRangesData = $this->deliveryMapper->getDeliveryTimeRange();
        $deliveryTimeRangeCollection = new DeliveryTimeRangeCollection();
        foreach ($deliveryTimeRangesData->toArray() as $deliveryTimeRangeData) {
            $deliveryTimeRangeCollection->add($this->createDeliveryTimeRange($deliveryTimeRangeData));
        }
        return $deliveryTimeRangeCollection;
    }

    /**
     *
     * @return integer | null
     */
    public function getAnyDeliveryTimeRangeId() {
        return $this->deliveryMapper->getAnyDeliveryTimeRangeId();
    }

    /**
     *
     * @return array
     */
    public function getUnavailDays() {
        $unavailDaysData = $this->deliveryMapper->getUnavailDays();
        if (is_null($unavailDaysData)) {
            return [];
        }
        $unavailDays = \Solo\Stdlib\ArrayHelper::enumOneColumn($unavailDaysData, 'Day');
        foreach ($unavailDays as &$unavailDay) {
            $unavailDay = new \DateTime($unavailDay);
        }
        unset($unavailDay);
        return $unavailDays;
    }

    /**
     *
     * @param integer $cityId
     * @return array
     */
    public function getAvailDayNumbers($cityId) {
        $availDayNumbers = $this->deliveryMapper->getAvailDayNumbers($cityId);
        if (empty($cityId)) {
            return null;
        }
        return \Solo\Stdlib\ArrayHelper::enumOneColumn($availDayNumbers, 'WeekdayNumber');
    }

    /**
     *
     * @param integer $orderId
     * @param string $securityKey
     * @param array $deliveryInfo
     * @return \SoloDelivery\Service\Ultima\Result\GetDeliveryCostResult
     */
    public function getDeliveryCost($orderId, $securityKey, $deliveryInfo) {
        $wm = $this->getDeliveryCostWebMethod();
        $wm->addPar('ReserveId', $orderId);
        $wm->addPar('SecurityKey', $securityKey);
        $wm->addPar('Delivery', $deliveryInfo);
        $response = $wm->call();
        $result = new Ultima\Result\GetDeliveryCostResult();
        $result->setData(json_decode($response->getResponse()->getContent()));
        if ($response->hasError()) {
            $result->setError($response->getError());
        }
        return $result;
    }

    /*
     * @param string $coords
     * @return array
     */
    public function parseCoords($coords) {
        if (is_array($coords) && count($coords) == 2) {
            $result = new \stdClass;
            $result->latitude = $coords[0];
            $result->longitude = $coords[1];
            return $result;
        } elseif (is_string($coords)) {
            preg_match('/^\[([0-9]*[.][0-9]*){1},([0-9]*[.][0-9]*){1}\]/', $coords, $matches);
            if (isset($matches[1]) && isset($matches[2]) && !empty($matches[1]) && !empty($matches[2])) {
				$result = new \stdClass();
				$result->latitude = $matches[1];
				$result->longitude = $matches[2];
				return $result;
			}
		}
		return null;
	}

	/**
	 * 
	 * @param string $latitude
	 * @param string $longitude
	 * @param array $articles
	 * @param integer $floor
	 * @param integer $elevatorTypeId
	 * @return boolean
	 */
	public function getPreliminaryDeliveryCost($latitude, $longitude, array $articles, $floor = 0, $elevatorTypeId = 0) {
		$wm = $this->getPreliminaryDeliveryWebMethod();
		$wm->addPar('Longitude', $longitude);
		$wm->addPar('Latitude', $latitude);
		if (0 < $floor) {
			$wm->addPar('Latitude', $latitude);
		}
		$articlesInfo = [];
		foreach ($articles as $articleId => $quantity) {
        	$articlesInfo[] = ['Id' => $articleId, 'Quantity' => $quantity];
        }
        $wm->addPar('Articles', $articlesInfo);
        
        $wm->addPar('Floor', $floor);
        $wm->addPar('ElevatorTypeID', $elevatorTypeId);

        $response = $wm->call();

        $result = json_decode($response->getResponse()->getContent());
//         print json_encode($wm->getPars(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
//         print_r($result);
//         print "\n----------------------------\n";
        //exit();

        if ($response->hasError()) {
            return false;
        }
        return $result->Cost;
    }

    /**
     *
     * @param ResultSet $deliveryTimeRangeData
     * @return DeliveryTimeRange
     */
    private function createDeliveryTimeRange($deliveryTimeRangeData) {
        $time = new DeliveryTimeRange();
        $time->setId((int) $deliveryTimeRangeData['TimeRangeID']);
        $time->setName($deliveryTimeRangeData['TimeRangeName']);
        $time->setTimeFrom(new \DateTime($deliveryTimeRangeData['TimeFrom']));
        $time->setTimeTo(new \DateTime($deliveryTimeRangeData['TimeTo']));
        $time->setDateTimeFrom(new \DateTime($deliveryTimeRangeData['DateFrom']));
        $time->setDateTimeTo(new \DateTime($deliveryTimeRangeData['DateTo']));
        return $time;
    }

    /**
     *
     * @return \SoloERP\Data\Method\GetDeliveryCost
     */
    public function getDeliveryCostWebMethod() {
        return $this->getWebMethod('GetDeliveryCost');
    }

    /**
     *
     * @return \SoloERP\Data\Method\GetPreliminaryDelivery
     */
    public function getPreliminaryDeliveryWebMethod() {
        return $this->getWebMethod('GetPreliminaryDelivery');
    }

    /**
     *
     * @return \SoloDelivery\Entity\DeliveryDates
     */
    public function getDeliveryDatesInstance() {
        return $this->getServiceLocator()->get('SoloDelivery\Entity\DeliveryDates');
    }

    public function getElevatorTypes() {
        return $this->deliveryMapper->getElevatorTypes()->toArray();
    }

    public function getLiftingPrices() {
        $rows = $this->deliveryMapper->getLiftingPrices();

        $result = [];

        foreach ($rows as $row) {
            $result[$row->ElevatorTypeID] = (int) $row->Price;
        }

        return $result;
    }

    public function getLiftingCost($elevatorTypeId, $floor) {
        $prices = $this->getLiftingPrices();

        $price = $prices[$elevatorTypeId] * $floor;

        return $price;
    }

}
