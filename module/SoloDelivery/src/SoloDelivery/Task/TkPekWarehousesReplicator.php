<?php

namespace SoloDelivery\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloDelivery\Data\TkPekWarehousesLinkRegionsTableInterface;
use SoloDelivery\Data\TkPekWarehousesTableInterface;
use SoloDelivery\Data\TkPekMapperInterface;

class TkPekWarehousesReplicator extends AbstractReplicationTask {

    /**
     *
     * @var array
     */
    private $config = [];

    /**
     *
     * @var TkPekWarehousesLinkRegionsTableInterface
     */
    private $tkPekWarehousesLinkRegionsTable;
    
    /**
     *
     * @var TkPekWarehousesTableInterface
     */
    private $tkPekWarehousesTable;
    
    /**
     *
     * @var TkPekMapperInterface
     */
    private $tkPekMapper;
    
    /**
     * 
     * @param TkPekWarehousesLinkRegionsTableInterface $tkPekWarehousesLinkRegionsTable
     * @param TkPekWarehousesTableInterface $tkPekWarehousesTable
     */
    public function __construct(
            array $config,
            TkPekWarehousesLinkRegionsTableInterface $tkPekWarehousesLinkRegionsTable,
            TkPekWarehousesTableInterface $tkPekWarehousesTable,
            TkPekMapperInterface $tkPekMapper
        ) {
        $this->config = $config;
        $this->tkPekWarehousesLinkRegionsTable = $tkPekWarehousesLinkRegionsTable;
        $this->tkPekWarehousesTable = $tkPekWarehousesTable;
        $this->tkPekMapper = $tkPekMapper;
        
        $this->addSwapTable('tk_pek_warehouses', 'tk_pek_warehouses_link_regions');
        $this->scriptNotRespondingTimeInMinutes = 120 * 24;
    }
    
    public function process() {
        $data = $this->getData();
        
        $regionsLinkPekCityIds = $this->tkPekMapper->getPekRegions();
        
        $warehousesData = [];
        $warhousesLinkCitiesDate = [];
        $whIterator = 1;
        foreach ($data['branches'] as $branch) {
            $cityIds = [];
            if ($branch['bitrixId'] > 0) {
                $cityIds[] = (int)$branch['bitrixId'];
            }
            foreach ($branch['cities'] as $city) {
                if ($city['bitrixId'] > 0) {
                    $cityIds[] = (int)$city['bitrixId'];
                }
            }
            foreach ($branch['warehouses'] as $warehouse) {
                $coord = explode(';', $warehouse['coordinates']);
                $latitude = (float)str_replace(',', '.', $coord[0]);
                $longitude = (float)str_replace(',', '.', $coord[1]);
                for ($i = 0; $i < 7; $i++) {
                    ${'warehouseWorkTimeDay' . $i . 'From'} = null;
                    ${'warehouseWorkTimeDay' . $i . 'To'} = null;
                    ${'warehouseWorkTimeDay' . $i . 'DinnerFrom'} = null;
                    ${'warehouseWorkTimeDay' . $i . 'DinnerTo'} = null;
                    ${'divisionWorkTimeDay' . $i . 'From'} = null;
                    ${'divisionWorkTimeDay' . $i . 'To'} = null;
                    ${'divisionWorkTimeDay' . $i . 'DinnerFrom'} = null;
                    ${'divisionWorkTimeDay' . $i . 'DinnerTo'} = null;
                }
                if (is_array($warehouse['timeOfWork'])) {
                    foreach ($warehouse['timeOfWork'] as $timeOfWork) {
                        $day = (int)$timeOfWork['dayOfWeek'];
                        if ($timeOfWork['workFrom']) {
                            ${'warehouseWorkTimeDay' . $day . 'From'} = $timeOfWork['workFrom'] . ':00';
                        }
                        if ($timeOfWork['workTo']) {
                            ${'warehouseWorkTimeDay' . $day . 'To'} = $timeOfWork['workTo'] . ':00';
                        }
                        if ($timeOfWork['dinnerFrom']) {
                            ${'warehouseWorkTimeDay' . $day . 'DinnerFrom'} = $timeOfWork['dinnerFrom'] . ':00';
                        }
                        if ($timeOfWork['dinnerTo']) {
                            ${'warehouseWorkTimeDay' . $day . 'DinnerTo'} = $timeOfWork['dinnerTo'] . ':00';
                        }
                    }
                }
                if (is_array($warehouse['divisionTimeOfWork'])) {
                    foreach ($warehouse['divisionTimeOfWork'] as $timeOfWork) {
                        $day = (int)$timeOfWork['dayOfWeek'];
                        if ($timeOfWork['workFrom']) {
                            ${'divisionWorkTimeDay' . $day . 'From'} = $timeOfWork['workFrom'] . ':00';
                        }
                        if ($timeOfWork['workTo']) {
                            ${'divisionWorkTimeDay' . $day . 'To'} = $timeOfWork['workTo'] . ':00';
                        }
                        if ($timeOfWork['dinnerFrom']) {
                            ${'divisionWorkTimeDay' . $day . 'DinnerFrom'} = $timeOfWork['dinnerFrom'] . ':00';
                        }
                        if ($timeOfWork['dinnerTo']) {
                            ${'divisionWorkTimeDay' . $day . 'DinnerTo'} = $timeOfWork['dinnerTo'] . ':00';
                        }
                    }
                }
                $warehouseRecord = [
                    'warehousePekID' => $warehouse['id'],
                    'divisionPekID' => $warehouse['divisionId'],
                    'warehouseName' => $warehouse['name'],
                    'warehouseAddress' => $warehouse['address'],
                    'isAcceptanceOnly' => (int)$warehouse['isAcceptanceOnly'],
                    'isFreightSurcharge' => (int)$warehouse['isFreightSurcharge'],
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'warehouseEmail' => $warehouse['email'],
                    'warehousePhone' => $warehouse['telephone'],
                    'isRestrictions' => (int)$warehouse['isRestrictions'],
                    'maxWeight' => (float)str_replace(',', '.', $warehouse['maxWeight']),
                    'maxVolume' => (float)str_replace(',', '.', $warehouse['maxVolume']),
                    'maxWeightPerPlace' => (float)str_replace(',', '.', $warehouse['maxWeightPerPlace']),
                    'maxDimention' => (float)str_replace(',', '.', $warehouse['maxDimention']),
                    'cityName' => $this->getCityName($warehouse['name'], $warehouse['address']),
                ];
                for ($i = 0; $i < 7; $i++) {
                    $warehouseRecord['warehouseWorkTimeDay' . $i . 'From'] = ${'warehouseWorkTimeDay' . $i . 'From'};
                    $warehouseRecord['warehouseWorkTimeDay' . $i . 'To'] = ${'warehouseWorkTimeDay' . $i . 'To'};
                    $warehouseRecord['warehouseWorkTimeDay' . $i . 'DinnerFrom'] = ${'warehouseWorkTimeDay' . $i . 'DinnerFrom'};
                    $warehouseRecord['warehouseWorkTimeDay' . $i . 'DinnerTo'] = ${'warehouseWorkTimeDay' . $i . 'DinnerTo'};
                }
                for ($i = 0; $i < 7; $i++) {
                    $warehouseRecord['divisionWorkTimeDay' . $i . 'From'] = ${'divisionWorkTimeDay' . $i . 'From'};
                    $warehouseRecord['divisionWorkTimeDay' . $i . 'To'] = ${'divisionWorkTimeDay' . $i . 'To'};
                    $warehouseRecord['divisionWorkTimeDay' . $i . 'DinnerFrom'] = ${'divisionWorkTimeDay' . $i . 'DinnerFrom'};
                    $warehouseRecord['divisionWorkTimeDay' . $i . 'DinnerTo'] = ${'divisionWorkTimeDay' . $i . 'DinnerTo'};
                }
                
                $noCities = true;
                foreach ($cityIds as $cityId) {
                    if (isset($regionsLinkPekCityIds[$cityId])) {
                        $noCities = false;
                        $warhousesLinkCitiesDate[] = [
                            'warehouseID' => $whIterator,
                            'RegionID' => $regionsLinkPekCityIds[$cityId],
                        ];
                    }
                }
                
                if (!$noCities) {
                    $warehousesData[] = $warehouseRecord;
                    $whIterator++;
                }
            }
            
        }
        
        $this->tkPekWarehousesLinkRegionsTable->truncate();
        $this->tkPekWarehousesTable->truncate();
        
        $this->tkPekWarehousesTable->insertSet($warehousesData);
        foreach (array_chunk($warhousesLinkCitiesDate, 500) as $chunk) {
            $this->tkPekWarehousesLinkRegionsTable->insertSet($chunk);
        }
    }
    
    /**
     * 
     * @return array
     */
    protected function getData() {
        $ch = curl_init();
        $curlOptions = [
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_CAINFO => $this->config['sertificateFile'],
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
            CURLOPT_ENCODING => 'gzip',
            CURLOPT_USERPWD => sprintf('%s:%s', $this->config['login'], $this->config['pass']),
            CURLOPT_URL => $this->config['path'] . $this->config['method'],
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json; charset=utf-8',
                'Content-Length: 0',
            ],
        ];
        curl_setopt_array($ch, $curlOptions);
        return json_decode(curl_exec($ch), true);
    }
    
    /**
     * 
     * @param string $warehouseName
     * @param string $warehouseAddress
     * @return string
     */
    protected function getCityName($warehouseName, $warehouseAddress) {
        $excludedWords = [
            'Центральный',
            'Промышленная',
            'Кировский',
        ];
        $resultParts = [];
        $cityParts = explode(' ', str_replace(range(0,9), '', $warehouseName));
        foreach ($cityParts as $cityPart) {
            if (strlen($cityPart) > 0 && stripos(strtolower($warehouseAddress), strtolower($cityPart)) !== false) {
                $resultParts[] = $cityPart;
            }
        }
        if (count($resultParts) !== 0) {
            return trim(str_replace($excludedWords, [], (implode(' ', $resultParts))));
        }
        $cityName = preg_match('/г\.([\w\s]+)/u', $warehouseAddress, $matches) ? $matches[1] : null;
        if (!is_null($cityName)) {
            return trim(str_replace($excludedWords, [], $cityName));
        }
        return trim(str_replace($excludedWords, [], $warehouseName));
    }

}
