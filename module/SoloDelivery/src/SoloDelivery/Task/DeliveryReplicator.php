<?php

namespace SoloDelivery\Task;

use SoloDelivery\Data\LiftingServicePricesTable;
use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\Service\ProvidesWebservice;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloReplication\Service\DataMapper;
use SoloDelivery\Data\DeliveryTimeRangesTable;
use SoloDelivery\Data\DeliveryUnavailDatesTable;
use SoloDelivery\Data\GoodReserveAvailDateTable;
use SoloDelivery\Data\HolidaysTable;
use Solo\Db\QueryGateway\QueryGateway;
use Solo\DateTime\DateTime;

class DeliveryReplicator extends AbstractReplicationTask {
	
	use ProvidesWebservice;

	/**
	 *
	 * @var DeliveryTimeRangesTable
	 */
	private $deliveryTimeRangesTable;

	/**
	 *
	 * @var DeliveryUnavailDatesTable
	 */
	private $deliveryUnavailDatesTable;

	/**
	 *
	 * @var GoodReserveAvailDateTable
	 */
	private $goodReserveAvailDateTable;

	/**
	 *
	 * @var HolidaysTable
	 */
	private $holidaysTable;

	/**
	 *
	 * @var LiftingServicePricesTable
	 */
	private $liftingServicePricesTable;

	/**
	 *
	 * @param DeliveryTimeRangesTable $deliveryTimeRangesTable        	
	 * @param DeliveryUnavailDatesTable $deliveryUnavailDatesTable        	
	 * @param GoodReserveAvailDateTable $goodReserveAvailDateTable        	
	 * @param HolidaysTable $holidaysTable        	
	 */
	public function __construct(DeliveryTimeRangesTable $deliveryTimeRangesTable, DeliveryUnavailDatesTable $deliveryUnavailDatesTable, GoodReserveAvailDateTable $goodReserveAvailDateTable, HolidaysTable $holidaysTable, LiftingServicePricesTable $liftingServicePricesTable) {
		$this->deliveryTimeRangesTable = $deliveryTimeRangesTable;
		$this->deliveryUnavailDatesTable = $deliveryUnavailDatesTable;
		$this->goodReserveAvailDateTable = $goodReserveAvailDateTable;
		$this->holidaysTable = $holidaysTable;
		$this->liftingServicePricesTable = $liftingServicePricesTable;
		
		$this->addSwapTable('delivery_time_ranges', 'delivery_unavail_dates', 'good_reserve_avail_date', 'holidays', 'lifting_service_prices');
		
		$this->scriptNotRespondingTimeInMinutes = 60 * 2;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \SoloReplication\Service\TaskInterface::process()
	 */
	public function process() {
		$this->processDeliveryTimeRanges();
		// $this->processDeliveryUnavailDates();
		$this->processGoodReserveAvailDate();
		// $this->processHolidays();
		// $this->logDistributionByDays();
		$this->processLiftingServicePrices();
	}

	/**
	 *
	 * @return \SoloReplication\Service\DataMapper
	 */
	protected function createDeliveryTimeRangesMapper() {
		$context = $this;
		$mapper = new DataMapper();
		$mapper->setMappings([
			'TimeRangeID' => '%d: Id',
			'TimeRangeName' => 'Name' 
		]);
		$mapper->setMapping('TimeTo', 'TimeTo', function ($field, $row) use($context) {
			return $context->helper()->parseTime($field);
		});
		$mapper->setMapping('TimeFrom', 'TimeFrom', function ($field, $row) use($context) {
			return $context->helper()->parseTime($field);
		});
		$mapper->setMapping('DateTo', 'DateTimeTo', function ($field, $row) use($context) {
			return $context->helper()->parseJSONDate($field);
		});
		$mapper->setMapping('DateFrom', 'DateTimeFrom', function ($field, $row) use($context) {
			return $context->helper()->parseJSONDate($field);
		});
		return $mapper;
	}

	/**
	 */
	protected function processDeliveryTimeRanges() {
		$this->log->info('Insert time ranges');
		$mapper = $this->createDeliveryTimeRangesMapper();
		$dataSet = [];
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetDeliveryTimeRanges'));
		if (!$rdr->isEmpty()) {
			$this->deliveryTimeRangesTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('delivery_time_ranges');
			$this->notify()->send($this->getNotifyMessage(__CLASS__, __METHOD__, 'GetDeliveryTimeRanges', []), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->deliveryTimeRangesTable->insertSet($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->deliveryTimeRangesTable->insertSet($dataSet);
		}
		
		$this->log->info('Inserted time ranges = ' . sizeof($dataSet));
	}

	/**
	 *
	 * @return \SoloReplication\Service\DataMapper
	 */
	protected function createDeliveryUnavailDatesMapper() {
		$context = $this;
		$mapper = new DataMapper();
		$mapper->setMapping('Day', 'Day', function ($field, $row) use($context) {
			return $context->helper()->parseJSONDate($field);
		});
		return $mapper;
	}

	/**
	 */
	protected function processDeliveryUnavailDates() {
		$this->log->info('Insert delivery unavail dates');
		$mapper = $this->createDeliveryUnavailDatesMapper();
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetNearestCalendar'));
		$this->deliveryUnavailDatesTable->truncate();
		$dataSet = [];
		$count = 0;
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->deliveryUnavailDatesTable->insertSet($dataSet);
				$count += sizeof($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->deliveryUnavailDatesTable->insertSet($dataSet);
			$count += sizeof($dataSet);
		}
		$this->log->info('Inserted delivery unavail dates = ' . $count);
	}

	/**
	 *
	 * @return \SoloReplication\Service\DataMapper
	 */
	protected function createGoodReserveAvailDateMapper() {
		$context = $this;
		$mapper = new DataMapper();
		$mapper->setMappings([
			'GoodID' => '%d: ArticleId',
			'CityID' => '%d: LocationId' 
		]);
		$mapper->setMapping(
			'AvailDeliveryDate', 
			'DeliveryDate', 
			function ($field, $row) use($context) {
				$result = null;
				if (!empty($field)) {
					$result = $context->helper()->parseJSONDate($field);
				}
				return $result;
			});
		$mapper->setMapping(
			'PickupDate', 
			'RetrieveDate', 
			function ($field, $row) use($context) {
				$result = null;
				if (!empty($field)) {
					$result = $context->helper()->parseJSONDate($field);
				}
				return $result;
			});
		$mapper->setMapping(
			'SuborderDeliveryDate', 
			'DeliveryDate', 
			function ($field, $row) use($context) {
				$result = null;
				if (!empty($field)) {
					$result = $context->helper()->parseJSONDate($field);
				}
				return $result;
			});
		$mapper->setMapping(
			'DeliveryDate ', 
			'DeliveryDate ', 
			function ($field, $row) use($context) {
				$result = null;
				if (!empty($field)) {
					$result = $context->helper()->parseJSONDate($field);
				}
				return $result;
			});
		$mapper->setMapping(
			'ShowcaseRetrieveDate', 
			'ShowcaseRetrieveDate', 
			function ($field, $row) use($context) {
				$result = null;
				if (!empty($field)) {
					$result = $context->helper()->parseJSONDate($field);
				}
				return $result;
			});
		return $mapper;
	}

	/**
	 */
	protected function processGoodReserveAvailDate() {
		// $str = '/Date(1538546400000+0300)/';
		// $date = $this->helper()->parseJSONDate($str);
		// $date = DateTime::createFromTimestamp(DateTime::createFromFormat('Y-m-d H:i:s', $date)->getTimestamp());
		// var_dump($date->getYear());
		// var_dump(DateTime::now()->getYear());
		// print_r($date); exit();
		$this->log->info('Insert good reserve avail dates');
		$mapper = $this->createGoodReserveAvailDateMapper();
		$dataSet = [];
		$count = 0;
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetGoodsRetrieveDays'));
		if (!$rdr->isEmpty()) {
			$this->goodReserveAvailDateTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('good_reserve_avail_date');
			$this->notify()->send($this->getNotifyMessage(__CLASS__, __METHOD__, 'GetGoodsRetrieveDays', []), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		foreach ($rdr as $row) {
			if (136 == $row['ArticleId']) {
				print_r($row);
			}
			$row = $mapper->convert($row);
			
// 			$availDate = DateTime::createFromTimestamp(DateTime::createFromFormat('Y-m-d H:i:s', $row['AvailDeliveryDate'])->getTimestamp());
// 			$suborderDate = DateTime::createFromTimestamp(DateTime::createFromFormat('Y-m-d H:i:s', $row['SuborderDeliveryDate'])->getTimestamp());
// 			if (($availDate instanceof DateTime) && ($suborderDate instanceof DateTime)) {
// 				$diff = $availDate->diff($suborderDate, true);
// 				if (0 < $diff->y) {
// 					$row['AvailDeliveryDate'] = $suborderDate->format('Y-m-d H:i:s');
// 				}
// 			}
			
			$dataSet[] = $row;
			if (500 == sizeof($dataSet)) {
				$this->goodReserveAvailDateTable->insertSet($dataSet, true);
				$count += sizeof($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->goodReserveAvailDateTable->insertSet($dataSet, true);
			$count += sizeof($dataSet);
		}
		
		$this->log->info('Inserted good reserve avail dates = ' . $count);
		
		// @todo temporary
		if (true) {
			$now = date('Y-m-d H:i:s', (time() + (60 * 60 * 24 * 1)));
			$queryGateway = new QueryGateway();
			$sql = "INSERT INTO #good_reserve_avail_date:passive#
	        		SELECT p.* FROM (
	        			SELECT
	        				ag.GoodID,
	        				2 AS CityID,
	        				'{$now}' AS PickupDate,
				        	'{$now}' AS AvailDeliveryDate,
				        	'{$now}' AS SuborderDeliveryDate
	        			FROM #all_goods:active# ag
	        			WHERE ag.GoodID NOT IN (SELECT grad.GoodID FROM #good_reserve_avail_date:passive# grad)
					) p";
			// $queryGateway->query($sql);
			
// 			$sql = "DELETE FROM #good_reserve_avail_date:passive# WHERE GoodID NOT IN (SELECT GoodID FROM #arrival_goods#)";
// 			$queryGateway->query($sql);
		}
	}

	/**
	 *
	 * @return \SoloReplication\Service\DataMapper
	 */
	protected function createHolidaysMapper() {
		$context = $this;
		$mapper = new DataMapper();
		$mapper->setMapping('HolidayDate', 'Day', function ($field, $row) use($context) {
			return $context->helper()->parseJSONDate($field);
		});
		return $mapper;
	}

	/**
	 */
	protected function processHolidays() {
		$this->log->info('Insert holidays');
		$mapper = $this->createHolidaysMapper();
		$dataSet = [];
		$count = 0;
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetNearestCalendarYandex'));
		$this->holidaysTable->truncate();
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->holidaysTable->insertSet($dataSet, true);
				$count += sizeof($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->holidaysTable->insertSet($dataSet, true);
			$count += sizeof($dataSet);
		}
		
		$this->log->info('Holiday dates = ' . $count);
	}

	/**
	 *
	 * @return \SoloReplication\Service\DataMapper
	 */
	protected function createLiftingServicePricesMapper() {
		$mapper = new DataMapper();
		$mapper->setMappings([
			'Price' => '%f: Cost',
			'ElevatorTypeID' => '%d: ElevatorTypeId' 
		]);
		
		return $mapper;
	}

	/**
	 */
	protected function processLiftingServicePrices() {
		$this->log->info('Insert lifting service prices');
		$mapper = $this->createLiftingServicePricesMapper();
		$dataSet = [];
		$count = 0;
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetLiftingCost'));
		if (!$rdr->isEmpty()) {
			$this->liftingServicePricesTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('lifting_service_prices');
			$this->notify()->send($this->getNotifyMessage(__CLASS__, __METHOD__, 'GetLiftingCost', []), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		foreach ($rdr->getValue('LiftingCost') as $row) {
			$dataSet[] = $mapper->convert((array)$row);
			if (500 == sizeof($dataSet)) {
				$this->liftingServicePricesTable->insertSet($dataSet, true);
				$count += sizeof($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->liftingServicePricesTable->insertSet($dataSet, true);
			$count += sizeof($dataSet);
		}
		
		$this->log->info('Lifting service prices inserted = ' . $count);
	}

	/**
	 * log delivery and pick up distribution by days
	 */
	protected function logDistributionByDays() {
		// пишем в лог инфу о количестве
		$loggedInfo = $this->goodReserveAvailDateTable->getDistributionByDays();
		$loggedStringTable = PHP_EOL;
		foreach ($loggedInfo as $loggedRecord) {
			$loggedStringTable .= $loggedRecord['type'] . str_repeat("\t", $loggedRecord['tabCount']) . $loggedRecord['daysInterval'] . "\t" . $loggedRecord['goodsQuantity'] . PHP_EOL;
		}
		$this->log->info($loggedStringTable);
	}

}
