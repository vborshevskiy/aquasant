<?php

namespace SoloDelivery\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\Service\ProvidesWebservice;
use SoloDelivery\Data\LogisticCompaniesTable;
use SoloDelivery\Data\LogisticCompaniesShippingScheduleTable;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloReplication\Service\DataMapper;

class LogisticCompaniesReplicator extends AbstractReplicationTask {

    use ProvidesWebservice;

    /**
     *
     * @var LogisticCompaniesTable
     */
    private $logisticCompaniesTable;
    
    /**
     *
     * @var LogisticCompaniesShippingScheduleTable
     */
    private $logisticCompaniesShippingScheduleTable;

    /**
     *
     * @param LogisticCompaniesTable LogisticCompaniesTable   
     * @param LogisticCompaniesShippingScheduleTable $logisticCompaniesShippingScheduleTable
     */
    public function __construct(LogisticCompaniesTable $logisticCompaniesTable, LogisticCompaniesShippingScheduleTable $logisticCompaniesShippingScheduleTable) {
        $this->logisticCompaniesTable = $logisticCompaniesTable;
        $this->logisticCompaniesShippingScheduleTable = $logisticCompaniesShippingScheduleTable;

        $this->addSwapTable('logistic_companies', 'logistic_companies_shipping_schedule');
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SoloReplication\Service\TaskInterface::process()
     */
    public function process() {
        $this->processLogisticCompanies();
        $this->processLogisticCompaniesShippingSchedule();
    }

    /**
     *
     * @return \SoloReplication\Service\DataMapper
     */
    protected function createLogisticCompaniesMapper() {
        $mapper = new DataMapper();
        $mapper->setMappings([
            'CompanyID' => '%d: Id',
            'CompanyName' => 'Name',
        ]);

        return $mapper;
    }

    /**
     */
    protected function processLogisticCompanies() {
        $this->log->info('Insert logistic companies');
        $mapper = $this->createLogisticCompaniesMapper();
        $dataSet = [];
        $rdr = new UltimaJsonListReader($this->callWebMethod('GetLogisticCompanies'));
        if (!$rdr->isEmpty()) {
            $this->logisticCompaniesTable->truncate();
            $this->notify()->fileWarning(false);
        } else {
            $this->removeSwapTable('logistic_companies');
            $this->notify()->send($this->getNotifyMessage(__CLASS__,__METHOD__,'GetLogisticCompanies',[]), 'Пустой ответ по методу репликации');
            $this->notify()->fileWarning(true);
        }
        foreach ($rdr as $row) {
            $dataSet[] = $mapper->convert($row);
            if (500 == sizeof($dataSet)) {
                $this->logisticCompaniesTable->insertSet($dataSet);
                $dataSet = [];
            }
        }
        if (0 < sizeof($dataSet)) {
            $this->logisticCompaniesTable->insertSet($dataSet);
        }

        $this->log->info('Inserted logistic companies = ' . sizeof($dataSet));
    }
    
    /**
     *
     * @return \SoloReplication\Service\DataMapper
     */
    protected function processLogisticCompaniesShippingScheduleMapper() {
        $mapper = new DataMapper();
        $mapper->setMappings([
            'LogisticCompanyID' => '%d: LogisticCompanyId',
            'CityID' => '%d: LocationId',
            'WeekDayID' => '%d: WeekDayId',
            'DaysInterval' => '%d: DaysGap',
            'ArrivalMinute' => '%d: ArrivalMinute',
            'ArrivalHour' => '%d: ArrivalHour',
            'ShippingHour' => '%d: ShippingHour',
            'ShippingMinute' => '%d: ShippingMinute',
        ]);

        return $mapper;
    }

    /**
     */
    protected function processLogisticCompaniesShippingSchedule() {
        $this->log->info('Insert shipping schedule');
        $mapper = $this->processLogisticCompaniesShippingScheduleMapper();
        $dataSet = [];
        $rdr = new UltimaJsonListReader($this->callWebMethod('GetTKShippingGraph'));
        if (!$rdr->isEmpty()) {
            $this->logisticCompaniesShippingScheduleTable->truncate();
            $this->notify()->fileWarning(false);
        } else {
            $this->removeSwapTable('logistic_companies_shipping_schedule');
            $this->notify()->send($this->getNotifyMessage(__CLASS__,__METHOD__,'GetTKShippingGraph',[]), 'Пустой ответ по методу репликации');
            $this->notify()->fileWarning(true);
        }
        foreach ($rdr as $row) {
            $dataSet[] = $mapper->convert($row);
            if (500 == sizeof($dataSet)) {
                $this->logisticCompaniesShippingScheduleTable->insertSet($dataSet);
                $dataSet = [];
            }
        }
        if (0 < sizeof($dataSet)) {
            $this->logisticCompaniesShippingScheduleTable->insertSet($dataSet);
        }

        $this->log->info('Inserted shipping schedule = ' . sizeof($dataSet));
    }

}
