<?php

namespace SoloDelivery\Data;

use Solo\Db\Mapper\AbstractMapper;

class PostDeliveryTermsMapper extends AbstractMapper implements PostDeliveryTermsMapperInterface {
    
    /**
     * 
     * @param integer $tkPekRegionId
     * @return array|null
     */
    public function getTerms($tkPekRegionId) {
        $sql = '
            SELECT
                *
            FROM
                tk_pek_delivery_terms_from_moscow dt
            WHERE
                dt.TkPekRegionID = ' . $tkPekRegionId . '
            LIMIT
                1
                ';
        $result = $this->query($sql);
        if ($result->count() > 0) {
            return $result->current();
        }
        return null;
    }
    
}
