<?php

namespace SoloDelivery\Data;

use Solo\Db\Mapper\AbstractMapper;

class PostDeliveryMapper extends AbstractMapper implements PostDeliveryMapperInterface {
    
    /**
     * 
     * @param integer $regionId
     * @return string
     */
    public function getRegionById($regionId) {
        $sql = 'SELECT
                        pdr.*,
                        CASE
                            WHEN pdr.TkEmsRegionID IS NOT NULL
                            THEN pdr.TkEmsRegionID
                            ELSE (
                                SELECT TkEmsRegionID FROM post_delivery_regions WHERE RegionID = pdr.ParentRegionID
                            )
                        END AS TkEmsRegionID,
                        CASE
                            WHEN pdr.Zoom IS NOT NULL
                            THEN pdr.Zoom
                            ELSE (
                                SELECT Zoom FROM post_delivery_regions WHERE RegionID = pdr.ParentRegionID
                            )
                        END AS Zoom,
                        CASE
                            WHEN pdr.Latitude IS NOT NULL
                            THEN pdr.Latitude
                            ELSE (
                                SELECT Latitude FROM post_delivery_regions WHERE RegionID = pdr.ParentRegionID
                            )
                        END AS Latitude,
                        CASE
                            WHEN pdr.Longitude IS NOT NULL
                            THEN pdr.Longitude
                            ELSE (
                                SELECT Longitude FROM post_delivery_regions WHERE RegionID = pdr.ParentRegionID
                            )
                        END AS Longitude,
                        CASE
                            WHEN pdr.DeliveryDays IS NOT NULL
                            THEN pdr.DeliveryDays
                            ELSE (
                                SELECT DeliveryDays FROM post_delivery_regions WHERE RegionID = pdr.ParentRegionID
                            )
                        END AS DeliveryDays,
                        CASE
                            WHEN pdr.Shipped IS NOT NULL
                            THEN pdr.Shipped
                            ELSE (
                                SELECT Shipped FROM post_delivery_regions WHERE RegionID = pdr.ParentRegionID
                            )
                        END AS Shipped,
                        CASE
                            WHEN pdr.Phone IS NULL
                            THEN c.CityPhone
                            ELSE pdr.Phone
                        END AS CityPhone
                FROM
                        post_delivery_regions pdr
                INNER JOIN
                        #cities:active# c
                        ON c.CityID = pdr.ErpCityID
                WHERE
                        pdr.RegionID = ' . $regionId . '
                LIMIT
                        1
                ';
        $resultSet = $this->query($sql);
        if ($resultSet->count() > 0) {
            return $resultSet->current();
        }
        return null;
    }
    
    /**
     * 
     * @param string $regionName
     * @return integer | null
     */
    public function getRegionIdByName($regionName) {
        $sql = 'SELECT
                        pdr.RegionID
                FROM
                        post_delivery_regions pdr
                WHERE
                        pdr.RegionName = "' . $regionName . '"
                LIMIT
                        1
                ';
        $resultSet = $this->query($sql);
        if ($resultSet->count() > 0) {
            return $resultSet->current();
        }
        return null;
    }
    
    /**
     * 
     * @param integer $regionId
     * @param string $query
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getRegionsByParentId($regionId, $query = null) {
        $sql = 'SELECT
                        pdr.RegionID,
                        pdr.RegionName
                FROM
                        post_delivery_regions pdr
                WHERE
                        pdr.ParentRegionID = ' . $regionId . '
                ';
        if (strlen($query) > 0) {
            $sql .= '
                        AND pdr.RegionName LIKE "%' . $query . '%"
                    ';
        }
        return $this->query($sql);
    }
    
    /**
     * 
     * @param string $query
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function searchRegions($query) {
        $sql = 'SELECT
                        pdr.RegionID,
                        pdr.RegionName
                FROM
                        post_delivery_regions pdr
                WHERE
                        pdr.RegionName LIKE "%' . $query . '%"
                LIMIT
                        50
                ';
        return $this->query($sql);
    }

    /**
     *
     * @param string $query
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function searchCities($query) {
        $sql = 'SELECT
                        pdr.RegionID,
                        pdr.RegionName
                FROM
                        post_delivery_regions pdr
                WHERE
                        pdr.RegionName LIKE "%' . $query . '%" AND
                        pdr.Dropdown = 0
                LIMIT
                        50
                ';
        return $this->query($sql);
    }
    
    /**
     * 
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getAllRootRegions() {
        $sql = 'SELECT
                        *
                FROM
                        post_delivery_regions pdr
                WHERE
                        pdr.ParentRegionID IS NULL
                ';
        return $this->query($sql);
    }
    
    /**
     * 
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getRootRegions() {
        $sql = 'SELECT
                    *
                    FROM
                            (SELECT
                                    pdr.RegionID,
                                    pdr.RegionName,
                                    pdr.EndGroup
                            FROM
                                    post_delivery_regions pdr
                            WHERE
                                    pdr.ParentRegionID IS NULL
                                    AND pdr.Sort < 0
                            ORDER BY
                                    pdr.Sort
                            ) AS pdr_1
                UNION ALL
                SELECT
                    *
                    FROM
                            (
                            SELECT
                                    pdr.RegionID,
                                    pdr.RegionName,
                                    0 AS EndGroup
                            FROM
                                    post_delivery_regions pdr
                            WHERE
                                    pdr.ParentRegionID IS NULL
                            ORDER BY
                                    pdr.RegionName
                            ) AS pdr_2
                ';
        return $this->query($sql);
    }
    
    /**
     * 
     * @param integer $regionId
     * @return boolean
     */
    public function isDropdownRegion($regionId) {
        $sql = 'SELECT
                        pdr.Dropdown
                FROM
                        post_delivery_regions pdr
                WHERE
                        pdr.RegionID = ' . $regionId . '
                LIMIT
                        1
                ';
        $resultSet = $this->query($sql);
        if ($resultSet->count() > 0) {
            return (bool)$resultSet->current()->Dropdown;
        }
        return false;
    }
    
    /**
     * 
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getShippedAddingRegions() {
        $sql = 'SELECT
                        pdr.RegionID,
                        pdr.UpdateShippedChance,
                        pdr.MaxShippedAdding
                FROM
                        post_delivery_regions pdr
                WHERE
                        pdr.UpdateShippedChance > 0';
        return $this->query($sql);
    }
    
    /**
     * 
     * @param array $data
     * @return integer
     */
    public function addShipped(array $data) {
        $sql = '';
        foreach ($data as $regionId => $addingShipped) {
            $sql .= '
                    UPDATE
                            post_delivery_regions pdr
                    SET
                            pdr.Shipped = pdr.Shipped + ' . $addingShipped . '
                    WHERE
                            RegionID = ' . $regionId . ';
                    ';
        }
        return $this->query($sql);
    }
    
    /**
     * 
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getRootShippedSummary() {
        $sql = 'SELECT
                        pdr.ParentRegionID,
                        SUM(pdr.Shipped) AS ShippedSum
                FROM
                        post_delivery_regions pdr
                WHERE
                        pdr.ParentRegionID IS NOT NULL
                GROUP BY
                        pdr.ParentRegionID';
        return $this->query($sql);
    }
    
    /**
     * 
     * @param array $data
     * @return integer
     */
    public function updateDeliveryDays(array $data) {
        $sql = '';
        foreach ($data as $regionId => $deliveryDays) {
            $sql .= '
                    UPDATE
                            post_delivery_regions pdr
                    SET
                            pdr.DeliveryDays = ' . $deliveryDays . '
                    WHERE
                            RegionID = ' . $regionId . ';
                    ';
        }
        return $this->query($sql);
    }
    
}
