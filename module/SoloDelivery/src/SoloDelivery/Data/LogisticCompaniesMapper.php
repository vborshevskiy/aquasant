<?php

namespace SoloDelivery\Data;

use Solo\Db\Mapper\AbstractMapper;

class LogisticCompaniesMapper extends AbstractMapper implements LogisticCompaniesMapperInterface {
    
    /**
     * 
     * (non-PHPdoc)
     * @see \SoloDelivery\Data\LogisticCompaniesMapperInterface::getLogisticCompanyById()
     */
    public function getLogisticCompanyById($logisticCompanyId) {
        $sql = '
            SELECT
                    lc.*
                FROM
                    #logistic_companies:active# lc
                WHERE
                    lc.CompanyID = ' . $logisticCompanyId . '
                ';
        $resultSet = $this->query($sql);
        if ($resultSet->count() > 0) {
            return $resultSet->current();
        }
        return [];
    }
    
    /**
     * 
     * (non-PHPdoc)
     * @see \SoloDelivery\Data\LogisticCompaniesMapperInterface::getScheduleRecords()
     */
    public function getScheduleRecords($cityId) {
        $sql = '
            SELECT
                    lcss.*
                FROM
                    #logistic_companies:active# lc
                INNER JOIN
                    #logistic_companies_shipping_schedule# lcss
                    ON lcss.LogisticCompanyID = lc.CompanyID
                WHERE
                    lcss.CityID = ' . $cityId . '
                ';
        return $this->query($sql);
    }
    
}
