<?php

namespace SoloDelivery\Data;

use Solo\Db\Mapper\AbstractMapper;
use SoloCache\Service\ProvidesCache;

class TkPekMapper extends AbstractMapper implements TkPekMapperInterface {
    
    use ProvidesCache;
    
    /**
     * 
     * @return array
     */
    public function getPekRegions() {
        $result = [];
        $sql = '
            SELECT
                    pdr.RegionID,
                    pdr.TkPekRegionID
                FROM
                    post_delivery_regions pdr
        ';
        foreach ($this->query($sql)->toArray() as $record) {
            $result[(int)$record['TkPekRegionID']] = (int)$record['RegionID'];
        }
        return $result;
    }
    
    /**
     * 
     * @return array
     */
    public function getPekWarehouses() {
        $cacher = $this->createMethodCacher(__METHOD__, func_get_args());
        if ($cacher->has()) {
            return $cacher->get();
        }
        $sql = '
            SELECT
                    w.*
                FROM
                    #tk_pek_warehouses:active# w
                WHERE
                    w.isAcceptanceOnly = 0
        ';
        $rows = $this->query($sql)->toArray();
        $cacher->set($rows);
        return $rows;
    }
    
    /**
     * 
     * @param integer $regionId
     * @return array
     */
    public function getPekWarehousesByRegionId($regionId) {
        $cacher = $this->createMethodCacher(__METHOD__, func_get_args());
        if ($cacher->has()) {
            return $cacher->get();
        }
        $sql = '
            SELECT
                    w.*
                FROM
                    #tk_pek_warehouses:active# w
                INNER JOIN
                    #tk_pek_warehouses_link_regions:active# wr
                    ON wr.warehouseID = w.warehouseID
                WHERE
                    w.isAcceptanceOnly = 0
                    AND wr.RegionID = ' . $regionId . '
        ';
        $rows = $this->query($sql)->toArray();
        $cacher->set($rows);
        return $rows;
    }
    
    /**
     * 
     * @return integer
     */
    public function getPekWarehousesCount() {
        $sql = '
            SELECT
                    COUNT(*) WarehousesCount
                FROM
                    #tk_pek_warehouses:active# w
                WHERE
                    w.isAcceptanceOnly = 0
        ';
        $rows = $this->query($sql);
        if ($rows->count() === 1) {
            return (int)$rows->current()->WarehousesCount;
        }
        return 0;
    }
    
    /**
     * 
     * @param integer $pekWarehouseId
     * @return array
     */
    public function getWarehouseById($pekWarehouseId) {
        $sql = '
            SELECT
                    w.*
                FROM
                    #tk_pek_warehouses:active# w
                WHERE
                    w.isAcceptanceOnly = 0
                    AND w.warehouseId = ' . $pekWarehouseId . '
                LIMIT 1
        ';
        $rows = $this->query($sql);
        if ($rows->count() > 0) {
            return $rows->current();
        }
        return null;
    }
    
}
