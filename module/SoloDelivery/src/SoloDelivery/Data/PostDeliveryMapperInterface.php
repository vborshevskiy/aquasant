<?php

namespace SoloDelivery\Data;

interface PostDeliveryMapperInterface {
    
    /**
     * 
     * @param integer $regionId
     * @return string
     */
    public function getRegionById($regionId);
    
    /**
     * 
     * @param string $regionName
     * @return integer | null
     */
    public function getRegionIdByName($regionName);
    
    /**
     * 
     * @param integer $regionId
     * @param string $query
     * @return array
     */
    public function getRegionsByParentId($regionId, $query = null);
    
    /**
     * 
     * @param string $query
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function searchRegions($query);

    /**
     *
     * @param string $query
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function searchCities($query);
    
    /**
     * 
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getRootRegions();
    
    /**
     * 
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getAllRootRegions();
    
    /**
     * 
     * @param integer $regionId
     * @return boolean
     */
    public function isDropdownRegion($regionId);
    
    /**
     * 
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getShippedAddingRegions();
    
    /**
     * 
     * @param array $data
     * @return integer
     */
    public function addShipped(array $data);
    
    /**
     * 
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getRootShippedSummary();
    
    /**
     * 
     * @param array $data
     * @return integer
     */
    public function updateDeliveryDays(array $data);
    
}
