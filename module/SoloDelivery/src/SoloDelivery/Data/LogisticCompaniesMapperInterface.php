<?php

namespace SoloDelivery\Data;

use Zend\Db\ResultSet\ResultSet;

interface LogisticCompaniesMapperInterface {
    
    /**
     * 
     * @param integer $logisticCompanyId
     * @return ResultSet
     */
    public function getLogisticCompanyById($logisticCompanyId);
    
    /**
     * 
     * @param integer $cityId
     * @return ResultSet
     */
    public function getScheduleRecords($cityId);
    
}
