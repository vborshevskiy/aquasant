<?php

namespace SoloDelivery\Data;

use Zend\Db\ResultSet\ResultSet;

interface DeliveryMapperInterface {

	/**
	 *
	 * @param integer $goodId        	
	 * @param integer $cityId        	
	 * @return string null
	 */
	public function getSingleGoodDeliveryDate($goodId, $cityId);

	/**
	 *
	 * @param integer $cityId        	
	 * @param array $goodIds        	
	 * @return \Zend\Db\ResultSet\ResultSet
	 */
	public function getAvailDeliveryDateByGoodIds($cityId, $goodIds);

	/**
	 *
	 * @param integer $cityId        	
	 * @param array $goodIds        	
	 * @return \Zend\Db\ResultSet\ResultSet
	 */
	public function getSuborderDeliveryDateByGoodIds($cityId, $goodIds);

	/**
	 *
	 * @return \Zend\Db\ResultSet\ResultSet
	 */
	public function getDeliveryTimeRange();

	/**
	 *
	 * @return \Zend\Db\ResultSet\ResultSet
	 */
	public function getUnavailDays();

	/**
	 *
	 * @param integer $cityId        	
	 * @return ResultSet
	 */
	public function getPickupDeliveriesByCityId($cityId);

}

?>