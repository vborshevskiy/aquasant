<?php

namespace SoloDelivery\Data;

use Zend\Db\ResultSet\ResultSet;
use Solo\Db\TableGateway\TableInterface;

interface DeliveryTimeRangesTableInterface extends TableInterface {

    /**
     * 
     * @return ResultSet
     */
    public function getDeliveryTimeRanges();
}

?>