<?php

namespace SoloDelivery\Data;

use Solo\Db\TableGateway\AbstractTable;

class GoodReserveAvailDateTable extends AbstractTable implements GoodReserveAvailDateTableInterface {
    
    /**
     * 
     * @return Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getDistributionByDays() {
        $sql = "
                        SELECT
                            'PickupDate|RetreiveDate' AS type,
                            4 AS tabCount,
                            TO_DAYS(grad.PickupDate) - TO_DAYS(NOW()) daysInterval,
                            COUNT(*) goodsQuantity
                        FROM
                            good_reserve_avail_date_1 grad
                        WHERE
                            grad.PickupDate > NOW()
                        GROUP BY
                            TO_DAYS(grad.PickupDate) - TO_DAYS(NOW())

                    UNION

                        SELECT
                            'AvailDeliveryDate|MinDeliveryDate' AS type,
                            2 AS tabCount,
                            TO_DAYS(grad.AvailDeliveryDate) - TO_DAYS(NOW()) daysInterval,
                            COUNT(*) goodsQuantity
                        FROM
                            good_reserve_avail_date_1 grad
                        WHERE
                            grad.AvailDeliveryDate > NOW()
                        GROUP BY
                            TO_DAYS(grad.AvailDeliveryDate) - TO_DAYS(NOW())

                    UNION

                        SELECT
                            'SuborderDeliveryDate|SuborderDeliveryDate' AS type,
                            1 AS tabCount,
                            TO_DAYS(grad.SuborderDeliveryDate) - TO_DAYS(NOW()) daysInterval,
                            COUNT(*) goodsQuantity
                        FROM
                            good_reserve_avail_date_1 grad
                        WHERE
                            grad.SuborderDeliveryDate > NOW()
                        GROUP BY
                            TO_DAYS(grad.SuborderDeliveryDate) - TO_DAYS(NOW())
               ";
        return $this->query($sql);
    }
    
}
