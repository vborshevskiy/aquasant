<?php

namespace SoloDelivery\Data;

use Solo\Db\Mapper\AbstractMapper;

class DeliveryMapper extends AbstractMapper implements DeliveryMapperInterface {

    /**
     *
     * @param integer $goodId
     * @param integer $cityId
     * @return string|null
     */
    public function getSingleGoodDeliveryDate($goodId, $cityId) {
        $sql = 'SELECT
                        grad.AvailDeliveryDate AS DeliveryDate
                    FROM
                        #good_reserve_avail_date:active# grad
                    WHERE
                        grad.CityID = '.$cityId.'
                        AND grad.GoodID = '.$goodId.'
                        AND grad.AvailDeliveryDate > NOW()

                UNION

                SELECT
                        grad.SuborderDeliveryDate AS DeliveryDate
                    FROM
                        #good_reserve_avail_date:active# grad
                    WHERE
                        grad.CityID = '.$cityId.'
                        AND grad.GoodID = '.$goodId.'
                    AND grad.SuborderDeliveryDate > NOW()

                ORDER BY
                        DeliveryDate ASC
                LIMIT 1';
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            return $rows->current()->DeliveryDate;
        }
        return null;
    }

    /**
     *
     * @param integer $cityId
     * @param array $goodIds
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getAvailDeliveryDateByGoodIds($cityId,$goodIds) {
        if (!is_array($goodIds) || count($goodIds) == 0) {
            return null;
        }
        $sql = 'SELECT
                        grad.GoodID,
                        grad.AvailDeliveryDate,
						grad.DeliveryDate,
						grad.ShowcaseRetrieveDate
                    FROM
                        #good_reserve_avail_date:active# grad
                    WHERE
                        grad.CityID = '.$cityId.'
                        AND grad.GoodID IN ('.implode(',', $goodIds).')
                        AND (
							grad.DeliveryDate > NOW()
							OR grad.DeliveryDate IS NULL
						)';
        return $this->query($sql);
    }

    /**
     *
     * @param integer $cityId
     * @param array $goodIds
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getSuborderDeliveryDateByGoodIds($cityId,$goodIds) {
        if (!is_array($goodIds) || count($goodIds) == 0) {
            return null;
        }
        $sql = 'SELECT
                        grad.GoodID,
                        grad.AvailDeliveryDate as SuborderDeliveryDate,
						grad.DeliveryDate,
        				grad.ShowcaseRetrieveDate
                    FROM
                        #good_reserve_avail_date:active# grad
                    WHERE
                        grad.CityID = '.$cityId.'
                        AND grad.GoodID IN ('.implode(',', $goodIds).')
                        AND (
							grad.DeliveryDate > NOW()
							OR grad.DeliveryDate IS NULL
						)';
        return $this->query($sql);
    }

    /**
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getDeliveryTimeRange() {
        $sql = 'SELECT
                        dtr.*
                    FROM
                        #delivery_time_ranges:active# dtr';
        return $this->query($sql);
    }

    /**
     *
     * @return integer | null
     */
    public function getAnyDeliveryTimeRangeId() {
        $sql = 'SELECT
                        dtr.TimeRangeID
                    FROM
                        #delivery_time_ranges:active# dtr';
        $resultSet = $this->query($sql);
        if ($resultSet->count() > 0) {
            return (int)$resultSet->current()->TimeRangeID;
        }
        return null;
    }

    /**
     *
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getUnavailDays() {
        $sql = 'SELECT
                        dud.*
                    FROM
                        #delivery_unavail_dates:active# dud
                    WHERE
                        dud.Day > NOW()';
        return $this->query($sql);
    }

    /**
     *
     * @param integer $cityId
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getAvailDayNumbers($cityId) {
        $sql = 'SELECT
                        ws.WeekdayNumber
                    FROM
                        #work_schedule:active# ws
                    WHERE
                        ws.WeekdayNumber BETWEEN 1 AND 7
                        AND ws.Works > 0
                        AND ws.IsDelivery > 0
                        AND ws.CityID = '.$cityId;
        return $this->query($sql);
    }

    /**
     *
     * @param integer $cityId
     * @param array $goodIds
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getDeliveryDatesByGoodIds($cityId,$goodIds) {
        if (!is_array($goodIds) || count($goodIds) == 0) {
            return null;
        }
        $sql = 'SELECT
                        grad.GoodID,
						grad.PickupDate,
                        grad.AvailDeliveryDate,
                        grad.SuborderDeliveryDate,
						grad.DeliveryDate,
        				grad.ShowcaseRetrieveDate
                    FROM
                        #good_reserve_avail_date:active# grad
                    WHERE
                        grad.CityID = '.$cityId.'
                        AND grad.GoodID IN ('.implode(',', $goodIds).')';
        return $this->query($sql);
    }

    public function getDeliveryDatesByGoodId($cityId,$goodId) {

        $sql = 'SELECT
                        grad.GoodID,
						grad.PickupDate,
                        grad.AvailDeliveryDate,
                        grad.SuborderDeliveryDate,
						grad.DeliveryDate,
        				grad.ShowcaseRetrieveDate
                    FROM
                        #good_reserve_avail_date:active# grad
                    WHERE
                        grad.CityID = '.$cityId.'
                        AND grad.GoodID =' . $goodId;
        return $this->query($sql);
    }

    /**
     *
     * @param integer $cityId
     * @param array $goodIds
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getPickupDeliveryDatesByCityId($cityId) {
        if (!is_int($cityId) || $cityId <= 0) {
            return null;
        }
        $sql = 'SELECT
                    grad.GoodID,
                    grad.PickupDate
                FROM
                    #good_reserve_avail_date:active# grad
                WHERE
                    grad.CityID = '.$cityId;
        $rows = $this->query($sql, true)->toArray();
        $result = [];
        foreach ($rows as $row) {
            $result[(int)$row['GoodID']] = $row['PickupDate'];
        }
        return $result;
    }

    /**
     *
     * @see \SoloDelivery\Data\DeliveryMapperInterface::getPickupDeliveriesByCityId()
     */
    public function getPickupDeliveriesByCityId($cityId) {
    	$sql = "SELECT
    				ag.GoodID,
    				grad.PickupDate,
    				ag.SuborderQuantity,
    				alg.CategoryID AS HardCategoryID,
    				alg.BrandID
    			FROM #good_reserve_avail_date:active# grad
    				INNER JOIN #arrival_goods# ag
    					ON grad.GoodID = ag.GoodID
    				INNER JOIN
                        #all_goods:active# alg
                        ON alg.GoodID = ag.GoodID
    			WHERE
    				grad.CityID = {$cityId}";
    	return $this->query($sql);
    }

    public function getElevatorTypes() {
        $sql = 'SELECT
                    *
                FROM
                    elevator_types';

        return $this->query($sql);
    }

    public function getLiftingPrices() {
        $sql = 'SELECT
                    *
                FROM
                    #lifting_service_prices#';

        return $this->query($sql);
    }
}
