<?php

namespace SoloDelivery\Data;

use Solo\Db\TableGateway\AbstractTable;

class DeliveryTimeRangesTable extends AbstractTable implements DeliveryTimeRangesTableInterface {

    /**
     * (non-PHPdoc)
     * @see \SoloDelivery\Data\DeliveryTimeRangesTableInterface::getDeliveryTimeRanges()
     */
    public function getDeliveryTimeRanges() {
        $select = $this->createSelect();
        return $this->tableGateway->selectWith($select);
    }

}

?>