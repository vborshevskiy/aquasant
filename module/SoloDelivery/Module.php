<?php

namespace SoloDelivery;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Solo\ModuleManager\MultiConfigModule;
use Zend\Mvc\MvcEvent;
use SoloDelivery\Service\ProvidesPostDeliveryTerms;

class Module extends MultiConfigModule implements AutoloaderProviderInterface {

    public function __construct() {
        parent::__construct(__DIR__);
        $this->addConfigFile('task.config.php');
    }

    public function getAutoloaderConfig() {
        return [
            'Zend\Loader\ClassMapAutoloader' => [
                __DIR__ . DIRECTORY_SEPARATOR . 'autoload_classmap.php'
            ],
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . str_replace('\\', '/', __NAMESPACE__)
                ]
            ]
        ];
    }
    
    /**
     * 
     * @param Zend\Mvc\MvcEvent $e
     */
    public function onBootstrap(MvcEvent $e) {
        $sm = $e->getApplication()->getServiceManager();
        $postDeliveryTermsService = $sm->get(__NAMESPACE__ . '\\Service\\PostDeliveryTermsService');
        ProvidesPostDeliveryTerms::setPostDeliveryTermsService($postDeliveryTermsService);
    }

}
