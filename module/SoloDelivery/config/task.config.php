<?php
use SoloDelivery\Task\LogisticCompaniesReplicator;
use SoloDelivery\Task\DeliveryReplicator;
use SoloDelivery\Task\TkPekWarehousesReplicator;

return [
    'tasks' => [
        'logistic_companies' => 'SoloDelivery\Task\LogisticCompaniesReplicator',
        'delivery' => 'SoloDelivery\Task\DeliveryReplicator',
        'tk_pek_warehouses' => 'SoloDelivery\Parser\TkPekWarehousesReplicator',
    ],
    'service_manager' => [
        'factories' => [
            'SoloDelivery\Task\LogisticCompaniesReplicator' => function ($sm) {
                $logisticCompaniesTable = $sm->get('SoloDelivery\Data\LogisticCompaniesTable');
                $logisticCompaniesTable->getTableGateway()->setTable($sm->get('triggers')->passive('logistic_companies'));
                $logisticCompaniesShippingScheduleTable = $sm->get('SoloDelivery\Data\LogisticCompaniesShippingScheduleTable');
                $logisticCompaniesShippingScheduleTable->getTableGateway()->setTable($sm->get('triggers')->passive('logistic_companies_shipping_schedule'));
                $task = new LogisticCompaniesReplicator($logisticCompaniesTable, $logisticCompaniesShippingScheduleTable);
                return $task;
            },
            'SoloDelivery\Task\DeliveryReplicator' => function ($sm) {
                $deliveryTimeRangesTable = $sm->get('SoloDelivery\Data\DeliveryTimeRangesTable');
                $deliveryTimeRangesTable->getTableGateway()->setTable($sm->get('triggers')->passive('delivery_time_ranges'));
                
                $deliveryUnavailDatesTable = $sm->get('SoloDelivery\Data\DeliveryUnavailDatesTable');
                $deliveryUnavailDatesTable->getTableGateway()->setTable($sm->get('triggers')->passive('delivery_unavail_dates'));
                
                $goodReserveAvailDateTable = $sm->get('SoloDelivery\Data\GoodReserveAvailDateTable');
                $goodReserveAvailDateTable->getTableGateway()->setTable($sm->get('triggers')->passive('good_reserve_avail_date'));
                
                $holidaysTable = $sm->get('SoloDelivery\Data\HolidaysTable');
                $holidaysTable->getTableGateway()->setTable($sm->get('triggers')->passive('holidays'));

                $liftingServicePricesTable = $sm->get('SoloDelivery\Data\LiftingServicePricesTable');
                $liftingServicePricesTable->getTableGateway()->setTable($sm->get('triggers')->passive('lifting_service_prices'));
                
                $task = new DeliveryReplicator($deliveryTimeRangesTable, $deliveryUnavailDatesTable, $goodReserveAvailDateTable, $holidaysTable, $liftingServicePricesTable);
                return $task;
            },
            'SoloDelivery\Parser\TkPekWarehousesReplicator' => function($sm) {
                $config = $sm->get('config');
                
                $tkPekWarehousesLinkRegionsTable = $sm->get('SoloDelivery\Data\TkPekWarehousesLinkRegionsTable');
                $tkPekWarehousesLinkRegionsTable->getTableGateway()->setTable($sm->get('triggers')->passive('tk_pek_warehouses_link_regions'));
                
                $tkPekWarehousesTable = $sm->get('SoloDelivery\Data\TkPekWarehousesTable');
                $tkPekWarehousesTable->getTableGateway()->setTable($sm->get('triggers')->passive('tk_pek_warehouses'));
                
                $tkPekMapper = $sm->get('SoloDelivery\Data\TkPekMapper');
                
                return new TkPekWarehousesReplicator($config['tk_pek_warehouses_parser']['options'], $tkPekWarehousesLinkRegionsTable, $tkPekWarehousesTable, $tkPekMapper);
            },
        ],
    ],
];