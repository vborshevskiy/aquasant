<?php

use Solo\Db\QueryGateway\QueryGateway;

return [
    'console' => [
        'router' => [
            'routes' => [
                'delivery' => [
                    'options' => [
                        'route' => 'shipped',
                        'defaults' => [
                            'controller' => 'SoloDelivery\Controller\Console',
                            'action' => 'shipped'
                        ],
                    ],
                ],
                'delivery-days' => [
                    'options' => [
                        'route' => 'delivery-days',
                        'defaults' => [
                            'controller' => 'SoloDelivery\Controller\Console',
                            'action' => 'delivery-days'
                        ],
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'invokables' => [
            'SoloDelivery\Controller\Console' => 'SoloDelivery\Controller\ConsoleController',
        ],
    ],
    'controller_plugins' => [
        'invokables' => [
            'delivery' => 'SoloDelivery\Controller\Plugin\Delivery',
            'postDelivery' => 'SoloDelivery\Controller\Plugin\PostDelivery',
            'region' => 'SoloDelivery\Controller\Plugin\Region',
            'postPickup' => 'SoloDelivery\Controller\Plugin\PostPickup',
        ]
    ],
    'service_manager' => [
        'aliases' => [
            'delivery_service' => 'SoloDelivery\Service\DeliveryService',
        ],
        'factories' => [
            'SoloDelivery\Data\LogisticCompaniesTable' => function ($sm) {
                $gateway = new Solo\Db\TableGateway\TableGateway($sm->get('triggers')->active('logistic_companies'), 'CompanyID');
                $table = new SoloDelivery\Data\LogisticCompaniesTable($gateway);
                return $table;
            },
            'SoloDelivery\Data\DeliveryTimeRangesTable' => function ($sm) {
                $gateway = new Solo\Db\TableGateway\TableGateway($sm->get('triggers')->active('delivery_time_ranges'), 'TimeRangeID');
                $table = new SoloDelivery\Data\DeliveryTimeRangesTable($gateway);
                return $table;
            },
            'SoloDelivery\Data\DeliveryUnavailDatesTable' => function ($sm) {
                $gateway = new Solo\Db\TableGateway\TableGateway($sm->get('triggers')->active('delivery_unavail_dates'), 'Date');
                $table = new SoloDelivery\Data\DeliveryUnavailDatesTable($gateway);
                return $table;
            },
            'SoloDelivery\Data\GoodReserveAvailDateTable' => function ($sm) {
                $gateway = new Solo\Db\TableGateway\TableGateway($sm->get('triggers')->active('good_reserve_avail_date'), ['CityID', 'GoodID']);
                $table = new SoloDelivery\Data\GoodReserveAvailDateTable($gateway);
                return $table;
            },
            'SoloDelivery\Data\HolidaysTable' => function ($sm) {
                $gateway = new Solo\Db\TableGateway\TableGateway($sm->get('triggers')->active('holidays'));
                $table = new SoloDelivery\Data\HolidaysTable($gateway);
                return $table;
            },
            'SoloDelivery\Data\LiftingServicePricesTable' => function ($sm) {
                $gateway = new Solo\Db\TableGateway\TableGateway($sm->get('triggers')->active('lifting_service_prices'));
                $table = new SoloDelivery\Data\LiftingServicePricesTable($gateway);
                return $table;
            },
            'SoloDelivery\Data\TkPekWarehousesLinkRegionsTable' => function ($sm) {
                $gateway = new Solo\Db\TableGateway\TableGateway($sm->get('triggers')->active('tk_pek_warehouses_link_regions'));
                $table = new SoloDelivery\Data\TkPekWarehousesLinkRegionsTable($gateway);
                return $table;
            },
            'SoloDelivery\Data\TkPekWarehousesTable' => function ($sm) {
                $gateway = new Solo\Db\TableGateway\TableGateway($sm->get('triggers')->active('tk_pek_warehouses'));
                $table = new SoloDelivery\Data\TkPekWarehousesTable($gateway);
                return $table;
            },
            'SoloDelivery\Data\DeliveryMapper' => function ($sm) {
                return new SoloDelivery\Data\DeliveryMapper(new QueryGateway());
            },
            'SoloDelivery\Data\PostDeliveryMapper' => function ($sm) {
                return new SoloDelivery\Data\PostDeliveryMapper(new QueryGateway());
            },
            'SoloDelivery\Data\LogisticCompaniesMapper' => function ($sm) {
                return new SoloDelivery\Data\LogisticCompaniesMapper(new QueryGateway());
            },
            'SoloDelivery\Data\PostDeliveryTermsMapper' => function ($sm) {
                return new SoloDelivery\Data\PostDeliveryTermsMapper(new QueryGateway());
            },
            'SoloDelivery\Data\TkPekMapper' => function ($sm) {
                return new SoloDelivery\Data\TkPekMapper(new QueryGateway());
            },
            'SoloDelivery\Service\LogisticCompaniesService' => function ($sm) {
                $logisticCompaniesMapper = $sm->get('SoloDelivery\Data\LogisticCompaniesMapper');
                $service = new SoloDelivery\Service\LogisticCompanyService($logisticCompaniesMapper);
                return $service;
            },
            'SoloDelivery\Service\DeliveryTimeRangeService' => function ($sm) {
                $deliveryTimeRangesTable = $sm->get('SoloDelivery\Data\DeliveryTimeRangesTable');
                $deliveryHelper = $sm->get('SoloDelivery\Service\Helper\DeliveryHelper');
                $service = new SoloDelivery\Service\DeliveryTimeRangeService($deliveryTimeRangesTable, $deliveryHelper);
                return $service;
            },
            'SoloDelivery\Service\DeliveryService' => function($sm) {
                $deliveryMapper = $sm->get('SoloDelivery\Data\DeliveryMapper');
                $deliveryHelper = $sm->get('SoloDelivery\Service\Helper\DeliveryHelper');
                $service = new \SoloDelivery\Service\DeliveryService($deliveryMapper, $deliveryHelper);
                return $service;
            },
            'SoloDelivery\Service\PostDeliveryService' => function($sm) {
                $config = $sm->get('Config');
                $service = new \SoloDelivery\Service\PostDeliveryService($config['transport_companies']);
                return $service;
            },
            'SoloDelivery\Service\RegionService' => function($sm) {
                $postDeliveryMapper = $sm->get('SoloDelivery\Data\PostDeliveryMapper');
                $service = new \SoloDelivery\Service\RegionService($postDeliveryMapper);
                return $service;
            },
            'SoloDelivery\Service\PostDeliveryCalculatorService' => function($sm) {
                $tkConfig = $sm->get('Config')['transport_companies'];
                $logisticCompaniesService = $sm->get('SoloDelivery\Service\LogisticCompaniesService');
                $logisticCompaniesSchedule = $logisticCompaniesService->getSchedule();
                $resultConfig = [];
                foreach ($tkConfig as $tkUid => $tk) {
                    if (array_key_exists($tk['erp_id'], $logisticCompaniesSchedule)) {
                        $tk['schedule'] = $logisticCompaniesSchedule[$tk['erp_id']];
                        $resultConfig[$tkUid] = $tk;
                    }
                }
                $service = new SoloDelivery\Service\PostDeliveryCalculatorService($resultConfig);
                return $service;
            },
            'SoloDelivery\Service\PostDeliveryTermsService' => function($sm) {
                $postDeliveryTermsMapper = $sm->get('SoloDelivery\Data\PostDeliveryTermsMapper');
                $service = new \SoloDelivery\Service\PostDeliveryTermsService($postDeliveryTermsMapper);
                return $service;
            },
            'SoloDelivery\Service\TkPekService' => function($sm) {
                $tkPekMapper = $sm->get('SoloDelivery\Data\TkPekMapper');
                $service = new \SoloDelivery\Service\TkPekService($tkPekMapper);
                return $service;
            },
            'SoloDelivery\Service\Helper\DeliveryHelper' => function($sm) {
                $helper = new \SoloDelivery\Service\Helper\DeliveryHelper();
                return $helper;
            },
            'SoloDelivery\Entity\DeliveryDates' => function($sm) {
                $config = $sm->get('Config');
                $deliveryHelper = $sm->get('SoloDelivery\Service\Helper\DeliveryHelper');
                return new \SoloDelivery\Entity\DeliveryDates($config['delivery']['delivery_days_count'], $deliveryHelper);
            },
        ]
    ]
];
