<?php

namespace SoloBank\Controller\Plugin;

use Solo\Mvc\Controller\Plugin\AbstractPlugin;

class Bank extends AbstractPlugin {

	/**
	 *
	 * @param string $name        	
	 * @param string $arguments        	
	 * @throws \BadMethodCallException
	 * @return mixed
	 */
	public function __call($name, $arguments) {
		$service = $this->getController()->getServiceLocator()->get('\\SoloBank\\Service\\BanksService');
		if (!method_exists($service, $name)) {
			throw new \BadMethodCallException('Invalid banks method: ' . $name);
		}
		return call_user_func_array(array(
			$service,
			$name 
		), $arguments);
	}

}

?>