<?php

namespace SoloBank\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class BanksServiceFactory extends AbstractServiceFactory {

	/**
	 * (non-PHPdoc)
	 *
	 * @see \Solo\ServiceManager\AbstractServiceFactory::create()
	 */
	protected function create() {
		$banksTable = $this->getServiceLocator()->get('SoloBank\Data\BanksTable');
		$service = new BanksService($banksTable);
		return $service;
	}
}

?>