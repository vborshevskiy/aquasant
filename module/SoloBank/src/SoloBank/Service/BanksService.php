<?php

namespace SoloBank\Service;

use SoloBank\Entity\BankCollection;
use SoloBank\Entity\Bank;
use SoloBank\Data\BanksTableInterface;

class BanksService {

    /**
     *
     * @var BanksTableInterface
     */
    protected $banksTable;
    
    /**
	 *
	 * @param BanksTableInterface $banksTable     	
	 */
	public function __construct(BanksTableInterface $banksTable) {
		$this->banksTable = $banksTable;
	}

    /**
     * 
     * @param array $bankIds
     * @return BankCollection
     */
    public function getBanksByIds(array $bankIds) {
        if (empty($bankIds)) {
            return [];
        }
        $banksData = $this->banksTable->getBanksByIds($bankIds);
        $bankCollection = new BankCollection();
        foreach ($banksData as $bankData) {
            $bankCollection->add($this->createBank($bankData));
        }
        return $bankCollection;
    }
    
    /**
     * 
     * @param ResultSet $bankData
     * @return Bank
     */
    private function createBank($bankData) {
        $bank = new Bank();
        $bank->setId((int)$bankData['BankID']);
        $bank->setName($bankData['BankName']);
        $bank->setBic((int)$bankData['BankBIC']);
        $bank->setCorrAccount((int)$bankData['BankCorrAccount']);
        $bank->setAddress($bankData['BankAddress']);
        return $bank;
    }

}

?>