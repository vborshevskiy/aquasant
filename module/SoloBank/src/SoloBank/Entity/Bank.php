<?php

namespace SoloBank\Entity;

class Bank {

    /**
     *
     * @var integer
     */
    private $id = 0;

    /**
     *
     * @var string
     */
    private $name = '';

    /**
     *
     * @var integer
     */
    private $bic = null;

    /**
     *
     * @var integer
     */
    private $corrAccount = null;

    /**
     *
     * @var string
     */
    private $address = '';

    /**
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @param integer $id        	
     * @throws \InvalidArgumentException
     * @return \SoloBank\Entity\Bank
     */
    public function setId($id) {
        if (!is_integer($id)) {
            throw new \InvalidArgumentException('Id must be integer');
        }
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     *
     * @param string $name        	
     * @return \SoloBank\Entity\Bank
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getBic() {
        return $this->bic;
    }

    /**
     *
     * @param integer $bic        	
     * @throws \InvalidArgumentException
     * @return \SoloBank\Entity\Bank
     */
    public function setBic($bic) {
        if (!is_integer($bic)) {
            throw new \InvalidArgumentException('BIC id must be integer');
        }
        $this->bic = $bic;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getCorrAccount() {
        return $this->corrAccount;
    }

    /**
     *
     * @param integer $corrAccount        	
     * @throws \InvalidArgumentException
     * @return \SoloBank\Entity\Bank
     */
    public function setCorrAccount($corrAccount) {
        if (!is_integer($corrAccount)) {
            throw new \InvalidArgumentException('Correspondent account id must be integer');
        }
        $this->corrAccount = $corrAccount;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     *
     * @param string $address        	
     * @return \SoloBank\Entity\Bank
     */
    public function setAddress($address) {
        $this->address = $address;
        return $this;
    }

}

?>