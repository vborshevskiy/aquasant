<?php

namespace SoloBank\Entity;

use Solo\Collection\Collection;

class BankCollection extends Collection {

    /**
     *
     * @param Bank $bank        	
     * @return Bank
     */
    public function add($bank, $key = null) {
        if (!($bank instanceof Bank)) {
            throw new \RuntimeException('Bank must be an instace of Bank');
        }
        $key = (is_null($key) ? $bank->getId() : $key);
        parent::add($bank, $key);
        return $bank;
    }

}

?>