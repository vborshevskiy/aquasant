<?php

namespace SoloBank\Data;

use Solo\Db\TableGateway\AbstractTable;

class BanksTable extends AbstractTable implements BanksTableInterface {

    /**
     * (non-PHPdoc)
     * @see \SoloBank\Data\BanksTableInterface::getBanksByIds()
     */
    public function getBanksByIds(array $bankIds) {
        $select = $this->createSelect();
        $select->where(['BankID' => $bankIds]);
        return $this->tableGateway->selectWith($select);
    }

}

?>