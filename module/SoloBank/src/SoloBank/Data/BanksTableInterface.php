<?php

namespace SoloBank\Data;

use Zend\Db\ResultSet\ResultSet;
use Solo\Db\TableGateway\TableInterface;

interface BanksTableInterface extends TableInterface {

    /**
     * 
     * @param array $bankIds
     * @return ResultSet
     */
    public function getBanksByIds(array $bankIds);
}

?>