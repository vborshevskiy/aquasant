<?php

namespace SoloBank\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\Service\ProvidesWebservice;
use SoloBank\Data\BanksTable;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloReplication\Service\DataMapper;

class BanksReplicator extends AbstractReplicationTask {
	
	use ProvidesWebservice;

	/**
	 *
	 * @var BanksTable
	 */
	private $banksTable;

	/**
	 *
	 * @param BanksTable $banksTable        	
	 */
	public function __construct(BanksTable $banksTable) {
		$this->banksTable = $banksTable;
		
		$this->addSwapTable('banks');
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \SoloReplication\Service\TaskInterface::process()
	 */
	public function process() {
		$this->processBanks();
	}

	/**
	 *
	 * @return \SoloReplication\Service\DataMapper
	 */
	protected function createBanksMapper() {
		$mapper = new DataMapper();
		$mapper->setMappings(
			[
				'BankID' => '%d: Id',
				'BankName' => 'Name',
				'BankBIC' => '%d: Bic',
				'BankCorrAccount' => '%d: CorrAccount',
				'BankAddress' => 'Address',
			]);
		
		return $mapper;
	}

	/**
	 */
	protected function processBanks() {
        $this->log->info('Insert banks');
		$mapper = $this->createBanksMapper();
		$dataSet = [];
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetBanks'));             
                if (!$rdr->isEmpty()) {
			$this->banksTable->truncate();
		}
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->banksTable->insertSet($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->banksTable->insertSet($dataSet);
		}
		
		$this->log->info('Inserted banks = ' . sizeof($dataSet));
	}

}
?>