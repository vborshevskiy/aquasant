<?php
use Solo\Db\TableGateway\TableGateway;
use SoloBank\Data\BanksTable;

return [
	'controller_plugins' => [
		'invokables' => [
			'bank' => 'SoloBank\Controller\Plugin\Bank' 
		] 
	],
	'service_manager' => [
		'factories' => [
			'SoloBank\Data\BanksTable' => function ($sm) {
				$gateway = new TableGateway($sm->get('triggers')->active('banks'), 'BankID');
				$table = new BanksTable($gateway);
				return $table;
			},
			'SoloBank\Service\BanksService' => 'SoloBank\Service\BanksServiceFactory' 
		]
	] 
];
