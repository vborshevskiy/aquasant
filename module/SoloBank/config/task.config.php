<?php
use SoloBank\Task\BanksReplicator;

return [
	'tasks' => [
		'banks' => 'SoloBank\Task\BanksReplicator' 
	],
    'service_manager' => [
		'factories' => [
			'SoloBank\Task\BanksReplicator' => function ($sm) {
				$bankTable = $sm->get('SoloBank\Data\BanksTable');
				$bankTable->getTableGateway()->setTable($sm->get('triggers')->passive('banks'));
				$task = new BanksReplicator($bankTable);
				return $task;
			},
		],
	],
];