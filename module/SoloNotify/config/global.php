<?php

return [
    'notify' => [
        'admin_emails' => [
            'ibu@ebdrive.com',
        ],
        'max_notifies' => 20,
        'timeout' => 10,
        'timeout_after_max' => 30 * 60,
    ],
    'file_notify' => [
        'alert_file_path' => dirname(__FILE__) . '/../../../data/',
        'alert_file_flag' => 'webservice_alert.flag',
    ]
];
