<?php

return [
    'controller_plugins' => [
        'invokables' => [
            'notify' => 'SoloNotify\Controller\Plugin\Notify'
        ]
    ],
    'service_manager' => [
        'factories' => [
            'SoloNotify\Service\NotifyService' => 'SoloNotify\Service\NotifyServiceFactory',
        ]
    ]
];
