<?php

namespace SoloNotify\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class NotifyServiceFactory extends AbstractServiceFactory {

    /**
     * (non-PHPdoc)
     *
     * @see \Solo\ServiceManager\AbstractServiceFactory::create()
     */
    protected function create() {
        $mailer = $this->getServiceLocator()->get('mailer');
        $adminEmails = $this->getServiceLocator()->get('config')['notify']['admin_emails'];
        $from = $this->getServiceLocator()->get('config')['mail']['from'];
        $fromName = $this->getServiceLocator()->get('config')['mail']['fromName'];
        $alertFilePath = $this->getServiceLocator()->get('config')['file_notify']['alert_file_path'];
        $alertFileName = $this->getServiceLocator()->get('config')['file_notify']['alert_file_flag'];
        $service = new NotifyService($mailer, $adminEmails, $from, $fromName, $alertFilePath, $alertFileName);
        return $service;
    }

}

?>