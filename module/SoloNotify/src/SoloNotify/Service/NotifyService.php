<?php

namespace SoloNotify\Service;

use Zend\Mail\Transport\Smtp;

class NotifyService {

    /**
     *
     * @var array|string
     */
    protected $emails;

    /**
     *
     * @var \Zend\Mail\Transport\Smtp
     */
    protected $mailer;
    
    /**
     *
     * @var string
     */
    protected $fromName;
    
    /**
     *
     * @var string
     */
    protected $from;
    
    /**
     *
     * @var string
     */
    private $alertFilePath = null;
    
    /**
     *
     * @var string
     */
    private $alertFileName = null;

    /**
     * 
     * @param Smtp $mailer
     * @param array|string $emails
     * @param string $from
     * @param string $fromName
     * @param string $alertFilePath
     * @param string $alertFileName
     */
    public function __construct(Smtp $mailer, $emails, $from, $fromName, $alertFilePath, $alertFileName) {
        if (is_array($emails) && count($emails) > 0) {
            $this->emails = $emails;
            $this->mailer = $mailer;
            $this->from = $from;
            $this->fromName = $fromName;
        }
        if (file_exists($alertFilePath)) {
            $this->alertFilePath = $alertFilePath;
        }
        $this->alertFileName = $alertFileName;
    }

    /**
     * 
     * @return array|string
     */
    public function getEmails() {
        return $this->emails;
    }

    /**
     * 
     * @return boolean
     */
    public function hasEmail() {
        if (is_array($this->emails) && count($this->emails) > 0) {
            return true;
        }
        if (is_string($this->emails) && strlen($this->emails) > 0) {
            return true;
        }
        return false;
    }
    
    /**
     * 
     * @return string
     */
    public function getFrom() {
        return $this->from;
    }
    
    /**
     * 
     * @return string
     */
    public function getFromName() {
        return $this->fromName;
    }

    /**
     * 
     * @return \Zend\Mail\Transport\Smtp
     */
    public function getMailer() {
        return $this->mailer;
    }

    /**
     * 
     * @param string $message
     */
    public function send($message, $title = 'Брошено исключение', $emails = null) {
        if ($this->hasEmail()) {
            $mail = new \Zend\Mail\Message();
            $mail->addFrom($this->getFrom(), $this->getFromName());
            $mail->addTo(is_null($emails) ? $this->getEmails() : $emails);
            $mail->setSubject($title);
            $mail->setEncoding('UTF-8');
            $mail->setBody($message);
            $this->getMailer()->send($mail);
        }
    }
    
    /**
     * 
     * @param string $request
     * @param string $response
     */
    public function sendOrderNotify($request, $response) {
        if ($this->hasEmail()) {
            $text = 'Input:'."\n";
            $text .= $request."\n\n";
            $text .= 'Output:'."\n";
            $text .= $response."\n\n";
            
            $mail = new \Zend\Mail\Message();
            $mail->addFrom($this->getFrom(), $this->getFromName());
            $mail->addTo($this->getEmails());
            $mail->setSubject('Ахтунг на аквасанте '.date('d-m-Y H:i'));
            $mail->setEncoding('UTF-8');
            $mail->setBody($text);
            $this->getMailer()->send($mail);
        }
    }
    
    /**
     * 
     * @param integer $reserveId
     */
    public function sendPaymentNotify($reserveId) {
        if ($this->hasEmail()) {
            $text = 'Оплата через интернет прошла, но данные о платеже не ушли в ERP.'."\n";
            $text .= 'Номер резерва: '. $reserveId . "\n";
            
            $mail = new \Zend\Mail\Message();
            $mail->addFrom($this->getFrom(), $this->getFromName());
            $mail->addTo($this->getEmails());
            $mail->setSubject('Ахтунг оплаты на аквасанте '.date('d-m-Y H:i'));
            $mail->setEncoding('UTF-8');
            $mail->setBody($text);
            $this->getMailer()->send($mail);
        }
    }
    
    /**
     * 
     * @param boolean $flag
     * @throws \InvalidArgumentException
     */
    public function fileWarning($flag = false) {
        if (!is_bool($flag)) {
            throw new \InvalidArgumentException('Warning flag must be boolean');
        }
        if ($flag && !file_exists($this->alertFilePath . $this->alertFileName)) {
            file_put_contents($this->alertFilePath . $this->alertFileName, '', LOCK_EX);
        } elseif (!$flag && file_exists($this->alertFilePath . $this->alertFileName)) {
            unlink($this->alertFilePath . $this->alertFileName);
        }
    }

}
