<?php

namespace SoloNotify\Controller\Plugin;

use Solo\Mvc\Controller\Plugin\AbstractPlugin;

class Notify extends AbstractPlugin {

    /**
     *
     * @param string $name        	
     * @param string $arguments        	
     * @throws \BadMethodCallException
     * @return mixed
     */
    public function __call($name, $arguments) {
        $service = $this->getController()->getServiceLocator()->get('\\SoloNotify\\Service\\NotifyService');
        if (!method_exists($service, $name)) {
            throw new \BadMethodCallException('Invalid notify method: ' . $name);
        }
        return call_user_func_array(array(
            $service,
            $name
        ), $arguments);
    }

}