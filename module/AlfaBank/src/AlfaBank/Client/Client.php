<?php

namespace AlfaBank\Client;

use Zend\Http\Client as HttpClient;
use Zend\Http\Request;
use Zend\Http\Response;

class Client
{
    /** @var HttpClient  */
    private $httpClient;

    /** @var string */
    private $endpoint;

    /** @var string */
    private $login;

    /** @var string */
    private $password;

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param $resource
     * @param array $params
     * @return mixed|Response
     */
    public function get($resource, $params = [])
    {
        return $this->request($resource, $params);
    }

    /**
     * @param $resource
     * @param array $data
     * @return mixed|Response
     */
    public function post($resource, $data = [])
    {
        return $this->request($resource, $data, Request::METHOD_POST);
    }

    /**
     * @param $resource
     * @param array $params
     * @param string $method
     * @return mixed|Response
     * @throws \Exception
     */
    public function request($resource, $params = [], $method = Request::METHOD_GET)
    {
        $this->httpClient->setUri($this->endpoint . $resource);
        $this->httpClient->setMethod($method);

        $params['userName'] = $this->getLogin();
        $params['password'] = $this->getPassword();

        switch ($method) {
            case Request::METHOD_GET:
                $this->httpClient->setParameterGet(http_build_query($params));
                break;
            case Request::METHOD_POST:
                $this->httpClient->setParameterPost($params);
                break;
        }

        $response = $this->httpClient->send();

        if (!$response->isSuccess()) {
            throw new \Exception(
                'AlfaBankApi error: '
                . $response->getStatusCode()
                . ' ' . $response->getReasonPhrase()
                . ': ' . $this->endpoint . $resource . ' Params: ' . print_r($params, true),
                $response->getStatusCode()
            );
        }

        $response = json_decode($response->getBody(), true);

        if (is_null($response)) {
            throw new \Exception('AlfaBankApi error: cannot parse json. ' . json_last_error());
        }

        return $response;
    }

    /**
     * @param $endpoint
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }
}