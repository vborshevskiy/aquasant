<?php

namespace AlfaBank\Client\Factory;

use AlfaBank\Client\Client;
use Zend\Http\Client as HttpClient;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ClientFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config')['alfabank-api-client'];

        $httpClient = new HttpClient();

        if (!empty($config['options'])) {
            $httpClient->setOptions($config['options']);
        }

        $client = new Client($httpClient);
        $client->setEndpoint($config['endpoint']);
        $client->setLogin($config['login']);
        $client->setPassword($config['password']);

        return $client;
    }
}