<?php

namespace AlfaBank;

use AlfaBank\Client\Client;
use AlfaBank\Client\Factory\ClientFactory;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                Client::class => ClientFactory::class
            ]
        ];
    }

    public function getAutoloaderConfig() {
        return [
            'Zend\Loader\ClassMapAutoloader' => [
                __DIR__ . DIRECTORY_SEPARATOR . 'autoload_classmap.php'
            ],
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . str_replace('\\', '/', __NAMESPACE__)
                ]
            ]
        ];
    }
}