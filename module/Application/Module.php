<?php

namespace Application;

use Zend\Console\Request as ConsoleRequest;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature as ZendGlobalAdapterFeature;
use Solo\Db\QueryGateway\Feature\GlobalAdapterFeature as SoloGlobalAdapterFeature;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\ConsoleBannerProviderInterface;
use Zend\Console\Adapter\AdapterInterface;
use Solo\ModuleManager\MultiConfigModule;
use Solo\Db\QueryGateway\Feature\GlobalBalancingFeature;
use Solo\Db\QueryGateway\Feature\TemplatedSqlFeature;
use Zend\Mvc\Router\RouteMatch;
use Zend\ServiceManager\ServiceLocatorInterface;
use Solo\Db\QueryGateway\QueryGateway;
use Solo\Db\TableGateway\TableGateway;

class Module extends MultiConfigModule implements ConsoleBannerProviderInterface {

    public function __construct() {
        parent::__construct(__DIR__);
    }

    public function onBootstrap(MvcEvent $e) {
        $em = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($em);

        $sm = $e->getApplication()->getServiceManager();
        $config = $sm->get('Config');

        if (isset($config['db']['balancing']) && $config['db']['balancing']['enabled']) {
            $balancing = $sm->get('Solo\Db\BalancingManager\BalancingManager');
            GlobalBalancingFeature::setStaticBalancingManager($balancing);
            QueryGateway::addDefaultFeature(new GlobalBalancingFeature());
        }

        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
        ZendGlobalAdapterFeature::setStaticAdapter($dbAdapter);
        SoloGlobalAdapterFeature::setStaticAdapter($dbAdapter);
        TableGateway::addDefaultFeature(new ZendGlobalAdapterFeature());
        QueryGateway::addDefaultFeature(new SoloGlobalAdapterFeature());

        // try to set serializer to IgBinary if installed
        try {
            \Zend\Serializer\Serializer::setDefaultAdapter('IgBinary');
        } catch (\Exception $ex) {
            
        }

        QueryGateway::addDefaultFeature($this->createTemplateSqlFeature($sm));

        if (isset($config['session_cache'])) {
            try {
            	//print_r($config['session_cache']); exit();
                $cache = \Zend\Cache\StorageFactory::factory($config['session_cache']);
                //print __LINE__; exit();
                $saveHandler = new \Zend\Session\SaveHandler\Cache($cache);
                $manager = new \Zend\Session\SessionManager();
                $manager->setSaveHandler($saveHandler);
                \Zend\Session\Container::setDefaultManager($manager);
            } catch (\Exception $ex) {
                
            }
        }

//        $em->attach(MvcEvent::EVENT_DISPATCH_ERROR, function($e) {
//            $result = $e->getResult();
//            $result->setTerminal(true);
//        });

        $em->attach(MvcEvent::EVENT_DISPATCH, [$this, 'setLayout'], 100);
        $em->attach(MvcEvent::EVENT_DISPATCH_ERROR, [$this, 'setLayout'], 100);

        $em->attach(MvcEvent::EVENT_DISPATCH, [$this, 'setErrorLayout']);
        $em->attach(MvcEvent::EVENT_DISPATCH_ERROR, [$this, 'setErrorLayout']);
    }

    public function getAutoloaderConfig() {
        return [
            'Zend\Loader\ClassMapAutoloader' => [
                __DIR__ . DIRECTORY_SEPARATOR . 'autoload_classmap.php'
            ],
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . str_replace('\\', '/', __NAMESPACE__)
                ]
            ]
        ];
    }

    public function setLayout(MvcEvent $event) {
        $matches  = $event->getRouteMatch();

        if (!$matches instanceof RouteMatch) {
            // Can't do anything without a route match
            return;
        }

        $request = $event->getRequest();

        if ($request instanceof ConsoleRequest) {
            return;
        }

        $viewModel = $event->getViewModel();

        if (strpos($matches->getMatchedRouteName(), 'cabinet') === 0) {
            $viewModel->setTemplate('layout/cabinet');
        } elseif (strpos($matches->getMatchedRouteName(), 'home') === 0) {
            $viewModel->setTemplate('layout/main');
        } elseif (strpos($matches->getMatchedRouteName(), 'order') === 0) {
            $viewModel->setTemplate('layout/order');
        }
    }

    public function setErrorLayout(MvcEvent $event) {
        $request = $event->getRequest();

        if ($request instanceof ConsoleRequest) {
            return;
        }

        $viewModel = $event->getViewModel();

        $response = $event->getResponse();

        if ($response->getStatusCode() == 404) {
            $viewModel->setTemplate('layout/404');
            return;
        }

        if ($response->getStatusCode() == 500) {
            $viewModel->setTemplate('layout/500');
            return;
        }
    }

    public function getConsoleBanner(AdapterInterface $adapter) {
        return "==-----------------------------------==\n
				Welcome to Solo console application
				==-----------------------------------==\n";
    }

    private function createTemplateSqlFeature(ServiceLocatorInterface $sm) {
        $feature = new TemplatedSqlFeature("/#(.*?)#/");

        // triggers
        $feature->addProcessor(function($tableName, $match, $reload = false) use ($sm) {
            if (false !== stripos($match[1], '@')) {
                $chunks = explode('@', $match[1]);
                $triggeredPart = $chunks[0];
            } else {
                $triggeredPart = $match[1];
            }

            $triggeredPartChunks = explode(':', $triggeredPart);

            $baseName = $triggeredPartChunks[0];
            $state = (isset($triggeredPartChunks[1]) ? $triggeredPartChunks[1] : 'active');

            $triggersService = $sm->get('triggers');
            if ($reload) {
                $triggersService->reload(true, $baseName);
            }
            $tableName = $triggersService->get($baseName)->getTable($state);
            if (empty($tableName)) {
                $tableName = $triggeredPart;
            }
            return $tableName;
        }, 1000);

        return $feature;
    }

}
