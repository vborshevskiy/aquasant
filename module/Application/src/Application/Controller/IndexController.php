<?php

namespace Application\Controller;

use Solo\Stdlib\ArrayHelper;
use SoloCatalog\Service\Helper\DateHelper;
use Google\Analytics\Entity\Ecommerce\TrackerImpression;

class IndexController extends BaseController {

	public function indexAction() {
		$cityId = $this->getErpCityId();
		$title = $this->getSeoTitle('Интернет магазин сантехники «Aquasant» {cityName} — купить в {where} с доставкой через интернет или самовывоз из магазина', $cityId);
		$this->getLayout()->setTitle($title);
		$this->getLayout()->setBodyClass('main');
		$this->getLayout()->addCssFile('main.css');

		$this->getLayout()->addJsFile('catalogue.js');
		$this->getLayout()->addJsFile('main.js');

		$slideImages = glob($_SERVER["DOCUMENT_ROOT"] . "/images/slideshow/*.jpg");
		shuffle($slideImages);

		$slideImage = substr(current($slideImages), strlen($_SERVER["DOCUMENT_ROOT"]));

		$priceCategoryId = $this->catalog()->prices()->helper()->getPriceColumnId($this->isPostDelivery());
		$cityId = $this->getErpCityId();

		$categoryIds = [
			22,
			4,
			23,
			35,
			81
		];
		$categories = $this->catalog()->categories()->getCategoriesByIds($categoryIds);

		$categoriesImages = [
			4 => '/images/main-gr/02.jpg',
			22 => '/images/main-gr/dushevie_kabini.jpg',
			23 => '/images/main-gr/03.jpg',
			81 => '/images/main-gr/unitazi.jpg',
			35 => '/images/main-gr/mebel.jpg'
		];

		$goods = [];
		$goodIds = [];
		foreach ($categoryIds as $categoryId) {
			$result = $this->catalog()->goods()->getAvailableGoodsByCategoryId($categoryId, $cityId, $priceCategoryId);
			$goods[$categoryId] = $result;

			foreach ($result as $good) {
				$goodIds[] = $good['GoodID'];
			}
		}
		
		$goodsRemainsInTheMainStore = $this->catalog()->goods()->getGoodsRemainsByStoreId($goodIds, 1);

		$goodsImages = $this->catalog()->images()->getImagesByGoodIds($goodIds);

		$title = "Оптово-розничная база сантехники Аквасант — вся сантехника в одном месте: душевые кабины, унитазы, ванны, мебель для ванной, полотенцесушители, смесители, купить сантехнику онлайн, доставка сантехники по Москве и России, низкие цены на сантехнику, дешево в интернет магазине сантехники Aquasant, самовывоз";
		$keywords = "сантехника, купить, онлайн, душевые, кабины, унитазы, ванны, мебель, полотенцесушители, смесители, доставка, оптово-розничная, база, Аквасант, Aquasant, дешево, самовывоз, интернет, магазин";
		$description = "Сайт оптово-розничной базы сантехники Аквасант, интернет магазин Aquasant продажа сантехники дешево, есть самовывоз и доставка по Москве и России, большой ассортимент сантехники: душевые кабины, ванны, унитазы, мебель для ванной, смесители, полотенцесушители — на все низкие цены и гарантия";

		$this->getLayout()->setTitle($title);
		$this->getLayout()->addMetaKeywords($keywords);
		$this->getLayout()->addMetaDescription($description);

		$vars = [
			'mainPage' => true,
			'slideImage' => $slideImage,
			'goods' => $goods,
			'goodsImages' => $goodsImages,
			'categories' => $categories,
			'categoryIds' => $categoryIds,
			'categoriesImages' => $categoriesImages,
			'goodsRemainsInTheMainStore' => $goodsRemainsInTheMainStore,
			'dateHelper' => new DateHelper(),
			'dateTime' => new \DateTime()
		];

		// ecommerce tracker
		$ecommerceTracker = $this->createEcommerceTracker('main');
		$position = 1;
		foreach ($categoryIds as $categoryId) {
			if (isset($goods[$categoryId])) {
				$items = $goods[$categoryId];
				foreach ($items as $item) {
					$impression = new TrackerImpression();
					$impression->setId($item['GoodID']);
					$impression->setName($item['GoodName']);
					$impression->setCategoryName($item['CategoryName']);
					$impression->setPrice(intval($item['Value']));
					$impression->setBrandName($item['BrandName']);
					$impression->setListName('ProductMainPage');
					$impression->setPosition($position);
					$position++;

					$ecommerceTracker->addImpression($impression);
				}
			}
		}
		$vars['ecommerceTracker'] = $ecommerceTracker;
		// END ecommerce tracker

		return $this->outputVars($vars);
	}

}
