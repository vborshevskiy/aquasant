<?php

namespace Application\Controller;

use SoloCatalog\Service\Helper\DateHelper;
use Zend\Uri\Uri;
use SoloCatalog\Entity\PackageGroup;
use SoloCatalog\Entity\PackageGroupItem;
use Google\Analytics\Entity\Ecommerce\TrackerImpression;

class ItemController extends BaseController {

	public function indexAction() {
		if (GOD_MODE) {
			if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
				$user = $_SERVER['PHP_AUTH_USER'];
				$pass = $_SERVER['PHP_AUTH_PW'];
				if ($user == "aquasant" && $pass == "superdata") {
					$authenticated = true;
				}
			}
			
			if (!$authenticated) {
				header('WWW-Authenticate: Basic realm="Restricted Area"');
				header('HTTP/1.1 401 Unauthorized');
				echo ("Access denied.");
				exit();
			}
		}
		
		$this->getLayout()->addCssFile('item.css');
		$this->getLayout()->addCssFile('item.print');
		$this->getLayout()->addJsFile('qrcode.min.js');
		$this->getLayout()->addJsFile('item.js');
		
		$itemId = intval($this->params()->fromRoute('item'));
		$item = $this->catalog()->goods()->getGoodByGoodId($itemId);
		
		$itemCategory = $this->catalog()->categories()->getPrimaryCategoryByGoodId(intval($item->GoodID));
		
		$uri = new Uri($_SERVER['REQUEST_URI']);
		$path = $uri->getPath();
		$parsedGoodName = rtrim(substr($path, strpos($path, strval($itemId)) + strlen(strval($itemId)) + 1), '/');
		
		if (!GOD_MODE) {
			if (!strrpos($_SERVER['REQUEST_URI'], $itemCategory->CategoryUID) || $parsedGoodName != $item->GoodEngName) {
				if (empty($itemCategory->CategoryUID)) {
					return $this->notfound();
				}
				
				$response = $this->getResponse();
				$response->getHeaders()->addHeaderLine('Location', '/' . $itemCategory->CategoryUID . '/' . $itemId . '_' . $item->GoodEngName . '/');
				$response->setStatusCode(301);
				return $response;
			}
		}
		
		$categories = [];
		if (!GOD_MODE) {
			$categories = $this->catalog()->categories()->getParentCategories($itemCategory['cLevel'], $itemCategory['cLeft'], $itemCategory['cRight']);
		}
		
		foreach ($categories as $category) {
			$breadcrumbs[] = [
				"href" => '/' . $category['CategoryUID'] . '/',
				"title" => $category['CategoryName'] 
			];
		}
		
		if ($itemCategory) {
			$breadcrumbs[] = [
				"href" => "/" . $itemCategory['CategoryUID'] . "/",
				"title" => $itemCategory['CategoryName'] 
			];
		}
		
		$isPackage = ($item->IsPackage && !$item->IsVirtualPackage);
		
		$priceCategoryId = $this->catalog()->prices()->helper()->getPriceColumnId($this->isPostDelivery());
		$cityId = $this->getErpCityId();
		
		$images = $this->catalog()->images()->getImagesByGoodId($itemId);
		if (isset($_GET['debug_images'])) {
			//print_r($images); exit();
		}
		
		$price = $this->catalog()->prices()->getPriceWithOldPriceByGoodId($itemId, $cityId, $priceCategoryId);
		$properties = [];
		if (!$isPackage) {
			$properties = $this->catalog()->goods()->getGoodDescription($itemId);
		}
		if (isset($_GET['debug_props'])) {
			print_r($properties); exit();
		}
		
		if ($item['VendorCode']) {
			$vendorCodeProperty = new \stdClass();
			$vendorCodeProperty->name = 'Код производителя';
			$vendorCodeProperty->value = $item['VendorCode'];
			$vendorCodeProperty->valueId = 99999;
			$vendorCodeProperty->kindId = 1;
			$vendorCodeProperty->typeId = 1;
			$vendorCodeProperty->hint = '';
			$vendorCodeProperty->hintImage = '';
			$vendorCodeProperty->displayInSmallDescription = 0;
			$vendorCodeProperty->displayInMiddleDescription = 1;
			$vendorCodeProperty->isComparable = 0;
			$vendorCodeProperty->isSelectable = 0;
			
			$properties[$vendorCodeProperty->valueId] = $vendorCodeProperty;
		}
		
		$selectableProperties = [];
		if (!$isPackage) {
			$selectableProperties = $this->catalog()->goods()->getGoodSelectableProperties($itemId, $properties);
		}


        //Узнаём остатки товара
        foreach($this->catalog()->goods()->getStoresRemainsByGoodId($item->GoodID) as $row) switch($row['StoreID']) {
            case 1: //Витрина
                $place = $row;
                $inShowRoom = true;
                break;
            case 12: //Склад
                $store = $row;
                break;
        }

		$bannerService = $this->service('Application\Service\BannerService');
		$dateHelper = new DateHelper();
		
		$availability = $this->catalog()->goods()->getAvailabilityByGoodId($itemId, $cityId);
		
		$deliveryDay = null;
		$deliveryDate = null;
		$showRoomPickupDay = '';
		
		$onlyInShowRoom = ($availability && (0 < $availability['WindowQuantity']) && ($availability['AvailQuantity'] == $availability['WindowQuantity']) && (0 == $availability['SuborderQuantity']));

        $SupplierRemains = $this->catalog()->goods()->getSupplierRemainsByGoodId($item->GoodID);

		if ($availability) {
			
			$deliveryDates = $this->delivery()->getDeliveryDatesByGoodId($cityId, $itemId);
			
			$pickupDate = null;
			if ((0 < $availability['AvailQuantity']) && ($availability['AvailQuantity'] > $availability['WindowQuantity'])) {
				if ($deliveryDates['PickupDate']) {
					$pickupDate = \DateTime::createFromFormat('Y-m-d H:i:s', $deliveryDates['PickupDate']);
				} else {
					$pickupDate = new \DateTime();
				}
			} elseif (0 < $availability['SuborderQuantity'] && $availability['ArrivalDate']) {
				if ($deliveryDates['PickupDate']) {
					$pickupDate = \DateTime::createFromFormat('Y-m-d H:i:s', $deliveryDates['PickupDate']);
				} else {
					$pickupDate = \DateTime::createFromFormat('Y-m-d', $availability['ArrivalDate']);
				}
			}
			if (!$pickupDate && $onlyInShowRoom) {
				$pickupDate = \DateTime::createFromFormat('Y-m-d H:i:s', $deliveryDates['ShowcaseRetrieveDate']);
			}
			if ($pickupDate) {
				$pickupDayNumber = $pickupDate->format('j');
				$pickupShortWeekday = DateHelper::getRussianShortWeekdayByNumber($pickupDate->format('N'));
				$pickupDayName = DateHelper::getDayName($pickupDate);
				$pickupDay = $dateHelper->getPickupDateText($pickupDate);
				$showRoomPickupDay = $pickupDate->format('j') . ' ' . DateHelper::getMonthNameGenitive($pickupDate->format('n'));
			}
			
			/**
			 * OLD Dates format
			 * $deliveryDate = $availability['AvailQuantity'] > 0 ? \DateTime::createFromFormat('Y-m-d H:i:s', $deliveryDates['AvailDeliveryDate']) : \DateTime::createFromFormat(
			 * 'Y-m-d H:i:s',
			 * $deliveryDates['SuborderDeliveryDate']);
			 */
			$deliveryDate = \DateTime::createFromFormat('Y-m-d H:i:s', $deliveryDates['DeliveryDate']);
			if (!$deliveryDate && $onlyInShowRoom) {
				$deliveryDate = \DateTime::createFromFormat('Y-m-d H:i:s', $deliveryDates['ShowcaseRetrieveDate']);
				$deliveryDate->add(new \DateInterval('P1D'));
			}
			
			if ($deliveryDate) {
				$deliveryDayNumber = $deliveryDate->format('j');
				$deliveryShortWeekday = DateHelper::getRussianShortWeekdayByNumber($deliveryDate->format('N'));
				
				$deliveryDayName = DateHelper::getDayName($deliveryDate);
				$deliveryDay = $dateHelper->parseDate(DateHelper::DATE_FORMAT_WITHOUT_YEAR, $deliveryDate->getTimestamp());
			}
		}

		//Если дату доставки определить не удалось, ищем по дате производства
		if(empty($deliveryDayName) && empty($deliveryDay)) foreach($SupplierRemains as $supplier) if($supplier['ProductionTerm']) {
            $deliveryDay = $dateHelper->parseDate(DateHelper::DATE_FORMAT_WITHOUT_YEAR, time() + 86400 * (1 + $supplier['ProductionTerm']));
        }
		
		if (isset($_GET['debug_selectable'])) {
			print_r($selectableProperties); exit();
		}

		$vars = [
			'dateHelper' => $dateHelper,
			'availability' => $availability,
			'pickupDate' => $pickupDate,
			'pickupDayNumber' => $pickupDayNumber,
			'pickupShortWeekday' => $pickupShortWeekday,
			'pickupDayName' => $pickupDayName,
			'pickupDay' => $pickupDay,
			'showRoomPickupDay' => $showRoomPickupDay,
			'deliveryDate' => $deliveryDate,
			'deliveryDayNumber' => $deliveryDayNumber,
			'deliveryShortWeekday' => $deliveryShortWeekday,
			'deliveryDayName' => $deliveryDayName,
			'deliveryDay' => $deliveryDay,
			'item' => $item,
			'images' => $images,
			'breadcrumbs' => $breadcrumbs,
			'price' => $price,
			'properties' => $properties,
			'selectableProperties' => $selectableProperties,
			'rightBanner' => $bannerService->getRandomBanner(1),
			'centerBanner' => $bannerService->getRandomBanner(3),
			'deliveryCost' => $this->catalog()->prices()->getDeliveryCost($cityId),
			'inShowRoom' => $inShowRoom ?? false,
			'onlyInShowRoom' => $onlyInShowRoom,
			'place' => $place ?? false,
            'store' => $store ?? false,
			'dateTime' => new \DateTime(),
			'isWorkTime' => date('H') >= 10 && date('H') < 20,
            'supplierRemains' => MANAGER_MODE ? $SupplierRemains : [],
		];
		if (!GOD_MODE) {
			$vars['category'] = $itemCategory;
			$vars['itemCategory'] = $itemCategory;
			$vars['isCompared'] = $this->comparer()->hasGood($itemId, $itemCategory->SoftCategoryID);
		}
		
		$titles = [
			$item->GoodName . ($item->OriginalGoodID > 0 ? ' уценка ' : ' ') . 'купить в {whereShort} с доставкой на оптово-розничной базе сантехники Аквасант дешево, характеристики' . ($item->ShortVendorName ? ' ' . $item->ShortVendorName : ''),
			$item->GoodName . ($item->OriginalGoodID > 0 ? ' уценка ' : ' ') . 'купить в {whereShort} через интернет магазин сантехники онлайн дешево, характеристики' . ($item->ShortVendorName ? ' ' . $item->ShortVendorName : '') 
		];
		
		$randKey = array_rand($titles);
		$title = $this->getSeoTitle($titles[$randKey], $cityId);
		$this->getLayout()->setTitle($title);
		
		$keywords = str_replace(',', '', $title);
		$keywords = str_replace(' ', ', ', $keywords);
		
		switch ($randKey) {
			case 0:
				$keywords .= ', страница, товара, описание, гарантия, похожие товары, характеристики, сравнение';
				break;
			case 1:
				$keywords .= ', страница, товара, галерея, характеристики, отзывы, сравнение, гарантия, описание';
				break;
		}
		
		$this->getLayout()->addMetaKeywords($keywords);
		
		$seoDescriptions = [
			'Страница товара ' . $item->GoodName . ': описание' . ($item->ShortVendorName ? ' ' . $item->ShortVendorName : '') . ', характеристики, сравнение, галерея изображений, продажа на базе сантехники Аквасант в Москве, доставка, самовывоз',
			'Страница товара ' . $item->GoodName . ': описание' . ($item->ShortVendorName ? ' ' . $item->ShortVendorName : '') . ', характеристики, сравнение, галерея изображений, возможность купить через интернет на онлайн базе сантехники Аквасант, заказ с доставкой по Москве' 
		];
		
		$randKey = array_rand($seoDescriptions);
		$this->getLayout()->addMetaDescription($seoDescriptions[$randKey]);
		
		// satellite
		$satellite = $this->catalog()->satellite()->enumSatelliteGoods($itemId, $cityId, $priceCategoryId);
		
		$satelliteGroups = [];
		$satelliteGoodIds = [];
		foreach ($satellite as $row) {
			$groupId = intval($row['GroupID']);
			if (!isset($satelliteGroups[$groupId])) {
				$satelliteGroups[$groupId] = [
					'id' => $groupId,
					'name' => $row['GroupName'],
					'goods' => [] 
				];
			}
			if (48 >= count($satelliteGroups[$groupId]['goods'])) {
				$satelliteGroups[$groupId]['goods'][$row['GoodID']] = $row;
				$satelliteGoodIds[] = $row['GoodID'];
			}
		}
		$vars['satelliteGroups'] = $satelliteGroups;
		
		$satelliteGoodsImages = [];
		if (0 < count($satelliteGoodIds)) {
			$satelliteGoodsImages = $this->catalog()->images()->getImagesByGoodIds($satelliteGoodIds);
		}
		$vars['satelliteGoodsImages'] = $satelliteGoodsImages;
		
		$satelliteGoodsRemainsInTheMainStore = [];
		if (count($satelliteGoodIds)) {
			$satelliteGoodsRemainsInTheMainStore = $this->catalog()->goods()->getGoodsRemainsByStoreId($satelliteGoodIds, 1);
		}
		$vars['satelliteGoodsRemainsInTheMainStore'] = $satelliteGoodsRemainsInTheMainStore;
		// END satellite
		
		if ($item->IsPackage && !$item->IsVirtualPackage && (2 == $item->ViewMode)) {
			$components = $this->catalog()->goods()->getPackageComponents($itemId, $cityId, $priceCategoryId);
			$componentIds = [];
			
			$groups = $this->catalog()->goods()->getPackageGroups($itemId);
			
			$places = [];
			
			$packageGroups = [];
			foreach ($groups as $group) {
				$packageGroup = new PackageGroup();
				$selectMode = $packageGroup::SELECT_MODE_AND;
				if (1 == $group['PickingMethodID']) {
					$selectMode = $packageGroup::SELECT_MODE_OR;
				}
				$packageGroup->setSelectMode($selectMode);
				$packageGroup->isMain(1 == $group['IsMain']);
				$packageGroup->isRequired(1 == $group['IsMandatory']);
				
				foreach ($components as $groupId => $items) {
					if ($group['GroupID'] == $groupId) {
						foreach ($items as $component) {
							$packageGroupItem = new PackageGroupItem();
							$packageGroupItem->setId($component['GoodID']);
							$packageGroupItem->setName($component['GoodName']);
							$packageGroupItem->setUrl(sprintf('/%s/%s_%s/', $component['CategoryUID'], $component['GoodID'], $component['GoodEngName']));
							$packageGroupItem->setGroupId($component['GroupID']);
							$packageGroupItem->isDefault(1 == $component['IsDefault']);
							$packageGroupItem->setAvailQuantity($component['AvailQuantity']);
							$packageGroupItem->setWindowQuantity($component['WindowQuantity']);
							$packageGroupItem->setArrivalDate($component['ArrivalDate']);
							$packageGroupItem->setPickupDate(\DateTime::createFromFormat('Y-m-d H:i:s', $component['PickupDate']));
							$packageGroupItem->setDeliveryDate(\DateTime::createFromFormat('Y-m-d H:i:s', $component['DeliveryDate']));
							
							if ($component['PlaceRow'] || $component['PlaceCell'] || $component['PlaceRack']) {
								$places[$component['GoodID']] = [
									'row' => $component['PlaceRow'],
									'cell' => $component['PlaceCell'],
									'rack' => $component['PlaceRack'] 
								];
							}
							if (isset($places[$component['GoodID']])) {
								$packageGroupItem->setPlace($places[$component['GoodID']]);
							}
							
							$packageGroup->addItem($packageGroupItem);
						}
					}
				}
				
				$packageGroup->selectDefaultItems();
				if (0 < $group['cntDefaultItems']) {
					$packageGroup->selectFirstItem();
				}
				if (false && !$packageGroup->hasSelectedItem() && $packageGroup->hasDefaultItems()) {
					$packageGroup->selectFirstItem();
				}
				$packageGroup->moveSelectedToTheTop();
				$packageGroups[$group['GroupID']] = $packageGroup;
				
				// component place
				if (empty($place) && (0 < count($components))) {
					foreach ($components as $groupId => $items) {
						foreach ($items as $component) {
							if (empty($place) && (1 == $component['IsDefault']) && ($component['PlaceRow'] || $component['PlaceCell'] || $component['PlaceRack'])) {
								$place = [
								'row' => $component['PlaceRow'],
								'cell' => $component['PlaceCell'],
								'rack' => $component['PlaceRack']
								];
							}
						}
					}
				}
				if (!empty($place)) {
					$vars['place'] = $place;
				}
			}
			
			$componentsCount = 0;
			$selectedComponentsCount = 0;
			foreach ($components as $groupId => $items) {
				if ($groups[$groupId]['PickingMethodID'] == 1) {
					$componentsCount++;
				} else {
					$componentsCount += count($items);
				}
				
				foreach ($items as $component) {
					$selectedComponentsCount += $component['IsDefault'] ? 1 : 0;
					$componentIds[] = $component['GoodID'];
				}
			}
			
			foreach ($components as $groupId => $items) {
				$hasDefault = false;
				foreach ($items as $component) {
					if ($component['IsDefault']) {
						$hasDefault = true;
					}
				}
				if (false && !$hasDefault) {
					$components[$groupId][0]['IsDefault'] = 1;
				}
			}
			
			// check components
			foreach ($components as $groupId => &$items) {
				$hasDefault = false;
				foreach ($items as &$component) {
					if ($component['IsDefault']) {
						$hasDefault = true;
						$component['IsChecked'] = true;
					}
				}
				if (false && !$hasDefault) {
					$components[$groupId][0]['IsChecked'] = true;
				}
			}
			
			$propsFromQuery = [];
			if (isset($_SERVER['HTTP_REFERER'])) {
				$referer = new Uri($_SERVER['HTTP_REFERER']);
				$refererQuery = $referer->getQueryAsArray();
				if (isset($refererQuery['f'])) {
					foreach (explode(';', $refererQuery['f']) as $row) {
						list($propId, $values) = explode(':', $row);
						$propsFromQuery[$propId] = explode(',', $values);
					}
				}
			} elseif ($this->params()->fromQuery('f')) {
				foreach (explode(';', $this->params()->fromQuery('f')) as $row) {
					list($propId, $values) = explode(':', $row);
					$propsFromQuery[$propId] = explode(',', $values);
				}
			}
			
			if (0 < count($propsFromQuery)) {
				$selectedProperties = $propsFromQuery;
				$checked = [];
				foreach ($components as $groupId => &$items) {
					foreach ($items as &$item) {
						$productProperties = $this->catalog()->goods()->getGoodDescription($item['GoodID']);
						$isChecked = false;
						foreach ($selectedProperties as $propId => $values) {
							if (isset($productProperties[$propId]) && in_array($productProperties[$propId]->valueId, $values)) {
								$isChecked = true;
							}
						}
						if ($isChecked) {
							$checked[$groupId] = $item['GoodID'];
						}
					}
				}
				if (0 < count($checked)) {
					foreach ($components as $groupId => &$items) {
						foreach ($items as &$item) {
							if (isset($checked[$groupId])) {
								$item['IsChecked'] = (isset($checked[$groupId]) && ($item['GoodID'] == $checked[$groupId]));
							}
						}
					}
				}
				
				foreach ($checked as $groupId => $productId) {
					if (isset($packageGroups[$groupId])) {
						$packageGroups[$groupId]->selectItem($productId);
					}
				}
			}
			
			$componentPrices = $this->catalog()->prices()->getPriceWithOldPriceByGoodIds($componentIds, $cityId, $priceCategoryId);
			
			// @todo Find a better way
			$componentImages = [];
			foreach ($componentIds as $componentId) {
				$componentImages[$componentId] = $this->catalog()->images()->getGoodMainImage($componentId);
			}
			
			$anotherPackages = $this->catalog()->goods()->getAnotherPackages($itemId, $cityId, $priceCategoryId);
			
			if (count($anotherPackages)) {
				$anotherPackagesImages = $this->catalog()->images()->getGoodsMainImage(array_keys($anotherPackages));
			}
			
			$newComponents = [];
			foreach ($groups as $group) {
				$groupId = $group['GroupID'];
				if (isset($components[$groupId])) {
					$newComponents[$groupId] = $components[$groupId];
				}
			}
			$components = $newComponents;
			
			$properties = [];
			$selectableProperties = [];
			foreach ($components as $groupId => $items) {
				foreach ($items as $comp) {
					if ($comp['IsDefault']) {
						$productId = $comp['GoodID'];
						if ($comp['IsPackage'] && !$comp['IsVirtualPackage'] && (1 == $comp['ViewMode'])) {
							$subcomponents = $this->catalog()->goods()->getPackageComponentsIds($comp['GoodID']);
							$productProperties = [];
							foreach ($subcomponents as $compProductId) {
								$productProperties = array_merge($productProperties, $this->catalog()->goods()->getGoodDescription($compProductId));
							}
						} else {
							$productProperties = $this->catalog()->goods()->getGoodDescription($productId);
						}
						$productSelectableProperties = $this->catalog()->goods()->getGoodSelectableProperties($productId, $productProperties);
						
						$properties[$productId] = $productProperties;
						$selectableProperties[$productId] = $productSelectableProperties;
					}
				}
			}
			
			$productsNotes = [];
			foreach ($components as $groupId => $items) {
				foreach ($items as $comp) {
					if ($comp['IsDefault']) {
						$productId = $comp['GoodID'];
						$notes = $this->catalog()->goods()->getGoodNotesByGoodId($productId);
						$productsNotes[$productId] = $notes;
					}
				}
			}
			
			$vars = array_merge(
				$vars, 
				[
					'packageGroups' => $packageGroups,
					'components' => $components,
					'componentsCount' => $componentsCount,
					'selectedComponentsCount' => $selectedComponentsCount,
					'componentPrices' => $componentPrices,
					'componentImages' => $componentImages,
					'anotherPackages' => $anotherPackages,
					'anotherPackagesImages' => $anotherPackagesImages,
					'groups' => $groups,
					'properties' => $properties,
					'selectableProperties' => $selectableProperties,
					'productsNotes' => $productsNotes 
				]);
			
			// ecommerce tracker
			$ecommerceTracker = $this->createEcommerceTracker('product');
			$ecommerceTracker->addAttribute('product-id', $item['GoodID']);
			$ecommerceTracker->addAttribute('product-name', $item['GoodName']);
			$ecommerceTracker->addAttribute('product-brand', $item['BrandName']);
			$ecommerceTracker->addAttribute('product-price', $price->value);
			$ecommerceTracker->addAttribute('product-category', $itemCategory['CategoryName']);
			$position = 1;
			
			foreach ($anotherPackages as $item) {
				$impression = new TrackerImpression();
				$impression->setId($item['GoodID']);
				$impression->setName($item['GoodName']);
				$impression->setCategoryName($item['CategoryName']);
				$impression->setPrice(intval($item['Value']));
				$impression->setBrandName($item['BrandName']);
				$impression->setListName('SuitableGoods');
				$impression->setPosition($position);
				$position++;
				
				$ecommerceTracker->addImpression($impression);
			}
			
			$vars['ecommerceTracker'] = $ecommerceTracker;
			// END ecommerce tracker
			
			$this->getLayout()->setBodyClass('complect');
			
			return $this->outputVars($vars)->setTemplate('application/item/complement');
		} else {
			$similarGoods = $this->catalog()->goods()->enumSimilarGoods($itemId, $cityId, $priceCategoryId, 100);
			// @todo kick some ass
			// $similarGoodIds = $this->catalog()->goods()->enumSimilarGoodIds($itemId, $cityId, $priceCategoryId, 100);
			$similarGoodIds = array_keys($similarGoods);
			$similarGoodsImages = [];
			if (0 < count($similarGoodIds)) {
				$similarGoodsImages = $this->catalog()->images()->getImagesByGoodIds($similarGoodIds);
			}
			
			$notSimilarGoods = $this->catalog()->goods()->enumNotSimilarGoods($itemId, $cityId, $priceCategoryId, 100);
			// @todo kick some ass
			// $notSimilarGoodIds = $this->catalog()->goods()->enumNotSimilarGoodIds($itemId, $cityId, $priceCategoryId, 100);
			$notSimilarGoodIds = array_keys($notSimilarGoods);
			$notSimilarGoodImages = [];
			if (0 < count($notSimilarGoodIds)) {
				$notSimilarGoodImages = $this->catalog()->images()->getImagesByGoodIds($notSimilarGoodIds);
			}
			
			$goodNotes = $this->catalog()->goods()->getGoodNotesByGoodId($itemId);
			
			$files = $this->catalog()->files()->getFilesByGoodId($itemId);
			
			$shortDescription = mb_substr($item->GoodDescription, 0, mb_strpos($item->GoodDescription, '<cut>'));
			$description = mb_substr($item->GoodDescription, mb_strpos($item->GoodDescription, '<cut>'));
			
			$packages = iterator_to_array($this->catalog()->goods()->getPackagesByComponentId($itemId, $cityId, $priceCategoryId));
			$packageIds = [];
			foreach ($packages as $package) {
				$packageIds[] = $package['GoodID'];
			}
			$packagesImages = $this->catalog()->images()->getImagesByGoodIds($packageIds);
			
			if (count($similarGoodIds)) {
				$similarGoodsRemainsInTheMainStore = $this->catalog()->goods()->getGoodsRemainsByStoreId($similarGoodIds, 1);
			}
			
			if (count($notSimilarGoodIds)) {
				$notSimilarGoodsRemainsInTheMainStore = $this->catalog()->goods()->getGoodsRemainsByStoreId($notSimilarGoodIds, 1);
			}
			
			// properties like a package
			if ($item->IsPackage && (1 == $item->ViewMode)) {
				$components = $this->catalog()->goods()->getPackageComponentsIds($itemId);
				
				$properties = [];
				foreach ($components as $productId) {
					$productProperties = $this->catalog()->goods()->getGoodDescription($productId);
					
					$properties = array_merge($properties, $productProperties);
				}
				
				$vars['properties'] = $properties;
			}
			// END properties like a package
			
			$vars = array_merge(
				$vars, 
				[
					'shortDescription' => $shortDescription,
					'description' => $description,
					'similarGoods' => $similarGoods,
					'notSimilarGoods' => $notSimilarGoods,
					'goodNotes' => $goodNotes,
					'similarGoodsRemainsInTheMainStore' => $similarGoodsRemainsInTheMainStore,
					'similarGoodsImages' => $similarGoodsImages,
					'notSimilarGoodsRemainsInTheMainStore' => $notSimilarGoodsRemainsInTheMainStore,
					'notSimilarGoodImages' => $notSimilarGoodImages,
					'files' => $files,
					'packages' => $packages,
					'packagesImages' => $packagesImages 
				]);
			
			// ecommerce tracker
			$ecommerceTracker = $this->createEcommerceTracker('product');
			$ecommerceTracker->addAttribute('product-id', $item['GoodID']);
			$ecommerceTracker->addAttribute('product-name', $item['GoodName']);
			$ecommerceTracker->addAttribute('product-brand', $item['BrandName']);
			$ecommerceTracker->addAttribute('product-price', $price->value);
			$ecommerceTracker->addAttribute('product-category', $itemCategory['CategoryName']);
			
			$position = 1;
			foreach ($similarGoods as $item) {
				$impression = new TrackerImpression();
				$impression->setId($item->GoodID);
				$impression->setName($item->GoodName);
				$impression->setCategoryName($item->CategoryName);
				$impression->setPrice(intval($item->Value));
				$impression->setBrandName($item->BrandName);
				$impression->setListName('SimilarProduct');
				$impression->setPosition($position);
				$position++;
				
				$ecommerceTracker->addImpression($impression);
			}
			
			foreach ($notSimilarGoods as $item) {
				$impression = new TrackerImpression();
				$impression->setId($item->GoodID);
				$impression->setName($item->GoodName);
				$impression->setCategoryName($item->CategoryName);
				$impression->setPrice(intval($item->Value));
				$impression->setBrandName($item->BrandName);
				$impression->setListName('UnlikeGoods');
				$impression->setPosition($position);
				$position++;
				
				$ecommerceTracker->addImpression($impression);
			}
			
			$vars['ecommerceTracker'] = $ecommerceTracker;
			// END ecommerce tracker
			
			return $this->outputVars($vars);
		}
	}

	public function propertiesAction() {
		$items = $this->params()->fromPost('items');
		
		$productIds = $items;
		$productIds = array_unique($productIds);
		
		$goods = $this->catalog()->goods()->enumGoodsByIds($productIds);
		
		foreach ($productIds as $productId) {
			$componentImages[$productId] = $this->catalog()->images()->getGoodMainImage($productId);
		}
		
		$properties = [];
		$selectableProperties = [];
		foreach ($productIds as $productId) {
			$productProperties = $this->catalog()->goods()->getGoodDescription($productId);
			if (0 == count($productProperties)) {
				$subcomponents = $this->catalog()->goods()->getPackageComponentsIds($productId);
				$productProperties = [];
				foreach ($subcomponents as $compProductId) {
					$productProperties = array_merge($productProperties, $this->catalog()->goods()->getGoodDescription($compProductId));
				}
			}
			$productSelectableProperties = $this->catalog()->goods()->getGoodSelectableProperties($productId, $productProperties);
			
			$properties[$productId] = $productProperties;
			$selectableProperties[$productId] = $productSelectableProperties;
		}
		
		$productsNotes = [];
		foreach ($productIds as $productId) {
			$notes = $this->catalog()->goods()->getGoodNotesByGoodId($productId);
			$productsNotes[$productId] = $notes;
		}
		
		$vars = [
			'productIds' => $productIds,
			'goods' => $goods,
			'componentImages' => $componentImages,
			'properties' => $properties,
			'selectableProperties' => $selectableProperties,
			'productsNotes' => $productsNotes 
		];
		
		$out = [
			'state' => 'ok',
			'characteristics' => $this->renderer()->render('item/properties.tpl', $vars) 
		];
		return $this->getResponse()->setContent(json_encode($out, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
		
		return $this->outputVars($vars);
	}

}