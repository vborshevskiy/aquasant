<?php

namespace Application\Controller;

use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use Zend\View\Model\JsonModel;
use Zend\Validator;

class ContactsController extends BaseController {

    public function indexAction() {
        $this->getLayout()->setBodyClass('contacts');
        $this->getLayout()->addCssFile('contacts.css');
        $this->getLayout()->addJsFile('contacts.js');

        $this->getLayout()->setTitle('контакты Аквасант, телефон и адрес оптово-розничной база сантехники «Аквасант», Aquasant в Москве');
        $this->getLayout()->addMetaKeywords('контакты, телефон, адрес, база, Аквасант, Aquasant, оптово-розничная, сантехники');
        $this->getLayout()->addMetaDescription('Контактная информация оптово-розничной базы сантехники «Аквасант», Aquasant — вся сантехника из одного места');

        return $this->outputVars([]);
    }

    public function mailAction() {
        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest()) {
            return $this->notfound();
        }

        $email = $request->getQuery('email');
        $message = $request->getQuery('message');
        $userName = $request->getQuery('username');

        $emailInput = new Input('email');
        $emailInput->getValidatorChain()
            ->attach(new Validator\EmailAddress());

        $userNameInput = new Input('username');
        $userNameInput->getValidatorChain()
            ->attach(new Validator\NotEmpty());

        $messageInput = new Input('message');
        $messageInput->getValidatorChain()
            ->attach(new Validator\NotEmpty());

        $inputFilter = new InputFilter();
        $inputFilter->add($emailInput)
            ->add($userNameInput)
            ->add($messageInput)
            ->setData(['email' => $email, 'username' => $userName, 'message' => $message]);

        if (!$inputFilter->isValid()) {
            return new JsonModel([
                'state' => 'error',
                'error' => 'Не все поля заполены правильно'
            ]);
        }

        $recipientId = $request->getQuery('sendTo');

        $emails = $this->service('Config')['feedback_emails'];

        $recipient = (isset($emails[$recipientId]) ? $emails[$recipientId] : $emails['questions']);

        if (!$recipient) {
            return new JsonModel([
                'state' => 'error',
                'error' => 'Не получилось отправить письмо.'
            ]);
        }

        $this->notify()->send($message, 'Сообщение с сайта', $recipient);

        return new JsonModel([
            'state' => 'ok'
        ]);
    }
}