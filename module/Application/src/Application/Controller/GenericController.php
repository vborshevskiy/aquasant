<?php

namespace Application\Controller;

use Solo\Db\QueryGateway\QueryGateway;

class GenericController extends BaseController {

	public function indexAction() {
		$this->checkUrlEnding('?', '?/');
		$search = $this->params('search');
		
		$categoryUid = $this->getCategoryFromRoute($search);
		$brandId = $this->getBrandIdFromRoute($search);
		$sets = $this->getSetsFromRoute($search);
		$pageNumber = $this->getPageNumberFromRoute($search);
		$category = $this->catalog()->categories()->getCategoryByUid($categoryUid);
		
		if (null !== $category) {
            return $this->forward()->dispatch(CatalogController::class, array(
                'action' => 'category',
                'id' => $category['SoftCategoryID'],
                'page' => $pageNumber,
                'brandId' => $brandId,
            	'sets' => $sets
            ))
			;
		}
		
		return $this->notfound();
	}

	private function checkUrlEnding() {
		$wrongUrlEndings = func_get_args();
		$uri = $_SERVER['REQUEST_URI'];
		foreach ($wrongUrlEndings as $wrongUrlEnding) {
			if (mb_substr($uri, mb_strlen($uri) - mb_strlen($wrongUrlEnding)) == $wrongUrlEnding) {
				$correctUrl = mb_substr($uri, 0, mb_strlen($uri) - mb_strlen($wrongUrlEnding));
				if (mb_substr($correctUrl, mb_strlen($correctUrl) - 1) != '/') {
					$correctUrl .= '/';
				}
				$response = $this->getResponse();
				$response->getHeaders()->addHeaderLine('Location', $correctUrl);
				$response->setStatusCode(301);
				return $response;
			}
		}
	}

	public function whyAction() {
		error_reporting(E_ALL);
		ini_set('display_errors', 1);
		
		$search = intval(trim((isset($_GET['search']) ? $_GET['search'] : '')));
		
		print '<form action="/why/" method="get">';
		print '<input type="text" name="search" value="' . ($search ?  : '') . '" placeholder="артикул товара" autocomplete="off" />&nbsp;';
		print '<input type="submit" value="найти товар" />';
		print '</form>';
		
		if (0 < $search) {
			$errors = [];
			$queryGateway = new QueryGateway();
			
			// card
			$productId = 0;
			$sql = "SELECT * FROM #all_goods# alg WHERE GoodID = {$search} LIMIT 1";
			$rows = $queryGateway->query($sql);
			if (0 == $rows->count()) {
				$errors[] = 'not_found';
			} else {
				$product = $rows->current();
				
				$isPackage = ($product['IsPackage'] && !$product['IsVirtualPackage']);
				$isVirtualPackage = ($product['IsPackage'] && $product['IsVirtualPackage']);
				$isPackageLikeGood = (1 == $product['ViewMode']);
				$productId = $product['GoodID'];
				
				// avail
				$availQuantity = 0;
				$sql = "SELECT * FROM #goods_remains# WHERE GoodID = {$productId} AND Remains > 0";
				$rows = $queryGateway->query($sql);
				foreach ($rows as $row) {
					$remains = $row->Remains - $row->Reserve;
					if (0 > $remains) {
						$remains = 0;
					}
					$availQuantity += $remains;
				}
				$arrivalQuantity = 0;
				$sql = "SELECT * FROM #arrival_goods# WHERE GoodID = {$productId} AND SuborderQuantity > 0";
				$rows = $queryGateway->query($sql);
				foreach ($rows as $row) {
					$remains = $row->SuborderQuantity;
					if (0 > $remains) {
						$remains = 0;
					}
					$arrivalQuantity += $remains;
				}
				if ((0 == $availQuantity) && (0 == $arrivalQuantity)) {
					$errors[] = 'no_avail_on_main_components';
				}
				
				// prices
				$sql = "SELECT * FROM #goods_prices# WHERE GoodID = {$productId}";
				$rows = $queryGateway->query($sql);
				if (0 == $rows->count()) {
					$errors[] = 'no_price';
				}
				
				// images
				$sql = "SELECT * FROM #goods_images# WHERE GoodID = {$productId}";
				$rows = $queryGateway->query($sql);
				if (0 == $rows->count()) {
					$errors[] = 'no_photos';
				}
				
				// links to categories
				$siteCategoryId = 0;
				$siteCategoryName = '';
				$sql = "SELECT
						sc.SoftCategoryID,
						sc.CategoryName
					FROM #goods_to_soft_categories# gtsc
					INNER JOIN #soft_categories# sc
						ON gtsc.SoftCategoryID = sc.SoftCategoryID
					WHERE
						gtsc.GoodID = {$productId}";
				$rows = $queryGateway->query($sql);
				if (0 < $rows->count()) {
					$row = $rows->current();
					$siteCategoryId = intval($row->SoftCategoryID);
					$siteCategoryName = strval($row->CategoryName);
				}
				
				// properties
				if (!$isPackage) {
					$sql = "SELECT * FROM #goods_to_propvalues# gtp WHERE gtp.GoodID = {$productId}";
					$rows = $queryGateway->query($sql);
					if (0 == $rows->count()) {
						$errors[] = 'no_propvalues';
					}
				}
				
				// no arrival dates / or incorrect
				/**
				 * $sql = "SELECT
				 * arg.*
				 * FROM #arrival_goods# arg
				 * WHERE
				 * (arg.ArrivalDate IS NULL || YEAR(arg.ArrivalDate) = '2037')
				 * && arg.GoodID = {$productId}";
				 * $rows = $queryGateway->query($sql);
				 * if (0 < $rows->count()) {
				 * $errors[] = 'no_arrival_date';
				 * }
				 */
				
				// no pickup dates / or null
				$sql = "SELECT
							grad.*
						FROM #good_reserve_avail_date# grad
						WHERE
							(grad.PickupDate IS NOT NULL || grad.DeliveryDate IS NOT NULL || grad.ShowcaseRetrieveDate IS NOT NULL)
							&& grad.GoodID = {$productId}";
				$rows = $queryGateway->query($sql);
				if (0 == $rows->count()) {
					$errors[] = 'no_reserve_avail_date';
				}
				
				// complect / not avail components
				if ($isPackage && !$isVirtualPackage && !$isPackageLikeGood) {
					$sql = "SELECT
								ptg.PackageID
							FROM #goods_to_packages# gtp
								INNER JOIN #avail_goods# ag
									ON gtp.GoodID = ag.GoodID
								INNER JOIN #packages_to_groups# ptg
									ON gtp.PackageID = ptg.PackageID
									AND gtp.GroupID = ptg.GroupID
									AND ptg.IsMain = 1
    						WHERE gtp.PackageID = {$productId}";
					$rows = $queryGateway->query($sql);
					if (0 == $rows->count()) {
						$errors[] = 'no_require_accessories';
					}
				}
				
				// complect / no linked components
				if ($isPackage && !$isVirtualPackage && !$isPackageLikeGood) {
					$sql = "SELECT
								gtp.PackageID
							FROM #goods_to_packages# gtp
								INNER JOIN #all_goods# alg
									ON gtp.PackageID = alg.GoodID
									AND alg.IsPackage = 1
									AND alg.IsVirtualPackage = 0
								LEFT OUTER JOIN #avail_goods# ag
									ON gtp.GoodID = ag.GoodID
							WHERE gtp.PackageID = {$productId}
							GROUP BY gtp.PackageID
							HAVING 1 < COUNT(ag.GoodID)";
					$rows = $queryGateway->query($sql);
					if (0 == $rows->count()) {
						$errors[] = 'no_accessories';
					}
				}
			}
			
			if (0 < count($errors)) {
				
				if (in_array('not_found', $errors)) {
					$errors[] = 'Не найдена карточка товара с указанным id/артикулом/кодом';
					print '<div style="color: red">Не найдена карточка товара с указанным id/артикулом/кодом</div>';
				} else {
					
					$item = [
						'Артикул' => [
							'value' => '<a href="/goods/' . $productId . '/" target="_blank">' . $productId . '</a>',
							'status' => true 
						],
						'Название товара' => [
							'value' => $product['GoodName'],
							'status' => strlen($product['GoodName']) 
						],
						'ID категории сайта' => [
							'value' => $siteCategoryId ?  : 'Нет привязки к категории на сайте',
							'status' => $siteCategoryId 
						],
						'Название категории сайта' => [
							'value' => $siteCategoryName ? $siteCategoryName : 'Нет привязки к категории на сайте',
							'status' => $siteCategoryId 
						],
						'ID бренда' => [
							'value' => $product['BrandID'] ?  : 'нет',
							'status' => $product['BrandID'] 
						],
						'ID шаблона характеристик' => [
							'value' => '-',
							'status' => true 
						],
						'ID категории в ERP' => [
							'value' => $product['CategoryID'] ?  : 'нет',
							'status' => $product['CategoryID'] 
						],
						'Цена' => [
							'value' => !in_array('no_price', $errors) ? 'есть' : 'нет',
							'status' => !in_array('no_price', $errors) 
						],
						'Наличие' => [
							'value' => !in_array('no_avail_on_main_components', $errors) ? 'есть' : 'нет',
							'status' => !in_array('no_avail_on_main_components', $errors) 
						],
						'Фото' => [
							'value' => !in_array('no_photos', $errors) ? 'есть' : 'нет',
							'status' => !in_array('no_photos', $errors) 
						],
						'Комплект' => [
							'value' => $isPackage ? 'да' : 'нет',
							'status' => true 
						],
						'Показывать как товар' => [
							'value' => $isPackageLikeGood ? 'да' : 'нет',
							'status' => true 
						],
						'Виртуальный комплект' => [
							'value' => $isVirtualPackage ? 'да' : 'нет',
							'status' => true 
						] 
					];
					
					// no_reserve_avail_date
					if (!$isPackage && in_array('no_reserve_avail_date', $errors)) {
						$item['Дата поставки (PickupDate или DeliveryDate)'] = [
							'value' => 'некорректна или отсутствует',
							'status' => false 
						];
					}
					
					// properties
					if (!$isPackage && in_array('no_propvalues', $errors)) {
						$item['Характеристики товара'] = [
							'value' => 'нет',
							'status' => false 
						];
					}
					
					// require accessories
					if ($isPackage && in_array('no_require_accessories', $errors)) {
						$item['Наличие обязательных комплектующих'] = [
							'value' => 'нет',
							'status' => false 
						];
					}
					
					// require accessories
					if ($isPackage && in_array('no_accessories', $errors)) {
						$item['Наличие комплектующих'] = [
							'value' => 'нет',
							'status' => false 
						];
					}
					
					print '<div>';
					foreach ($item as $prop => $data) {
						print '<span style="color: ' . ($data['status'] ? 'green' : 'red') . '">';
						print $prop . ': ' . $data['value'];
						print '</span><br />';
					}
					print '</div>';
				}
			} else {
				$sql = "SELECT DISTINCT
    						alg.*, sc.CategoryName, sc.CategoryUID, ag.GoodName
    					FROM #all_goods# ag 
    						INNER JOIN #goods_to_soft_categories# gtsc
	    						ON ag.GoodID = gtsc.GoodID
    						INNER JOIN #soft_categories# sc
    							ON gtsc.SoftCategoryID = sc.SoftCategoryID
    						LEFT JOIN #avail_goods# alg
    							ON alg.GoodID = ag.GoodID
    					WHERE
    						alg.GoodID = {$productId}
    					LIMIT 1";
				$rows = $queryGateway->query($sql);
				if (0 < count($rows)) {
					$row = $rows->current();
					
					print '<div style="color: green">';
					print 'Товар должен показываться в каталоге:<br />';
					print 'Категория: <a href="/' . $row['CategoryUID'] . '/" target="_blank">' . $row['CategoryName'] . '</a><br />';
					print 'Товар: <a href="/goods/' . $row['GoodID'] . '_/" target="_blank">' . $row['GoodName'] . '</a>';
					print '</div>';
				}
			}
		}
		
		exit();
	}

}
