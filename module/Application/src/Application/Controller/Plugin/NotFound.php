<?php

namespace Application\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\View\Model\ViewModel;

class NotFound extends AbstractPlugin {

	public function __invoke() {
	    $response = $this->getController()->getResponse();
	    $response->setStatusCode(404);
	    $viewModel = new ViewModel();

	    return $viewModel;
	}

}

?>