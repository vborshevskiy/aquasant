<?php

namespace Application\Controller\Plugin;

use Zend\View\Model\ViewModel;
use Zend\View\Renderer\RendererInterface;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class Renderer extends AbstractPlugin {

	/**
	 *
	 * @var RendererInterface
	 */
	private $engine = null;

	/**
	 *
	 * @var string
	 */
	private $templateFolder = null;

	/**
	 *
	 * @param string $template        	
	 * @param array $vars        	
	 * @return string
	 */
	public function render($template, array $vars = []) {
		$vm = new ViewModel();
		$vm->setVariables($vars);
		$vm->setTemplate($this->getTemplateFolder() . '/' . $template);
		
		return $this->getEngine()->render($vm);
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return \Zend\View\Renderer\RendererInterface
	 */
	private function getEngine() {
		if (null === $this->engine) {
			$renderer = $this->getController()->getServiceLocator()->get('Smarty\View\Renderer\SmartyRenderer');
			if ($renderer instanceof RendererInterface) {
				$this->engine = $renderer;
			} else {
				throw new \RuntimeException(sprintf('Invalid renderer engine %s', get_class($renderer)));
			}
		}
		return $this->engine;
	}

	/**
	 *
	 * @throws \RuntimeException
	 * @return string
	 */
	private function getTemplateFolder() {
		if (null === $this->templateFolder) {
			$this->templateFolder = __DIR__ . '/../../../../../../templates/application';
		}
		return $this->templateFolder;
	}

}

?>