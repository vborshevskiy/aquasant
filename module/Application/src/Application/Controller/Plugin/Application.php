<?php

namespace Application\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Http\Header\UserAgent;
use SoloIdentity\Entity\Ultima\Agent;

class Application extends AbstractPlugin {

	/**
	 *
	 * @var array
	 */
	private $cache = [];

	/**
	 *
	 * @return boolean
	 */
	public function isMobileDevice() {
		if (isset($this->cache[__FUNCTION__])) {
			return $this->cache[__FUNCTION__];
		}
		
		$httpRequest = $this->getController()->getRequest();
		
		$isMobileDevice = false;
		if ($httpRequest) {
			$userAgent = $httpRequest->getHeader('user-agent');
			if ($userAgent instanceof UserAgent) {
				$userAgentStr = $userAgent->toString();
				$userAgents = [
					'midp',
					'samsung',
					'nokia',
					'j2me',
					'avant',
					'docomo',
					'novarra',
					'palmos',
					'palmsource',
					'opwv',
					'chtml',
					'pda',
					'mmp',
					'blackberry',
					'mib',
					'symbian',
					'wireless',
					'nokia',
					'hand',
					'mobi',
					'phone',
					'cdm',
					'upb',
					'audio',
					'SIE',
					'SEC',
					'samsung',
					'HTC',
					'mot-',
					'mitsu',
					'sagem',
					'sony',
					'alcatel',
					'lg',
					'eric',
					'vx',
					'NEC',
					'philips',
					'mmm',
					'xx',
					'panasonic',
					'sharp',
					'wap',
					'sch',
					'rover',
					'pocket',
					'benq',
					'java',
					'pt',
					'pg',
					'vox',
					'amoi',
					'bird',
					'compal',
					'kg',
					'voda',
					'sany',
					'kdd',
					'dbt',
					'sendo',
					'sgh',
					'gradi',
					'jb',
					'dddi',
					'moto',
					'iphone',
					'android' 
				];
				foreach ($userAgents as $search) {
					if (false !== stripos($userAgentStr, $search)) {
						$isMobileDevice = true;
						break;
					}
				}
			}
		}
		$this->cache[__FUNCTION__] = $isMobileDevice;
		return $isMobileDevice;
	}

	/**
	 *
	 * @return boolean
	 */
	public function isMobileIosDevice() {
		if (isset($this->cache[__FUNCTION__])) {
			return $this->cache[__FUNCTION__];
		}
		
		$httpRequest = $this->getController()->getRequest();
		$isMobileDevice = false;
		if ($httpRequest) {
			$userAgent = $httpRequest->getHeader('user-agent');
			if ($userAgent instanceof UserAgent) {
				$userAgentStr = $userAgent->toString();
				$userAgents = [
					'iphone' 
				];
				foreach ($userAgents as $search) {
					if (false !== stripos($userAgentStr, $search)) {
						$isMobileDevice = true;
						break;
					}
				}
			}
		}
		$this->cache[__FUNCTION__] = $isMobileDevice;
		return $isMobileDevice;
	}

	/**
	 *
	 * @return boolean
	 */
	public function isIpadDevice() {
		if (isset($this->cache[__FUNCTION__])) {
			return $this->cache[__FUNCTION__];
		}
		
		$httpRequest = $this->getController()->getRequest();
		$isMobileDevice = false;
		if ($httpRequest) {
			$userAgent = $httpRequest->getHeader('user-agent');
			if ($userAgent instanceof UserAgent) {
				$userAgentStr = $userAgent->toString();
				$userAgents = [
					'ipad' 
				];
				foreach ($userAgents as $search) {
					if (false !== stripos($userAgentStr, $search)) {
						$isMobileDevice = true;
						break;
					}
				}
			}
		}
		$this->cache[__FUNCTION__] = $isMobileDevice;
		return $isMobileDevice;
	}

	/**
	 *
	 * @param string $inn        	
	 * @return array
	 */
	public function getOrganizationByInn($inn) {
		$result = [];
		
		$ch = curl_init('https://suggestions.dadata.ru/suggestions/api/4_1/rs/findById/party');
		curl_setopt_array($ch, [
			CURLOPT_TIMEOUT => 2,
			CURLOPT_SSL_VERIFYPEER => 0,
			CURLOPT_FOLLOWLOCATION => 0,
			CURLOPT_FAILONERROR => 1,
			CURLOPT_RETURNTRANSFER => 1 
		]);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, '{ "query": "' . $inn . '" }');
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Accept: application/json',
			'Authorization: Token 0bb9876cbd5d802426809feafddb7d41d173fbff' 
		));
		$response = curl_exec($ch);
		curl_close($ch);
		
		if ($response) {
			$data = json_decode($response, true);
			if (is_array($data) && isset($data['suggestions']) && is_array($data['suggestions']) && (0 < count($data['suggestions']))) {
				$result = $data['suggestions'][0]['data'];
			}
		}
		
		return $result;
	}
	
	/**
	 * 
	 * @param Agent $agent
	 */
	public function updateAgentRequisites(Agent $agent) {
		if ($agent->requisites()->getInn()) {
			$data = $this->getOrganizationByInn($agent->requisites()->getInn());
			if (!empty($data)) {
				if (isset($data['opf']) && isset($data['opf']['short'])) {
					$agent->requisites()->setOpf($data['opf']['short']);
				}
				if (isset($data['kpp'])) {
					$agent->requisites()->setKpp($data['kpp']);
				}
				if (isset($data['ogrn'])) {
					$agent->requisites()->setOgrn($data['ogrn']);
				}
				if (isset($data['ogrn_date']) && is_numeric($data['ogrn_date'])) {
					$ogrnDate = \DateTime::createFromFormat('Y-m-d', date('Y-m-d', $data['ogrn_date']));
					$agent->requisites()->setOgrnDate($ogrnDate);
				}
				if (!$agent->requisites()->getOgrnDate()) {
					$ogrnDate = \DateTime::createFromFormat('Y-m-d', date('Y-m-d', $data['registration_date']));
					$agent->requisites()->setOgrnDate($ogrnDate);
				}
			}
		} 
	}

}

?>