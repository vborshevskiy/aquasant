<?php

namespace Application\Controller\Plugin;

use Application\Controller\GenericController;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\View\Model\ViewModel;

class Problem extends AbstractPlugin {

	public function __invoke() {
        $response = $this->getController()->getResponse();
        $response->setStatusCode(500);
        $viewModel = new ViewModel();

        $viewModel->setTemplate('error/500');
        return $viewModel;
	}

}

?>