<?php

namespace Application\Controller;

use SoloCabinet\Entity\Ultima\Reserve;
use SoloCatalog\Service\Helper\DateHelper;
use SoloIdentity\Entity\Ultima\Agent;
use Zend\View\Model\JsonModel;
use SoloERP\Service\ProvidesWebservice;
use SoloIdentity\Entity\Phone;
use Solo\DateTime\DateTime;
use SoloERP\WebService\Reader\UltimaJsonListReader;

class CabinetController extends BaseController {
	
	use ProvidesWebservice;

	public function indexAction() {
		$this->getLayout()->setTitle('Личный кабинет');
		$this->getLayout()->setBodyClass('cabinet cabinet-main');
		if (MANAGER_MODE) {
			$bodyClass = 'cabinet manager cabinet-main';
			if ($this->auth()->getUser()->isSuperManager()) {
				$bodyClass .= ' super';
			}
			$this->getLayout()->setBodyClass($bodyClass);
		}
		$this->getLayout()->addCssFile('cabinet.css');
		$this->getLayout()->addCssFile('cabinet.mobile.css');
		$this->getLayout()->addJsFile('cabinet.js');
		
		$user = $this->auth()->getUser();
		
		$result = $this->cabinet()->reserves()->getReserveStatuses();
		
		$reserveStatuses = [];
		$reservesCount = [];
		$totalReservesCount = 0;
		
		if (!$result->hasError()) {
			$reserveStatuses = $result->statuses;
		}
		
		$managers = [];
		if (MANAGER_MODE && $this->auth()->getUser()->isSuperManager()) {
			$managers = $this->getManagers();
		}
		
		$createdFrom = DateTime::now();
		$addDays = ((1 < $createdFrom->getWeekday()) ? (1 - $createdFrom->getWeekday()) : 0);
		$createdFrom->addDays($addDays);
		$createdTo = DateTime::now();
		
		$bannerService = $this->service('\Application\Service\BannerService');
		
		return $this->outputVars(
			[
				'agents' => $user->agents(),
				'reserveStatuses' => $reserveStatuses,
				'reservesCount' => $reservesCount,
				'totalReservesCount' => $totalReservesCount,
				'currentPage' => 'orders',
				'banner' => $bannerService->getRandomBanner(2),
				'createdFrom' => $createdFrom->format('d.m.y'),
				'createdTo' => $createdTo->format('d.m.y'),
				'managers' => $managers 
			]);
	}

	public function reservesAction() {
		$request = $this->getRequest();
		
		if (!$request->isXmlHttpRequest()) {
			// Freturn $this->notfound();
		}
		
		$action = $request->getQuery('action');
		if (!$action) {
			$action = $request->getPost('action');
		}
		
		switch ($action) {
			case 'get-list':
				$agentId = $request->getQuery('f-agent');
				$statusId = $request->getQuery('f-type');
				
				$result = [];
				if (MANAGER_MODE) {
					$createdFrom = null;
					$createdTo = null;
					if ($request->getQuery('f-dates')) {
						list($dateFrom, $dateTo) = explode(';', $request->getQuery('f-dates'));
						$createdFrom = DateTime::dayBegin(DateTime::createFromFormat('d.m.y', $dateFrom)->getTimestamp());
						$createdTo = DateTime::dayEnd(DateTime::createFromFormat('d.m.y', $dateTo)->getTimestamp());
					}
					
					$searchQuery = $request->getQuery('f-search');
					if (is_numeric(str_replace([
						'-',
						'(',
						')',
						' ' 
					], '', $searchQuery))) {
						$searchQuery = str_replace('-', '', $searchQuery);
					}
					if ((12 == strlen($searchQuery)) && ('+7' == substr($searchQuery, 0, 2)) && is_numeric(substr($searchQuery, 2))) {
						$searchQuery = substr($searchQuery, 2);
					}
					if ((11 == strlen($searchQuery)) && ('7' == substr($searchQuery, 0, 1)) && is_numeric(substr($searchQuery, 1))) {
						$searchQuery = substr($searchQuery, 1);
					}
					if ((11 == strlen($searchQuery)) && ('8' == substr($searchQuery, 0, 1)) && is_numeric(substr($searchQuery, 1))) {
						$searchQuery = substr($searchQuery, 1);
					}
					
					$managerId = $this->auth()->getUser()->getManagerId();
					if ($this->auth()->getUser()->isSuperManager()) {
						$managerId = null;
					}
					if ($request->getQuery('f-manager-id')) {
						$managerId = intval($request->getQuery('f-manager-id'));
					}
					$result = $this->getManagerReserveList($managerId, $statusId, $createdFrom, $createdTo, $searchQuery);
				} else {
					return $this->getReserveList($agentId, $statusId);
				}
				return $result;
				break;
			
			case 'get-order':
				$reserveId = $request->getQuery('id');

				$agentId = $request->getQuery('agent');
				$managerId = $request->getQuery('manager');
				return $this->getReserve($reserveId, $agentId, $managerId);
				break;

            case 'cancel':
                $out = [
                    'state' => false
                ];
                $reserveId = $request->getPost('id');

                $method = $this->getWebMethod('DeleteReserve');
                $method->addPar('Id', $reserveId);
                $response = $method->call();
                $data = $response->getData();

                if (isset($data->Success) && (1 == $data->Success)) {
                    $out['state'] = 'ok';
                } elseif ($response->getErrorMessage()) {
                    $out['message'] = '<h1>Ultima error</h1><p>' . $response->getErrorMessage() . '</p>';
                }
                return new JsonModel($out);
                break;
			
			case 'set-comment':
				$reserveId = $request->getPost('id');
				$comment = $request->getPost('comment');
				switch ($request->getPost('number')) {
					case 1:
						$result = $this->updateReserveDeliveryComment($reserveId, $comment);
						break;
					case 2:
						$result = $this->updateReserveOrderComment($reserveId, $comment);
						break;
				}
				$out = [
					'state' => 'failed' 
				];
				if ($result) {
					$out = [
						'state' => 'ok',
						'comment' => $comment 
					];
				}
				return new JsonModel($out);
				
				break;
			
			case 'check-failed':
				$out = [
					'state' => 'dontcheck' 
				];
				
				if (MANAGER_MODE) {
					$result = $this->cabinet()->reserves()->getManagerReservesCount($this->auth()->getUser()->getManagerId());
					
					if (!$result->hasError()) {
						foreach ($result->reservesCount as $item) {
							if (65636 == $item->StatusId) {
								$count = intval($item->Count);
								if (0 < $count) {
									$out['state'] = 'fail';
									$out['count'] = $count;
								} else {
									$out['state'] = 'ok';
								}
							}
						}
						if ('dontcheck' == $out['state']) {
							$out['state'] = 'ok';
						}
					}
				}
				return new JsonModel($out);
				break;
		}
	}

	/**
	 *
	 * @param integer $agentId        	
	 * @param integer $statusId        	
	 * @return \Zend\View\Model\JsonModel
	 */
	public function getReserveList($agentId, $statusId) {
		$result = $this->cabinet()->reserves()->getReserveStatuses();
		
		$reservesCount = [];
		$totalReservesCount = 0;
		$statuses = [];
		
		if (!$result->hasError()) {
			$reserveStatuses = $result->statuses;
			
			foreach ($reserveStatuses as $reserveStatus) {
				$reservesCount[$reserveStatus->Id] = 0;
				$statuses[$reserveStatus->Id] = $reserveStatus->Name;
			}
			
			$result = $this->cabinet()->reserves()->getReservesCount($agentId);
			
			if (!$result->hasError()) {
				foreach ($result->reservesCount as $item) {
					$reservesCount[$item->StatusId] = $item->Count;
					$totalReservesCount += $item->Count;
				}
			}
		}
		
		$allReserves = [];
		$allReservesResult = $this->cabinet()->allReserves()->enum($agentId, $statusId);
		
		$dateHelper = new DateHelper();
		
		if (isset($allReservesResult->items)) {
			$reserves = $allReservesResult->items;
			foreach ($reserves as $reserve) {
				$creationDate = DateHelper::parseJsonDate($reserve->getCreationDate());
				$dateText = $dateHelper->parseDate(DateHelper::DATE_FORMAT_WITH_YEAR, $creationDate->getTimestamp());
				
				$allReserves[] = [
					'id' => $reserve->getDocumentId(),
					'name' => 'Заказ ' . $reserve->getDocumentId(),
					'date' => $dateText,
					'dateDt' => $creationDate->getTimestamp(),
					'state' => isset($statuses[$reserve->getStatusId()]) ? $statuses[$reserve->getStatusId()] : '',
					'stateId' => $reserve->getStatusId(),
					'amount' => $reserve->getAmount(),
					'agent' => $agentId 
				];
			}
		}
		
		if (!$agentId) {
			$user = $this->auth()->getUser();
			
			foreach ($user->agents() as $agent) {
				$result = $this->cabinet()->reserves()->getReservesCount($agent->getId());
				
				if (!$result->hasError()) {
					foreach ($result->reservesCount as $item) {
						$reservesCount[$item->StatusId] += $item->Count;
						$totalReservesCount += $item->Count;
					}
				}
				
				$allReservesResult = $this->cabinet()->allReserves()->enum($agent->getId(), $statusId);
				if (isset($allReservesResult->items)) {
					foreach ($allReservesResult->items as $reserve) {
						$creationDate = DateHelper::parseJsonDate($reserve->getCreationDate());
						$dateText = $dateHelper->parseDate(DateHelper::DATE_FORMAT_WITH_YEAR, $creationDate->getTimestamp());
						
						$allReserves[] = [
							'id' => $reserve->getDocumentId(),
							'name' => 'Заказ ' . $reserve->getDocumentId(),
							'date' => $dateText,
							'dateDt' => $creationDate->getTimestamp(),
							'state' => isset($statuses[$reserve->getStatusId()]) ? $statuses[$reserve->getStatusId()] : '',
							'stateId' => $reserve->getStatusId(),
							'amount' => $reserve->getAmount(),
							'agent' => $agent->getId() 
						];
					}
				}
			}
		}
		
		usort(
			$allReserves, 
			function ($reserve1, $reserve2) {
				$date1 = $reserve1['dateDt'];
				$date2 = $reserve2['dateDt'];
				if ($date1 > $date2) {
					return -1;
				} elseif ($date1 < $date2) {
					return 1;
				} else {
					return 0;
				}
			});
		
		$reservesCount[0] = $totalReservesCount;
		
		$result = [
			'state' => 'ok',
			'orders' => $allReserves,
			'f-type' => $reservesCount 
		];
		
		return new JsonModel($result);
	}

	/**
	 *
	 * @param integer $managerId        	
	 * @param integer $statusId        	
	 * @return \Zend\View\Model\JsonModel
	 */
	public function getManagerReserveList($managerId, $statusId, DateTime $createdFrom = null, DateTime $createdTo = null, $searchQuery = null) {
		$result = $this->cabinet()->reserves()->getReserveStatuses();
		
		$managers = $this->getManagers();
		
		$reservesCount = [];
		$totalReservesCount = 0;
		$statuses = [];
		
		if (!$result->hasError()) {
			$reserveStatuses = $result->statuses;
			
			foreach ($reserveStatuses as $reserveStatus) {
				if (!isset($reservesCount[$reserveStatus->Id])) {
					$reservesCount[$reserveStatus->Id] = 0;
					$statuses[$reserveStatus->Id] = $reserveStatus->Name;
				}
			}
			
			if ($managerId) {
				$result = $this->cabinet()->reserves()->getManagerReservesCount($managerId);
				if (!$result->hasError()) {
					foreach ($result->reservesCount as $item) {
						$reservesCount[$item->StatusId] += $item->Count;
						$totalReservesCount += $item->Count;
					}
				}
			} else {
				foreach ($managers as $checkManagerId => $checkManagerName) {
					$result = $this->cabinet()->reserves()->getManagerReservesCount($checkManagerId);
					if (!$result->hasError()) {
						foreach ($result->reservesCount as $item) {
							$reservesCount[$item->StatusId] += $item->Count;
							$totalReservesCount += $item->Count;
						}
					}
				}
			}
		}
		
		$allReserves = [];
		$allReservesResult = $this->cabinet()->allReserves()->enumManagerReserves($managerId, $statusId, $createdFrom, $createdTo, $searchQuery);
		
		$dateHelper = new DateHelper();
		
		if (isset($allReservesResult->items)) {
			$reserves = $allReservesResult->items;
			foreach ($reserves as $reserve) {
				$creationDate = DateHelper::parseJsonDate($reserve->getCreationDate());
				$dateText = $dateHelper->parseDate(DateHelper::DATE_FORMAT_WITH_YEAR, $creationDate->getTimestamp());
				
				$dateText = $creationDate->format('d.m');
				if ($creationDate->format('d.m.Y') !== DateTime::now()->format('d.m.Y')) {
					$dateText = $creationDate->format('d.m H:i');
				}
				$dateText = $creationDate->format('d.m.y');
				
				$countProducts = $this->cabinet()->allReserves()->countReserveArticles($reserve->getAgentId(), $reserve->getDocumentId());
				$listItem = [
					'id' => $reserve->getDocumentId(),
					'name' => 'Заказ ' . $reserve->getDocumentId(),
					'date' => $dateText,
					'dateDt' => $creationDate->getTimestamp(),
					'state' => isset($statuses[$reserve->getStatusId()]) ? $statuses[$reserve->getStatusId()] : '',
					'stateId' => $reserve->getStatusId(),
					'amount' => $reserve->getAmount(),
					'agent' => $reserve->getAgentId(),
					'failed' => (65636 == $reserve->getStatusId()),
					'count' => $countProducts,
					'time' => $creationDate->format('H:i') 
				];
				if ($this->auth()->getUser()->isSuperManager()) {
					$listItem['managerName'] = $managers[$reserve->getManagerId()];
				}
				$listItem['canSummarize'] = false;
				if (in_array($reserve->getSubTypeId(), [
					9089,
					4137 
				])) {
					$listItem['canSummarize'] = true;
				}
				$listItem['subTypeId'] = $reserve->getSubTypeId();
				$listItem['statusId'] = $reserve->getStatusId();
				$listItem['statuses'] = count($statuses);
				$allReserves[] = $listItem;
			}
		}
		
		$reservesCount[0] = $totalReservesCount;
		
		$result = [
			'state' => 'ok',
			'orders' => $allReserves,
			'f-type' => $reservesCount 
		];
		
		return new JsonModel($result);
	}

	/**
	 *
	 * @param integer $reserveId        	
	 * @param integer $agentId        	
	 * @param integer $managerId        	
	 * @throws \RuntimeException
	 * @return \Zend\View\Model\JsonModel
	 */
	public function getReserve($reserveId, $agentId, $managerId = false) {
		if (!$reserveId) {
			return new JsonModel([
				'state' => 'error',
				'message' => 'Reserve ID is missing' 
			]);
		}
		
		$reserveInfoResult = $this->cabinet()->reserves()->getReserve($reserveId, $agentId);
		
		if (!$reserveInfoResult->success()) {
			throw new \RuntimeException($reserveInfoResult->getErrorCode());
		}
		
		$reserve = $reserveInfoResult->item;
		
		$user = $this->auth()->getUser();
		$agents = $user->agents();
		
		if ($agentId) {
			$agent = $agents->offsetGet($agentId);
		}
		
		$reserveGoods = $reserve->getGoods()->toArray();
		$items = [];
		
		if (count($reserveGoods)) {
			$goods = $this->catalog()->goods()->enumGoodsByIds(array_keys($reserveGoods));
			
			// check for virtual packages
			foreach ($goods as $good) {
				if ($good['IsPackage'] && ($good['IsVirtualPackage'] || (1 == $good['ViewMode']))) {
					$packageComponentIds = $this->catalog()->goods()->getPackageComponentsIds($good['GoodID']);
					foreach ($packageComponentIds as $packageComponentId) {
						if (isset($reserveGoods[$packageComponentId])) {
							unset($reserveGoods[$packageComponentId]);
						}
					}
				}
			}
			// END check for virtual packages
			
			foreach ($reserveGoods as $reserveGood) {
				
				$good = $goods[$reserveGood->getId()];
				$goodImage = $this->catalog()->images()->getGoodMainImage($reserveGood->getId());
				
				$image = $goodImage ? '/img/260x280/' . $goodImage['MiniatureUrl'] : '/i/nophoto.png';
				
				$items[] = [
					'id' => $reserveGood->getId(),
					'count' => $reserveGood->getQuantity() ?  : round($reserveGood->getAmount() / $reserveGood->getPrice()),
					'price' => $reserveGood->getPrice(),
					'href' => '/' . $good->CategoryUID . '/' . $good->GoodID . '_' . $good->GoodEngName,
					'title' => $good->GoodName,
					'image' => $image,
					'claimed' => 0,
					'article' => $reserveGood->getId() 
				];
			}
		}
		
		$dateHelper = new DateHelper();
		$creationDate = DateHelper::parseJsonDate($reserve->getCreationDate());
		$dateText = $dateHelper->parseDate(DateHelper::DATE_FORMAT_WITH_YEAR, $creationDate->getTimestamp());
		$dateText .= ' ' . $creationDate->format('H:i');
		
		$result = $this->cabinet()->reserves()->getReserveStatuses();
		$statuses = [];
		
		if (!$result->hasError()) {
			$reserveStatuses = $result->statuses;
			
			foreach ($reserveStatuses as $reserveStatus) {
				$statuses[$reserveStatus->Id] = $reserveStatus->Name;
			}
		}
//        [4128] => Резерв
//        [4131] => Набор
//        [4134] => Собранный заказ
//        [4137] => Отгруженный заказ
//        [5780] => Заказ на самовывоз
//        [9080] => Готов к доставке
//        [9083] => Заказ на доставку
//        [9089] => Отгруженная доставка
//        [13543] => Черновик
//        [14221] => Аннулирован
//        [15780] => Счет выставлен
//        [15783] => Счет оплачен
//        [59249] => Подзаказ
//        [65636] => Проблема
//        [70112] => Принят в работу
//        [71400] => Отказ клиента
		
		$result = $this->cabinet()->reserves()->getReserveDocuments($reserveId);
		$files = [];
		
		if (!$result->hasError()) {
			if (count($result->documents)) {
				foreach ($result->documents as $document) {
					$files[] = [
						'name' => $document->Name,
						'type' => $document->DataType,
						'href' => '/cabinet/download?reserveId=' . $reserveId . '&formId=' . $document->Id . '&agentId=' . $agentId 
					];
				}
			}
		}
		$cancelable = 0;
		switch ($reserve->getStatusId()) {
			case 4128: //Резерв
				$deadDate = DateHelper::parseJsonDate($reserve->getDeadReserveDateTime());
				$deadDateText = $dateHelper->parseDate(DateHelper::DATE_FORMAT_WITHOUT_YEAR, $deadDate->getTimestamp());
				$fullState = 'не выкуплен, в резерве до ' . $deadDateText;
                $cancelable = 1;
				break;
            case 9083: //Заказ на доставку
                $cancelable = 1;
			default:
				$fullState = $statuses[$reserve->getStatusId()];
				break;
		}
		
		$phone = '8-800-555-30-55';
		$onelinePhone = '88005553055';
		$userRegion = $this->getUserRegion();
		if ($userRegion) {
			if (42 == $userRegion->getId()) {
				$phone = '+7-495-540-51-52';
				$onelinePhone = '+74955405152';
			} else {
				$phone = '+7-812-313-27-27';
				$onelinePhone = '+78123132727';
			}
		}
		
		$reserveData = [
			'id' => $reserveId,
			'name' => 'Заказ №' . $reserveId,
			'amount' => $reserve->getAmount(),
			'date' => $dateText,
			'agent' => isset($agent) ? $agent->getName() : $reserve->getContactName(),
			'comment' => ($reserve->getDeliveryComments() ? $reserve->getDeliveryComments() : ''),
			'comment2' => $reserve->getComment(),
			'fullState' => $fullState,
			'files' => $files,
			'items' => $items,
			'deliveryDate' => '',
			'deliveryCost' => 0,
			'undefinedDelivery' => $reserve->isUndefinedDeliveryArea(),
			'floor' => '',
			'address' => '',
			'manager' => '',
			'notice' => 'Если вам требуется внести<br>изменения в существующий<br>заказ, свяжитесь с нами<br>по телефону <a href="tel:' . $onelinePhone . '">' . $phone . '</a>',
            'cancelable' => $cancelable,
		];
		
		// agent info
		$agentPhone = '';
		if ($reserve->getContactPhone()) {
			$phone = Phone::createFromString($reserve->getContactPhone());
			$agentPhone = $phone->toString('+7 ({code}) {number123}-{number45}-{number67}');
		} else {
			$clientInfo = $this->identity()->users()->getClientInfo($agentId);
			if ($clientInfo->success()) {
				$agentInfo = $clientInfo->user;
				$phone = Phone::createFromString($agentInfo->getPhone());
				$agentPhone = $phone->toString('+7 ({code}) {number123}-{number45}-{number67}');
			}
		}
		if ($agentPhone) {
			$reserveData['phone'] = $agentPhone;
		}
		// END agent info
		
		// delivery info
		if ($reserve->getIsDelivery()) {
			$deliveryDate = DateTime::createFromTimestamp(DateHelper::parseJsonDate($reserve->getDeliveryDate())->getTimestamp());
			$reserveData['deliveryDate'] = $dateHelper->parseDate(DateHelper::DATE_FORMAT_WITH_YEAR, $deliveryDate->getTimestamp());
			$addressInfo = $this->getReserveAddress($agentId, $reserve->getDeliveryAddressID());
			$reserveData['floor'] = $addressInfo['floor'];
			$reserveData['address'] = $addressInfo['address'];
			$reserveData['needLifting'] = $reserve->getDeliveryNeedLifting();
			$reserveData['deliveryCost'] = $reserve->getDeliveryCost();
			$reserveData['elevatorType'] = '';
			if (3 == $addressInfo['elevatorTypeId']) {
				$reserveData['elevatorType'] = 'грузовой лифт';
			} elseif (2 == $addressInfo['elevatorTypeId']) {
				$reserveData['elevatorType'] = 'обычный лифт';
			}
		}
		// END delivery info
		
		// manager info
		if ($reserve->getManagerId()) {
			$managers = $this->getManagers();
			if (isset($managers[$reserve->getManagerId()])) {
				$reserveData['manager'] = $managers[$reserve->getManagerId()];
			}
		}
		// END manager info
		
		if (!$reserve->isPayed() && (4128 == $reserve->getStatusId()) && (!isset($agent) || !$agent->requisites() || empty($agent->requisites()->getInn()))) {
			$reserveData['onlinePayment'] = '/cabinet/pay-reserve/?id=' . $reserveId . '&agentId=' . $agentId;
			$reserveData['onlinePaymentButton'] = 'Оплатить онлайн';
			$reserveData['agent'] = '';
		}
		
		if (false && MANAGER_MODE) {
			if (isset($reserveData['onlinePayment'])) unset($reserveData['onlinePayment']);
			if (isset($reserveData['onlinePaymentButton'])) unset($reserveData['onlinePaymentButton']);
		}
		
		if (isset($_GET['debug1'])) {
			// print_r($reserve->getContactName()); exit();
		}
		
		$result = [
			'state' => 'ok',
			'order' => $reserveData 
		];
		
		return new JsonModel($result);
	}

	public function logoutAction() {
		$this->auth()->logout();
		$this->order()->clearStorage();
		
		return $this->redirect()->toUrl('/');
	}

	public function profileAction() {
		$this->getLayout()->setTitle('Личный кабинет');
		$this->getLayout()->setBodyClass('cabinet profile');
		$this->getLayout()->addCssFile('cabinet.css');
		$this->getLayout()->addCssFile('cabinet.mobile.css');
		$this->getLayout()->addCssFile('cabinet.profile.css');
		$this->getLayout()->addJsFile('cabinet.js');
		$this->getLayout()->addJsFile('cabinet.profile.js');
		
		$user = $this->auth()->getUser();
		
		$bannerService = $this->service('\Application\Service\BannerService');
		
		return $this->outputVars(
			[
				'userPhone' => substr($user->getPhone(), 1),
				'userEmail' => $user->getEmail(),
				'currentPage' => 'profile',
				'banner' => $bannerService->getRandomBanner(2) 
			]);
	}

	public function changeProfileAction() {
		$request = $this->getRequest();
		
		if (!$request->isXmlHttpRequest()) {
			return $this->notfound();
		}
		
		$action = $request->getPost('action');
		
		$result = [
			'state' => 'error',
			'message' => 'Неизвестная команда' 
		];
		
		switch ($action) {
			case 'save':
				$phone = $request->getPost('phone');
				$email = $request->getPost('email');
				
				$result = $this->saveProfile($phone, $email);
				break;
			case 'change-pass':
				$oldPassword = $request->getPost('current');
				$newPassword = $request->getPost('pass1');
				$confirm = $request->getPost('pass2');
				
				$result = $this->changePassword($oldPassword, $newPassword, $confirm);
				break;
		}
		
		return new JsonModel($result);
	}

	private function saveProfile($phone, $email) {
		$result = [
			'state' => 'ok' 
		];
		
		if (!empty($phone)) {
			$phone = Phone::createFromString($phone)->toString('7{code}{number}');
		}
		
		$userUpdateResult = $this->identity()->users()->updateClientInfo($phone, $email);
		
		if (!$userUpdateResult->success()) {
			$result = [
				'state' => 'error',
				'message' => ($userUpdateResult->getData() ?  : 'Ошибка') 
			];
		} else {
			$userSession = $this->getServiceLocator()->get('user_session');
			
			$user = $this->auth()->getUser();
			$user->setPhone($phone);
			$user->setEmail($email);
			
			$userSession->user = $user->serialize();
		}
		
		return $result;
	}

	private function changePassword($oldPassword, $newPassword, $confirm) {
		$result = [
			'state' => 'ok' 
		];
		
		if (empty($oldPassword)) {
			return [
				'state' => 'error',
				'message' => 'Введите текущий пароль' 
			];
		}
		
		if ($newPassword !== $confirm) {
			return [
				'state' => 'error',
				'message' => 'Пароли не совпадают' 
			];
		}
		
		$userUpdatePasswordResult = $this->identity()->users()->updatePassword($oldPassword, $newPassword);
		
		if ($userUpdatePasswordResult->hasError()) {
			return [
				'state' => 'error',
				'message' => 'Неизвестная ошибка' 
			];
		} else {
			if (!$userUpdatePasswordResult->success()) {
				return [
					'state' => 'error',
					'message' => 'Неверный текущий пароль' 
				];
			}
		}
		
		return $result;
	}

	public function requisitesAction() {
		$user = $this->auth()->getUser();
		$agents = $user->agents();
		
		$request = $this->getRequest();
		
		if ($request->isXmlHttpRequest()) {
			$action = $request->getQuery('action') ?  : $request->getPost('action');
			
			$result = [
				'state' => 'error',
				'message' => 'Неизвестная команда' 
			];
			
			switch ($action) {
				case 'get-list':
					$agentId = $request->getQuery('f-agent');
					$agent = $agents->offsetGet($agentId);
					
					$result = [
						'state' => 'ok',
						'requisites' => [
							[
								'title' => 'Наименование компании',
								'value' => $agent->getName() 
							],
							[
								'title' => 'Юридический адрес',
								'value' => $agent->requisites()->getAddress() ?  : '' 
							],
							[
								'title' => 'ИНН',
								'value' => $agent->requisites()->getInn() 
							],
							[
								'title' => 'КПП',
								'value' => $agent->requisites()->getKpp() 
							],
							[
								'title' => 'Банк',
								'value' => $agent->requisites()->getBankName() ?  : '' 
							],
							[
								'title' => 'БИК',
								'value' => $agent->requisites()->getBic() ?  : '' 
							],
							[
								'title' => 'Кор. счет',
								'value' => $agent->requisites()->getBankCorrAccount() ?  : '' 
							],
							[
								'title' => 'Расчетный счет',
								'value' => $agent->requisites()->getSettlementAccount() ?  : '' 
							] 
						] 
					];
					break;
				case 'add':
					$agent = new Agent();
					$agent->setName($request->getPost('name'));
					$agent->requisites()->setAddress($request->getPost('address'));
					$agent->requisites()->setInn(trim($request->getPost('inn')));
					$agent->requisites()->setKpp(trim($request->getPost('kpp')));
					$this->application()->updateAgentRequisites($agent);
					
					$result = $this->identity()->agents()->createAgent($agent);
					
					if ($result->success()) {
						$result = [
							'state' => 'ok' 
						];
					} else {
						$errorMessage = 'Ошибка при создании контрагента';
						if ($result->hasErrorMessage()) {
							$errorMessage = $result->getErrorMessage();
						}
						$result = [
							'state' => 'error',
							'message' => $errorMessage 
						];
					}
					
					break;
			}
			
			return new JsonModel($result);
		}
		
		$this->getLayout()->setTitle('Личный кабинет');
		$this->getLayout()->setBodyClass('cabinet requisites');
		$this->getLayout()->addCssFile('cabinet.css');
		$this->getLayout()->addCssFile('cabinet.mobile.css');
		$this->getLayout()->addCssFile('cabinet.requisites.css');
		$this->getLayout()->addJsFile('cabinet.js');
		$this->getLayout()->addJsFile('cabinet.requisites');
		
		$bannerService = $this->service('\Application\Service\BannerService');
		
		return $this->outputVars([
			'currentPage' => 'requisites',
			'agents' => $agents,
			'banner' => $bannerService->getRandomBanner(2) 
		]);
	}

	public function payReserveAction() {
		$request = $this->getRequest();
		
		$reserveId = $request->getQuery('id');
		$agentId = $request->getQuery('agentId', 0);
		
		if (!$reserveId) {
			return $this->notfound();
		}
		
		if ($agentId) {
			$reserveInfoResult = $this->cabinet()->reserves()->getReserve($reserveId, $agentId);
		} else {
			$reserveInfoResult = $this->cabinet()->reserves()->getReserve($reserveId);
		}
		
		if ($reserveInfoResult->hasError()) {
			return $this->problem();
		}
		
		$reserveInfo = $reserveInfoResult->item;
		
		$bankPercent = (float)$this->getServiceLocator()->get('SoloSettings\Service\ConstantsService')->get('BankPercent');
		$amount = $reserveInfo->getAmount() + $reserveInfo->getDeliveryCost();
		
		$amount += ($bankPercent / 100) * $amount;
		$amount = ceil($amount);
		
		$returnUrl = 'https://' . $_SERVER['HTTP_HOST'] . '/cabinet/payment-success';
		$failUrl = 'https://' . $_SERVER['HTTP_HOST'] . '/cabinet/payment-fail';
		
		$data = [
			'orderNumber' => urlencode(substr($reserveId . '-' . uniqid(rand(), true), 0, 32)),
			'amount' => $amount * 100,
			'returnUrl' => $returnUrl,
			'failUrl' => $failUrl 
		];
		
		$alfaBankClient = $this->getServiceLocator()->get('AlfaBank\Client\Client');
		
		$response = $alfaBankClient->post('register.do', $data);
		
		if (!isset($response['errorCode'])) {
			return $this->redirect()->toUrl($response['formUrl']);
		} else {
			return $this->problem();
		}
	}

	public function paymentSuccessAction() {
		$request = $this->getRequest();
		
		if (!$request->getQuery('orderId')) {
			return $this->notfound();
		}
		
		$data['orderId'] = $request->getQuery('orderId');
		
		$this->getLayout()->setTitle('Личный кабинет');
		$this->getLayout()->setBodyClass('cabinet');
		$this->getLayout()->addCssFile('cabinet.css');
		$this->getLayout()->addCssFile('cabinet.mobile.css');
		$this->getLayout()->addJsFile('cabinet.js');
		
		$alfaBankClient = $this->getServiceLocator()->get('AlfaBank\Client\Client');
		
		$response = $alfaBankClient->post('getOrderStatus.do', $data);
		
		$merchantOrderNumber = explode('-', $response['OrderNumber'])[0];
		
		$this->order()->payReserve($merchantOrderNumber);
		
		// @todo Check if reserve paid
		
		return $this->outputVars([
			'reserveId' => $merchantOrderNumber 
		]);
	}

	public function paymentFailAction() {
		$request = $this->getRequest();
		
		if (!$request->getQuery('orderId')) {
			return $this->notfound();
		}
		
		$data['orderId'] = $request->getQuery('orderId');
		
		$this->getLayout()->setTitle('Личный кабинет');
		$this->getLayout()->setBodyClass('cabinet');
		$this->getLayout()->addCssFile('cabinet.css');
		$this->getLayout()->addCssFile('cabinet.mobile.css');
		$this->getLayout()->addJsFile('cabinet.js');
		
		$alfaBankClient = $this->getServiceLocator()->get('AlfaBank\Client\Client');
		
		$response = $alfaBankClient->post('getOrderStatus.do', $data);
		
		$merchantOrderNumber = explode('-', $response['OrderNumber'])[0];
		
		return $this->outputVars([
			'reserveId' => $merchantOrderNumber 
		]);
	}

	public function downloadAction() {
		$request = $this->getRequest();
		
		$agentId = $request->getQuery('agentId');
		$reserveId = $request->getQuery('reserveId');
		$printFormId = $request->getQuery('formId');
		
		if (!$printFormId || !$reserveId) {
			return $this->notfound();
		}
		
		$result = $this->cabinet()->reserves()->getReservePrintForm($reserveId, $printFormId, $agentId);
		
		if ($result->hasError()) {
			return $this->problem();
		}
		
		$reserveInfoResult = $this->cabinet()->reserves()->getReserve($reserveId, $agentId);
		$reserve = $reserveInfoResult->item;
		
		$printForm = $result->printForm;
		if ($reserve->hasChildDocuments()) {
			$childDocument = $reserve->getFirstChildDocument();
			$filename = sprintf('%s %d (заказ %d).%s', $printForm->Name, $childDocument->getId(), $reserveId, $printForm->DataType);
		} else {
			$filename = sprintf('%s (заказ %d).%s', $printForm->Name, $reserveId, $printForm->DataType);
		}
		
		$stream = fopen('data://application/pdf;base64,' . $printForm->Data, 'r');
		
		$response = new \Zend\Http\Response\Stream();
		$response->setStream($stream);
		$response->setStatusCode(200);
		$response->setStreamName($filename);
		$headers = new \Zend\Http\Headers();
		if (false) {
			$attachMode = 'attachment';
			if ($this->application()->isMobileIosDevice() || $this->application()->isIpadDevice()) {
				$attachMode = 'inline';
			}
			$headers->addHeaders(
				array(
					'Content-Description' => 'File Transfer',
					'Content-Transfer-Encoding' => 'binary',
					'Content-Disposition' => $attachMode . '; filename="' . $filename . '"',
					'Content-Type' => 'application/pdf',
					'Content-Length' => mb_strlen($printForm->Data),
					'Expires' => '@0',
					'Cache-Control' => 'must-revalidate',
					'Pragma' => 'public' 
				));
		} else {
			$headers->addHeaders(
				array(
					'Content-Disposition' => 'attachment; filename="' . basename($filename) . '"',
					'Content-Type' => 'application/octet-stream',
					'Content-Length' => mb_strlen($printForm->Data),
					'Expires' => '@0',
					'Cache-Control' => 'must-revalidate',
					'Pragma' => 'public' 
				));
		}
		$response->setHeaders($headers);
		return $response;
	}

	public function documentsAction() {
		$request = $this->getRequest();
		
		if ($request->isXmlHttpRequest()) {
			$agentId = $request->getQuery('f-agent');
			
			$allReserves = [];
			$allReservesResult = $this->cabinet()->allReserves()->enum($agentId);
			
			$dateHelper = new DateHelper();
			
			if (isset($allReservesResult->items)) {
				foreach ($allReservesResult->items as $reserve) {
					$result = $this->cabinet()->reserves()->getReserveDocuments($reserve->getDocumentId());
					$files = [];
					
					if (!$result->hasError()) {
						if (count($result->documents)) {
							foreach ($result->documents as $document) {
								$files[] = [
									'title' => $document->Name,
									'type' => $document->DataType,
									'href' => '/cabinet/download?reserveId=' . $reserve->getDocumentId() . '&formId=' . $document->Id 
								];
							}
						}
					}
					
					$creationDate = DateHelper::parseJsonDate($reserve->getCreationDate());
					$dateText = $dateHelper->parseDate(DateHelper::DATE_FORMAT_WITH_YEAR, $creationDate->getTimestamp());
					
					$allReserves[] = [
						'name' => 'Заказ ' . $reserve->getDocumentId(),
						'date' => $dateText,
						'amount' => $reserve->getAmount(),
						'paymentType' => $reserve->getPayTypeId() == Reserve::PAYMENT_TYPE_CASH ? 'наличные' : 'безнал',
						'docs' => $files 
					];
				}
			}
			
			if (!$agentId) {
				$user = $this->auth()->getUser();
				
				foreach ($user->agents() as $agent) {
					$allReservesResult = $this->cabinet()->allReserves()->enum($agent->getId());
					
					if (isset($allReservesResult->items)) {
						foreach ($allReservesResult->items as $reserve) {
							$result = $this->cabinet()->reserves()->getReserveDocuments($reserve->getDocumentId());
							$files = [];
							
							if (!$result->hasError()) {
								if (count($result->documents)) {
									foreach ($result->documents as $document) {
										$files[] = [
											'title' => $document->Name,
											'type' => $document->DataType,
											'href' => '/cabinet/download?reserveId=' . $reserve->getDocumentId() . '&formId=' . $document->Id . '&agentId=' . $agentId 
										];
									}
								}
							}
							
							$creationDate = DateHelper::parseJsonDate($reserve->getCreationDate());
							$dateText = $dateHelper->parseDate(DateHelper::DATE_FORMAT_WITH_YEAR, $creationDate->getTimestamp());
							
							$allReserves[] = [
								'name' => 'Заказ ' . $reserve->getDocumentId(),
								'date' => $dateText,
								'amount' => $reserve->getAmount(),
								'paymentType' => $reserve->getPayTypeId() == Reserve::PAYMENT_TYPE_CASH ? 'наличные' : 'безнал',
								'docs' => $files 
							];
						}
					}
				}
			}
			
			return new JsonModel([
				'state' => 'ok',
				'orders' => $allReserves 
			]);
		}
		
		$this->getLayout()->setTitle('Личный кабинет');
		$this->getLayout()->setBodyClass('cabinet cabinet-main documents');
		$this->getLayout()->addCssFile('cabinet.css');
		$this->getLayout()->addCssFile('cabinet.mobile.css');
		$this->getLayout()->addJsFile('cabinet.js');
		
		$user = $this->auth()->getUser();
		
		$bannerService = $this->service('\Application\Service\BannerService');
		
		return $this->outputVars([
			'agents' => $user->agents(),
			'currentPage' => 'documents',
			'banner' => $bannerService->getRandomBanner(2) 
		]);
	}

	public function deletereserveAction() {
		$out = [
			'success' => false 
		];
		$number = $this->params()->fromQuery('order');
		$out['number'] = $number;
		
		$method = $this->getWebMethod('DeleteReserve');
		$method->addPar('Id', $number);
		// $method->addPar('AgentId', $this->auth()->getUser()->getId());
		// print json_encode($method->getPars(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT); exit();
		$response = $method->call();
		$data = $response->getData();
		if (isset($data->Success) && (1 == $data->Success)) {
			print '<h1>Красаучик, удалил</h1>';
			exit();
		} elseif ($response->getErrorMessage()) {
			print '<h1>Ultima error</h1>';
			print '<p>' . $response->getErrorMessage() . '</p>';
			exit();
		}
		print_r($response);
		exit();
		
		return $this->getResponse()->setContent(json_encode($out, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
	}

	/**
	 *
	 * @return array
	 */
	private function getManagers() {
		$result = [];
		
		$wm = $this->getWebMethod('GetManagers');
		$response = $wm->call();
		if (!$response->hasError()) {
			$data = $response->getData();
			foreach ($data->ManagerInfo as $row) {
				$result[$row->ManagerID] = $row->ManagerName;
			}
		}
		
		uasort($result, function ($name1, $name2) {
			return strcasecmp($name1, $name2);
		});
		
		return $result;
	}

	/**
	 *
	 * @param inetger $agentId        	
	 * @param integer $addressId        	
	 * @return string
	 */
	public function getReserveAddress($agentId, $addressId) {
		$result = [
			'address' => '',
			'floor' => null,
			'elevatorTypeId' => 0 
		];
		
		if (!MANAGER_MODE) {
			$addresses = $this->auth()->getUser()->addresses()->getDeliveryAddresses();
			foreach ($addresses as $address) {
				if ($address->getId() == $addressId) {
					$result = [
						'address' => $address->getAddress(),
						'floor' => $address->getFloor() 
					];
				}
			}
		} else {
			$wm = $this->getWebMethod('GetDeliveryAddresses');
			$wm->addPar('AgentId', $agentId);
			$response = $wm->call();
			if (!$response->hasError()) {
				$rdr = new UltimaJsonListReader($response);
				foreach ($rdr as $adress) {
					if ($adress['Id'] == $addressId) {
						$result = [
							'address' => $adress['Address'],
							'floor' => $adress['Floor'],
							'elevatorTypeId' => $adress['ElevatorTypeID'] 
						];
					}
				}
			}
		}
		
		return $result;
	}

	/**
	 *
	 * @param integer $reserveId        	
	 * @param string $comment        	
	 * @return boolean
	 */
	private function updateReserveDeliveryComment($reserveId, $comment) {
		$result = false;
		
		$method = $this->getWebMethod('UpdateReserveDeliveryComment');
		$method->addPar('ReserveID', $reserveId);
		$method->addPar('Comment', $comment);
		$response = $method->call();
		$data = json_decode($response->getResponse()->getContent(), true);
		if (is_array($data)) {
			$result = $data['Status'];
		}
		
// 		print "Request:\n";
// 		print json_encode($method->getPars(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
// 		print "\n";
// 		print "Response:\n";
// 		print_r($response->getResponse()->getContent());
		
		return $result;
	}

	/**
	 *
	 * @param integer $reserveId        	
	 * @param string $comment        	
	 * @return boolean
	 */
	private function updateReserveOrderComment($reserveId, $comment) {
		$result = false;
		
		$method = $this->getWebMethod('UpdateReserveOrderComment');
		$method->addPar('ReserveID', $reserveId);
		$method->addPar('Comment', $comment);
		$response = $method->call();
		$data = json_decode($response->getResponse()->getContent(), true);
		if (is_array($data)) {
			$result = $data['Status'];
		}
		
		return $result;
	}

}