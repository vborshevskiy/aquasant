<?php

namespace Application\Controller;

use Solo\Stdlib\ArrayHelper;
use Zend\View\Model\JsonModel;

class SearchController extends BaseController {

	public function indexAction($searchRepeat = false) {
		$query = $this->clearQuery($this->params()->fromQuery('q'));
		$category = $this->params()->fromRoute('category');
		$softCategoryId = (!empty($category) ? (int)$this->catalog()->categories()->getCategoryByUid($category)->SoftCategoryID : null);
		
		if ($searchRepeat) {
			$query = str_replace('/', '', $query);
			$query = str_replace('-', '', $query);
			$query = str_replace(' ', '', $query);
			$query = str_replace('№', '', $query);
		}
		
		$cityId = $this->getErpCityId();
		if (0 == strlen(trim($query))) {
			$goods = [];
		} else {
			if (0 < intval($query)) {
				$good = null;
				if (GOD_MODE || MANAGER_MODE) {
					$good = $this->catalog()->goods()->getGoodWithCategoryUIDByGoodId(intval($query), $cityId);
					if (!$good->CategoryUID) {
						$good->CategoryUID = 'goods';
					}
				} else {
					$good = $this->catalog()->goods()->getAvailGoodWithCategoryUIDByGoodId(intval($query), $cityId);
				}
				if (null !== $good) {
					$redirectUrl = '/' . $good->CategoryUID . '/' . $good->GoodID . '_' . $good->GoodEngName . '/';
					if (GOD_MODE) {
						$redirectUrl .= '?polygon=1';
					}
					return $this->redirect()->toUrl($redirectUrl);
				}
			}
			try {
				$logger = $this->service('searchLogger');
				$pRedis = $this->service('PredisClient');
				$logger->addKey($pRedis, $query);
			} catch (\Exception $e) {}
			
			$categories = $this->enumAvailSearchCategories($this->catalog()->search()->searchCategories($query, 4));
			if (0 < sizeof($categories)) {
				$categoryName = str_replace('  ', ' ', $categories[0]['CategoryName']);
				if (0 == strcasecmp(mb_strtoupper($query), mb_strtoupper($categoryName))) {
					return $this->redirect()->toUrl('/' . $categories[0]['CategoryUID'] . '/');
				}
			}
			
			$goods = $this->enumAvailSearchGoods($this->catalog()->search()->searchGoods($query), null, $softCategoryId);
			
			if (count($goods) == 1 && is_null($softCategoryId)) {
				$goodsInfo = $this->catalog()->goods()->enumGoodsByIds(ArrayHelper::enumOneColumn($goods, 'GoodID'));
				if (count($goodsInfo) == 1) {
					return $this->redirect()->toUrl('/' . current($goodsInfo)->CategoryUID . '/' . current($goodsInfo)->GoodID . '_' . current($goodsInfo)->GoodEngName . '/');
				}
			}
		}
		
		if (0 == sizeof($goods) && !$searchRepeat) {
			// $searchRepeat = true;
			// $this->indexAction($searchRepeat);
		}
		
		return $this->forward()->dispatch('Application\Controller\CatalogController', [
			'action' => 'search',
			'goods' => $goods,
			'category' => $category,
			'query' => $query 
		]);
	}

	public function autocompleteAction() {
		$query = trim($this->params()->fromQuery('q'));
		
		if (empty($query)) {
			return $this->notfound();
		}
		$query = str_replace('/', ' ', $query);
		$query = str_replace('№', '', $query);
		$cityId = $this->getErpCityId();
		$result = $this->catalog()->search()->createAutocompleteResult($query);
		$categories = $this->enumAvailSearchCategories($this->catalog()->search()->searchCategories($query, 4));
		$goods = $this->enumAvailSearchGoods($this->catalog()->search()->searchGoods($query, 8));
		if (is_numeric($query) && (0 < intval($query))) {
			$topGoods = $this->enumAvailSearchGoods([
				[
					'GoodID' => $query 
				] 
			], 1);
			if (0 < count($topGoods)) {
				array_unshift($goods, reset($topGoods));
			}
		}
		
		if (0 < sizeof($categories)) {
			$group = $result->addGroup('categories');
			foreach ($categories as $category) {
				$group->addItem($category['CategoryName'], '/' . $category['CategoryUID'] . '/');
			}
		}
		try {
			$logger = $this->service('searchLogger');
			$pRedis = $this->service('PredisClient');
			$logger->addKey($pRedis, $query, true);
		} catch (\Exception $e) {}
		
		if (0 < sizeof($goods)) {
			$goodsInfo = $this->catalog()->goods()->enumGoodsByIds(ArrayHelper::enumOneColumn($goods, 'GoodID'));
			$group = $result->addGroup('items');
			foreach ($goodsInfo as $good) {
				$group->addItem($good->GoodName, '/' . $good->CategoryUID . '/' . $good->GoodID . '_' . $good->GoodEngName . '/');
			}
			/*
			 * if ($max < sizeof($goodsInfo)) { $result->addGroup()->addItem('Показать все', '/search/?q=' . $query); }
			 */
		}
		// $result->highlightQuery($query);
		
		return new JsonModel([
			'state' => 'ok',
			'items' => $result->getItems() 
		]);
	}

	private function enumAvailSearchCategories($categories, $limit = null) {
		if (0 == sizeof($categories)) {
			return [];
		}
		$availIds = $this->catalog()->categories()->enumAvailCategoryIdsBySoftCategoryIds(ArrayHelper::enumOneColumn($categories, 'CategoryID'));
		$result = [];
		foreach ($categories as $category) {
			if (in_array($category['CategoryID'], $availIds)) {
				$result[] = $category;
			}
			if ((null !== $limit) && ($limit == sizeof($result))) {
				return $result;
			}
		}
		return $result;
	}

	private function enumAvailSearchGoods($goods, $limit = null, $softCategoryId = null) {
		if (0 == sizeof($goods)) {
			return [];
		}
		$availIds = $this->catalog()->goods()->enumAvailGoodIdsByGoodIds(ArrayHelper::enumOneColumn($goods, 'GoodID'), $softCategoryId);
		$result = [];
		foreach ($goods as $good) {
			if (in_array($good['GoodID'], $availIds)) {
				$result[] = $good;
			}
			if ((null !== $limit) && ($limit == sizeof($result))) {
				return $result;
			}
		}
		return $result;
	}

	private function clearQuery($query) {
		$query = str_replace('!', ' ', $query);
		$query = str_replace('/', ' ', $query);
		$query = str_replace('*', ' ', $query);
		$query = str_replace('(', ' ', $query);
		$query = str_replace(')', ' ', $query);
		$query = str_replace('\'', ' ', $query);
		$query = str_replace('"', ' ', $query);
		$query = str_replace('-', ' ', $query);
		$query = str_replace('#', ' ', $query);
		$query = str_replace('$', ' ', $query);
		$query = str_replace('^', ' ', $query);
		$query = strip_tags(trim($query));
		return $query;
	}

}

?>