<?php

namespace Application\Controller;

use SoloCatalog\Service\Helper\DateHelper;
use Zend\View\Model\JsonModel;
use Solo\Stdlib\StringHelper;
use SoloOrder\Entity\Basket\Good;
use Solo\DateTime\DateTime;

class BasketController extends BaseController {

	public function indexAction() {
		$this->getLayout()->setTitle('Корзина');
		$this->getLayout()->addCssFile('basket.css');
		$this->getLayout()->addCssFile('item.css');
		$this->getLayout()->addJsFile('basket.js');
		$this->getLayout()->addJsFile('item.js');
		$this->getLayout()->setBodyClass('basket full');
		
		$cityId = $this->getErpCityId();
		$priceColumn = $this->catalog()->prices()->helper()->getPriceColumnId($this->isPostDelivery());
		
		if ($this->basket()->isEmpty()) {
			return $this->forward()->dispatch('Application\Controller\BasketController', [
				'action' => 'empty' 
			]);
		}
		
		// var_dump($this->basket()->getGroups()); die;
		$goodIds = $this->basket()->enumGoodIds();
		$goods = $this->catalog()->goods()->enumGoodsByIds($goodIds);
		$goodsMiniatures = $this->catalog()->images()->getGoodsMainImage($goodIds);
		
		$avails = $this->catalog()->goods()->getAvailabilityByGoodIds($goodIds, $cityId);
		
		$prices = $this->catalog()->prices()->getPriceByGoodIds($goodIds, $cityId, $priceColumn);
		
		$vars = [
			'dateHelper' => new DateHelper(),
			'basketGoods' => $this->basket()->getGoods(),
			'goods' => $goods,
			'goodsMiniatures' => $goodsMiniatures,
			'avails' => $avails,
			'prices' => $prices,
			'itemsCount' => $this->basket()->getTotalQuantity(),
			'totalAmount' => $this->basket()->getTotalAmount() 
		];
		
		$ecommerceTracker = $this->createEcommerceTracker('basket');
		$vars['ecommerceTracker'] = $ecommerceTracker;
		
		return $this->outputVars($vars);
	}

	public function emptyAction() {
		$this->getLayout()->setTitle('Корзина');
		$this->getLayout()->addCssFile('item.css');
		$this->getLayout()->addJsFile('basket.js');
		$this->getLayout()->setBodyClass('basket empty');
		
		return $this->outputVars([]);
	}

	/**
	 * Update basket content
	 */
	public function updateAction() {
		$cityId = $this->getErpCityId();
		$priceColumn = $this->catalog()->prices()->helper()->getPriceColumnId($this->isPostDelivery());
		
		$confirmations = $this->params()->fromQuery('confirmations', false) ?  : [];
		if ($confirmations) {
			unset($confirmations[0]); // unset '-1' element
		}
		
		// Комплекты
		$complectGoods = $this->params()->fromQuery('complectGoods', []);
		
		if (is_array($this->params()->fromQuery('id'))) {
			$goodIds = $this->params()->fromQuery('id'); // Все переданные ID
			$goodIds = array_diff($goodIds, $complectGoods); // убираем все части коплектов, чтобы они не попали в корзину
		} else {
			$goodIds = [
				(int)$this->params()->fromQuery('id') 
			];
		}
		
		$goodId = $generalGoodId = (int)reset($goodIds); // Основной товар (комплект)
		                                                 
		// Опции
		$satelliteGoods = $this->params()->fromQuery('satelliteGoods', []);
		$goodIds = array_merge($goodIds, $satelliteGoods);
		$goodIds = array_unique($goodIds);
		
		// Комплекты
		$goodIds = array_unique(array_merge($goodIds, $complectGoods));
		
		$quantity = (int)$this->params()->fromQuery('count');
		
		if (array_key_exists($goodId, $confirmations) && $confirmations[$goodId] == 'yes' && $this->basket()->hasGood($goodId)) {
			$existingBasketGood = $this->basket()->getGoodById($goodId);
			$quantity = $existingBasketGood->getQuantity();
		}
		
		$result["onlyInShop"] = [];
		
		$result['state'] = 'ok';
		
		$prices = $this->catalog()->prices()->getPricesByGoodId($goodId, $priceColumn);
		$price = (array_key_exists($cityId, $prices) ? $prices[$cityId] : -1);
		$good = $this->catalog()->goods()->getGoodByGoodId($goodId);
		$product = [
			'id' => $this->params()->fromQuery('id'),
			'name' => $good['GoodName'],
			'price' => $price,
			'brand' => $this->catalog()->goods()->getBrandNameByProductId($goodId),
			'category' => $this->catalog()->goods()->getCategoryNameByProductId($goodId),
			'quantity' => $quantity 
		];
		
		$modalItems = [];
		if ($quantity > 0) {
			foreach ($goodIds as $goodId) {
				// Если товар является частью комплекта, то получим основной товар (комплект) и обновим свойство allowShowcaseSale
				if (in_array($goodId, $complectGoods)) {
					if ($this->basket()->hasGood($generalGoodId)) {
						$existingGeneralBasketGood = $this->basket()->getGoodById($generalGoodId);
						$packageComponents = $existingGeneralBasketGood->getPackageComponents();
						$packageAllowShowcaseSale = array_key_exists($goodId, $confirmations) && $confirmations[$goodId] == 'yes';
						if (isset($packageComponents[$goodId])) {
							$packageComponents[$goodId]->allowShowcaseSale($packageAllowShowcaseSale);
						}
					} else {
						return new JsonModel([
							'state' => 'error',
							'message' => 'Не найден основной комплект' 
						]);
					}
					continue;
				}
				
				$prices = $this->catalog()->prices()->getPricesByGoodId($goodId, $priceColumn);
				$price = (array_key_exists($cityId, $prices) ? $prices[$cityId] : -1);
				$deliveryDates = $this->delivery()->getDeliveryDatesByGoodId($cityId, $goodId);
				
				$avail = $this->catalog()->goods()->getAvailabilityByGoodId($goodId, $cityId);
				// Fix. gdoc 180.2 "...не опираться на наличие на витрине для клиента..."
				if (!MANAGER_MODE) {
					$availQuantity = $avail->AvailQuantity - $avail->WindowQuantity;
				} else {
					$availQuantity = $avail->AvailQuantity;
				}
				
				$good = $this->catalog()->goods()->getGoodByGoodId($goodId);
				if ($quantity > ($availQuantity + $avail->SuborderQuantity)) {
					$message = ($availQuantity + $avail->SuborderQuantity) > 0 ? 'Превышено допустимое количество товара' : 'Товар недоступен для продажи';
					
					return new JsonModel([
						'state' => 'error',
						'message' => $message 
					]);
				}
				
				$image = $this->catalog()->images()->getGoodMainImage($goodId);
				
				$allowShowcaseSale = array_key_exists($goodId, $confirmations) && $confirmations[$goodId] == 'yes';
				
				$basketGood = $this->basket()->newGood();
				$basketGood->setId($goodId);
				$basketGood->setPrice($price);
				$basketGood->setPrices($prices);
				$basketGood->setQuantity($quantity);
				$basketGood->setWeight($good->Weight);
				$basketGood->setVolume($good->Volume);
				$basketGood->allowShowcaseSale($allowShowcaseSale);
				
				if ($allowShowcaseSale) {
					$basketGood->setPickupDate(date('Y-m-d'));
				} else {
					if ($deliveryDates->PickupDate) {
						$basketGood->setPickupDate(date('Y-m-d', strtotime($deliveryDates->PickupDate)));
					} else {
						$basketGood->setPickupDate(date('Y-m-d', strtotime($avail->ArrivalDate)));
					}
				}
				
				if ($quantity > $availQuantity) {
					$basketGood->setSuborderQuantity($quantity - $availQuantity);
					$supplierRemains = $this->catalog()->goods()->getGoodSupplierRemains($goodId);
					if ($supplierRemains) {
						$arrivalDate = date('Y-m-d', strtotime($supplierRemains->ArrivalDate) + (60 * 60 * 24));
						$basketGood->setPickupDate($arrivalDate);
					}
				}
				
				if ($good->IsPackage) {
					if ($this->basket()->hasGood($goodId)) {
						$existingBasketGood = $this->basket()->getGoodById($goodId);
						$basketGood->setPackageComponents($existingBasketGood->getPackageComponents());
					}
					
					if (0 < count($complectGoods)) {
						$defaultComponentIds = [];
						foreach ($complectGoods as $componentId) {
							$componentGood = $this->basket()->newComponentGood();
							$componentGood->setId($componentId);
							$defaultComponentIds[$componentId] = $componentGood;
						}
						$basketGood->setPackageComponents($defaultComponentIds);
					} elseif (0 == count($basketGood->getPackageComponents())) {
						$defaultComponentIds = [];
						
						if ((1 == $good->IsVirtualPackage) || (1 == $good->ViewMode)) {
							// $defaultComponentIds = $this->catalog()->goods()->getPackageComponentsIds($goodId);
						} else {
							$defaultComponentIds = [];
							foreach ($this->catalog()->goods()->getPackageDefaultComponentsIds($goodId) as $componentId) {
								$componentGood = $this->basket()->newComponentGood();
								$componentGood->setId($componentId);
								$defaultComponentIds[$componentId] = $componentGood;
							}
						}
						if (0 == count($defaultComponentIds)) {
							$componentGood = $this->basket()->newComponentGood();
							$componentGood->setId($goodId);
							$componentGood->allowShowcaseSale($allowShowcaseSale);
							$defaultComponentIds[] = $componentGood;
						}
						$basketGood->setPackageComponents($defaultComponentIds);
					}
					
					$maxPickupDate = strtotime($basketGood->getPickupDate());
					foreach ($basketGood->getPackageComponents() as $packageComponent) {
						$packageComponentId = $packageComponent->getId();
						$packageDeliveryDates = $this->delivery()->getDeliveryDatesByGoodId($cityId, $packageComponentId);
						$packagePickupDate = strtotime($packageDeliveryDates['PickupDate']);
						$maxPickupDate = max($maxPickupDate, $packagePickupDate);
					}
					if ($maxPickupDate) {
						$basketGood->setPickupDate(date('Y-m-d', $maxPickupDate));
					}
					
					$packagePrices = $this->catalog()->prices()->getPriceByGoodIds($basketGood->getPackageComponentIds(), $cityId, $priceColumn);
					$packagePrice = array_sum($packagePrices);
					$basketGood->setPrice($packagePrice);
				}
				
				$modalItems[] = [
					"title" => $good->GoodName,
					"image" => isset($image) ? '/img/260x280/' . $image['MiniatureUrl'] : "/i/nophoto.png",
					"count" => $quantity,
					"min" => 1,
					"max" => $availQuantity + $avail->SuborderQuantity,
					"price" => $basketGood->getPrice(),
					"item_id" => $goodId,
					"items" => [] 
				];
				
				$allowAdd = true;
				
				// $showRoomRemains
				$showRoomRemains = $this->catalog()->goods()->getGoodsRemainsByStoreId([
					$goodId 
				], 1);
				$inShowRoom = ((0 < count($showRoomRemains)) && isset($showRoomRemains[$goodId]) && (0 < $showRoomRemains[$goodId]));
				$onlyInShowRoom = ($avail && (0 < $avail['WindowQuantity']) && ($avail['AvailQuantity'] == $avail['WindowQuantity']) && (0 == $avail['SuborderQuantity']));
				
				if (MANAGER_MODE && $inShowRoom && $onlyInShowRoom && (!$complectGoods && $goodId == $generalGoodId)) {
					$allowAdd = false;
					$result["onlyInShop"][] = [
						"id" => $goodId,
						"title" => $good->GoodName,
						"img" => isset($image) ? '/img/260x280/' . $image['MiniatureUrl'] : "/i/nophoto.png",
						"noimage" => true,
						'header' => 'Товар есть ТОЛЬКО на витрине.<br>Продаем выставочный образец?',
						'buttons' => [
							[
								"title" => "Да, продаем!",
								"key" => "yes",
								"type" => "button" 
							],
							[
								"title" => "Нет!",
								"key" => "no",
								"type" => "-cancel" 
							] 
						] 
					];
				} else if (MANAGER_MODE && $inShowRoom && !$onlyInShowRoom && $avail['AvailQuantity'] == $avail['WindowQuantity'] && (!$complectGoods && $goodId == $generalGoodId)) {
					$allowAdd = false;
					$result["onlyInShop"][] = [
						"id" => $goodId,
						"title" => $good->GoodName,
						"img" => isset($image) ? '/img/260x280/' . $image['MiniatureUrl'] : "/i/nophoto.png",
						"noimage" => true,
						'header' => 'Как будем продавать?',
						'buttons' => [
							[
								"title" => "С витрины",
								"key" => "yes",
								"type" => "button" 
							],
							[
								"title" => "Подзаказ (см. даты)",
								"key" => "no",
								"type" => "button" 
							] 
						] 
					];
				} elseif (MANAGER_MODE && $inShowRoom && !$onlyInShowRoom && ($quantity > ($avail['AvailQuantity'] - $avail['WindowQuantity'])) && (!$complectGoods && $goodId == $generalGoodId)) {
					$allowAdd = false;
					$result["onlyInShop"][] = [
						"id" => $goodId,
						"title" => $good->GoodName,
						"img" => isset($image) ? '/img/260x280/' . $image['MiniatureUrl'] : "/i/nophoto.png",
						"noimage" => true,
						'header' => 'Хотите ' . $quantity . 'шт, но<br />
на складе есть только ' . ($avail['AvailQuantity'] - $avail['WindowQuantity']) . 'шт,<br />
и еще ' . ((1 == $avail['WindowQuantity']) ? 'одна штука' : $avail['WindowQuantity'].'шт') . ' на витрине<br />
<br />
Продаем эту одну?',
						'buttons' => [
							[
								"title" => "Да",
								"key" => "yes",
								"type" => "button",
								'action' => 'setShowcaseSale(' . $goodId . ');',
								"sourceModalAction"=>"ok"
							],
							[
								"title" => "Нет",
								"key" => "no",
								"type" => "button",
								'action' => 'decrementBasketQuantity(' . $goodId . ', 1);' ,
								"sourceModalAction"=>"close"
							] 
						] 
					];
				}
				
				if (!$allowAdd && (1 < $basketGood->getQuantity())) {
					$allowAdd = true;
				}
				
				if ($allowAdd || array_key_exists($goodId, $confirmations)) {
					if ($this->basket()->hasGood($goodId)) {
						$existingBasketGood = $this->basket()->getGoodById($goodId);
						$quantityChange = $basketGood->getQuantity() - $existingBasketGood->getQuantity();
						$product['quantity'] = $quantityChange;
						$basketGood->allowShowcaseSale($allowShowcaseSale);
						$this->basket()->updateGood($basketGood);
					} else {
						$this->basket()->addGood($basketGood);
						$product['quantity'] = $basketGood->getQuantity();
					}
				}
			}
			
			if (0 < count($modalItems)) {
				$result['modalItems'] = $modalItems;
				$result['mode'] = 'items';
			}
		} else {
			foreach ($goodIds as $goodId) {
				$basketGood = $this->basket()->getGoodById($goodId);
				if (!is_null($basketGood)) {
					$product['quantity'] = $basketGood->getQuantity();
					$this->basket()->removeGood($basketGood);
				}
			}
		}
		
		$basketGoodIds = $this->basket()->enumGoodIds();
		$basketGoods = $this->catalog()->goods()->enumGoodsByIds($basketGoodIds);
		
		$result['items'] = [];
		foreach ($this->basket()->getGoods() as $basketGood) {
			$result['items'][$basketGood->getId()] = [
				'title' => $basketGoods[$basketGood->getId()]['GoodName'],
				'count' => $basketGood->getQuantity(),
				'price' => $basketGood->getPrice(),
				'article' => $basketGood->getId() 
			];
		}
		
		$result['total_amount'] = (int)$this->basket()->getTotalAmount();
		$result['product'] = $product;
		
		// Проверим комплекты на наличие
		$checkProducts = array_unique(array_merge($complectGoods, $satelliteGoods));
		if (MANAGER_MODE && $checkProducts) {
			foreach ($checkProducts as $complectGoodId) {
				// $showRoomRemains
				$good = $this->catalog()->goods()->getGoodByGoodId($complectGoodId);
				$avail = $this->catalog()->goods()->getAvailabilityByGoodId($complectGoodId, $cityId);
				$showRoomRemains = $this->catalog()->goods()->getGoodsRemainsByStoreId([
					$complectGoodId 
				], 1);
				$inShowRoom = ((0 < count($showRoomRemains)) && isset($showRoomRemains[$complectGoodId]) && (0 < $showRoomRemains[$complectGoodId]));
				$onlyInShowRoom = ($avail && (0 < $avail['WindowQuantity']) && ($avail['AvailQuantity'] == $avail['WindowQuantity']) && (0 == $avail['SuborderQuantity']));
				$image = $this->catalog()->images()->getGoodMainImage($complectGoodId);
				
				if ($inShowRoom && $onlyInShowRoom) {
					$allowAdd = false;
					$result["onlyInShop"][] = [
						"id" => $complectGoodId,
						"title" => $good->GoodName,
						"img" => isset($image) ? '/img/260x280/' . $image['MiniatureUrl'] : "/i/nophoto.png",
						"noimage" => true,
						'header' => 'Товар есть ТОЛЬКО на витрине.<br>Продаем выставочный образец?',
						'buttons' => [
							[
								"title" => "Да, продаем!",
								"key" => "yes",
								"type" => "button",
								"sourceModalAction"=>"ok"
							],
							[
								"title" => "Нет!",
								"key" => "no",
								"type" => "-cancel",
								"sourceModalAction"=>"close"
							] 
						] 
					];
				} else if ($inShowRoom && !$onlyInShowRoom && $avail['AvailQuantity'] == $avail['WindowQuantity']) {
					$allowAdd = false;
					$result["onlyInShop"][] = [
						"id" => $complectGoodId,
						"title" => $good->GoodName,
						"img" => isset($image) ? '/img/260x280/' . $image['MiniatureUrl'] : "/i/nophoto.png",
						"noimage" => true,
						'header' => 'Как будем продавать?',
						'buttons' => [
							[
								"title" => "С витрины",
								"key" => "yes",
								"type" => "button" 
							],
							[
								"title" => "Подзаказ (см. даты)",
								"key" => "no",
								"type" => "button" 
							] 
						] 
					];
				}
			}
		}
		
		return new JsonModel($result);
	}

	public function checkAction() {
		// ini_set('display_errors', 1);
		$request = $this->getRequest();
		
		if (!$request->isXmlHttpRequest()) {
			return $this->notfound();
		}
		
		$result['state'] = 'ok';
		
		$items = [];
		$goodIds = $this->basket()->enumGoodIds();
		$goods = $this->catalog()->goods()->enumGoodsByIds($goodIds);
		
		$dateHelper = new DateHelper();
		
		$minDate = PHP_INT_MAX;
		$maxDate = 0;
		
		foreach ($this->basket()->getGoods() as $basketGood) {
			if (!$basketGood->getPickupDate()) {
				$basketGood->setPickupDate(date('Y-m-d'));
			}
			$pickupDate = \DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime($basketGood->getPickupDate())));
			if (false === $pickupDate) {
				$pickupDate = \DateTime::createFromFormat('Y-m-d', $basketGood->getPickupDate());
			}
			$minDate = min($minDate, $pickupDate->getTimestamp());
			$maxDate = max($maxDate, $pickupDate->getTimestamp());
		}
		
		$minDateText = $dateHelper->parseDate(DateHelper::DATE_FORMAT_WITHOUT_YEAR, $minDate);
		$maxDateText = $dateHelper->parseDate(DateHelper::DATE_FORMAT_WITHOUT_YEAR, $maxDate);
		
		if ($minDate < $maxDate) {
			$items = [];
			
			$item = [
				'title' => 'В корзине есть товары с разным сроком отгрузки <a href="#" class="expand-items">показать</a>',
				'mode' => 'redates',
				'id' => 0,
				'items_html' => '',
				'variants' => [
					Good::PICKUP_METHOD_WHOLE => [
						'title' => 'Отгрузить все с ' . $maxDateText,
						'details' => 'Заказ будет укомплектован и готов к отрузке с ' . $maxDateText 
					],
					Good::PICKUP_METHOD_MIN_MAX => [
						'title' => 'Частичная отгрузка',
						'details' => 'Оформить 2 заказа:<br />- отгрузить всё, что есть;<br />- остальное с ' . $maxDateText 
					] 
				] 
			];
			
			$byDates = [];
			foreach ($this->basket()->getGoods() as $basketGood) {
				if (!$basketGood->getPickupDate()) {
					$basketGood->setPickupDate(date('Y-m-d'));
				}
				$pickupDate = \DateTime::createFromFormat('Y-m-d H:i:s', $basketGood->getPickupDate());
				if (false === $pickupDate) {
					$pickupDate = \DateTime::createFromFormat('Y-m-d', $basketGood->getPickupDate());
				}
				
				$dateTimestamp = strtotime(date('Y-m-d', $pickupDate->getTimestamp()));
				if (!isset($byDates[$dateTimestamp])) {
					$byDates[$dateTimestamp] = [];
				}
				$byDates[$dateTimestamp][] = $basketGood;
			}
			if (1 < count($byDates)) {
				foreach ($byDates as $pickupTimestamp => $dateGoods) {
					$itemHtml = '';
					
					$title = 'В наличии';
					if (date('Y-m-d', time()) != date('Y-m-d', $pickupTimestamp)) {
						$title = 'Отгрузка с ' . $dateHelper->parseDate(DateHelper::DATE_FORMAT_WITHOUT_YEAR, $pickupTimestamp);
					}
					$itemHtml .= '<h3>' . $title . '</h3>';
					$itemHtml .= '<ul>';
					foreach ($dateGoods as $dateGood) {
						$itemHtml .= '<li>';
						$itemHtml .= '<a href="/goods/' . $dateGood->getId() . '/">';
						$itemHtml .= $dateGood->getId();
						$itemHtml .= '</a>';
						$itemHtml .= $goods[$dateGood->getId()]['GoodName'];
						if (1 < $dateGood->getQuantity()) {
							$itemHtml .= '<em>× ' . $dateGood->getQuantity() . ' шт</em>';
						}
						$itemHtml .= '</li>';
					}
					$itemHtml .= '</ul>';
					
					$item['items_html'] .= $itemHtml;
				}
				
				$items[] = $item;
			}
		}
		
		// foreach ($this->basket()->getGoods() as $basketGood) {
		// if ($basketGood->getSuborderQuantity() <= 0) {
		// continue;
		// }
		
		// $availQuantity = $basketGood->getQuantity() - $basketGood->getSuborderQuantity();
		
		// if ($availQuantity <= 0) {
		// continue;
		// }
		
		// $pickupDate = \DateTime::createFromFormat('Y-m-d', $basketGood->getPickupDate());
		// $pickupDateText = $dateHelper->parseDate(DateHelper::DATE_FORMAT_WITHOUT_YEAR, $pickupDate->getTimestamp());
		
		// $image = $this->catalog()->images()->getGoodMainImage($basketGood->getId());
		
		// $items[] = [
		// 'title' => $goods[$basketGood->getId()]['GoodName'],
		// 'content' => "Остал" . StringHelper::wordending($availQuantity, 'ась', 'ось', 'ось') . " только <strong>{$availQuantity}</strong> {$basketGood->getQuantityText($availQuantity)}",
		// 'content-title' => 'Раскупили!',
		// 'id' => $basketGood->getId(),
		// 'image' => isset($image) ? '/img/260x280/' . $image['MiniatureUrl'] : "/i/nophoto.png",
		// 'variants' => [
		// 1 => [
		// 'title' => 'Забрать частично',
		// 'details' => "{$availQuantity} {$basketGood->getQuantityText($availQuantity, 'accusative')} забрать сейчас,<br style='onlymobile'>а еще {$basketGood->getSuborderQuantity()} {$basketGood->getQuantityText($basketGood->getSuborderQuantity(), 'accusative')} — <strong>{$pickupDateText}</strong>"
		// ],
		// 2 => [
		// 'title' => 'Забрать, что есть',
		// 'details' => "Оформить заказ только на {$availQuantity} {$basketGood->getQuantityText($availQuantity, 'accusative')} в наличии"
		// ],
		// 3 => [
		// 'title' => "Забрать все, но {$pickupDateText}",
		// 'details' => "Мы подвезем желаемое количество<br>и отгрузим заказ целиком <strong>{$pickupDateText}</strong>"
		// ]
		// ]
		// ];
		// }
		
		if (count($items)) {
			$result['state'] = 'less';
			$result['modals'] = $items;
		}
		
		return new JsonModel($result);
	}

	public function setPickupMethodAction() {
		$request = $this->getRequest();
		
		if (!$request->isXmlHttpRequest()) {
			return $this->notfound();
		}
		
		$pickupMethodId = $request->getPost('id');
		$goodId = intval($request->getPost('item_id'));
		
		if (false) {
			if (!$this->basket()->hasGood($goodId) || !in_array($pickupMethodId, [
				1,
				2,
				3 
			])) {
				return new JsonModel([
					'state' => 'error' 
				]);
			}
		}
		
		if (0 < $goodId) {
			$basketGood = $this->basket()->getGoodById($goodId);
			$basketGood->setPickupMethodId($pickupMethodId);
		} else {
			foreach ($this->basket()->getGoods() as $basketGood) {
				$basketGood->setPickupMethodId($pickupMethodId);
			}
		}
		
		return new JsonModel([
			'state' => 'ok' 
		]);
	}

	public function changePriceColumnAction() {
		$regionId = $this->params('regionId');
		$this->updateUserRegionId($regionId);
		$priceColumn = $this->catalog()->prices()->helper()->getPriceColumnId($this->isPostDelivery());
		if (!$this->basket()->isEmpty()) {
			$prices = $this->catalog()->prices()->getPriceByGoodIds($this->basket()->enumGoodIds(), $this->getErpCityId(), $priceColumn);
			$this->basket()->recalcBasketPrices($prices);
		}
	}

}

?>
