<?php
namespace Application\Controller;

use SoloIdentity\Service\Ultima\Result\AuthResult;
use Zend\View\Model\JsonModel;

class LoginController extends BaseController
{
    public function indexAction() {
        if ($this->auth()->logged()) {
            return $this->redirect()->toUrl('/cabinet');
        }

        $this->getLayout()->setTitle('Вход на сайт');
        $this->getLayout()->setBodyClass('login');
        $this->getLayout()->addCssFile('login.css');
        $this->getLayout()->addJsFile('login.js');

        $vars = [];

        return $this->outputVars($vars);
    }

    public function loginAction() {
        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest()) {
            return $this->notfound();
        }

        $action = $request->getQuery('action');

        switch ($action) {
            case 'login':
                return $this->authenticate();
            case 'forgot':
                return $this->restorePassword();
        }

        return new JsonModel([
            'message' => 'Неизвестная команда',
            'state' => 'error',
        ]);
    }

    protected function authenticate() {
        $request = $this->getRequest();

        $login = $request->getQuery('login');
        $password = trim($request->getQuery('password'), 'q');

		$login = !preg_match('/@/', $login) ? preg_replace("/\D/", '', $login) : strtolower($login);

        $authResult = $this->auth()->login($login, $password);

        if ($authResult->success()) {
            $this->order()->clearStorage();

            return new JsonModel([
                'state' => 'ok',
                'redirect' => '/cabinet'
            ]);
        }

        switch ($authResult->getErrorCode()) {
            case AuthResult::ERROR_UNDEFINED:
                $error = "Ошибка авторизации. Повторите попытку позже.";
                break;
            case AuthResult::ERROR_LOGIN_PASS_PAIR_NOT_EXISTS:
                $error = "Нет указанной пары логин-пароль";
                break;
        }

        return new JsonModel([
            'state' => 'error',
            'message' => $error,
        ]);
    }

    protected function restorePassword() {
        $request = $this->getRequest();

        $login = $request->getQuery('login');

        $result = $this->identity()->users()->passwordReset(strtolower($login));

        return new JsonModel([
            'state' => 'ok',
            'password' => $result->Password,
        ]);
    }
}