<?php

namespace Application\Controller;

use Solo\Cookie\Cookie;
use Solo\Inflector\Inflector;
use Solo\Stdlib\ArrayHelper;
use SoloCatalog\Entity\Filters\FilterSet;
use SoloCatalog\Entity\Filters\SelectedFilter\Brands;
use SoloCatalog\Entity\Filters\SelectedFilter\Features;
use SoloCatalog\Entity\Filters\SelectedFilter\Limit;
use SoloCatalog\Entity\Filters\SelectedFilter\SelectedFilterSet;
use SoloCatalog\Entity\Goods\FilterOptionFeature;
use SoloCatalog\Entity\Goods\FilterOptions;
use SoloCatalog\Service\Helper\DateHelper;
use Zend\Http\Request;
use Zend\Paginator\Paginator;
use Zend\Stdlib\Parameters;
use Zend\View\Model\JsonModel;
use Solo\Db\QueryGateway\QueryGateway;
use Zend\Uri\Uri;
use Google\Analytics\Entity\Ecommerce\TrackerImpression;

class CatalogController extends BaseController {

	public function categoryAction() {
		$request = $this->getRequest();
		$action = $request->getQuery('action') ?  : $request->getPost('action');

		$categoryId = intval($this->params('id'));
		$category = $this->catalog()->categories()->getCategoryById($categoryId);

		$cityId = $this->getErpCityId();
		$isPostDelivery = $this->isPostDelivery();

		// sets
		$originalGetFeatureParameters = (isset($_GET['f']) ? $_GET['f'] : null);

		$sets = $this->catalog()->sets()->getCategorySets($categoryId);
		$setsGroups = $this->catalog()->sets()->getGroupsByCategoryId($categoryId);

		$currentSets = [];
		$selectedFeatures = [];
		if ($this->params('sets')) {
			$currentSets = $this->catalog()->sets()->getSetsByUids($this->params('sets'), $categoryId);

			foreach ($currentSets as $currentSet) {
				$query = [];
				if (!empty($currentSet['SelectedFeatures'])) {
					$setSelectedFeatures = json_decode($currentSet['SelectedFeatures'], true);
					if (is_array($setSelectedFeatures)) {
						foreach ($setSelectedFeatures as $selectedFeature) {
							$featureId = intval($selectedFeature['Id']);
							if (!isset($query[$featureId])) {
								$selectedFeatures[$featureId] = [];
							}

							// range
							if (isset($selectedFeature['ValueFrom']) && !empty($selectedFeature['ValueFrom']) && isset($selectedFeature['ValueTo']) && !empty($selectedFeature['ValueTo'])) {
								$selectedFeatures[$featureId] = [
									'min' => $selectedFeature['ValueFrom'],
									'max' => $selectedFeature['ValueTo']
								];
							}

							// value set
							if (isset($selectedFeature['ValueId']) && !empty($selectedFeature['ValueId'])) {
								$featureValueId = intval($selectedFeature['ValueId']);
								$selectedFeatures[$featureId][] = $featureValueId;
							}
						}
					}
				}
			}
		}
		// END sets

		if ($request->isXmlHttpRequest() && $action == 'get-filter') {
			$this->buildAndSetFilterQuery($request);
		}

		$selectedFilters = $this->catalog()->filters()->extractFiltersFromQuery($this->params());
		if (0 < count($selectedFeatures)) {
			ini_set('display_errors', 1);
			if (!$selectedFilters->has('features')) {
				$selectedFilters->add('features', new Features());
			}
			foreach ($selectedFeatures as $featureId => $selectedFeature) {
				if (isset($selectedFeature['min']) && isset($selectedFeature['max'])) {
					$selectedFilters->get('features')->addFeature($featureId, sprintf('%d_%d', $selectedFeature['min'], $selectedFeature['max']));
				}
			}
		}
		//print_r($selectedFilters); exit();

		if ($request->isXmlHttpRequest() && $action == 'reset-filter') {
			$filterId = $request->getPost('name');
			$filterValueId = $request->getPost('value');
			$urlFilters = $selectedFilters->copy();

			if (is_numeric($filterId)) {
				$urlFilters->features()->removeFeature($filterId, $filterValueId);
			} elseif ($filterId == 'brands') {
				$urlFilters->brands()->removeId($filterValueId);
			}

			$queryString = ltrim($urlFilters->toQueryString(), '/');

			if ($this->catalog()->goods()->hasUserDefinedSorting()) {
				$defaultSorting = $this->catalog()->goods()->getUserDefinedSorting();
				$queryString = $queryString . '&sort=' . $defaultSorting['column'] . '&dir=' . $defaultSorting['direction'];
			}

			$baseUrl = '/' . $category['CategoryUID'];
			if (0 < count($currentSets)) {
				foreach ($currentSets as $currentSet) {
					//$baseUrl .= '-' . $currentSet['SetUrlPart'];
				}
			}
			if ('_' !== substr($queryString, 0, 1)) {
				$baseUrl .= '/';
			}

			return new JsonModel([
				'href' => $baseUrl . $queryString,
				'state' => 'ok'
			]);
		}

		$options = $this->catalog()->goods()->createOptions();

		$this->setupOptionsPriceFiltration($options, $selectedFilters);
		$this->setupOptionsBrandsFiltration($options, $selectedFilters);
		$this->setupOptionsFeaturesFiltration($options, $selectedFilters);
		$this->setupOptionsLimitation($options, $selectedFilters);
		$sorting = $this->setupSorting($options);

		$priceRange = ($options->hasPriceRange() ? $options->getPriceRange() : null);

		$priceCategory = $this->catalog()->prices()->helper()->getPriceColumnId($this->isPostDelivery());
		$zoneId = $this->catalog()->prices()->getZoneId($cityId);

		$filters = new FilterSet();

		$setIds = [];
		foreach ($currentSets as $currentSet) {
			$setIds[] = $currentSet['SetID'];
		}
		$this->catalog()->filters()->fillFeatureFiltersByCategoryId($filters, $categoryId, $cityId, $isPostDelivery, $priceRange, $zoneId, $priceCategory, $setIds);
		$this->catalog()->filters()->fillBrandsFiltersByCategoryId($filters, $categoryId, $cityId, $isPostDelivery, $priceRange, $zoneId, $priceCategory, $setIds);

		$this->setupFiltersFeaturesSelection($filters, $options);
		$this->setupFiltersBrandsSelection($filters, $options);
		$this->setupRangeForNumericFilters($filters);

		$this->buildFiltersFeaturesUrls($filters, $selectedFilters);
		$this->buildFiltersBrandsUrls($filters, $selectedFilters);

		foreach ($selectedFeatures as $featureId => $selectedFeature) {
			if (isset($selectedFeature['min']) && isset($selectedFeature['max'])) {
				//$filters->selectRange($featureId, $selectedFeature['min'], $selectedFeature['max']);
			} elseif (is_array($selectedFeature)) {
				foreach ($selectedFeature as $featureValueId) {
					if (is_numeric($featureValueId)) {
						$filters->selectValue($featureId, $featureValueId);
					}
				}
			}
		}

		if (0 < $filters->countSelectedFilters()) {
			$nonPriceFilters = new FilterSet();
			if ($this->catalog()->filters()->hasOnlyOnePropertyTemplateInCategory($categoryId)) {
				$this->catalog()->filters()->fillFeatureFiltersByCategoryId($nonPriceFilters, $categoryId, $cityId, $isPostDelivery, null, $zoneId, $priceCategory, $setIds);
			}

			$this->catalog()->filters()->fillBrandsFiltersByCategoryId($nonPriceFilters, $categoryId, $cityId, $isPostDelivery, null, $zoneId, $priceCategory, $setIds);

			$this->setupFiltersFeaturesSelection($nonPriceFilters, $options);
			$this->setupFiltersBrandsSelection($nonPriceFilters, $options);

			$selectedNonPriceGoodIds = $nonPriceFilters->enumSelectedGoodIds();

			$priceRange = $this->catalog()->filters()->getPriceRange($selectedNonPriceGoodIds, $priceCategory, $zoneId);
		} else {
			$priceRange = $this->catalog()->filters()->getPriceRangeForCategory($categoryId, $cityId, $priceCategory, $zoneId, $isPostDelivery);
		}

		$goodIds = (0 < $filters->countSelectedFilters()) ? $filters->enumSelectedGoodIds() : $filters->enumGoodIds();

		if ($this->getRequest()->isXmlHttpRequest() && $action == 'get-filter') {
			$baseUrl = '/' . $category['CategoryUID'];
			if ((0 < count($currentSets)) && empty(urldecode($request->getQuery()->toString()))) {
				foreach ($currentSets as $currentSet) {
					$baseUrl .= '-' . $currentSet['SetUrlPart'];
				}
			}
			$baseUrl .= '/';
			$link = $baseUrl . '?' . urldecode($request->getQuery()->toString());
			if (isset($_GET['name']) && ('brands' == $_GET['name']) && isset($_GET['value'])) {
				if (1 == count($_GET['value'])) {
					$link = rtrim($baseUrl, '/') . $selectedFilters->toQueryString();
				} elseif (1 < count($_GET['value'])) {
					$link = $baseUrl . '?' . urldecode($request->getQuery()->toString());
				}
			}
			return new JsonModel([
				'count' => count($goodIds),
				'state' => 'ok',
				'href' => $link
			]);
		}

		foreach ($goodIds as $goodId) {
			$options->addGoodId($goodId);
		}

		$goods = $this->catalog()->goods()->enumFilterGoods($options, $cityId, $priceCategory, $zoneId, $categoryId);
		
		if (isset($_GET['debug_goods'])) {
			print_r($goods); exit();
		}

		if (count($goods)) {
			$currentPage = $options->getLimitation('currentPage');
			$itemCountPerPage = $options->getLimitation('perPage');
			$pagination = $this->createPagination($goodIds, $currentPage, $itemCountPerPage, $selectedFilters);
			foreach ($pagination['pages'] as $i => $page) {
				if (isset($page['url'])) {
					$uri = new Uri($page['url']);
					$query = $uri->getQueryAsArray();
					if (null !== $originalGetFeatureParameters) {
						$query['f'] = $originalGetFeatureParameters;
					} elseif (isset($query['f'])) {
						unset($query['f']);
					}
					foreach ($query as $n => $v) {
						$query[$n] = $n . '=' . $v;
					}

					$uri->setQuery(implode('&', $query));

					$url = '?';
					if ($uri->isValid()) {
						$url = $uri->toString();
					}
					$pagination['pages'][$i]['url'] = $url;
				}
			}

			$goodsImages = $this->catalog()->images()->getImagesByGoodIds(array_keys($goods));
			$goodsRemainsInTheMainStore = $this->catalog()->goods()->getGoodsRemainsByStoreId($goodIds, 1);

			$comparisonInfo = $this->comparer()->getComparisonCategoryInfo($category, $this->catalog());
			if ($comparisonInfo && (array_key_exists('goods', $comparisonInfo))) {
				foreach ($comparisonInfo['goods'] as $good) {
					if (array_key_exists($good->GoodID, $goods)) $goods[$good->GoodID]->Compared = true;
				}
			}
		}

		$menu = $this->createMenu();
		$categories = $menu->enumChildsByParentId(intval($categoryId), [
			$category['ParentID']
		]);

		$excludeFilters = [
			'limit',
			'brands'
		];
		$brandLabelInHeader = null;
		$usePageInCategoryName = false;
		if (!$selectedFilters->hasAnyFilter($excludeFilters)) {
			if ($selectedFilters->has('brands') && count($selectedFilters->get('brands')->enumIds()) == 1) {
				$brandLabelInHeader = $filters->getFilter('brands')->getValue('brand' . current(($selectedFilters->get('brands')->enumIds())))->getLabel();
			}
			if (isset($currentPage) && $currentPage > 1) {
				$usePageInCategoryName = true;
			}
		}
		$title = $category['CategoryName'];
		if (0 < count($currentSets)) {
			foreach ($currentSets as $currentSet) {
				$title .= ' ' . $currentSet['SetName'];
			}
		}
		if (!empty($brandLabelInHeader)) {
			$title .= ' ' . $brandLabelInHeader;
		}
		$title .= ' купить {cityName} в интернет магазине';
		if (empty($brandLabelInHeader) && !$usePageInCategoryName) {
			$title .= ' «Аквасант»';
		}
		$title .= ' с доставкой в {where}';
		if ($usePageInCategoryName) {
			$title .= ', страница ' . $currentPage;
		}

		$this->getLayout()->addCssFile('catalogue.css');
		$this->getLayout()->addJsFile('catalogue.js');
		$this->getLayout()->setTitle($this->getSeoTitle($title, $cityId));

		$setsTitle = 'Вас интересуют ' . $category['CategoryName'];
		if (0 < count($currentSets)) {
			foreach ($currentSets as $currentSet) {
				$setsTitle .= ' ' . $currentSet['SetName'];
			}
		}
		$setsTitle .= ', а какие еще можно купить ' . mb_strtolower($category['CategoryName']) . ' в {where}?';
		$setsTitle = $this->getSeoTitle($setsTitle, $cityId);

		$bannerService = $this->service('Application\Service\BannerService');

		$vars = [
			'filters' => $filters,
			'imageFilters' => $filters->getImageFilters(),
			'simpleFilters' => $filters->getSimpleFilters(),
			'selectedFilters' => $selectedFilters,
			'priceRange' => $priceRange,
			'goods' => $goods,
			'goodsImages' => $goodsImages,
			'pagination' => $pagination,
			'goodsRemainsInTheMainStore' => $goodsRemainsInTheMainStore,
			'category' => $category,
			'categories' => $categories,
			'sorting' => $sorting,
			'dateHelper' => new DateHelper(),
			'categoryId' => $categoryId,
			'banner' => $bannerService->getRandomBanner(0),
			'bannerBottom' => $bannerService->getRandomBanner(2),
			'brandNameForTitle' => $brandLabelInHeader,
			'sets' => $sets,
			'setsGroups' => $setsGroups,
			'currentSets' => $currentSets,
			'setsTitle' => $setsTitle,
			'selectedFeatures' => $selectedFeatures,
			'isWorkTime' => date('H') >= 10 && date('H') < 20
		];

		if (null !== $originalGetFeatureParameters) {
			$_GET['f'] = $originalGetFeatureParameters;
		} elseif (isset($_GET['f'])) {
			unset($_GET['f']);
		}

		// ecommerce tracker
		$ecommerceTracker = $this->createEcommerceTracker('category');
		$position = 1;
		foreach ($goods as $item) {
			$impression = new TrackerImpression();
			$impression->setId($item->getId());
			$impression->setName($item->getName());
			$impression->setCategoryName($category->CategoryName);
			$impression->setPrice(intval($item->getPrice('retail')));
			$impression->setBrandName($item->getBrandName());
			$impression->setListName('ProductList');
			$impression->setPosition($position);
			$position++;

			$ecommerceTracker->addImpression($impression);
		}
		$vars['ecommerceTracker'] = $ecommerceTracker;
		// END ecommerce tracker

		return $this->outputVars($vars);
	}

	/**
	 *
	 * Search results page
	 */
	public function searchAction() {
		$request = $this->getRequest();
		$action = $request->getQuery('action') ?  : $request->getPost('action');

		if ($request->isXmlHttpRequest() && $action == 'get-filter') {
			$this->buildAndSetFilterQuery($request);
		}

		$cityId = $this->getErpCityId();
		$isPostDelivery = $this->isPostDelivery();

		$searchGoods = $this->params()->fromRoute('goods');
		$searchedCategoryUid = $this->params()->fromRoute('category');

		$searchGoodIds = ArrayHelper::enumOneColumn($searchGoods, 'GoodID');

		$selectedFilters = $this->catalog()->filters()->extractFiltersFromQuery($this->params());

		if ($request->isXmlHttpRequest() && $action == 'reset-filter') {
			$filterId = $request->getPost('name');
			$filterValueId = $request->getPost('value');
			$urlFilters = $selectedFilters->copy();

			if (is_numeric($filterId)) {
				$urlFilters->features()->removeFeature($filterId, $filterValueId);
			} elseif ($filterId == 'brands') {
				$urlFilters->brands()->removeId($filterValueId, true);
			}

			$queryString = ltrim($urlFilters->toQueryString(), '/');

			if ($this->catalog()->goods()->hasUserDefinedSorting()) {
				$defaultSorting = $this->catalog()->goods()->getUserDefinedSorting();
				$queryString = $queryString . '&sort=' . $defaultSorting['column'] . '&dir=' . $defaultSorting['direction'];
			}

			$queryString = $queryString . '&q=' . $request->getQuery('q');

			return new JsonModel([
				'href' => $queryString,
				'state' => 'ok'
			]);
		}

		if (0 < sizeof($searchGoods)) {
			if ($selectedFilters->has('brands')) {
				$selectedFilters->get('brands')->routeParam(false);
			}
			if ($selectedFilters->has('limit')) {
				$selectedFilters->get('limit')->routeParam(false);
			}
			$options = $this->catalog()->goods()->createOptions();
			$priceCategory = $this->catalog()->prices()->helper()->getPriceColumnId($this->isPostDelivery());
			$zoneId = $this->catalog()->prices()->getZoneId($cityId);

			$this->setupOptionsPriceFiltration($options, $selectedFilters);
			$this->setupOptionsFeaturesFiltration($options, $selectedFilters);
			$this->setupOptionsBrandsFiltration($options, $selectedFilters);

			$sorting = $this->setupSorting($options);
			$vars['sorting'] = array(
				'uid' => $sorting['uid'],
				'column' => $options->getSorting('column'),
				'direction' => $options->getSorting('direction')
			);

			$this->setupOptionsLimitation($options, $selectedFilters);

			// fill filters data
			$filters = new FilterSet();
			$priceRange = ($options->hasPriceRange() ? $options->getPriceRange() : null);
			if ($this->catalog()->filters()->hasOnlyOnePropertyTemplateForGoods($searchGoodIds)) {
				$this->catalog()->filters()->fillFeatureFiltersByGoodIds($filters, $searchGoodIds, $cityId, $isPostDelivery, $priceRange);
			}

			$this->catalog()->filters()->fillBrandsFiltersByGoodIds($filters, $searchGoodIds, $cityId, $isPostDelivery, $priceRange);

			$this->setupFiltersFeaturesSelection($filters, $options);
			$this->setupFiltersBrandsSelection($filters, $options);

			$this->buildFiltersFeaturesUrls($filters, $selectedFilters);
			$this->buildFiltersBrandsUrls($filters, $selectedFilters);

			if (0 < $filters->countSelectedFilters()) {
				$nonPriceFilters = new FilterSet();
				if ($this->catalog()->filters()->hasOnlyOnePropertyTemplateForGoods($searchGoodIds)) {
					$this->catalog()->filters()->fillFeatureFiltersByGoodIds($nonPriceFilters, $searchGoodIds, $cityId, $isPostDelivery);
				}

				$this->catalog()->filters()->fillBrandsFiltersByGoodIds($nonPriceFilters, $searchGoodIds, $cityId);

				$this->setupFiltersFeaturesSelection($nonPriceFilters, $options);
				$this->setupFiltersBrandsSelection($nonPriceFilters, $options);
				$selectedNonPriceGoodIds = $nonPriceFilters->enumSelectedGoodIds();
				$priceRange = $this->catalog()->filters()->getPriceRange($selectedNonPriceGoodIds, $priceCategory, $zoneId);
			} else {
				$priceRange = $this->catalog()->filters()->getPriceRange($searchGoodIds, $priceCategory, $zoneId, $isPostDelivery);
			}

			$options->setPriceColumn(
				$this->auth()->logged() ? $this->catalog()->prices()->helper()->getColumnNumber($this->auth()->getUser()->getPriceCategoryId()) : $this->catalog()->prices()->helper()->getDefaultColumnNumber());
			// goods
			$goodIds = (0 < $filters->countSelectedFilters()) ? $filters->enumSelectedGoodIds() : $filters->enumGoodIds();

			if ($this->getRequest()->isXmlHttpRequest() && $action == 'get-filter') {
				return new JsonModel([
					'count' => count($goodIds),
					'state' => 'ok',
					'href' => '?' . urldecode($request->getQuery()->toString())
				]);
			}

			foreach ($goodIds as $goodId) {
				$options->addGoodId($goodId);
			}

			if (empty($searchedCategoryUid)) {
				$searchedCategories = $this->catalog()->categories()->getCategoriesByGoodsIds($options->enumGoodIds());
			}

			$goods = $this->catalog()->goods()->enumFilterGoods($options, $cityId, $priceCategory, $zoneId, $searchedCategories);

			if (count($goods)) {
				$currentPage = $options->getLimitation('currentPage');
				$itemCountPerPage = $options->getLimitation('perPage');
				$pagination = $this->createPagination($goodIds, $currentPage, $itemCountPerPage, $selectedFilters, true);

				$goodsImages = $this->catalog()->images()->getImagesByGoodIds(array_keys($goods));

				$comparedGoodIds = $this->comparer()->getGoodIds();

				$goodsRemainsInTheMainStore = $this->catalog()->goods()->getGoodsRemainsByStoreId($goodIds, 1);

				foreach ($comparedGoodIds as $goodId) {
					if (array_key_exists($goodId, $goods)) {
						$goods[$goodId]->Compared = true;
					}
				}
			}

			$this->getLayout()->setTitle($this->params()->fromQuery('q') . ' купить в интернет магазине с доставкой');

			$this->getLayout()->addCssFile('catalogue.css');
			$this->getLayout()->addJsFile('catalogue.js');

			$bannerService = $this->service('Application\Service\BannerService');

			$vars = [
				'filters' => $filters,
				'imageFilters' => $filters->getImageFilters(),
				'simpleFilters' => $filters->getSimpleFilters(),
				'selectedFilters' => $selectedFilters,
				'priceRange' => $priceRange,
				'goods' => $goods,
				'goodsImages' => $goodsImages,
				'goodsRemainsInTheMainStore' => $goodsRemainsInTheMainStore,
				'pagination' => $pagination,
				'sorting' => $sorting,
				'dateHelper' => new DateHelper(),
				'banner' => $bannerService->getRandomBanner(0)
			];

			// ecommerce tracker
			$ecommerceTracker = $this->createEcommerceTracker('search');
			$position = 1;
			foreach ($goods as $item) {
				$impression = new TrackerImpression();
				$impression->setId($item->getId());
				$impression->setName($item->getName());
				$impression->setCategoryName($item->getCategoryName());
				$impression->setPrice(intval($item->getPrice('retail')));
				$impression->setBrandName($item->getBrandName());
				$impression->setListName('SearchList');
				$impression->setPosition($position);
				$position++;

				$ecommerceTracker->addImpression($impression);
			}
			$vars['ecommerceTracker'] = $ecommerceTracker;
			// END ecommerce tracker
		} else {
			return $this->outputVars()->setTemplate('application/catalog/search-empty');
		}

		return $this->outputVars($vars)->setTemplate('application/catalog/category');
	}

	/**
	 *
	 * @return \Zend\View\Model\JsonModel
	 */
	public function polygonAction() {
		$authenticated = false;

		if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
			$user = $_SERVER['PHP_AUTH_USER'];
			$pass = $_SERVER['PHP_AUTH_PW'];
			if ($user == "aquasant" && $pass == "superdata") {
				$authenticated = true;
			}
		}

		if (!$authenticated) {
			header('WWW-Authenticate: Basic realm="Restricted Area"');
			header('HTTP/1.1 401 Unauthorized');
			echo ("Access denied.");
			exit();
		}

		$request = $this->getRequest();
		$action = $request->getQuery('action') ?  : $request->getPost('action');

		if ($request->isXmlHttpRequest() && $action == 'get-filter') {
			$this->buildAndSetFilterQuery($request);
		}

		$cityId = $this->getErpCityId();
		$isPostDelivery = $this->isPostDelivery();

		$searchedCategoryUid = $this->params()->fromRoute('category');

		$searchGoodIds = [];
		$queryGateway = new QueryGateway();
		$sql = "SELECT DISTINCT GoodID FROM #all_goods#";
		$rows = $queryGateway->query($sql);
		foreach ($rows as $row) {
			$searchGoodIds[] = $row['GoodID'];
		}

		$selectedFilters = $this->catalog()->filters()->extractFiltersFromQuery($this->params());

		if ($request->isXmlHttpRequest() && $action == 'reset-filter') {
			$filterId = $request->getPost('name');
			$filterValueId = $request->getPost('value');
			$urlFilters = $selectedFilters->copy();

			if (is_numeric($filterId)) {
				$urlFilters->features()->removeFeature($filterId, $filterValueId);
			} elseif ($filterId == 'brands') {
				$urlFilters->brands()->removeId($filterValueId, true);
			}

			$queryString = ltrim($urlFilters->toQueryString(), '/');

			if ($this->catalog()->goods()->hasUserDefinedSorting()) {
				$defaultSorting = $this->catalog()->goods()->getUserDefinedSorting();
				$queryString = $queryString . '&sort=' . $defaultSorting['column'] . '&dir=' . $defaultSorting['direction'];
			}

			$queryString = $queryString . '&q=' . $request->getQuery('q');

			return new JsonModel([
				'href' => $queryString,
				'state' => 'ok'
			]);
		}

		if (0 < sizeof($searchGoodIds)) {
			if ($selectedFilters->has('brands')) {
				$selectedFilters->get('brands')->routeParam(false);
			}
			if ($selectedFilters->has('limit')) {
				$selectedFilters->get('limit')->routeParam(false);
			}
			$options = $this->catalog()->goods()->createOptions();
			$priceCategory = $this->catalog()->prices()->helper()->getPriceColumnId($this->isPostDelivery());
			$zoneId = $this->catalog()->prices()->getZoneId($cityId);

			$this->setupOptionsPriceFiltration($options, $selectedFilters);
			$this->setupOptionsFeaturesFiltration($options, $selectedFilters);
			$this->setupOptionsBrandsFiltration($options, $selectedFilters);

			$sorting = $this->setupSorting($options);
			$vars['sorting'] = array(
				'uid' => $sorting['uid'],
				'column' => $options->getSorting('column'),
				'direction' => $options->getSorting('direction')
			);

			$this->setupOptionsLimitation($options, $selectedFilters);

			// fill filters data
			$filters = new FilterSet();
			$priceRange = ($options->hasPriceRange() ? $options->getPriceRange() : null);
			if ($this->catalog()->filters()->hasOnlyOnePropertyTemplateForGoods($searchGoodIds)) {
				$this->catalog()->filters()->fillFeatureFiltersByGoodIds($filters, $searchGoodIds, $cityId, $isPostDelivery, $priceRange);
			}

			$this->catalog()->filters()->fillBrandsFiltersByGoodIds($filters, $searchGoodIds, $cityId, $isPostDelivery, $priceRange);

			$this->setupFiltersFeaturesSelection($filters, $options);
			$this->setupFiltersBrandsSelection($filters, $options);

			$this->buildFiltersFeaturesUrls($filters, $selectedFilters);
			$this->buildFiltersBrandsUrls($filters, $selectedFilters);

			if (0 < $filters->countSelectedFilters()) {
				$nonPriceFilters = new FilterSet();
				if ($this->catalog()->filters()->hasOnlyOnePropertyTemplateForGoods($searchGoodIds)) {
					$this->catalog()->filters()->fillFeatureFiltersByGoodIds($nonPriceFilters, $searchGoodIds, $cityId, $isPostDelivery);
				}

				$this->catalog()->filters()->fillBrandsFiltersByGoodIds($nonPriceFilters, $searchGoodIds, $cityId);

				$this->setupFiltersFeaturesSelection($nonPriceFilters, $options);
				$this->setupFiltersBrandsSelection($nonPriceFilters, $options);
				$selectedNonPriceGoodIds = $nonPriceFilters->enumSelectedGoodIds();
				$priceRange = $this->catalog()->filters()->getPriceRange($selectedNonPriceGoodIds, $priceCategory, $zoneId);
			} else {
				$priceRange = $this->catalog()->filters()->getPriceRange($searchGoodIds, $priceCategory, $zoneId, $isPostDelivery);
			}

			$options->setPriceColumn(
				$this->auth()->logged() ? $this->catalog()->prices()->helper()->getColumnNumber($this->auth()->getUser()->getPriceCategoryId()) : $this->catalog()->prices()->helper()->getDefaultColumnNumber());
			// goods
			$goodIds = (0 < $filters->countSelectedFilters()) ? $filters->enumSelectedGoodIds() : $filters->enumGoodIds();

			if ($this->getRequest()->isXmlHttpRequest() && $action == 'get-filter') {
				return new JsonModel([
					'count' => count($goodIds),
					'state' => 'ok',
					'href' => '?' . urldecode($request->getQuery()->toString())
				]);
			}

			foreach ($goodIds as $goodId) {
				$options->addGoodId($goodId);
			}

			if (empty($searchedCategoryUid)) {
				$searchedCategories = $this->catalog()->categories()->getCategoriesByGoodsIds($options->enumGoodIds());
			}

			$goods = $this->catalog()->goods()->enumFilterGoods($options, $cityId, $priceCategory, $zoneId, $searchedCategories);

			if (count($goods)) {
				$currentPage = $options->getLimitation('currentPage');
				$itemCountPerPage = $options->getLimitation('perPage');
				$pagination = $this->createPagination($goodIds, $currentPage, $itemCountPerPage, $selectedFilters, true);

				$goodsImages = $this->catalog()->images()->getImagesByGoodIds(array_keys($goods));

				$comparedGoodIds = $this->comparer()->getGoodIds();

				$goodsRemainsInTheMainStore = $this->catalog()->goods()->getGoodsRemainsByStoreId($goodIds, 1);

				foreach ($comparedGoodIds as $goodId) {
					if (array_key_exists($goodId, $goods)) {
						$goods[$goodId]->Compared = true;
					}
				}
			}

			$this->getLayout()->setTitle('Тестовый полигон');

			$this->getLayout()->addCssFile('catalogue.css');
			$this->getLayout()->addJsFile('catalogue.js');

			$bannerService = $this->service('Application\Service\BannerService');

			$vars = [
				'filters' => $filters,
				'imageFilters' => $filters->getImageFilters(),
				'simpleFilters' => $filters->getSimpleFilters(),
				'selectedFilters' => $selectedFilters,
				'priceRange' => $priceRange,
				'goods' => $goods,
				'goodsImages' => $goodsImages,
				'goodsRemainsInTheMainStore' => $goodsRemainsInTheMainStore,
				'pagination' => $pagination,
				'sorting' => $sorting,
				'dateHelper' => new DateHelper(),
				'banner' => $bannerService->getRandomBanner(0),
				'isPolygon' => true
			];
		}

		return $this->outputVars($vars)->setTemplate('application/catalog/category');
	}

	/**
	 *
	 * @param array $goodIds
	 * @param integer $currentPage
	 * @param integer $perPage
	 * @param SelectedFilterSet $selectedFilters
	 * @param boolean $forceAsGetParam
	 * @return array
	 */
	private function createPagination(array $goodIds, $currentPage, $perPage, SelectedFilterSet $selectedFilters, $forceAsGetParam = false) {
		$paginator = new Paginator(new \Zend\Paginator\Adapter\ArrayAdapter($goodIds));
		$paginator->setCurrentPageNumber($currentPage);
		$paginator->setDefaultItemCountPerPage($perPage);
		$paginator->setPageRange(5);
		$pages = $paginator->getPages('sliding');

		$pagination = array(
			'current' => $pages->current,
			'previous' => $pages->previous,
			'next' => $pages->next,
			'first' => $pages->first,
			'last' => $pages->last,
			'pages' => []
		);

		$pagesInRange = $pages->pagesInRange;
		if ($pages->firstPageInRange != $pages->first) {
			if (2 != $pages->firstPageInRange) {
				$pagesInRange[2] = 'delimiter';
			}
			$pagesInRange[1] = 1;
		}
		if ($pages->lastPageInRange < $pages->last) {
			if (($pages->last - 1) != $pages->lastPageInRange) {
				$pagesInRange[$pages->last - 1] = 'delimiter';
			}
			$pagesInRange[$pages->last] = $pages->last;
		}
		ksort($pagesInRange);

		foreach ($pagesInRange as $page => $pageName) {
			if (false === strpos($pageName, 'delimiter')) {
				$urlFilters = $selectedFilters->copy();

				if (!$urlFilters->has('limit')) {
					$urlFilters->add('limit', new Limit());
				}

				if ($page > 1) {
					if ($forceAsGetParam) {
						$urlFilters->limit()->routeParam(false);
					}
					$urlFilters->limit()->setPage($page);
				} else {
					$urlFilters->remove('limit');
				}

				$url = $urlFilters->toQueryString();
				$uri = new Uri($url);
				$path = $this->getRequest()->getUri()->getPath();
				if ($uri->getPath()) {
					$path = rtrim($path, '/');
					$path .= $uri->getPath();
				}
				$uri->setPath($path);
				// $url = $uri->toString();

				$rangePaginator = new Paginator(new \Zend\Paginator\Adapter\ArrayAdapter($goodIds));
				$rangePaginator->setCurrentPageNumber($page);
				$cpages = $rangePaginator->getPages();
				$range = array(
					'from' => $cpages->firstItemNumber,
					'to' => $cpages->lastItemNumber
				);

				$pagination['pages'][$page] = array(
					'number' => $page,
					'url' => $url,
					'range' => $range
				);
			} else {
				$pagination['pages'][$page] = 'delimiter';
			}
		}

		// print_r($pagination); exit();

		return $pagination;
	}

	/**
	 *
	 * @param FilterOptions $options
	 * @param SelectedFilterSet $selectedFilters
	 */
	private function setupOptionsPriceFiltration(FilterOptions $options, SelectedFilterSet $selectedFilters) {
		if ($selectedFilters->has('price')) {
			$priceFilter = $selectedFilters->price();

			$options->setPriceRange(
				$priceFilter->getFrom(),
				$priceFilter->getTo(),
				$this->catalog()->prices()->helper()->getPriceColumnId($this->isPostDelivery()),
				$this->catalog()->prices()->getZoneId($this->facility()->cities()->getCityId()));
		}
	}

	/**
	 *
	 * @param FilterOptions $options
	 * @param SelectedFilterSet $selectedFilters
	 */
	private function setupOptionsFeaturesFiltration(FilterOptions $options, SelectedFilterSet $selectedFilters) {
		if ($selectedFilters->has('features')) {
			foreach ($selectedFilters->features()->enumFeatures() as $id => $valueIds) {
				foreach ($valueIds as $valueId) {
					$options->addFeatureFilter(new FilterOptionFeature($id, $valueId));
				}
			}
		}
	}

	/**
	 *
	 * @param FilterOptions $options
	 * @param SelectedFilterSet $selectedFilters
	 */
	private function setupOptionsBrandsFiltration(FilterOptions $options, SelectedFilterSet $selectedFilters) {
		if ($selectedFilters->has('brands')) {
			$options->addBrandId($selectedFilters->brands()->enumIds());
		}
	}

	/**
	 *
	 * @param FilterSet $filters
	 * @param FilterOptions $options
	 */
	private function setupFiltersFeaturesSelection(FilterSet $filters, FilterOptions $options) {
		if ($options->hasFeatures()) {
			$features = $options->getFeatureFilters();
			foreach ($features as $feature) {
				$filters->selectValue($feature->getId(), $feature->getValueId());
			}
		}
	}

	/**
	 *
	 * @param FilterSet $filters
	 * @param FilterOptions $options
	 */
	private function setupFiltersBrandsSelection(FilterSet $filters, FilterOptions $options) {
		if ($options->hasBrands()) {
			$brands = $options->getBrandIds();
			foreach ($brands as $brand) {
				$value = (is_numeric($brand) ? 'brand' . $brand : 'brand' . Inflector::camelize($brand));
				$filters->selectValue('brands', $value);
			}
		}
	}

	/**
	 *
	 * @param FilterOptions $options
	 * @param SelectedFilterSet $selectedFilters
	 */
	private function setupOptionsLimitation(FilterOptions $options, SelectedFilterSet $selectedFilters) {
		if ('all' == $this->params()->fromQuery('p')) {
			Cookie::set('catalogShowAll', 1);
		}
		$currentPage = 1;
		if ($selectedFilters->has('limit')) {
			$currentPage = $selectedFilters->limit()->getPage();
			Cookie::killCookie('catalogShowAll');
		}
		if (!Cookie::exists('catalogShowAll') || (1 != Cookie::get('catalogShowAll'))) {
			$options->setLimitation($this->catalog()->goods()->getDefaultValue('goods_per_page'), $currentPage);
		}
	}

	/**
	 *
	 * @param FilterOptions $options
	 * @return array
	 */
	private function setupSorting(FilterOptions $options) {
		$sortingUid = $this->params()->fromQuery('sort') ?  : $this->catalog()->goods()->getDefaultValue('sort_column');
		$sortingColumn = $this->catalog()->goods()->getSortingColumn($sortingUid);
		$sortingDirection = $this->params()->fromQuery('dir') ?  : $this->catalog()->goods()->getDefaultValue('sort_direction');
		$sortingDirection = $this->catalog()->goods()->getSortingDirection($sortingDirection);

		$sortingParam = null;

		// пока оставил старую логику, вообще это надо выкинуть все. с сортировками перемудрили
		if ((null !== $this->params()->fromQuery('sort')) && ('' !== trim($this->params()->fromQuery('sort')))) {

			// устанавливаем сортировку для каталога
			$dir = $this->params()->fromQuery('dir');
			if (null !== $dir && ('' !== trim($dir))) {
				$userSort = $this->params()->fromQuery('dir');
			} else {
				$userSort = $this->catalog()->goods()->getDefaultValue('sort_direction');
			}

			$this->catalog()->goods()->setUserDefinedSorting($this->params()->fromQuery('sort'), $userSort);
		}

		$options->setSorting($sortingColumn, $sortingDirection, $sortingParam);

		return [
			'uid' => $sortingUid,
			'column' => $sortingColumn,
			'direction' => $sortingDirection,
			'param' => $sortingParam
		];
	}

	/**
	 *
	 * @param FilterSet $filters
	 */
	private function setupRangeForNumericFilters(FilterSet $filters) {
		foreach ($filters as $filter) {
			if ($filter->getType() == 1) {
				$filter->setMinValue();
				$filter->setMaxValue();
				$filter->setStepValue();
			}
		}
	}

	/**
	 *
	 * @param FilterSet $filters
	 * @param SelectedFilterSet $selectedFilters
	 */
	private function buildFiltersFeaturesUrls(FilterSet $filters, SelectedFilterSet $selectedFilters) {
		$removeFeatures = [];
		foreach ($filters as $filter) {
			if (!$filter->isEmpty() && ('warehouses' != $filter->getId()) && ('brands' != $filter->getId())) {

				// собираем выбранные фичи которые стали пустыми
				// их надо выкинуть из фильтра
				foreach ($filter->enumEmptySelectedValues() as $filterValue) {
					$removeFeatures[$filter->getId()][] = $filterValue->getId();
				}
			}
		}

		foreach ($filters as $filter) {
			if (!$filter->isEmpty() && ('warehouses' != $filter->getId()) && ('brands' != $filter->getId())) {
				// формируем ссылки для каждой фичи
				if ($filter->getType() == 1) {
					$urlFilters = $selectedFilters->copy();
					$urlFilters->remove('limit');
					if (!$urlFilters->has('features')) {
						$urlFilters->add('features', new Features());
					}
					if (($selectedFilters->get('features')) && ($selectedFilters->get('features')->isFilterSelect($filter->getId()))) {
						$selectedRangeValue = $selectedFilters->get('features')->getFilterById($filter->getId());
						$urlFilters->features()->removeFeature($filter->getId(), key($selectedRangeValue));
						$urlFilters->features()->removeFeature($filter->getId(), "@min@_@max@");
					}
					$filter->setUrlUnsetFilter($urlFilters->toQueryString());
					$urlFilters->features()->addFeature($filter->getId(), "@min@_@max@");
					$filter->setUrlSetFilter($urlFilters->toQueryString());
				}
				foreach ($filter->enumNonEmptyValues() as $filterValue) {
					$urlFilters = $selectedFilters->copy();
					$urlFilters->remove('limit');
					if (!$urlFilters->has('features')) {
						$urlFilters->add('features', new Features());
					}

					$urlFilters->features()->addFeature($filter->getId(), $filterValue->getId());

					// выкидываем все лишние фичи
					foreach ($removeFeatures as $filterId => $remValues) {
						foreach ($remValues as $remVal) {
							$urlFilters->features()->removeFeature($filterId, $remVal);
						}
					}
					$filterValue->additional('url', $urlFilters->toQueryString());
					$urlFilters->features()->removeFeature($filter->getId(), $filterValue->getId());
					$filterValue->additional('deleteUrl', $urlFilters->toQueryString());
				}
			}
		}
	}

	private function buildFiltersBrandsUrls(FilterSet $filters, SelectedFilterSet $selectedFilters, $forseAsGetParam = false) {
		foreach ($filters->getFilter('brands')->enumNonEmptyValues() as $filterValue) {
			$urlFilters = $selectedFilters->copy();
			$urlFilters->remove('limit');
			if (!$urlFilters->has('brands')) {
				$urlFilters->add('brands', new Brands());
			}
			$urlFilters->brands()->addId($filterValue->getTitle(), $forseAsGetParam);
			if (!is_null($filterValue->additional('uid'))) {
				$urlFilters->brands()->addUid($filterValue->getTitle(), $filterValue->additional('uid'));
			}
			$filterValue->additional('url', $urlFilters->toQueryString());
			$urlFilters->brands()->removeId($filterValue->getTitle(), $forseAsGetParam);
			$filterValue->additional('deleteUrl', $urlFilters->toQueryString());
		}
	}

	protected function buildAndSetFilterQuery(Request $request) {
		$filters = $request->getQuery('all-filters');

		$filterIds = array_filter(array_keys($filters), function ($value) {
			return is_int($value);
		});

		if (count($filterIds)) {
			$filterTypes = $this->catalog()->filters()->getFilterTypesByIds($filterIds);
		}

		$features = $brands = $params = [];

		foreach ($filters as $filterId => $filterValues) {

			if (is_numeric($filterId) && !empty($filterTypes)) {
				if ($filterTypes[$filterId] == 1) {
					$params['rmin'][$filterId] = $filterValues[0];
					$params['rmax'][$filterId] = $filterValues[1];
				} else {
					$features[] = $filterId . ':' . join(',', $filterValues);
				}
			} elseif ($filterId == 'brands') {
				$brands = join(',', $filterValues);
			} elseif ($filterId == 'price') {
				$params['pricemin'] = $filterValues[0];
				$params['pricemax'] = $filterValues[1];
			}
		}

		if (!empty($features)) {
			$features = join(';', $features);
			$params['f'] = $features;
		}

		if (!empty($brands)) {
			$params['b'] = $brands;
		}

		$oldParams = $request->getQuery()->toArray();
		unset($oldParams['action']);
		unset($oldParams['name']);
		unset($oldParams['value']);
		unset($oldParams['all-filters']);

		$queryFilter = [];
		if (isset($oldParams['f']) && !empty($oldParams['f'])) {
			foreach (explode(';', $oldParams['f']) as $v) {
				list($propertyId, $propertyValueIds) = explode(':', $v);
				if (!isset($queryFilter[$propertyId])) {
					$queryFilter[$propertyId] = [];
				}
				foreach (explode(',', $propertyValueIds) as $propertyValueId) {
					$propertyValueId = intval($propertyValueId);
					$queryFilter[$propertyId][] = $propertyValueId;
				}
			}
		}
		if (isset($params['f']) && !empty($params['f'])) {
			foreach (explode(';', $params['f']) as $v) {
				list($propertyId, $propertyValueIds) = explode(':', $v);
				if (!isset($queryFilter[$propertyId])) {
					$queryFilter[$propertyId] = [];
				}
				foreach (explode(',', $propertyValueIds) as $propertyValueId) {
					$propertyValueId = intval($propertyValueId);
					$queryFilter[$propertyId][] = $propertyValueId;
				}
			}
		}
		foreach ($queryFilter as $i => $v) {
			$queryFilter[$i] = $i . ':' . implode(',', $v);
		}
		if (0 < count($queryFilter)) {
			$oldParams['f'] = implode(';', $queryFilter);
			$params['f'] = implode(';', $queryFilter);
		}

		$params = array_merge($oldParams, $params);

		$newQuery = new Parameters($params);
		foreach ($params as $name => $value) {
			$_GET[$name] = $value;
		}

		$request->setQuery($newQuery);
	}

}
