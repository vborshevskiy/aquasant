<?php

namespace Application\Controller;

use SoloERP\Service\ProvidesWebservice;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use Google\Analytics\Entity\Transaction;
use Google\Analytics\Entity\TransactionItem;

class DebugController extends BaseController {

	use ProvidesWebservice;

	public function indexAction() {
		$basketGood = $this->basket()->getGoodById(146094);
		$good = $this->catalog()->goods()->getGoodByGoodId(146094);
		print_r($basketGood); print_r($good); exit();
		
		ini_set('display_errors', 1);
		
		$clientId = $this->google()->analytics()->getClientId();
		$transaction = new Transaction();
		$transaction->setId('324234');
		$transaction->setAffiliation('santehbaza.ru-crm');
		$transaction->setRevenue(999);
		$transaction->setTax(0);
		$transaction->setShipping(100);
		
		$item = new TransactionItem();
		$item->setSku(777);
		$item->setName('Name#1');
		$item->setPrice(44);
		$item->setQuantity(2);
		$item->setCategory('Category#1');
		$item->setBrand('Brand#1');
		$item->setVariant('');
		$transaction->addItem($item);
		
		//$result = $this->google()->analytics()->ecommerce()->sendExtendedTransaction($clientId, $transaction);
		var_dump($result);
		
		
		print "OK"; exit();


		$pRedis = $this->getRedisClient();
		$params = [
			'StoreIds' => [
				1,
				4,
				6,
				10,
				12,
				13,
				15,
				16,
				17,
				5,
				8,
				7,
				9
			]
		];
		$rdr = new UltimaJsonListReader($this->callWebMethod('RedisGetProductRemains', $params));
		if (!$rdr->isEmpty()) {
			$keys = $rdr->getValue("Keys");
			if (count($keys) > 0) {
				foreach ($keys as $row) {
					$pack = json_decode($pRedis->get($row));
					foreach ($pack as $item) {
						print_r($item);
						exit();
					}
				}
			}
		}

		exit();
	}

	/**
	 *
	 * @return \Predis\Client
	 */
	private function getRedisClient() {
		$result = null;

		$client = $this->getServiceLocator()->get('PredisClient');
		if ($client instanceof \Predis\Client) {
			$result = $client;
		}

		return $result;
	}

	public function ymlfeedAction() {
		print '<a href="https://www.'.DOMAIN_NAME.'/yml/yml_msk.xml">Feed link: moscow</a>';
		exit();
	}
}

?>