<?php

namespace Application\Controller;

class PageController extends BaseController
{
    public function aboutAction()
    {
        $this->getLayout()->addCssFile('baza.css');
        $this->getLayout()->setBodyClass('baza');

        $this->getLayout()->setTitle('Оптово-розничная база «Аквасант», Aquasant');
        $this->getLayout()->addMetaKeywords('Аквасант, Aquasant, оптово-розничная, база, сантехники');
        $this->getLayout()->addMetaDescription('Информация об оптово-розничной базе сантехники «Аквасант», Aquasant — вся сантехника из одного места');

        return $this->outputVars([]);
    }

    public function deliveryAction()
    {
        $this->getLayout()->addCssFile('static.css');
        $this->getLayout()->setBodyClass('static');

        $this->getLayout()->setTitle('Аквасант: доставка сантехники');
        $this->getLayout()->addMetaKeywords('доставка, сантехники, москва, санкт-петербург, аквасант');
        $this->getLayout()->addMetaDescription('доставка сантехники с оптово-розничной базы Аквасант');

        return $this->outputVars([]);
    }

    public function paymentsAction()
    {
        $this->getLayout()->addCssFile('static.css');
        $this->getLayout()->setBodyClass('static');

        $this->getLayout()->setTitle('Аквасант: оплата заказов');
        $this->getLayout()->addMetaKeywords('оплата, заказов, наличные, карты, безнал, аквасант');
        $this->getLayout()->addMetaDescription('оплата заказов с оптово-розничной базы Аквасант');

        return $this->outputVars([]);
    }

    public function refundAction()
    {
        $this->getLayout()->addCssFile('static.css');
        $this->getLayout()->setBodyClass('static');

        $this->getLayout()->setTitle('Аквасант: возврат сантехники');
        $this->getLayout()->addMetaKeywords('возврат, сантехники, аквасант');
        $this->getLayout()->addMetaDescription('возврат сантехники с оптово-розничной базы Аквасант');

        return $this->outputVars([]);
    }

    public function installAction()
    {
        $this->getLayout()->addCssFile('static.css');
        $this->getLayout()->setBodyClass('static');

        $this->getLayout()->setTitle('Аквасант: установка сантехники');
        $this->getLayout()->addMetaKeywords('установка, сантехники, аквасант');
        $this->getLayout()->addMetaDescription('установка сантехники, купленной на оптово-розничной базе Аквасант');

        return $this->outputVars([]);
    }
    
    public function agreementAction()
    {
    	$this->getLayout()->addCssFile('static.css');
    	$this->getLayout()->setBodyClass('static');
    
    	$this->getLayout()->setTitle('Политика конфиденциальности в интернет-магазине «Aquasant», согласие на обработку персональных данных клиентов-физических лиц');
    	$this->getLayout()->addMetaKeywords('установка, сантехники, аквасант');
    	$this->getLayout()->addMetaDescription('Политика конфиденциальности в интернет-магазине «Aquasant», согласие на обработку персональных данных клиентов-физических лиц');
    
    	return $this->outputVars([]);
    }
}