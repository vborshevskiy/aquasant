<?php

namespace Application\Controller;

use Zend\View\Model\JsonModel;

class CompareController extends BaseController {

    public function indexAction() {
        $this->getLayout()->setTitle('Сравнение товаров интернет магазина Аквасант');
        $this->getLayout()->setBodyClass('compare');
        $this->getLayout()->addCssFile('compare.css');
        $this->getLayout()->addJsFile('compare.js');

        $categoryUID = $this->params()->fromRoute('category');

        if (!$categoryUID) {
            return $this->redirect()->toRoute('home');
        }
        $category = $this->catalog()->categories()->getCategoryByUid($categoryUID);
        if (!$category) {
            return $this->redirect()->toRoute('home');
        }
        $requestGoodIds = array_map('intval', explode(',',$this->params()->fromQuery('ids')));
        $goodIds = $this->catalog()->goods()->getGoodIdsInSoftCategoryId($requestGoodIds,$category->SoftCategoryID);

        if (count($goodIds) == 0) {
            return $this->redirect()->toRoute('home');
        }

        $comparisonInfo = $this->comparer()->getComparisonCategoryInfoByGoodIds($category, $this->catalog(), $goodIds);

        $propertyTemplateId = $this->params()->fromQuery('ptid');
        if ($propertyTemplateId) {
            $comparisonInfo = $this->comparer()->findByPropertyTemplateID($propertyTemplateId, $category, $this->catalog());
        }

        if (!$comparisonInfo) {
            return $this->redirect()->toRoute('home');
        }

        if (($comparisonInfo != false) && (array_key_exists('goods', $comparisonInfo))) {
            foreach ($comparisonInfo['goods'] as $good) {
                $itemIds[] = $good->GoodID;
                $good->SeoWord = $this->catalog()->goods()->getRandomWord();
                if ($this->basket()->hasGood($good->GoodID)) {
                    $good->InBasket = $this->basket()->getGoodQuantity($good->GoodID);
                } else {
                    $good->InBasket = 0;
                }
            }

            $cityId = $this->getErpCityId();
            $priceColumn = $this->catalog()->prices()->helper()->getPriceColumnId($this->isPostDelivery());
            $prices = $this->catalog()->prices()->getPriceByGoodIds($itemIds, $cityId, $priceColumn);

            $images = $this->catalog()->images()->getGoodsMainImage($itemIds, 'max');
        }

        $vars = array(
            'category' => $category,
            'comparisonInfo' => $comparisonInfo,
            'prices' => $prices,
            'images' => $images,
        );

        return $this->outputVars($vars);
    }

    public function updateAction() {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->redirect()->toRoute('home');
        }

        $action = $this->params()->fromQuery('action');

        if ($action == 'add' || $action == 'remove') {
            $goodId = (int)$this->params()->fromQuery('id');

            if ($goodId > 0) {
                $good = $this->catalog()->goods()->getGoodByGoodId($goodId);
                if ($good) {
                    $category = $this->catalog()->categories()->getPrimaryCategoryByGoodId(intval($good['GoodID']));
                }
            }
        }

        if ($action == 'add') {
            $this->comparer()->addGood($good, $category, $this->catalog());
        } elseif ($action == 'remove') {
            $this->comparer()->removeGood($goodId, $category);
        } elseif ($action == 'remove-group') {
            $categoryId = (int)$this->params()->fromQuery('id');
            $category = $this->catalog()->categories()->getCategoryByID($categoryId);
            $this->comparer()->removeComparisonCategory($category);
        }

        $result = $this->comparer()->getComparisonInfo();
        $result['state'] = 'ok';
        $result['added'] = $action == 'add';
        $result['id'] = $goodId ?? $categoryId;

        return new JsonModel($result);
    }
}