<?php

namespace Application\Controller;

use AlfaBank\Client\Client;
use Solo\DateTime\DateTime;
use SoloCatalog\Service\Helper\DateHelper;
use SoloIdentity\Entity\Ultima\Agent;
use SoloIdentity\Service\Ultima\Result\AuthResult;
use SoloOrder\Entity\Order\Ultima\Order;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;
use SoloIdentity\Entity\Phone;
use SoloIdentity\Entity\Ultima\Manager;
use Zend\Validator\EmailAddress;
use SoloDelivery\Entity\PostDeliveryRegion;
use Google\Analytics\Entity\Transaction;
use Google\Analytics\Entity\TransactionItem;

class OrderController extends BaseController {

	private function getOrderData() {
		$container = new Container('orders');
		
		if ($container->ordersCount == 1) {
			return [];
		}
		
		$order = $this->order()->getOrders()->current();
		
		$arrivalDateTimestamp = $order->setting('arrivalDate');
		
		$currentDate = new \DateTime();
		$currentDate->setTime(0, 0, 0);
		
		$currentDateTimestamp = $currentDate->getTimestamp();
		
		$dateHelper = new DateHelper();
		$orderGoods = $order->getGoodsCollection();
		
		$amount = 0;
		
		foreach ($orderGoods as $good) {
			$amount += $good->getPriceSum();
		}
		
		$goodIds = array_keys($orderGoods->toArray());
		
		// @todo Find a better way
		$goodsImages = [];
		foreach ($goodIds as $goodId) {
			$goodsImages[$goodId] = $this->catalog()->images()->getGoodMainImage($goodId);
		}
		
		$goods = $this->catalog()->goods()->enumGoodsByIds($goodIds);
		
		$data = [
			'orderAmount' => $amount,
			'goodsImages' => $goodsImages,
			'orderGoods' => $orderGoods,
			'goods' => $goods,
			'currentDateTimestamp' => $currentDateTimestamp,
			'arrivalDateTimestamp' => $arrivalDateTimestamp,
			'dateHelper' => $dateHelper,
			'currentOrderNumber' => $container->currentOrderNumber,
			'ordersCount' => $container->ordersCount 
		];
		
		return $data;
	}

	public function indexAction() {
		$order = $this->order()->getOrders()->current();
		
		if ($order === false) {
			return $this->redirect()->toRoute('home');
		}
		return $this->redirect()->toUrl('/order/step-one/');
	}

	public function changeOrderAction() {
		$request = $this->getRequest();
		
		if (!$request->isXmlHttpRequest()) {
			return $this->outputVars();
		}
		
		$action = $request->getPost('action');
		
		switch ($action) {
			case 'step1':
				$result = $this->setDeliveryAndPickupInfo();
				break;
			case 'add-address':
				$result = $this->addAddress();
				break;
			case 'step2':
				$result = $this->setPaymentInfo();
				break;
			case 'new-org':
				$result = $this->addOrganization();
				break;
			case 'step3':
				$result = $this->setContacts();
				break;
			case 'set-comment':
				$result = $this->setPickupComment();
				break;
			case 'set-source':
				$result = $this->setSource();
				break;
			case 'change-stage':
				$result = $this->recalcDelivery();
				break;
			default:
				$result = [
					'state' => 'error',
					'message' => 'Неизвестная команда' 
				];
		}
		
		return new JsonModel($result);
	}

	protected function addAddress() {
		$request = $this->getRequest();
		
		if ($this->auth()->logged() && !MANAGER_MODE) {
			$user = $this->auth()->getUser();
			$deliveryAddresses = $user->addresses()->getDeliveryAddresses();
		}
		
		$city = $request->getPost('city');
		$street = $request->getPost('street');
		$house = $request->getPost('house');
		$flat = $request->getPost('flat');
		
		$address = $city . ', ' . $street . ', ' . $house . ', кв.' . $flat;
		$address = $request->getPost('full');
		
		if (!$address) {
			return [
				'state' => 'fail',
				'message' => 'Указан пустой или некорректный адрес' 
			];
		}
		
		if (isset($deliveryAddresses)) {
			foreach ($deliveryAddresses as $deliveryAddress) {
				if ($address == $deliveryAddress->getAddress()) {
					$addressId = $deliveryAddress->getId();
					break;
				}
			}
		}
		
		$result = [
			'state' => 'ok',
			'address' => $address,
			'id' => $addressId ? $addressId : $address 
		];
		
		return $result;
	}

	protected function setDeliveryAndPickupInfo() {
		$request = $this->getRequest();
		
		$obtainMethod = $request->getPost('method');
		
		if (MANAGER_MODE) {
			$this->order()->setManagerId($this->auth()->getUser()->getManagerId());
		}
		
		switch ($obtainMethod) {
			case 'delivery':
				$addressId = $request->getPost('address');
				$liftToTheFloor = (bool)$request->getPost('elevation');
				$floor = $this->params()->fromPost('stage', 0);
				$elevatorTypeId = $this->params()->fromPost('elevator', 0);
				if (!$liftToTheFloor) {
					$floor = 0;
					$elevatorTypeId = 0;
				}
				
				$deliveryDateTimestamp = $request->getPost('date');
				$deliveryDate = new \DateTime();
				$deliveryDate->setTimestamp($deliveryDateTimestamp);
				
				$fakeDeliveryTimeId = $this->delivery()->getAnyDeliveryTimeRangeId();
				$fakeDeliveryTimeId = 8;
				
				$regionId = 0;
				$userRegion = $this->getUserRegion();
				if ($userRegion) {
					$regionId = $userRegion->getId();
				}
				
				$address = $request->getQuery('address');
				$addressTitle = '';
				
				if (is_numeric($addressId) && $this->auth()->logged() && !MANAGER_MODE) {
					$user = $this->auth()->getUser();
					$deliveryAddress = $user->addresses()->getDeliveryAddressessById($addressId);
					$regionId = $this->detectRegionId($deliveryAddress->getAddress());
					
					$longitude = $deliveryAddress->getLongitude();
					$latitude = $deliveryAddress->getLatitude();
				} else {
					$coordinates = $this->identity()->addresses()->helper()->geocode($addressId);
					$regionId = $this->detectRegionId($addressId);
					
					list($longitude, $latitude) = explode(' ', $coordinates);
				}
				
				$isRegion = (0 == $regionId);
				if (false !== mb_stripos($addressTitle, 'Московская')) {
					$isRegion = false;
				}
				if (false !== mb_stripos($addressTitle, 'Ленинградская о')) {
					$isRegion = false;
				}
				
				$articles = [];
				foreach ($this->basket()->getGoods() as $basketGood) {
					$articles[$basketGood->getId()] = $basketGood->getQuantity();
					foreach ($basketGood->getPackageComponents() as $componentId => $packageComponent) {
						if (0 < $componentId) {
							$articles[$componentId] = $basketGood->getQuantity();
						}
					}
				}
				$deliveryCost = $this->delivery()->getPreliminaryDeliveryCost($latitude, $longitude, $articles, $floor, $elevatorTypeId);
				$result['method'] = $this->generatePremilinaryDeliveryCostParams($latitude, $longitude, $articles, $floor, $elevatorTypeId);
				
				if ($deliveryCost && $deliveryCost > 0) {
					if ($liftToTheFloor) {
						$liftingCost = $this->delivery()->getLiftingCost($elevatorTypeId, $floor);
						$deliveryCost += $liftingCost;
					}
				} elseif (!$isRegion) {
					$result = [
						'state' => 'error',
						'message' => 'Не удалось определить адрес доставки.' 
					];
					
					return $result;
				}
				
				$this->order()->setDelivery($addressId, $deliveryCost, $fakeDeliveryTimeId, $deliveryDate, $liftToTheFloor, $floor, $elevatorTypeId);
				$this->order()->setObtainMethod(Order::DELIVERY_OBTAIN_METHOD);
				break;
			case 'outpost':
				$phone = $request->getPost('phone');
				if ($phone) {
					$this->order()->setPhone($phone);
				}
				$this->order()->setObtainMethod(Order::PICKUP_OBTAIN_METHOD);
				break;
			default:
				throw new \RuntimeException('Invalid obtain method');
		}
		
		if ($request->getPost('username')) {
			$this->order()->setUserName(trim($request->getPost('username')));
		}
		
		$result = [
			'state' => 'ok',
			'redirect' => '/order/step-two/' 
		];
		
		return $result;
	}

	public function stepOneAction() {
		$this->getLayout()->setTitle('Оформление заказа');
		$this->getLayout()->setBodyClass('order');
		$this->getLayout()->addCssFile('order.css');
		$this->getLayout()->addCssFile('order.mobile.css');
		$this->getLayout()->addJsFile('order.js');
		$this->getLayout()->addJsFile('jquery.mask.js');
		
		$order = $this->order()->getOrders()->current();
		
		$deliveryAddresses = [];
		if ($this->auth()->logged() && !MANAGER_MODE) {
			$user = $this->auth()->getUser();
			$deliveryAddresses = $user->addresses()->getDeliveryAddresses();
			$phone = substr($user->getPhone(), 1);
		}
		
		$elevatorTypes = $this->delivery()->getElevatorTypes();
		$liftingPrices = $this->delivery()->getLiftingPrices();
		
		$elevatorTypeId = $elevatorTypes[0]['ElevatorTypeID'];
		
		$liftToTheFloor = false;
		$floor = 1;
		
		if ($order->hasDelivery()) {
			$deliveryAddressId = $order->getDelivery()->getAddressId();
			$newDeliveryAddress = $order->getDelivery()->getAddress();
			
			if (count($deliveryAddresses)) {
				foreach ($deliveryAddresses as $deliveryAddress) {
					if ($deliveryAddress->getAddress() == $newDeliveryAddress) {
						unset($newDeliveryAddress);
						break;
					}
				}
			}
			
			$liftToTheFloor = $order->getDelivery()->needLiftToTheFloor();
			$floor = $order->getDelivery()->getFloor();
			$elevatorTypeId = $order->getDelivery()->getElevatorTypeId();
		}
		
		if (!$deliveryAddressId && count($deliveryAddresses)) {
			$deliveryAddress = reset($deliveryAddresses->toArray());
			$deliveryAddressId = $deliveryAddress->getId();
		}
		
		$phone = Phone::createFromString($order->setting('phone') ?  : $phone);
		
		$vars = [
			'phone' => $phone->toString('{code}{number}'),
			'obtainMethod' => $order->getObtainMethod(),
			'deliveryAddresses' => $deliveryAddresses,
			'deliveryAddressId' => $deliveryAddressId,
			'newDeliveryAddress' => $newDeliveryAddress,
			'elevatorTypes' => $elevatorTypes,
			'elevatorTypeId' => $elevatorTypeId,
			'liftingPrices' => $liftingPrices,
			'liftToTheFloor' => $liftToTheFloor,
			'floor' => $floor 
		];
		
		$orderData = $this->getOrderData();
		
		$vars = array_merge($vars, $orderData);
		
		$ecommerceTracker = $this->createEcommerceTracker('order2');
		$vars['ecommerceTracker'] = $ecommerceTracker;
		
		return $this->outputVars($vars);
	}

	public function deliveryAction() {
		$request = $this->getRequest();
		
		if (!$request->isXmlHttpRequest()) {
			// return $this->notfound();
		}
		
		$cityId = $this->getErpCityId();
		$order = $this->order()->getOrders()->current();
		
		$goods = $order->getGoodsCollection();
		
		$goodIds = array_keys($goods->toArray());
		
		$floor = $this->params()->fromPost('stage', 0);
		$elevatorTypeId = $this->params()->fromPost('elevator', 0);
		
		$regionId = 0;
		$userRegion = $this->getUserRegion();
		if ($userRegion) {
			$regionId = $userRegion->getId();
		}
		
		$address = $request->getQuery('address');
		$addressTitle = '';
		
		if (is_numeric($address) && $this->auth()->logged() && !MANAGER_MODE) {
			$user = $this->auth()->getUser();
			$deliveryAddress = $user->addresses()->getDeliveryAddressessById($address);
			$addressTitle = $deliveryAddress->getAddress();
			$regionId = $this->detectRegionId($deliveryAddress->getAddress());
			
			$longitude = $deliveryAddress->getLongitude();
			$latitude = $deliveryAddress->getLatitude();
		} else {
			$coordinates = $this->identity()->addresses()->helper()->geocode($address);
			$regionId = $this->detectRegionId($address);
			$addressTitle = $address;
			
			list($longitude, $latitude) = explode(' ', $coordinates);
		}
		
		// foreach ($goods as $good) {
		// var_dump($good->getPickupMethodId());
		// }
		// exit();
		
		$avails = $this->catalog()->goods()->enumAvailabilityByGoodIds($goodIds, $cityId);
		
		$minDeliveryDate = 0;
		if ($order->setting('arrivalDate')) {
			$minDeliveryDate = $order->setting('arrivalDate');
		}
		$deliveryDates = $this->delivery()->getPossibleDeliveryDates($avails, $goods, $cityId, $regionId, $minDeliveryDate);
		$articles = [];
		foreach ($this->basket()->getGoods() as $basketGood) {
			$articles[$basketGood->getId()] = $basketGood->getQuantity();
			foreach ($basketGood->getPackageComponents() as $componentId => $packageComponent) {
				if (0 < $componentId) {
					$articles[$componentId] = $basketGood->getQuantity();
				}
			}
		}
		$deliveryCost = $this->delivery()->getPreliminaryDeliveryCost($latitude, $longitude, $articles, $floor, $elevatorTypeId);
		$result['method'] = $this->generatePremilinaryDeliveryCostParams($latitude, $longitude, $articles, $floor, $elevatorTypeId);
		$result['coords'] = [
			'longitude' => $longitude,
			'latitude' => $latitude 
		];
		
		$result['isRegion'] = (0 == $regionId);
		$result['regionId'] = $regionId;
		
		if (false !== mb_stripos($addressTitle, 'Московская')) {
			$result['isRegion'] = false;
		}
		if (false !== mb_stripos($addressTitle, 'Ленинградская о')) {
			$result['isRegion'] = false;
		}
		
		if ($deliveryCost && $deliveryCost > 0) {
			if (count($deliveryDates)) {
				$result["state"] = "ok";
				$minDate = PHP_INT_MAX;
				$maxDate = 0;
				foreach ($deliveryDates as $deliveryDate) {
					$result["delivery"][$deliveryDate->getTimestamp()] = $deliveryCost;
					$minDate = min($minDate, $deliveryDate->getTimestamp());
					$maxDate = max($maxDate, $deliveryDate->getTimestamp());
				}
				if ((false !== mb_stripos($addressTitle, 'Москва')) || (false !== mb_stripos($addressTitle, 'Московская'))) {
					$maxDate += (60 * 60 * 24 * 365);
					$result['user-date'] = [
						"enabled" => 1,
						"price" => $deliveryCost,
						"min-date" => $minDate,
						"max-date" => $maxDate 
					];
				}
				
				$maxDatesCount = 7;
				if ($this->application()->isMobileDevice() && isset($result['user-date'])) {
					$maxDatesCount = 5;
				}
				if ($maxDatesCount < count($result['delivery'])) {
					$result['delivery'] = array_slice($result['delivery'], 0, $maxDatesCount, true);
				}
			} else {
				$result['state'] = 'error';
				$result['message'] = 'Отсутствуют даты доставки.';
			}
		} else {
			$result['state'] = 'error';
			$result['message'] = 'Не удалось определить адрес';
		}
		
		if (('error' == $result['state']) && $result['isRegion']) {
			$result['state'] = 'ok';
		}
		
		return new JsonModel($result);
	}

	/**
	 *
	 * @param string $address        	
	 * @return integer
	 */
	public function detectRegionId($address) {
		$result = 0;
		
		// spb
		if (!$result && (false !== mb_stripos($address, 'петербург'))) {
			$result = PostDeliveryRegion::REGION_SPB_ID;
		}
		if (!$result && (false !== mb_stripos($address, 'ленинградская о'))) {
			$result = PostDeliveryRegion::REGION_SPB_ID;
		}
		if (!$result && (false !== mb_stripos($this->identity()->addresses()->helper()->geocodeAddress($address), 'петербург'))) {
			$result = PostDeliveryRegion::REGION_SPB_ID;
		}
		
		// msk
		if (!$result && (false !== mb_stripos($address, 'москва'))) {
			$result = PostDeliveryRegion::REGION_MOSCOW_ID;
		}
		if (!$result && (false !== mb_stripos($this->identity()->addresses()->helper()->geocodeAddress($address), 'москва'))) {
			$result = PostDeliveryRegion::REGION_MOSCOW_ID;
		}
		if (!$result && (false !== mb_stripos($this->identity()->addresses()->helper()->geocodeAddress($address), 'московская'))) {
			$result = PostDeliveryRegion::REGION_MOSCOW_ID;
		}
		
		return $result;
	}

	public function stepTwoAction() {
		if (!$this->order()->isStepAvailable('step2')) {
			return $this->redirect()->toUrl('/order/step-one/');
		}
		
		$this->getLayout()->setTitle('Оформление заказа');
		$this->getLayout()->setBodyClass('order');
		$this->getLayout()->addCssFile('order.css');
		$this->getLayout()->addCssFile('order.mobile.css');
		$this->getLayout()->addJsFile('order.js');
		$this->getLayout()->addJsFile('jquery.mask.js');
		
		$order = $this->order()->getOrders()->current();
		
		$agents = [];
		$email = '';
		$agentFio = '';
		if ($this->auth()->logged() && !MANAGER_MODE) {
			$user = $this->auth()->getUser();
			$agents = $user->agents();
			$email = $user->getEmail();
		} elseif (MANAGER_MODE) {
			if ($order->setting('userEmail')) {
				$email = $order->setting('userEmail');
			}
			if ($order->setting('userName')) {
				$agentFio = $order->setting('userName');
			}
		}
		
		$agentId = $order->getAgentId();
		
		if (!$agentId && count($agents)) {
			$firstAgent = current($agents->toArray());
			$agentId = $firstAgent->getId();
		}
		
		$vars = [
			'payerPerson' => $order->getPayerPerson(),
			'obtainMethod' => $order->getObtainMethod(),
			'agentId' => $agentId,
			'newAgent' => $order->getAgent(),
			'agents' => $agents,
			'email' => $email,
			'agentFio' => $agentFio,
			'order' => $order 
		];
		
		$orderData = $this->getOrderData();
		
		$vars = array_merge($vars, $orderData);
		
		$ecommerceTracker = $this->createEcommerceTracker('order3');
		$vars['ecommerceTracker'] = $ecommerceTracker;
		
		return $this->outputVars($vars);
	}

	protected function addOrganization() {
		$request = $this->getRequest();
		
		$agents = [];
		if ($this->auth()->logged() && !MANAGER_MODE) {
			$user = $this->auth()->getUser();
			$agents = $user->agents();
		}
		
		$data = $request->getPost('data');
		
		if (!is_array($data) || !isset($data['inn'])) {
			return [
				'state' => 'fail',
				'message' => 'Не указана организация' 
			];
		}
		
		$name = $data['name']['short_with_opf'];
		$inn = $data['inn'];
		$kpp = $data['kpp'];
		$address = $data['address']['value'];
		
		$agentId = join('|', [
			$name,
			$inn,
			$kpp,
			$address,
			$data['opf']['short'] 
		]);
		
		if (count($agents)) {
			foreach ($agents as $agent) {
				if ($agent->requisites()->getInn() == $inn) {
					$agentId = $agent->getId();
					break;
				}
			}
		}
		
		$result = [
			'state' => 'ok',
			'name' => $name,
			'id' => $agentId 
		];
		
		return $result;
	}

	protected function setPaymentInfo() {
		$request = $this->getRequest();
		
		$payer = $request->getPost('mode');
		
		switch ($payer) {
			case 'physic':
				$paymentTypeId = $request->getPost('payment_type_id');
				
				$this->order()->setPayerPerson(Order::PAYER_NATURAL_PERSON);
				$this->order()->setPaymentId($paymentTypeId);
				break;
			case 'firm':
				$this->order()->setPayerPerson(Order::PAYER_LEGAL_PERSON);
				$this->order()->setPaymentId(Order::PAYMENT_CASH_ID);
				
				$agentId = $request->getPost('agent_id');
				$email = $request->getPost('email');
				$name = $request->getPost('name');
				
				$isValidEmail = (new EmailAddress())->isValid($email);
				if (!$isValidEmail) {
					return [
						'state' => 'fail',
						'message' => 'Введите корректный адрес электронной почты' 
					];
				}
				
				if (!$agentId) {
					return [
						'state' => 'fail',
						'message' => 'Создайте или укажите реквизиты юридического лица или платите как "физик"' 
					];
				}
				
				if ($email) {
					$this->order()->setUserEmail($email);
				}
				if ($name) {
					$this->order()->setUserName($name);
				}
				
				if (is_numeric($agentId)) {
					$this->order()->setAgentId($agentId);
				} else {
					list($name, $inn, $kpp, $address, $opfShort) = explode('|', $agentId);
					
					$agent = new Agent();
					$agent->setName($name);
					$agent->requisites()->setAddress($address);
					$agent->requisites()->setInn(trim($inn));
					$agent->requisites()->setKpp(trim($kpp));
					$agent->requisites()->setOpf(trim($opfShort));
					
					$this->order()->setAgent($agent);
				}
				
				break;
			default:
				throw new \RuntimeException('Invalid payer type');
		}
		
		$order = $this->order()->getOrders()->current();
		$redirectUrl = '/order/complete/';
		if ($order->getObtainMethod() == Order::DELIVERY_OBTAIN_METHOD) {
			$redirectUrl = '/order/step-three/';
		} else {
			if (MANAGER_MODE) {
				$redirectUrl = '/order/step-three-pickup/';
			}
		}
		
		$result = [
			'state' => 'ok',
			'redirect' => $redirectUrl 
		];
		
		return $result;
	}

	public function stepThreeAction() {
		if (!$this->order()->isStepAvailable('step3')) {
			return $this->redirect()->toUrl('/order/step-two/');
		}
		
		$this->getLayout()->setTitle('Оформление заказа');
		$this->getLayout()->setBodyClass('order phone');
		$this->getLayout()->addCssFile('order.css');
		$this->getLayout()->addCssFile('order.mobile.css');
		$this->getLayout()->addJsFile('order.js');
		$this->getLayout()->addJsFile('jquery.mask.js');
		
		$order = $this->order()->getOrders()->current();
		
		$clientPhone = $order->setting('phone');
		
		if (!$clientPhone && $this->auth()->logged() && !MANAGER_MODE) {
			$user = $this->auth()->getUser();
			$clientPhone = substr($user->getPhone(), 1);
		}
		
		$clientPhone = Phone::createFromString($clientPhone);
		
		$vars = [
			'phone' => $clientPhone->toString('{code}{number}'),
			'contactName' => $order->setting('userName') 
		];
		
		$orderData = $this->getOrderData();
		
		$vars = array_merge($vars, $orderData);
		
		$ecommerceTracker = $this->createEcommerceTracker('order4');
		$vars['ecommerceTracker'] = $ecommerceTracker;
		
		return $this->outputVars($vars);
	}

	public function stepSourceAction() {
		$this->getLayout()->setTitle('Оформление заказа');
		$this->getLayout()->setBodyClass('order source');
		$this->getLayout()->addCssFile('order.css');
		$this->getLayout()->addCssFile('order.mobile.css');
		$this->getLayout()->addJsFile('order.js');
		$this->getLayout()->addJsFile('jquery.mask.js');
		
		$vars = [];
		return $this->outputVars($vars);
	}

	public function stepThreePickupAction() {
		$this->getLayout()->setTitle('Оформление заказа');
		$this->getLayout()->setBodyClass('order phone');
		$this->getLayout()->addCssFile('order.css');
		$this->getLayout()->addCssFile('order.mobile.css');
		$this->getLayout()->addJsFile('order.js');
		$this->getLayout()->addJsFile('jquery.mask.js');
		
		$vars = [];
		return $this->outputVars($vars);
	}

	protected function setContacts() {
		$request = $this->getRequest();
		
		$phone = Phone::createFromString($request->getPost('phone'));
		if ($request->getPost('phone')) {
			$this->order()->setPhone($phone->toString('7{code}{number}'));
		}
		
		if ($request->getPost('clientName')) {
			$this->order()->setUserName($request->getPost('clientName'));
		}
		
		$order = $this->order()->getOrders()->current();
		$email = $order->setting('userEmail');
		if (!$email && $this->auth()->logged() && !MANAGER_MODE) {
			$email = $this->auth()->getUser()->getEmail();
		}
		if ($email) {
			$this->order()->setUserEmail($email);
		}
		
		$this->order()->setComment($request->getPost('deliveryRemarks'));
		$this->order()->setDeliveryComment($request->getPost('comment'));
		$this->order()->getLocalStorage()->setDeliveryPhone($phone->toString('7{code}{number}'));
		
		$redirectUrl = '/order/complete/';
		if (MANAGER_MODE) {
			$redirectUrl = '/order/step-source/';
		}
		$result = [
			'state' => 'ok',
			'redirect' => $redirectUrl 
		];
		
		return $result;
	}

	protected function setPickupComment() {
		$request = $this->getRequest();
		$this->order()->setComment($request->getPost('comment'));
		
		$result = [
			'state' => 'ok',
			'redirect' => '/order/step-source/' 
		];
		return $result;
	}

	protected function setSource() {
		$request = $this->getRequest();
		$this->order()->setSource($request->getPost('source'));
		$_SESSION['orderClientCode'] = $request->getPost('clientCode');
		
		$result = [
			'state' => 'ok',
			'redirect' => '/order/complete/' 
		];
		return $result;
	}

	/**
	 *
	 * @return array
	 */
	public function recalcDelivery() {
		$deliveryPrice = 0;
		
		$addressId = $this->params()->fromPost('address_id');
		if (is_numeric($addressId) && $this->auth()->logged() && !MANAGER_MODE) {
			$user = $this->auth()->getUser();
			$deliveryAddress = $user->addresses()->getDeliveryAddressessById($addressId);
			
			$longitude = $deliveryAddress->getLongitude();
			$latitude = $deliveryAddress->getLatitude();
		} else {
			$coordinates = $this->identity()->addresses()->helper()->geocode($addressId);
			
			list($longitude, $latitude) = explode(' ', $coordinates);
		}
		
		$articles = [];
		foreach ($this->basket()->getGoods() as $basketGood) {
			$articles[$basketGood->getId()] = $basketGood->getQuantity();
			foreach ($basketGood->getPackageComponents() as $componentId => $packageComponent) {
				if (0 < $componentId) {
					$articles[$componentId] = $basketGood->getQuantity();
				}
			}
		}
		$floor = intval($this->params()->fromPost('stage'), 0);
		$elevatorTypeId = $this->params()->fromPost('elevator', 0);
		
		$basicDeliveryPrice = $this->delivery()->getPreliminaryDeliveryCost($latitude, $longitude, $articles);
		$withStageDeliveryPrice = $this->delivery()->getPreliminaryDeliveryCost($latitude, $longitude, $articles, $floor, $elevatorTypeId);
		
		$deliveryPrice = $withStageDeliveryPrice - $basicDeliveryPrice;
		
		$result = [
			'state' => 'ok',
			'delivery_price' => $deliveryPrice,
			'method' => $this->generatePremilinaryDeliveryCostParams($latitude, $longitude, $articles, $floor, $elevatorTypeId) 
		];
		return $result;
	}

	public function completeAction() {
		if (!$this->order()->isStepAvailable('complete')) {
			return $this->redirect()->toUrl('/order/step-three/');
		}
		
		$this->layout()->setTemplate('layout/inner');
		
		$this->getLayout()->setTitle('Оформление заказа');
		$this->getLayout()->setBodyClass('order final');
		$this->getLayout()->addCssFile('order.final.css');
		$this->getLayout()->addCssFile('order.mobile.css');
		
		$order = $this->order()->getOrders()->current();
		
		$clientPhone = Phone::createFromString($order->setting('phone'))->toString('7{code}{number}');
		
		$orderAgentId = $order->getAgentId();
		
		// agent id
		// manager: natural person
		
		if (!$orderAgentId && MANAGER_MODE) {
			$result = $this->identity()->users()->isClientExists($clientPhone);
			$isUserExists = $result->success();
			$orderClientEmail = $order->setting('userEmail');
			if ($isUserExists) {
				$orderAgentId = intval($result->getData());
				if (!$orderAgentId) {
					return $this->problem();
				}
				
				if (!$orderClientEmail) {
					$result = $this->identity()->users()->getClientInfo($orderAgentId);
					if ($result->success()) {
						$userInfo = $result->getData()['user'];
						$orderClientEmail = $userInfo->getEmail();
					}
				}
			} else {
				$result = $this->identity()->users()->register($clientPhone);
				if ($result->success()) {
					$orderAgentId = $result->userId;
				} else {
					return $this->problem();
				}
			}
			if ($orderClientEmail) {
				$this->order()->setUserEmail($orderClientEmail);
			}
		}
		// END manager: natural person
		
		// all: new legal
		if ($order->hasAgent() && (true || Order::PAYER_LEGAL_PERSON == $this->order()->getPayerPerson())) {
			$agent = $order->getAgent();
			$this->application()->updateAgentRequisites($agent);
			$result = $this->identity()->agents()->createAgent($agent);
			if (!$result->hasError()) {
				$orderAgentId = $result->agentId;
				$this->order()->setPaymentId(4);
				$order->setPaymentId(4);
			} else {
				$this->notify()->sendOrderNotify($result->getOriginalRequest(), $result->getOriginalResponse());
				return $this->problem();
			}
		}
		// END all: new legal
		
		if ($orderAgentId) {
			$this->order()->setAgentId($orderAgentId);
			$order->setAgentid($orderAgentId);
		}
		// END agent id
		
		if (!$this->auth()->logged() && !MANAGER_MODE) {
			$result = $this->identity()->users()->register($clientPhone);
			
			if ($result->success()) {
				$authResult = $this->auth()->login($clientPhone, false);
				
				if (!$authResult->success()) {
					return $this->problem();
				}
			} else {
				return $this->problem();
			}
		}
		
		$order = $this->order()->getOrders()->current();
		$delivery = $order->getDelivery();
		
		if ($order->getObtainMethod() == Order::DELIVERY_OBTAIN_METHOD && $delivery) {
			$delivery->setDeliveryPhone($clientPhone);
			
			if (!$delivery->getAddressId()) {
				$deliveryAddress = $delivery->getAddress();
				$floor = $delivery->getFloor();
				
				$address = $this->identity()->addresses()->createDeliveryAddressInstance($deliveryAddress, $floor);
				$address->setElevatorTypeId($order->getDelivery()->getElevatorTypeId());
				$address = $this->identity()->addresses()->createAddress($address, $orderAgentId);
				
				$delivery->setAddressId($address->getId());
			} else {
				$user = $this->auth()->getUser();
				$deliveryAddress = $user->addresses()->getDeliveryAddressessById($delivery->getAddressId());
				
				// Проверить, нужен ли нам новый адрес
				if ($delivery->getFloor() != $deliveryAddress->getFloor()) {
					$address = $this->identity()->addresses()->createDeliveryAddressInstance($deliveryAddress->getAddress(), $delivery->getFloor());
					$address->setElevatorTypeId($order->getDelivery()->getElevatorTypeId());
					$address = $this->identity()->addresses()->createAddress($address, $orderAgentId);
					$delivery->setAddressId($address->getId());
				} else {
					$delivery->setAddress($deliveryAddress->getAddress());
				}
				
				$this->order()->getLocalStorage()->setDeliveryAddress($deliveryAddress->getAddress());
			}
		}
		
		$result = $this->order()->create($order);
		
		$logger = $this->logger('orders_logger');
		$logger->info('Входные: ' . $result->getOriginalRequest());
		$logger->info('Выходные: ' . $result->getOriginalResponse());
		
		if (!$result->hasError()) {
			$reserve = $result->getData();
			$order->setAmount($reserve->Amount);
			$order->setId($reserve->Id);
			
			$this->order()->getLocalStorage()->setId($reserve->Id);
			$this->order()->getLocalStorage()->setAmount($reserve->Amount);
		} else {
			$this->notify()->sendOrderNotify($result->getOriginalRequest(), $result->getOriginalResponse());
			return $this->problem();
		}
		
		// Онлайн оплата
		if ($order->getPayerPerson() == Order::PAYER_NATURAL_PERSON && $order->isAlfabankPayment()) {
			$alfaBankClient = $this->getServiceLocator()->get('Alfabank\Client\Client');
			
			$returnUrl = 'https://' . $_SERVER['HTTP_HOST'] . '/order/payment-complete/';
			$failUrl = 'https://' . $_SERVER['HTTP_HOST'] . '/order/payment-fail/';
			
			$data = [
				'orderNumber' => urlencode(substr($order->getId() . '-' . uniqid(rand(), true), 0, 32)),
				'amount' => urlencode($order->getBankPaymentSum() * 100),
				'returnUrl' => $returnUrl,
				'failUrl' => $failUrl 
			];
			
			$response = $alfaBankClient->post('register.do', $data);
			
			if (!isset($response['errorCode'])) {
				return $this->redirect()->toUrl($response['formUrl']);
			} else {
				return $this->problem();
			}
		}
		
		$orderNumber = $order->getId();
		$amount = $order->getAmount();
		
		$reserveInfo = null;
		$reserveInfoResult = $this->cabinet()->reserves()->getReserve($orderNumber, $order->getAgentId());
		if ($reserveInfoResult && isset($reserveInfoResult->item)) {
			$reserveInfo = $reserveInfoResult->item;
		}
		$deliveryAmount = 0;
		if ($reserveInfo) {
			$deliveryAmount = $reserveInfo->getDeliveryCost();
		}
		
		$paymentTypeId = $order->getPaymentId();
		
		if ($order->getObtainMethod() == Order::DELIVERY_OBTAIN_METHOD && $order->hasDelivery()) {
			$deliveryAddress = $order->getDelivery()->getAddress() ?  : $this->order()->getLocalStorage()->getDeliveryAddress();
			$deliveryDate = $order->getDelivery()->getDeliveryDate();
			
			$deliveryDateText = DateHelper::getDeliveryDateText($deliveryDate);
		}
		
		$container = new Container('orders');
		$vars['currentOrderNumber'] = $container->currentOrderNumber;
		$vars['ordersCount'] = $container->ordersCount;
		
		$ecommerceTracker = $this->createEcommerceTracker('purchase');
		$ecommerceTracker->addAttribute('data-order-id', $orderNumber);
		$ecommerceTracker->addAttribute('data-order-amount', $amount);
		$ecommerceTracker->addAttribute('data-order-delivery-amount', $deliveryAmount);
		$vars['ecommerceTracker'] = $ecommerceTracker;
		
		if (count($this->order()->getOrders()) == 1) {
			$this->basket()->clearStorage();
			
			unset($container->currentOrderNumber);
			unset($container->ordersCount);
		} else {
			$container->currentOrderNumber++;
			
			$orderGoods = $order->getGoodsCollection();
			
			foreach ($orderGoods as $orderGood) {
				$basketGood = $this->basket()->getGoodById($orderGood->getMarking());
				
				if ($basketGood->getQuantity() == $orderGood->getQuantity()) {
					$this->basket()->removeGood($basketGood);
				} else {
					$basketGood->setQuantity($basketGood->getQuantity() - $orderGood->getQuantity());
				}
			}
		}
		
		// google commit
		$clientCode = $_SESSION['orderClientCode'];
		if ($clientCode) {
			$clientId = $this->getClientIdByCode($clientCode);
			if ($clientId) {
				$transaction = new Transaction();
				$transaction->setId($orderNumber);
				$transaction->setAffiliation('santehbaza.ru-crm');
				$transaction->setRevenue($amount);
				$transaction->setTax(0);
				$transaction->setShipping($deliveryAmount);
				
				$orderGoods = $order->getGoodsCollection();
				foreach ($orderGoods as $orderGood) {
					$basketGood = $this->basket()->getGoodById($orderGood->getMarking());
					$good = $this->catalog()->goods()->getGoodByGoodId($orderGood->getMarking());
					
					$item = new TransactionItem();
					$item->setSku($orderGood->getMarking());
					$item->setName($good->GoodName);
					$item->setPrice($orderGood->getPrice());
					$item->setQuantity($orderGood->getQuantity());
					$item->setCategory($good->CategoryName);
					$item->setBrand($good->BrandName);
					$item->setVariant('');
					$transaction->addItem($item);
				}
				
				$this->google()->analytics()->ecommerce()->sendExtendedTransaction($clientId, $transaction);
				$this->google()->analytics()->ecommerce()->sendCallOrder($clientId, $clientCode);
			}
		}
		$_SESSION['orderClientCode'] = '';
		// END google commit
		
		$this->order()->clearStorage();
		
		// reserve
		$isUndefinedDelivery = false;
		if ($reserveInfo) {
			$isUndefinedDelivery = $reserveInfo->isUndefinedDeliveryArea();
		}
		// END reserve
		
		$vars = array_merge(
			[
				'orderNumber' => $orderNumber,
				'deliveryAddress' => $deliveryAddress,
				'paymentTypeId' => $paymentTypeId,
				'payerPerson' => $order->getPayerPerson(),
				'agentId' => $order->getAgentId(),
				'deliveryDateText' => $deliveryDateText,
				'amount' => $amount,
				'orderStatus' => 0,
				'isUndefinedDelivery' => $isUndefinedDelivery 
			], 
			$vars);
		
		return $this->outputVars($vars);
	}

	public function paymentCompleteAction() {
		$this->layout()->setTemplate('layout/inner');
		
		$this->getLayout()->setTitle('Оформление заказа');
		$this->getLayout()->setBodyClass('order final');
		$this->getLayout()->addCssFile('order.final.css');
		$this->getLayout()->addCssFile('order.mobile.css');
		
		$reserveId = $this->getRequest()->getQuery('reserveId');
		$order = $this->order()->getOrders()->current();
		
		if ($reserveId && ($reserveId != $order->getId() || $order->getPaymentId() != Order::PAYMENT_CASH_ID)) {
			return $this->problem();
		}
		
		if (!$reserveId) {
			$orderId = $this->getRequest()->getQuery('orderId');
			
			if (!$orderId || $order->getPaymentId() != Order::PAYMENT_ALFABANK_ID) {
				// return $this->problem();
			}
			
			$alfaBankClient = $this->getServiceLocator()->get('Alfabank\Client\Client');
			
			$data['orderId'] = $orderId;
			
			$response = $alfaBankClient->post('getOrderStatus.do', $data);
			
			if ($response['errorCode']) {
				return $this->problem();
			}
			
			$merchantOrderNumber = explode('-', $response['OrderNumber'])[0];
			
			if ($merchantOrderNumber != $order->getId()) {
				// return $this->problem();
			}
			
			$result = $this->order()->payReserve($merchantOrderNumber);
			
			if (!$result->success()) {
				$this->notify()->sendPaymentNotify($order->getId());
				return $this->problem();
			}
			
			$order->setAmount($response['Amount'] / 100);
		}
		
		$amount = $order->getAmount();
		$paymentTypeId = $order->getPaymentId();
		
		if ($order->hasDelivery() && $order->getObtainMethod() == Order::DELIVERY_OBTAIN_METHOD) {
			$deliveryAddress = $order->getDelivery()->getAddress();
			$deliveryDate = $order->getDelivery()->getDeliveryDate();
			$deliveryDateText = DateHelper::getDeliveryDateText($deliveryDate);
		}
		
		$container = new Container('orders');
		$vars['currentOrderNumber'] = $container->currentOrderNumber;
		$vars['ordersCount'] = $container->ordersCount;
		
		if (count($this->order()->getOrders()) == 1) {
			$this->basket()->clearStorage();
			
			unset($container->currentOrderNumber);
			unset($container->ordersCount);
		} else {
			$container->currentOrderNumber++;
			
			$orderGoods = $order->getGoodsCollection();
			
			foreach ($orderGoods as $orderGood) {
				$basketGood = $this->basket()->getGoodById($orderGood->getMarking());
				
				if ($basketGood->getQuantity() == $orderGood->getQuantity()) {
					$this->basket()->removeGood($basketGood);
				} else {
					$basketGood->setQuantity($basketGood->getQuantity() - $orderGood->getQuantity());
				}
			}
		}
		
		$this->order()->clearStorage();
		
		$vars = array_merge(
			[
				'orderNumber' => $order->getId(),
				'deliveryAddress' => $deliveryAddress,
				'paymentTypeId' => $paymentTypeId,
				'deliveryDateText' => $deliveryDateText,
				'amount' => $amount 
			], 
			$vars);
		
		$viewModel = $this->outputVars($vars)->setTemplate('application/order/complete');
		
		return $viewModel;
	}

	public function paymentFailAction() {
		$order = $this->order()->getOrders()->current();
		
		if ($order->getPaymentId() != Order::PAYMENT_ALFABANK_ID) {
			return $this->notfound();
		}
		
		$request = $this->getRequest();
		
		if (!$request->getQuery('orderId')) {
			return $this->notfound();
		}
		
		$data['orderId'] = $request->getQuery('orderId');
		
		$this->getLayout()->setTitle('Оформление заказа');
		$this->getLayout()->setBodyClass('order');
		$this->getLayout()->addCssFile('order.css');
		$this->getLayout()->addCssFile('order.mobile.css');
		$this->getLayout()->addJsFile('order.js');
		$this->getLayout()->addJsFile('jquery.mask.js');
		$this->getLayout()->addJsFile('jquery.autocomplete.js');
		
		$alfaBankClient = $this->getServiceLocator()->get('Alfabank\Client\Client');
		
		$response = $alfaBankClient->post('getOrderStatus.do', $data);
		
		$merchantOrderNumber = explode('-', $response['OrderNumber'])[0];
		
		$orderData = $this->getOrderData();
		
		$vars = [
			'reserveId' => $merchantOrderNumber 
		];
		
		$vars = array_merge($vars, $orderData);
		
		return $this->outputVars($vars);
	}

	public function paymentRepeatAction() {
		$request = $this->getRequest();
		
		$reserveId = $request->getQuery('reserveId');
		
		if (!$reserveId) {
			return $this->notfound();
		}
		
		$order = $this->order()->getOrders()->current();
		
		if ($order->getId() != $reserveId) {
			return $this->problem();
		}
		
		$clientPhone = '7' . $order->setting('phone');
		$clientPhone = !preg_match('/@/', $clientPhone) ? preg_replace("/\D/", '', $clientPhone) : strtolower($clientPhone);
		
		if (!$this->auth()->logged()) {
			$result = $this->identity()->users()->register($clientPhone);
			
			if ($result->success()) {
				$authResult = $this->auth()->login($clientPhone);
				
				if (!$authResult->success()) {
					return $this->problem();
				}
			} else {
				return $this->problem();
			}
		}
		
		$reserveInfoResult = $this->cabinet()->reserves()->getReserve($reserveId);
		
		if ($reserveInfoResult->hasError()) {
			return $this->problem();
		}
		
		$reserveInfo = $reserveInfoResult->item;
		
		$bankPercent = (float)$this->getServiceLocator()->get('SoloSettings\Service\ConstantsService')->get('BankPercent');
		$amount = $reserveInfo->getAmount() + $reserveInfo->getDeliveryCost();
		
		$amount += ($bankPercent / 100) * $amount;
		$amount = ceil($amount);
		
		$returnUrl = 'https://' . $_SERVER['HTTP_HOST'] . '/order/payment-complete/';
		$failUrl = 'https://' . $_SERVER['HTTP_HOST'] . '/order/payment-fail/';
		
		$data = [
			'orderNumber' => urlencode(substr($reserveId . '-' . uniqid(rand(), true), 0, 32)),
			'amount' => $amount * 100,
			'returnUrl' => $returnUrl,
			'failUrl' => $failUrl 
		];
		
		$alfaBankClient = $this->getServiceLocator()->get('Alfabank\Client\Client');
		
		$response = $alfaBankClient->post('register.do', $data);
		
		if (!isset($response['errorCode'])) {
			return $this->redirect()->toUrl($response['formUrl']);
		} else {
			return $this->problem();
		}
	}

	public function changeOnlinePaymentAction() {
		$request = $this->getRequest();
		
		$reserveId = $request->getQuery('reserveId');
		
		if (!$reserveId) {
			return $this->notfound();
		}
		
		$order = $this->order()->getOrders()->current();
		
		if ($order->getId() != $reserveId || $order->getPaymentId() != Order::PAYMENT_ALFABANK_ID) {
			return $this->problem();
		}
		
		$result = $this->order()->changeReservePaymentMethod($reserveId, Order::PAYMENT_CASH_ID);
		
		if (!$result->success()) {
			return $this->problem();
		}
		
		$this->order()->setPaymentId(Order::PAYMENT_CASH_ID);
		
		return $this->redirect()->toUrl('/order/payment-complete/?reserveId=' . $reserveId);
	}

	/**
	 * Check is phone exists in erp
	 */
	public function checkPhoneAction() {
		$request = $this->getRequest();
		
		if (!$request->isXmlHttpRequest()) {
			return $this->notfound();
		}
		
		$phone = $request->getPost('phone');
		$phone = '7' . $phone;
		
		if ($this->auth()->logged() && !MANAGER_MODE) {
			return new JsonModel([
				'state' => 'ok' 
			]);
		}
		
		$result = $this->identity()->users()->isClientExists($phone);
		
		if ($result->hasError()) {
			return new JsonModel([
				'state' => 'error',
				'message' => 'Неизвестная ошибка' 
			]);
		}
		
		if ($result->success() && !MANAGER_MODE) {
			return new JsonModel(
				[
					'state' => 'request-password',
					'phone' => preg_replace('/^(\d{3})(.*)$/', '+7 $1 $2', $request->getPost('phone')),
					'message' => 'Такой телефон уже есть в системе, введите пароль для входа на сайт:',
					'button-text' => 'Войти и продолжить' 
				]);
		}
		
		return new JsonModel([
			'state' => 'ok' 
		]);
	}

	public function loginAction() {
		$request = $this->getRequest();
		
		if (!$request->isXmlHttpRequest()) {
			return $this->notfound();
		}
		
		$login = '7' . $request->getPost('phone');
		$password = $request->getPost('pass');
		
		$login = !preg_match('/@/', $login) ? preg_replace("/\D/", '', $login) : strtolower($login);
		
		$authResult = $this->auth()->login($login, $password);
		
		if ($authResult->success()) {
			return new JsonModel([
				'state' => 'ok' 
			]);
		}
		
		switch ($authResult->getErrorCode()) {
			case AuthResult::ERROR_UNDEFINED:
				$error = "Ошибка авторизации. Повторите попытку позже.";
				break;
			case AuthResult::ERROR_LOGIN_PASS_PAIR_NOT_EXISTS:
				$error = "Нет указанной пары логин-пароль";
				break;
			default:
				$error = "Неизвестная ошибка";
		}
		
		return new JsonModel([
			'state' => 'error',
			'message' => $error 
		]);
	}

	private function generatePremilinaryDeliveryCostParams($latitude, $longitude, array $articles, $floor = 0, $elevatorTypeId = 0) {
		$result = [];
		
		$result['Longitude'] = $longitude;
		$result['Latitude'] = $latitude;
		if (0 < $floor) {
			$result['Latitude'] = $latitude;
		}
		$articlesInfo = [];
		foreach ($articles as $articleId => $quantity) {
			$articlesInfo[] = [
				'Id' => $articleId,
				'Quantity' => $quantity 
			];
		}
		$result['Articles'] = $articlesInfo;
		
		$result['Floor'] = $floor;
		$result['ElevatorTypeID'] = $elevatorTypeId;
		
		return $result;
	}

}
