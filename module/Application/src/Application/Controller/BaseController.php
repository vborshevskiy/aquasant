<?php

namespace Application\Controller;

use Solo\Mvc\Controller\ActionController;
use Solo\Cookie\Cookie;
use SoloCatalog\Entity\Categories\MainMenu;
use SoloIdentity\Entity\Phone;
use Google\Analytics\Entity\Ecommerce\Tracker as EcommerceTracker;
use Google\Analytics\Entity\Ecommerce\TrackerBasketProduct;
use Zend\Http\Request as HttpRequest;
use Solo\Db\QueryGateway\QueryGateway;

abstract class BaseController extends ActionController {

	/**
	 *
	 * @var \SoloDelivery\Entity\PostDeliveryRegion
	 */
	public $userRegion = null;

	/**
	 *
	 * @var MainMenu
	 */
	protected $menu;

	/**
	 *
	 * @see \Solo\Mvc\Controller\ActionController::preDispatch()
	 */
	public function preDispatch(\Zend\Mvc\MvcEvent $event) {
		parent::preDispatch($event);
		
		if ($this->getRequest() instanceof HttpRequest) {
			$secureDomains = [
				'dev.'.DOMAIN_NAME 
			];
			$secureDomainsAllowableUrls = [
				'/debug/upload/' 
			];
			if (in_array($_SERVER['HTTP_HOST'], $secureDomains) && !in_array($_SERVER['REQUEST_URI'], $secureDomainsAllowableUrls)) {
				$authenticated = false;
				
				if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
					$user = $_SERVER['PHP_AUTH_USER'];
					$pass = $_SERVER['PHP_AUTH_PW'];
					if ($user == "santeh" && $pass == "baza") {
						$authenticated = true;
					}
				}
				
				if (!$authenticated) {
					header('WWW-Authenticate: Basic realm="Restricted Area"');
					header('HTTP/1.1 401 Unauthorized');
					echo ("Access denied.");
					exit();
				}
			}
			
			// города для почтовой торговли
			$postRegionId = $this->postDelivery()->getUserRegionId();
			if (is_null($postRegionId) && is_null($this->userRegion)) {
				$this->userRegion = $this->region()->getRegionById(\SoloDelivery\Service\PostDeliveryService::DEFAULT_CITY_ID);
				
				$this->postDelivery()->setUserRegionId(\SoloDelivery\Service\PostDeliveryService::DEFAULT_CITY_ID);
				$this->postDelivery()->isUserRegionConfirmed(false);
			}
			
			$godMode = (Cookie::exists('god_mode') && (1 == Cookie::get('god_mode')));
			if (('god' == $this->params()->fromQuery('mode')) && ('on' == $this->params()->fromQuery('status')) && ('HJ34235' == $this->params()->fromQuery('hash'))) {
				Cookie::set('god_mode', 1, null, (time() + 60 * 60 * 24 * 30), '/', '.'.DOMAIN_NAME);
				$godMode = true;
			}
			if (('god' == $this->params()->fromQuery('mode')) && ('off' == $this->params()->fromQuery('status'))) {
				Cookie::set('god_mode', 0, null, (time() + 60 * 60 * 24 * 30), '/', '.'.DOMAIN_NAME);
				$godMode = false;
			}
			$godMode = false;
			if (false !== stripos($this->getRequest()->getUri()->getPath(), '/polygon')) {
				$godMode = true;
			}
			if (false !== stripos($this->getRequest()->getUri()->getPath(), '/search') && (1 == $this->params()->fromQuery('polygon'))) {
				$godMode = true;
			}
			if (false !== stripos($this->getRequest()->getUri()->getPath(), '/goods') && (1 == $this->params()->fromQuery('polygon'))) {
				$godMode = true;
			}
			if (!defined('GODE_MODE')) {
				define('GOD_MODE', $godMode);
			}
			
			$managerMode = ($this->auth()->logged() && $this->auth()->getUser() && $this->auth()->getUser()->isManager());
			if (!defined('MANAGER_MODE')) {
				define('MANAGER_MODE', $managerMode);
			}
			
			if (isset($_GET['manager']) && 'super' == $_GET['manager']) {
				$this->auth()->getUser()->isSuperManager(true);
			}
		}
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \Solo\Mvc\Controller\ActionController::getLayout()
	 */
	protected function getLayout() {
		if (!$this->hasLayout()) {
			$this->setLayout($this->getServiceLocator()->get('Solo\\Mvc\\Controller\\Layout'));
		}
		return parent::getLayout();
	}

	/**
	 *
	 * @return MainMenu
	 */
	protected function createMenu() {
		if (!$this->menu) {
			$urlParams = [];
			
			$dir = $this->params()->fromQuery('dir');
			$column = $this->params()->fromQuery('sort');
			
			$sorting = $this->catalog()->goods()->getUserDefinedSorting($dir, $column);
			
			if (null !== $sorting) {
				$urlParams['sort'] = $sorting['column'];
				$urlParams['dir'] = $sorting['direction'];
			}
			
			$cityId = $this->getErpCityId();
			$isPostDelivery = $this->isPostDelivery();
			$this->menu = $this->catalog()->menu()->getMenu($cityId, $isPostDelivery, $urlParams);
		}
		
		return $this->menu;
	}

	protected function setDebugMode() {
		$this->getLayout()->setDebug(true);
	}

	/**
	 *
	 * @param string $key
	 *        	// example site.id
	 * @throws \RuntimeException
	 * @return mixed
	 */
	protected function getConfig($key) {
		$config = $this->getServiceLocator()->get('Config');
		if (!empty($config)) {
			$code = 'return $config[\'' . str_replace('.', '\'][\'', $key) . '\'];';
			$value = eval($code);
			return $value;
		} else {
			throw new \RuntimeException('Failed to load config section for application');
		}
	}

	/**
	 * override ActionController methods
	 */
	protected function outputVars($vars = []) {
		if ('Application\Controller\CabinetController' != get_class($this)) {
			if (Cookie::get('visited') == 1) {
				//$vars['htmlClass'] = 'uc-shown';
			} else {
				$vars['htmlClass'] = 'first';
				Cookie::set('visited', '1', null, strtotime('+ 1 year'), '/');
			}
		}
		
		$vars['menu'] = $this->createMenu();
		$vars['this'] = $this;
		
		$vars['absHost'] = 'https://www.'.DOMAIN_NAME;
		
		if ($this->auth()->logged()) {
			$vars['user'] = $this->auth()->getUser();
		}
		
		$bannerService = $this->service('\Application\Service\BannerService');
		
		$vars['basketTotalAmount'] = intval($this->basket()->getTotalAmount());
		$vars['basketItemsCount'] = intval($this->basket()->getTotalQuantity());
		$vars['menuBanner'] = $bannerService->getRandomBanner(1);
		$vars['comparison'] = $this->comparer()->getComparisonInfo();
		
		$userRegion = $this->getUserRegion();
		$vars['userRegion'] = $userRegion;
		
		//clientCode Begin
		if(isset($_SESSION['clientCodeActivTime'])){
			if($_SESSION['clientCodeActivTime']<time()-1800){
				unset($_SESSION['clientCode']);
			}
		 }
		 $_SESSION['clientCodeActivTime']=time();

		if(isset($_SESSION['clientCode'])){
			$clientCode=$_SESSION['clientCode'];
			
		}else{
			$clientCode= sprintf("%'.02d",rand(0,99))."-".sprintf("%'.02d",rand(0,99));
			$_SESSION['clientCode']=$clientCode;
		}
		//clientCode End


		$vars['clientCode']=$clientCode;
		$this->trackClientCode($clientCode);

		 
		$isDefaultCity = false;
		if ($userRegion) {
			$isDefaultCity = (42 == $userRegion->getId());
		}
		$vars['isDefaultCity'] = $isDefaultCity;
		
		return parent::outputVars($vars);
	}

	/**
	 *
	 * @return \SoloDelivery\Entity\PostDeliveryRegion
	 */
	protected function getUserRegion() {
		$this->updateUserRegionId();
		return $this->userRegion;
	}

	protected function isPostDelivery() {
		return false;
		// $this->updateUserRegionId();
		// return $this->userRegion->getId() != \SoloDelivery\Service\PostDeliveryService::DEFAULT_CITY_ID;
	}

	/**
	 *
	 * @return integer
	 */
	protected function getErpCityId() {
		$this->updateUserRegionId();
		
		if (is_null($this->userRegion)) {
			$userRegion = $this->region()->getRegionById(\SoloDelivery\Service\PostDeliveryService::DEFAULT_CITY_ID);
			return $userRegion->getErpCityId();
		}
		
		return $this->userRegion->getErpCityId();
	}

	/**
	 *
	 * @return \SoloDelivery\Entity\PostDeliveryRegion
	 */
	protected function updateUserRegionId($userRegionId = 0) {
		if (is_null($this->userRegion) || $userRegionId > 0) {
			$userRegionId = ($userRegionId > 0 ? $userRegionId : $this->postDelivery()->getUserRegionId());
			if (!is_null($userRegionId)) {
				$this->userRegion = $this->region()->getRegionById($userRegionId);
			} else {
				$this->userRegion = $this->region()->getRegionById(\SoloDelivery\Service\PostDeliveryService::DEFAULT_CITY_ID);
			}
		}
		
		return $this->userRegion;
	}

	/**
	 *
	 * @param string $search        	
	 * @return string
	 */
	public function getCategoryFromRoute($search) {
		$result = $search;
		
		$start = 0;
		$finish = strlen($search);
		
		$specialChars = [
			'__',
			'-' 
		];
		foreach ($specialChars as $specialChar) {
			$charNumber = strpos($search, $specialChar);
			if ((false !== $charNumber) && (intval($charNumber) < $finish)) {
				$finish = intval($charNumber);
			}
		}
		
		if ($finish < strlen($search)) {
			$result = substr($search, $start, $finish);
		}
		
		return $result;
	}

	/**
	 *
	 * @param string $search        	
	 * @return string
	 */
	public function getSetsFromRoute($search) {
		$result = [];
		
		if (false !== strpos($search, '-')) {
			$start = strpos($search, '-') + 1;
			$finish = strlen($search);
			
			$specialChars = [
				'__' 
			];
			foreach ($specialChars as $specialChar) {
				$charNumber = strpos($search, $specialChar);
				if ((false !== $charNumber) && (intval($charNumber) < $finish)) {
					$finish = intval($charNumber);
				}
			}
			
			if (0 < $start) {
				$finish -= $start;
			}
			if ($finish < strlen($search)) {
				$subresult = substr($search, $start, $finish);
				$result = explode('-', $subresult);
			}
		}
		
		return $result;
	}

	/**
	 *
	 * @param string $search        	
	 * @return string
	 */
	public function getCategorySetFromRoute($search) {
		$charNumber = strpos($search, "-");
		if ($charNumber === false) {
			return $search;
		}
		return substr($search, 0, strpos($search, "__"));
	}

	/**
	 *
	 * @param string $search        	
	 * @return integer
	 */
	public function getBrandIdFromRoute($search) {
		$charNumber = strpos($search, "___");
		if ($charNumber !== false) {
			$search = substr($search, 0, $charNumber);
		}
		$charNumber = strpos($search, "__");
		if ($charNumber === false) {
			return null;
		}
		$search = substr($search, $charNumber);
		return (int)end(explode('__', $search));
	}

	/**
	 *
	 * @param string $search        	
	 * @return integer
	 */
	public function getPageNumberFromRoute($search) {
		return (int)substr($search, strpos($search, "___page") + strlen('___page'));
	}

	/**
	 *
	 * @param integer $cityId        	
	 * @return \Zend\Http\PhpEnvironment\Response
	 */
	public function cityRedirect($cityId) {
		$subdomain = $this->facility()->cities()->getSubdomainByCityId($cityId);
		$this->facility()->cities()->setCityId($cityId, $this->getMainDomain());
		$referer = $this->getRequest()->getHeader('Referer');
		if ($referer !== false) {
			$redirect = $referer->uri();
			$host = $redirect->getHost();
			if (is_null($this->facility()->cities()->getSubdomain())) {
				$host = str_replace('www.', '', $host);
				if (empty($subdomain)) {
					$redirect->setHost(trim($host));
				} else {
					$redirect->setHost(trim($subdomain . '.' . $host, '.'));
				}
			} else {
				foreach (explode('.', $host) as $hostPath) {
					if ($this->facility()->cities()->issetCitySubdomain($hostPath)) {
						$host = str_replace($hostPath, $subdomain, $host);
					}
				}
				$host = trim($host, '.');
				$redirect->setHost($host);
			}
		} else {
			$redirect = $this->getRequest()->getUri();
			$redirect->setHost(str_replace('www.', '', $redirect->getHost()));
			if (!empty($subdomain)) {
				$redirect->setHost($subdomain . '.' . $this->getMainDomain());
			} else {
				$redirect->setHost($this->getMainDomain());
			}
		}
		if (strpos($redirect->getPath(), 'changeCity')) {
			$redirect->setPath(null);
			$redirect->setQuery(null);
		}
		return $this->redirect()->toUrl($redirect->toString());
	}

	/**
	 *
	 * @return string
	 */
	public function getMainDomain() {
		$uri = $this->getRequest()->getUri();
		$host = $uri->getHost();
		foreach (explode('.', $host) as $hostPath) {
			if ($this->facility()->cities()->issetCitySubdomain($hostPath)) {
				$host = str_replace($hostPath, '', $host);
			}
		}
		return trim(str_replace('www.', '', $host), '.');
	}

	/**
	 *
	 * @param string $tempalte        	
	 * @param integer $cityId        	
	 * @return string
	 */
	public function getSeoTitle($tempalte, $cityId) {
		$config = $this->getServiceLocator()->get('config');
		if (isset($config['seo']['meta'][$cityId])) {
			$seoWords = $config['seo']['meta'][$cityId];
		}
		
		$tempalte = str_replace('{where}', $seoWords['where'], $tempalte);
		$tempalte = str_replace('{whereShort}', $seoWords['whereShort'], $tempalte);
		$tempalte = str_replace('{cityName}', $seoWords['cityName'], $tempalte);
		$tempalte = str_replace('{short}', $seoWords['short'], $tempalte);
		return $tempalte;
	}

	/**
	 *
	 * @return string | null
	 */
	public function getCounter() {
		$subdomain = $this->facility()->cities()->getSubdomain();
		if (is_null($subdomain)) {
			$subdomain = 'msk';
		}
		if (file_exists('public/counters/' . $subdomain . '.txt')) {
			return file_get_contents('public/counters/' . $subdomain . '.txt');
		}
		return null;
	}

	/**
	 *
	 * @param string $phone        	
	 * @param string $format        	
	 * @return string
	 */
	public function formatPhone($phone, $format) {
		$obj = Phone::createFromString($phone);
		return $obj->toString($format);
	}

	protected function print_r() {
		print '<h1>Don\'t panic. Debugging</h1>';
		print '<pre>';
		foreach (func_get_args() as $obj) {
			print_r($obj);
		}
		print '</pre>';
		exit();
	}

	/**
	 *
	 * @return \Google\Analytics\Entity\Ecommerce\Tracker
	 */
	protected function createEcommerceTracker($pageType = null) {
		$tracker = new EcommerceTracker();
		if (null !== $pageType) {
			$tracker->setPageType($pageType);
		}
		
		// basket products
		$goodIds = $this->basket()->enumGoodIds();
		if (0 < count($goodIds)) {
			$goods = $this->catalog()->goods()->enumGoodsByIds($goodIds);
			$siteCategories = $this->catalog()->goods()->enumSiteCategoryIds($goodIds);
			
			$mainMenu = $this->createMenu();
			$position = 1;
			foreach ($this->basket()->getGoods() as $basketGood) {
				if (isset($goods[$basketGood->getId()])) {
					$good = $goods[$basketGood->getId()];
					$productId = $good['GoodID'];
					
					$basketProduct = new TrackerBasketProduct();
					$basketProduct->setId($good['GoodID']);
					$basketProduct->setName($good['GoodName']);
					if (isset($siteCategories[$productId])) {
						$siteCategoryId = $siteCategories[$productId];
						$menuCategory = $mainMenu->findById($siteCategoryId);
						if ($menuCategory) {
							$basketProduct->setCategoryName($menuCategory->getName());
						} else {
							$basketProduct->setCategoryName('');
						}
					}
					$basketProduct->setPrice($basketGood->getPrice());
					$basketProduct->setQuantity($basketGood->getQuantity());
					$basketProduct->setBrandName($good['BrandName']);
					$basketProduct->setListName('Корзина');
					$basketProduct->setPosition($position);
					$position++;
					
					$tracker->addBasketProduct($basketProduct);
				}
			}
		}
		// END basket products
		
		return $tracker;
	}

	/**
	 *
	 * @return boolean
	 */
	public function isStoreWork() {
		$result = false;
		
		$dinfo = getdate();
		if ((9 <= $dinfo['hours']) && (($dinfo['hours'] < 22))) {
			$result = true;
		}
		
		return $result;
	}

	/**
	 *
	 * @param string $clientCode        	
	 */
	protected function trackClientCode($clientCode) {
		$queryGateway = new QueryGateway();
		$clientId = $this->google()->analytics()->getClientId();
		$sql = "INSERT INTO client_codes SET ClientID = '" . $clientId . "', ClientCode = '" . $clientCode . "' ON DUPLICATE KEY UPDATE ClientCode = '" . $clientCode . "'";
		$queryGateway->query($sql);
	}
	
	/**
	 * 
	 * @param string $clientCode
	 * @return string
	 */
	protected function getClientIdByCode($clientCode) {
		$result = '';
		
		$queryGateway = new QueryGateway();
		$sql = "SELECT * FROM client_codes WHERE ClientCode = '".$clientCode."' LIMIT 1";
		$rows = $queryGateway->query($sql);
		if (0 < $rows->count()) {
			$row = $rows->current();
			$result = $row->ClientID;
		}
		
		return $result;
	}

}
