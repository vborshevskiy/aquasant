<?php

namespace Application\Controller;

use Zend\View\Model\JsonModel;

class PostDeliveryController extends BaseController {

    public function searchCityAction() {
        $result = [];
        $query = $this->params()->fromQuery('term');
        $cities = $this->region()->searchCities($query);

        foreach ($cities as $city) {
            $result[] = [
                'value' => $city->getId(),
                'label' => $city->getName(),
            ];
        }

        return new JsonModel($result);
    }

    public function changeCityAction() {
        $regionId = (int) $this->params()->fromRoute('id');

        $this->postDelivery()->setUserRegionId($regionId);
        $this->postDelivery()->isUserRegionConfirmed(true);

        $this->forward()->dispatch(BasketController::class,[
            'action' => 'changePriceColumn',
            'regionId' => $regionId,
        ]);

        $request = $this->getRequest();

        $referer = $request->getHeader('referer');

        if ($referer) {
            return $this->redirect()->toUrl($referer->getUri());
        }

        return $this->redirect()->toUrl('/');
    }
}