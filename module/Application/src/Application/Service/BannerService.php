<?php

namespace Application\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class BannerService implements ServiceLocatorAwareInterface {
    use ServiceLocatorAwareTrait;

    public function getRandomBanner($groupKey) {
        $config = $this->getServiceLocator()->get('Config');

        $banners = $config['banners'][$groupKey];

        if (empty($banners)) {
            return false;
        }

        $bannerNumber = rand(0, count($banners) - 1);

		$url = isset($banners[$bannerNumber][1]) ? $banners[$bannerNumber][1] : null;
		if (empty($url)) {
			$url = '/about/';
		}

        return [
            'image' => '/images/banners/' . $banners[$bannerNumber][0],
            'url' => $url
        ];
    }

}