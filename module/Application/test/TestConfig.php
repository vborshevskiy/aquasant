<?php
return array(
    'modules' => array(
        'Solo',
        'Application',
        'SoloWarehouse',
        'SoloERP',
        'SoloIdentity',
        'SoloCompare',
        'SoloCatalog',
        'SoloSettings',
        'SoloCache',
        'SoloSearch',
        'SoloOrder',
        'Predis',
        'Smarty',
        'SoloFacility',
        'SoloDelivery',
        'SoloLog',
        'SoloCabinet',
        'SoloNotify',
        'SxGeo',
    ),
    'module_listener_options' => array(
        'config_glob_paths'    => array(
            '../../../config/autoload/{,*.}{global,local}.php',
        ),
        'module_paths' => array(
            'module',
            'vendor',
        ),
    ),
);
