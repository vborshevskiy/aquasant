<?php

namespace ApplicationTest;


abstract class BaseTestCase extends \PHPUnit_Framework_TestCase {

    protected $controller;
    protected $request;
    protected $response;
    protected $routeMatch;
    protected $event;
    protected $testName = 'noname test';
    
    public function onNotSuccessfulTest(\Exception $exception) {
        $sm = \ApplicationTest\Bootstrap::getServiceManager();
        $wsLogger = $sm->get('SoloLog\Service\LogManager')->create('ws_logger');
        $message = 'Тест: ' . $this->testName . "\n";
        $message .= '-----------Exception-----------' . "\n";
        $message .= 'Code: ' . $exception->getCode() . "\n" . 'Message: ' . $exception->getMessage() . "\n";
        $message .= 'Trace: ' . "\n";
        $message .= $exception->getTraceAsString() . "\n\n";
        $prevException = $exception->getPrevious();
        if ($prevException) {
            $message .= '-----------Previous exceptions-----------' . "\n\n";
            while ($prevException) {
                $message .= 'Code: ' . $prevException->getCode() . "\n" . 'Message: ' . $prevException->getMessage() . "\n";
                $message .= 'Trace: ' . "\n";
                $message .= $prevException->getTraceAsString() . "\n\n";
                $prevException = $prevException->getPrevious();
            }
        }
        $message .= '=======================================' . "\n\n\n\n";
        $wsLogger->err($message);

        $notifyService = $sm->get('SoloNotify\Service\NotifyService');
        $notifyService->send($message, 'Провален ' . $this->testName);
        
        $this->fail('See details in the mail and logs');
    }
    
}

?>
