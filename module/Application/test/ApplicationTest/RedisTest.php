<?php
namespace ApplicationTest;

class RedisTest extends BaseTestCase {
    
    protected $testName = 'redis юнит-тест';
    
    /**
     *
     * @var \Predis\Client
     */
    protected $redisConnection = null;
    
    public function setUp() {
        $serviceManager = Bootstrap::getServiceManager();
        $config = $serviceManager->get('Config');
        $this->redisConnection = new \Predis\Client($config['predis_settings']);
    }
    
    public function testRedisConnection() {
        try {
            $this->redisConnection->connect();
            $this->redisConnection->disconnect();
        } catch (\Predis\Connection\ConnectionException $ex) {
            $this->fail('Connection to Redis server has refused');
        }
    }
    
}

?>
