<?php
namespace ApplicationTest;

class MysqlTest extends BaseTestCase {
    
    protected $dbAdapter = null;
    protected $testName = 'MySQL юнит-тест';
    
    public function setUp() {
        $serviceManager = Bootstrap::getServiceManager();
        $this->dbAdapter = $serviceManager->get('Zend\Db\Adapter\Adapter');
    }
    
    public function testDbConnection() {
        try {
            $this->dbAdapter->query('SHOW TABLES');
        } catch (\RuntimeException $ex) {
            $this->fail('Can\'t connect to mysql server');
        }
    }
    
}

?>
