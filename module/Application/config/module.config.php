<?php

use Application\Controller\CabinetController;
use Application\Controller\CompareController;
use Application\Controller\ContactsController;
use Application\Controller\DownloadController;
use Application\Controller\LoginController;
use Application\Controller\OrderController;
use Application\Controller\PageController;
use Application\Controller\PostDeliveryController;
use Application\Controller\SearchController;
use Application\Controller\DebugController;
use Application\Service\BannerService;
use Zend\Mail\Transport\SmtpOptions;

use Application\Controller\IndexController;
use Application\Controller\GenericController;
use Application\Controller\CatalogController;
use Application\Controller\ItemController;
use Application\Controller\BasketController;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'priority' => 1000,
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'index'
                    ]
                ]
            ],
            'generic' => [
                'type' => 'Regex',
                'options' => [
                    'regex' => '/(?<search>[a-zA-Z0-9_-]+)(\/)?',
                    'defaults' => [
                        'controller' => GenericController::class,
                        'action' => 'index'
                    ],
                    'spec' => '/%search%'
                ]
            ],
            'why' => [
            	'type' => 'Literal',
            	'options' => [
            		'route' => '/why/',
            		'defaults' => [
            			'controller' => GenericController::class,
	            		'action' => 'why'
    	        	]
            	]
            ],
            'catalog_item' => [
                'type' => 'Regex',
                'priority' => 2,
                'options' => [
                    'regex' => '/(?<category>.*?)/(?<item>[0-9]+).*?(\/)?',
                    'defaults' => [
                        'controller' => ItemController::class,
                        'action' => 'index'
                    ],
                    'spec' => '/%category%/%item%'
                ]
            ],
            'plygon' => [
	            'type' => 'Literal',
	            'priority' => 1000,
	            'options' => [
	            	'route' => '/polygon/',
	            	'defaults' => [
	            		'controller' => CatalogController::class,
	            		'action' => 'polygon'
            		],
            	]
            ],
            'item-properties' => [
            	'type' => 'Literal',
            	'priority' => 1000,
            	'options' => [
                    'route' => '/item/properties/',
                    'defaults' => [
                        'controller' => ItemController::class,
                        'action' => 'properties'
                    ],
                ]
            ],
            'basket' => [
                'type' => 'segment',
                'options' => [
                    'route' => '/basket/[:action/]',
                    'defaults' => [
                        'controller' => BasketController::class,
                        'action' => 'index'
                    ],
                ]
            ],
            'download' => [
                'type' => 'segment',
                'options' => [
                    'route' => '/download/',
                    'defaults' => [
                        'controller' => DownloadController::class,
                        'action' => 'download'
                    ],
                ]
            ],
            'order' => [
                'type' => 'Segment',
                'priority' => '1000',
                'options' => [
                    'route' => '/order/[:action/]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ],
                    'defaults' => [
                        'controller' => OrderController::class,
                        'action' => 'index'
                    ]
                ]
            ],
            'login' => [
                'type' => 'segment',
                'options' => [
                    'route' => '/login/[:action/]',
                    'defaults' => [
                        'controller' => LoginController::class,
                        'action' => 'index'
                    ],
                ]
            ],
            'cabinet' => [
                'type' => 'Segment',
                'priority' => 1000,
                'options' => [
                    'route' => '/cabinet/[:action/]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'other' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ],
                    'defaults' => [
                        'controller' => CabinetController::class,
                        'action' => 'index'
                    ]
                ]
            ],
            'post-delivery' => [
                'type' => 'Segment',
                'priority' => 1000,
                'options' => [
                    'route' => '/post-delivery[/:action/]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ],
                    'defaults' => [
                        'controller' => PostDeliveryController::class,
                        'action' => 'index'
                    ]
                ]
            ],
            'change-city' => [
                'type' => 'Segment',
                'priority' => 1000,
                'options' => [
                    'route' => '/city/:id/',
                    'constraints' => [
                        'action' => '[0-9]+'
                    ],
                    'defaults' => [
                        'controller' => PostDeliveryController::class,
                        'action' => 'change-city'
                    ]
                ]
            ],
            'compare' => [
                'type' => 'Segment',
                'priority' => 1,
                'options' => [
                    'route' => '/compare/[:category/]',
                    'constraints' => [
                        'category' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ],
                    'defaults' => [
                        'controller' => CompareController::class,
                        'action' => 'index'
                    ]
                ]
            ],
            'compare-manage' => [
                'type' => 'literal',
                'priority' => 1,
                'options' => [
                    'route' => '/compare/update/',
                    'defaults' => [
                        'controller' => CompareController::class,
                        'action' => 'update'
                    ]
                ]
            ],
            'search-autocomplete' => [
                'type' => 'literal',
                'priority' => 1000,
                'options' => [
                    'route' => '/search/autocomplete/',
                    'defaults' => [
                        'controller' => SearchController::class,
                        'action' => 'autocomplete'
                    ]
                ]
            ],
            'search' => [
                'type' => 'Segment',
                'priority' => 999,
                'options' => [
                    'route' => '/search/[:category/]',
                    'constraints' => [
                        'category' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ],
                    'defaults' => [
                        'controller' => SearchController::class,
                        'action' => 'index'
                    ]
                ]
            ],
            'debug' => [
            'type' => 'Segment',
            'priority' => 1000,
            'options' => [
            'route' => '/debug/[:action][/]',
            'constraints' => [
            'action' => '[a-zA-Z_]*'
            	],
            	'defaults' => [
            	'controller' => DebugController::class,
            	'action' => 'index'
            		]
            		]
            		],
            'contacts' => [
                'type' => 'Segment',
                'priority' => 1000,
                'options' => [
                    'route' => '/contacts/[:action/]',
                    'defaults' => [
                        'controller' => ContactsController::class,
                        'action' => 'index'
                    ]
                ]
            ],
            'aquasant-about' => [
                'type' => 'literal',
                'priority' => 1000,
                'options' => [
                    'route' => '/about/',
                    'defaults' => [
                        'controller' => PageController::class,
                        'action' => 'about'
                    ]
                ]
            ],
            'aquasant-delivery' => [
                'type' => 'literal',
                'priority' => 1000,
                'options' => [
                    'route' => '/delivery/',
                    'defaults' => [
                        'controller' => PageController::class,
                        'action' => 'delivery'
                    ]
                ]
            ],
            'aquasant-payments' => [
                'type' => 'literal',
                'priority' => 1000,
                'options' => [
                    'route' => '/payments/',
                    'defaults' => [
                        'controller' => PageController::class,
                        'action' => 'payments'
                    ]
                ]
            ],
            'aquasant-install' => [
                'type' => 'literal',
                'priority' => 1000,
                'options' => [
                    'route' => '/install/',
                    'defaults' => [
                        'controller' => PageController::class,
                        'action' => 'install'
                    ]
                ]
            ],
            'aquasant-about' => [
                'type' => 'literal',
                'priority' => 1000,
                'options' => [
                    'route' => '/about/',
                    'defaults' => [
                        'controller' => PageController::class,
                        'action' => 'about'
                    ]
                ]
            ],
            'aquasant-refund' => [
                'type' => 'literal',
                'priority' => 1000,
                'options' => [
                    'route' => '/refund/',
                    'defaults' => [
                        'controller' => PageController::class,
                        'action' => 'refund'
                    ]
                ]
            ],
            'aquasant-agreement' => [
                'type' => 'literal',
                'priority' => 1000,
                'options' => [
                    'route' => '/agreement/',
                    'defaults' => [
                        'controller' => PageController::class,
                        'action' => 'agreement'
                    ]
                ]
            ],
        ]
    ],
    'controllers' => [
        'invokables' => [
            IndexController::class => IndexController::class,
            CatalogController::class => CatalogController::class,
            GenericController::class => GenericController::class,
            ItemController::class => ItemController::class,
            BasketController::class => BasketController::class,
            DownloadController::class => DownloadController::class,
            OrderController::class => OrderController::class,
            LoginController::class => LoginController::class,
            CabinetController::class => CabinetController::class,
            PostDeliveryController::class => PostDeliveryController::class,
            CompareController::class => CompareController::class,
            SearchController::class => SearchController::class,
            ContactsController::class => ContactsController::class,
            PageController::class  => PageController::class,
            DebugController::class  => DebugController::class,
        ]
    ],
    'controller_plugins' => [
        'invokables' => [
            'notfound' => 'Application\Controller\Plugin\NotFound',
            'problem' => 'Application\Controller\Plugin\Problem',
            'app' => 'Application\Controller\Plugin\App',
            'renderer' => 'Application\Controller\Plugin\Renderer',
            'application' => 'Application\Controller\Plugin\Application',
        ]
    ],
	'console' => [
		'router' => [
			'routes' => [
				'debug' => [
					'options' => [
						'route' => 'debug [<action>]',
						'defaults' => [
							'controller' => 'Application\Controller\DebugController',
							'action' => 'index'
						]
					]
				]
			]
		]
	],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'layout' => 'layout/inner',
        'template_path_stack' => [
            __DIR__ . '/../../templates'
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'serializers' => [
    ],
    'service_manager' => [
        'invokables' => [
            BannerService::class => BannerService::class
        ],
        'aliases' => [
        ],
        'factories' => [
            'Solo\Mvc\Controller\Layout' => 'Solo\Mvc\Controller\Layout\LayoutServiceFactory',
            'mailer' => function ($sm) {
                $config = $sm->get('Config');
                if (isset($config['mail'])) {
                    $transport = (isset($config['mail']['transport'])) ? $config['mail']['transport'] : 'smtp';
                    if ($transport == 'smtp') {
                        unset($config['mail']['transport']);
                        unset($config['mail']['from']);
                        unset($config['mail']['fromName']);
                        unset($config['mail']['reply_to']);
                        $options = new SmtpOptions($config['mail']);
                        $mailer = new \Zend\Mail\Transport\Smtp($options);
                    } elseif ($transport == 'sendmail') {
                        $mailer = new \Zend\Mail\Transport\Sendmail();
                    }
                    $mailer->attemptsCount = $config['mailer']['max_send_attempts'];
                    return $mailer;
                }
                return null;
            },
            'captcha_session' => function ($sm) {
                return new \Zend\Session\Container('captcha_session');
            },
        ]
    ]
];

