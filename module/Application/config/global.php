<?php
return [
	'layout' => [
		'adapter' => 'frontend',
		'debug' => false,
		'options' => [
			'stylesheet' => [
				'folder' => 'css',
				'ieFolder' => 'layout/www/css/ie',
				'extension' => 'css' 
			],
			'javascript' => [
				'folder' => 'js',
				'ieFolder' => 'layout/www/js/ie',
				'extension' => 'js' 
			],
			'autoload' => [
				'stylesheet' => [
					'fonts.css',
					'reset.css',
					'rub.css',
					'common.css',
					'wide-screen.css',
					'middle-screen.css',
					'mobile.css' 
				],
				'stylesheetIe' => [
					'ie9' => [] 
				],
				'javascript' => [
					'jquery.suggestions.min.js',
					'ga.helper.js',
					'datepicker.js',
					'common.js' 
				] 
			] 
		] 
	],
	'renderer' => [
		'engine' => 'Smarty\View\Renderer\SmartyRenderer',
		'templatesFolder' => __DIR__ . '/../../../templates/application' 
	],
	'seo' => [
		'meta' => [
			2 => [
				'cityName' => 'Москва',
				'where' => 'Москве',
				'whereShort' => 'москве',
				'short' => 'москва' 
			],
			1 => [
				'cityName' => 'Санкт-Петербург',
				'where' => 'Санкт-Петербурге',
				'whereShort' => 'санкт-петербурге',
				'short' => 'санкт-петербург' 
			] 
		] 
	],
	'banners' => [
		[
			[
				'224-1.jpg' 
			],
			[
				'224-2.jpg' 
			],
			[
				'224-3.jpg' 
			],
			[
				'224-4.jpg' 
			],
			[
				'224-5.jpg' 
			],
			[
				'224-6.jpg' 
			],
			[
				'224-7.jpg' 
			],
			[
				'224-8.jpg' 
			],
			[
				'224-9.jpg' 
			],
			[
				'224-10.jpg' 
			],
			[
				'224-11.jpg' 
			],
			[
				'224-12.jpg' 
			],
			[
				'224-13.jpg' 
			],
			[
				'224-14.jpg' 
			],
			[
				'224-15.jpg' 
			],
			[
				'224-16.jpg' 
			],
			[
				'224-17.jpg' 
			] 
		],
		[
			[
				'393-1.jpg' 
			],
			[
				'393-2.jpg' 
			],
			[
				'393-3.jpg' 
			],
			[
				'393-4.jpg' 
			],
			[
				'393-5.jpg' 
			],
			[
				'393-6.jpg' 
			],
			[
				'393-7.jpg' 
			],
			[
				'393-8.jpg' 
			],
			[
				'393-9.jpg' 
			],
			[
				'393-10.jpg' 
			],
			[
				'393-11.jpg' 
			],
			[
				'393-12.jpg' 
			],
			[
				'393-13.jpg' 
			],
			[
				'393-14.jpg' 
			],
			[
				'393-15.jpg' 
			],
			[
				'393-16.jpg' 
			],
			[
				'393-17.jpg' 
			] 
		],
		[
			[
				'486-1.jpg' 
			],
			[
				'486-2.jpg' 
			],
			[
				'486-3.jpg' 
			],
			[
				'486-4.jpg' 
			],
			[
				'486-5.jpg' 
			],
			[
				'486-6.jpg' 
			],
			[
				'486-7.jpg' 
			],
			[
				'486-8.jpg' 
			],
			[
				'486-9.jpg' 
			],
			[
				'486-10.jpg' 
			],
			[
				'486-11.jpg' 
			],
			[
				'486-12.jpg' 
			],
			[
				'486-13.jpg' 
			],
			[
				'486-14.jpg' 
			],
			[
				'486-15.jpg' 
			],
			[
				'486-16.jpg' 
			],
			[
				'486-17.jpg' 
			] 
		],
		[
			[
				'523-1.jpg' 
			],
			[
				'523-2.jpg' 
			],
			[
				'523-3.jpg' 
			],
			[
				'523-4.jpg' 
			],
			[
				'523-5.jpg' 
			],
			[
				'523-6.jpg' 
			],
			[
				'523-7.jpg' 
			],
			[
				'523-8.jpg' 
			],
			[
				'523-9.jpg' 
			],
			[
				'523-10.jpg' 
			],
			[
				'523-11.jpg' 
			],
			[
				'523-12.jpg' 
			],
			[
				'523-13.jpg' 
			],
			[
				'523-14.jpg' 
			],
			[
				'523-15.jpg' 
			],
			[
				'523-16.jpg' 
			],
			[
				'523-17.jpg' 
			] 
		] 
	] 
];
