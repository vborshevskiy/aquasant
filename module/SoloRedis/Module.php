<?php

namespace SoloRedis;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\MvcEvent;
use Solo\ModuleManager\MultiConfigModule;
use SoloRedis\Service\ProvidesRedis;

class Module extends MultiConfigModule implements AutoloaderProviderInterface {

    public function __construct() {
        parent::__construct(__DIR__);
    }

    public function getAutoloaderConfig() {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }
    
    /**
     * 
     * @param MvcEvent $e
     */
    public function onBootstrap(MvcEvent $e) {
        $sm = $e->getApplication()->getServiceManager();
        $redisClient = $sm->get('PredisClient');
        ProvidesRedis::setStaticRedisClient($redisClient);
    }

}
