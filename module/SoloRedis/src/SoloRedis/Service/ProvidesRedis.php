<?php

namespace SoloRedis\Service;

trait ProvidesRedis {

    /**
     *
     * @var \Predis\Client
     */
    private static $staticRedisClient = null;

    /**
     *
     * @param \Predis\Client $redisClient        	
     */
    public static function setStaticRedisClient(\Predis\Client $redisClient) {
        self::$staticRedisClient = $redisClient;
    }

    /**
     *
     * @return \Predis\Client
     */
    public static function getStaticRedisClient() {
        return self::$staticRedisClient;
    }

    /**
     * 
     * @return \Predis\Client | null
     */
    protected function redis() {
        if (!is_null(self::$staticRedisClient)) {
            return self::$staticRedisClient;
        }
        return null;
    }

}
