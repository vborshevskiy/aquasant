<?php

namespace SoloFacility\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class StoresServiceFactory extends AbstractServiceFactory {

	/**
	 * (non-PHPdoc)
	 *
	 * @see \Solo\ServiceManager\AbstractServiceFactory::create()
	 */
	protected function create() {
		$storesMapper = $this->getServiceLocator()->get('SoloFacility\Data\StoresMapper');
		$service = new StoresService($storesMapper);
		return $service;
	}
}

?>