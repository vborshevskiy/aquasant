<?php

namespace SoloFacility\Service;

use SoloFacility\Entity\LocationCollection;
use SoloFacility\Entity\Location;
use SoloFacility\Data\LocationsTableInterface;

class LocationsService {

    /**
     *
     * @var LocationsTableInterface
     */
    protected $locationsTable;
    
    /**
	 *
	 * @param LocationsTableInterface $locationsTable     	
	 */
	public function __construct(LocationsTableInterface $locationsTable) {
		$this->locationsTable = $locationsTable;
	}

    /**
     * 
     * @param array $locationIds
     * @return LocationCollection
     */
    public function getLocationsByIds(array $locationIds) {
        if (empty($locationIds)) {
            return [];
        }
        $locationsData = $this->locationsTable->getLocationsByIds($locationIds);
        $locationCollection = new LocationCollection();
        foreach ($locationsData as $locationData) {
            $locationCollection->add($this->createLocation($locationData));
        }
        return $locationCollection;
    }
    
    /**
     * 
     * @param ResultSet $locationData
     * @return Location
     */
    private function createLocation($locationData) {
        $location = new Location();
        $location->setId((int)$locationData['LocationID']);
        $location->setName($locationData['LocationName']);
        $location->setParentId((int)$locationData['LocationParrentID']);      
        $location->setZoneId((int)$locationData['LocationZoneID']);             
        return $location;
    }

}

?>