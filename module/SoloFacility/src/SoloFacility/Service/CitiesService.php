<?php

namespace SoloFacility\Service;

use SoloFacility\Entity\CityCollection;
use SoloFacility\Entity\City;
use SoloFacility\Data\CitiesTableInterface;
use SoloFacility\Data\CitiesMapperInterface;
use Solo\Cookie\Cookie;

class CitiesService {

    /**
     *
     * @var CitiesTableInterface
     */
    protected $citiesTable;
    
    /**
     *
     * @var CitiesMapperInterface
     */
    protected $citiesMapper;
    
    /**
     *
     * @var array
     */
    protected $onlyDeliveryCitiesIds;
    
    /**
     *
     * @var array
     */
    protected $subdomains;
    
    /**
     *
     * @var integer
     */
    protected $defaultCityId;

    /**
     *
     * @param CitiesTableInterface $citiesTable     	
     */
    public function __construct(CitiesTableInterface $citiesTable, CitiesMapperInterface $citiesMapper, $onlyDeliveryCitiesIds = [], $subdoomains = [], $defaultCityId = 2) {
        $this->citiesTable = $citiesTable;
        $this->citiesMapper = $citiesMapper;
        $this->onlyDeliveryCitiesIds = $onlyDeliveryCitiesIds;
        if (is_array($subdoomains)) {
            $this->subdomains = $subdoomains;
        }
        $this->defaultCityId = (int)$defaultCityId;
    }

    /**
     * 
     * @param array $cityIds
     * @return CityCollection
     */
    public function getCitiesByIds(array $cityIds) {
        if (empty($cityIds)) {
            return [];
        }
        $citiesData = $this->citiesTable->getCitiesByIds($cityIds);
        $cityCollection = new CityCollection();
        foreach ($citiesData as $cityData) {
            $cityCollection->add($this->createCity($cityData));
        }
        return $cityCollection;
    }

    /**
     * 
     * @param ResultSet $cityData
     * @return City
     */
    private function createCity($cityData) {
        $city = new City();
        $city->setId((int) $cityData['CityID']);
        $city->setName($cityData['CityName']);
        $city->setZoneId((int) $cityData['CityZoneID']);
        $city->setPhone($cityData['CityPhone']);
        return $city;
    }

    public function getAllCities() {
        return $this->citiesMapper->getAllCities();
    }
    
    /**
     * 
     * @param ResultSet $cityId
     * @return City
     */
    public function getCityById($cityId) {
        return $this->createCity($this->citiesMapper->getCityById($cityId));
    }
    
    /**
     * 
     * @return City
     */
    public function getDefaultCity() {
        return $this->createCity($this->citiesMapper->getCityById($this->defaultCityId));
    }

//    public function getCityId($alwaysReturn = true) {
//        $cook = (Cookie::exists('selectedCity')) ? Cookie::get('selectedCity') : null;
//        $citiesIds = $this->citiesMapper->getCitiesIds();
//        if (!empty($cook) && in_array($cook,$citiesIds)) {
//            return (int)$cook;
//        } elseif($alwaysReturn) {
//            if (count($citiesIds) > 0) {
//                return (int)current($citiesIds);
//            }
//        }
//        return null;
//    }
    
    /**
     * 
     * @param boolean $alwaysReturn
     * @return integer|null
     * @throws \Exception
     */
    public function getCityId($alwaysReturn = true) {
        $subdomain = $this->getSubdomain();
        if (is_null($subdomain)) {
            $cityId = (Cookie::exists('selectedCity')) ? (int)Cookie::get('selectedCity') : null;
        } elseif (array_key_exists($subdomain, $this->subdomains)) {
            $cityId = (int)$this->subdomains[$subdomain];
        } else {
            throw new \Exception('Cat\'t find city id for subdomain "'.$subdomain.'"');
        }
        $citiesIds = $this->citiesMapper->getCitiesIds();
        if (!empty($cityId) && in_array($cityId,$citiesIds)) {
            return $cityId;
        } elseif($alwaysReturn) {
            if (count($citiesIds) > 0) {
                return (int)current($citiesIds);
            }
        }
        return null;
    }
    
    /**
     * 
     * @param int $cityId
     * @return int
     * @throws Exception
     */
    public function setCityId($cityId, $domain) {
        if (!is_int($cityId)) {
            throw new \Exception('City id must be integer', 500);
        }
        if (0 >= $cityId) {
            throw new \Exception('City id must be greater than zero', 500);
        }
        Cookie::set('selectedCity', $cityId, null, time() + 60 * 60 * 24 * 365, '/', '.'.$domain);
        Cookie::set('selectedCity', $cityId, null, time() + 60 * 60 * 24 * 365, '/', $domain);
        Cookie::set('selectedCity', $cityId, null, time() + 60 * 60 * 24 * 365, '/', '.www.'.$domain);
        Cookie::set('selectedCity', $cityId, null, time() + 60 * 60 * 24 * 365, '/', 'www.'.$domain);
        return $cityId;
    }
    
    /**
     * 
     * @param integer $cityId
     * @return boolean
     */
    public function hasDeliveryOnly($cityId) {
        return in_array($cityId, $this->onlyDeliveryCitiesIds);
    }
    
    /**
     * 
     * @return string|null
     */
    public function getSubdomain() {
        if (!isset($_SERVER['HTTP_HOST'])) {
            return null;
        }
        $host = $_SERVER['HTTP_HOST'];
        $hostArray = explode('.', $host);
        if (count($hostArray) > 0) {
            if (current($hostArray) !== 'www' && array_key_exists(current($hostArray), $this->subdomains)) {
                return current($hostArray);
            } elseif (count($hostArray) > 1 && array_key_exists($hostArray[1], $this->subdomains)) {
                return $hostArray[1];
            }
        }
        return null;
    }
    
    /**
     * 
     * @param integer $cityId
     * @return string|null
     */
    public function getSubdomainByCityId($cityId) {
        if (in_array($cityId, $this->subdomains)) {
            return array_search($cityId, $this->subdomains);
        }
        return null;
    }
    
    /**
     * 
     * @param string $hostPath
     * @return boolean
     */
    public function issetCitySubdomain($hostPath) {
        return array_key_exists($hostPath, $this->subdomains);
    }
    
    /**
     * 
     * @param integer $cityId
     * @return boolean
     */
    public function isDefaultCity($cityId) {
        return ($cityId === $this->defaultCityId);
    }
    
    /**
     * 
     * @return integer
     */
    public function getDefaultCityId() {
        return $this->defaultCityId;
    }
    
    /**
     * 
     * @param integer $cityId
     * @return integer
     */
    public function getRandomAvailGood($cityId) {
        if (!is_int($cityId)) {
            throw new \Exception('City id must be integer', 500);
        }
        return $this->citiesMapper->getRandomAvailGood($cityId);
    }
    
    /**
     * 
     * @param integer $cityId
     * @return integer
     */
    public function getRandomSuborderGood($cityId) {
        if (!is_int($cityId)) {
            throw new \Exception('City id must be integer', 500);
        }
        return $this->citiesMapper->getRandomSuborderGood($cityId);
    }

}
