<?php

namespace SoloFacility\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class CitiesServiceFactory extends AbstractServiceFactory {

    /**
     * (non-PHPdoc)
     *
     * @see \Solo\ServiceManager\AbstractServiceFactory::create()
     */
    protected function create() {
        $citiesTable = $this->getServiceLocator()->get('SoloFacility\Data\CitiesTable');
        $citiesMapper = $this->getServiceLocator()->get('SoloFacility\Data\CitiesMapper');
        $config = $this->getServiceLocator()->get('config');
        $onlyDeliveryCitiesIds = (isset($config['catalog']['onlyDeliveryCitiesIds']) ? $config['catalog']['onlyDeliveryCitiesIds'] : []);
        $subdomains = (isset($config['subdomains']['cities']) ? $config['subdomains']['cities'] : []);
        $defaultCityId = (isset($config['subdomains']['default_city_id']) ? $config['subdomains']['default_city_id'] : []);
        $service = new CitiesService($citiesTable, $citiesMapper, $onlyDeliveryCitiesIds, $subdomains, $defaultCityId);
        return $service;
    }

}

?>