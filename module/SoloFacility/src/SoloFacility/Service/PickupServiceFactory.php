<?php

namespace SoloFacility\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class PickupServiceFactory extends AbstractServiceFactory {

    /**
     * (non-PHPdoc)
     *
     * @see \Solo\ServiceManager\AbstractServiceFactory::create()
     */
    protected function create() {
        $pickupMapper = $this->getServiceLocator()->get('SoloFacility\Data\PickupMapper');
        $service = new PickupService($pickupMapper);
        return $service;
    }

}

?>