<?php

namespace SoloFacility\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class OfficesServiceFactory extends AbstractServiceFactory {

    /**
     * (non-PHPdoc)
     *
     * @see \Solo\ServiceManager\AbstractServiceFactory::create()
     */
    protected function create() {
        $officeHelper = $this->getServiceLocator()->get('SoloFacility\Service\Helper\OfficesMapper');
        $officesTable = $this->getServiceLocator()->get('SoloFacility\Data\OfficesTable');
        $officesMapper = $this->getServiceLocator()->get('SoloFacility\Data\OfficesMapper');
        $officesWithSchemes = $this->getServiceLocator()->get('config')['offices_with_schemes'];
        $service = new OfficesService($officeHelper, $officesTable, $officesMapper, $officesWithSchemes);
        return $service;
    }

}

?>