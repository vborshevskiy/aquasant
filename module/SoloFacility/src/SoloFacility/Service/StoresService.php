<?php

namespace SoloFacility\Service;

use SoloFacility\Entity\StoreCollection;
use SoloFacility\Entity\Store;
use SoloFacility\Data\StoresMapperInterface;

class StoresService {

    /**
     *
     * @var StoresMapperInterface
     */
    protected $storesMapper;

    /**
     *
     * @param storesMapperInterface $storesMapper     	
     */
    public function __construct(StoresMapperInterface $storesMapper) {
        $this->storesMapper = $storesMapper;
    }

    public function getAllStoresWithOffices() {
        return $this->storesMapper->getAllStoresWithOffices();
    }
    
    /**
     *
     * @param integer $officeId     	
     */
    public function getStoreByOfficeId($officeId) {
        return $this->storesMapper->getStoreByOfficeId($officeId);
    }
    
    public function getStoresIds() {
        $stores = $this->storesMapper->getStoresIds();
        $storesIds = \Solo\Stdlib\ArrayHelper::enumOneColumn($stores, 'StoreID');
        foreach ($storesIds as &$storeId) {
            $storeId = (int)$storeId;
        }
        unset($storeId);
        return $storesIds;
    }
    
    /**
     *
     * @param integer $cityId
     * $return Store
     */
    public function getStoreByCityId($cityId) {
        return $this->createStore($this->storesMapper->getStoreByCityId($cityId));
    }
    
    /**
     *
     * $return array
     */
    public function getLinkedOfficesIds() {
        return $this->storesMapper->getLinkedOfficesIds();
    }

    /**
     * 
     * @param ResultSet $storeData
     * @return Store
     */
    private function createStore($storeData) {
        $store = new Store();
        if (!is_null($storeData)) {
            $store->setId((int)$storeData['StoreID']);
            $store->setOfficeId((int)$storeData['StoreOfficeID']);
        }
        return $store;
    }

}

?>