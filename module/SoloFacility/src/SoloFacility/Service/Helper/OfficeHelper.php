<?php

namespace SoloFacility\Service\Helper;

use SoloCatalog\Service\Helper\DateHelper;

final class OfficeHelper {
    
//    const FIRST_WEEKDAY_NUMBER = 1;
//    const LAST_WEEKDAY_NUMBER = 7;
    
    /**
     * 
     * @param array $schedule
     * @return array
     */
    public function getTextOfficeSchedule($schedule) {
        ksort($schedule);
        $result = [];
        $weekends = array_diff([1,2,3,4,5,6,7], array_keys($schedule));
        $workTimes = [];
        foreach ($weekends as &$weekend) {
            $weekend = DateHelper::getRussianWeekdayByNumber($weekend);
        }
        foreach ($schedule as $weekDay) {
            $workTimes[DateHelper::getFormattedTime($weekDay->HourFrom, $weekDay->MinuteFrom) . ' - ' . DateHelper::getFormattedTime($weekDay->HourTo, $weekDay->MinuteTo)][] = DateHelper::getRussianWeekdayByNumber($weekDay->WeekdayNumber);
        }
        foreach ($workTimes as $workTime => $days) {
            $result[] = implode(',', $days) . ': ' . $workTime;
        }
        if (count($weekends) > 0) {
            $result[] = implode(',', $weekends) . ': выходной';
        }
        return $result;
    }
    
//    public function getTextOfficeSchedule($schedule) {
//        $maxDay = max(array_keys($schedule));
//        $minDay = min(array_keys($schedule));
//        ksort($schedule);
//        $result = [];
//        $record = '';
//        $prevDay = null;
//        $weekends = [];
//        for ($i = self::FIRST_WEEKDAY_NUMBER; $i <= self::LAST_WEEKDAY_NUMBER; $i++) {
//            if (array_key_exists($i, $schedule)) {
//                $workDay = $schedule[$i];
//            } else {
//                $workDay = null;
//                $weekends[] = DateHelper::getRussianWeekdayByNumber($i);
//            }
//            if (is_null($workDay) || $i == $minDay || is_null($prevDay) || $workDay->HourFrom != $prevDay->HourFrom || $workDay->HourTo != $prevDay->HourTo || $workDay->MinuteFrom != $prevDay->MinuteFrom || $workDay->MinuteTo != $prevDay->MinuteTo) {
//                $record .= ' - ' . DateHelper::getRussianWeekdayByNumber($i - 1);
//                if ($i != $minDay && $i != self::LAST_WEEKDAY_NUMBER) {
//                    $record .= ': ';
//                    $record .= 'с ' . DateHelper::getFormattedTime($prevDay->HourFrom, $prevDay->MinuteFrom) . ' до ' . DateHelper::getFormattedTime($prevDay->HourTo, $prevDay->MinuteTo);
//                    $result[] = $record;
//                }
//                if ($workDay->WeekdayNumber != $maxDay) {
//                    $record = DateHelper::getRussianWeekdayByNumber($i);
//                } else {
//                    $record = '';
//                }
//            }
//            if ($i == $maxDay) {
//                if (!empty($record)) {
//                    $record .= ' - ';
//                    $record .= DateHelper::getRussianWeekdayByNumber($i);
//                } else {
//                    $record = DateHelper::getRussianWeekdayByNumber($i);
//                }
//                $record .= ': ';
//                $record .= 'с ' . DateHelper::getFormattedTime($workDay->HourFrom, $workDay->MinuteFrom) . ' до ' . DateHelper::getFormattedTime($workDay->HourTo, $workDay->MinuteTo);
//                $result[] = $record;
//            }
//            $prevDay = $workDay;
//        }
//        if (count($weekends) > 0) {
//            $result[] = implode(',', $weekends) . ': выходной';
//        }
//        return $result;
//    }
}

?>