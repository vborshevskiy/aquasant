<?php

namespace SoloFacility\Service;

use SoloFacility\Entity\OfficeCollection;
use SoloFacility\Entity\Office;
use SoloFacility\Data\OfficesTableInterface;
use SoloFacility\Data\OfficesMapperInterface;
use SoloFacility\Service\Helper\OfficeHelper;

class OfficesService {
    
    /**
     *
     * @var OfficeHelper
     */
    protected $officeHelper;
    
    /**
     *
     * @var OfficesTableInterface
     */
    protected $officesTable;

    /**
     *
     * @var OfficesMapperInterface
     */
    protected $officesMapper;
    
    /**
     *
     * @var array
     */
    protected $officesWithSchemes = [];

    /**
     *
     * @param OfficesTableInterface $officesTable     	
     */
    public function __construct(OfficeHelper $officeHelper, OfficesTableInterface $officesTable, OfficesMapperInterface $officesMapper, $officesWithSchemes) {
        $this->officesTable = $officesTable;
        $this->officesMapper = $officesMapper;
        $this->officeHelper = $officeHelper;
        $this->officesWithSchemes = $officesWithSchemes;
    }

    /**
     * 
     * @param array $officeIds
     * @return OfficeCollection
     */
    public function getOfficesByIds(array $officeIds) {
        if (empty($officeIds)) {
            return null;
        }
        $officesData = $this->officesTable->getOfficesByIds($officeIds);
        $officeCollection = new OfficeCollection();
        foreach ($officesData as $officeData) {
            $officeCollection->add($this->createOffice($officeData));
        }
        return $officeCollection;
    }
    
    /**
     * 
     * @param integer $officeId
     * @return OfficeCollection
     */
    public function getOfficeById($officeId) {
        if (!is_int($officeId)) {
            throw new \InvalidArgumentException('Office id must be integer');
        }
        return $this->createOffice($this->officesMapper->getOfficeById($officeId));
    }
    
    public function helper() {
        return $this->officeHelper;
    }

    /**
     * 
     * @param ResultSet $officeData
     * @return Office
     */
    private function createOffice($officeData) {
        $office = new Office();
        $office->setId((int) $officeData['OfficeID']);
        $office->setName($officeData['OfficeName']);
        $office->setLocationId((int) $officeData['OfficeLocationId']);
        $office->setAddress($officeData['OfficeAddress']);
        if (array_key_exists('OfficePhone', $officeData)) {
            $office->setPhone($officeData['OfficePhone']);
        }
        if (array_key_exists('OfficeShortName', $officeData)) {
            $office->setShortName($officeData['OfficeShortName']);
        }
        if (array_key_exists('MetroStation', $officeData)) {
            $office->setMetroStation($officeData['MetroStation']);
        }
        if (array_key_exists('OfficePrepositionalName', $officeData)) {
            $office->setPrepositionalName($officeData['OfficePrepositionalName']);
        }
        if (array_key_exists('WorkTime', $officeData)) {
            $office->setWorkTime($officeData['WorkTime']);
        }
        $office->setLatitude($officeData['Latitude']);
        $office->setLongitude($officeData['Longitude']);
        return $office;
    }

    /**
     * 
     * @param integer $storeId
     * @return Office
     */
    public function getOfficeByStoreId($storeId) {
        return ($this->createOffice($this->officesMapper->getOfficeByStoreId($storeId)));
    }

    public function getAllPickupOffices() {
        $officeCollection = new OfficeCollection();
        foreach ($this->officesMapper->getAllPickupOffices() as $office) {
            $officeCollection->add($this->createOffice($office));
        }
        return $officeCollection;
    }
    
    public function getAllOffices() {
        $officeCollection = new OfficeCollection();
        foreach ($this->officesMapper->getAllOffices() as $office) {
            $officeCollection->add($this->createOffice($office));
        }
        return $officeCollection;
    }

    /**
     * 
     * @param integer $cityId
     * @return Office
     */
    public function getOneOfficeByCityId($cityId) {
        $office = $this->officesMapper->getOneOfficeByCityId($cityId);
        return $this->createOffice($office);
    }
    
    /**
     * 
     * @param array $officesIds
     * @return array
     */
    public function getOfficesWorkSchedule($officesIds) {
        if (!is_array($officesIds) || count($officesIds) == 0) {
            return [];
        }
        $officesSchedule = [];
        $officesScheduleData = $this->officesMapper->getOfficesWorkSchedule($officesIds);
        foreach ($officesScheduleData as $officeScheduleData) {
            $officesSchedule[(int)$officeScheduleData['CityID']][(int)$officeScheduleData['WeekdayNumber']] = (object)$officeScheduleData;
        }
        foreach ($officesSchedule as $cityId => $officeSchedule) {
            $result[$cityId] = $this->helper()->getTextOfficeSchedule($officeSchedule);
        }
        return $result;
    }
    
    public function getOfficeWorkSchedule($officeId) {
        if (!is_int($officeId)) {
            return null;
        }
        $officeSchedule = [];
        $officeScheduleData = $this->officesMapper->getOfficeWorkSchedule($officeId);
        foreach ($officeScheduleData as $officeDayScheduleData) {
            $officeSchedule[(int)$officeDayScheduleData['WeekdayNumber']] = (object)$officeDayScheduleData;
        }
        return $this->helper()->getTextOfficeSchedule($officeSchedule);
    }
    
    /**
     * 
     * @param integer $cityId
     * @return boolean
     * @throws \InvalidArgumentException
     */
    public function hasScheme($cityId) {
        if (!is_int($cityId)) {
            throw new \InvalidArgumentException('City id must be integer');
        }
        return (in_array($cityId, $this->officesWithSchemes));
    }
    
    /**
     * 
     * @param array $cityIds
     * @return array
     */
    public function getOfficesIdsWithScheme($cityIds) {
        if (!is_array($cityIds) || count($cityIds) == 0) {
            return [];
        }
        $result = [];
        foreach ($cityIds as $cityId) {
            if (in_array($cityId, $this->officesWithSchemes)) {
                $result[] = $cityId;
            }
        }
        return $result;
    }

    public function getNearestOffice($limitDistance = 50, $lat, $lon) {
        $allOffices = $this->getAllOffices();
        $nearestOffice_id = NULL;
        $minDistanse = NULL;
        foreach ($allOffices as $key => $office) {
            $distanse = $this->calculateTheDistance($lat, $lon, $office->getLatitude(), $office->getLongitude());           
            if($distanse <= $limitDistance*1000 ){
                if(!$nearestOffice_id || $distanse < $minDistanse)
                    $minDistanse = $distanse;
                    $nearestOffice_id = $office->getID();
            }
        }
        if($nearestOffice_id)
            return $allOffices[$nearestOffice_id];
        else return NULL;
    }

    private function calculateTheDistance($φA, $λA, $φB, $λB) {
        // Радиус земли
        $EARTH_RADIUS = 6372795;

        // перевести координаты в радианы
        $lat1 = $φA * M_PI / 180;
        $lat2 = $φB * M_PI / 180;
        $long1 = $λA * M_PI / 180;
        $long2 = $λB * M_PI / 180;

        // косинусы и синусы широт и разницы долгот
        $cl1 = cos($lat1);
        $cl2 = cos($lat2);
        $sl1 = sin($lat1);
        $sl2 = sin($lat2);
        $delta = $long2 - $long1;
        $cdelta = cos($delta);
        $sdelta = sin($delta);

        // вычисления длины большого круга
        $y = sqrt(pow($cl2 * $sdelta, 2) + pow($cl1 * $sl2 - $sl1 * $cl2 * $cdelta, 2));
        $x = $sl1 * $sl2 + $cl1 * $cl2 * $cdelta;

        //
        $ad = atan2($y, $x);
        $dist = $ad * $EARTH_RADIUS;

        return $dist;
    }

}

?>