<?php

namespace SoloFacility\Service;

use SoloFacility\Data\PickupMapperInterface;
use SoloCatalog\Service\Helper\DateHelper;


class PickupService {

    /**
     *
     * @var PickupMapperInterface
     */
    protected $pickupMapper;    

    /**
     * 
     * @param PickupMapperInterface $pickupMapper
     */
    public function __construct(PickupMapperInterface $pickupMapper) {
        $this->pickupMapper = $pickupMapper;
    }
    
    /**
     * 
     * @param array $goodIds
     * @param boolean $haveEnoughAvailGoods
     * @param integer $cityId
     * @return string
     */
    public function getPickupDate($goodIds, $haveEnoughAvailGoods, $cityId) {
        if ($haveEnoughAvailGoods) {
            $date = new \DateTime($this->pickupMapper->getPickupDate($goodIds,$cityId));
        } else {
            $datesInfo = $this->pickupMapper->getMaxDate($goodIds,$cityId);
            if (!is_null($datesInfo->PickupDate)) $dates[] = new \DateTime($datesInfo->PickupDate);
            //if (!is_null($datesInfo->AvailDeliveryDate)) $dates[] = new \DateTime($datesInfo->AvailDeliveryDate);
            //if (!is_null($datesInfo->SuborderDeliveryDate)) $dates[] = new \DateTime($datesInfo->SuborderDeliveryDate);
            $date = DateHelper::getMaxDate($dates);
        }
        return $date;
    }

}

?>