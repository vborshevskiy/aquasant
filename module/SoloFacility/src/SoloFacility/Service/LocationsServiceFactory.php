<?php

namespace SoloFacility\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class LocationsServiceFactory extends AbstractServiceFactory {

	/**
	 * (non-PHPdoc)
	 *
	 * @see \Solo\ServiceManager\AbstractServiceFactory::create()
	 */
	protected function create() {
		$locationsTable = $this->getServiceLocator()->get('SoloFacility\Data\LocationsTable');
		$service = new LocationsService($locationsTable);
		return $service;
	}
}

?>