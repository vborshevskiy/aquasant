<?php

namespace SoloFacility\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\Service\ProvidesWebservice;
use SoloFacility\Data\LocationsTable;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloReplication\Service\DataMapper;

class LocationsReplicator extends AbstractReplicationTask {

    use ProvidesWebservice;

    /**
     *
     * @var LocationsTable
     */
    private $locationsTable;

    /**
     *
     * @param LocationsTable $locationsTable        	
     */
    public function __construct(LocationsTable $locationsTable) {
        $this->locationsTable = $locationsTable;

        $this->addSwapTable('locations');
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SoloReplication\Service\TaskInterface::process()
     */
    public function process() {
        $this->processLocations();
    }

    /**
     *
     * @return \SoloReplication\Service\DataMapper
     */
    protected function createLocationsMapper() {
        $mapper = new DataMapper();
        $mapper->setMappings(
                [
                    'LocationID' => '%d: Id',
                    'LocationName' => 'Name',
                    'LocationParentID' => '%d: ParentId',
                    'LocationZoneID' => '%d: ZoneId',
        ]);

        return $mapper;
    }

    /**
     */
    protected function processLocations() {
        $this->log->info('Insert locations');
        $mapper = $this->createLocationsMapper();
        $dataSet = [];
        $rdr = new UltimaJsonListReader($this->callWebMethod('GetLocations'));
        if (!$rdr->isEmpty()) {
            $this->locationsTable->truncate();
            $this->notify()->fileWarning(false);
        } else {
            $this->removeSwapTable('locations');
            $this->notify()->send($this->getNotifyMessage(__CLASS__,__METHOD__,'GetLocations',[]), 'Пустой ответ по методу репликации');
            $this->notify()->fileWarning(true);
        }
        foreach ($rdr as $row) {
            $dataSet[] = $mapper->convert($row);
            if (500 == sizeof($dataSet)) {
                $this->locationsTable->insertSet($dataSet);
                $dataSet = [];
            }
        }
        if (0 < sizeof($dataSet)) {
            $this->locationsTable->insertSet($dataSet);
        }

        $this->log->info('Inserted locations = ' . sizeof($dataSet));
    }

}

?>