<?php

namespace SoloFacility\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\Service\ProvidesWebservice;
use SoloFacility\Data\StoresTable;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloReplication\Service\DataMapper;

class StoresReplicator extends AbstractReplicationTask {

    use ProvidesWebservice;

    /**
     *
     * @var StoresTable
     */
    private $storesTable;

    /**
     *
     * @param StoresTable $storesTable        	
     */
    public function __construct(StoresTable $storesTable) {
        $this->storesTable = $storesTable;

        $this->addSwapTable('stores');
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SoloReplication\Service\TaskInterface::process()
     */
    public function process() {
        $this->processStores();
    }

    /**
     *
     * @return \SoloReplication\Service\DataMapper
     */
    protected function createStoresMapper() {
        $mapper = new DataMapper();
        $mapper->setMappings([
            'StoreID' => '%d: Id',
            'StoreOfficeID' => '%d: OfficeId',
            'CityID' => '%d: LocationId',
        ]);
        
        $mapper->setMapping('IsDelivery', 'IsDelivery', function ($field) {
            return $this->helper()->parseBoolean($field);
        });
        $mapper->setMapping('IsPickup', 'IsAutoRetreive', function ($field) {
            return $this->helper()->parseBoolean($field);
        });

        return $mapper;
    }

    /**
     */
    protected function processStores() {
        $this->log->info('Insert stores');
        $mapper = $this->createStoresMapper();
        $dataSet = [];
        $params = [];

        $rdr = new UltimaJsonListReader($this->callWebMethod('GetStores', $params));
        if (!$rdr->isEmpty()) {
            $this->storesTable->truncate();
            $this->notify()->fileWarning(false);
        } else {
            $this->removeSwapTable('stores');
            $this->notify()->send($this->getNotifyMessage(__CLASS__,__METHOD__,'GetStores', $params), 'Пустой ответ по методу репликации');
            $this->notify()->fileWarning(true);
        }
        foreach ($rdr as $row) {
            $dataSet[] = $mapper->convert($row);
            if (500 == sizeof($dataSet)) {
                $this->storesTable->insertSet($dataSet);
                $dataSet = [];
            }
        }
        if (0 < sizeof($dataSet)) {
            $this->storesTable->insertSet($dataSet);
        }

        $this->log->info('Inserted stores = ' . sizeof($dataSet));
    }

}

?>