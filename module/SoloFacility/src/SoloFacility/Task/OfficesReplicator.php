<?php

namespace SoloFacility\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\Service\ProvidesWebservice;
use SoloFacility\Data\OfficesTable;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloReplication\Service\DataMapper;
use SoloFacility\Data\StoresMapper;
use SoloFacility\Data\OfficesWorkScheduleTable;

class OfficesReplicator extends AbstractReplicationTask {

    use ProvidesWebservice;

    /**
     *
     * @var OfficesTable
     */
    private $officesTable;

    /**
     *
     * @var StoresMapper
     */
    private $storesMapper;
    
    /**
     *
     * @var OfficesWorkScheduleTable 
     */
    private $officesWorkScheduleTable;

    /**
     *
     * @param StoresMapper $storesMapper
     * @param OfficesTable $officesTable
     * @param OfficesWorkScheduleTable $officesWorkScheduleTable
     */
    public function __construct(StoresMapper $storesMapper, OfficesTable $officesTable, OfficesWorkScheduleTable $officesWorkScheduleTable) {
        $this->storesMapper = $storesMapper;
        $this->officesTable = $officesTable;
        $this->officesWorkScheduleTable = $officesWorkScheduleTable;

        $this->addSwapTable('offices', 'work_schedule');
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SoloReplication\Service\TaskInterface::process()
     */
    public function process() {
        $this->processOffices();
        //$this->processOfficesWorkSchedule();
    }

    /**
     *
     * @return \SoloReplication\Service\DataMapper
     */
    protected function createOfficesMapper() {
        $mapper = new DataMapper();
        $mapper->setMappings(
                [
                    'OfficeID' => '%d: Id',
                    'OfficeName' => 'Name',
                    'OfficePrepositionalName' => 'PredName',
                    'OfficeShortName' => 'ShortName',
                    'OfficeLocationID' => '%d: LocationId',
                    'OfficeAddress' => 'Address',
                    'WorkHours' => 'WorkHours',
                    'Longitude' => '%f: Longitude',
                    'Latitude' => '%f: Latitude',
                    'OfficePhone' => 'Phone',
                    'MetroStation' => 'MetroStation',
        ]);
        $mapper->setMapping('Hide', '', function ($field, $row) {
            $activeOfficesIds = $this->storesMapper->getLinkedOfficesIds();
            return (in_array($row['Id'], $activeOfficesIds) ? 0 : 1);
        });
//        $mapper->setMapping('IsDelivery', 'IsDelivery', function ($field) {
//            return $this->helper()->parseBoolean($field);
//        });
//        $mapper->setMapping('IsPickup', 'IsAutoRetreive', function ($field) {
//            return $this->helper()->parseBoolean($field);
//        });

        return $mapper;
    }

    /**
     */
    protected function processOffices() {
        $this->log->info('Insert offices');
        $mapper = $this->createOfficesMapper();
        $dataSet = [];
        $params = [];
        $rdr = new UltimaJsonListReader($this->callWebMethod('GetOffices', $params));
        if (!$rdr->isEmpty()) {
            $this->officesTable->truncate();
            $this->notify()->fileWarning(false);
        } else {
            $this->removeSwapTable('offices');
            $this->notify()->send($this->getNotifyMessage(__CLASS__,__METHOD__,'GetOffices', $params), 'Пустой ответ по методу репликации');
            $this->notify()->fileWarning(true);
        }
        foreach ($rdr as $row) {
            $row['Longitude'] = str_replace(',', '.', $row['Longitude']);
            $row['Latitude'] = str_replace(',', '.', $row['Latitude']);
            if (!preg_match('/^\d+\.{1}\d+$/', $row['Longitude']) || !preg_match('/^\d{1,}\.{0,1}\d{0,}$/', $row['Latitude'])) {
                $this->log->warn('Invalid longitude or latitude for office ' . $row['Id']);
                $this->removeSwapTable('offices');
                //$this->notify()->send($this->getNotifyMessage(__CLASS__,__METHOD__,'GetOffices', $params), 'Неверные координаты офиса');
            }
            $dataSet[] = $mapper->convert($row);
            if (500 == sizeof($dataSet)) {
                $this->officesTable->insertSet($dataSet, true);
                $dataSet = [];
            }
        }
        if (0 < sizeof($dataSet)) {
            $this->officesTable->insertSet($dataSet, true);
        }

        $this->log->info('Inserted offices = ' . sizeof($dataSet));
    }
    
    /**
     *
     * @return \SoloReplication\Service\DataMapper
     */
    protected function createOfficesWorkScheduleMapper() {
        $context = $this;
        $mapper = new DataMapper();
        $mapper->setMappings([
            'CityId' => '%d: LocationId',
            'WeekdayNumber' => '%d: DayOfWeek',
            'HourTo' => '%d: HourTo',
            'HourFrom' => '%d: HourFrom',
            'MinuteTo' => '%d: MinuteTo',
            'MinuteFrom' => '%d: MinuteFrom',
        ]);
        $mapper->setMapping('Works', 'Works', function ($field, $row) use($context) {
            return $context->helper()->parseBoolean($field);
        });
        $mapper->setMapping('IsOffice', 'IsAuto', function ($field, $row) use($context) {
            return $context->helper()->parseBoolean($field);
        });
        $mapper->setMapping('IsDelivery', 'IsDelivery', function ($field, $row) use($context) {
            return $context->helper()->parseBoolean($field);
        });
        return $mapper;
    }
    
    /**
     */
    protected function processOfficesWorkSchedule() {
        $this->log->info('Insert offices work schedule');
        $mapper = $this->createOfficesWorkScheduleMapper();
        $dataSet = [];
        $count = 0;
        $response = $this->callWebMethod('GetWorkDays');
        print_r($response);
        $rdr = new UltimaJsonListReader($response);
        if (!$rdr->isEmpty()) {
            $this->officesWorkScheduleTable->truncate();
        }
        foreach ($rdr as $row) {
            $dataSet[] = $mapper->convert($row);
            if (500 == sizeof($dataSet)) {
                $this->officesWorkScheduleTable->insertSet($dataSet);
                $count += sizeof($dataSet);
                $dataSet = [];
            }
        }
        if (0 < sizeof($dataSet)) {
            $this->officesWorkScheduleTable->insertSet($dataSet);
            $count += sizeof($dataSet);
        }

        $this->log->info('Inserted offices work schedule = ' . $count);
    }
    

}

?>