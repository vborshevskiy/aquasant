<?php

namespace SoloFacility\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\Service\ProvidesWebservice;
use SoloFacility\Data\CitiesTable;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloReplication\Service\DataMapper;

class CitiesReplicator extends AbstractReplicationTask {

    use ProvidesWebservice;

    /**
     *
     * @var CitiesTable
     */
    private $citiesTable;

    /**
     *
     * @param CitiesTable $citiesTable        	
     */
    public function __construct(CitiesTable $citiesTable) {
        $this->citiesTable = $citiesTable;

        $this->addSwapTable('cities');
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SoloReplication\Service\TaskInterface::process()
     */
    public function process() {
        $this->processCities();
    }

    /**
     *
     * @return \SoloReplication\Service\DataMapper
     */
    protected function createCitiesMapper() {
        $mapper = new DataMapper();
        $mapper->setMappings(
                [
                    'CityID' => '%d: CityID',
                    'CityName' => 'CityName',
                    'CityPhone' => 'Phone',
                    'CityZoneID' => '%d: ZoneId',
        ]);

        return $mapper;
    }

    /**
     */
    protected function processCities() {
        $this->log->info('Insert cities');
        $mapper = $this->createCitiesMapper();
        $dataSet = [];
        
        $params = [];
        $params['SiteID'] = $this->getSiteId();
        $rdr = new UltimaJsonListReader($this->callWebMethod('GetCityForHeader', $params));
        
        if (!$rdr->isEmpty()) {
            $this->citiesTable->truncate();
            $this->notify()->fileWarning(false);
        } else {
            $this->removeSwapTable('cities');
            $this->notify()->send($this->getNotifyMessage(__CLASS__,__METHOD__,'GetCityForHeader',$params), 'Пустой ответ по методу репликации');
            $this->notify()->fileWarning(true);
        }
        foreach ($rdr as $row) {
            $dataSet[] = $mapper->convert($row);
            if (500 == sizeof($dataSet)) {                
                $this->citiesTable->insertSet($dataSet);
                $dataSet = [];
            }
        }
        if (0 < sizeof($dataSet)) {
            $this->citiesTable->insertSet($dataSet);
        }

        $this->log->info('Inserted cities = ' . sizeof($dataSet));
    }

}

?>