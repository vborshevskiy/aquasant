<?php

namespace SoloFacility\Data;

use Zend\Db\ResultSet\ResultSet;
use Solo\Db\TableGateway\TableInterface;

interface CitiesTableInterface extends TableInterface {

    /**
     * 
     * @param array $cityIds
     * @return ResultSet
     */
    public function getCitiesByIds(array $cityIds);
}

?>