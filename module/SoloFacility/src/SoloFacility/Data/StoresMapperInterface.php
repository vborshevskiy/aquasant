<?php

namespace SoloFacility\Data;

interface StoresMapperInterface {
    
    /*
     * @param integer $goodId
     * @return array
     */
    public function getAllStoresWithOffices();       
    
    /*
     * @return mixed
     */
    public function getStoresIds();
    
    /*
     * @param integer $cityId
     * @return mixed
     */
    public function getStoreByCityId($cityId);
    
    /*
     * @return array
     */
    public function getLinkedOfficesIds();
}
?>