<?php

namespace SoloFacility\Data;

use Solo\Db\TableGateway\AbstractTable;

class CitiesTable extends AbstractTable implements CitiesTableInterface {

    /**
     * (non-PHPdoc)
     * @see \SoloFacility\Data\CitiesTableInterface::getCitiesByIds()
     */
    public function getCitiesByIds(array $cityIds) {
        $select = $this->createSelect();
        $select->where(['CityID' => $cityIds]);       
        return $this->tableGateway->selectWith($select);
    }
}

?>