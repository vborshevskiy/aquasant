<?php

namespace SoloFacility\Data;

use Solo\Db\Mapper\AbstractMapper;

class PickupMapper extends AbstractMapper implements PickupMapperInterface {

    /**
     * 
     * @param array $goodIds
     * @param integer $cityId
     * @return string
     */
    public function getPickupDate($goodIds, $cityId) {
        if (!is_array($goodIds) || count($goodIds) == 0) {
            return null;
        }
        $sql = 'SELECT
                            MAX(grad.PickupDate) AS PickupDate
                    FROM
                            #good_reserve_avail_date:active# grad
                    WHERE
                            grad.GoodID IN ('.implode(',',$goodIds).')
                            AND grad.CityID = '.$cityId;
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            $result = $rows->current();
            return $result->PickupDate;
        }
        return null;
    }
    
    /**
     * 
     * @param array $goodIds
     * @param integer $cityId
     * @return string
     */
    public function getMaxDate($goodIds,$cityId) {
        if (!is_array($goodIds) || count($goodIds) == 0) {
            return null;
        }
        $sql = 'SELECT
                            MAX(grad.PickupDate) AS PickupDate,
                            MAX(grad.AvailDeliveryDate) AS AvailDeliveryDate,
                            MAX(grad.SuborderDeliveryDate) AS SuborderDeliveryDate
                    FROM
                            #good_reserve_avail_date:active# grad
                    WHERE
                            grad.GoodID IN ('.implode(',',$goodIds).')
                            AND grad.CityID = '.$cityId;
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            $result = $rows->current();
            return $result;
        }
        return null;
    }

}

?>