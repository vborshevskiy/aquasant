<?php

namespace SoloFacility\Data;

interface PickupMapperInterface {
    
    /**
     * 
     * @param array $goodIds
     * @param integer $cityId
     * @return string
     */
    public function getPickupDate($goodIds, $cityId);
    
    /**
     * 
     * @param array $goodIds
     * @param integer $cityId
     * @return string
     */
    public function getMaxDate($goodIds,$cityId);
}
?>