<?php

namespace SoloFacility\Data;

use Solo\Db\TableGateway\AbstractTable;

class LocationsTable extends AbstractTable implements LocationsTableInterface {

    /**
     * (non-PHPdoc)
     * @see \SoloFacility\Data\LocationsTableInterface::getLocationsByIds()
     */
    public function getLocationsByIds(array $locationIds) {
        $select = $this->createSelect();
        $select->where(['LocationID' => $locationIds]);       
        return $this->tableGateway->selectWith($select);
    }
}

?>