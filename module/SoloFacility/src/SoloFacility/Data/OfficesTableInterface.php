<?php

namespace SoloFacility\Data;

use Zend\Db\ResultSet\ResultSet;
use Solo\Db\TableGateway\TableInterface;

interface OfficesTableInterface extends TableInterface {

    /**
     * 
     * @param array $officeIds
     * @return ResultSet
     */
    public function getOfficesByIds(array $officeIds);
}

?>