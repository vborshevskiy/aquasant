<?php

namespace SoloFacility\Data;

use Zend\Db\ResultSet\ResultSet;
use Solo\Db\TableGateway\TableInterface;

interface StoresTableInterface extends TableInterface {

    /**
     * 
     * @param array $storeIds
     * @return ResultSet
     */
    public function getStoresByIds(array $storeIds);
}

?>