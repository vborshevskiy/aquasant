<?php

namespace SoloFacility\Data;

use Solo\Db\TableGateway\AbstractTable;

class OfficesTable extends AbstractTable implements OfficesTableInterface {

    /**
     * (non-PHPdoc)
     * @see \SoloFacility\Data\OfficesTableInterface::getOfficesByIds()
     */
    public function getOfficesByIds(array $officeIds) {
        $select = $this->createSelect();
        $select->where(['OfficeID' => $officeIds]);
        return $this->tableGateway->selectWith($select);
    }

}

?>