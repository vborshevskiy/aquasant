<?php

namespace SoloFacility\Data;

use Solo\Db\TableGateway\AbstractTable;

class StoresTable extends AbstractTable implements StoresTableInterface {

    /**
     * (non-PHPdoc)
     * @see \SoloFacility\Data\StoresTableInterface::getStoresByIds()
     */
    public function getStoresByIds(array $officeIds) {
        $select = $this->createSelect();
        $select->where(['StoreID' => $officeIds]);
        return $this->tableGateway->selectWith($select);
    }

}

?>