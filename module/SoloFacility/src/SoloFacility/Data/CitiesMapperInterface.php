<?php

namespace SoloFacility\Data;

interface CitiesMapperInterface {
    
    /*
     * @param integer $goodId
     * @return array
     */
    public function getAllCities(); 
    
    /**
     * 
     * @param ResultSet $cityId
     * @return mixed
     */
    public function getCityById($cityId);

}
?>