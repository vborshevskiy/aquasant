<?php

namespace SoloFacility\Data;

use SoloCache\Service\ProvidesCache;
use Solo\Db\Mapper\AbstractMapper;

class StoresMapper extends AbstractMapper implements StoresMapperInterface {

    use ProvidesCache;

    /*
     * @param integer $goodId
     * @return array
     */

    public function getAllStoresWithOffices() {
        $sql = 'SELECT
                        *
                FROM
                        #stores# st
                INNER JOIN
                        #offices# of
                        ON of.OfficeID = st.StoreOfficeID                
              ';
        return $this->query($sql)->toArray();
    }

    /*
     * @param integer $officeId
     * @return mixed
     */
    public function getStoreByOfficeId($officeId) {
        $sql = "SELECT
                            *
                    FROM
                            #stores# st
                    INNER JOIN
                            #offices# of
                            ON of.OfficeID = st.StoreOfficeID            
                    WHERE
                            of.OfficeID = " . intval($officeId) . "
                    LIMIT 1";
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            return $rows->current();
        }
        return null;
    }
    
    /*
     * @return mixed
     */
    public function getStoresIds() {
        $sql = 'SELECT
                        StoreID
                FROM #stores:active#';
        return $this->query($sql)->toArray();
    }
    
    /*
     * @param integer $cityId
     * @return mixed
     */
    public function getStoreByCityId($cityId) {
        $sql = 'SELECT
                        st.*
                FROM
                        #stores:active# st
                INNER JOIN
                        #offices# of
                        ON of.OfficeID = st.StoreOfficeID
                WHERE
                        of.OfficeLocationId = '.$cityId.'
                LIMIT 1';
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            return $rows->current();
        }
        return null;
    }
    
    /*
     * @return array
     */
    public function getLinkedOfficesIds() {
        $sql = 'SELECT
                        StoreOfficeID
                FROM #stores:active#';
        return \Solo\Stdlib\ArrayHelper::enumOneColumn($this->query($sql)->toArray(), 'StoreOfficeID');
    }

}

?>