<?php

namespace SoloFacility\Data;

interface OfficesMapperInterface {
    
    /*
     * @param integer $storeId
     * @return array
     */
    public function getOfficeByStoreId($storeId);
    
    /*
     * @return array
     */
    public function getAllPickupOffices();
    
    /*
     * @return array
     */
    public function getAllOffices();
    
    /**
     * 
     * @param integer $cityId
     * @return Office
     */
    public function getOneOfficeByCityId($cityId);
    
    /**
     * 
     * @param array $officesIds
     * @return array
     */
    public function getOfficesWorkSchedule($officesIds);
    
    /**
     * 
     * @param integer $officeId
     */
    public function getOfficeById($officeId);
}
?>