<?php

namespace SoloFacility\Data;

use SoloCache\Service\ProvidesCache;
use Solo\Db\Mapper\AbstractMapper;

class OfficesMapper extends AbstractMapper implements OfficesMapperInterface {

    use ProvidesCache;

    /*
     * @return array
     */
    public function getAllPickupOffices() {
        $sql = 'SELECT
                        of.*
                FROM
                        #offices:active# of
                INNER JOIN
                        #cities:active# cit
                        ON cit.CityID = of.OfficeLocationId
                WHERE
                        of.IsPickup = 1
              ';
        return $this->query($sql)->toArray();
    }
    
    /*
     * @return array
     */
    public function getAllOffices() {
        $sql = 'SELECT
                        *
                FROM              
                        #offices#                    
              ';
        return $this->query($sql)->toArray();
    }
    
    /**
     * 
     * @param integer $storeId
     * @return mixed
     */
    public function getOfficeByStoreId($storeId) {
        $sql = 'SELECT
                            of.*
                    FROM
                            #offices# of
                    INNER JOIN
                            #stores# st
                            ON of.OfficeID = st.StoreOfficeID
                    WHERE
                            st.StoreID = '.$storeId.'
                    LIMIT 1
              ';
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            return $rows->current();
        }
        return null;
    }
    
    /*
     * @param integer $cityId
     * @return mixed
     */
    public function getOneOfficeByCityId($cityId) {
        $sql = 'SELECT
                            of.*
                    FROM
                            #offices# of
                    WHERE
                            of.OfficeLocationId = '.$cityId.'
                    LIMIT 1
              ';
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            return $rows->current();
        }
        return null;
    }
    
    /**
     * 
     * @param array $officesIds
     * @return array
     */
    public function getOfficesWorkSchedule($officesIds) {
        $sql = 'SELECT
                            ws.CityID,
                            ws.WeekdayNumber,
                            ws.HourFrom,
                            ws.HourTo,
                            ws.MinuteFrom,
                            ws.MinuteTo
                    FROM
                            #work_schedule:active# ws
                    INNER JOIN
                            #offices:active# of
                            ON of.OfficeLocationId = ws.CityID
                    WHERE
                            ws.Works > 0
                            AND ws.IsOffice > 0
                            AND of.OfficeID IN ('.implode(',',$officesIds).')
                    GROUP BY
                            ws.CityID, ws.WeekdayNumber
              ';
        return $this->query($sql)->toArray();
    }
    
    /**
     * 
     * @param integer $officeId
     * @return array
     */
    public function getOfficeWorkSchedule($officeId) {
        $sql = 'SELECT
                            ws.CityID,
                            ws.WeekdayNumber,
                            ws.HourFrom,
                            ws.HourTo,
                            ws.MinuteFrom,
                            ws.MinuteTo
                    FROM
                            #work_schedule:active# ws
                    INNER JOIN
                            #offices:active# of
                            ON of.OfficeLocationId = ws.CityID
                    WHERE
                            ws.Works > 0
                            AND ws.IsOffice > 0
                            AND of.OfficeID = '.$officeId.'
                    GROUP BY
                            ws.CityID, ws.WeekdayNumber
              ';
        return $this->query($sql)->toArray();
    }
    
    /**
     * 
     * @param integer $officeId
     */
    public function getOfficeById($officeId) {
        $sql = 'SELECT
                        of.*
                FROM
                        #offices:active# of
                WHERE
                        of.OfficeID = '.$officeId.'
                ';
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            return $rows->current();
        }
        return null;
    }

}

?>