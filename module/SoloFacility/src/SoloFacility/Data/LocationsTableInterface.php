<?php

namespace SoloFacility\Data;

use Zend\Db\ResultSet\ResultSet;
use Solo\Db\TableGateway\TableInterface;

interface LocationsTableInterface extends TableInterface {

    /**
     * 
     * @param array $locationIds
     * @return ResultSet
     */
    public function getLocationsByIds(array $locationIds);
}

?>