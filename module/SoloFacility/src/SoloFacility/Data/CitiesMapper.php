<?php

namespace SoloFacility\Data;

use SoloCache\Service\ProvidesCache;
use Solo\Db\Mapper\AbstractMapper;

class CitiesMapper extends AbstractMapper implements CitiesMapperInterface {

    use ProvidesCache;

    /*
     * @param integer $goodId
     * @return array
     */

    public function getAllCities() {
        $sql = 'SELECT
                        *
                FROM
                        #cities#';
        return $this->query($sql)->toArray();
    }
    
    /**
     * 
     * @param integer $cityId
     * @return mixed
     */
    public function getCityById($cityId) {
        $sql = 'SELECT
                            c.*
                    FROM
                            #cities:active# c
                    WHERE
                            c.CityID = '.$cityId.'
                    LIMIT 1
              ';
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            return $rows->current();
        }
        return null;
    }
    
    public function getCitiesIds() {
        $sql = 'SELECT
                        c.CityID
                FROM
                        #cities:active# c
                ';
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            return \Solo\Stdlib\ArrayHelper::enumOneColumn($rows, 'CityID');
        }
        return null;
    }

}

?>