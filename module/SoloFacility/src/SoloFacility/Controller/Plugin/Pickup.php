<?php

namespace SoloFacility\Controller\Plugin;

use Solo\Mvc\Controller\Plugin\AbstractPlugin;

class Pickup extends AbstractPlugin {

    public function __call($name, $arguments) {
        $pickup = $this->getController()->getServiceLocator()->get('pickup');
        if (!method_exists($pickup, $name)) {
            throw new \BadMethodCallException('Invalid pickup method: ' . $name);
        }
        return call_user_func_array(array(
            $pickup,
            $name
                ), $arguments);
    }

}

?>