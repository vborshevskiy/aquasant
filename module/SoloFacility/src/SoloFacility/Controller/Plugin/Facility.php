<?php

namespace SoloFacility\Controller\Plugin;

use Solo\Mvc\Controller\Plugin\AbstractPlugin;

class Facility extends AbstractPlugin {

    /**
     *
     * @return OfficesService
     */
    public function offices() {
        return $this->getServiceLocator()->get('facility_offices');
    }

    /**
     *
     * @return OfficesService
     */
    public function stores() {
        return $this->getServiceLocator()->get('facility_stores');
    }

    /**
     *
     * @return LocationsService
     */
    public function locations() {
        return $this->getServiceLocator()->get('facility_locations');
    }

    /**
     *
     * @return CitiesService
     */
    public function cities() {
        return $this->getServiceLocator()->get('facility_cities');
    }

}

?>