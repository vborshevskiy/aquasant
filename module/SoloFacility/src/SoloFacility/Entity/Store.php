<?php

namespace SoloFacility\Entity;

class Store {

    /**
     *
     * @var integer
     */
    private $id = 0;
   
    /**
     *
     * @var integer
     */
    private $officeId = null;  
  

    /**
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @param integer $id        	
     * @throws \InvalidArgumentException
     * @return \SoloFacility\Entity\Store
     */
    public function setId($id) {
        if (!is_integer($id)) {
            throw new \InvalidArgumentException('Id must be integer');
        }
        $this->id = $id;
        return $this;
    } 

    /**
     *
     * @return integer
     */
    public function getOfficeId() {
        return $this->officeId;
    }

    /**
     *
     * @param integer $officeId        	
     * @throws \InvalidArgumentException
     * @return \SoloFacility\Entity\Store
     */
    public function setOfficeId($officeId) {
        if (!is_integer($officeId)) {
            throw new \InvalidArgumentException('Office Id must be integer');
        }
        $this->officeId = $officeId;
        return $this;
    }

}

?>