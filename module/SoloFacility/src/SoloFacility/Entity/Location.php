<?php

namespace SoloFacility\Entity;

class Location {

    /**
     *
     * @var integer
     */
    private $id = 0;

    /**
     *
     * @var string
     */
    private $name = '';

    /**
     *
     * @var integer
     */
    private $parentId = null;  
    
    /**
     *
     * @var integer
     */
    private $zoneId = null;  
  

    /**
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @param integer $id        	
     * @throws \InvalidArgumentException
     * @return \SoloFacility\Entity\Location
     */
    public function setId($id) {
        if (!is_integer($id)) {
            throw new \InvalidArgumentException('Id must be integer');
        }
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     *
     * @param string $name        	
     * @return \SoloFacility\Entity\Location
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getParentId() {
        return $this->parentId;
    }

    /**
     *
     * @param integer $locationId        	
     * @throws \InvalidArgumentException
     * @return \SoloFacility\Entity\Location
     */
    public function setParentId($parentId) {
        if (!is_integer($parentId)) {
            throw new \InvalidArgumentException('parent Id must be integer');
        }
        $this->parentId = $parentId;
        return $this;
    }

   /**
     *
     * @return integer
     */
    public function getZoneId() {
        return $this->zoneId;
    }

    /**
     *
     * @param integer $locationId        	
     * @throws \InvalidArgumentException
     * @return \SoloFacility\Entity\Location
     */
    public function setZoneId($zoneId) {
        if (!is_integer($zoneId)) {
            throw new \InvalidArgumentException('zone Id must be integer');
        }
        $this->zoneId = $zoneId;
        return $this;
    }

}

?>