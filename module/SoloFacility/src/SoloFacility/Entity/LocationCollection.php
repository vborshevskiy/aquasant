<?php

namespace SoloFacility\Entity;

use Solo\Collection\Collection;

class LocationCollection extends Collection {

    /**
     *
     * @param Location $location        	
     * @return Location
     */
    public function add($location, $key = null) {
        if (!($location instanceof Location)) {
            throw new \RuntimeException('Location must be an instace of Location');
        }
        $key = (is_null($key) ? $location->getId() : $key);
        parent::add($location, $key);
        return $location;
    }

}

?>