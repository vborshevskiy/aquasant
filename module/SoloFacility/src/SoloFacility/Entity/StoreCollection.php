<?php

namespace SoloFacility\Entity;

use Solo\Collection\Collection;

class StoreCollection extends Collection {

    /**
     *
     * @param Store $store        	
     * @return Store
     */
    public function add($store, $key = null) {
        if (!($store instanceof Store)) {
            throw new \RuntimeException('Store must be an instace of Store');
        }
        $key = (is_null($key) ? $store->getId() : $key);
        parent::add($store, $key);
        return $store;
    }

}

?>