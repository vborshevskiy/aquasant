<?php

namespace SoloFacility\Entity;

use Solo\Collection\Collection;

class OfficeCollection extends Collection {

    /**
     *
     * @param Office $office        	
     * @return Office
     */
    public function add($office, $key = null) {
        if (!($office instanceof Office)) {
            throw new \RuntimeException('Office must be an instace of Office');
        }
        $key = (is_null($key) ? $office->getId() : $key);
        parent::add($office, $key);
        return $office;
    }

}

?>