<?php

namespace SoloFacility\Entity;

class City {

    /**
     *
     * @var integer
     */
    private $id = 0;

    /**
     *
     * @var string
     */
    private $name = '';

    /**
     *
     * @var integer
     */
    private $zoneId = null;

    /**
     *
     * @var string
     */
    private $phone = '';

    /**
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @param integer $id        	
     * @throws \InvalidArgumentException
     * @return \SoloFacility\Entity\City
     */
    public function setId($id) {
        if (!is_integer($id)) {
            throw new \InvalidArgumentException('Id must be integer');
        }
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     *
     * @param string $name        	
     * @return \SoloFacility\Entity\City
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getZoneId() {
        return $this->zoneId;
    }

    /**
     *
     * @param integer $cityId        	
     * @throws \InvalidArgumentException
     * @return \SoloFacility\Entity\City
     */
    public function setZoneId($zoneId) {
        if (!is_integer($zoneId)) {
            throw new \InvalidArgumentException('zone Id must be integer');
        }
        $this->zoneId = $zoneId;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     *
     * @param string $phone        	
     * @return \SoloFacility\Entity\City
     */
    public function setPhone($phone) {
        $this->phone = $phone;
        return $this;
    }

}

?>