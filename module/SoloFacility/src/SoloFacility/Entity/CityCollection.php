<?php

namespace SoloFacility\Entity;

use Solo\Collection\Collection;

class CityCollection extends Collection {

    /**
     *
     * @param City $city        	
     * @return City
     */
    public function add($city, $key = null) {
        if (!($city instanceof City)) {
            throw new \RuntimeException('City must be an instace of City');
        }
        $key = (is_null($key) ? $city->getId() : $key);
        parent::add($city, $key);
        return $city;
    }

}

?>