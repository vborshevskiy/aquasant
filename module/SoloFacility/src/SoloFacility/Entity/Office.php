<?php

namespace SoloFacility\Entity;

class Office {

    /**
     *
     * @var integer
     */
    private $id = 0;

    /**
     *
     * @var string
     */
    private $name = '';
    
    /**
     *
     * @var string
     */
    private $shortName = '';
    
    /**
     *
     * @var string
     */
    private $prepositionalName = '';

    /**
     *
     * @var integer
     */
    private $locationId = null;  

    /**
     *
     * @var string
     */
    private $address = '';
    
    /**
     *
     * @var string
     */
    private $phone = '';
    
    /**
     *
     * @var string
     */
    private $workTime = '';
    
    /**
     *
     * @var float
     */
    private $longitude = 0;
    
    /**
     *
     * @var float
     */
    private $latitude = 0;
    
    /**
     *
     * @var string
     */
    private $metroStation = '';

    /**
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @param integer $id        	
     * @throws \InvalidArgumentException
     * @return \SoloFacility\Entity\Office
     */
    public function setId($id) {
        if (!is_integer($id)) {
            throw new \InvalidArgumentException('Id must be integer');
        }
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     *
     * @param string $name        	
     * @return \SoloFacility\Entity\Office
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    
    /**
     *
     * @return string
     */
    public function getShortName() {
        return $this->shortName;
    }

    /**
     *
     * @param string $shortName        	
     * @return \SoloFacility\Entity\Office
     */
    public function setShortName($shortName) {
        $this->shortName = $shortName;
        return $this;
    }
    
    /**
     *
     * @return string
     */
    public function getPrepositionalName() {
        return $this->prepositionalName;
    }

    /**
     *
     * @param string $prepositionalName        	
     * @return \SoloFacility\Entity\Office
     */
    public function setPrepositionalName($prepositionalName) {
        $this->prepositionalName = $prepositionalName;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getLocationId() {
        return $this->locationId;
    }

    /**
     *
     * @param integer $locationId        	
     * @throws \InvalidArgumentException
     * @return \SoloFacility\Entity\Office
     */
    public function setLocationId($locationId) {
        if (!is_integer($locationId)) {
            throw new \InvalidArgumentException('Location Id must be integer');
        }
        $this->locationId = $locationId;
        return $this;
    }

   /**
     *
     * @return string
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     *
     * @param string $address        	
     * @return \SoloFacility\Entity\Office
     */
    public function setAddress($address) {
        $this->address = $address;
        return $this;
    }
    
    /**
     *
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     *
     * @param string $phone        	
     * @return \SoloFacility\Entity\Office
     */
    public function setPhone($phone) {
        $this->phone = $phone;
        return $this;
    }
    
    /**
     *
     * @return boolean
     */
    public function hasPhone() {
        return (!is_null($this->phone) && !empty($this->phone));
    }
    
    /**
     *
     * @return string
     */
    public function getWorkTime() {
        return $this->workTime;
    }

    /**
     *
     * @param string $workTime        	
     * @return \SoloFacility\Entity\Office
     */
    public function setWorkTime($workTime) {
        $this->workTime = $workTime;
        return $this;
    }
    
    /**
     *
     * @return float
     */
    public function getLatitude() {
        return $this->latitude;
    }

    /**
     *
     * @param float $latitude        	
     * @return \SoloFacility\Entity\Office
     */
    public function setLatitude($latitude) {
        $this->latitude = (float)$latitude;
        return $this;
    }
    
    /**
     *
     * @return float
     */
    public function getLongitude() {
        return $this->longitude;
    }

    /**
     *
     * @param float $longitude        	
     * @return \SoloFacility\Entity\Office
     */
    public function setLongitude($longitude) {
        $this->longitude = (float)$longitude;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getMetroStation() {
        return $this->metroStation;
    }
    
    /**
     * 
     * @param string $metroStation
     * @return \SoloFacility\Entity\Office
     */
    public function setMetroStation($metroStation) {
        $this->metroStation = $metroStation;
        return $this;
    }

}
