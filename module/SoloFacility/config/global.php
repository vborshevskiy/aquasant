<?php

return [
    'subdomains' => [
        'cities' => [
            'rnd' => 31,
            'nsk' => 30,
        ],
        'default_city_id' => 2,
    ],
    'offices_with_schemes' => [
        30,
    ]
];