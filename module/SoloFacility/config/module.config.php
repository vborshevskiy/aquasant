<?php

use Solo\Db\TableGateway\TableGateway;
use Solo\Db\QueryGateway\QueryGateway;
use SoloFacility\Service\Helper\OfficeHelper;
use SoloFacility\Data\OfficesTable;
use SoloFacility\Data\StoresTable;
use SoloFacility\Data\LocationsTable;
use SoloFacility\Data\CitiesTable;
use SoloFacility\Data\OfficesWorkScheduleTable;
use SoloFacility\Data\StoresMapper;
use SoloFacility\Data\OfficesMapper;
use SoloFacility\Data\PickupMapper;

return [
    'controller_plugins' => [
        'invokables' => [
            'facility' => 'SoloFacility\Controller\Plugin\Facility',
            'pickup' => 'SoloFacility\Controller\Plugin\Pickup',
        ]
    ],
    'service_manager' => [
        'aliases' => [
            'facility_offices' => 'SoloFacility\Service\OfficesService',
            'facility_stores' => 'SoloFacility\Service\StoresService',
            'facility_locations' => 'SoloFacility\Service\LocationsService',
            'facility_cities' => 'SoloFacility\Service\CitiesService',
            'pickup' => 'SoloFacility\Service\PickupService',
        ],
        'factories' => [
            'SoloFacility\Data\OfficesTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('offices'), 'OfficeID');
                $table = new OfficesTable($gateway);
                return $table;
            },
            'SoloFacility\Data\StoresTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('stores'), 'StoreID');
                $table = new StoresTable($gateway);
                return $table;
            },
            'SoloFacility\Data\LocationsTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('locations'), 'LocationID');
                $table = new LocationsTable($gateway);
                return $table;
            },
            'SoloFacility\Data\CitiesTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('cities'), 'CityID');
                $table = new CitiesTable($gateway);
                return $table;
            },
            'SoloFacility\Data\OfficesWorkScheduleTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('work_schedule'), ['CityID','WeekdayNumber','IsDelivery','IsOffice']);
                $table = new OfficesWorkScheduleTable($gateway);
                return $table;
            },
            'SoloCatalog\Data\StoresMapper' => function ($sm) {
                return new StoresMapper(new QueryGateway());
            },
            'SoloCatalog\Data\CitiesMapper' => function ($sm) {
                return new CitiesMapper(new QueryGateway());
            },
            'SoloCatalog\Data\OfficesMapper' => function ($sm) {
                return new OfficesMapper(new QueryGateway());
            },
            'SoloCatalog\Data\PickupMapper' => function ($sm) {
                return new PickupMapper(new QueryGateway());
            },

            'SoloFacility\Service\Helper\OfficesMapper' => function ($sm) {
                return new OfficeHelper();
            },

            'SoloFacility\Service\CitiesService' => 'SoloFacility\Service\CitiesServiceFactory',
            'SoloFacility\Service\OfficesService' => 'SoloFacility\Service\OfficesServiceFactory',
            'SoloFacility\Service\StoresService' => 'SoloFacility\Service\StoresServiceFactory',
            'SoloFacility\Service\LocationsService' => 'SoloFacility\Service\LocationsServiceFactory',
            'SoloFacility\Service\PickupService' => 'SoloFacility\Service\PickupServiceFactory',
        ]
    ]
];
