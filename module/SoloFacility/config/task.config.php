<?php

use SoloFacility\Task\OfficesReplicator;
use SoloFacility\Task\StoresReplicator;
use SoloFacility\Task\LocationsReplicator;
use SoloFacility\Task\CitiesReplicator;

return [
    'tasks' => [
        'offices' => 'SoloFacility\Task\OfficesReplicator',
        'stores' => 'SoloFacility\Task\StoresReplicator',
        'locations' => 'SoloFacility\Task\LocationsReplicator',
        'cities' => 'SoloFacility\Task\CitiesReplicator',
    ],
    'service_manager' => [
        'factories' => [
            'SoloFacility\Task\OfficesReplicator' => function ($sm) {
                $officeTable = $sm->get('SoloFacility\Data\OfficesTable');
                $officeTable->getTableGateway()->setTable($sm->get('triggers')->passive('offices'));
                $officesWorkScheduleTable = $sm->get('SoloFacility\Data\OfficesWorkScheduleTable');
                $officesWorkScheduleTable->getTableGateway()->setTable($sm->get('triggers')->passive('work_schedule'));
                $storesMapper = $sm->get('SoloCatalog\Data\StoresMapper');
                $task = new OfficesReplicator($storesMapper, $officeTable, $officesWorkScheduleTable);
                return $task;
            },
            'SoloFacility\Task\StoresReplicator' => function ($sm) {
                $storeTable = $sm->get('SoloFacility\Data\StoresTable');
                $storeTable->getTableGateway()->setTable($sm->get('triggers')->passive('stores'));
                $task = new StoresReplicator($storeTable);
                return $task;
            },
            'SoloFacility\Task\LocationsReplicator' => function ($sm) {
                $locationTable = $sm->get('SoloFacility\Data\LocationsTable');
                $locationTable->getTableGateway()->setTable($sm->get('triggers')->passive('locations'));
                $task = new LocationsReplicator($locationTable);
                return $task;
            },
            'SoloFacility\Task\CitiesReplicator' => function ($sm) {
                $citiesTable = $sm->get('SoloFacility\Data\CitiesTable');
                $citiesTable->getTableGateway()->setTable($sm->get('triggers')->passive('cities'));
                $task = new CitiesReplicator($citiesTable);
                return $task;
            },
        ],
    ],
];
