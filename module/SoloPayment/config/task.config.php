<?php

use SoloPayment\Task\YandexKassaPercentReplicator;

return [
    'tasks' => [
        'yandex_kassa_percent' => 'SoloPayment\Task\YandexKassaPercentReplicator'
    ],
    'service_manager' => [
        'factories' => [
            'SoloPayment\Task\BanksReplicator' => function ($sm) {
                $constantsService = $sm->get('SoloSettings\Service\ConstantsService');
                $task = new YandexKassaPercentReplicator($constantsService);
                return $task;
            },
        ],
    ],
];
