<?php

return [
    'controller_plugins' => [
        'invokables' => [
            'payment' => 'SoloPayment\Controller\Plugin\Payment'
        ]
    ],
    'service_manager' => [
        'factories' => [
            'SoloPayment\Service\YandexKassaService' => function($sm) {
                $config = $sm->get('Config');
                $service = new SoloPayment\Service\YandexKassaService($config['yandex_kassa']);
                return $service;
            },
        ]
    ]
];
