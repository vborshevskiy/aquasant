<?php

namespace SoloPayment\Service;

class YandexKassaService {
    
    /**
     *
     * @var array
     */
    private $paymentTerms = [];

    /**
     * 
     * @param array $config
     */
    public function __construct($config) {
        if (!isset($config['payment_terms']) || !is_array($config['payment_terms']) || count($config['payment_terms']) == 0) {
            throw new \RuntimeException('Can\'t find yandex kassa payment terms');
        }
        $this->paymentTerms = $config['payment_terms'];
    }
    
    /**
     * 
     * @return array
     */
    public function getPaymentTerms() {
        return $this->paymentTerms;
    }
    
}