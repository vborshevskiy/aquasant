<?php

namespace SoloPayment\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\Service\ProvidesWebservice;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloSettings\Service\ConstantsService;

class YandexKassaPercentReplicator extends AbstractReplicationTask {

    use ProvidesWebservice;

    /**
     *
     * @var ConstantsService
     */
    private $constantsService;

    /**
     *
     * @param ConstantsService $constantsService        	
     */
    public function __construct(ConstantsService $constantsService) {
        $this->constantsService = $constantsService;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SoloReplication\Service\TaskInterface::process()
     */
    public function process() {
        $this->processGetYandexPercent();
    }

    /**
     * 
     * Updating yandex kassa percent value
     */
    protected function processGetYandexPercent() {
        $rdr = new UltimaJsonListReader($this->callWebMethod('GetYandexPercent'));
        $percent = (float)$rdr->getValue('Percent');
        $this->constantsService->set('YandexKassaPercent', $percent);
        $this->constantsService->saveChanges();
        $this->log->info('Yandex kassa percent = ' . $percent);
    }

}
