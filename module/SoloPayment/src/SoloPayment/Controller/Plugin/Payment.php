<?php

namespace SoloPayment\Controller\Plugin;

use Solo\Mvc\Controller\Plugin\AbstractPlugin;

class Payment extends AbstractPlugin {

    /**
     * 
     * @return \SoloPayment\Service\YandexKassaService
     */
    public function yandexKassa() {
        $service = $this->getController()->getServiceLocator()->get('\\SoloPayment\\Service\\YandexKassaService');
        return $service;
    }

}
