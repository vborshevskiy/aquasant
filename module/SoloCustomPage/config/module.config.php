<?php
use Solo\Db\TableGateway\TableGateway;
use SoloCustomPage\Data\PagesTable;
return [
	'controller_plugins' => [
		'invokables' => [
			'pages' => 'SoloCustomPage\Controller\Plugin\Pages' 
		] 
	],
	'service_manager' => [
		'factories' => [
			'SoloCustomPage\Data\PagesTable' => function() {
				$gateway = new TableGateway('pages', 'TextID');
				return new PagesTable($gateway);
			},
			'SoloCustomPage\Service\PagesService' => 'SoloCustomPage\Service\PagesServiceFactory' 
		] 
	] 
];
