<?php

namespace SoloCustomPage\Data;

interface PagesTableInterface {

	/**
	 *
	 * @param string $uid        	
	 * @return mixed
	 */
	public function findByUid($uid);

}

?>