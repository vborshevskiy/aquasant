<?php

namespace SoloCustomPage\Data;

use SoloCache\Service\ProvidesCache;
use Solo\Db\TableGateway\AbstractTable;

class PagesTable extends AbstractTable implements PagesTableInterface {
	
	use ProvidesCache;

	/**
	 *
	 * @param string $uid        	
	 * @return mixed
	 */
	public function findByUid($uid) {
		$select = $this->createSelect();
		$select->where(['TextUID' => $uid]);
		$select->limit(1);
		$result = $this->tableGateway->selectWith($select);
		if (0 < $result->count()) {
			return $result->current();
		}
		return null;
	}

}

?>