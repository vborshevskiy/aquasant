<?php

namespace SoloCustomPage\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class Pages extends AbstractPlugin {

	/**
	 *
	 * @param string $uid        	
	 * @return mixed
	 */
	public function __invoke($uid = null) {
		$serv = $this->getController()->getServiceLocator()->get('\\SoloCustomPage\\Service\\PagesService');
		if (null !== $uid) {
			return $serv->findByUid($uid);
		} else {
			return $serv;
		}
	}

}

?>