<?php

namespace SoloCustomPage\Service;

use SoloCustomPage\Data\PagesTableInterface;

class PagesService {

	/**
	 *
	 * @var PagesTableInterface
	 */
	private $pagesTable;

	/**
	 * 
	 * @param PagesTableInterface $pagesTable
	 */
	public function __construct(PagesTableInterface $pagesTable) {
		$this->pagesTable = $pagesTable;
	}

	/**
	 *
	 * @param string $uid        	
	 * @return \ArrayObject
	 */
	public function findByUid($uid) {
		return $this->pagesTable->findByUid($uid);
	}

}

?>