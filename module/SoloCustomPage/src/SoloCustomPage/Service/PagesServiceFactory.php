<?php

namespace SoloCustomPage\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class PagesServiceFactory extends AbstractServiceFactory {

	protected function create() {
		return new PagesService($this->getServiceLocator()->get('SoloCustomPage\\Data\\PagesTable'));
	}

}

?>