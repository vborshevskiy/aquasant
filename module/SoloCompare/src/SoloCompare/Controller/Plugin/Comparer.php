<?php

namespace SoloCompare\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class Comparer extends AbstractPlugin {

	public function __call($name, $arguments) {
		$basket = $this->getController()->getServiceLocator()->get('\\SoloCompare\\Service\\CompareService');
		if (!method_exists($basket, $name)) {
			throw new \BadMethodCallException('Invalid comparer method: ' . $name);
		}
		return call_user_func_array(array(
			$basket,
			$name 
		), $arguments);
	}

}

?>