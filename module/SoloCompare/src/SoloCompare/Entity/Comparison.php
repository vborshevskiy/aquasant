<?php

namespace SoloCompare\Entity;

class Comparison {

	/**
	 *
	 * @var int
	 */
	private $id = 0;

	/**
	 *
	 * @var string
	 */
	private $name = '';

	/**
	 *
	 * @var string
	 */
	private $uid = '';

	/**
	 *
	 * @var array
	 */
	private $goodIds = [];

	/**
	 *
	 * @var array
	 */
	private $parentIds = [];

	/**
	 *
	 * @param
	 *        	array
	 */
	public function __construct($category) {
		$this->id = intval($category['SoftCategoryID']);
		$this->name = $category['CategoryName'];
		$this->uid = $category['CategoryUID'];
	}

	/**
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 *
	 * @return string
	 */
	public function getUid() {
		return $this->uid;
	}

	/**
	 *
	 * @return array
	 */
	public function getGoodIds() {
		return $this->goodIds;
	}

	/**
	 *
	 * @param
	 *        	int
	 */
	public function addParentId($parentId) {
		$this->parentIds[$parentId] = $parentId;
	}

	/**
	 *
	 * @param
	 *        	int
	 * @return boolean
	 */
	public function hasParentId($parentId) {
		return isset($this->parentIds[$parentId]);
	}

	/**
	 *
	 * @param
	 *        	int
	 * @return boolean
	 */
	public function addGoodId($goodId) {
		if (!in_array($goodId, $this->goodIds)) {
			$this->goodIds[$goodId] = $goodId;
			return true;
		}
		return false;
	}

	/**
	 *
	 * @param
	 *        	int
	 * @return boolean
	 */
	public function removeGoodId($goodId) {
		if (in_array($goodId, $this->goodIds)) {
			unset($this->goodIds[$goodId]);
			return true;
		}
		return false;
	}

	/**
	 *
	 * @return int
	 */
	public function getGoodIdsLength() {
		return count($this->goodIds);
	}

}

?>