<?php

namespace SoloCompare\Service;

use SoloCompare\Entity\Comparison;
use SoloCatalog\Controller\Plugin\Catalog;
use Zend\Session\Container;

class CompareService {

    /**
     *
     * @var array
     */
    private $ComparisonList = [];

    /**
     *
     * @var Zend\Session\Container
     */
    private $session;

    public function __construct() {
        $this->session = new Container();
        if ($this->session->offsetExists('comparison_list')) {
            $this->ComparisonList = (array) unserialize($this->session['comparison_list']);
        }
    }

    /**
     *
     * @param integer $good        	
     * @param unknown $category        	
     * @param Catalog $catalog        	
     * @return Ambigous <multitype:, number, multitype:NULL unknown >
     */
    public function addGood($good, $category, Catalog $catalog) {
        if (!isset($this->ComparisonList[intval($category['SoftCategoryID'])])) {
            $categories = $catalog->categories()->enumCategoriesByHardCategoryId(intval($good['CategoryID']));

            $this->ComparisonList[intval($category['SoftCategoryID'])] = new Comparison($category);
            foreach ($categories as $cat) {
                $current = $cat;
                $this->ComparisonList[intval($category['SoftCategoryID'])]->addParentId(intval($current['SoftCategoryID']));
                do {
                    $current = $catalog->categories()->getCategoryById(intval($current['ParentID']));
                    if ((!$current) || ($this->ComparisonList[intval($category['SoftCategoryID'])]->hasParentId(intval($current['SoftCategoryID'])))) {
                        break;
                    }
                    $this->ComparisonList[intval($category['SoftCategoryID'])]->addParentId(intval($current['SoftCategoryID']));
                } while ($current['ParentID'] != 0);
            }
        }
        $this->ComparisonList[intval($category['SoftCategoryID'])]->addGoodId($good['GoodID']);
        $this->storeComparisonList();
        
        return $this->getComparisonInfo($category->SoftCategoryID);
    }

    /**
     *
     * @param
     *        	int
     * @param
     *        	array
     * @return array
     */
    public function removeGood($goodId, $category) {
        if (isset($this->ComparisonList[intval($category['SoftCategoryID'])])) {
            $this->ComparisonList[intval($category['SoftCategoryID'])]->removeGoodId($goodId);
            if ($this->ComparisonList[intval($category['SoftCategoryID'])]->getGoodIdsLength() == 0) {
                $this->removeComparisonCategory($category);
            }
            $this->storeComparisonList();
        }
        return $this->getComparisonInfo($category->SoftCategoryID);
    }

    /**
     *
     * @param integer $goodId
     * @param integer $softCategoryId
     * @return boolean
     */
    public function hasGood($goodId, $softCategoryId) {
        if (isset($this->ComparisonList[intval($softCategoryId)])) {
            return (in_array($goodId, $this->ComparisonList[intval($softCategoryId)]->getGoodIds()));
        }
        return false;
    }

    /**
     *
     * @param
     *        	array
     * @return array
     */
    public function getComparisonCategory($category) {
        return $this->ComparisonList[intval($category['SoftCategoryID'])];
    }

    /**
     * 
     * @param array $category
     * @param Catalog $catalog
     * @param array|null $goodIds
     * @return boolean
     */
    public function getComparisonCategoryInfo($category, Catalog $catalog) {
        if (!isset($this->ComparisonList[intval($category['SoftCategoryID'])])) {
            return false;
        }
        $comparison = $this->ComparisonList[intval($category['SoftCategoryID'])];
        $info = array(
            'name' => $comparison->getName(),
            'uid' => $comparison->getUid(),
            'goods' => $catalog->goods()->enumGoodsByIds($comparison->getGoodIds()),
            'goodsDescriptions' => $catalog->goods()->enumGoodDescriptionsByGoodIds($comparison->getGoodIds()),
            'count' => $comparison->getGoodIdsLength(),
            'goodIds' => $comparison->getGoodIds(),
        );
        return $info;
    }
    
    /**
     * 
     * @param array $category
     * @param Catalog $catalog
     * @param array $goodIds
     * @return boolean
     */
    public function getComparisonCategoryInfoByGoodIds($category, Catalog $catalog, $goodIds) {
        $info = array(
            'name' => $category->CategoryName,
            'uid' => $category->CategoryUID,
            'goods' => $catalog->goods()->enumGoodsByIds($goodIds),
            'goodsDescriptions' => $catalog->goods()->enumGoodDescriptionsByGoodIds($goodIds),
            'count' => count($goodIds),
            'goodIds' => $goodIds,
        );
        return $info;
    }

    /**
     *
     * @param
     *        	array
     * @return array
     */
    public function removeComparisonCategory($category) {
        unset($this->ComparisonList[intval($category['SoftCategoryID'])]);
        $this->storeComparisonList();
        return $this->getComparisonInfo();
    }

    /**
     *
     * @return array
     */
    public function getComparisonList() {
        return $this->ComparisonList;
    }

    /**
     *
     * @return array
     */
    public function getComparisonInfo($categoryId = null) {
        if (is_null($categoryId)) {
            $info['groups'] = [];
            $totalCount = 0;
            foreach ($this->ComparisonList as $key => $comparison) {
                $count = $comparison->getGoodIdsLength();
                $totalCount += $count;
                $info['groups'][] = array(
                    'title' => $comparison->getName(),
                    'id' => $key,
                    'link' => '/compare/' . $comparison->getUid() . '/?ids=' . implode(',', $comparison->getGoodIds()),
                    'count' => $count,
                );
                $info['totalCount'] = $totalCount;
            }

        } elseif (array_key_exists($categoryId, $this->ComparisonList)) {
            $comparison = $this->ComparisonList[$categoryId];
            $count = $comparison->getGoodIdsLength();
            $info = [
                'name' => $comparison->getName(),
                'uid' => $comparison->getUid(),
                'goods' => $comparison->getGoodIds(),
                'count' => $count,
                'totalCount' => $count,
                'newCompareUrl' => '/compare/' . $comparison->getUid() . '/?ids=' . implode(',', $comparison->getGoodIds()),
            ];
        } else {
            $info = [
                'name' => null,
                'uid' => null,
                'goods' => [],
                'count' => 0,
                'totalCount' => 0,
                'newCompareUrl' => '/compare/',
            ];
        }
        return $info;
    }

    private function storeComparisonList() {
        if ($this->ComparisonList) {
            $this->session['comparison_list'] = serialize($this->ComparisonList);
        } else {
            unset($this->session['comparison_list']);
        }
    }

    /**
     *
     * @return array
     */
    public function removeComparisonList() {
        $this->ComparisonList = [];
        unset($this->session['comparison_list']);
        return $this->getComparisonInfo();
    }

    /**
     *
     * @param
     *        	string
     * @param
     *        	SoloCatalog\Controller\Plugin\Catalog
     * @return boolean
     */
    public function isCompareButtonEnabled($page, Catalog $catalog) {
        $category = $catalog->categories()->getCategoryByUid($page);
        $checker = false;
        $result['enabled'] = true;
        foreach ($this->ComparisonList as $comparison) {
            if ($comparison->hasParentId($category['SoftCategoryID'])) {
                if ($checker) {
                    $goodIds = array_keys($comparison->getGoodIds());
                    $good = $catalog->goods()->getGoodByGoodId($goodIds[0]);
                    if ($checker != $good['PropertyTemplateID']) {
                        $result['enabled'] = false;
                        break;
                    } else {
                        $result['href'] = '/compare/' . $category['CategoryUID'] . '/?ptid=' . $checker;
                    }
                } else {
                    $result['href'] = '/compare/' . $comparison->getUid() . '/';
                    $goodIds = array_keys($comparison->getGoodIds());
                    $good = $catalog->goods()->getGoodByGoodId($goodIds[0]);                                        //var_dump($good); die;
                    $checker = $good['PropertyTemplateID'];
                }
            }
        }
        if ($checker === false) {
            $result['enabled'] = true;
        }
        return $result;
    }

    /**
     *
     * @param
     *        	int
     * @param
     *        	array
     * @param
     *        	SoloCatalog\Controller\Plugin\Catalog
     * @return array
     */
    public function findByPropertyTemplateID($propertyTemplateID, $category, Catalog $catalog) {
        $result = array(
            'name' => $category['CategoryName'],
            'uid' => $category
        );
        $allGoods = [];
        foreach ($this->ComparisonList as $comparison) {
            $goodIds = array_keys($comparison->getGoodIds());
            $good = $catalog->goods()->getGoodByGoodId($goodIds[0]);
            if ($propertyTemplateID == $good['PropertyTemplateID']) {
                $allGoods = array_merge($allGoods, $goodIds);
            }
        }
        if (!$allGoods) {
            return null;
        }
        $result['goods'] = $catalog->goods()->enumGoodsByIds($allGoods);
        $result['goodsDescriptions'] = $catalog->goods()->enumGoodDescriptionsByGoodIds($allGoods);
        $result['goodsPrices'] = $catalog->prices()->enumPricesByGoodIds($allGoods);
        $result['count'] = count($allGoods);
        return $result;
    }
    
    public function getComparedGoodIdsByCategoryId($category) {
        if (array_key_exists($category->SoftCategoryID, $this->ComparisonList)) {
            return $this->ComparisonList[$category->SoftCategoryID]->getGoodIds();
        }
        return [];
    }

    public function getGoodIds() {
        $result = [];
        foreach ($this->ComparisonList as $item) {
            $result = array_merge($result, $item->getGoodIds());
        }

        return $result;
    }

}

?>
