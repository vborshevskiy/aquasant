<?php
use SoloCompare\Service\CompareService;

return [
	'controller_plugins' => [
		'invokables' => [
			'comparer' => 'SoloCompare\Controller\Plugin\Comparer' 
		] 
	],
	'service_manager' => [
		'factories' => [
			'SoloCompare\Service\CompareService' => function ($sm) {
				return new CompareService();
			} 
		] 
	] 
];