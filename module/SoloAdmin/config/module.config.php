<?php
use SoloAdmin\Service\UserService;
use SoloAdmin\Data\UserGateway;

return array(
	'router' => array(
		'routes' => array(
			'admin_home' => array(
				'type' => 'Segment',
				'priority' => 1000,
				'options' => array(
					'route' => '/admin[/][index][/]',
					'defaults' => array(
						'controller' => 'SoloAdmin\Controller\Index',
						'action' => 'index' 
					) 
				) 
			),
			'admin_login' => array(
				'type' => 'Segment',
				'priority' => 1000,
				'options' => array(
					'route' => '/admin/login[/]',
					'defaults' => array(
						'controller' => 'SoloAdmin\Controller\Login',
						'action' => 'index' 
					) 
				) 
			),
			'admin_logout' => array(
				'type' => 'Segment',
				'priority' => 1000,
				'options' => array(
					'route' => '/admin/logout[/]',
					'defaults' => array(
						'controller' => 'SoloAdmin\Controller\Login',
						'action' => 'logout' 
					) 
				) 
			),
			'admin_comments' => array(
				'type' => 'Literal',
				'priority' => 1000,
				'options' => array(
					'route' => '/admin/comments/',
					'defaults' => array(
						'controller' => 'SoloAdmin\Controller\Comments',
						'action' => 'index' 
					) 
				),
				'may_terminate' => true,
				'child_routes' => array(
					'delete' => array(
						'type' => 'Segment',
						'options' => array(
							'route' => 'delete/[:id][/]',
							'constraints' => array(
								'id' => '[0-9]*' 
							),
							'defaults' => array(
								'action' => 'delete' 
							) 
						) 
					),
				) 
			) 
		) 
	),
	'controllers' => array(
		'invokables' => array(
			'SoloAdmin\Controller\Index' => 'SoloAdmin\Controller\IndexController',
			'SoloAdmin\Controller\Login' => 'SoloAdmin\Controller\LoginController',
			'SoloAdmin\Controller\Comments' => 'SoloAdmin\Controller\CommentsController' 
		) 
	),
	'controller_plugins' => array(
		'invokables' => array(
			'backend' => 'SoloAdmin\Controller\Plugin\Backend' 
		) 
	),
	'service_manager' => array(
		'aliases' => array(
			'admin_user' => 'SoloAdmin\Service\User' 
		),
		'factories' => array(
			'SoloAdmin\Service\User' => function ($sm) {
				return new UserService(new UserGateway());
			} 
		) 
	) 
);
