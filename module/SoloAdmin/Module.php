<?php

namespace SoloAdmin;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Solo\ModuleManager\MultiConfigModule;

class Module extends MultiConfigModule implements AutoloaderProviderInterface {

	public function __construct() {
		parent::__construct(__DIR__);
	}

	public function getAutoloaderConfig() {
		return [
			'Zend\Loader\ClassMapAutoloader' => [
				__DIR__ . DIRECTORY_SEPARATOR . 'autoload_classmap.php' 
			],
			'Zend\Loader\StandardAutoloader' => [
				'namespaces' => [
					__NAMESPACE__ => __DIR__ . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . str_replace('\\', '/', __NAMESPACE__) 
				] 
			] 
		];
	}

}