<?php

namespace SoloAdmin\Service;

use Solo\Db\QueryGateway\QueryGateway;
use SoloAdmin\Data\UserGateway;
use Zend\Session\Container;

class UserService {

    private $userInfo = null;

    /**
     *
     * @var UserGateway
     */
    private $userGateway;

    public function __construct(QueryGateway $userGateway) {
        $this->userGateway = $userGateway;
        $this->restore();
    }

    public function authenticate($login, $password) {
        $user = $this->userGateway->getByLoginAndPassword($login, md5($password));
        if (null !== $user) {
            $this->userInfo = $user;
            $this->backup();
            return true;
        }
        return false;
    }

    public function logout() {
        $this->userInfo = null;
        $this->backup();
    }

    public function isLogged() {
        return ((null !== $this->userInfo) && (0 < $this->userInfo->UserID));
    }

    private function backup() {
        $sc = new Container('admin');
        if (null !== $this->userInfo) {
            $sc->user = $this->userInfo;
        } else {
            if (isset($sc->user)) {
                unset($sc->user);
            }
        }
    }

    private function restore() {
        $sc = new Container('admin');
        $this->userInfo = (isset($sc->user) ? $sc->user : null);
    }

}

?>