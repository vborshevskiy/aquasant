<?php

namespace SoloAdmin\Controller;

class CommentsController extends BaseController {

    public function indexAction() {
        $comments = [];
        $goodId = null;
        if ($this->getRequest()->isPost()) {
            $goodId = (int)$this->params()->fromPost('good-id');
        } elseif ($this->getRequest()->isGet()) {
            $goodId = (int)$this->params()->fromQuery('good-id');
        }
        if (!is_null($goodId) && $goodId > 0) {
            $comments = $this->item()->comment()->getComments($goodId);
        }
        return $this->outputVars([
            'comments' => $comments,
            'goodId' => $goodId,
        ]);
    }

    public function deleteAction() {
        $goodId = (int)$this->params()->fromQuery('good-id');
        $commentId = (int)$this->params()->fromQuery('comment-id');
        if ($commentId > 0 && $goodId > 0) {
            $this->item()->comment()->deleteComment($commentId, $goodId);
        }
        return $this->redirect()->toUrl('/admin/comments/?good-id='.$goodId);
    }

}

?>