<?php

namespace SoloAdmin\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use SoloAdmin\Service\UserService;

class Backend extends AbstractPlugin {

    /**
     *
     * @return UserService
     */
    public function user() {
        return $this->getController()->getServiceLocator()->get('admin_user');
    }

}

?>