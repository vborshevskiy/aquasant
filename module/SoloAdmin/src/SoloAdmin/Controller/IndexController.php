<?php

namespace SoloAdmin\Controller;

class IndexController extends BaseController {

	public function indexAction() {
		$this->getLayout()->setTitle('Администрирование');
		
		return $this->outputVars([]);
	}

}
