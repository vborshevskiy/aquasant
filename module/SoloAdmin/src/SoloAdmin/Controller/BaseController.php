<?php

namespace SoloAdmin\Controller;

use Solo\Mvc\Controller\ActionController;
use Zend\Mvc\MvcEvent;

class BaseController extends ActionController {

    private $menu = [];

    /**
     * (non-PHPdoc)
     * @see \Solo\Mvc\Controller\ActionController::getLayout()
     */
    protected function getLayout() {
        if (!$this->hasLayout()) {
            $this->setLayout($this->getServiceLocator()->get('Solo\\Mvc\\Controller\\Layout'));
        }
        return parent::getLayout();
    }

    private function initCommonVars() {
        $this->getLayout()->addCssFile('admin.css');
    }

    public function onDispatch(MvcEvent $e) {
        if ('SoloAdmin\Controller\LoginController' !== get_class($this)) {
            $this->checkAuthentication();
        }
        return parent::onDispatch($e);
    }

    private function checkAuthentication() {
        if (!$this->backend()->user()->isLogged()) {
            $headers = $this->getRequest()->getHeaders();
            $referer = '/admin/';
            if ($headers->has('Referer')) {
                $referer = $headers->get('Referer')->getFieldValue();
            }
            header('Location: /admin/login/?ret=' . $referer);
            exit();
        }
    }

    private function initMenu() {
        $this->addMenuItem('index', 'Главная');
        $this->addMenuItem('comments', 'Комментарии');
    }

    private function addMenuItem($uid, $title, $icon = null) {
        $this->menu[$uid] = array(
            'uid' => $uid,
            'title' => $title,
            'url' => '/admin/' . $uid . '/',
            'selected' => false,
            'icon' => $icon
        );
    }

    protected function selectMenu($uid) {
        if (array_key_exists($uid, $this->menu)) {
            $this->menu[$uid]['selected'] = true;
        }
    }

    /**
     * override ActionController methods
     */
    protected function outputVars($vars) {
        $layout = $this->getLayout();
//        $layout->setStylesheetFolder('css/admin');
//        $layout->setJavascriptFolder('js/admin');
        $layout->clearCssFiles();
        $layout->clearJsFiles();
        $this->initCommonVars();
        $this->initMenu();

        $vars['menu'] = $this->menu;
        $vars['module_appearance_path'] = '../solo-admin';
        $vars['this'] = $this;
        return parent::outputVars($vars);
    }

}

?>