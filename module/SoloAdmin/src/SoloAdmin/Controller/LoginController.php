<?php

namespace SoloAdmin\Controller;

use Solo\Cookie\Cookie;

class LoginController extends BaseController {

    public function indexAction() {
        if ($this->backend()->user()->isLogged()) {
            return $this->redirect()->toUrl('/admin/');
        }

        if ($this->getRequest()->isPost()) {
            return $this->processLogin();
        }

        $this->getLayout()->setTitle('SA: Авторизация');
        $this->getLayout()->disableAppearance();

        $login = (Cookie::exists('admin_user_login') ? Cookie::get('admin_user_login') : null);

        return $this->outputVars([
            'login' => $login,
            'err' => $this->params()->fromQuery('err') == 'notfound',
        ]);
    }

    private function processLogin() {
        $login = $this->params()->fromPost('login');
        $password = $this->params()->fromPost('password');
        if ($this->backend()->user()->authenticate($login, $password)) {
            Cookie::set('admin_user_login', $login, null, 0, '/');
            $referer = $this->params()->fromPost('ret');
            if ((null === $referer) || empty($referer)) {
                $url = '/admin/';
            } else {
                $url = $referer;
            }
            return $this->redirect()->toUrl($url);
        } else {
            $referer = $this->params()->fromPost('ret');
            $url = '/admin/login/?err=notfound';
            if (null !== $referer) {
                $url .= '&ret=' . $referer;
            }
            return $this->redirect()->toUrl($url);
        }
    }

    public function logoutAction() {
        $this->backend()->user()->logout();
        return $this->redirect()->toUrl('/admin/login/');
    }

}
