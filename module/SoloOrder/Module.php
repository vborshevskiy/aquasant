<?php

namespace SoloOrder;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Solo\ModuleManager\MultiConfigModule;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;

class Module extends MultiConfigModule implements AutoloaderProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        $application = $e->getApplication();
        $eventManager = $application->getEventManager();

        $eventManager->attach(MvcEvent::EVENT_DISPATCH, [$this, 'cartNotEmptyListener'], 100);
    }

    public function __construct() {
        parent::__construct(__DIR__);
    }

    public function getAutoloaderConfig() {
        return [
            'Zend\Loader\ClassMapAutoloader' => [
                __DIR__ . DIRECTORY_SEPARATOR . 'autoload_classmap.php'
            ],
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . str_replace('\\', '/', __NAMESPACE__)
                ]
            ]
        ];
    }

    public function cartNotEmptyListener(MvcEvent $e) {
        $application = $e->getApplication();
        $serviceManager = $application->getServiceManager();

        $routeMatch = $e->getRouteMatch();

        if (!$routeMatch instanceof RouteMatch || $routeMatch->getMatchedRouteName() != 'order') {
            return;
        }

        $basket = $serviceManager->get('basket_service');

        if (!$basket->getTotalQuantity()) {
            $response = $e->getResponse();
            $response->getHeaders()->addHeaderLine('Location', '/');
            $response->setStatusCode(302);

            $e->stopPropagation(true);
            $e->setResponse($response);
            $e->setResult($response);
        }
    }

}