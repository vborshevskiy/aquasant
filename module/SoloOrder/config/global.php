<?php

return [
    'basket' => [
        'grouping' => true,
        'group_by' => function(\SoloOrder\Entity\Basket\Good $basketGood1, \SoloOrder\Entity\Basket\Good $basketGood2) {
            if ($basketGood1->hasDelivery() && $basketGood2->hasDelivery()) {
                $result = true;
                $delivery1 = $basketGood1->getDelivery();
                $delivery2 = $basketGood2->getDelivery();
                $result &= ($delivery1->getAddressId() == $delivery2->getAddressId());
                $result &= ($delivery1->getDeliveryTime() == $delivery2->getDeliveryTime());
                $result &= ($delivery1->getDeliveryDate() == $delivery2->getDeliveryDate());
                return $result;
            } elseif (!$basketGood1->hasDelivery() && !$basketGood2->hasDelivery()) {
                return ($basketGood1->getWarehouseId() == $basketGood2->getWarehouseId());
            }
            return false;
        },
        'compare' => function(\SoloOrder\Entity\Basket\Good $basketGood, $warehouseId = null, \SoloOrder\Entity\Basket\Delivery $delivery = null) {
            if ($warehouseId !== null) {
                return ($basketGood->getWarehouseId() == $warehouseId);
            } elseif ($delivery !== null) {
                /**
                 * @todo valid comparation
                 */
                return ($basketGood->getDelivery() === $delivery);
            }
            return false;
        },
    ],
];
