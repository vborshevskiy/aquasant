<?php

return array(
    'controller_plugins' => array(
        'invokables' => array(
            'order' => 'SoloOrder\Controller\Plugin\Order',
            'agents' => 'SoloOrder\Controller\Plugin\Agents',
            'basket' => 'SoloOrder\Controller\Plugin\Basket',
        )
    ),
    'service_manager' => array(
        'aliases' => array(
            /* order */
            'order_service' => '\SoloOrder\Service\Ultima\OrderService',
            'order_instance' => '\SoloOrder\Entity\Order\Ultima\Order',
            'order_storage_instance' => '\SoloOrder\Entity\Order\Ultima\Storage',
            'order_good_instance' => '\SoloOrder\Entity\Order\Ultima\Good',
            'order_queue_instance' => '\SoloOrder\Entity\Order\Ultima\OrderQueue',
            'order_goods_collection' => '\SoloOrder\Entity\Order\GoodsCollection',
            'orders_collection' => '\SoloOrder\Entity\Order\OrdersCollection',
            'order_delivery' => '\SoloOrder\Entity\Order\Ultima\Delivery',
            'order_post_delivery' => '\SoloOrder\Entity\Order\Ultima\PostDelivery',
            'orders_decorator' => '\SoloOrder\Service\Behavior\Order\OrdersCollectionDecoratorGetter',
            'order_settings' => '\SoloOrder\Entity\Order\Ultima\Settings',
            'order_definition' => '\SoloOrder\Entity\Order\OrderDefinition',
            /* basket */
            'basket_service' => '\SoloOrder\Service\BasketService',
            'basket_group_instance' => '\SoloOrder\Entity\Basket\Group',
            'basket_good_instance' => '\SoloOrder\Entity\Basket\Good',
			'basket_component_good_instance' => '\SoloOrder\Entity\Basket\ComponentGood',
            'delivery_instance' => '\SoloOrder\Entity\Basket\Delivery',
            'basket_order_good_collection' => '\SoloOrder\Entity\Basket\GoodsCollection',
            'basket_storage' => '\SoloOrder\Entity\Basket\Storage',
            'good_processing_behavior' => '\SoloOrder\Service\Behavior\Good\WarehouseDependent',
            'group_handler' => '\SoloOrder\Service\Behavior\Good\GroupHandler',
            'basket_group_service' => '\SoloOrder\App\Service\BasketGroupService',
            'order_group_service' => '\SoloOrder\App\Service\OrderGroupService',
            'orders_collection_getter' => '\SoloOrder\App\Service\Behavior\Order\OrdersCollectionGetter',
            'orders_initializer' => '\SoloOrder\App\Service\Behavior\Order\OrdersInitByGroupingStrategy',
            'order_decorator' => '\SoloOrder\App\Service\Behavior\Order\OrderDecoratorGetter',
        ),
        'factories' => array(
            '\SoloOrder\Entity\Order\Ultima\Settings' => function($sm) {
                return new \SoloOrder\Entity\Order\Ultima\Settings;
            },
            '\SoloOrder\Entity\Order\Ultima\Delivery' => function($sm) {
                return new \SoloOrder\Entity\Order\Ultima\Delivery;
            },
            '\SoloOrder\Entity\Order\Ultima\PostDelivery' => function($sm) {
                return new \SoloOrder\Entity\Order\Ultima\PostDelivery;
            },
            '\SoloOrder\Service\Behavior\Order\OrdersCollectionDecoratorGetter' => function($sm) {
                return new \SoloOrder\Service\Behavior\Order\OrdersCollectionDecoratorGetter;
            },
            '\SoloOrder\Entity\Order\OrdersCollection' => function($sm) {
                return new \SoloOrder\Entity\Order\OrdersCollection;
            },
            '\SoloOrder\Service\Ultima\OrderService' => function ($sm) {
                $orderService = new \SoloOrder\Service\Ultima\OrderService();
                $orderService->setGoodsAggregate($sm->get('\SoloOrder\Data\GoodsAggregate'));
                $orderService->setLocalStorage($sm->get('\SoloOrder\Entity\Order\Ultima\Storage'));

                $orderService->events()->attach('getOrders', $sm->get('order_group_service')->getOrdersCollectionGetterCallback());

                //$orderService->events()->attach('getOrders.post', $sm->get('orders_decorator')->ordersDecoratorCallback(), 1);
                //$orderService->events()->attach('getOrders.post', $sm->get('AllOrdersMode')->getOrdersForShowingCallback(), 2);

                //$orderService->events()->attach('getOrder.post', $sm->get('order_group_service')->getOrderDecorator()->orderDecoratorCallback());

                //$orderService->events()->attach('isOrdersInit', $sm->get('order_group_service')->getInitCheckerCallback());
                //$orderService->events()->attach('initOrders', $sm->get('order_group_service')->getInitOrdersCallback());
                //$orderService->events()->attach('syncWithBasket', $sm->get('order_group_service')->getSyncWithBasketCallback());

                return $orderService;
            },
            '\SoloOrder\Data\GoodsAggregate' => function($sm) {
                return new \SoloOrder\Data\GoodsAggregate();
            },
            '\SoloOrder\Entity\Order\GoodsCollection' => function($sm) {
                return new \SoloOrder\Entity\Order\GoodsCollection;
            },
            '\SoloOrder\Entity\Order\Ultima\Storage' => function ($sm) {
                $storage = new \SoloOrder\Entity\Order\Ultima\Storage('orders');
                return $storage;
            },
            '\SoloOrder\Entity\Order\Ultima\Order' => function($sm) {
                $bankPercent = (float)$sm->get('SoloSettings\Service\ConstantsService')->get('BankPercent');
                $order = new \SoloOrder\Entity\Order\Ultima\Order($bankPercent);
                $order->setGoodsCollection($sm->get('order_goods_collection'));
                return $order;
            },
            '\SoloOrder\Entity\Order\Ultima\Good' => function($sm) {
                return new \SoloOrder\Entity\Order\Ultima\Good;
            },
            '\SoloOrder\Entity\Order\Ultima\OrderQueue' => function($sm) {
                return new \SoloOrder\Entity\Order\Ultima\OrderQueue;
            },
            'AllOrdersMode' => function($sm) {
                return new \SoloOrder\Service\Behavior\Order\AllOrdersOnPage;
            },
            'QueueOrdersMode' => function($sm) {
                return new \SoloOrder\Service\Behavior\Order\OrdersQueueItemOnPage();
            },
            '\SoloOrder\Service\Behavior\Order\SelectOrderByWarehouse' => function($sm) {
                return new \SoloOrder\Service\Behavior\Order\SelectOrderByWarehouse;
            },
            '\SoloOrder\Service\Behavior\Order\SelectOrderByDelivery' => function($sm) {
                return new \SoloOrder\Service\Behavior\Order\SelectOrderByDelivery;
            },
            'OrderSelectBehaviors' => function($sm) {
                $result = [
                    $sm->get('\SoloOrder\Service\Behavior\Order\SelectOrderByWarehouse'),
                    $sm->get('\SoloOrder\Service\Behavior\Order\SelectOrderByDelivery')
                ];
                return $result;
            },
            '\SoloOrder\Entity\Order\OrderDefinition' => function($sm) {
                return new \SoloOrder\Entity\Order\OrderDefinition;
            },
            '\SoloOrder\Service\BasketService' => function ($sm) {
                $basketService = new \SoloOrder\Service\BasketService();
                $basketService->setBasketStorage($sm->get('basket_storage'));
                if (\Solo\Stdlib\ClassUtils::hasTrait(get_class($basketService), 'SoloOrder\Service\ProvidesGrouping')) {
                    $groupHandler = $sm->get('group_handler');
                    if ($groupHandler === null) {
                        throw new \RuntimeException('Basket goods group handler doesn\'t configured! Check SoloOrder basket global config, swicth on grouping and define group_by and get_group collback functions');
                    }
                    $basketService->setGroupHandler($groupHandler);
                }
                $basketConfig = $sm->get('Config')['basket'];
                return $basketService;
            },
            '\SoloOrder\Entity\Basket\Delivery' => function($sm) {
                $delivery = new \SoloOrder\Entity\Basket\Delivery;
                $delivery->setAddressId(-1);
                return $delivery;
            },
            '\SoloOrder\Service\Behavior\Good\GroupHandler' => function($sm) {
//                $basketConfig = $sm->get('Config')['basket'];
                $handler = new \SoloOrder\Service\Behavior\Good\RemainsGroupHandler();
//                if (!array_key_exists('group_by', $basketConfig)) {
//                    throw new \RuntimeException("Grouping config must contain 'group_by' callback function");
//                } else {
//                    $handler->setGroupByCallback($basketConfig['group_by']);
//                    $handler->setCompareCallback($basketConfig['compare']);
//                    $handler->events()->attach('getGroup', $sm->get('\SoloOrder\Service\Behavior\Group\SelectGroupByDefault')->groupSelectCallback());
//                    return $handler;
//                }

                return $handler;
            },
            '\SoloOrder\Service\Behavior\Good\WarehouseDependent' => function($sm) {
                $behavior = new \SoloOrder\Service\Behavior\Good\WarehouseDependent();
                return $behavior;
            },
            '\SoloOrder\Entity\Basket\GoodsCollection' => function($sm) {
                return new \SoloOrder\Entity\Basket\GoodsCollection;
            },
            '\SoloOrder\Entity\Basket\Group' => function($sm) {
                /* @var $basketService \SoloOrder\Service\BasketService */
                $basketService = $sm->get('basket_service');
                $group = new \SoloOrder\Entity\Basket\Group($basketService->generateId());
                $group->events()->attach('addGoodToGroup', function($e) {

                        });
                return $group;
            },
            '\SoloOrder\Entity\Basket\Storage' => function($sm) {
                $storage = new \SoloOrder\Entity\Basket\Storage('basket');
                $storage->setGoodProcessingBehavior($sm->get('good_processing_behavior'));
                return $storage;
            },
            '\SoloOrder\Entity\Basket\Good' => function($sm) {
                $basketGood = new \SoloOrder\Entity\Basket\Good;
                return $basketGood;
            },
			'\SoloOrder\Entity\Basket\ComponentGood' => function($sm) {
                $basketGood = new \SoloOrder\Entity\Basket\ComponentGood;
                return $basketGood;
            },
            '\SoloOrder\Service\Behavior\Group\SelectGroupByWarehouse' => function($sm) {
                return new \SoloOrder\Service\Behavior\Group\SelectGroupByWarehouse();
            },
            '\SoloOrder\Service\Behavior\Group\SelectGroupByDelivery' => function($sm) {
                return new \SoloOrder\Service\Behavior\Group\SelectGroupByDelivery();
            },
            '\SoloOrder\Service\Behavior\Group\SelectGroupByDefault' => function($sm) {
                return new \SoloOrder\Service\Behavior\Group\SelectGroupByDefault();
            },
            'GroupSelectBehavior' => function($sm) {
                return [
                    $sm->get('\SoloOrder\Service\Behavior\Group\SelectGroupByDefault'),
                ];
            },
            '\SoloOrder\App\Service\BasketGroupService' => function($sm) {
                $service = new \SoloOrder\App\Service\BasketGroupService;
                $service->setGroupMergingHandler($sm->get('\SoloOrder\App\Service\Behavior\GroupMergingHandler'));
                $service->setDeliveryCostCalculator($sm->get('delivery_cost_calculator'));
                $service->setDeliveryShedulleCalculator($sm->get('delivery_shedulle_calculator'));
                return $service;
            },
            '\SoloOrder\App\Service\Behavior\GroupMergingHandler' => function($sm) {
                $merge = new \SoloOrder\App\Service\Behavior\GroupMergingHandler;
                return $merge;
            },
            '\SoloOrder\App\Service\Behavior\Order\OrdersCollectionGetter' => function($sm) {
                $ordersCollectionGetter = new \SoloOrder\App\Service\Behavior\Order\OrdersCollectionGetter;
                return $ordersCollectionGetter;
            },
            '\SoloOrder\App\Service\OrderGroupService' => function($sm) {
                $service = new \SoloOrder\App\Service\OrderGroupService;
                $service->setOrdersCollectionGetter($sm->get('orders_collection_getter'));
                $service->setOrdersInitializer($sm->get('orders_initializer'));
                $service->setOrderDecorator($sm->get('order_decorator'));
                return $service;
            },
            '\SoloOrder\App\Service\Behavior\Order\OrdersInitByGroupingStrategy' => function($sm) {
                $initializer = new \SoloOrder\App\Service\Behavior\Order\OrdersInitByGroupingStrategy;
                return $initializer;
            },
            '\SoloOrder\App\Service\Behavior\Order\OrderDecoratorGetter' => function($sm) {
                $decoratorGetter = new \SoloOrder\App\Service\Behavior\Order\OrderDecoratorGetter;
                return $decoratorGetter;
            }
        ),
        'shared' => array(
            /* order */
            '\SoloOrder\Entity\Order\Ultima\Good' => false,
            '\SoloOrder\Entity\Order\GoodsCollection' => false,
            '\SoloOrder\Entity\Order\Ultima\Order' => false,
            '\SoloOrder\Entity\Order\Ultima\Storage' => false,
            '\SoloOrder\Entity\Order\OrdersCollection' => false,
            '\SoloOrder\Entity\Order\Ultima\Settings' => false,
            '\SoloOrder\Entity\Order\OrderDefinition' => false,
            /* basket */
            '\SoloOrder\Entity\Basket\GoodsCollection' => false,
            '\SoloOrder\Entity\Basket\Good' => false,
			'\SoloOrder\Entity\Basket\ComponentGood' => false,
            '\SoloOrder\Entity\Basket\Storage' => false,
            '\SoloOrder\Entity\Basket\Group' => false,
            '\SoloOrder\Entity\Basket\Delivery' => false,
        )
    )
);
