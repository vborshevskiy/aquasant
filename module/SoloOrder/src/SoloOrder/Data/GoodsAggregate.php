<?php

namespace SoloOrder\Data;

use SoloOrder\Entity\Order\GoodOptions;
use Solo\Db\QueryGateway\QueryGateway;

class GoodsAggregate extends QueryGateway {

	public function enumGoods(GoodOptions $options, $cityId) {
		if (0 == count($options->getGoodIds())) {
			return [];
		}
		$quantityColumn = "";
		if ($options->hasDelivery()) {
			$quantityColumn = "agd.DeliveryQuantity AS AvailQuantity";
		} else {
			$quantityColumn = "aw.AvailQuantity" . (($options->hasSuborderedGoods()) ? "+agd.SuborderQuantity AS AvailQuantity" : "");
		}
		$sql = "SELECT
					ag.GoodID,
					ag.GoodName,
					ag.GoodEngName,
                                        ag.Volume,
                                        ag.Weight,
					gp." . $options->getPriceColumn() . " AS Price,
					gp." . $options->getPurchasePriceColumn() . " AS PurchasePrice,
                                        gp.BonusPrice,
					" . $quantityColumn . "
				FROM
					#all_goods# ag";
		// if ($hasWarehouseSorting) {
		if (!$options->hasDelivery() && $options->getWarehouseId()) {
			$wh = "AND aw.WarehouseID = " . $options->getWarehouseId();
		} else {
			$wh = "";
		}
		$sql .= " LEFT JOIN
						#avail_warehouses# aw
					ON (
						ag.GoodID = aw.GoodID " . $wh . "
					)";
		// if ($options->hasSuborderedGoods()) {
		$sql .= " LEFT JOIN #avail_goods# agd
					ON aw.GoodID = agd.GoodID";
		// }
		// }
		$sql .= " LEFT JOIN 
                                    #goods_prices@cityId:{$cityId}# gp
				ON (
					ag.GoodID = gp.GoodID
					AND gp.CityID = ".$cityId."
				)
				WHERE
					ag.GoodID IN (" . implode(',', $options->getGoodIds()) . ") GROUP BY ag.GoodID";
		$rows = $this->query($sql)->toArray();
		return $rows;
	}

	public function enumGoodsDescriptions(GoodOptions $options) {
		$sql = "SELECT
					ag.GoodID,
					ag.GoodName,
					ag.GoodEngName
				FROM
					#all_goods# ag
                                WHERE
                                ag.GoodID IN (" . implode(',', $options->getGoodIds()) . ")";
		$rows = $this->query($sql)->toArray();
		return $rows;
	}

}

?>