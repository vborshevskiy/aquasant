<?php

namespace SoloOrder\Data;

use Solo\Db\QueryGateway\QueryGateway;
use SoloOrder\Entity\CargoDeliveryCost;
use SoloOrder\Entity\CarDeliveryCost;

class RegionalDeliveryCostGateway extends QueryGateway {

	public function getRegionalDeliveryPrice($isHard = false) {
		$costCondition = 0;
		if ($isHard) {
			$column = 'hard';
			$cargoCost = new CargoDeliveryCost();
			$costCondition = $cargoCost->getCost();
		} else {
			$column = 'light';
			$carCost = new CarDeliveryCost();
			$costCondition = $carCost->getCost();
		}
		$sql = "SELECT
					$column as price
				FROM
					#kladr_costs#
				" . (0 < $costCondition ? " WHERE " . $column . " > " . $costCondition . " " : '') . " 
                                ORDER BY
                                        price ASC
                                LIMIT 1;";
		$rows = $this->query($sql)->toArray();
		return $rows[0]['price'];
	}

}

?>