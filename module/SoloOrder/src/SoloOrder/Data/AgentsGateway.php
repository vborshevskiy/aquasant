<?php

namespace SoloOrder\Data;

use Solo\Db\QueryGateway\QueryGateway;

class AgentsGateway extends QueryGateway {

	public function insertDeliveryLastAddress($idUser, $idAddress) {
		$sql = 'INSERT INTO agents
                        
                        (Login,LastAddr)
                    
                    VALUES
                    
                        (' . $idUser . ',' . $idAddress . ');';
		
		return $this->query($sql);
	}

	public function updateDeliveryLastAddress($idUser, $idAddress) {
		$sql = 'UPDATE agents
                    SET
                        LastAddr = ' . $idAddress . '
                    WHERE
                        Login = ' . $idUser . ';';
		
		return $this->query($sql);
	}

	public function getLastDeliveryAddress($idUser) {
		$sql = 'SELECT
                        LastAddr
                    FROM agents
                    WHERE Login = ' . $idUser;
		return $this->query($sql)->current();
	}

}

?>