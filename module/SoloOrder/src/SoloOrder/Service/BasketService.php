<?php

namespace SoloOrder\Service;

use SoloOrder\Entity\Basket\Good;
use SoloOrder\Entity\Basket\ComponentGood;
use SoloOrder\Entity\Basket\Group;
use SoloOrder\Entity\Basket\Delivery;
use SoloCatalog\Controller\Plugin\Catalog;

class BasketService extends \SoloOrder\Service\AbstractBasketService {

    use ProvidesGrouping;

    /**
     *
     * @return int
     */
    public function getTotalQuantity() {
        $quantity = 0;
        foreach ($this->getGoods() as $good) {
            $quantity += $good->getQuantity();
        }
        return $quantity;
    }

    /**
     *
     * @return int
     */
    public function getGroupsCount() {
        return sizeof($this->getGroups());
    }

    /**
     *
     * @return float
     */
    public function getTotalAmount() {
        $amount = 0;
        foreach ($this->getGoods() as $basketGood) {
            $amount += $basketGood->getAmount();
        }
        return $amount;
    }

    /**
     *
     * @return float
     */
    public function getTotalAmountByColumn($priceColumnName) {
        $amount = 0;
        foreach ($this->getGroups() as $group) {
            $amount += $group->getAmountByColumn($priceColumnName);
        }
        return $amount;
    }

    /**
     *
     * @return array
     */
    // метод не актуален, он не считает сумму согласно ценовой колонки выбранного КА
    // использовать BasketController::getState() либо аналог
    public function getState() {
        $state = [
            'quantity' => $this->getTotalQuantity(),
            'orders' => $this->getOrdersCount(),
            'amount' => $this->getTotalAmount()
        ];
        return $state;
    }

    /**
     *
     * @return array
     */
    public function enumGoodIds() {
        $ids = array();
        foreach ($this->getGoods() as $good) {
            $ids[] = $good->getId();
        }
        return array_unique($ids);
    }

    /**
     *
     * @param int $goodId
     * @return boolean
     */
    public function hasGood($goodId) {
        foreach ($this->getGoods() as $basketGood) {
            if ($basketGood->getId() == $goodId) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param array $goodsPrices
     * @return boolean
     */
    public function recalcBasketPrices($goodsPrices) {
        $goodsCollection = $this->getGoods();
        foreach ($goodsCollection as $good) {
            if (array_key_exists($good->getId(), $goodsPrices)) {
                $good->setPrice($goodsPrices[$good->getId()]);
            }
        }
        $this->updateBasket($goodsCollection);
        return true;
    }

    /**
     *
     * @param int $goodId
     * @return mixed
     */
    public function hasGoodByWarehouseId($goodId, $warehouseId) {
        $result = array();
        foreach ($this->orders as $order) {
            if (($warehouseId == $order->getWarehouse()) && $order->hasGood($goodId)) {
                $result[] = $order;
            }
        }
        return $result;
    }

    /**
     *
     * @param int $goodId
     * @param int $warehouseId
     */
    public function removeGoodFromWarehouse($goodId, $warehouseId) {
        $orders = $this->hasGoodByWarehouseId($goodId, $warehouseId);
        foreach ($orders as $order) {
            if ($order !== null) {
                $order->removeGood($goodId);
                if ($order->isEmpty()) {
                    $this->deleteOrder($order->getId());
                }
            }
        }
    }

    /**
     *
     * @param int $goodId
     */
    public function removeGoodFromDelivery($goodId) {
        if ($this->getGoodDeliveryQuantity($goodId) > 0) {
            foreach ($this->getGoods() as $basketGood) {
                if ($basketGood->hasDelivery() && $basketGood->getId() == $goodId) {
                    $this->removeGood($basketGood);
                }
            }
        }
    }

    /**
     *
     * @param int $goodId
     * @param int $warehouseId
     * @param boolean $first
     * @return int
     */
    public function getGoodQuantity($goodId, $warehouseId = null) {
        $quantity = 0;
        /*@var $basketGood \SoloBasket\Entity\Good */
        foreach ($this->getGoods() as $basketGood) {
            if (!$basketGood->hasDelivery() && $goodId == $basketGood->getId()) {
                if ($warehouseId !== null) {
                    if ($warehouseId == $basketGood->getWarehouseId()) {
                        $quantity += $basketGood->getQuantity();
                        return $quantity;
                    }
                } else {
                    $quantity += $basketGood->getQuantity();
                }
            }
        }
        return $quantity;
    }

    /**
     *
     * @param int $goodId
     * @return int
     */
    public function getGoodSuborderQuantity($goodId) {
        $quantity = 0;
        /*@var $basketGood \SoloBasket\Entity\Good */
        foreach ($this->getGoods() as $basketGood) {
            if (!$basketGood->hasDelivery() && ($basketGood->getId() == $goodId)) {
                $quantity += $basketGood->getSuborderQuantity();
            }
        }
        return $quantity;
    }

    /**
     *
     * @param BasketOrder $basketOrder
     * @param SoloCatalog\Controller\Plugin\Catalog $catalog
     * @return mixed
     */
    public function updateSuborderQuantity(BasketOrder $basketOrder, Catalog $catalog, $withSuborder = true) {
        foreach ($basketOrder->enumGoods() as $good) {
            $goodId = $good->getId();
            $good->setSuborderQuantity(0);
            $warehouse = $basketOrder->getWarehouse();
            $avails = $catalog->goods()->enumWarehouseAvailabilityByGoodId($goodId);
            $suborderInBasket = $this->getGoodSuborderQuantity($goodId);
            $deliveryInBasket = $this->getGoodDeliveryQuantity($goodId);
            $baseInBasket = $this->getGoodQuantity($goodId, 1);
            $suborderQty = $avails[1]->AvailQuantity - $baseInBasket - $deliveryInBasket - $suborderInBasket;
            $inOrder = $good->getQuantity();
            if ($suborderQty < 0 || !$withSuborder) {
                $suborderQty = 0;
            }
            if ($warehouse > 1) {
                $totalInBasket = $this->getGoodQuantity($goodId, $warehouse);
                $avail = $avails[$warehouse]->AvailQuantity - $totalInBasket + $suborderInBasket + $inOrder;
                if ($avail < 0) {
                    $avail = 0;
                }
                $diff = $inOrder - $avail;
                if ($diff > 0) {
                    if ($suborderQty >= ($diff)) {
                        $good->setSuborderQuantity($diff);
                    } else {
                        $good->setSuborderQuantity($suborderQty);
                        $good->setQuantity($good->getQuantity() - $diff + $suborderQty);
                    }
                    if ($good->getQuantity() <= 0) {
                        $basketOrder->removeGood($goodId);
                    }
                }
                $basketOrderAvails[$goodId] = $avail + $suborderQty;
            } else {
                $basketOrderAvails[$goodId] = $suborderQty + $inOrder;
            }
        }
        if ($basketOrder->isEmpty()) {
            $this->deleteOrder($basketOrder->getId());
            return null;
        }
        return $basketOrderAvails;
    }

    /**
     *
     * @param int $quantity
     * @param int $goodId
     * @param int $price
     * @param mixed $prices
     * @param mixed $good
     * @param int $availQuantity
     * @param int $suborderAvailQuantity
     * @param int $warehouseId
     * @return Group
     */

    public function increaseGoodQuantityBy($quantity, $goodId, $price, $prices, $good, $availQuantity, $suborderAvailQuantity = 0, $warehouseId = 0) {
        if ((int)$availQuantity < 0) {
            $availQuantity = 0;
        }
        $suborderQuantity = 0;
        if ($quantity > $availQuantity) {
            if (($warehouseId == 1) || ($suborderAvailQuantity <= 0)) {
                $quantity = $availQuantity;
            } else {
                $suborderQuantity = $quantity - $availQuantity;
                $diff = $suborderAvailQuantity - $suborderQuantity;
                if ($diff < 0) {
                    $suborderQuantity = $suborderAvailQuantity;
                    $quantity += $diff;
                }
            }
        } else {
            $suborderQuantity = 0;
        }
        if ($quantity > 0) {
            /*@var $basketGood \SoloOrder\Entity\Basket\Good */
            $basketGood = $this->newGood();
            $basketGood->setId($goodId);
            $basketGood->setPrice($price);
            $basketGood->setQuantity($quantity);
            $basketGood->setSuborderQuantity($suborderQuantity);
            $basketGood->setVolume($good->Volume);
            $basketGood->setWeight($good->Weight);
            $basketGood->setWarehouseId($warehouseId);
            $basketGood->setPrices($prices);
            $this->addGood($basketGood);
        }
        return $this->getGroup(array('warehouse' => $warehouseId));
    }

    /**
     *
     * @param int $quantity
     * @param int $goodId
     * @param int $warehouseId
     * @return mixed
     */
    public function decreaseGoodQuantityBy($quantity, $goodId, $warehouseId = 0) {
        arsort($this->orders);
        $decrease['quantity'] = $quantity;
        foreach ($this->orders as &$order) {
            if ($order->getWarehouse() == $warehouseId && $order->hasGood($goodId)) {
                $decrease = $this->decreaseGoodQuantity($goodId, $decrease['quantity'], $order);
                $result[] = $decrease['order'];
                if ($decrease['quantity'] == 0) {
                    return $result;
                }
            }
        }
        arsort($this->orders);
        return $result;
    }

    public function newDelivery() {
        return $this->getServiceLocator()->get('delivery_instance');
    }

    /**
     *
     * @param int $goodId
     * @param int $quantity
     * @param mixed $price
     * @param mixed $good
     * @param int $deliveryQuantity
     * @return BasketOrder
     */
    public function increaseGoodDeliveryQuantityBy($quantity, $goodId, $price, $prices, $good, $deliveryQuantity) {
        $totalInBasket = $this->getGoodQuantity($goodId);
        $deliveryInBasket = $this->getGoodDeliveryQuantity($goodId);
        $inOrder = $this->getGoodDeliveryQuantity($goodId, -1);
        $avail = $deliveryQuantity - $totalInBasket;
        if ($quantity > $avail) {
            $quantity = $avail;
        }
        if ($quantity > 0) {
            /*@var $basketGood \SoloBasket\Entity\Good */
            $basketGood = $this->newGood();
            $basketGood->setId($goodId);
            $basketGood->setPrice($price);
            $basketGood->setQuantity($quantity);
            $basketGood->setVolume($good->Volume);
            $basketGood->setWeight($good->Weight);
            $basketGood->setDelivery($this->newDelivery());
            $basketGood->setPrices($prices);
            $this->addGood($basketGood);
        }
        $emptyDelivery = $this->newDelivery();
        return $this->getGroup(array('delivery' => $emptyDelivery->getAddressId(), 'date' => $emptyDelivery->getDeliveryDate(), 'time' => $emptyDelivery->getDeliveryTime()));
    }

    /**
     *
     * @param int $goodId
     * @param int $quantity
     * @return mixed
     */
    public function decreaseGoodDeliveryQuantityBy($quantity, $goodId) {
        $order = $this->findDeliveryOrder(-1);
        if ($order !== null && $order->hasGood($goodId)) {
            $decrease = $this->decreaseGoodQuantity($goodId, $quantity, $order);
            $result[] = $decrease['order'];
            if ($decrease['quantity'] == 0) {
                return $result;
            }
        } else {
            $decrease['quantity'] = $quantity;
        }
        arsort($this->orders);
        foreach ($this->orders as &$order) {
            if ($order->hasDelivery() && ($order->getDelivery()->getAddressId() != -1) && $order->hasGood($goodId)) {
                $decrease = $this->decreaseGoodQuantity($goodId, $decrease['quantity'], $order);
                $result[] = $decrease['order'];
                if ($decrease['quantity'] == 0) {
                    return $result;
                }
            }
        }
        arsort($this->orders);
        return $result;
    }

    /**
     *
     * @param int $goodId
     * @param int $quantity
     * @param BasketOrder $order
     * @return mixed
     */
    private function decreaseGoodQuantity($goodId, $quantity, $order) {
        $inOrder = $order->getGood($goodId)->getQuantity();
        $diff = $quantity - $inOrder;
        if ($diff >= 0) {
            $order->removeGood($goodId);
            if ($order->isEmpty()) {
                $this->deleteOrder($order->getId());
                $order = null;
            }
        } else {
            $good = $order->getGood($goodId);
            $good->setQuantity(abs($diff));
            if (!$order->hasDelivery()) {
                if ($good->getSuborderQuantity() > $quantity) {
                    $good->setSuborderQuantity($good->getSuborderQuantity() - $quantity);
                } else {
                    $good->setSuborderQuantity(0);
                }
            }
            $diff = 0;
        }
        return array(
            'quantity' => $diff,
            'order' => $order
        );
    }

    /**
     *
     * @param int $goodId
     * @param int $deliveryAddressId
     * @return int
     */
    public function getGoodDeliveryQuantity($goodId, $deliveryAddressId = null) {
        $quantity = 0;
        /*@var $basketGood \SoloBasket\Entity\Good */
        foreach ($this->getGoods() as $basketGood) {
            if (($basketGood->hasDelivery() && is_null($deliveryAddressId)) || ($basketGood->hasDelivery() && $deliveryAddressId !== null && $deliveryAddressId == $basketGood->getDelivery()->getAddressId())) {
                $quantity += $basketGood->getQuantity();
            }
        }
        return $quantity;
    }

    /**
     *
     * @return boolean
     */
    public function isEmpty() {
        return (0 == $this->getTotalQuantity());
    }

}
