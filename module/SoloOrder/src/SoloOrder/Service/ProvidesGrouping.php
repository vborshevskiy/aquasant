<?php
namespace SoloOrder\Service;

/**
 * ProvidesGoodsGrouping
 *
 * @author slava
 */
trait ProvidesGrouping {
    
    /**
     *
     * @var \SoloOrder\Service\Behavior\Good\GroupHandler
     */
    protected $groupHandler = null;
    
    /**
     *
     * @var array
     */
    protected $eventListeners = array();
    
    public function addEventListener($name, $callback, $priority) {
        $this->eventListeners[] = array(
            'name' => $name,
            'callback' => $callback,
            'priority' => $priority
        );
    }
    
    public function removeEventListener($name) {
        if (isset($this->eventListeners[$name])) {
            unset($this->eventListeners[$name]);
        }
    }
    
    public function getEventListeners() {
        return $this->eventListeners;
    }
    
    public function setGroupHandler(\SoloOrder\Service\Behavior\Good\GroupHandlerInterface $groupHandler) {
//        $groupHandler->basket($this);
        $this->groupHandler = $groupHandler;
    }
    
    public function getGroupHandler() {
        return $this->groupHandler;
    }
    
    public function newGroup() {
        return $this->getServiceLocator()->get('basket_group_instance');
    }
    
    public function getGroups() {
        $groups = $this->getGroupHandler()->getGroups($this->getGoods(), $this->getEventListeners());
        return $groups;
    }
    
    public function getGroup($queryParams) {
        return $this->getGroupHandler()->getGroup($queryParams);
    }
    
    public function removeGroup($queryParams) {
        $group = $this->getGroup($queryParams);
        if ($group !== null) {
            $goods = $group->getGoods();
            foreach ($goods as $basketGood) {
                $this->removeGood($basketGood);
            }
        }
    }
    
    public function modifyGroup($group, $toWarehouseId, $toDelivery) {
        if ($group !== null) {
            $goods = $group->getGoods();
            foreach ($goods as $good) {
                $this->removeGood($good);
                $good->setWarehouseId($toWarehouseId);
                $good->setDelivery($toDelivery);
                $this->addGood($good);
            }
        }
    }
    
}

?>
