<?php

namespace SoloOrder\Service\Result\Entity;

/**
 * GoodNotAvailable
 *
 * @author Slava Tutrinov
 */
class Warranty {

	private $documentID = null;

	private $documentDate = null;

	private $documentNo = null;

	private $goodId = null;

	private $goodName = null;

	private $goodStatus = null;

	private $agentId = null;

	public function getDocumentID() {
		return $this->documentID;
	}

	public function setDocumentID($documentID) {
		$this->documentID = $documentID;
	}

	public function getDocumentDate() {
		return $this->documentDate;
	}

	public function setDocumentDate($documentDate) {
		$dateOnly = explode('T', $documentDate);
		$dateOnlyArray = explode('-', $dateOnly[0]);
		$this->documentDate = $dateOnlyArray[2] . '.' . $dateOnlyArray[1] . '.' . $dateOnlyArray[0];
		if ($this->documentDate == '..') {
			$this->documentDate = null;
		}
	}

	public function getDocumentNo() {
		return $this->documentNo;
	}

	public function setDocumentNo($documentNo) {
		$this->documentNo = $documentNo;
	}

	public function getGoodId() {
		return $this->goodId;
	}

	public function setGoodId($goodId) {
		$this->goodId = $goodId;
	}

	public function getGoodName() {
		return $this->goodName;
	}

	public function setGoodName($goodName) {
		$this->goodName = $goodName;
	}

	public function getGoodStatus() {
		return $this->goodStatus;
	}

	public function setGoodStatus($goodStatus) {
		$this->goodStatus = $goodStatus;
	}

	public function getAgentId() {
		return $this->agentId;
	}

	public function setAgentId($agentId) {
		$this->agentId = $agentId;
	}

}

?>
