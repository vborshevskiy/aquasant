<?php

namespace SoloOrder\Service\Ultima\Result\Entity;

/**
 * CashlessReserve
 *
 * @author Slava Tutrinov
 */
class CashlessReserve extends \SoloOrder\Service\Result\Entity\CashReserve {

	private $invoiceCode = null;

	private $invoiceNumber = null;

	private $invoiceTimestamp = null;

	public function getInvoiceCode() {
		return $this->invoiceCode;
	}

	public function setInvoiceCode($invoiceCode) {
		$this->invoiceCode = $invoiceCode;
	}

	public function getInvoiceNumber() {
		return $this->invoiceNumber;
	}

	public function setInvoiceNumber($invoiceNumber) {
		$this->invoiceNumber = $invoiceNumber;
	}

	public function getInvoiceTimestamp() {
		return $this->invoiceTimestamp;
	}

	public function setInvoiceTimestamp($invoiceTimestamp) {
		$this->invoiceTimestamp = $invoiceTimestamp;
	}

}

?>
