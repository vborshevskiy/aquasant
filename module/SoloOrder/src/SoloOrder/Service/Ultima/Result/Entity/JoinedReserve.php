<?php

namespace SoloOrder\Service\Result\Entity;

/**
 * Description of JoinedReserve
 *
 * @author slava
 */
class JoinedReserve {

	private $code = null;

	private $agent = null;

	public function getCode() {
		return $this->code;
	}

	public function setCode($code) {
		$this->code = $code;
	}

	public function getAgent() {
		return $this->agent;
	}

	public function setAgent($agent) {
		$this->agent = $agent;
	}

}

?>
