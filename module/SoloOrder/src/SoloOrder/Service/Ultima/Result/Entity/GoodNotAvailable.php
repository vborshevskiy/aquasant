<?php

namespace SoloOrder\Service\Ultima\Result\Entity;

/**
 * GoodNotAvailable
 *
 * @author Slava Tutrinov
 */
class GoodNotAvailable {

	private $goodId = null;

	private $barcodeId = null;

	private $availQuantity = null;

	public function getAvailQuantity() {
		return $this->availQuantity;
	}

	public function setAvailQuantity($availQuantity) {
		$this->availQuantity = $availQuantity;
	}

	public function getGoodId() {
		return $this->goodId;
	}

	public function setGoodId($goodId) {
		$this->goodId = $goodId;
	}

	public function getBarcodeId() {
		return $this->barcodeId;
	}

	public function setBarcodeId($barcodeId) {
		$this->barcodeId = $barcodeId;
	}

}

?>
