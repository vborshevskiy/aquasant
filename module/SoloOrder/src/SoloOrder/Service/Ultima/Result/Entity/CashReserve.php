<?php

namespace SoloOrder\Service\Ultima\Result\Entity;

/**
 * CashReserve
 *
 * @author Slava Tutrinov
 */
class CashReserve implements \SoloOrder\Entity\Order\CustomerInfoOwner {

	private $reserveCode = null;

	private $reserveNumber = null;

	private $customerInfo = null;

	private $reserveSum = null;

	public function getReserveSum() {
		return $this->reserveSum;
	}

	public function setReserveSum($reserveSum) {
		$this->reserveSum = $reserveSum;
	}

	public function getReserveCode() {
		return $this->reserveCode;
	}

	public function setReserveCode($reserveCode) {
		$this->reserveCode = $reserveCode;
	}

	public function getReserveNumber() {
		return $this->reserveNumber;
	}

	public function setReserveNumber($reserveNumber) {
		$this->reserveNumber = $reserveNumber;
	}

	public function getCustomerInfo() {
		return $this->customerInfo;
	}

	public function setCustomerInfo(\SoloOrder\Entity\Order\CustomerInfo $customerInfo) {
		$this->customerInfo = $customerInfo;
	}

}

?>
