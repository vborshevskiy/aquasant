<?php

namespace SoloOrder\Service\Result;

/**
 * DeleteJBReserve
 *
 * @author Slava Tutrinov
 */
class DeleteJBReserveResult extends \Solo\ServiceManager\Result {

	const ERROR_INVALID_RESERVE_OWNER = 501;

	const ERROR_RESERVE_DONT_EXISTS = 502;

}

?>
