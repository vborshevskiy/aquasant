<?php

namespace SoloOrder\Service\Ultima\Result;

/**
 * CreateReserveResult
 *
 * @author Slava Tutrinov
 */
class CreateReserveResult extends \Solo\ServiceManager\Result {

	const ERROR_UNPROVIDED = 401;

	const ERROR_UNPROVIDED_FROM_INVOICE_CREAT = 402;

        public function setData($result) {
		if (sizeof($result) == 2 && is_string($result[1]) && is_int($result[0])) {
			$reserve = new \SoloOrder\Service\Ultima\Result\Entity\CashReserve();
			$reserve->setReserveCode($result[0]);
			$reserve->setReserveNumber($result[1]);
			$this->data = $reserve;
		} elseif (sizeof($result) == 5) {
			$reserve = new \SoloOrder\Service\Ultima\Result\Entity\CashlessReserve();
			$reserve->setReserveCode($result[0]);
			$reserve->setReserveNumber($result[1]);
			$reserve->setInvoiceCode($result[2]);
			$reserve->setInvoiceNumber($result[3]);
			$reserve->setInvoiceTimestamp($result[4]);
			$this->data = $reserve;
		} else {
			if (sizeof($result) == 2 && is_array($result[0]) && sizeof($result[0]) == 3 && $result[0][0] == 'GoodID' && $result[0][1] == 'BarcodeID' && $result[0][2] == 'AvailQuantity') {
				$res = new \SoloOrder\Service\Ultima\Result\Entity\NotAvailableGoodCollection();
				foreach ($result[1] as $g) {
					$nag = new \SoloOrder\Service\Ultima\Result\Entity\GoodNotAvailable();
					$nag->setGoodId($g[0]);
					$nag->setBarcodeId($g[1]);
					$nag->setAvailQuantity($g[2]);
					$res->add($nag);
				}
				$this->data = $res;
			} else {
				$this->data = $result;
			}
		}
	}

	public function setCustomerInfo(\SoloOrder\Entity\CustomerInfo $customerInfo) {
		$data = $this->getData();
		if ($data instanceof \SoloOrder\Service\Ultima\Result\Entity\CashReserve || $data instanceof \SoloOrder\Service\Ultima\Result\Entity\CashlessReserve) {
			/* @var $data \SoloOrder\Entity\CustomerInfoOwner */
			$this->data->setCustomerInfo($customerInfo);
		}
	}

	public function getCustomerInfo() {
		$data = $this->getData();
		if ($data instanceof \SoloOrder\Service\Ultima\Result\Entity\CashReserve || $data instanceof \SoloOrder\Service\Ultima\Result\Entity\CashlessReserve) {
			/* @var $data \SoloOrder\Entity\CustomerInfoOwner */
			return $data->getCustomerInfo();
		}
	}

	public function setReserveSum($reserveSum) {
		$data = $this->getData();
		if ($data instanceof \SoloOrder\Service\Ultima\Result\Entity\CashReserve || $data instanceof \SoloOrder\Service\Ultima\Result\Entity\CashlessReserve) {
			$this->data->setReserveSum($reserveSum);
		}
	}

	public function getReserveSum() {
		if ($this->data instanceof \SoloOrder\Service\Ultima\Result\Entity\CashReserve || $this->data instanceof \SoloOrder\Service\Ultima\Result\Entity\CashlessReserve) {
			return $this->data->getReserveSum();
		}
	}

}

?>
