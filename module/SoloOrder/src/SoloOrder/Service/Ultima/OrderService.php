<?php

namespace SoloOrder\Service\Ultima;

use SoloCatalog\Service\Helper\DateHelper;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloERP\WebService\Reader\UltimaObjectReader;
use SoloIdentity\Entity\AbstractAgent;
use SoloIdentity\Entity\Ultima\User;
use SoloOrder\Entity\Order\Ultima\Order;
use SoloOrder\Service\Ultima\Result\CreateReserveResult;
use SoloOrder\Entity\Ultima\Good;
use Solo\ServiceManager\Result;
use SoloOrder\Service\Ultima\Result\PayReserveResult;
use SoloIdentity\Entity\Phone;

/**
 * OrderService
 *
 * @author Slava Tutrinov
 */
class OrderService extends \SoloOrder\Service\AbstractOrderService {

    const MANUALLY_ENTERED_ADDRESS_ID = 2;

    const MANUALLY_ENTERED_DELIVERY_COST = -1;

    /**
     *
     * @var \SoloOrder\Data\GoodsAggregate;
     */
    private $goodsAggregate = null;

    /**
     *
     * @var \SoloKladr\Service\AddressSearchService
     */
    private $addressService = null;

    public function getAddressService() {
        return $this->addressService;
    }

    public function setAddressService($addressService) {
        $this->addressService = $addressService;
    }

    public function getGoodsAggregate() {
        return $this->goodsAggregate;
    }

    public function setGoodsAggregate(\SoloOrder\Data\GoodsAggregate $goodsAggregate) {
        $this->goodsAggregate = $goodsAggregate;
    }

    public function getGoodsInfo(\SoloOrder\Entity\Order\GoodOptions $options, $cityId) {
        $enumGoods = $this->getGoodsAggregate()->enumGoods($options, $cityId);
        foreach ($enumGoods as $key => $good) {
            if ($good['BonusPrice'] > 0) {
                $enumGoods[$key]['NoBonusPrice'] = $good['PurchasePrice'];
            } else {
                $enumGoods[$key]['NoBonusPrice'] = $good['Price'];
            }
        }
        return $enumGoods;
    }

    /**
     *
     * @param \SoloOrder\Entity\GoodOptions $options
     * @return array
     */
    public function getGoodsDescriptions(\SoloOrder\Entity\GoodOptions $options) {
        return $this->getGoodsAggregate()->enumGoodsDescriptions($options);
    }

    /**
     *
     * @return \SoloERP\Data\Site\CreateReserve
     */
    public function getReserveCreatorWebMethod() {
        return $this->getWebMethod('CreateReserve');
    }

    /**
     *
     * @return \SoloERP\Data\Site\PayReserve
     */
    public function getPayReserveWebMethod() {
        return $this->getWebMethod('PayReserve');
    }

    /**
     *
     * @return \SoloERP\Data\Site\GetReserveAmount
     */
    public function getReserveAmountWebMethod() {
        return $this->getWebMethod('GetReserveAmount');
    }

    /**
     *
     * @return \SoloERP\Data\Site\CreateReserve
     */
    public function getReserveUpdaterWebMethod() {
        return $this->getWebMethod('UpdateReserve');
    }

    public function getSavedOrderFromQueue($orderId) {
        $storage = $this->getLocalStorage();
        return $storage->getSavedOrderFromQueue($orderId);
    }


    public function setOrdersQueue(\SoloOrder\Entity\Ultima\OrdersQueue $ordersQueue) {
        $this->getLocalStorage()->setOrdersQueue($ordersQueue);
    }

    public function getOrdersQueue() {
        return $this->getLocalStorage()->getOrdersQueue();
    }

    public function syncOrdersQueue(\SoloOrder\Entity\Ultima\OrdersQueue $ordersQueue) {
        $this->getLocalStorage()->syncOrdersQueue();
    }

    public function getOrdersQueueSize() {
        return $this->getLocalStorage()->getOrdersQueueSize();
    }

    public function getOrderIndexInQueue($orderId) {
        return $this->getLocalStorage()->getOrderIndexInQueue();
    }

    public function clearOrdersQueue() {
        $this->getLocalStorage()->clearOrdersQueue();
    }

    /**
     * Создание резерва
     * @param \SoloOrder\Entity\Order\AbstractOrder $order
     * @param integer $storeId
     *
     * @return \SoloIdentity\Result\CreateReserveResult
     */
    public function create(\SoloOrder\Entity\Order\AbstractOrder $order, $storeId = \SoloOrder\Entity\Order\Ultima\Order::MOSCOW_MAIN_STORE_ID) {
        $args = $this->events()->prepareArgs([
            'order' => $order,
        ]);
        $this->events()->trigger('createReserve.pre', $this, $args);
        $order = $args['order'];
        $goodsCollection = $order->getGoodsCollection();
        $articles = [];
        /*@var $orderGood \SoloOrder\Entity\Ultima\Good */
        foreach ($goodsCollection as $orderGood) {
            if (count($orderGood->getPackageComponents())) {
                $articles[$orderGood->getMarking()] = [
                    'Id' => $orderGood->getMarking(),
                    'Quantity' => $orderGood->getQuantity(),
                    'PackageId' => $orderGood->getMarking(),
                    'AllowShowcaseSale' => $orderGood->allowShowcaseSale()
                ];

                foreach ($orderGood->getPackageComponents() as $componentGood) {
                	$componentId = (int) $componentGood->getId();
                    $articles[$componentId] = [
                        'Id' => $componentId,
                        'Quantity' => $orderGood->getQuantity(),
                        'PackageId' => $orderGood->getMarking(),
                        'AllowShowcaseSale' => $componentGood->allowShowcaseSale()
                    ];
                }
            } else {
                $articles[$orderGood->getMarking()] = [
                    'Id' => $orderGood->getMarking(),
                    'Quantity' => $orderGood->getQuantity(),
                    'AllowShowcaseSale' => $orderGood->allowShowcaseSale()
                ];
            }
        }
        $articles = array_values($articles);

        $wm = $this->getReserveCreatorWebMethod();
        $wm->addPar('Articles', $articles);
        $wm->addPar('PaymentTypeId', $order->getPaymentId());
        if ($order->setting('comment')) {
        	$wm->addPar('Comment', str_replace("\n", '', $order->setting('comment')));
        }
        if ($order->setting('source')) {
        	$wm->addPar('SaleSourceId', $order->setting('source'));
        }

        if ($order->getObtainMethod() == Order::DELIVERY_OBTAIN_METHOD &&  $order->hasDelivery()) {
            //доставка
            $wm->addPar('ObtainMethod', \SoloOrder\Entity\Order\Ultima\Order::DELIVERY_OBTAIN_METHOD);

            $delivery = [
                'AddressId' => $order->getDelivery()->getAddressId(),
                'TimeId' => $order->getDelivery()->getDeliveryTimeId(),
                'Date' => DateHelper::getSerializeJsonDate($order->getDelivery()->getDeliveryDate()),
                'Option' => \SoloOrder\Entity\Order\Ultima\Order::OWN_DELIVERY,
				'Comments' => $order->setting('deliveryComment') ? $order->setting('deliveryComment') : '',
                //'ContactPhone' => $order->getDelivery()->getDeliveryPhone(),
            ];

            if ($order->getDelivery()->needLiftToTheFloor()) {
				$delivery['NeedLifting'] = true;
                $delivery['ElevatorTypeID'] = $order->getDelivery()->getElevatorTypeId();
                $delivery['Floor'] = $order->getDelivery()->getFloor();
            }

            $wm->addPar('Delivery', $delivery);
            $wm->addPar('ReserveOfficeId', $storeId);
        } elseif ($order->getObtainMethod() == Order::DELIVERY_OBTAIN_METHOD && $order->hasManuallyEnteredDelivery()) {
            // доставка на введенный руками адрес
            $wm->addPar('ObtainMethod', \SoloOrder\Entity\Order\Ultima\Order::DELIVERY_OBTAIN_METHOD);
            $delivery = [
                'AddressId' => $order->getManuallyEnteredDelivery()->getAddressId(),
                'TimeId' => $order->getManuallyEnteredDelivery()->getDeliveryTimeId(),
                'Date' => $order->getManuallyEnteredDelivery()->getDeliveryDate(),
                'Option' => \SoloOrder\Entity\Order\Ultima\Order::OWN_DELIVERY,
                'Comments' => 'Уточнить адрес доставки - ' . $order->getManuallyEnteredDelivery()->getDeliveryInfo(),
                //'ContactPhone' => $order->getManuallyEnteredDelivery()->getDeliveryPhone(),
            ];
            $wm->addPar('Delivery', $delivery);
            $wm->addPar('ReserveStoreId', $storeId);
        } elseif ($order->getObtainMethod() == Order::DELIVERY_OBTAIN_METHOD && $order->hasPostDelivery()) {
            // почтовая доставка
            $wm->addPar('ObtainMethod', \SoloOrder\Entity\Order\Ultima\Order::DELIVERY_OBTAIN_METHOD);
            $delivery = [
                'AddressId' => $order->getPostDelivery()->getAddressId(),
                'Option' => \SoloOrder\Entity\Order\Ultima\Order::HYBRID_DELIVERY,
                //'ContactPhone' => $order->getPostDelivery()->getDeliveryPhone(),
                'LogisticCompanyCost' => $order->getPostDelivery()->getDeliveryCost(),
                'LogisticCompanyId' => $order->getPostDelivery()->getTransportCompanyErpId(),
                'TimeId' => $order->getPostDelivery()->getFakeDeliveryTimeId(),
                'Date' => $order->getPostDelivery()->getDeliveryDate(),
                'Comments' => 'Почтовая торговля',
            ];
            $wm->addPar('Delivery', $delivery);
            $wm->addPar('ReserveStoreId', $storeId);
        } elseif ($order->getObtainMethod() == Order::DELIVERY_OBTAIN_METHOD && $order->hasPostPickup()) {
            // почтовая доставка
            $wm->addPar('ObtainMethod', \SoloOrder\Entity\Order\Ultima\Order::DELIVERY_OBTAIN_METHOD);
            $delivery = [
                'AddressId' => $order->getPostPickup()->getAddressId(),
                'Option' => \SoloOrder\Entity\Order\Ultima\Order::HYBRID_DELIVERY,
                //'ContactPhone' => $order->getPostPickup()->getDeliveryPhone(),
                'LogisticCompanyCost' => $order->getPostPickup()->getDeliveryCost(),
                'LogisticCompanyId' => $order->getPostPickup()->getTransportCompanyErpId(),
                'TimeId' => $order->getPostPickup()->getFakeDeliveryTimeId(),
                'Date' => $order->getPostPickup()->getDeliveryDate(),
                'Comments' => 'Самовывоз ПЭК',
            ];
            $wm->addPar('Delivery', $delivery);
            $wm->addPar('ReserveStoreId', $storeId);
        } else {
            // самовывоз
            if (!is_null($order->getPickupDate())) {
                $wm->addPar('PickupDate', $order->getPickupDate());
            }
            $wm->addPar('ObtainMethod', \SoloOrder\Entity\Order\Ultima\Order::PICKUP_OBTAIN_METHOD);
            $wm->addPar('ReserveOfficeId', $storeId);
        }
        if ($order->getPayerPerson() == Order::PAYER_LEGAL_PERSON && $order->hasAgentId()) {
            // юрик
            $wm->addPar('AgentId', $order->getAgentId());
        } elseif ($order->hasAgentId()) {
        	$wm->addPar('AgentId', $order->getAgentId());
        }

        if ($order->setting('phone')) {
        	$phone = Phone::createFromString($order->setting('phone'));
        	$wm->addPar('OrderPhoneNumber', $phone->toString('7{code}{number}'));
			$wm->addPar('ContactPhone', $phone->toString('7{code}{number}'));
        }
        if ($order->setting('managerId')) {
        	$wm->addPar('ManagerId', $order->setting('managerId'));
        }

		$wm->addPar('ContactName', $order->setting('userName') ?: 'no name');
		$wm->addPar('ContactEmail', $order->setting('userEmail') ?: '');

//        else {
//            // физик
//            $wm->addPar('TempPhone', $order->setting('phone'));
//            $wm->addPar('TempName', $order->setting('userName'));
//            if ($order->hasBonusAmount()) {
//                $wm->addPar('BonusAmount', $order->getBonusAmount());
//            }
//        }

		//print json_encode($wm->dump(true)); exit();
		
// 		sleep(60);

//          print '<h1>Don\'t panic. Debugging</h1>';
//  		print '<pre>'.json_encode($wm->getPars(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT).'</pre>';
//  		exit();

        //mail('rgoryanin@it-solo.com', 'santbaza.ru: create reserve', json_encode($wm->getPars(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));

        //$wm->addPar('TargetReserveType', 'draft');
        //print json_encode($wm->getPars(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT); exit();
		//echo '<pre>',print_r($wm->getPars()),'</pre>'; die();
		//mail('v.detman@gmail.com', 'santbaza.ru: create reserve '.date("H:i"), json_encode($wm->getPars(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));

		$response = $wm->call();

		//print_r($response); exit();
		//print_r($response->getResponse()->getContent()); exit();

//        var_dump($response->getResponse()->getHeaders()->toString()); die;

        $mailContent = json_encode($wm->getPars(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        $mailContent .= 'Response: '."\n";
        $mailContent .= print_r(json_decode($response->getResponse()->getContent(), true), true);
		//mail('rgoryanin@it-solo.com', 'Aquasant: order', $mailContent);
		mail('vborshevskiy@taxasoftware.com', 'Aquasant: order', $mailContent);
		//mail('v.detman@gmail.com', 'Aquasant: order', $mailContent);

        $result = new CreateReserveResult();
        $result->setData(json_decode($response->getResponse()->getContent()));
        if ($response->hasError()) {
            $result->setError($response->getError());
            print "<h1>Ultima error</h1>";
            print "<p>Message: ".$response->getErrorMessage()."</p>";
            print "<p>Method: CreateReserve</p>";
            print "<p>Parameters: <pre>".json_encode($wm->getPars(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT)."</pre></p>";
            return false;
        }
        $result->setOriginalRequest(json_encode($wm->getPars()));
        $result->setOriginalResponse($response->getResponse()->getHeaders()->toString());
        return $result;
    }

    public function createDraft(User $user, $goods, $quantities, $prices = array(), $noBonuses = false, $hasDelivery = false, $deliveryDate = null, $deliveryTimeId = null, $addressId = null, $warehouseId = null, $deliveryComments = "", $siteId = 2, $bonusAmount = null) {
        /* @var $wm \SoloERP\Data\CreateDraftBusiness */
        $wm = $this->getWebMethod("CreateDraftBusiness");
        $wm->setAgentId($user->getNaturalPersonId());
        $wm->setGoods($goods);
        $wm->setQuantities($quantities);
        $wm->setDelivery($hasDelivery);
        $wm->setDeliveryDate($deliveryDate);
        $wm->setDeliveryTimeId($deliveryTimeId);
        $wm->setAddressId($addressId);
        $wm->setWarehouseId((int) $warehouseId);
        $wm->setDeliveryComments($deliveryComments);
        $wm->setSiteId($siteId);
        $wm->setPrices($prices);
        $wm->setNoBonuses($noBonuses);
        $wm->setBonusAmount($bonusAmount);
        $wm->setUserId($user->getId());
        $response = $wm->call();

        $result = new \SoloOrder\Service\Result\CreateDraftResult();
        $result->setData($response->getData());
        if ($response->hasError()) {
            $result->setError($response->getError());
        }
        return $result;
    }

    public function cleanedOfDeliveryGoods(&$items) {
        $keys = array_keys($items);
        $itemIds = array_intersect($keys, Good::deliveryGoods());
        $deliveryGoods = array();
        foreach ($itemIds as $n => $key) {
            $deliveryGoods[$key] = $items[$key];
            unset($items[$key]);
        }
        return $deliveryGoods;
    }

    /**
     * Получаем доступный лимит на сумму бронирования по контрагенту
     *
     * @param int $agentId
     *        	ид контрагента
     */
    public function getAgentAvailableLimit($agentId = 0) {
        $defaultLimit = (float) 120000.00;

        if (!preg_match("~^[0-9]+$~", $agentId)) {
            return $defaultLimit;
        }

        $wm = $this->getWebMethod("GetMaxReserveInfo");
        $wm->setAgentId((int) $agentId);
        $response = $wm->call();

        $data = $response->getData();

        // если лимит получен из сервиса значит берем его
        if (!empty($data[0]) && !empty($data[1][0])) {
            $limit = array_combine($data[0], $data[1][0]);

            if (isset($limit['NORMA']) && isset($limit['AMOUNT'])) {
                return (float) ($limit['NORMA'] - $limit['AMOUNT']);
            }
        }

        return $defaultLimit;
    }

    public function getOrders() {
        if ($this->events()->getListeners('getOrders')->count() != 1) {
            throw new \RuntimeException('Orders getting event callback isn\'t defined');
        }
        $args = $this->events()->prepareArgs([
            'orderService' => $this,
        ]);
        $this->events()->trigger('getOrders', $this, $args);
        $this->events()->trigger('getOrders.post', $this, $args);
        $collection = (isset($args['collection'])?$args['collection']:$this->newOrdersCollection());
        reset($collection);
        if (sizeof($collection) == 1) {
            $order = $collection[0];
            $args = $this->events()->prepareArgs(['order' => $order]);
            $this->events()->trigger('getOrder.post', $this, $args);
            $collection->copyFrom([$order]);
        }
        return $collection;
    }

    public function getOrdersWithoutPageBehaviorHandling() {
        if ($this->events()->getListeners('getOrders')->count() != 1) {
            throw new \RuntimeException('Orders getting event callback isn\'t defined');
        }
        $args = $this->events()->prepareArgs([
            'orderService' => $this,
        ]);
        $this->events()->trigger('getOrders', $this, $args);

        $postListeners = $this->events()->getListeners('getOrders.post');
        foreach ($postListeners as $listener) {
            if ($listener->getMetadatum('priority') == 2) {
                $this->events()->detach($listener);
                $this->events()->trigger('getOrders.post', $this, $args);
                $this->events()->attach('getOrders.post', $listener->getCallback(), 2);
                return (isset($args['collection'])?$args['collection']:null);
            }
        }
        return (isset($args['collection'])?$args['collection']:null);
    }

    public function getOrder(array $params) {
        $behaviors = $this->ordersGettingBehaviors();
        /*@var $bh \SoloOrder\Service\Behavior\Order\SelectOrderByParametersBehavior */
        foreach ($behaviors as $bh) {
            if ($bh->applicable($params)) {
                $this->events()->clearListeners('getOrder.pre');
                $this->events()->attach('getOrder.pre', $bh->orderSelectCallback());
                break;
            }
        }
        if ($this->events()->getListeners('getOrder.pre')->count() == 0) {
            throw new \RuntimeException('Order setting require event listener. Check URL parameters or existing handlers');
        }
        $args = $this->events()->prepareArgs(['queryParams' => $params]);
        $this->events()->trigger('getOrder.pre', $this, $args);
        $this->events()->trigger('getOrder.post', $this, $args);
        $order = (isset($args['order'])?$args['order']:null);
        return $order;
    }

    /**
     *
     * @param integer $reserveId
     * @return \SoloOrder\Service\Ultima\Result\GetReserveAmountResult
     */
    public function getReserveAmount($reserveId) {
        $wm = $this->getReserveAmountWebMethod();
        $wm->addPar('Id', $reserveId);
        $response = $wm->call();
        $result = new Result\GetReserveAmountResult();
        $result->setData($response->getData());
        if ($response->hasError()) {
            $result->setError($response->getError());
        }
        return $result;
    }

    /**
     *
     * @param integer $reserveId
     * @return \SoloOrder\Service\Result\PayReserve
     */
    public function payReserve($reserveId) {
        $wm = $this->getPayReserveWebMethod();
        $wm->addPar('Id', $reserveId);
        $response = $wm->call();
        $result = new PayReserveResult();
        $result->setData($response->getData());
        if ($response->hasError()) {
            $result->setError($response->getError());
        } else {
            $rdr = new UltimaJsonListReader($response);
            $result->success($rdr->getValue('Success'));
        }
        $result->setOriginalRequest(json_encode($wm->getPars()));
        $result->setOriginalResponse($response->getResponse()->getContent());
        return $result;
    }

    public function changeReservePaymentMethod($reserveId, $paymentMethodId) {
        $wm = $this->getWebMethod('ChangeReservePaymentMethod');
        $wm->addPar('Id', $reserveId);
        $wm->addPar('PaymentTypeId', $paymentMethodId);

        $response = $wm->call();
        $result = new Result();
        $result->setData($response->getData());
        if ($response->hasError()) {
            $result->setError($response->getError());
        } else {
            $rdr = new UltimaJsonListReader($response);
            $result->success($rdr->getValue('Success'));
        }
        $result->setOriginalRequest(json_encode($wm->getPars()));
        $result->setOriginalResponse($response->getResponse()->getContent());

        return $result;
    }

    /**
     *
     * @param integer $reserveId
     * @return integer
     */
    public function setReserveId($reserveId) {
        $orderSession = $this->getLocalStorage();
        $orderSession->setId($reserveId);
        return $reserveId;
    }

    /**
     *
     * @param integer $storeId
     * @return integer
     */
    public function setStoreId($storeId) {
        $orderSession = $this->getLocalStorage();
        $orderSession->setStoreId($storeId);
        return $storeId;
    }

    /**
     *
     * @param string $phone
     * @return string
     */
    public function setPhone($phone) {
        $orderSession = $this->getLocalStorage();
        $orderSession->setPhone($phone);
        return $phone;
    }

    /**
     *
     * @param string $phone
     * @return string
     */
    public function setComment($comment) {
    	$orderSession = $this->getLocalStorage();
    	$orderSession->setComment($comment);
    	return $comment;
    }
    
    /**
     *
     * @param string $phone
     * @return string
     */
    public function setDeliveryComment($comment) {
    	$orderSession = $this->getLocalStorage();
    	$orderSession->setDeliveryComment($comment);
    	return $comment;
    }
    
    /**
     *
     * @param string $phone
     * @return string
     */
    public function setSource($source) {
    	$orderSession = $this->getLocalStorage();
    	$orderSession->setSource($source);
    	return $source;
    }

    /**
     *
     * @param integer $managerId
     * @return integer
     */
    public function setManagerId($managerId) {
    	$orderSession = $this->getLocalStorage();
    	$orderSession->setManagerId($managerId);
    	return $managerId;
    }

    /**
     *
     * @param string $userName
     * @return string
     */
    public function setUserName($userName) {
        $orderSession = $this->getLocalStorage();
        $orderSession->setUserName($userName);
        return $userName;
    }

	/**
     *
     * @param string $userEmail
     * @return string
     */
    public function setUserEmail($userEmail) {
        $orderSession = $this->getLocalStorage();
        $orderSession->setUserEmail($userEmail);
        return $userEmail;
    }

    /**
     *
     * @param \DateTime $date
     * @return \DateTime
     */
    public function setPickupDate($date) {
        $orderSession = $this->getLocalStorage();
        $orderSession->setPickupDate($date);
        return $date;
    }

    /**
     *
     * @param integer $paymentId
     * @return integer
     */
    public function setPaymentId($paymentId) {
        $orderSession = $this->getLocalStorage();
        $orderSession->setPaymentId($paymentId);
        return $paymentId;
    }

    /**
     *
     * @param string $paymentTerm
     * @return string
     */
    public function setYandexKassaPaymentTerm($paymentTerm) {
        $orderSession = $this->getLocalStorage();
        $orderSession->setYandexKassaPaymentTerm($paymentTerm);
        return $paymentTerm;
    }

    /**
     *
     * @return void
     */
    public function clearYandexKassaPayment() {
        $orderSession = $this->getLocalStorage();
        $orderSession->setYandexKassaPaymentTerm(null);
    }

    /**
     *
     * @param float $amount
     * @return float
     */
    public function setAmount($amount) {
        $orderSession = $this->getLocalStorage();
        $orderSession->setAmount($amount);
        return $amount;
    }

    /**
     *
     * @param array $notAvailGoods
     * @return array
     */
    public function setNotAvailGoods($notAvailGoods) {
        $orderSession = $this->getLocalStorage();
        $orderSession->setNotAvailGoods($notAvailGoods);
        return $notAvailGoods;
    }

    /**
     *
     * @param integer $agentId
     * @return integer
     */
    public function setAgentId($agentId) {
        $orderSession = $this->getLocalStorage();
        $orderSession->setAgentId($agentId);
        $orderSession->setAgent(null);
        return $agentId;
    }

    public function setAgent($agent) {
        $orderSession = $this->getLocalStorage();
        $orderSession->setAgent($agent);
        $orderSession->setAgentId(null);
        return $agent;
    }

    /**
     *
     * @param float $bonusAmount
     * @return float
     */
    public function setBonusAmount($bonusAmount) {
        $orderSession = $this->getLocalStorage();
        $orderSession->setBonusAmount($bonusAmount);
        return $bonusAmount;
    }

    /**
     *
     * @param integer $cityId
     * @return integer
     */
    public function setCityId($cityId) {
        $orderSession = $this->getLocalStorage();
        $orderSession->setCityId($cityId);
        return $cityId;
    }

    public function setObtainMethod($obtainMethod) {
        $orderSession = $this->getLocalStorage();
        $orderSession->setObtainMethod($obtainMethod);
    }

    public function setPayerPerson($payerPerson) {
        $orderSession = $this->getLocalStorage();
        $orderSession->setPayerPerson($payerPerson);
    }
    
    public function getPayerPerson() {
    	$orderSession = $this->getLocalStorage();
    	$orderSession->getPayerPerson();
    }

    public function setDelivery($address, $deliveryCost, $timeId, $date, $liftToTheFloor, $floor, $elevatorTypeId) {
        $orderSession = $this->getLocalStorage();

		if (is_numeric($address)) {
            $orderSession->setDeliveryAddressId($address);
            $orderSession->setDeliveryAddress(null);
        } else {
            $orderSession->setDeliveryAddress($address);
            $orderSession->setDeliveryAddressId(null);
        }

        $orderSession->setDeliveryCost($deliveryCost);
        $orderSession->setDeliveryTimeId($timeId);
        $orderSession->setDeliveryDate($date);
        $orderSession->setDeliveryLiftToTheFloor($liftToTheFloor);
        $orderSession->setDeliveryFloor($floor);
        $orderSession->setDeliveryElevatorTypeId($elevatorTypeId);

        return true;
    }

    /**
     *
     * @param integer $addressId
     * @param integer $transportCompanyErpId
     * @param string $transportCompanyUid
     * @param float $cost
     * @param float $twinLogisticCost
     * @param array $addressInfo
     * @param string $phone
     * @param string $userName
     * @param integer $fakeDeliveryTimeId
     * @param \DateTime $deliveryDate
     * @param integer $minDeliveryDate
     * @param integer $maxDeliveryDate
     * @return boolean
     */
    public function setPostDelivery($addressId, $transportCompanyErpId, $transportCompanyUid, $cost, $twinLogisticCost, $addressInfo, $phone, $userName, $fakeDeliveryTimeId, $deliveryDate, $minDeliveryDate = null, $maxDeliveryDate = null) {
        $orderSession = $this->getLocalStorage();
        $orderSession->setDeliveryAddressId($addressId);
        $orderSession->setDeliveryCost($cost);
        $orderSession->setTwinLogisticCost($twinLogisticCost);
        $orderSession->setDeliveryInfo($addressInfo);
        $orderSession->setDeliveryPhone($phone);
        $orderSession->setDeliveryTransportCompanyId($transportCompanyUid);
        $orderSession->setDeliveryTransportCompanyErpId($transportCompanyErpId);
        $orderSession->setFakeDeliveryTimeId($fakeDeliveryTimeId);
        $orderSession->setUserName($userName);
        $orderSession->setPostDeliveryMinDeliveryDate($minDeliveryDate);
        $orderSession->setPostDeliveryMaxDeliveryDate($maxDeliveryDate);
        $orderSession->setDeliveryDate($deliveryDate);
        return true;
    }

    /**
     *
     * @param integer $addressId
     * @param integer $cost
     * @param integer $twinLogisticCost
     * @param string $addressInfo
     * @param string $phone
     * @param string $userName
     * @param integer $minDeliveryDate
     * @param integer $maxDeliveryDate
     * @param integer $fakeDeliveryTimeId
     * @param string $deliveryDate
     * @param integer|null $transportCompanyErpId
     */
    public function setPostPickup($addressId, $cost, $twinLogisticCost, $addressInfo, $phone, $userName, $minDeliveryDate, $maxDeliveryDate, $fakeDeliveryTimeId, $deliveryDate, $transportCompanyErpId = 6) {
        $orderSession = $this->getLocalStorage();
        $orderSession->setDeliveryAddressId($addressId);
        $orderSession->setDeliveryCost($cost);
        $orderSession->setTwinLogisticCost($twinLogisticCost);
        $orderSession->setDeliveryInfo($addressInfo);
        $orderSession->setDeliveryPhone($phone);
        $orderSession->setDeliveryTransportCompanyErpId($transportCompanyErpId);
        $orderSession->setFakeDeliveryTimeId($fakeDeliveryTimeId);
        $orderSession->setUserName($userName);
        $orderSession->setPostDeliveryMinDeliveryDate($minDeliveryDate);
        $orderSession->setPostDeliveryMaxDeliveryDate($maxDeliveryDate);
        $orderSession->setDeliveryDate($deliveryDate);
        return true;
    }

    public function clearStorage() {
        return $this->getLocalStorage()->clearStorage();
    }

    public function isStepAvailable($step) {
        $result = false;
        $orderSession = $this->getLocalStorage();

        switch ($step) {
            case 'step2' :
                if ($orderSession->getObtainMethod() == Order::DELIVERY_OBTAIN_METHOD) {
                    $result = $orderSession->getDeliveryAddressId() || $orderSession->getDeliveryAddress();
                } else {
                    $result = $orderSession->hasPhone();
                }
                break;
            case 'step3':
                $result = $this->isStepAvailable('step2');

                if ($result) {
                    if ($orderSession->getObtainMethod() == Order::PICKUP_OBTAIN_METHOD) {
                        $result = false;
                    } else {
                        if ($orderSession->getPayerPerson() == Order::PAYER_LEGAL_PERSON) {
                            $result = $orderSession->hasAgent() || $orderSession->hasAgentId();
                        } else {
                            $result = $orderSession->hasPaymentId();
                        }
                    }
                }
                break;
            case 'complete':
                $result = $orderSession->getObtainMethod() == Order::DELIVERY_OBTAIN_METHOD
                        ? $this->isStepAvailable('step3') && $orderSession->hasPhone()
                        : $this->isStepAvailable('step2');
                break;
        }

        return $result;
    }

    public function getReserveInfo($reserveId) {
        $wm = $this->getWebMethod('GetReserveInfo');
        $wm->addPar('Id', $reserveId);

        $response = $wm->call();

        $result = new Result();
        if ($response->hasError()) {
            $result->setError($response->getError());
        } else {
            $rdr = new UltimaObjectReader($response);
            $result->Amount = $rdr->Amount;
            $result->DeliveryAddressId = $rdr->Delivery->AddressId;

        }
        return $result;
    }
}
