<?php

namespace SoloOrder\Service\Behavior\Good;

/**
 * Description of GroupHandler
 *
 * @author slava
 */
class GroupHandler extends \Solo\ServiceManager\ServiceLocatorAwareService implements GroupHandlerInterface, \Zend\EventManager\EventManagerAwareInterface {

    use \Solo\EventManager\ProvidesEvents;

    protected $groupByCallback = null;
    protected $compareCallback = null;

    /**
     *
     * @var \SoloOrder\Service\BasketService
     */
    protected $basket = null;

    public function basket(\SoloOrder\Service\BasketService $service = null) {
        if ($service !== null) {
            $this->basket = $service;
            return;
        }
        return $this->basket;
    }

    public function getCompareCallback() {
        return $this->compareCallback;
    }

    public function setCompareCallback($compareCallback) {
        $this->compareCallback = $compareCallback;
    }

    public function getGroupByCallback() {
        return $this->groupByCallback;
    }

    public function setGroupByCallback(\Closure $groupByCallback) {
        $this->groupByCallback = $groupByCallback;
    }

    public function getGroupBehaviors() {
        return $this->getServiceLocator()->get('GroupSelectBehavior');
    }

    public function getGroups(\SoloOrder\Entity\Basket\GoodsCollection $goodsCollection, array $eventListeners = null) {
        $resultCollection = new \SoloOrder\Entity\Basket\GroupCollection;
        $groups = \Solo\Stdlib\ArrayHelper::array_group($goodsCollection->toArray(), $this->getGroupByCallback());
        foreach ($groups as $group) {
            $newGroup = $this->basket()->newGroup();
            foreach ($eventListeners as $listener) {
                $newGroup->events()->attach($listener['name'], $listener['callback'], $listener['priority']);
            }
            $collection = new \SoloOrder\Entity\Basket\GoodsCollection;
            $collection->copyFrom($group);
            $newGroup->setGoods($collection);
            $resultCollection->add($newGroup);
        }
        return $resultCollection;
    }

    public function getGroup($queryParams) {
        $behaviors = $this->getGroupBehaviors();
        /* @var $bh \SoloBasket\Service\Behavior\Group\SelectGroupByParametersBehavior */
        foreach ($behaviors as $bh) {
            if ($bh->applicable($queryParams)) {
                $this->attachGetGroupListener($bh);
                break;
            }
        }
        if ($this->events()->getListeners('getGroup')->count() != 1) {
            throw new \RuntimeException('Event listener for getOrder event did not define or query parameters are invalid');
        }
        $args = $this->events()->prepareArgs(['queryParams' => $queryParams]);
        $this->events()->trigger('getGroup', $this, $args);
        $group = isset($args['group']) ? $args['group'] : null;
        $this->attachDefaultGetGroupListener();
        return $group;
    }

    public function attachDefaultGetGroupListener() {
        $this->detachGetGroupListener();
        $this->events()->attach('getGroup', $this->getServiceLocator()->get('\SoloOrder\Service\Behavior\Group\SelectGroupByDefault')->groupSelectCallback());
    }

    public function attachGetGroupListener(\SoloOrder\Service\Behavior\Group\SelectGroupByParametersBehavior $behavior) {
        $this->detachGetGroupListener();
        $this->events()->attach('getGroup', $behavior->groupSelectCallback());
    }
    
    public function detachGetGroupListener() {
        $this->events()->clearListeners('getGroup');
    }
    
}

?>
