<?php

namespace SoloOrder\Service\Behavior\Good;

use SoloOrder\Entity\Basket\Good;
use SoloOrder\Entity\Basket\Group;
use Solo\DateTime\DateTime;

class RemainsGroupHandler implements GroupHandlerInterface {

	public function getGroups(\SoloOrder\Entity\Basket\GoodsCollection $goodsCollection) {
		$groupCollection = new \SoloOrder\Entity\Basket\GroupCollection();
		
		$minDateTimestamp = PHP_INT_MAX;
		$maxDateTimestamp = 0;
		foreach ($goodsCollection as $good) {
			$pickupDate = \DateTime::createFromFormat('Y-m-d', $good->getPickupDate());
			if ($pickupDate instanceof \DateTime) {
				$minDateTimestamp = min($minDateTimestamp, $pickupDate->getTimestamp());
				$maxDateTimestamp = max($maxDateTimestamp, $pickupDate->getTimestamp());
			}
		}
		
		foreach ($goodsCollection as $good) {
			switch ($good->getPickupMethodId()) {
				case Good::PICKUP_METHOD_MIN_MAX:
					if (!$good->getGroupId()) {
						$pickupTimestamp = strtotime($good->getPickupDate());
						$groupId = 0;
							
						if (date('Y-m-d', $pickupTimestamp) == date('Y-m-d', $minDateTimestamp)) {
							$groupId = $minDateTimestamp;
						} else {
							$groupId = $maxDateTimestamp;
						}
						
						$good->setGroupId($groupId);
					}
						
					break;
			}
		}
		
		/**
		 *
		 * @var Good $good
		 */
		foreach ($goodsCollection as $good) {
			
			switch ($good->getPickupMethodId()) {
				case Good::PICKUP_METHOD_PARTIAL:
					$currentDate = new \DateTime();
					$currentDate->setTime(0, 0, 0);
					
					$currentDateTimestamp = $currentDate->getTimestamp();
					
					$groupId = $currentDateTimestamp;
					
					if (!$groupCollection->keyExists($groupId)) {
						$group = new Group($groupId);
						$groupCollection->add($group, $groupId);
					} else {
						$group = $groupCollection->offsetGet($groupId);
					}
					
					$availQuantity = $good->getQuantity() - $good->getSuborderQuantity();
					
					if ($availQuantity > 0) {
						$goodCopy = clone $good;
						
						$goodCopy->setQuantity($availQuantity);
						$group->addGood($goodCopy);
					} else {
						$group->removeGood($good->getId());
					}
					
					$groupId = strtotime($good->getPickupDate());
					
					if (!$groupCollection->keyExists($groupId)) {
						$group = new Group($groupId);
						$groupCollection->add($group, $groupId);
					} else {
						$group = $groupCollection->offsetGet($groupId);
					}
					
					$goodCopy = clone $good;
					$goodCopy->setQuantity($good->getSuborderQuantity());
					
					$group->addGood($goodCopy);
					
					break;
				case Good::PICKUP_METHOD_REMAIN:
					$currentDate = new \DateTime();
					$currentDate->setTime(0, 0, 0);
					
					$currentDateTimestamp = $currentDate->getTimestamp();
					
					$groupId = $currentDateTimestamp;
					
					if (!$groupCollection->keyExists($groupId)) {
						$group = new Group($groupId);
						$groupCollection->add($group, $groupId);
					} else {
						$group = $groupCollection->offsetGet($groupId);
					}
					
					$goodCopy = clone $good;
					$goodCopy->setQuantity($good->getQuantity() - $good->getSuborderQuantity());
					
					$group->addGood($goodCopy);
					
					break;
				case Good::PICKUP_METHOD_ALL:
					$groupId = strtotime($good->getPickupDate());
					
					if (!$groupCollection->keyExists($groupId)) {
						$group = new Group($groupId);
						$groupCollection->add($group, $groupId);
					} else {
						$group = $groupCollection->offsetGet($groupId);
					}
					
					$group->addGood(clone $good);
					break;
				
				case Good::PICKUP_METHOD_WHOLE:
					$groupId = $maxDateTimestamp;
					
					if (!$groupCollection->keyExists($groupId)) {
						$group = new Group($groupId);
						$groupCollection->add($group, $groupId);
					} else {
						$group = $groupCollection->offsetGet($groupId);
					}
						
					$group->addGood(clone $good);
					
					break;
				
				case Good::PICKUP_METHOD_MIN_MAX:
					$pickupTimestamp = strtotime($good->getPickupDate());
					$groupId = $good->getGroupId();
						
					if (!$groupCollection->keyExists($groupId)) {
						$group = new Group($groupId);
						$groupCollection->add($group, $groupId);
					} else {
						$group = $groupCollection->offsetGet($groupId);
					}
					
					$group->addGood(clone $good);
					
					break;
			}
		}
		
		foreach ($groupCollection as $key => $group) {
			if ($group->isEmpty()) {
				$groupCollection->removeAt($key);
			}
		}
		
// 		foreach ($groupCollection as $key => $group) {
// 			var_dump($key);
// 			foreach ($group->getGoods() as $good) {
// 				print $good->getId()."\n";
// 			}
// 			print "\n----------\n";
// 		}
// 		exit();
		
		return $groupCollection;
	}

}