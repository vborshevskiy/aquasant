<?php
namespace SoloOrder\Service\Behavior\Good;

/**
 * Description of AbstractGroupHandler
 *
 * @author slava
 */
interface GroupHandlerInterface {
    
    public function getGroups(\SoloOrder\Entity\Basket\GoodsCollection $goodsCollection);
    
}

?>
