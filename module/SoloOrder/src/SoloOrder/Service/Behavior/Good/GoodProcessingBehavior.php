<?php
namespace SoloOrder\Service\Behavior\Good;
/**
 *
 * @author slava
 */
interface GoodProcessingBehavior {
    
    public function addGood(\SoloOrder\Entity\Basket\AbstractGood $basketGood, \SoloOrder\Entity\Basket\AbstractStorage &$basketStorage);
    public function removeGood(\SoloOrder\Entity\Basket\AbstractGood $basketGood, \SoloOrder\Entity\Basket\AbstractStorage &$storage);
    public function updateGood(\SoloOrder\Entity\Basket\AbstractGood $basketGood, \SoloOrder\Entity\Basket\AbstractStorage &$storage);
    
}

?>
