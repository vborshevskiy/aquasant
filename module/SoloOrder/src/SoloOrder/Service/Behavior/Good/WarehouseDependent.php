<?php
namespace SoloOrder\Service\Behavior\Good;
/**
 * Description of WarehouseDependent
 *
 * @author slava
 */
class WarehouseDependent implements GoodProcessingBehavior {
    
    public function addGood(\SoloOrder\Entity\Basket\AbstractGood $basketGood, \SoloOrder\Entity\Basket\AbstractStorage &$storage) {
        $collection = $storage->getGoodsCollection();
        $existingGoods = $storage->getGoodsById($basketGood->getId());
        if ($existingGoods !== null) {
            $warehouseId = $basketGood->getWarehouseId();
            foreach ($existingGoods as $key => $existingGood) {
                $exWarehouseId = $existingGood->getWarehouseId();
                if (
                        (!$basketGood->hasDelivery() && !$existingGood->hasDelivery() && $warehouseId == $exWarehouseId) ||
                        ($basketGood->hasDelivery() && $existingGood->hasDelivery() && $basketGood->getDelivery()->getAddressId() == $existingGood->getDelivery()->getAddressId() &&
                        $basketGood->getDelivery()->getDeliveryTime() == $existingGood->getDelivery()->getDeliveryTime() &&
                        $basketGood->getDelivery()->getDeliveryDate() == $existingGood->getDelivery()->getDeliveryDate())
                ) {
                    $newQuantity = $existingGood->getQuantity()+$basketGood->getQuantity();
                    $collection->remove($existingGood);
                    $existingGood->setQuantity($newQuantity);
                    $collection->add($existingGood);
                    $storage->setGoodsCollection($collection);
                    return;
                }
            }
        }
        $collection->add($basketGood);
        $storage->setGoodsCollection($collection);
        return;
    }
    
    public function removeGood(\SoloOrder\Entity\Basket\AbstractGood $basketGood, \SoloOrder\Entity\Basket\AbstractStorage &$storage) {
        $collection = $storage->getGoodsCollection();
        foreach ($collection as $index => $bg) {
            if (
                    $basketGood->getId() == $bg->getId() && 
                    (
                            (!$basketGood->hasDelivery() && !$bg->hasDelivery() && $basketGood->getWarehouseId() == $bg->getWarehouseId()) ||
                            ($basketGood->hasDelivery() && $bg->hasDelivery() && $basketGood->getDelivery()->getAddressId() == $bg->getDelivery()->getAddressId() &&
                            $basketGood->getDelivery()->getDeliveryTime() == $bg->getDelivery()->getDeliveryTime() &&
                            $basketGood->getDelivery()->getDeliveryDate() == $bg->getDelivery()->getDeliveryDate())
                    )
            ) {
                unset($collection[$index]);
                $storage->setGoodsCollection($collection);
                return;
            }
        }
    }
    
    public function updateGood(\SoloOrder\Entity\Basket\AbstractGood $basketGood, \SoloOrder\Entity\Basket\AbstractStorage &$storage) {
        /*@var $collection \SoloBasket\Entity\GoodsCollection */
        $collection = $storage->getGoodsCollection();
        foreach ($collection as $index => $bg) {
            if (
                    $basketGood->getId() == $bg->getId() &&
                    (
                            (!$basketGood->hasDelivery() && !$bg->hasDelivery() && $basketGood->getWarehouseId() == $bg->getWarehouseId()) || 
                            ($basketGood->hasDelivery() && $bg->hasDelivery() && $basketGood->getDelivery()->getAddressId() == $bg->getDelivery()->getAddressId() &&
                            $basketGood->getDelivery()->getDeliveryTime() == $bg->getDelivery()->getDeliveryTime() &&
                            $basketGood->getDelivery()->getDeliveryDate() == $bg->getDelivery()->getDeliveryDate())
                            
                    )
            ) {
                $collection->offsetUnset($index);
                $collection->offsetSet($index, $basketGood);
                $storage->setGoodsCollection($collection);
                return;
            }
        }
    }
    
}

?>
