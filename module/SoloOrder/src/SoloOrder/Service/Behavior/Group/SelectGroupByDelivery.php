<?php
namespace SoloOrder\Service\Behavior\Group;
/**
 * Description of SelectGroupByDelivery
 *
 * @author slava
 */
class SelectGroupByDelivery extends \Solo\ServiceManager\ServiceLocatorAwareService implements SelectGroupByParametersBehavior {
    
    public function applicable($params) {
        return (
                is_array($params) && (array_key_exists('delivery', $params) && array_key_exists('date', $params) && array_key_exists('time', $params)) ||
                $params instanceof \SoloOrder\Entity\Basket\Delivery
        );
    }

    public function groupSelectCallback() {
        return function($e) {
            $basketService = $e->getTarget()->getServiceLocator()->get('basket_service');
            $groups = $basketService->getGroups();
            $queryParams = $e->getParam('queryParams');
            if (is_array($queryParams)) {
                $deliveryAddressId = intval($queryParams['delivery']);
                $deliveryDate = intval($queryParams['date']);
                $deliveryTime = intval($queryParams['time']);
            } elseif ($queryParams instanceof \SoloBasket\Entity\Delivery) {
                $deliveryAddressId = intval($queryParams->getAddressId());
                $deliveryDate = intval($queryParams->getDeliveryDate());
                $deliveryTime = intval($queryParams->getDeliveryTime());
            }
            foreach ($groups as $basketGroup) {
                $delivery = $basketGroup->getDelivery();
                if (
                        $delivery &&
                        (int)$delivery->getAddressId() == $deliveryAddressId &&
                        (int)$delivery->getDeliveryDate() == $deliveryDate &&
                        (int)$delivery->getDeliveryTime() == $deliveryTime
                ) {
                    $e->setParam('group', $basketGroup);
                    return;
                }
            }
        };
    }
    
    public function getGroupParamValue($params) {
        if (is_array($params) && (array_key_exists('delivery', $params) && array_key_exists('date', $params) & array_key_exists('time', $params))) {
            $delivery = $this->getServiceLocator()->get('delivery_service')->newDelivery();
            $delivery->setAddressId(intval($params['delivery']));
            $delivery->setDeliveryDate(intval($params['date']));
            $delivery->setDeliveryTime(intval($params['time']));
            return $delivery;
        } elseif ($params instanceof \SoloBasket\Entity\Delivery) {
            return $delivery;
        }
        throw new \InvalidArgumentException("Invalid parameters for getting delivery");
    }
    
}

?>
