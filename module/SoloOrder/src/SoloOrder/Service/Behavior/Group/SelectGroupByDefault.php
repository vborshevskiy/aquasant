<?php
namespace SoloOrder\Service\Behavior\Group;
/**
 * Description of SelectGroupByDefault
 *
 * @author slava
 */
class SelectGroupByDefault implements SelectGroupByParametersBehavior {
    
    public function applicable($params) {
        return true;
    }

    public function groupSelectCallback() {
        return function($e) {
            $basketService = $e->getTarget()->getServiceLocator()->get('basket_service');
            $groups = $basketService->getGroups();
            if ($groups->count() > 0) {
                $e->setParam('group', $groups->current());
            }
        };
    }
    
    public function getGroupParamValue($params) {
        return null;
    }
    
}

?>
