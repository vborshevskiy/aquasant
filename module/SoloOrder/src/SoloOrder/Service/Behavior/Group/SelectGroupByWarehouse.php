<?php
namespace SoloOrder\Service\Behavior\Group;
/**
 * Description of SelectGroupByWarehouse
 *
 * @author slava
 */
class SelectGroupByWarehouse extends \Solo\ServiceManager\ServiceLocatorAwareService implements SelectGroupByParametersBehavior {
    
    public function applicable($params) {
        return (
                is_array($params) && (array_key_exists('w', $params) || array_key_exists('warehouse', $params) || array_key_exists('w', $params)) ||
                is_integer($params)
        );
    }

    public function groupSelectCallback() {
        return function($e) {
            $basketService = $e->getTarget()->getServiceLocator()->get('basket_service');
            $groups = $basketService->getGroups();
            $queryParams = $e->getParam('queryParams');
            $warehouseId = null;
            if (is_array($queryParams)) {
                if (array_key_exists('w', $queryParams)) {
                    $warehouseId = intval($queryParams['w']);
                } elseif (array_key_exists('wh', $queryParams)) {
                    $warehouseId = intval($queryParams['wh']);
                } elseif (array_key_exists('warehouse', $queryParams)) {
                    $warehouseId = intval($queryParams['warehouse']);
                } 
            } elseif (is_integer($queryParams)) {
                $warehouseId = $queryParams;
            } else {
                throw new \InvalidArgumentException('Query parameters are invalid for getting basket goods group by warehouse identifier');
            }
            foreach ($groups as $basketGroup) {
                if ((int)$basketGroup->getWarehouseId() == $warehouseId) {
                    $e->setParam('group', $basketGroup);
                    return;
                }
            }
        };
    }
    
    public function getGroupParamValue($params) {
        if (is_array($params)) {
            if (array_key_exists('w', $params)) {
                return (int)$params['w'];
            } elseif (array_key_exists('wh', $params)) {
                return (int)$params['wh'];
            } elseif (array_key_exists('warehouse', $params)) {
                return (int)$params['warehouse'];
            } else {
                throw new \InvalidArgumentException('Invalid parameters for getting warehouse identifier');
            }
        } elseif (is_integer($params)) {
            return $params;
        } else {
            throw new \InvalidArgumentException('Invalid parameters for getting warehouse identifier');
        }
    }
    
}

?>
