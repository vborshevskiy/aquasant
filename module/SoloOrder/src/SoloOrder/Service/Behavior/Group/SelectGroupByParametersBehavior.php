<?php
namespace SoloOrder\Service\Behavior\Group;
/**
 *
 * @author slava
 */
interface SelectGroupByParametersBehavior {
    
    public function applicable($params);
    public function groupSelectCallback();
    public function getGroupParamValue($params);
    
}

?>
