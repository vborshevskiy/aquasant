<?php
namespace SoloOrder\Service\Behavior\Order;
/**
 * Description of SelectOrderByDelivery
 *
 * @author slava
 */
class SelectOrderByDelivery extends \Solo\ServiceManager\ServiceLocatorAwareService implements SelectOrderByParametersBehavior {
    
    public function applicable(array $params) {
        return (sizeof($params) >= 3 && (array_key_exists('delivery', $params) && array_key_exists('date', $params) && array_key_exists('time', $params)));
    }

    public function orderSelectCallback() {
        return function($e) {
            $orderService = $e->getTarget()->getServiceLocator()->get('order_service');
            $queryParams = $e->getParam('queryParams');
            $deliveryAddressId = null;
            $deliveryDate = null;
            $deliveryTime = null;
            if (array_key_exists('delivery', $queryParams) & array_key_exists('date', $queryParams) && array_key_exists('time', $queryParams)) {
                $deliveryAddressId = intval($queryParams['delivery']);
                $deliveryDate = intval($queryParams['date']);
                $deliveryTime = intval($queryParams['time']);
            } else {
                throw new \InvalidArgumentException('Invalid query params for getting order by delivery!');
            }
            $ordersCollection = $orderService->getOrdersWithoutPageBehaviorHandling();
            foreach ($ordersCollection as $order) {
                $orderDefiner = $order->settings()->getOrderDefinition();
                $delivery = $orderDefiner->define('delivery');
                if (
                        $delivery !== null &&
                        intval($delivery['addressId']) == $deliveryAddressId &&
                        intval($delivery['date']) == $deliveryDate &&
                        intval($delivery['time']) == $deliveryTime
                ) {
                    $e->setParam('order', $order);
                    break;
                }
            }
        };
    }
    
}

?>
