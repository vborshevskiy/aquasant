<?php
namespace SoloOrder\Service\Behavior\Order;

/**
 *
 * @author slava
 */
interface SelectOrderByParametersBehavior {
    
    public function applicable(array $params);
    public function orderSelectCallback();
    
}

?>
