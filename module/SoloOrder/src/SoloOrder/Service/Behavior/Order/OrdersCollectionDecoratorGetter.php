<?php
namespace SoloOrder\Service\Behavior\Order;

/**
 * Description of OrderDecoratorGetter
 *
 * @author slava
 */
class OrdersCollectionDecoratorGetter extends \Solo\ServiceManager\ServiceLocatorAwareService implements OrdersCollectionDecoratorGetterBehavior {
    
    public function ordersDecoratorCallback() {
        return function ($e) {
            /*@var $ordersCollection \SoloOrder\Entity\Order\OrdersCollection  */
            $ordersCollection = $e->getParam('collection');
            /*@var $newCollection \SoloOrder\Entity\Order\OrdersCollection  */
            $newCollection = clone $ordersCollection;
            $newCollection->clear();
            /*@var $orderService \SoloOrder\Service\Ultima\OrderService */
            $orderService = $e->getParam('orderService');
            $list = $orderService->getSettingsCollection();
            foreach ($ordersCollection as $index => $order) {
                        /* @var $delivery \SoloOrder\Entity\Ultima\Delivery */
                $delivery = $order->getDelivery();
                $warehouseId = $order->getWarehouseId();
                $definition = $orderService->newDefinition();
                if ($delivery === null) {
                    $definition->define('warehouseId', $warehouseId);
                    $definition->define('delivery', null);
                } else {
                    $definition->define('warehouseId', null);
                    $definition->define('delivery', [
                        'addressId' => $delivery->getAddressId(),
                        'date' => $delivery->getDeliveryDate(),
                        'time' => $delivery->getDeliveryTime(),
                        'info' => $delivery->getDeliveryInfo(),
                    ]);
                }
                $settings = $orderService->getSettingsGroup($definition);
                $order->settings($settings);
                $newCollection->add($order);
            }
            $e->setParam('collection', $ordersCollection);
        };
    }
    
}

?>
