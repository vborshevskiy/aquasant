<?php
namespace SoloOrder\Service\Behavior\Order;

/**
 *
 * @author slava
 */
interface OrdersCollectionDecoratorGetterBehavior {
    
    public function ordersDecoratorCallback();
    
}

?>
