<?php
namespace SoloOrder\Service\Behavior\Order;

/**
 *
 * @author slava
 */
interface SelectOrdersForShowingOnPageBehavior {
    
    public function getOrdersForShowingCallback();
    
}

?>
