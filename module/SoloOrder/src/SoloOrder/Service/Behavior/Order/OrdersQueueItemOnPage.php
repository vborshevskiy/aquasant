<?php
namespace SoloOrder\Service\Behavior\Order;
/**
 * Description of OrdersQueueItemOnPage
 *
 * @author slava
 */
class OrdersQueueItemOnPage implements SelectOrdersForShowingOnPageBehavior {
    
    public function getOrdersForShowingCallback() {
        return function($e) {
            $orders = $e->getTarget();
            $ordersCount = $orders->count();
            foreach ($orders as $index => $order) {
                $orderSettings = $order->settings();
                if (!$orderSettings->setting('processed')) {
                    $orders->clear();
                    $orders->add($order);
                    break;
                }
            }
            $e->setTarget($order);
        };
    }
    
}

?>
