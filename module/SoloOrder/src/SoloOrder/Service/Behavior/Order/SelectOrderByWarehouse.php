<?php
namespace SoloOrder\Service\Behavior\Order;

/**
 * Description of SelectOrderByWarehouse
 *
 * @author slava
 */
class SelectOrderByWarehouse extends \Solo\ServiceManager\ServiceLocatorAwareService  implements SelectOrderByParametersBehavior {
    
    public function applicable(array $params) {
        if (sizeof($params) > 0 && (array_key_exists('w', $params) || array_key_exists('wh', $params) || array_key_exists('warehouse', $params))) {
            return true;
        }
        return false;
    }

    public function orderSelectCallback() {
        return function($e) {
            $orderService = $e->getTarget()->getServiceLocator()->get('order_service');
            $queryParams = $e->getParam('queryParams');
            $warehouse = null;
            if (array_key_exists('wh', $queryParams)) {
                $warehouse = intval($queryParams['wh']);
            } elseif (array_key_exists('warehouse', $queryParams)) {
                $warehouse = intval($queryParams['warehouse']);
            } elseif (array_key_exists('w', $queryParams)) {
                $warehouse = intval($queryParams['w']);
            } else {
                throw new \InvalidArgumentException('invalid query params for getting order by warehouse identifier!');
            }
            $ordersCollection = $orderService->getOrdersWithoutPageBehaviorHandling();
            foreach ($ordersCollection as $order) {
                $orderDefiner = $order->settings()->getOrderDefinition();
                $wh = (int)$orderDefiner->define('warehouseId');
                if ($wh == $warehouse) {
                    $e->setParam('order', $order);
                    return;
                }
            }
        };
    }
    
}

?>
