<?php
namespace SoloOrder\Service;

use SoloERP\Service\ProvidesWebservice;
use Solo\EventManager\ProvidesEvents;
use Solo\ServiceManager\ServiceLocatorAwareService;

/**
 * AbstractBasketService
 *
 * @author slava
 */
abstract class AbstractBasketService extends ServiceLocatorAwareService implements \Zend\EventManager\EventManagerAwareInterface {

    use ProvidesWebservice;
    use ProvidesEvents;

    /**
     *
     * @var \SoloOrder\Entity\Basket\AbstractBasketStorage
     */
    protected $basketStorage = null;

    public function generateId() {
        return (time() + rand(100, 1000));
    }

    /**
     *
     * @return \SoloOrder\Entity\Basket\AbstractBasketStorage
     */
    public function getBasketStorage() {
        return $this->basketStorage;
    }

    public function setBasketStorage(\SoloOrder\Entity\Basket\AbstractStorage $basketStorage) {
        $this->basketStorage = $basketStorage;
    }

    public function addGood(\SoloOrder\Entity\Basket\AbstractGood $basketGood) {
        $this->getBasketStorage()->addGood($basketGood);
    }

    public function removeGood(\SoloOrder\Entity\Basket\AbstractGood $basketGood) {
        $this->getBasketStorage()->removeGood($basketGood);
    }

    public function updateGood(\SoloOrder\Entity\Basket\AbstractGood $basketGood) {
        $this->getBasketStorage()->updateGood($basketGood);
    }

    public function getGoods() {
        return $this->getBasketStorage()->getGoodsCollection();
    }

    public function enumGoodIds() {
        $goodsCollection = $this->getGoods();
        $ids = array();
        foreach ($goodsCollection as $basketGood) {
            if (!in_array($basketGood->getId(), $ids)) {
                $ids[] = $basketGood->getId();
            }
        }
        return sizeof($ids)>0?$ids:null;
    }

    public function getGoodById($goodId) {
        $goodsCollection = $this->getGoods();
        return $goodsCollection->getGoodById($goodId);
    }

    /**
     * @param \SoloOrder\Entity\Basket\GoodsCollection $goodsCollection
     */
    public function updateBasket(\SoloOrder\Entity\Basket\GoodsCollection $goodsCollection) {
        foreach ($goodsCollection as $basketGood) {
            $this->getBasketStorage()->updateGood($basketGood);
        }
    }

    public function newGood() {
        return $this->getServiceLocator()->get('basket_good_instance');
    }

	public function newComponentGood() {
        return $this->getServiceLocator()->get('basket_component_good_instance');
    }

    public function clearStorage() {
        $this->getBasketStorage()->setGoodsCollection($this->getServiceLocator()->get('basket_order_good_collection'));
    }

}

?>
