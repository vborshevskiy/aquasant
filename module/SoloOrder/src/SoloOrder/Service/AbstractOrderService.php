<?php

namespace SoloOrder\Service;

use SoloERP\Service\ProvidesWebservice;
use Solo\EventManager\ProvidesEvents;
use Solo\ServiceManager\ServiceLocatorAwareService;

/**
 * Description of AbstractOrderService
 *
 * @author slava
 */
abstract class AbstractOrderService extends ServiceLocatorAwareService implements \Zend\EventManager\EventManagerAwareInterface {

    use ProvidesWebservice;
    use ProvidesEvents;

    /**
     *
     * @var \SoloOrder\Entity\AbstractStorage
     */
    private $localStorage = null;
    
    public function getSettingsCollection() {
        return $this->getLocalStorage()->getSettingsCollection();
    }
    
    public function addSettingsGroup(\SoloOrder\Entity\Order\AbstractSettings $settings) {
        $this->getLocalStorage()->addSettingsGroup($settings);
    }
    
    public function updateSettingsGroup(\SoloOrder\Entity\Order\AbstractSettings $settings) {
        $this->getLocalStorage()->updateSettingsGroup($settings);
    }
    
    public function removeSettingsGroup(\SoloOrder\Entity\Order\AbstractSettings $settings) {
        $this->getLocalStorage()->removeSettingsGroup($settings);
    }
    
    public function getSettingsGroup(\SoloOrder\Entity\Order\OrderDefinition $orderDefinition) {
        return $this->getLocalStorage()->getSettingsGroup($orderDefinition);
    }
    
    public function newOrder() {
        return $this->getServiceLocator()->get('order_instance');
    }
    
    public function newOrdersCollection() {
        return $this->getServiceLocator()->get('orders_collection');
    }
    
    public function newGoodsCollection() {
        return $this->getServiceLocator()->get('order_goods_collection');
    }
    
    public function newGood() {
        return $this->getServiceLocator()->get('order_good_instance');
    }
    
    public function newDelivery() {
        return $this->getServiceLocator()->get('order_delivery');
    }
    
    public function newPostDelivery() {
        return $this->getServiceLocator()->get('order_post_delivery');
    }
    
    public function newSettings() {
        return $this->getServiceLocator()->get('order_settings');
    }
    
    public function newDefinition() {
        return $this->getServiceLocator()->get('order_definition');
    }
    
    public function ordersGettingBehaviors() {
        return $this->getServiceLocator()->get('OrderSelectBehaviors');
    }

    public function getLocalStorage() {
        return $this->localStorage;
    }

    public function setLocalStorage(\SoloOrder\Entity\Order\AbstractStorage $localStorage) {
        $this->localStorage = $localStorage;
    }
    
    public function isInitialized() {
        if ($this->events()->getListeners('isOrdersInit')->count() != 1) {
            var_dump($this->events()->getListeners('isOrdersInit')->count());
            throw new \RuntimeException("Event listener for checking orders initializing was not attached or attached too many listeners!");
        }
        $args = $this->events()->prepareArgs([]);
        $this->events()->trigger('isOrdersInit', $this, $args);
        if (array_key_exists('result', $args) && is_bool($args['result'])) {
            return $args['result'];
        }
        throw new \RuntimeException('Orders initialized status checker callback return invalid response');
    }
    
    public function init() {
        if ($this->events()->getListeners('initOrders')->count() != 1) {
            throw new \RuntimeException("Event listener for init orders was not defined or defined too many lieteners!");
        }
        $args = $this->events()->prepareArgs([]);
        $this->events()->trigger('initOrders', $this, $args);
    }
    
    public function syncWithBasket() {
        if ($this->events()->getListeners('syncWithBasket')->count() != 1) {
            throw new \RuntimeException("Event listener for sync orders settings with basket is not defined or too many listeners has defined!");
        }
        $agrs = $this->events()->prepareArgs([]);
        $this->events()->trigger('syncWithBasket', $this, $agrs);
    }
    
    public function getOrdersShowingMode($mode = 'all') {
        $validModeName = \Solo\Inflector\Inflector::camelize($mode)."OrdersMode";
        if ($this->getServiceLocator()->has($validModeName)) {
            return $this->getServiceLocator()->get($validModeName);
        }
        throw new \InvalidArgumentException('Requested orders view mode is unavaiable');
    }
    
    /**
     * 
     * @param string $mode
     */
    public function setMode($mode) {
        /*@var $callbackGetter \SoloOrder\Service\Behavior\Order\SelectOrdersForShowingOnPageBehavior */
        $callbackGetter = $this->getOrdersShowingMode($mode);
        $orderPostEventListeners = $this->events()->getListeners('getOrders.post');
        /*@var $listener \Zend\Stdlib\CallbackHandler */
        foreach ($orderPostEventListeners as $listener) {
            if ($listener->getMetadata('priority') == 2) {
                $this->events()->detach($listener);
                $this->events()->attach('getOrders.post', $callbackGetter->getOrdersForShowingCallback(), 2);
            }
        }
    }
    
    /**
     *
     * @return \SoloOrder\Entity\AbstractOrder
     */
    abstract function getOrder(array $params);
    
    /**
     * @return 
     */
    abstract function getOrders();
    
    abstract function create(\SoloOrder\Entity\Order\AbstractOrder $order, $siteId);

}

?>
