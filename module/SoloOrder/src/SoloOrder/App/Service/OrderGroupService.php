<?php
namespace SoloOrder\App\Service;
/**
 * Description of OrderGroupService
 *
 * @author slava
 */
class OrderGroupService extends \Solo\ServiceManager\ServiceLocatorAwareService {
    
    /**
     *
     * @var \SoloOrder\App\Service\Behavior\Order\OrdersCollectionGetterBehavior
     */
    protected $ordersCollectionGetter = null;
    
    /**
     *
     * @var \SoloOrder\App\Service\Behavior\Order\OrderItemsGetterBehavior
     */
    protected $orderItemsGetter = null;
    
    /**
     *
     * @var \SoloOrder\App\Service\Behavior\Order\OrdersInitBehavior
     */
    protected $ordersInitializer = null;
    
    /**
     *
     * @var \SoloOrder\App\Service\Behavior\Group\OrderDecoratorGetterBehavior
     */
    protected $orderDecorator = null;
    
    /**
     * 
     * @return \SoloOrder\App\Service\Behavior\Group\OrderDecoratorGetterBehavior
     */
    public function getOrderDecorator() {
        return $this->orderDecorator;
    }

    /**
     * 
     * @param \SoloOrder\App\Service\Behavior\Order\OrderDecoratorGetterBehavior $orderDecorator
     */
    public function setOrderDecorator(\SoloOrder\App\Service\Behavior\Order\OrderDecoratorGetterBehavior $orderDecorator) {
        $this->orderDecorator = $orderDecorator;
    }
    
    /**
     * 
     * @return \SoloOrder\App\Service\Behavior\Order\OrdersCollectionGetterBehavior
     */
    public function getOrdersCollectionGetter() {
        return $this->ordersCollectionGetter;
    }

    /**
     * 
     * @param \SoloOrder\App\Service\Behavior\Order\OrdersCollectionGetterBehavior $ordersCollectionGetter
     */
    public function setOrdersCollectionGetter(\SoloOrder\App\Service\Behavior\Order\OrdersCollectionGetterBehavior $ordersCollectionGetter) {
        $this->ordersCollectionGetter = $ordersCollectionGetter;
    }

    /**
     * 
     * @return \SoloOrder\App\Service\Behavior\Order\OrderItemsGetterBehavior
     */
    public function getOrderItemsGetter() {
        return $this->orderItemsGetter;
    }

    /**
     * 
     * @param \SoloOrder\App\Service\Behavior\Order\OrderItemsGetterBehavior $orderItemsGetter
     */
    public function setOrderItemsGetter(\SoloOrder\App\Service\Behavior\Order\OrderItemsGetterBehavior $orderItemsGetter) {
        $this->orderItemsGetter = $orderItemsGetter;
    }
    
    /**
     * 
     * @param \Zend\Stdlib\Parameters $parameters
     * @return \Closure
     */
    public function getOrdersCollectionGetterCallback(\Zend\Stdlib\Parameters $parameters = null) {
        return $this->getOrdersCollectionGetter()->getOrdersCollectionGetterCallback($parameters);
    }
    
    /**
     * 
     * @return \SoloOrder\App\Service\Behavior\Order\OrdersInitBehavior
     */
    public function getOrdersInitializer() {
        return $this->ordersInitializer;
    }

    /**
     * 
     * @param \SoloOrder\App\Service\Behavior\Order\OrdersInitBehavior $ordersInitializer
     */
    public function setOrdersInitializer(\SoloOrder\App\Service\Behavior\Order\OrdersInitBehavior $ordersInitializer) {
        $this->ordersInitializer = $ordersInitializer;
    }
    
    /**
     * 
     * @return \Closure
     */
    public function getInitCheckerCallback() {
        return $this->getOrdersInitializer()->ordersIsInitializedCallback();
    }
    
    /**
     * 
     * @param \Zend\Stdlib\Parameters $params
     * @return \Closure
     */
    public function getInitOrdersCallback(\Zend\Stdlib\Parameters $params = null) {
        return $this->getOrdersInitializer()->initOrdersCallback($params);
    }
    
    /**
     * 
     * @param \Zend\Stdlib\Parameters $params
     * @return \Closure
     */
    public function getSyncWithBasketCallback(\Zend\Stdlib\Parameters $params = null) {
        return $this->getOrdersInitializer()->syncOrdersWithBasketCallback($params);
    }
    
}
