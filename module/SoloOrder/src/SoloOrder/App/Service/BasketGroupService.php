<?php

namespace SoloOrder\App\Service;

/**
 * Description of BasketGroupAvailability
 *
 * @author slava
 */
class BasketGroupService extends \Solo\ServiceManager\ServiceLocatorAwareService implements \Zend\EventManager\EventManagerAwareInterface {

    use \Solo\EventManager\ProvidesEvents;

    /**
     *
     * @var \SoloOrder\App\Service\Behavior\GroupMergeProcessingBehavior
     */
    protected $groupMergingHandler = null;
    
    /**
     *
     * @var \SoloOrder\App\Service\Behavior\Delivery\DeliveryCostGetterBehavior
     */
    protected $deliveryCostCalculator = null;
    
    /**
     *
     * @var \SoloOrder\App\Service\Behavior\DeliveryShedulle\DeliveryShedulleGetterBehavior
     */
    protected $deliveryShedulleCalculator = null;
    
    /**
     * 
     * @return \SoloOrder\App\Service\Behavior\Delivery\DeliveryCostGetterBehavior
     */
    public function getDeliveryCostCalculator() {
        return $this->deliveryCostCalculator;
    }

    /**
     * 
     * @param \SoloDelivery\App\Service\Behavior\Delivery\DeliveryCostGetterBehavior $deliveryCostCalculator
     */
    public function setDeliveryCostCalculator(\SoloDelivery\App\Service\Behavior\Delivery\DeliveryCostGetterBehavior $deliveryCostCalculator) {
        $this->deliveryCostCalculator = $deliveryCostCalculator;
    }
    
    /**
     * 
     * @return \SoloDelivery\App\Service\Behavior\DeliveryShedulle\DeliveryShedulleGetterBehavior
     */
    public function getDeliveryShedulleCalculator() {
        return $this->deliveryShedulleCalculator;
    }

    /**
     * 
     * @param \SoloDelivery\App\Service\Behavior\DeliveryShedulle\DeliveryShedulleGetterBehavior $deliveryShedulleCalculator
     */
    public function setDeliveryShedulleCalculator(\SoloDelivery\App\Service\Behavior\DeliveryShedulle\DeliveryShedulleGetterBehavior $deliveryShedulleCalculator) {
        $this->deliveryShedulleCalculator = $deliveryShedulleCalculator;
    }

    /**
     * 
     * @return \SoloOrder\App\Service\Behavior\GroupMergeProcessingBehavior
     */
    public function getGroupMergingHandler() {
        return $this->groupMergingHandler;
    }

    /**
     * 
     * @param \SoloOrder\App\Service\Behavior\GroupMergeProcessingBehavior $groupMergingHandler
     */
    public function setGroupMergingHandler(\SoloOrder\App\Service\Behavior\GroupMergeProcessingBehavior $groupMergingHandler) {
        $this->groupMergingHandler = $groupMergingHandler;
    }

    /**
     * 
     * @param \SoloBasket\Entity\GroupCollection $groups
     * @param array $avails
     * @return \SoloBasket\Entity\GroupCollection
     */
    public function fillGroupsWithAvailabilityInfo(\SoloOrder\Entity\Basket\GroupCollection &$groups, array $avails) {
        $grps = $this->getGroupMergingHandler()->fillGroupsWithAvailabilityInfo(new \Zend\Stdlib\Parameters(array(
                    'avails' => $avails,
                    'groups' => $groups
                )));
        $groups = $grps;
    }
    
    public function getDeliveryCostCalculatorCallback(\Zend\Stdlib\Parameters $params) {
        return $this->getDeliveryCostCalculator()->getDeliveryCostCalculatorCallback($params);
    }
    
    public function getDeliveryShedulleCalculatorCallback(\Zend\Stdlib\Parameters $params) {
        return $this->getDeliveryShedulleCalculator()->getDeliveryShedulleCalculatorCallback($params);
    }

}

?>
