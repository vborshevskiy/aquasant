<?php
namespace SoloOrder\App\Service\Behavior;

/**
 * Description of AvailabilityProcessingBehavior
 *
 * @author slava
 */
interface GroupMergeProcessingBehavior {
    
    public function fillGroupsWithAvailabilityInfo(\Zend\Stdlib\Parameters $params);
    
}

?>
