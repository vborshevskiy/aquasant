<?php
namespace SoloOrder\App\Service\Behavior\Order;
/**
 *
 * @author slava
 */
interface OrdersCollectionGetterBehavior {
    
    public function getOrdersCollectionGetterCallback(\Zend\Stdlib\Parameters $parameters = null);
    
}

?>
