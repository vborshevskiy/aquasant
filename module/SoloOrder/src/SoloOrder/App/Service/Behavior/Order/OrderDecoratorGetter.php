<?php
namespace SoloOrder\App\Service\Behavior\Order;

/**
 * Description of OrderDecoratorGetter
 *
 * @author slava
 */
class OrderDecoratorGetter extends \Solo\ServiceManager\ServiceLocatorAwareService implements OrderDecoratorGetterBehavior {
    
    public function orderDecoratorCallback(\Zend\Stdlib\Parameters $parameters = null) {
        /*@var $reserveService \SoloCabinet\Service\Ultima\ReservesService */
        $reserveService = $this->getServiceLocator()->get('\SoloCabinet\Service\ReservesService');
        /*@var $userService \SoloIdentity\Service\Ultima\UserService */
        $userService = $this->getServiceLocator()->get('user_service');
        /*@var $addressSearchService \SoloKladr\Service\SearchService */
        $addressSearchService = $this->getServiceLocator()->get('SoloKladr\Service\SearchService');
        /*@var $addressService \SoloIdentity\Service\Ultima\AddressService */
        $addressService = $this->getServiceLocator()->get('address_service');
        return function($e) use($reserveService, $userService, $addressSearchService, $addressService) {
            /*@var $order \SoloOrder\Entity\Ultima\Order */
            $order = $e->getParam('order');
            /*@var $orderService \SoloOrder\Service\Ultima\OrderService */
            $orderService = $e->getTarget();
            if ($order !== null) {
                $settings = $order->settings();
                if ($settings->setting('temporaryReserveCreated')) {
                    $goodsCollection = $order->getGoodsCollection();
                    $orderConsistencyState = [];
                    foreach ($goodsCollection as $good) {
                        $orderConsistencyState[$good->getMarking()] = [
                            'quantity' => $good->getQuantity(),
                            'price' => intval($good->getChangedAmount()?($good->getChangedAmount()/$good->getQuantity()):$good->getPrice()),
                        ];
                    }
                    $tmpReserveId = $order->setting('temporaryReserveId');
                    $tmpReserveUser = $order->setting('user');
                    $tmpReserveAgent = $order->setting('agent');
                    $tmpReserve = $reserveService->getReserve($tmpReserveUser, $tmpReserveAgent, $tmpReserveId);
                    $reserveGoods = $tmpReserve->item->getGoods();
                    $reserveConsistencyState = [];
                    /*@var $rGood \SoloCabinet\Entity\Ultima\ReserveGood */
                    foreach ($reserveGoods as $rGood) {
                        $reserveConsistencyState[$rGood->getId()] = [
                            'quantity' => $rGood->getQuantity(),
                            'price' => intval($rGood->getPrice()),
                        ];
                    }
                    if ($reserveConsistencyState !== $orderConsistencyState) {
                        /*@var $reserve \SoloCabinet\Entity\Ultima\Reserve */
                        $reserve = $reserveService->newReserve();
                        $reserve->setDocumentId($tmpReserveId);
                        $reserve->setDelivery($order->hasDelivery());
                        if ($order->hasDelivery()) {
                            $reserve->setDeliveryAddressId($order->getDelivery()->getAddressId());
                            $reserve->setDeliveryTimeId($order->getDelivery()->getDeliveryTime());
                            $reserve->setDeliveryDate($order->getDelivery()->getDeliveryDate());
                            $reserve->setOutcomeWarehouseId(-1);
                            $reserve->setDeliveryGoodId(\SoloOrder\Entity\Ultima\Good::DELIVERY_GOOD_ID);
                        } else {
                            $reserve->setWarehouseId($order->getWarehouseId());
                            $reserve->setOutcomeWarehouseId($order->getWarehouseId());
                            $reserve->setDeliveryGoodId(-1);
                        }
                        foreach ($goodsCollection as $orderGood) {
                            $reserveGood = $reserveService->newGood();
                            $reserveGood->setId($orderGood->getMarking());
                            $reserveGood->setQuantity($orderGood->getQuantity());
                            $reserve->addGood($reserveGood);
                        }
                        $reserveService->updateReserve($tmpReserveUser, $tmpReserveAgent, $reserve);
                    }
                } else {
                    $order->isTemporary(true);
                    /* create fake account if user is unauthenticated */
                    if ($order->setting('user') === null) {
                        $fakeRegisterResult = $userService->registerFakeUser();
                        if ($fakeRegisterResult->success()) {
                            $userInfoGettingResult = $userService->getUserInfoById($fakeRegisterResult->userId);
                            if ($userInfoGettingResult->success()) {
                                /*@var $userInfo \SoloIdentity\Entity\Ultima\User */
                                $userInfo = $userInfoGettingResult->user;
                                $order->setting('user', $userInfo->getId());
                                $order->setting('agent', $userInfo->getNaturalPersonId());
                                $order->setting('cashless', false);
                            }
                        } else {
                            throw new \RuntimeException('Temporary user was not created! Cause: '.$fakeRegisterResult->getErrorCode());
                        }
                    }
                    /* create delivery address if is require */
                    $delivery = $order->getDelivery();
                    if ($delivery !== null && $delivery->getAddressId() < 0) {
                        $streetId = $delivery->getInfo('streetid');
                        if ($streetId === null) {
                            throw new \InvalidArgumentException('Delivery address is not defined!');
                        }
                        $postIndex = $addressSearchService->getPostIndex($streetId);
                        $newAddress = $addressService->emptyAddress();
                        $newAddress->setAgent($order->setting('agent'));
                        $newAddress->setPostIndex($postIndex);
                        $newAddress->setDescription($delivery->getInfo('text'));
                        $newAddress->setParameters($delivery->getInfo());
                        $newAddress->setPhone($delivery->getInfo('ph-num'));
                        $addressService->createAddress($newAddress);
                        $order->setting('createdAddressId', $newAddress->getId());
                    }
                    $orderService->updateSettingsGroup($order->settings());
                    $orderService->create($order);
                }
            }
            $e->setParam('order', $order);
        };
    }
    
}

?>
