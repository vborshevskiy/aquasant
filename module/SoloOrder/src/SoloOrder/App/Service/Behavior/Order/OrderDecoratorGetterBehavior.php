<?php
namespace SoloOrder\App\Service\Behavior\Order;

/**
 *
 * @author slava
 */
interface OrderDecoratorGetterBehavior {
    
    public function orderDecoratorCallback(\Zend\Stdlib\Parameters $parameters = null);
    
}

?>
