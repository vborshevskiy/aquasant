<?php
namespace SoloOrder\App\Service\Behavior\Order;

/**
 *
 * @author slava
 */
interface OrdersInitBehavior {
    
    public function initOrdersCallback(\Zend\Stdlib\Parameters $parameters = null);
    public function ordersIsInitializedCallback();
    public function syncOrdersWithBasketCallback(\Zend\Stdlib\Parameters $parameters = null);
    
}

?>
