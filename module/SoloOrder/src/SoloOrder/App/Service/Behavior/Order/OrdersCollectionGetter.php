<?php

namespace SoloOrder\App\Service\Behavior\Order;

use SoloIdentity\Entity\Ultima\User;
use SoloIdentity\Entity\Ultima\FakeUser;
use Zend\Session\Container;

/**
 * Description of OrdersCollectionGetter
 *
 * @author slava
 */
class OrdersCollectionGetter extends \Solo\ServiceManager\ServiceLocatorAwareService implements OrdersCollectionGetterBehavior {

    public function getOrdersCollectionGetterCallback(\Zend\Stdlib\Parameters $parameters = null) {
        $basketService = $this->getServiceLocator()->get('basket_service');
        $citiesService = $this->getServiceLocator()->get('facility_cities');
        $pricesService = $this->getServiceLocator()->get('catalog_prices');
        $userSession = $this->getServiceLocator()->get('user_session');
        $fakeUserSession = $this->getServiceLocator()->get('fake_user_session');
        if ($userSession->user) {
            $user = new User();
            $user->unserialize($userSession->user);
        } else {
            $user = null;
        }
//        if ($fakeUserSession->fakeUser) {
//            $fakeUser = new FakeUser();
//            $fakeUser->unserialize($fakeUserSession->fakeUser);
//        } else {
//            $fakeUser = null;
//        }

        $cityId = $citiesService->getCityId();
        $priceColumn = $pricesService->helper()->getPriceColumnId();
        return function($e) use($basketService, $priceColumn, $user, $cityId) {
            /* @var $ordersCollection \SoloOrder\Entity\OrdersCollection */
            $ordersCollection = $e->getTarget()->newOrdersCollection();
            $orderService = $e->getParam('orderService');
            /* @var $userService \SoloIdentity\Service\Ultima\UserService */
            $userService = $this->getServiceLocator()->get('user_service');
            $basketGroups = $basketService->getGroups();
            /* @var $group \SoloBasket\Entity\Group */

            $container = new Container('orders');
            $container->currentOrderNumber = $container->currentOrderNumber ?: 1;
            $container->ordersCount = count($basketGroups) + $container->currentOrderNumber - 1;

            foreach ($basketGroups as $group) {
                $goods = $group->getGoods();
                /* @var $orderGoodsCollection \SoloOrder\Entity\GoodsCollection */
                $orderGoodsCollection = $orderService->newGoodsCollection();

                $goodOptions = new \SoloOrder\Entity\Order\GoodOptions();
                $goodOptions->hasDelivery($group->hasDelivery());
                $goodOptions->setWarehouseId($group->getWarehouseId());
                //$goodOptions->setPriceColumn($priceColumn, $purchasePriceColumn);

                $hasSuborder = true;
                /* @var $basketGood \SoloBasket\Entity\Good */
                foreach ($goods as $basketGood) {
                    $goodOptions->addGoodId($basketGood->getId());
                    $hasSuborder &= $basketGood->hasSuborder();
                    /* @var $orderGood \SoloOrder\Entity\Ultima\Good */
                    $orderGood = $orderService->newGood();
                    $orderGood->setMarking($basketGood->getId());
                    $orderGood->setQuantity($basketGood->getQuantity());
                    //$orderGood->setChangedAmount($basketGood->getChangedAmount());
                    $orderGood->setPrice($basketGood->getPrice());
                    $orderGood->setWeight($basketGood->getWeight());
                    $orderGood->setVolume($basketGood->getVolume());
                    $orderGood->allowShowcaseSale($basketGood->allowShowcaseSale());

                    $orderGood->setPackageComponents($basketGood->getPackageComponents());

                    $orderGoodsCollection->add($orderGood);
                }

                $goodOptions->hasSuborderedGoods($hasSuborder);

                /* @var $order \SoloOrder\Entity\Ultima\Order */
                $order = $orderService->newOrder();
                $order->setGoodsCollection($orderGoodsCollection);

                $newSettings = $orderService->newSettings();
                if (!is_null($user)) {
                    $newSettings->setting('user', $user->getId());
                    $newSettings->setting('securityKey', $user->getSecurityKey());
                    if ($user->hasCurrentAgent()) {
                        $newSettings->setting('agent', $user->getCurrentAgent()->getId());
                    } else {
                        $newSettings->setting('agent', $user->getNaturalPersonId());
                    }
                }
//                elseif (!is_null($fakeUser)) {
//                    $newSettings->setting('user', $fakeUser->getId());
//                    $newSettings->setting('securityKey', $fakeUser->getSecurityKey());
//                } else {
//                    $fakeRegisterResult = $userService->registerFakeUser();
//
//                    if (!$fakeRegisterResult->hasError()) {
//                        $fakeUser = new FakeUser();
//                        $fakeUser->setId($fakeRegisterResult->userId);
//                        $fakeUser->setSecurityKey($fakeRegisterResult->SecurityKey);
//                        $fakeUser->setPassword($fakeRegisterResult->Password);
//                        $fakeSession = $this->getServiceLocator()->get('fake_user_session');
//                        $fakeSession->fakeUser = $fakeUser->serialize();
//                        $newSettings->setting('user', $fakeRegisterResult->userId);
//                        $newSettings->setting('securityKey', $fakeRegisterResult->SecurityKey);
//                    }
//                }

                $newSettings->setting('arrivalDate', $group->getId());
                $order->settings($newSettings);

                $orderSession = $orderService->getLocalStorage();
                if ($orderSession->hasId()) {
                    $order->setId($orderSession->getId());
                }
                if ($orderSession->hasStoreId()) {
                    $order->setStoreId($orderSession->getStoreId());
                }
                if ($orderSession->hasPaymentId()) {
                    $order->setPaymentId($orderSession->getPaymentId());
                }
                if ($orderSession->hasYandexKassaPaymentTerm()) {
                    $order->setYandexKassaPaymentTerm($orderSession->getYandexKassaPaymentTerm());
                }
                if ($orderSession->hasAmount()) {
                    $order->setAmount($orderSession->getAmount());
                }
                if ($orderSession->hasNotAvailGoods()) {
                    $order->setNotAvailGoods($orderSession->getNotAvailGoods());
                }
                if ($orderSession->hasPickupDate()) {
                    $order->setPickupDate($orderSession->getPickupDate());
                }
                if ($orderSession->hasAgentId()) {
                    $order->setAgentId($orderSession->getAgentId());
                }
                if ($orderSession->hasAgent()) {
                    $order->setAgent($orderSession->getAgent());
                }
                if ($orderSession->hasBonusAmount()) {
                    $order->setBonusAmount($orderSession->getBonusAmount());
                }
                if ($orderSession->hasCityId()) {
                    $order->setCityId($orderSession->getCityId());
                }
                if ($orderSession->hasObtainMethod()) {
                    $order->setObtainMethod($orderSession->getObtainMethod());
                }
                if ($orderSession->hasPayerPerson()) {
                    $order->setPayerPerson($orderSession->getPayerPerson());
                }
                if ($orderSession->hasPhone()) {
                    $order->setting('phone',$orderSession->getPhone());
                }
                if ($orderSession->hasComment()) {
                	$order->setting('comment',$orderSession->getComment());
                }
                if ($orderSession->hasManagerId()) {
                	$order->setting('managerId',$orderSession->getManagerId());
                }
                if ($orderSession->hasUserName()) {
                    $order->setting('userName',$orderSession->getUserName());
                }
				if ($orderSession->hasUserEmail()) {
                    $order->setting('userEmail',$orderSession->getUserEmail());
                }
                if ($orderSession->getDeliveryComment()) {
                	$order->setting('deliveryComment', $orderSession->getDeliveryComment());
                }
                if ($orderSession->getSource()) {
                	$order->setting('source', $orderSession->getSource());
                }

                if ($orderSession->hasDelivery()) {
                    /* @var $orderDelivery \SoloOrder\Entity\Ultima\Delivery */
                    $orderDelivery = $orderService->newDelivery();
                    $orderDelivery->setAddressId($orderSession->getDeliveryAddressId());
                    $orderDelivery->setAddress($orderSession->getDeliveryAddress());
                    $orderDelivery->setDeliveryCost($orderSession->getDeliveryCost());
                    $orderDelivery->setDeliveryDate($orderSession->getDeliveryDate());
                    $orderDelivery->setDeliveryTimeId($orderSession->getDeliveryTimeId());
                    $orderDelivery->setDeliveryInfo($orderSession->getDeliveryInfo());
                    $orderDelivery->setDeliveryPhone($orderSession->getDeliveryPhone());
                    $orderDelivery->setTextDeliveryDate($orderSession->getTextDeliveryDate());
                    $orderDelivery->setLiftToTheFloor($orderSession->getDeliveryLiftToTheFloor());
                    $orderDelivery->setFloor($orderSession->getDeliveryFloor());
                    $orderDelivery->setElevatorTypeId($orderSession->getDeliveryElevatorTypeId());

                    $order->setDelivery($orderDelivery);
                }

                if ($orderSession->hasManuallyEnteredDelivery()) {
                    /* @var $orderDelivery \SoloOrder\Entity\Ultima\Delivery */
                    $orderDelivery = $orderService->newDelivery();
                    $orderDelivery->setAddressId($orderSession->getDeliveryAddressId());
                    $orderDelivery->setDeliveryDate($orderSession->getDeliveryDate());
                    $orderDelivery->setDeliveryTimeId($orderSession->getDeliveryTimeId());
                    $orderDelivery->setDeliveryInfo($orderSession->getDeliveryInfo());
                    $orderDelivery->setDeliveryPhone($orderSession->getDeliveryPhone());
                    $orderDelivery->setTextDeliveryDate($orderSession->getTextDeliveryDate());
                    $order->setManuallyEnteredDelivery($orderDelivery);
                }

                if ($orderSession->hasPostDelivery()) {
                    /* @var $orderDelivery \SoloOrder\Entity\Ultima\Delivery */
                    $orderDelivery = $orderService->newPostDelivery();
                    $orderDelivery->setAddressId($orderSession->getDeliveryAddressId());
                    $orderDelivery->setDeliveryInfo($orderSession->getDeliveryInfo());
                    $orderDelivery->setDeliveryPhone($orderSession->getDeliveryPhone());
                    $orderDelivery->setDeliveryCost($orderSession->getDeliveryCost());
                    $orderDelivery->setTwinLogisticCost($orderSession->getTwinLogisticCost());
                    $orderDelivery->setTransportCompanyUid($orderSession->getDeliveryTransportCompanyId());
                    $orderDelivery->setTransportCompanyErpId($orderSession->getDeliveryTransportCompanyErpId());
                    $orderDelivery->setUserName($orderSession->getUserName());
                    $orderDelivery->setFakeDeliveryTimeId($orderSession->getFakeDeliveryTimeId());
                    $orderDelivery->setMinDeliveryDate($orderSession->getPostDeliveryMinDeliveryDate());
                    $orderDelivery->setMaxDeliveryDate($orderSession->getPostDeliveryMaxDeliveryDate());
                    $orderDelivery->setDeliveryDate($orderSession->getDeliveryDate());
                    $order->setPostDelivery($orderDelivery);
                }

                if ($orderSession->hasPostPickup()) {
                    /* @var $orderDelivery \SoloOrder\Entity\Ultima\Delivery */
                    $orderDelivery = $orderService->newPostDelivery();
                    $orderDelivery->setAddressId($orderSession->getDeliveryAddressId());
                    $orderDelivery->setDeliveryInfo($orderSession->getDeliveryInfo());
                    $orderDelivery->setDeliveryPhone($orderSession->getDeliveryPhone());
                    $orderDelivery->setDeliveryCost($orderSession->getDeliveryCost());
                    $orderDelivery->setTwinLogisticCost($orderSession->getTwinLogisticCost());
                    $orderDelivery->setTransportCompanyErpId($orderSession->getDeliveryTransportCompanyErpId());
                    $orderDelivery->setUserName($orderSession->getUserName());
                    $orderDelivery->setFakeDeliveryTimeId($orderSession->getFakeDeliveryTimeId());
                    $orderDelivery->setMinDeliveryDate($orderSession->getPostDeliveryMinDeliveryDate());
                    $orderDelivery->setMaxDeliveryDate($orderSession->getPostDeliveryMaxDeliveryDate());
                    $orderDelivery->setDeliveryDate($orderSession->getDeliveryDate());
                    $order->setPostPickup($orderDelivery);
                }

                $ordersCollection->add($order);
            }
            $e->setParam('collection', $ordersCollection);
        };
    }

}
