<?php
namespace SoloOrder\App\Service\Behavior\Order;

/**
 * Description of OrdersInitByGroupingStrategy
 *
 * @author slava
 */
class OrdersInitByGroupingStrategy extends \Solo\ServiceManager\ServiceLocatorAwareService  implements OrdersInitBehavior {
    
    public function initOrdersCallback(\Zend\Stdlib\Parameters $parameters = null) {
        $basketService = $this->getServiceLocator()->get('basket_service');
        $siteId = $this->getServiceLocator()->get('Config')['site']['id'];
        $userSession = $this->getServiceLocator()->get('user_session');
        /*@var $orderService \SoloOrder\Service\Ultima\OrderService */
        return function($e) use($basketService, $siteId, $userSession) {
            $orderService = $e->getTarget();
            $basketGroups = $basketService->getGroups();
            foreach ($basketGroups as $index => $group) {
                /*@var $orderDesinition \SoloOrder\Entity\OrderDefinition */
                $orderDefinition = $orderService->newDefinition();
                $orderDefinition->define('warehouseId', ((int)$group->getWarehouseId()>0?(int)$group->getWarehouseId():null));
                $groupDelivery = $group->getDelivery();
                if ($groupDelivery !== null) {
                    $delivery = [
                        'addressId' => $groupDelivery->getAddressId(),
                        'date' => $groupDelivery->getDeliveryDate(),
                        'time' => $groupDelivery->getDeliveryTime(),
                        'info' => $groupDelivery->getInfo(),
                    ];
                } else {
                    $delivery = null;
                }
                $orderDefinition->define('delivery', $delivery);
                /*@var $settings \SoloOrder\Entity\Ultima\Settings */
                $settings = $orderService->newSettings();
                $settings->setOrderDefinition($orderDefinition);
                $settings->setting('index', ($index+1));
                $settings->setting('processed', false);
                $settings->setting('bonusPayAmount', 0);
                $settings->setting('siteId', $siteId);
                $settings->setting('noBonuses', ($group->additional('spendBonus')?$group->additional('spendBonus'):false));
                $settings->setting('hasSuborder', $group->hasSuborder());
                if ($userSession->user !== null) {
                    $settings->setting('user', $userSession->user->getId());
                    if ($userSession->user->hasCurrentAgent()) {
                        $settings->setting('agent', $userSession->user->getCurrentAgent()->getId());
                        $settings->setting('cashless', false);
                    } else {
                        $settings->setting('agent', $userSession->user->getNaturalPersonId());
                        $settings->setting('cashless', true);
                    }
                }
                $orderService->addSettingsGroup($settings);
            }
        };
    }
    
    public function ordersIsInitializedCallback() {
        return function($e) {
            /*@var $orderService \SoloOrder\Service\Ultima\OrderService */
            $orderService = $e->getTarget();
            $settingsCollection = $orderService->getSettingsCollection();
            $e->setParam('result', ($settingsCollection !== null && $settingsCollection->count() > 0));
        };
    }
    
    public function syncOrdersWithBasketCallback(\Zend\Stdlib\Parameters $params = null) {
        $basketService = $this->getServiceLocator()->get('basket_service');
        /*@var $orderService \SoloOrder\Service\AbstractOrderService */
        $userSession = $this->getServiceLocator()->get('user_session');
        $siteId = $this->getServiceLocator()->get('Config')['site']['id'];
        return function($e) use($basketService, $orderService, $userSession, $siteId) {
            $orderService = $e->getTarget();
            $settingsCollection = $orderService->getSettingsCollection();
            $basketGroups = $basketService->getGroups();
            /*@var $settings \SoloOrder\Entity\AbstractSettings */
            foreach ($settingsCollection as $index => $settings) {
                $definitionArray = $settings->getOrderDefinition()->toArray();
                if (!array_key_exists('delivery', $definitionArray)) {
                    $definitionArray['delivery'] =  null;
                } elseif (!array_key_exists('warehouseId', $definitionArray)) {
                    $definitionArray['warehouseId'] =  null;
                    $definitionArray = array_reverse($definitionArray, true);
                }
                $esists = false;
                foreach ($basketGroups as $bGroup) {
                    if ($bGroup->hasDelivery()) {
                        $delivery = $bGroup->getDelivery();
                        $definition = [
                            'warehouseId' => null,
                            'delivery' => [
                                'addressId' => $delivery->getAddressId(),
                                'date' => $delivery->getDeliveryDate(),
                                'time' => $delivery->getDeliveryTime(),
                                'info' => $delivery->getInfo(),
                            ]
                        ];
                    } else {
                        $definition = [
                            'warehouseId' => $bGroup->getWarehouseId(),
                            'delivery' => null
                        ];
                    }
//                    var_dump($definition);
//                    var_dump($definitionArray);
//                    var_dump($definition === $definitionArray);
                    if ($definition === $definitionArray) {
                        $settings->setting('noBonuses', ($bGroup->additional('spendBonus')?$bGroup->additional('spendBonus'):false));
                        $settings->setting('hasSuborder', $bGroup->hasSuborder());
                        if (null !== $userSession->user) {
                            $settings->setting('user', $userSession->user->getId());
                            if ($userSession->user->hasCurrentAgent()) {
                                $settings->setting('agent', $userSession->user->getCurrentAgent()->getId());
                            } else {
                                $settings->setting('agent', $userSession->user->getNaturalPersonId());
                            }
                        }
                        $orderService->updateSettingsGroup($settings);
                        continue 2;
                    }
                }
                $orderService->removeSettingsGroup($settings);
                unset($settingsCollection[$index]);
                /**
                 * @todo temporary reserve removing
                 * if ($settings->setting('temporaryReserveCreated')) {
                 *      //do something
                 * }
                 */
            }
            foreach ($basketGroups as $index => $bGroup) {
                $exists = false;
                foreach ($settingsCollection as $settings) {
                    if ($bGroup->hasDelivery()) {
                        $delivery = $bGroup->getDelivery();
                        $definition = [
                            'warehouseId' => null,
                            'delivery' => [
                                'addressId' => $delivery->getAddressId(),
                                'date' => $delivery->getDeliveryDate(),
                                'time' => $delivery->getDeliveryTime(),
                                'info' => $delivery->getInfo(),
                            ]
                        ];
                    } else {
                        $definition = [
                            'warehouseId' => $bGroup->getWarehouseId(),
                            'delivery' => null
                        ];
                    }
                    $definitionArray = $settings->getOrderDefinition()->toArray();
                    if (!array_key_exists('delivery', $definitionArray)) {
                        $definitionArray['delivery'] =  null;
                    } elseif (!array_key_exists('warehouseId', $definitionArray)) {
                        $definitionArray['warehouseId'] =  null;
                        $definitionArray = array_reverse($definitionArray, true);
                    }
                    if ($definition === $definitionArray) {
                        $settings->setting('noBonuses', ($bGroup->additional('spendBonus')?$bGroup->additional('spendBonus'):false));
                        $settings->setting('hasSuborder', $bGroup->hasSuborder());
                        if ($userSession->user !== null) {
                            $settings->setting('user', $userSession->user->getId());
                            if ($userSession->user->hasCurrentAgent()) {
                                $settings->setting('agent', $userSession->user->getCurrentAgent()->getId());
                            } else {
                                $settings->setting('agent', $userSession->user->getNaturalPersonId());
                            }
                        }
                        $orderService->updateSettingsGroup($settings);
                        $exists = true;
                        continue 2;
                    }
                }
                /*define new settings group*/
                        /* @var $newSettings \SoloOrder\Entity\Ultima\Settings */
                $newSettings = $orderService->newSettings();
                $newSettings->setting('index', ($index+1));
                $newSettings->setting('processed', false);
                $newSettings->setting('bonusPayAmount', 0);
                $newSettings->setting('siteId', $siteId);
                $newSettings->setting('noBonuses', ($bGroup->additional('spendBonus')?$bGroup->additional('spendBonus'):false));
                $newSettings->setting('hasSuborder', $bGroup->hasSuborder());
                if ($userSession->user !== null) {
                    $newSettings->setting('user', $userSession->user->getId());
                    if ($userSession->user->hasCurrentAgent()) {
                        $newSettings->setting('agent', $userSession->user->getCurrentAgent()->getId());
                    } else {
                        $newSettings->setting('agent', $userSession->user->getNaturalPersonId());
                    }
                }
                
                $definition = $orderService->newDefinition();
                if ($bGroup->hasDelivery()) {
                    $delivery = $bGroup->getDelivery();
                    $definition->define('warehouseId', null);
                    $definition->define('delivery', [
                        'addressId' => $delivery->getAddressId(),
                        'date' => $delivery->getDeliveryDate(),
                        'time' => $delivery->getDeliveryTime(),
                        'info' => $delivery->getInfo(),
                    ]);
                } else {
                    $definition->define('warehouseId', $bGroup->getWarehouseId());
                    $definition->define('delivery', null);
                }
                $newSettings->setOrderDefinition($definition);
                
                $orderService->addSettingsGroup($newSettings);
            }
        };
    }
    
}

?>
