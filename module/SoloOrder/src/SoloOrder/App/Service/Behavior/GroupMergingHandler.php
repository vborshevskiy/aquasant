<?php
namespace SoloOrder\App\Service\Behavior;

/**
 * Description of AvailabilityDependent
 *
 * @author slava
 */
class GroupMergingHandler implements GroupMergeProcessingBehavior {
    
    public function fillGroupsWithAvailabilityInfo(\Zend\Stdlib\Parameters $params) {
        $warehouseAvails = $params->get('avails');
        $groups = $params->get('groups')->toArray();
        $totalQuantities = array();
        $goodsQuantitiesByWarehouseId = array();
        foreach ($warehouseAvails as $avail) {
            if (!array_key_exists(intval($avail['WarehouseID']), $goodsQuantitiesByWarehouseId)) {
                $goodsQuantitiesByWarehouseId[intval($avail['WarehouseID'])] = array();
            }
            $goodsQuantitiesByWarehouseId[intval($avail['WarehouseID'])][intval($avail['GoodID'])] = intval($avail['AvailQuantity']);
        }
        foreach ($groups as $index => $group) {
            $warehouse = $group->getWarehouseId();
            $delivery = $group->getDelivery();
            $availShops = array();
            foreach ($groups as $chGroup) {
                $childWarehouseId = $chGroup->getWarehouseId();
                $childDelivery = $chGroup->getDelivery();
                if (!$childDelivery && ($warehouse != $childWarehouseId)) {
                    $chQuantities = array();
                    foreach ($chGroup->getGoods() as $chGroupGood) {
                        $chQuantities[$chGroupGood->getId()] = $chGroupGood->getQuantity();
                    }
                    $whAvail = true;
                    foreach ($group->getGoods() as $groupGood) {
                        if ((isset($goodsQuantitiesByWarehouseId[$childWarehouseId][$groupGood->getId()])) &&
                              $goodsQuantitiesByWarehouseId[$childWarehouseId][$groupGood->getId()] >= 
                                      ($groupGood->getQuantity() + (isset($chQuantities[$groupGood->getId()])?$chQuantities[$groupGood->getId()]:0))) {
                            $whAvail &= true;
                        } else {
                            $whAvail &= false;
                        }
                    }
                    if ($whAvail) {
                        $availShops[] = $childWarehouseId;
                    }
                }
            }
            if (!$delivery) {
                array_unshift($availShops, $warehouse);
            }
            $group->setShops($availShops);
            $groups[$index] = $group;
        }
        return $groups;
    }
    
}

?>
