<?php

namespace SoloOrder\Entity\Order;

/**
 * OrderGood
 *
 * @author Slava Tutrinov
 */
abstract class AbstractGood {

	private $marking = null;

	private $name = null;

	private $quantity = null;

	private $maxQuantity = null;

	private $price = 0;

	private $url = null;
	
	private $allowShowcaseSale = false;

	public function getUrl() {
		return $this->url;
	}

	public function setUrl($url) {
		$this->url = $url;
	}

	public function getMaxQuantity() {
		return $this->maxQuantity;
	}

	public function setMaxQuantity($maxQuantity) {
		$this->maxQuantity = $maxQuantity;
	}

	public function getMarking() {
		return $this->marking;
	}

	public function setMarking($marking) {
		$this->marking = $marking;
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function getQuantity() {
		return $this->quantity;
	}

	public function setQuantity($quantity) {
		$this->quantity = $quantity;
	}

	public function getPrice() {
		return $this->price;
	}

	public function setPrice($price) {
		$this->price = $price;
	}

	public function getPriceSum() {
		return $this->price * $this->quantity;
	}
	
	/**
	 * 
	 * @param boolean $value
	 * @return boolean
	 */
	public function allowShowcaseSale($value = null) {
		if (null !== $value) {
			$this->allowShowcaseSale = $value;
		} else {
			return $this->allowShowcaseSale;
		}
	}

}

