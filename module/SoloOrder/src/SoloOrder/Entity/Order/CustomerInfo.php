<?php

namespace SoloOrder\Entity\Order;

/**
 * CustomerInfo
 *
 * @author Slava Tutrinov
 */
class CustomerInfo {

	private $loginId = null;

	private $agentId = null;

	public function getLoginId() {
		return $this->loginId;
	}

	public function setLoginId($loginId) {
		$this->loginId = $loginId;
	}

	public function getAgentId() {
		return $this->agentId;
	}

	public function setAgentId($agentId) {
		$this->agentId = $agentId;
	}

}

?>
