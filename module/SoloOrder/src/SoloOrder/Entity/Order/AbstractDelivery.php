<?php
namespace SoloOrder\Entity\Order;

/**
 * Description of AbstractOrderDelivery
 *
 * @author slava
 */
abstract class AbstractDelivery {
    
    protected $deliveryCost = null;
    
    protected $addressId = null;
    
    protected $deliveryInfo = null;
    
    protected $deliveryPhone = null;
    
    protected $deliveryDate = null;
    
    public function getDeliveryInfo() {
        return $this->deliveryInfo;
    }

    public function setDeliveryInfo($deliveryInfo) {
        $this->deliveryInfo = $deliveryInfo;
    }
    
    public function getDeliveryCost() {
        return $this->deliveryCost;
    }

    public function setDeliveryCost($deliveryCost) {
        $this->deliveryCost = (float)$deliveryCost;
    }
    
    public function getAddressId() {
        return $this->addressId;
    }

    public function setAddressId($addressId) {
        $this->addressId = (int)$addressId;
    }
    
    public function setDeliveryPhone($deliveryPhone) {
        $this->deliveryPhone = $deliveryPhone;
    }
    
    public function getDeliveryPhone() {
        return $this->deliveryPhone;
    }
    
    public function getDeliveryDate() {
        return $this->deliveryDate;
    }

    public function setDeliveryDate($deliveryDate) {
        $this->deliveryDate = $deliveryDate;
    }
    
    /**
     *
     * @return array
     */
    public function getInfo($param = null) {
        if (null !== $param) {
            if (isset($this->deliveryInfo[$param])) {
                return $this->deliveryInfo[$param];
            }
            return null;
        }
        return $this->deliveryInfo;
    }
    
    
}
