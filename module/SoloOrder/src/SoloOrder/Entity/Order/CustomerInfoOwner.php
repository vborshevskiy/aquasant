<?php

namespace SoloOrder\Entity\Order;

/**
 * CustomerInfoOwner
 * 
 * @author Slava Tutrinov
 */
interface CustomerInfoOwner {

	public function setCustomerInfo(\SoloOrder\Entity\Order\CustomerInfo $customerInfo);

	public function getCustomerInfo();

}

?>
