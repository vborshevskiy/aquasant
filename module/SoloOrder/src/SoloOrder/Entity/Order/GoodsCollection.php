<?php

namespace SoloOrder\Entity\Order;

/**
 * OrderGoodsCollection
 *
 * @author Slava Tutrinov
 */
class GoodsCollection extends \Solo\Collection\Collection {
    
    public function add($object, $key = null) {
        return parent::add($object, $object->getMarking());
    }
    
    public function getQuantitiesByGoodIds() {
        $result = [];
        if (sizeof($this->list) > 0) {
            foreach ($this->list as $basketGood) {
                if (!array_key_exists($basketGood->getMarking(), $result)) {
                    $result[$basketGood->getMarking()] = 0;
                }
                $result[$basketGood->getMarking()] += $basketGood->getQuantity();
            }
        }
        return $result;
    }

}

?>
