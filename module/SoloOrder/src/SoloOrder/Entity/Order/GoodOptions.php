<?php

namespace SoloOrder\Entity\Order;

/**
 * GoodOptions
 *
 * @author Slava Tutrinov
 */
class GoodOptions {

	private $goodIds = [];

	private $warehouseId = null;

	private $priceColumn = "RetailPrice";

	private $purchasePriceColumn = "PurchaseRetailPrice";

	private $hasSuborderedGoods = false;

	private $hasDelivery = false;

	public function hasDelivery($hasDelivery = null) {
		if ($hasDelivery !== null) {
			$this->hasDelivery = $hasDelivery;
			return;
		}
		return $this->hasDelivery;
	}

	public function getGoodIds() {
		return $this->goodIds;
	}

	public function setGoodIds($goodIds) {
		$this->goodIds = $goodIds;
	}

	public function addGoodId($goodId) {
		$this->goodIds[] = $goodId;
	}

	public function getWarehouseId() {
		return $this->warehouseId;
	}

	public function setWarehouseId($warehouseId) {
		$this->warehouseId = $warehouseId;
	}

	public function getPriceColumn() {
		return $this->priceColumn;
	}

	public function getPurchasePriceColumn() {
		return $this->purchasePriceColumn;
	}

	public function setPriceColumn($priceColumn, $purchasePriceColumn) {
		$this->priceColumn = $priceColumn;
		$this->purchasePriceColumn = $purchasePriceColumn;
	}

	public function hasSuborderedGoods($has = null) {
		if (null !== $has) {
			$this->hasSuborderedGoods = (bool)$has;
			return;
		} else {
			return $this->hasSuborderedGoods;
		}
	}

}

?>
