<?php
namespace SoloOrder\Entity\Order;

/**
 * Description of ProvidesManuallyEnterredDelivery
 */
trait ProvidesManuallyEnteredDelivery {
    
    /**
     *
     * @var \SoloOrder\Entity\Order\AbstractDelivery
     */
    protected $manuallyEnteredDelivery = null;
    
    /**
     * 
     * @param \SoloOrder\Entity\Order\AbstractDelivery $delivery
     */
    public function setManuallyEnteredDelivery(\SoloOrder\Entity\Order\AbstractDelivery $delivery) {
        $this->manuallyEnteredDelivery = $delivery;
    }
    
    /**
     * 
     * @return \SoloOrder\Entity\Order\AbstractDelivery
     */
    public function getManuallyEnteredDelivery() {
        return $this->manuallyEnteredDelivery;
    }
    
    /**
     * 
     * @return boolean
     */
    public function hasManuallyEnteredDelivery() {
        return ((null !== $this->manuallyEnteredDelivery) && ($this->manuallyEnteredDelivery instanceof \SoloOrder\Entity\Order\AbstractDelivery));
    }
    
}

?>
