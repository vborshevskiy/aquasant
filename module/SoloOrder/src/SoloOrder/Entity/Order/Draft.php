<?php

namespace SoloOrder\Entity\Order;
/*
 * To change this template, choose Tools | Templates and open the template in the editor.
 */

/**
 * Description of Draft
 *
 * @author slava
 */
class Draft {

	private $id = null;

	private $documentNo = null;

	private $createDate = null;

	private $amount = null;

	private $isDelivery = null;

	private $warehouseId = null;

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getDocumentNo() {
		return $this->documentNo;
	}

	public function setDocumentNo($documentNo) {
		$this->documentNo = $documentNo;
	}

	public function getCreateDate() {
		return $this->createDate;
	}

	public function setCreateDate($createDate) {
		$this->createDate = $createDate;
	}

	public function getAmount() {
		return $this->amount;
	}

	public function setAmount($amount) {
		$this->amount = $amount;
	}

	public function getIsDelivery() {
		return $this->isDelivery;
	}

	public function setIsDelivery($isDelivery) {
		$this->isDelivery = $isDelivery;
	}

	public function getWarehouseId() {
		return $this->warehouseId;
	}

	public function setWarehouseId($warehouseId) {
		$this->warehouseId = $warehouseId;
	}

}

?>
