<?php
namespace SoloOrder\Entity\Order\Ultima;
/**
 * Description of ProccessedOrder
 *
 * @author slava
 */
trait ProvidesOrderStatus {
    
    private $isProccessed = false;
    private $reserveId = null;
    
    public function proccessed($isProccessed = null) {
        if ($isProccessed !== null) {
            $this->isProccessed = (bool)$isProccessed;
        }
        return $this->isProccessed;
    }
    
    public function setReserveId($reserveId) {
        $this->reserveId = $reserveId;
    }
    
    public function getReserveId() {
        return $this->reserveId;
    }
    
}

?>
