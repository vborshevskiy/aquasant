<?php
namespace SoloOrder\Entity\Order\Ultima;
use SoloIdentity\Entity\AbstractAgent;

/**
 * Description of Order
 *
 * @author slava
 */
class Order extends \SoloOrder\Entity\Order\AbstractOrder {

    use ProvidesOrderStatus;
    use \SoloOrder\Entity\Order\ProvidesDelivery;
    use \SoloOrder\Entity\Order\ProvidesManuallyEnteredDelivery;
    use \SoloOrder\Entity\Order\ProvidesPostDelivery;
    use \SoloOrder\Entity\Order\ProvidesPostPickup;

    const PAYER_NATURAL_PERSON = 'naturalPerson';
    const PAYER_LEGAL_PERSON = 'legalPerson';

    private $storeId = null;
    private $paymentId = self::PAYMENT_CASH_ID;
    private $amount = -1;
    private $notAvailGoods = [];
    private $pickupDate = null;
    private $cityId = null;
    private $bankPercent = 0;
    private $obtainMethod = self::DELIVERY_OBTAIN_METHOD;
    private $payerPerson = self::PAYER_NATURAL_PERSON;
    private $agent = null;

    public function __construct($bankPercent = 0, $id = null, \SoloOrder\Entity\Order\GoodsCollection $orderGoodsCollection = null) {
        $this->bankPercent = $bankPercent;
        $this->bankPercent = 0;

        parent::__construct($id, $orderGoodsCollection);
    }

    public function setStoreId($storeId) {
        $this->storeId = (int)$storeId;
    }

    public function getStoreId() {
        return $this->storeId;
    }

    public function setPaymentId($paymentId) {
        $this->paymentId = (int)$paymentId;
    }

    public function getPaymentId() {
        return $this->paymentId;
    }

    public function isAlfabankPayment() {
        return $this->getPaymentId() === self::PAYMENT_ALFABANK_ID;
    }

    public function isCashPayment() {
        return $this->getPaymentId() === self::PAYMENT_CASH_ID;
    }

    public function getBankPercent() {
        return $this->bankPercent;
    }

    /**
     *
     * @return integer
     */
    public function getBankPaymentSum() {
        $sum = 0;
        if ($this->hasDelivery() && $this->getObtainMethod() == self::DELIVERY_OBTAIN_METHOD) {
            $sum += $this->getDelivery()->getDeliveryCost();
        } elseif ($this->hasPostDelivery()) {
            $sum += $this->getPostDelivery()->getDeliveryCost() + $this->getPostDelivery()->getTwinLogisticCost();
        } elseif ($this->hasManuallyEnteredDelivery()) {
            $sum += $this->getManuallyEnteredDelivery()->getDeliveryCost();
        }
        foreach ($this->getGoodsCollection() as $good) {
            $sum += $good->getPrice() * $good->getQuantity();
        }
        return ceil($sum / (100 - $this->bankPercent) * 100);
    }

    public function setNotAvailGoods($notAvailGoods) {
        $this->notAvailGoods = $notAvailGoods;
    }

    public function getNotAvailGoods() {
        return $this->notAvailGoods;
    }

    public function setAmount($amount) {
        $this->amount = (float)$amount;
    }

    public function getAmount() {
        return $this->amount;
    }

    public function getAmountWithKassaComission() {
        return ceil($this->amount / (100 - $this->bankPercent) * 100);
    }

    public function hasEmptyParameters() {
        return (0 > $this->getPaymentId() || !$this->hasDeliveryOrStoreId());
    }

    public function hasDeliveryOrStoreId() {
         return ($this->hasDelivery() ||  0 < $this->getStoreId() || $this->hasManuallyEnteredDelivery() || $this->hasPostDelivery() || $this->hasPostPickup());
    }

    public function setPickupDate($date) {
        $this->pickupDate = $date;
    }

    public function getPickupDate() {
        return $this->pickupDate;
    }

    public function setCityId($cityId) {
        $this->cityId = $cityId;
    }

    public function getCityId() {
        return $this->cityId;
    }

    public function hasCityId() {
        return (!is_null($this->cityId) && $this->cityId > 0);
    }

    public function setObtainMethod($obtainMethod) {
        $this->obtainMethod = $obtainMethod;
    }

    public function getObtainMethod() {
        return $this->obtainMethod;
    }

    public function setPayerPerson($payerPerson) {
        $this->payerPerson = $payerPerson;
    }

    public function getPayerPerson() {
        return $this->payerPerson;
    }

    public function setAgent($agent) {
        $this->agent = $agent;
    }

    public function getAgent() {
        return $this->agent;
    }

    public function hasAgent() {
        return  $this->agent instanceof AbstractAgent;
    }
}
