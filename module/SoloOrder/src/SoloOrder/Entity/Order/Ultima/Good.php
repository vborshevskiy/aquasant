<?php

namespace SoloOrder\Entity\Order\Ultima;

use SoloOrder\Entity\Order\AbstractGood;

/**
 * OrderGood
 *
 * @author Slava Tutrinov
 */
class Good extends AbstractGood {

//    const DELIVERY_GOOD_ID = 140164;
//
//    private static $deliveryGoods = array(
//        166965,
//        181503,
//        181502,
//        181504,
//        183321,
//        183322,
//        140166,
//        188690,
//        176037,
//        140164,
//        140165
//    );
    
    private $purchasePrice = 0;
    
    private $maxPrice = 0;
    
    private $bonuses = 0;
    
    private $changedAmount = 0;
    
    private $volume = 0;
    
    private $weight = 0;

    private $packageComponents;

    public function getChangedAmount() {
        return $this->changedAmount;
    }

    public function setChangedAmount($changedAmount) {
        $this->changedAmount = $changedAmount;
    }

    public function getPurchasePrice() {
        return $this->purchasePrice;
    }

    public function setPurchasePrice($purchasePrice) {
        $this->purchasePrice = $purchasePrice;
    }

    public function getBonuses() {
        return $this->bonuses;
    }

    public function setBonuses($bonuses) {
        $this->bonuses = $bonuses;
    }

    public function getMaxPrice() {
        return $this->maxPrice;
    }

    public function setMaxPrice($maxPrice) {
        $this->maxPrice = $maxPrice;
    }
    
    public function getVolume() {
        return $this->volume;
    }
    
    public function setVolume($volume) {
        $this->volume = (float)$volume;
    }
    
    public function getWeight() {
        return $this->weight;
    }

    public function setWeight($weight) {
        $this->weight = (float)$weight;
    }

//    public static function deliveryGoods($deliveryGoods = null) {
//        if ($deliveryGoods !== null) {
//            self::$deliveryGoods = $deliveryGoods;
//        }
//        return self::$deliveryGoods;
//    }

    public function setPackageComponents($packageComponents) {
        $this->packageComponents = $packageComponents;
    }

    public function getPackageComponents() {
        return $this->packageComponents;
    }

}
