<?php

namespace SoloOrder\Entity\Order\Ultima;

class Delivery extends \SoloOrder\Entity\Order\AbstractDelivery {
    
    protected $deliveryTimeId = null;
    
    protected $deliveryComments = null;
    
    protected $textDeliveryDate = '';

    protected $address = '';

    protected $liftToTheFloor = false;

    protected $floor = null;

    protected $elevatorTypeId = null;

    public function getDeliveryTimeId() {
        return $this->deliveryTimeId;
    }

    public function setDeliveryTimeId($deliveryTimeId) {
        $this->deliveryTimeId = (int)$deliveryTimeId;
    }

    public function getDeliveryComments() {
        return $this->deliveryComments;
    }

    public function setDeliveryComments($deliveryComments) {
        $this->deliveryComments = $deliveryComments;
    }
    
    public function setTextDeliveryDate($textDeliveryDate) {
        $this->textDeliveryDate = $textDeliveryDate;
    }
    
    public function getTextDeliveryDate() {
        return $this->textDeliveryDate;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function getAddress() {
        return $this->address;
    }

    public function needLiftToTheFloor(): bool {
        return $this->liftToTheFloor;
    }

    public function setLiftToTheFloor(bool $liftToTheFloor) {
        $this->liftToTheFloor = $liftToTheFloor;
    }

    public function getFloor()
    {
        return $this->floor;
    }

    public function setFloor($floor) {
        $this->floor = $floor;
    }

    public function getElevatorTypeId() {
        return $this->elevatorTypeId;
    }

    public function setElevatorTypeId($elevatorTypeId) {
        $this->elevatorTypeId = $elevatorTypeId;
    }
    
}
