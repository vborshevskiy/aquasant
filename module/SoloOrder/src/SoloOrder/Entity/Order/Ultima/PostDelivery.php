<?php

namespace SoloOrder\Entity\Order\Ultima;

class PostDelivery extends \SoloOrder\Entity\Order\AbstractDelivery {

    /**
     *
     * @var integer
     */
    protected $transportCompanyErpId = -1;
    
    /**
     *
     * @var string
     */
    protected $transportCompanyUid = null;
    
    /**
     *
     * @var string
     */
    protected $userName = null;
    
    /**
     *
     * @var integer
     */
    protected $fakeTimeId = 1;
    
    /**
     *
     * @var \DateTime
     */
    protected $minDeliveryDate = null;
    
    /**
     *
     * @var \DateTime
     */
    protected $maxDeliveryDate = null;
    
    /**
     *
     * @var float
     */
    protected $twinLogisticCost = 0;

    /**
     * 
     * @return integer
     */
    public function getTransportCompanyErpId() {
        return $this->transportCompanyErpId;
    }
    
    /**
     * 
     * @param integer $transportCompanyErpId
     */
    public function setTransportCompanyErpId($transportCompanyErpId) {
        $this->transportCompanyErpId = $transportCompanyErpId;
    }

    /**
     * 
     * @return string
     */
    public function getTransportCompanyUid() {
        return $this->transportCompanyUid;
    }
    
    /**
     * 
     * @param string $uid
     */
    public function setTransportCompanyUid($uid) {
        $this->transportCompanyUid = $uid;
    }
    
    /**
     * 
     * @return string
     */
    public function getUserName() {
        return $this->userName;
    }

    /**
     * 
     * @param string $userName
     */
    public function setUserName($userName) {
        $this->userName = $userName;
    }
    
    /**
     * 
     * @return integer
     */
    public function getFakeDeliveryTimeId() {
        return $this->fakeTimeId;
    }
    
    /**
     * 
     * @param integer $timeId
     */
    public function setFakeDeliveryTimeId($timeId) {
        $this->fakeTimeId = $timeId;
    }
    
    /**
     * 
     * @return \DateTime
     */
    public function getMinDeliveryDate() {
        return $this->minDeliveryDate;
    }
    
    /**
     * 
     * @param \DateTime $minDeliveryDate
     */
    public function setMinDeliveryDate(\DateTime $minDeliveryDate) {
        $this->minDeliveryDate = $minDeliveryDate;
    }
    
    /**
     * 
     * @return \DateTime
     */
    public function getMaxDeliveryDate() {
        return $this->maxDeliveryDate;
    }
    
    /**
     * 
     * @param \DateTime $maxDeliveryDate
     */
    public function setMaxDeliveryDate(\DateTime $maxDeliveryDate) {
        $this->maxDeliveryDate = $maxDeliveryDate;
    }
    
    /**
     * 
     * @return float
     */
    public function getTwinLogisticCost() {
        return $this->twinLogisticCost;
    }

    /**
     * 
     * @param float $twinLogisticCost
     */
    public function setTwinLogisticCost($twinLogisticCost) {
        $this->twinLogisticCost = (float)$twinLogisticCost;
    }

    /**
     * 
     * @return string
     */
    public function getTextDeliveryPossibleDate() {
        if ($this->minDeliveryDate instanceof \DateTime && $this->maxDeliveryDate instanceof \DateTime) {
            if (\SoloCatalog\Service\Helper\DateHelper::isIdenticalDates($this->minDeliveryDate, $this->maxDeliveryDate)) {
                return \SoloCatalog\Service\Helper\DateHelper::getTextDate($this->minDeliveryDate);
            }
            return \SoloCatalog\Service\Helper\DateHelper::getTextDate($this->minDeliveryDate) . ' - ' . \SoloCatalog\Service\Helper\DateHelper::getTextDate($this->maxDeliveryDate);
        }
        return null;
    }

    /**
     * 
     * @return string
     */
    public function getDeliveryDate() {
        return \SoloCatalog\Service\Helper\DateHelper::getSerializeJsonDate(parent::getDeliveryDate());
    }
    
}