<?php

namespace SoloOrder\Entity\Order\Ultima;
use SoloIdentity\Entity\AbstractAgent;

/**
 * OrderStorage
 *
 * @author Slava Tutrinov
 */
class Storage extends \SoloOrder\Entity\Order\AbstractStorage {

    public function setId($orderId) {
        $this->id = (int)$orderId;
    }

    public function getId() {
        return $this->id;
    }

    public function hasId() {
        return ($this->id > 0);
    }

    public function setStoreId($storeId) {
        $this->storeId = (int)$storeId;
    }

    public function getStoreId() {
        if ($this->hasStoreId()) {
            return $this->storeId;
        }
        return null;
    }

    public function hasStoreId() {
        return ($this->storeId > 0);
    }

    public function setPaymentId($paymentId) {
        $this->paymentId = (int)$paymentId;
    }

    public function getPaymentId() {
        if ($this->hasPaymentId()) {
            return $this->paymentId;
        }
        return null;
    }

    public function hasPaymentId() {
        return ($this->paymentId > 0);
    }

    public function setYandexKassaPaymentTerm($paymentTerm) {
        $this->yandexKassaPaymentTerm = $paymentTerm;
    }

    public function getYandexKassaPaymentTerm() {
        if ($this->hasYandexKassaPaymentTerm()) {
            return $this->yandexKassaPaymentTerm;
        }
        return null;
    }

    public function hasYandexKassaPaymentTerm() {
        return !empty($this->yandexKassaPaymentTerm);
    }

    public function getOrdersQueue() {
        $queue = isset($this->ordersQueue)?$this->ordersQueue:null;
        return $queue;
    }

    public function setOrdersQueue(OrdersQueue $orderQueue) {
        $this->ordersQueue = $ordersQueue;
    }

    public function getNextNotProccessedOrder() {
        foreach ($this->ordersQueue as $order) {
            if (!$order->proccessed()) {
                return $order;
            }
        }
        return null;
    }

    public function clearOrdersQueue() {
        if (isset($this->ordersQueue)) {
            unset($this->ordersQueue);
        }
    }

    public function isInitializedOrdersQueue() {
        return (isset($this->ordersQueue) && $this->ordersQueue instanceof OrdersQueue && count($this->ordersQueue) > 0);
    }

    public function getSavedOrderFromQueue($orderId) {
        $queue = $this->ordersQueue;
        return (isset($queue[$orderId]) ? $queue[$orderId] : null);
    }

    public function getOrdersQueueSize() {
        if (isset($this->ordersQueue)) {
            return count($this->ordersQueue);
        } else {
            return 0;
        }
    }

    public function geOrderIndexInQueue($orderId) {
        return array_search($orderId, array_keys($this->ordersQueue)) + 1;
    }

    public function orderQueueIsEmpty() {
        return !(isset($this->ordersQueue) && count($this->ordersQueue) > 0);
    }

    public function markOrderInQueueAsProccessed($orderId) {
        $queue = $this->ordersQueue;
        $order = $queue[$orderId];
        $order->proccessed(true);
        $queue->add($order);
        $this->ordersQueue = $queue;
    }

    public function syncOrdersQueue(OrdersQueue $ordersQueue) {
        $queue = $this->ordersQueue;
        $hasNewOrders = false;
        foreach ($ordersQueue as $order) {
            if (!isset($queue[$order->getId()])) {
                $hasNewOrders = true;
                $queue[$bOrder->getId()] = $order;
            }
        }
        $result = [];
        if ($hasNewOrders) {
            foreach ($queue as $key => $co) {
                if (!$co->proccessed()) {
                    $result[$key] = $co;
                }
            }
        }
        $this->ordersQueue = ($hasNewOrders) ? $result : $queue;
    }

    public function hasNotProccessedOrders() {
        $count = 0;
        foreach ($this->ordersCollection as $or) {
            if (!$or['proccessed'])
                $count++;
        }
        return ($count > 0 && sizeof($this->ordersCollection > 0));
    }

//    public function getNotAvailGoods($orderId) {
//        $key = $this->prefix . $orderId;
//        $orderSession = $this->$key;
//        if (isset($orderSession->notAvailGoods)) {
//            return $orderSession->notAvailGoods;
//        }
//        return null;
//    }

    public function setDeniedBonus($orderId, $deniedBonus) {
        $key = $this->getPrefix() . $orderId;
        $b = $this->$key;
        $b->noBonuses = $deniedBonus;
        $this->$key = $b;
    }

    public function getDeniedBonus($orderId) {
        $key = $this->prefix . $orderId;
        $orderSession = $this->$key;
        return $orderSession->noBonuses;
    }

    /**
     * Удаление товара из заказа
     *
     * @param int $orderId
     *        	идентификатор заказа
     * @param int $goodId
     *        	идентификатор товара (позиции) в заказе
     *
     * @return bool
     */
    public function removeGood($orderId, $goodId) {
        if ($this->orderExists($orderId)) {

            $key = $this->getPrefix() . $orderId;

            if (!empty($this->$key->goods[$goodId])) {

                $b = $this->$key;
                $goods = $b->goods;
                unset($goods[$goodId]);
                $b->goods = $goods;

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function setNotAvailGoods($notAvailGoods) {
        $this->notAvailGoods = $notAvailGoods;
    }

    public function getNotAvailGoods() {
        return $this->notAvailGoods;
    }

    public function hasNotAvailGoods() {
        return (isset($this->notAvailGoods) && is_array($this->notAvailGoods) && count($this->notAvailGoods) > 0);
    }

    public function setAmount($amount) {
        $this->amount = (float)$amount;
    }

    public function getAmount() {
        return $this->amount;
    }

    public function hasAmount() {
        return ($this->amount > 0);
    }



    public function setDeliveryAddressId($deliveryAddressId) {
        $this->deliveryAddressId = (int)$deliveryAddressId;
    }

    public function getDeliveryAddressId() {
        return $this->deliveryAddressId;
    }

    public function hasDeliveryAddressId() {
        return ($this->deliveryAddressId > 0);
    }

    public function setDeliveryCost($deliveryCost) {
        $this->deliveryCost = (float)$deliveryCost;
    }

    public function getDeliveryCost() {
        return $this->deliveryCost;
    }

    public function hasDeliveryCost() {
        return ($this->deliveryCost > 0);
    }

    public function setTwinLogisticCost($twinLogisticCost) {
        $this->twinLogisticCost = (float)$twinLogisticCost;
    }

    public function getTwinLogisticCost() {
        return $this->twinLogisticCost;
    }

    public function hasTwinLogisticCost() {
        return ($this->twinLogisticCost > 0);
    }

    public function setDeliveryDate($deliveryDate) {
        $this->deliveryDate = serialize($deliveryDate);
    }

    public function getDeliveryDate() {
        return unserialize($this->deliveryDate);
    }

    public function hasDeliveryDate() {
        return (!is_null($this->deliveryDate));
    }

     public function setDeliveryTimeId($deliveryTimeId) {
        $this->deliveryTimeId = (int)$deliveryTimeId;
    }

    public function getDeliveryTimeId() {
        return $this->deliveryTimeId;
    }

    public function hasDeliveryTimeId() {
        return ($this->deliveryTimeId > 0);
    }

    public function hasDelivery() {
        return (($this->hasDeliveryAddressId() || $this->hasDeliveryAddress()) && $this->hasDeliveryDate() && $this->hasDeliveryTimeId());
    }

    public function hasManuallyEnteredDelivery() {
        return false;
        //return ($this->hasDeliveryAddressId() == \SoloOrder\Service\Ultima\OrderService::MANUALLY_ENTERED_ADDRESS_ID && $this->hasDeliveryDate() && $this->hasDeliveryTimeId());
    }

    public function hasPostDelivery() {
        return ($this->hasDeliveryAddressId() && $this->hasDeliveryCost() && $this->hasDeliveryTransportCompanyId());
    }

    public function hasPostPickup() {
        return ($this->hasDeliveryAddressId() && $this->hasDeliveryCost() && $this->hasDeliveryTransportCompanyErpId() && !$this->hasDeliveryTransportCompanyId());
    }

    public function setDeliveryInfo($deliveryInfo) {
        $this->deliveryInfo = $deliveryInfo;
    }

    public function getDeliveryInfo() {
        return $this->deliveryInfo;
    }

    public function setDeliveryPhone($deliveryPhone) {
        $this->deliveryPhone = $deliveryPhone;
    }

    public function getDeliveryPhone() {
        return $this->deliveryPhone;
    }

    public function hasDeliveryPhone() {
        return (!is_null($this->deliveryPhone));
    }

    public function setTextDeliveryDate($textDeliveryDate) {
        $this->textDeliveryDate = $textDeliveryDate;
    }

    public function getTextDeliveryDate() {
        return $this->textDeliveryDate;
    }

    public function hasTextDeliveryDate() {
        return (!is_null($this->textDeliveryDate));
    }

    public function setPhone($phone) {
        $this->phone = $phone;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function hasPhone() {
        return (!is_null($this->phone));
    }

	public function setContactName($contactName) {
        $this->contactName = $contactName;
    }

    public function getContactName() {
        return $this->contactName;
    }

    public function hasContactName() {
        return (!is_null($this->contactName));
    }

    public function setComment($comment) {
    	$this->comment = $comment;
    }

    public function getComment() {
    	return $this->comment;
    }
    
    public function setDeliveryComment($comment) {
    	$this->deliveryComment = $comment;
    }
    
    public function getDeliveryComment() {
    	return $this->deliveryComment;
    }
    
    public function setSource($source) {
    	$this->source = $source;
    }
    
    public function getSource() {
    	return $this->source;
    }

    public function setManagerId($managerId) {
    	$this->managerId = $managerId;
    }

    public function getManagerId() {
    	return $this->managerId;
    }

    public function hasComment() {
    	return (!is_null($this->comment));
    }

    public function hasManagerId() {
    	return (!is_null($this->managerId));
    }

    public function setUserName($userName) {
        $this->userName = $userName;
    }

    public function getUserName() {
        return $this->userName;
    }

    public function hasUserName() {
        return (!is_null($this->userName));
    }

	public function setUserEmail($userEmail) {
        $this->userEmail = $userEmail;
    }

    public function getUserEmail() {
        return $this->userEmail;
    }

    public function hasUserEmail() {
        return (!is_null($this->userEmail));
    }

    public function setPickupDate($pickupDate) {
        $this->pickupDate = $pickupDate;
    }

    public function getPickupDate() {
        return $this->pickupDate;
    }

    public function hasPickupDate() {
        return (!is_null($this->pickupDate));
    }

    public function setAgentId($agentId) {
        $this->agentId = $agentId;
    }

    public function getAgentId() {
        return $this->agentId;
    }

    public function hasAgentId() {
        return (!is_null($this->agentId));
    }

    public function setBonusAmount($bonusAmount) {
        $this->bonusAmount = $bonusAmount;
    }

    public function getBonusAmount() {
        return $this->bonusAmount;
    }

    public function hasBonusAmount() {
        return (!is_null($this->bonusAmount) && $this->bonusAmount > 0);
    }

    public function setCityId($cityId) {
        $this->cityId = $cityId;
    }

    public function getCityId() {
        return $this->cityId;
    }

    public function hasCityId() {
        return (!is_null($this->cityId) && $this->cityId > 0);
    }

    public function setDeliveryTransportCompanyErpId($transportCompanyErpId) {
        $this->transportCompanyErpId = $transportCompanyErpId;
    }

    public function getDeliveryTransportCompanyErpId() {
        return $this->transportCompanyErpId;
    }

    public function hasDeliveryTransportCompanyErpId() {
        return (!is_null($this->transportCompanyErpId));
    }

    public function setDeliveryTransportCompanyId($transportCompanyId) {
        $this->transportCompanyId = $transportCompanyId;
    }

    public function getDeliveryTransportCompanyId() {
        return $this->transportCompanyId;
    }

    public function hasDeliveryTransportCompanyId() {
        return (!is_null($this->transportCompanyId));
    }

    public function setFakeDeliveryTimeId($fakeDeliveryTimeId) {
        $this->fakeDeliveryTimeId = $fakeDeliveryTimeId;
    }

    public function getFakeDeliveryTimeId() {
        return $this->fakeDeliveryTimeId;
    }

    public function hasFakeDeliveryTimeId() {
        return (!is_null($this->fakeDeliveryTimeId));
    }

    public function setPostDeliveryMinDeliveryDate($postDeliveryMinDeliveryDate) {
        $this->postDeliveryMinDeliveryDate = $postDeliveryMinDeliveryDate;
    }

    public function getPostDeliveryMinDeliveryDate() {
        return $this->postDeliveryMinDeliveryDate;
    }

    public function hasPostDeliveryMinDeliveryDate() {
        return (!is_null($this->postDeliveryMinDeliveryDate));
    }

    public function setPostDeliveryMaxDeliveryDate($postDeliveryMaxDeliveryDate) {
        $this->postDeliveryMaxDeliveryDate = $postDeliveryMaxDeliveryDate;
    }

    public function getPostDeliveryMaxDeliveryDate() {
        return $this->postDeliveryMaxDeliveryDate;
    }

    public function hasPostDeliveryMaxDeliveryDate() {
        return (!is_null($this->postDeliveryMaxDeliveryDate));
    }

    public function clearStorage() {
        $this->notAvailGoods = null;
        $this->amount = null;
        $this->deliveryAddressId = null;
        $this->deliveryCost = null;
        $this->twinLogisticCost = null;
        $this->deliveryDate = null;
        $this->deliveryTimeId = null;
        $this->deliveryInfo = null;
        $this->deliveryPhone = null;
        $this->textDeliveryDate = null;
        $this->phone = null;
		$this->contactName = null;
        $this->comment = null;
        $this->deliveryComment = null;
        $this->source = null;
        $this->managerId = null;
        $this->userName = null;
		$this->userEmail = null;
        $this->pickupDate = null;
        $this->agentId = null;
        $this->bonusAmount = null;
        $this->cityId = null;
        $this->transportCompanyId = null;
        $this->yandexKassaPaymentTerm = null;
        $this->transportCompanyErpId = null;
        $this->postDeliveryMinDeliveryDate = null;
        $this->postDeliveryMaxDeliveryDate = null;
        $this->obtainMethod = null;
        $this->agent = null;
        $this->paymentId = null;
    }

    public function setObtainMethod($obtainMethod) {
        $this->obtainMethod = $obtainMethod;
    }

    public function getObtainMethod() {
        return $this->obtainMethod;
    }

    public function hasObtainMethod() {
        return (bool) $this->obtainMethod;
    }

    public function setPayerPerson($payerPerson) {
        $this->payerPerson = $payerPerson;
    }

    public function getPayerPerson() {
        return $this->payerPerson;
    }

    public function hasPayerPerson() {
        return (bool) $this->payerPerson;
    }

    public function setDeliveryAddress($deliveryAddress) {
        $this->deliveryAddress = $deliveryAddress;
    }

    public function getDeliveryAddress() {
        return $this->deliveryAddress;
    }

    public function hasDeliveryAddress() {
        return (bool) $this->deliveryAddress;
    }

    public function setAgent($agent) {
        $this->agent = $agent;
    }

    public function getAgent() {
        return $this->agent;
    }

    public function hasAgent() {
        return $this->agent instanceof AbstractAgent;
    }

    public function setDeliveryLiftToTheFloor($liftToTheFloor) {
        $this->liftToTheFloor = $liftToTheFloor;
    }

    public function getDeliveryLiftToTheFloor() {
        return $this->liftToTheFloor;
    }

    public function hasDeliveryLiftToTheFloor() {
        return (bool) $this->liftToTheFloor;
    }

    public function setDeliveryFloor($floor) {
        $this->floor = $floor;
    }

    public function getDeliveryFloor() {
        return $this->floor;
    }

    public function setDeliveryElevatorTypeId($elevatorTypeId) {
        $this->elevatorTypeId = $elevatorTypeId;
    }

    public function getDeliveryElevatorTypeId() {
        return $this->elevatorTypeId;
    }
}

