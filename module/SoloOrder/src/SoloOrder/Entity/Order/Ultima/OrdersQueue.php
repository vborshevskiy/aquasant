<?php
namespace SoloOrder\Entity\Order\Ultima;

/**
 * Description of OrdersQueue
 *
 * @author slava
 */
class OrdersQueue extends \Solo\Collection\Collection {
    
    public function add($object, $key = null) {
        if (!($object instanceof Order)) {
            throw new \InvalidArgumentException('Added object must be instance of \SoloOrder\Entity\Ultima\Order class');
        }
        parent::add($object, $object->getId());
    }
    
}

?>
