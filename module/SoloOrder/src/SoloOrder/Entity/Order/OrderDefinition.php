<?php
namespace SoloOrder\Entity\Order;

/**
 * Class OrderDefinition define order from basket
 * who decorate by OrderSettings that consist of this definition
 * 
 * @author slava
 */
class OrderDefinition extends \Solo\Collection\Collection {
    
    public function define($paramName, $paramValue = null) {
        if ($paramValue !== null) {
            parent::add($paramValue, $paramName);
            return;
        }
        return parent::offsetGet($paramName);
    }
    
}

?>
