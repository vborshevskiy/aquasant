<?php
namespace SoloOrder\Entity\Order;

/**
 * Description of ProvidesDelivery
 *
 * @author slava
 */
trait ProvidesDelivery {
    
    /**
     *
     * @var \SoloOrder\Entity\Order\AbstractDelivery
     */
    protected $delivery = null;
    
    /**
     * 
     * @param \SoloOrder\Entity\Order\AbstractDelivery $delivery
     */
    public function setDelivery(\SoloOrder\Entity\Order\AbstractDelivery $delivery) {
        $this->delivery = $delivery;
    }
    
    /**
     * 
     * @return \SoloOrder\Entity\Order\AbstractDelivery
     */
    public function getDelivery() {
        return $this->delivery;
    }
    
    /**
     * 
     * @return boolean
     */
    public function hasDelivery() {
        return ((null !== $this->delivery) && ($this->delivery instanceof \SoloOrder\Entity\Order\AbstractDelivery));
    }
    
}

?>
