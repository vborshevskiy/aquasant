<?php
namespace SoloOrder\Entity\Order;

/**
 * Description of ProvidesPostDelivery
 */
trait ProvidesPostDelivery {
    
    /**
     *
     * @var \SoloOrder\Entity\Order\AbstractDelivery
     */
    protected $postDelivery = null;
    
    /**
     * 
     * @param \SoloOrder\Entity\Order\AbstractDelivery $delivery
     */
    public function setPostDelivery(\SoloOrder\Entity\Order\AbstractDelivery $delivery) {
        $this->postDelivery = $delivery;
    }
    
    /**
     * 
     * @return \SoloOrder\Entity\Order\AbstractDelivery
     */
    public function getPostDelivery() {
        return $this->postDelivery;
    }
    
    /**
     * 
     * @return boolean
     */
    public function hasPostDelivery() {
        return ((null !== $this->postDelivery) && ($this->postDelivery instanceof \SoloOrder\Entity\Order\AbstractDelivery));
    }
    
}
