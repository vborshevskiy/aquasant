<?php

namespace SoloOrder\Entity\Order;

/**
 * Order
 *
 * @author Slava Tutrinov
 */
abstract class AbstractOrder {

    const PAYMENT_CASH_ID = 3;
    const PAYMENT_CASHLESS_ID = 8;
    const PAYMENT_YANDEX_KASSA = 2;
    const PAYMENT_ALFABANK_ID = 7;
    const DELIVERY_OBTAIN_METHOD = 'delivery';
    const PICKUP_OBTAIN_METHOD = 'ownStorePickup';
    const MOSCOW_MAIN_STORE_ID = 6;
    const MOSCOW_MAIN_PICKUP_STORE_ID = 509;
    const MOSCOW_CITY_ID = 2;
    const FAKE_DELIVERY_ADDRESS = 321;
    const OWN_DELIVERY = 'own';
    const OUTSOURSE_DELIVERY = 'outsource';
    const HYBRID_DELIVERY = 'hybrid';
    const DELIVERY_FAKE_TIME_ID = 2;
    const DEFAULT_FIRST_UPDATE_VERSION = 2;

    protected $id = null;

    /**
     *
     * @var \SoloOrder\Entity\Order\OrderGoodsCollection
     */
    protected $goodsCollection = null;
    
    /**
     *
     * @var integer
     */
    protected $warehouseId = null;
    
    /**
     *
     * @var \SoloOrder\Entity\Order\AbstractSettings
     */
    protected $settings = null;

    /**
     *
     * @var integer
     */
    protected $agentId = null;
    
    /**
     *
     * @var integer
     */
    protected $bonusAmount = 0;

    public function __construct($id = null, \SoloOrder\Entity\Order\GoodsCollection $orderGoodsCollection = null) {
        if (null !== $id) {
            $this->setId($id);
        }
        if (null !== $orderGoodsCollection) {
            $this->setOrderGoodsCollection($orderGoodsCollection);
        }
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getGoodsCollection() {
        return $this->goodsCollection;
    }

    public function setGoodsCollection($goodsCollection) {
        $this->goodsCollection = $goodsCollection;
    }

    public function addGood(\SoloOrder\Entity\Order\AbstractGood $orderGood) {
        $this->goodsCollection->add($orderGood);
    }

    public function removeGood($articul) {
        if (isset($this->goodsCollection[$articul])) {
            unset($this->goodsCollection[$articul]);
        }
    }

    public function getWarehouseId() {
        return $this->warehouseId;
    }

    public function setWarehouseId($warehouseId) {
        $this->warehouseId = $warehouseId;
    }
    
    public function settings(AbstractSettings $settings = null) {
        if ($settings !== null) {
            $this->settings = $settings;
            return;
        }
        return $this->settings;
    }
    
    public function setting($key, $value = null) {
        if ($value !== null) {
            $this->settings->setting($key, $value);
            return;
        }
        return $this->settings->setting($key);
    }
    
    public function getFakeDeliveryInfo($fakeDate) {
        return [
            'Option' => self::OWN_DELIVERY,
            'Date' => $fakeDate,
            'TimeId' => self::DELIVERY_FAKE_TIME_ID,
            'AddressId' => self::FAKE_DELIVERY_ADDRESS,
        ];
    }
    
    public function setAgentId($agentId) {
        $this->agentId = $agentId;
        return $agentId;
    }
    
    public function getAgentId() {
        return $this->agentId;
    }
    
    public function hasAgentId() {
        return (!is_null($this->agentId));
    }
    
    public function setBonusAmount($bonusAmount) {
        $this->bonusAmount = $bonusAmount;
    }
    
    public function getBonusAmount() {
        return $this->bonusAmount;
    }
    
    public function hasBonusAmount() {
        return ($this->bonusAmount > 0);
    }

}
