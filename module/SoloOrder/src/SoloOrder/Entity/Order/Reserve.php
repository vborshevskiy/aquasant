<?php

namespace SoloOrder\Entity\Order;

/**
 * Reserve
 *
 * @author Slava Tutrinov
 */
class Reserve {

	const DELIVERY_TYPE_SHOP = 'shop';

	const DELIVERY_TYPE_TRANSPORT = 'transport';

	const PAYMENT_TYPE_CASH = 2;

	const PAYMENT_TYPE_CASHLESS = 1;

	protected $documentId = null;

	protected $buhId = null;

	protected $payTypeId = null;

	protected $documentNo = null;

	protected $createDate = null;

	protected $deadReserveDateTime = null;

	protected $amount = null;

	protected $buhNo = null;

	protected $isDelivery = null;

	protected $warehouseId = null;

	protected $deliveryAddressId = null;

	protected $status = null;

	protected $baId = null;

	protected $typeReserve = null;

	protected $bonusPayAmount = null;

	public function getBonusPayAmount() {
		return $this->bonusPayAmount;
	}

	public function setBonusPayAmount($bonusPayAmount) {
		$this->bonusPayAmount = $bonusPayAmount;
	}

	public function getTypeReserve() {
		return $this->typeReserve;
	}

	public function setTypeReserve($typeReserve) {
		$this->typeReserve = $typeReserve;
	}

	public function getDocumentId() {
		return $this->documentId;
	}

	public function setDocumentId($documentId) {
		$this->documentId = $documentId;
	}

	public function getBuhId() {
		return $this->buhId;
	}

	public function setBuhId($buhId) {
		$this->buhId = $buhId;
	}

	public function getPayTypeId() {
		return $this->payTypeId;
	}

	public function setPayTypeId($payTypeId) {
		$this->payTypeId = $payTypeId;
	}

	public function getBaId() {
		return $this->baId;
	}

	/**
	 * Set business agent id (or natural person id)
	 *
	 * @param int $baId        	
	 */
	public function setBaId($baId) {
		$this->baId = $baId;
	}

	public function getDocumentNo() {
		return $this->documentNo;
	}

	public function setDocumentNo($documentNo) {
		$this->documentNo = $documentNo;
	}

	public function getCreateDate() {
		return $this->createDate;
	}

	public function setCreateDate($createDate) {
		$this->createDate = $createDate;
	}

	public function getDeadReserveDateTime() {
		return $this->deadReserveDateTime;
	}

	public function setDeadReserveDateTime($deadReserveDateTime) {
		$this->deadReserveDateTime = $deadReserveDateTime;
	}

	public function getAmount() {
		return $this->amount;
	}

	public function setAmount($amount) {
		$this->amount = $amount;
	}

	public function getBuhNo() {
		return $this->buhNo;
	}

	public function setBuhNo($buhNo) {
		$this->buhNo = $buhNo;
	}

	public function getIsDelivery() {
		return $this->isDelivery;
	}

	public function setIsDelivery($isDelivery) {
		$this->isDelivery = $isDelivery;
	}

	public function getWarehouseId() {
		return $this->warehouseId;
	}

	public function getDeliveryAddressId() {
		return $this->deliveryAddressId;
	}

	public function setDeliveryAddressId($deliveryAddressId) {
		$this->deliveryAddressId = $deliveryAddressId;
	}

	public function setWarehouseId($warehouseId) {
		$this->warehouseId = $warehouseId;
	}

	public function getStatus() {
		return $this->status;
	}

	public function setStatus($status) {
		$this->status = $status;
	}

}

?>
