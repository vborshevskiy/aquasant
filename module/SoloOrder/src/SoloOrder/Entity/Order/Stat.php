<?php

namespace SoloOrder\Entity\Order;

/**
 * Description of Stat
 *
 * @author slava
 */
abstract class Stat {

	private $allNum = null;

	private $maxDate = null;

	private $minDate = null;

	private $maxCost = null;

	private $minCost = null;

	public function getAllNum() {
		return $this->allNum;
	}

	public function setAllNum($allNum) {
		$this->allNum = $allNum;
	}

	public function getMaxDate() {
		return $this->maxDate;
	}

	public function setMaxDate($maxDate) {
		$this->maxDate = $maxDate;
	}

	public function getMinDate() {
		return $this->minDate;
	}

	public function setMinDate($minDate) {
		$this->minDate = $minDate;
	}

	public function getMaxCost() {
		return $this->maxCost;
	}

	public function setMaxCost($maxCost) {
		$this->maxCost = $maxCost;
	}

	public function getMinCost() {
		return $this->minCost;
	}

	public function setMinCost($minCost) {
		$this->minCost = $minCost;
	}

	public function getMaxDateTimestamp() {
		$timestamp = strtotime($this->maxDate);
		$day = date("d", $timestamp);
		$month = date("m", $timestamp);
		$year = date("Y", $timestamp);
		return mktime(0, 0, 0, $month, $day, $year);
	}

	public function getMinDateTimestamp() {
		$timestamp = strtotime($this->minDate);
		$day = date("d", $timestamp);
		$month = date("m", $timestamp);
		$year = date("Y", $timestamp);
		return mktime(0, 0, 0, $month, $day, $year);
	}

}

?>
