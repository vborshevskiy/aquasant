<?php
namespace SoloOrder\Entity\Order;

/**
 * Description of AbstractSettings
 *
 * @author slava
 */
abstract class AbstractSettings {

    protected $settings = array();

    /**
     *
     * @var \SoloOrder\Entity\Order\OrderDefinition
     */
    protected $orderDefinition = null;

    public function cashless($cashless = null) {
        if (null !== $cashless) {
            $this->setting('cashless', (bool)$cashless);
            return;
        }
        return $this->settings('cashless');
    }

    public function setting($name, $value = null) {
        if ($value !== null) {
            $this->settings[$name] = $value;
            return;
        }
        return (array_key_exists($name, $this->settings)?$this->settings[$name]:null);
    }

    /**
     *
     * @return \SoloOrder\Entity\Order\OrderDefinition
     */
    public function getOrderDefinition() {
        return $this->orderDefinition;
    }

    /**
     *
     * @param \SoloOrder\Entity\Order\OrderDefinition $orderDefinition
     */
    public function setOrderDefinition(\SoloOrder\Entity\Order\OrderDefinition $orderDefinition) {
        $this->orderDefinition = $orderDefinition;
    }

}

?>
