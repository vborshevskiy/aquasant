<?php

namespace SoloOrder\Entity\Order;

/**
 * 
 * Description of ProvidesPostPickup
 */
trait ProvidesPostPickup {
    
    /**
     *
     * @var \SoloOrder\Entity\Order\AbstractDelivery
     */
    protected $postPickup = null;
    
    /**
     * 
     * @param \SoloOrder\Entity\Order\AbstractDelivery $delivery
     */
    public function setPostPickup(\SoloOrder\Entity\Order\AbstractDelivery $delivery) {
        $this->postPickup = $delivery;
    }
    
    /**
     * 
     * @return \SoloOrder\Entity\Order\AbstractDelivery
     */
    public function getPostPickup() {
        return $this->postPickup;
    }
    
    /**
     * 
     * @return boolean
     */
    public function hasPostPickup() {
        return ($this->postPickup instanceof \SoloOrder\Entity\Order\AbstractDelivery);
    }
    
}
