<?php

namespace SoloOrder\Entity\Order;

/**
 * AbstractOrderStorage
 *
 * @author Slava Tutrinov
 */
abstract class AbstractStorage extends \Zend\Session\Container {

    private $prefix = "order-";

    public function getPrefix() {
        return $this->prefix;
    }

    public function setPrefix($prefix) {
        $this->prefix = $prefix;
    }
    
    public function getSettingsCollection() {
        if (isset($this->settingsCollection)) {
            return $this->settingsCollection;
        }
        return null;
    }
    
    public function setSettingsCollection(SettingsCollection $collection) {
        $this->settingsCollection = $collection;
    }
    
    public function addSettingsGroup(AbstractSettings $settingsGroup) {
        if (!isset($this->settingsCollection)) {
            $this->settingsCollection = new SettingsCollection;
        }
        $this->settingsCollection->addSettingsGroup($settingsGroup);
    }
    
    public function updateSettingsGroup(AbstractSettings $settings) {
        foreach ($this->settingsCollection as $index => $set) {
            if ($settings->getOrderDefinition()->toArray() === $set->getOrderDefinition()->toArray()) {
                $this->settingsCollection[$index] == $settings;
            }
        }
    }
    
    public function removeSettingsGroup(AbstractSettings $setting) {
        foreach ($this->settingsCollection as $index => $settings) {
            if ($settings->getOrderDefinition()->toArray() === $setting->getOrderDefinition()->toArray()) {
                unset($this->settingsCollection[$index]);
            }
        }
    }
    
    public function getSettingsGroup(OrderDefinition $definition) {
        return $this->settingsCollection->getSettingsGroup($definition);
    }

}

