<?php
namespace SoloOrder\Entity\Order;

/**
 * Description of SettingsCollection
 *
 * @author slava
 */
class SettingsCollection extends \Solo\Collection\Collection {
    
    public function getSettingsGroup(OrderDefinition $orderDefinition) {
        $orderDefinition = $orderDefinition->toArray();
        if (!array_key_exists('delivery', $orderDefinition)) {
            $orderDefinition['delivery'] = null;
        } elseif (!array_key_exists('warehouseId', $orderDefinition)) {
            $orderDefinition['warehouseId'] = null;
            $orderDefinition = array_reverse($orderDefinition, true);
        }
        foreach ($this->list as $index => $settingsGroup) {
            $definition = $settingsGroup->getOrderDefinition()->toArray();
            if (!array_key_exists('delivery', $definition)) {
                $definition['delivery'] = null;
            } elseif (!array_key_exists('warehouseId', $definition)) {
                $definition['warehouseId'] = null;
                $definition = array_reverse($definition, true);
            }
            if ($definition === $orderDefinition) {
                return $settingsGroup;
            }
        }
        return null;
    }
    
    public function addSettingsGroup(AbstractSettings $settingsGroup) {
        $this->add($settingsGroup);
    }
    
}

?>
