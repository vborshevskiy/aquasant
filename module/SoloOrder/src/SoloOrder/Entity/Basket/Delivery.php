<?php

namespace SoloOrder\Entity\Basket;

class Delivery extends AbstractDelivery {

    /**
     *
     * @var int
     */
    private $addressId;

    /**
     *
     * @var string
     */
    private $phone;

    /**
     *
     * @return the $addressId
     */
    public final function getAddressId() {
        return $this->addressId;
    }

    /**
     *
     * @param number $addressId        	
     */
    public final function setAddressId($addressId) {
        $this->addressId = $addressId;
    }

    /**
     *
     * @return the $phone
     */
    public final function getPhone() {
        return $this->phone;
    }

    public function getFormattedPhone() {
        preg_match('/^\+7(\d{3})(\d+)$/', $this->phone, $matches);
        return $matches[1] . "," . $matches[2];
    }

    /**
     *
     * @param string $phone        	
     */
    public final function setPhone($phone) {
        $this->phone = $phone;
    }

    public function getDeliveryCost() {
        return $this->getInfo('deliverycost');
    }

    public function generateFakeAddressId() {
        return (time() + rand(100, 1000));
    }

}
