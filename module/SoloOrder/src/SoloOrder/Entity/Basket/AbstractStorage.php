<?php
namespace SoloOrder\Entity\Basket;

/**
 * Description of AbstractBasketStorage
 *
 * @author slava
 */
abstract class AbstractStorage extends \Zend\Session\Container {
    
    /**
     *
     * @var \SoloOrder\Behavior\Good\GoodProcessingBehavior
     */
    protected $goodProcessingBehavior = null;
    
    public function __construct($name = 'Default', \Zend\Session\ManagerInterface $manager = null) {
        parent::__construct($name, $manager);
        if (!isset($this->goods)) {
            $this->goods = new \SoloOrder\Entity\Basket\GoodsCollection;
        }
    }
    
    public function getGoodsById($goodId) {
        $result = null;
        if (isset($this->goods)) {
            foreach ($this->goods as $index => $gd) {
                if ($goodId == $gd->getId()) {
                    $result[$index] = $gd;
                }
            }
        }
        return $result;
    }
    
    public function addGood(\SoloOrder\Entity\Basket\AbstractGood $good) {
        $this->getGoodProcessingBehavior()->addGood($good, $this);
    }
    
    public function removeGood(\SoloOrder\Entity\Basket\AbstractGood $good) {
        $this->getGoodProcessingBehavior()->removeGood($good, $this);
    }
    
    public function updateGood(\SoloOrder\Entity\Basket\AbstractGood $good) {
        $this->getGoodProcessingBehavior()->updateGood($good, $this);
    }
    
    public function getPrefix() {
        return $this->prefix;
    }

    public function setPrefix($prefix) {
        $this->prefix = $prefix;
    }
    
    public function getGoodProcessingBehavior() {
        return $this->goodProcessingBehavior;
    }

    public function setGoodProcessingBehavior(\SoloOrder\Service\Behavior\Good\GoodProcessingBehavior $goodProcessingBehavior) {
        $this->goodProcessingBehavior = $goodProcessingBehavior;
    }
    
    public function getGoodsCollection() {
        return isset($this->goods)?$this->goods:null;
    }
    
    public function setGoodsCollection(\SoloOrder\Entity\Basket\GoodsCollection $goodsCollection) {
        $this->goods = $goodsCollection;
    }
    
}

?>
