<?php

namespace SoloOrder\Entity\Basket;

abstract class AbstractGood {

    /**
     *
     * @var int
     */
    private $id;

    /**
     *
     * @var float
     */
    private $price = 0;

    /**
     *
     * @var int
     */
    private $quantity = 0;

    /**
     *
     * @var float
     */
    private $weight = 0;

    /**
     *
     * @var float
     */
    private $volume = 0;
    
    /**
     *
     * @var array
     */
    private $additionalValues = array();

    /**
     *
     * @param int $goodId        	
     * @param int $quantity
     * @paarm float $price
     */
    public function __construct($goodId = null, $quantity = null, $price = null, $weight = null, $volume = null) {
        if (null !== $goodId) {
            $this->setId($goodId);
        }
        if (null !== $quantity) {
            $this->setQuantity($quantity);
        }
        if (null !== $price) {
            $this->setPrice($price);
        }
        if (null !== $weight) {
            $this->setWeight($weight);
        }
        if (null !== $volume) {
            $this->setVolume($volume);
        }
    }

    /**
     *
     * @return the $id
     */
    public final function getId() {
        return $this->id;
    }

    /**
     *
     * @param number $id        	
     */
    public final function setId($id) {
        $this->id = $id;
    }

    /**
     *
     * @return the $price
     */
    public final function getPrice() {
        return $this->price;
    }

    public function getWeight() {
        return $this->weight;
    }

    public function setWeight($weight) {
        $this->weight = $weight;
    }

    /**
     *
     * @param number $price        	
     */
    public final function setPrice($price) {
        $this->price = $price;
    }

    /**
     *
     * @return the $quantity
     */
    public final function getQuantity() {
        return $this->quantity;
    }

    public function getVolume() {
        return $this->volume;
    }

    public function setVolume($volume) {
        $this->volume = (float)$volume;
    }

    /**
     *
     * @param number $quantity        	
     */
    public final function setQuantity($quantity) {
        $this->quantity = $quantity;
    }

    /**
     *
     * @return float
     */
    public function getAmount() {
        return floatval($this->getQuantity() * $this->getPrice());
    }

    public function additional($name, $value = null) {
        if (null !== $value) {
            $this->additionalValues[$name] = $value;
            return $this->additionalValues[$name];
        } else {
            if (array_key_exists($name, $this->additionalValues)) {
                return $this->additionalValues[$name];
            }
            return null;
        }
    }

}
