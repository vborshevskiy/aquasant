<?php

namespace SoloOrder\Entity\Basket;

/**
 * Description of AbstractDelivery
 *
 * @author slava
 */
class AbstractDelivery {

    protected $deliveryTime = 0;
    protected $deliveryDate = 0;
    protected $info = array();

    public function getDeliveryTime() {
        return $this->deliveryTime;
    }

    public function setDeliveryTime($deliveryTime) {
        $this->deliveryTime = $deliveryTime;
    }

    public function getDeliveryDate() {
        return $this->deliveryDate;
    }

    public function setDeliveryDate($deliveryDate) {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * sets info about address with this fields: street, house, case, building, room, ph-code, ph-num, text, str
     * Array (
     * [street] => Комсомольская
     * [house] => 12
     * [case] => 2 // корпус
     * [building] => 2А
     * [room] => 555
     * [ph-code] => 921
     * [ph-num] => 5557815
     * [text] => Москва, улица dsfsdf, д. 1
     * [str] => улица
     * )
     * 
     * @param array $info        	
     */
    public function setInfo($info) {
        $this->info = $info;
    }

    /**
     *
     * @return array
     */
    public function getInfo($param = null) {
        if (null !== $param) {
            if (isset($this->info[$param])) {
                return $this->info[$param];
            }
            return null;
        }
        return $this->info;
    }

}
