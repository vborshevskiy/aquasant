<?php
namespace SoloOrder\Entity\Basket;

/**
 * Description of ProvidesGoodsGrouping
 *
 * @author slava
 */
trait ProvidesGoodsGrouping {
    
    protected $groupParams = array();
    
    public function addGroupParam(\SoloOrder\Service\Good\GroupParam $groupParam) {
        $this->groupParams[] = $groupParam;
    }
    
    public function getGroupParams() {
        return $this->groupParams;
    }
    
}

?>
