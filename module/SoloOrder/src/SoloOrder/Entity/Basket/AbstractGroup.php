<?php

namespace SoloOrder\Entity\Basket;

abstract class AbstractGroup {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var \SoloBasket\Entity\GoodsCollection
     */
    protected $goods = null;

    /**
     *
     * @param int $id        	
     */
    public function __construct($id = null) {
        $this->id = $id;
        $this->clearGoods();
    }

    public function getGoods() {
        return $this->goods;
    }

    public function setGoods(\SoloOrder\Entity\Basket\GoodsCollection $goods) {
        $this->goods = $goods;
    }

    /**
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @param BasketGood $good        	
     * @return BasketGood
     */
    public function addGood(Good $good) {
        $this->goods[$good->getId()] = $good;
        return $this->goods[$good->getId()];
    }

    /**
     *
     * @param int $goodId        	
     */
    public function removeGood($goodId) {
        foreach ($this->goods as $key => $bg) {
            if ($goodId == $bg->getId()) {
                $this->goods->offsetUnset($key);
                return;
            }
        }
    }

    public function replaceGood(Good $good) {
        $this->removeGood($good->getId());
        $this->goods->add($good);
    }

    /**
     *
     * @param int $goodId        	
     * @return boolean
     */
    public function hasGood($goodId) {
        foreach ($this->goods as $bg) {
            if ($goodId == $bg->getId()) return true;
        }
        return false;
    }

    /**
     *
     * @param int $goodId        	
     * @return BasketGood
     */
    public function getGood($goodId) {
        foreach ($this->goods as $key => $bg) {
            if ($goodId == $bg->getId()) return $bg;
        }
        return null;
    }
    
    public function additional($name, $value = null) {
        /*@var $basketGood \SoloBasket\Entity\Good */
        foreach ($this->getGoods() as $basketGood) {
            $basketGood->additional($name, $value);
            $this->replaceGood($basketGood);
        }
    }

    public function clearGoods() {
        $this->setGoods(new \SoloOrder\Entity\Basket\GoodsCollection);
    }

    /**
     *
     * @return float
     */
    public function getAmount() {
        $amount = 0;
        foreach ($this->goods as $good) {
            $amount += $good->getAmount();
        }
        return $amount;
    }

    /**
     *
     * @return boolean
     */
    public function isEmpty() {
        return (0 == count($this->goods));
    }

    /**
     *
     * @return int
     */
    public function getTotalQuantity() {
        $quantity = 0;
        foreach ($this->getGoods() as $good) {
            $quantity += $good->getQuantity();
        }
        return $quantity;
    }

}

?>