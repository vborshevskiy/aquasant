<?php
namespace SoloOrder\Entity\Basket;

/**
 * BasketGood
 *
 * @author slava
 */
class Good extends \SoloOrder\Entity\Basket\AbstractGood {

    const PICKUP_METHOD_PARTIAL = 1;
    const PICKUP_METHOD_REMAIN = 2;
    const PICKUP_METHOD_ALL = 3;
    const PICKUP_METHOD_WHOLE = 4;
    const PICKUP_METHOD_MIN_MAX = 5;

    protected $warehouseId = null;
    protected $delivery = null;
    protected $suborderQuantity = null;
    protected $prices = null;
    protected $changedAmount = null;
    protected $packageComponents = null;
    protected $pickupMethodId = self::PICKUP_METHOD_ALL;
    protected $pickupDate;
    protected $allowShowcaseSale = false;
    protected $groupId = null;


    public function getWarehouseId() {
        return $this->warehouseId;
    }

    public function setWarehouseId($warehouseId) {
        $this->warehouseId = (int)$warehouseId;
    }

    public function getDelivery() {
        return $this->delivery;
    }

    public function setDelivery($delivery) {
        $this->delivery = $delivery;
    }

    public function hasDelivery() {
        return ($this->getDelivery() !== null && $this->getDelivery() instanceof \SoloOrder\Entity\Basket\Delivery);
    }

    public function getSuborderQuantity() {
        return $this->suborderQuantity;
    }

    public function setSuborderQuantity($suborderQuantity) {
        $this->suborderQuantity = $suborderQuantity;
    }

    public function getPrices() {
        return $this->prices;
    }

    public function setPrices($prices) {
        $this->prices = $prices;
    }

    public final function getPriceByColumn($priceColumnName) {
        return $this->prices[$priceColumnName];
    }

    public function getAmountByColumn($priceColumnName) {
        return floatval($this->getQuantity() * $this->getPriceByColumn($priceColumnName));
    }

    /**
     *
     * @return boolean
     */
    public function hasSuborder() {
        return (0 < $this->suborderQuantity);
    }

    public function getChangedAmount() {
        return $this->changedAmount;
    }

    public function setChangedAmount($changedAmount) {
        $this->changedAmount = $changedAmount;
    }

    public function setPackageComponents($packageComponents) {
        $this->packageComponents = $packageComponents;
    }

    public function getPackageComponents() {
        return $this->packageComponents;
    }

	public function getPackageComponentIds() {
        $ids = [];
		foreach ($this->packageComponents as $componentGood) {
			$ids[] = $componentGood->getId();
		}
		return $ids;
    }

    public function getPickupMethodId() {
        return $this->pickupMethodId ?: self::PICKUP_METHOD_ALL;
    }

    public function setPickupMethodId($pickupMethodId) {
        $this->pickupMethodId = $pickupMethodId;
    }

    public function setPickupDate($pickupDate) {
        $this->pickupDate = $pickupDate;
    }

    public function getPickupDate() {
        return $this->pickupDate;
    }

    public function getQuantityText($number, $case = 'nominative') {
        switch ($case) {
            case 'nominative':
                $plurals = ['штука', 'штуки', 'штук'];
                break;
            case 'accusative':
                $plurals = ['штуку', 'штуки', 'штук'];
                break;
            default:
                $plurals = ['штука', 'штуки', 'штук'];
        }

        $plural = $number%10==1 && $number%100!=11 ? 0 : (($number%10>=2 && $number%10<=4 && ($number%100<10 || $number%100>=20)) ? 1 : 2);

        return $plurals[$plural];
    }

    /**
     *
     * @param boolean $value
     * @return boolean
     */
    public function allowShowcaseSale($value = null) {
    	if (null !== $value) {
    		$this->allowShowcaseSale = $value;
    	} else {
    		return $this->allowShowcaseSale;
    	}
    }
	/**
	 * @return field_type
	 */
	public function getGroupId() {
		return $this->groupId;
	}


	/**
	 * @param field_type $groupId
	 */
	public function setGroupId($groupId) {
		$this->groupId = $groupId;
		return $this;
	}


}
