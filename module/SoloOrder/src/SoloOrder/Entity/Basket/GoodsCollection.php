<?php
namespace SoloOrder\Entity\Basket;

/**
 * BasketGoodsCollection
 *
 * @author slava
 */
class GoodsCollection extends \Solo\Collection\Collection {
    
    public function add($object, $key = null) {
        if (!($object instanceof \SoloOrder\Entity\Basket\AbstractGood)) {
            throw new \InvalidArgumentException("BasketGoodsCollection item must be an instance of SoloBasket\Entity\AbstractGood");
        }
        parent::add($object, $key);
    }
    
    public function getGoodsById($basketGoodId) {
        $result = null;
        if (sizeof($this->list) > 0) {
            foreach ($this->list as $basketGood) {
                if ($basketGood->getId() == $basketGoodId) {
                    $result[] = $basketGood;
                }
            }
        }
        return $result;
    }
    
    public function getGoodById($basketGoodId) {
        if (sizeof($this->list) > 0) {
            foreach ($this->list as $basketGood) {
                if ($basketGood->getId() == $basketGoodId) {
                    return $basketGood;
                }
            }
        }
        return null;
    }
    
    public function getQuantitiesByGoodIds() {
        $result = [];
        if (sizeof($this->list) > 0) {
            foreach ($this->list as $basketGood) {
                $result[$basketGood->getId()] += $basketGood->getQuantity();
            }
        }
        return $result;
    }
    
}

?>
