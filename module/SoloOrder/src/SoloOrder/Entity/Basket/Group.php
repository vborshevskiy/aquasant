<?php

namespace SoloOrder\Entity\Basket;

class Group extends \SoloOrder\Entity\Basket\AbstractGroup implements \Zend\EventManager\EventManagerAwareInterface {
    
    use \Solo\EventManager\ProvidesEvents;

    protected $deliveryPrice = null;
    protected $shops = null;
    protected $deliveryShedulle = null;
    protected $deliveryTitle = null;
    protected $availFromMainWarehouse = null;
    
    public function getDeliveryShedulle() {
        return $this->deliveryShedulle;
    }

    public function setDeliveryShedulle($deliveryShedulle) {
        $this->deliveryShedulle = $deliveryShedulle;
    }
    
    public function availFromMainWarehouse($avail = null) {
        if ($avail !== null) {
            $this->availFromMainWarehouse = $avail;
            return;
        }
        return $this->availFromMainWarehouse;
    }
    
    public function addGood(Good $good) {
        $args = $this->events()->prepareArgs(array());
        parent::addGood($good);
        $this->events()->trigger("basketGroupChange.post", $this, $args);
    }
    
    public function setGoods(GoodsCollection $goods) {
        parent::setGoods($goods);
        $args = $this->events()->prepareArgs(array());
        $this->events()->trigger('basketGroupChange.post', $this, $args);
    }
    
    public function removeGood($goodId) {
        parent::removeGood($goodId);
        $args = $this->events()->prepareArgs(array());
        $this->events()->trigger('basketGroupChange.post', $this, $args);
    }
    
    public function replaceGood(Good $good) {
        parent::replaceGood($good);
    }
    
    public function clearGoods() {
        parent::clearGoods();
        $args = $this->events()->prepareArgs(array());
        $this->events()->trigger('basketGroupChange.post', $this, $args);
    }
    
    public function getDeliveryTitle() {
        return $this->deliveryTitle;
    }

    public function setDeliveryTitle($deliveryTitle) {
        $this->deliveryTitle = $deliveryTitle;
    }

    public function getDeliveryPrice() {
        return $this->deliveryPrice;
    }

    public function setDeliveryPrice($deliveryPrice) {
        $this->deliveryPrice = $deliveryPrice;
    }
    
    public function hasDelivery() {
        foreach ($this->getGoods() as $basketGood) {
            if ($basketGood->hasDelivery()) {
                return true;
            }
        }
        return false;
    }

    public function getDelivery() {
        foreach ($this->getGoods() as $basketGood) {
            if ($basketGood->hasDelivery()) {
                $this->goods->rewind();
                return $basketGood->getDelivery();
            }
        }
        return null;
    }

    public function getWarehouseId() {
        foreach ($this->getGoods() as $basketGood) {
            if (!$basketGood->hasDelivery()) {
                return $basketGood->getWarehouseId();
            }
        }
        return null;
    }
    
    public function hasSuborder() {
        foreach ($this->getGoods() as $basketGood) {
            if ($basketGood->hasSuborder()) {
                return true;
            }
        }
        return false;
    }
    
    public function getShops() {
        return $this->shops;
    }

    public function setShops($shops) {
        $this->shops = $shops;
    }

    public function getAmountByColumn($priceColumnName, $isPhysicalBA = false, $multipliedGoods = null) {
        $amount = 0;
        if ($multipliedGoods && !$isPhysicalBA) {
            foreach ($this->goods as $good) {
                $goodAmount = $good->getAmountByColumn($priceColumnName);
                if (isset($multipliedGoods[$good->getId()]['Amount']) && $goodAmount != $multipliedGoods[$good->getId()]['Amount']) {
                    $multiAmount = $multipliedGoods[$good->getId()]['Amount'];
                    $multiQuantity = $multipliedGoods[$good->getId()]['Quantity'];
                    $multiPrice = $multiAmount / $multiQuantity;
                    $goodPrice = $good->getPriceByColumn($priceColumnName);
                    $goodMaxPrice = intval($goodPrice * 1.1);
                    if ($multiPrice > $goodMaxPrice) {
                        $goodAmount = $goodMaxPrice * $multiQuantity;
                    } elseif ($multiPrice > $goodPrice) {
                        $goodAmount = $multiPrice * $multiQuantity;
                    }
                }
                $amount += $goodAmount;
            }
        } else {
            foreach ($this->getGoods() as $good) {
                $amount += $good->getAmountByColumn($priceColumnName);
            }
        }
        return $amount;
    }

}

?>