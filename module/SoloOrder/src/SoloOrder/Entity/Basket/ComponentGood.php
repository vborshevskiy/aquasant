<?php
namespace SoloOrder\Entity\Basket;

/**
 * BasketComponentGood
 *
 * @author slava
 */
class ComponentGood {

    protected $id;
    protected $allowShowcaseSale = false;


    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = (int)$id;
    }

    /**
     *
     * @param boolean $value
     * @return boolean
     */
    public function allowShowcaseSale($value = null) {
    	if (null !== $value) {
    		$this->allowShowcaseSale = $value;
    	} else {
    		return $this->allowShowcaseSale;
    	}
    }
}
