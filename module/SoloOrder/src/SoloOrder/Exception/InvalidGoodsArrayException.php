<?php

namespace SoloOrder\Exception;

/**
 * InvalidGoodsArrayException
 *
 * @author Slava Tutrinov
 */
class InvalidGoodsArrayException extends \InvalidArgumentException {

}

?>
