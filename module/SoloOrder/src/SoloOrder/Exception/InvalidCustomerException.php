<?php

namespace SoloOrder\Exception;

/**
 * InvalidCustomerException
 *
 * @author Slava Tutrinov
 */
class InvalidCustomerException extends \InvalidArgumentException {

}

?>
