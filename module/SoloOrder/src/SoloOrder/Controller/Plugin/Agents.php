<?php

namespace SoloOrder\Controller\Plugin;

use SoloOrder\Data\AgentsGateway;

class Agents extends \Zend\Mvc\Controller\Plugin\AbstractPlugin {

	private $agentsGateway;

	public function __construct() {
		$this->agentsGateway = new AgentsGateway();
	}

	public function updateLastDeliveryAddress($idUser, $idAddress) {
		if ((((int)$idUser < 1) || ((int)$idAddress < 1))) return false;
		if ($this->getLastDeliveryAddress($idUser) === false) {
			return $this->agentsGateway->insertDeliveryLastAddress($idUser, $idAddress);
		} else {
			return $this->agentsGateway->updateDeliveryLastAddress($idUser, $idAddress);
		}
	}

	public function getLastDeliveryAddress($idUser) {
		if ((int)$idUser < 1) return false;
		return $this->agentsGateway->getLastDeliveryAddress($idUser)->LastAddr;
	}

}

?>
