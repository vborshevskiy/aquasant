<?php

namespace SoloOrder\Controller\Plugin;

/**
 * Description of Order
 *
 * @author slava
 */
class Order extends \Zend\Mvc\Controller\Plugin\AbstractPlugin {

    public function __call($name, $arguments) {
        $order = $this->getController()->getServiceLocator()->get('order_service');
        if (!method_exists($order, $name)) {
            throw new \BadMethodCallException('Invalid order method: ' . $name);
        }
        return call_user_func_array(array(
                    $order,
                    $name
                        ), $arguments);
    }

}

?>
