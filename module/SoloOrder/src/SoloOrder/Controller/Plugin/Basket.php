<?php

namespace SoloOrder\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class Basket extends AbstractPlugin {

	public function __call($name, $arguments) {
		$basket = $this->getController()->getServiceLocator()->get('basket_service');
		if (!method_exists($basket, $name)) {
			throw new \BadMethodCallException('Invalid basket method: ' . $name);
		}
		return call_user_func_array(array(
			$basket,
			$name 
		), $arguments);
	}

}

?>