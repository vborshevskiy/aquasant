<?php
use Solo\Db\QueryGateway\QueryGateway;
use SoloAdvAction\Data\ActionsMapper;

return [
	'controller_plugins' => [
		'invokables' => [
			'actions' => 'SoloAdvAction\Controller\Plugin\Actions' 
		] 
	],
	'service_manager' => [
		'factories' => [
			'SoloAdvAction\Data\ActionsMapper' => function ($sm) {
				return new ActionsMapper(new QueryGateway());
			},
			'SoloAdvAction\Service\ActionsService' => 'SoloAdvAction\Service\ActionsServiceFactory' 
		] 
	] 
];
