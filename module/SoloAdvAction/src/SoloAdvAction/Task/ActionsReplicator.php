<?php

namespace SoloAdvAction\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\WebService\Reader\UltimaListReader;
use SoloReplication\Service\DataMapper;
use SoloAdvAction\Data\ActionsTableInterface;
use SoloAdvAction\Data\ActionGoodsTableInterface;
use SoloAdvAction\Data\ActionGoodsMapperInterface;

class ActionsReplicator extends AbstractReplicationTask {

	protected $actionsLogoPath;

	/**
	 *
	 * @var ActionsTableInterface
	 */
	protected $actionsTable;
	
	/**
	 * 
	 * @var ActionGoodsTableInterface
	 */
	protected $actionGoodsTable;
	
	/**
	 * 
	 * @var ActionGoodsMapperInterface
	 */
	protected $actionGoodsMapper;

	/**
	 * 
	 * @param ActionsTableInterface $actionsTable
	 * @param ActionGoodsTableInterface $actionGoodsTable
	 * @param ActionGoodsMapperInterface $actionGoodsMapper
	 */
	public function __construct(ActionsTableInterface $actionsTable, ActionGoodsTableInterface $actionGoodsTable, ActionGoodsMapperInterface $actionGoodsMapper) {
		$this->actionsTable = $actionsTable;
		$this->actionGoodsTable = $actionGoodsTable;
		$this->actionGoodsMapper = $actionGoodsMapper;
		
		$this->addSwapTable('actions', 'actions_goods');
	}

	public function process() {
		$this->initializeLogoPath();
		$this->processActions();
	}

	protected function initializeLogoPath() {
		if (!empty($_SERVER['DOCUMENT_ROOT'])) {
			$baseFolder = $_SERVER['DOCUMENT_ROOT'];
		} else {
			$baseFolder = system('pwd') . '/public';
		}
		
		$baseFolder .= '/img/actions/';
		
		if (file_exists($baseFolder) && is_dir($baseFolder)) {
			$this->actionsLogoPath = $baseFolder;
		} else {
			throw new \RuntimeException('cant save actions logo at $document_root$/img/actions/');
		}
	}

	protected function saveLogo($id, $content) {
		$fp = fopen($this->actionsLogoPath . $id . '.jpg', 'w');
		fwrite($fp, $content);
		fclose($fp);
	}

	protected function createActionsMapper() {
		$mapper = new DataMapper();
		$mapper->setMapping('ActionID', '%d: ID');
		$mapper->setMapping('ActionName', 'SiteActionName');
		$mapper->setMapping('ActionUID', '', function ($field, $row) use($this) {
			return $this->helper()->translitUrl($row['SiteActionName']);
		});
		$mapper->setMapping('ActionDescription', 'SiteDescr');
		$mapper->setMapping('StartedAt', '', function ($field, $row) {
			return (!empty($row['SiteDateBegin']) ? $row['SiteDateBegin'] : $row['DateBegin']);
		});
		$mapper->setMapping('FinishedAt', '', function ($field, $row) {
			return (!empty($row['SiteDateEnd']) ? $row['SiteDateEnd'] : $row['DateEnd']);
		});
		
		return $mapper;
	}

	protected function processActions() {
		$mapper = $this->createActionsMapper();
		
		$charges = [];
		$rdr = new UltimaListReader($this->callWebMethod('GetMarketingActionsList'));
		if (0 < $rdr->count()) {
			$this->actionsTable->truncate();
			
			$dataSet = [];
			foreach ($rdr as $row) {
				$hasImage = 0;
				// save image
				if (!empty($row['Base64Logo'])) {
					$fileBody = base64_decode($row['Base64Logo']);
					if (!empty($fileBody)) {
						$this->saveLogo($row->ID, $fileBody);
						$hasImage = 1;
					} else {
						$this->log->notice('Cant decode image for ActionID=' . $row->ID);
					}
				}
				$data = $mapper->convert($row);
				$data['HasImage'] = $hasImage;
				$dataSet[] = $data;
				
				/**
				 * в поле Charge содержатся привзки товаров к акции
				 */
				if (!empty($row['Charge'][1]) && is_array($row['Charge'][1])) {
					// здесь содержится массив ключей-описаний данных
					$keyMap = $row['Charge'][0];
					foreach ($row['Charge'][1] as $charge) {
						$link = [];
						foreach ($charge as $key => $val) {
							$link[$keyMap[$key]] = $val;
						}
						$charges[$row['ID']][] = $link;
					}
				}
			}
			$this->actionsTable->insertSet($dataSet);
		}
		
		$this->log->info('Inserted actions = ' . $rdr->count());
		
		return $charges;
	}
	
	protected function processActionGoods($charges) {
		$this->actionGoodsTable->truncate();
		
		foreach ($charges as $ActionId => $actionCharges) {
				
			foreach ($actionCharges as $charge) {
		
				// создаем массив GoodID или ManufacturerID или CategoryID
				// с которыми будем работать в зависимости от ключа привязки
				$items = [];
				if ($charge['Condition'] == 'Equals') {
						
					$items[] = (int)$charge['Value'];
				} elseif ($charge['Condition'] == 'InList') {
						
					$values = explode(',', $charge['Value']);
					foreach ($values as $value) {
						$items[] = (int)trim($value);
					}
				}
		
				// по типу каждой привязки отдельный обработчик
				// собираем массив уникальных значений ActionID - GoodsID
				switch ($charge['Key']) {
					// привязка по идентификатору товара
					case 'GoodID':
						foreach ($items as $item) {
							$goodId = intval($item);
							$this->actionGoodsMapper->addActionGoodInPassive($ActionId, $goodId);
						}
						break;
							
						// нужно добавить все товары из списка производителей
					case 'ManufacturerID':
						foreach ($items as $item) {
							$vendorId = intval($item);
							$this->actionGoodsMapper->addActionGoodsByVendorIdInPassive($ActionId, $vendorId);
						}
						break;
							
						// нужно добавить все товары из списка категорий
					case 'CategoryID':
						foreach ($items as $item) {
							$categoryId = intval($item);
							$this->actionGoodsMapper->addActionGoodsByCategoryIdInPassive($ActionId, $categoryId);
						}
						break;
				}
			}
		}
	}

}

?>