<?php

namespace SoloAdvAction\Data;

use Solo\Db\Mapper\AbstractMapper;

class ActionsMapper extends AbstractMapper implements ActionsMapperInterface {

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \SoloAdvAction\Data\ActionsTableInterface::getIndexActions()
	 */
	public function getIndexActions($limit = 4) {
		$sql = "SELECT a.*, IF (a.StartedAt < NOW() && a.FinishedAt > NOW(), 1, 0) AS current
			FROM #actions# a
			INNER JOIN #actions_goods# ag ON ag.ActionID=a.ActionID
			INNER JOIN #all_goods# alg ON alg.GoodID=ag.GoodID
			WHERE a.FinishedAt > NOW()
			GROUP BY a.ActionID
			ORDER BY current DESC, a.StartedAt DESC
			LIMIT " . $limit;
		
		return $this->query($sql);
	}

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \SoloAdvAction\Data\ActionsTableInterface::getActionsList()
	 */
	public function getActionsList($page = 0, $limit = 10) {
		$sql = "SELECT *, IF (StartedAt < NOW() && FinishedAt > NOW(), 1, 0) AS current
				FROM #actions#
				ORDER BY current DESC, StartedAt DESC
				LIMIT " . ($page * $limit) . ", " . $limit;
		
		return $this->query($sql);
	}

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \SoloAdvAction\Data\ActionsTableInterface::countAllActions()
	 */
	public function countAllActions() {
		return $this->query("SELECT COUNT(ActionID) AS cnt FROM #actions#")->current()->cnt;
	}

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \SoloAdvAction\Data\ActionsTableInterface::getAction()
	 */
	public function getAction($uid) {
		$sql = "SELECT a.*, IF (a.StartedAt < NOW() && a.FinishedAt > NOW(), 1, 0) AS current
				FROM #actions# a
				WHERE a.ActionUID = " . $this->queryGateway->quoteValue($uid);
		
		return $this->query($sql)->current();
	}

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \SoloAdvAction\Data\ActionsTableInterface::getActionGoods()
	 */
	public function getActionGoods($actionId) {
		$sql = "SELECT alg.*
				FROM #all_goods# alg
				INNER JOIN #actions_goods# ag
				ON alg.GoodID = ag.GoodID AND ag.ActionID = " . $actionId;
		
		return $this->query($sql);
	}

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \SoloAdvAction\Data\ActionsTableInterface::hasAnyGoodByActionId()
	 */
	public function hasAnyGoodByActionId($actionId = 0) {
		$sql = "SELECT *
				FROM #actions_goods# ag
				INNER JOIN #all_goods# alg ON alg.GoodID=ag.GoodID
				WHERE ActionID=" . $actionId . "
				LIMIT 1";
		
		$count = $this->query($sql)->current();
		
		return (!empty($count));
	}

}

?>