<?php

namespace SoloAdvAction\Data;

use Solo\Db\Mapper\AbstractMapper;

class ActionGoodsMapper extends AbstractMapper implements ActionGoodsMapperInterface {

	/**
	 * (non-PHPdoc)
	 * @see \SoloAdvAction\Data\ActionGoodsMapperInterface::addActionGoodInPassive()
	 */
	public function addActionGoodInPassive($actionId, $goodId) {
		$sql = "REPLACE #action_goods:passive#
				SET
					ActionID = {$actionId},
					GoodID = {$goodId}";
		$this->query($sql);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \SoloAdvAction\Data\ActionGoodsMapperInterface::addActionGoodsByVendorIdInPassive()
	 */
	public function addActionGoodsByVendorIdInPassive($actionId, $vendorId) {
		$sql = "REPLACE #action_goods:passive#
				SELECT {$actionId}, GoodID
				FROM #all_goods#
				WHERE VendorID = {$vendorId}";
		$this->query($sql);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \SoloAdvAction\Data\ActionGoodsMapperInterface::addActionGoodsByCategoryIdInPassive()
	 */
	public function addActionGoodsByCategoryIdInPassive($actionId, $categoryId) {
		$sql = "REPLACE #action_goods:passive#
				SELECT {$actionId}, GoodID
				FROM #all_goods#
				WHERE CategoryID = {$categoryId}";
		$this->query($sql);
	}
	
}

?>