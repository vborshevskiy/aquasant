<?php

namespace SoloAdvAction\Data;

use Zend\Db\ResultSet\ResultSet;

interface ActionsMapperInterface {

	/**
	 *
	 * @param integer $limit        	
	 * @return ResultSet
	 */
	public function getIndexActions($limit);

	/**
	 *
	 * @param integer $page        	
	 * @param integer $limit        	
	 * @return ResultSet
	 */
	public function getActionsList($page, $limit);

	/**
	 *
	 * @return integer
	 */
	public function countAllActions();

	/**
	 *
	 * @param string $uid        	
	 * @return \ArrayObject
	 */
	public function getAction($uid);

	/**
	 *
	 * @param integer $actionId        	
	 * @return ResultSet
	 */
	public function getActionGoods($actionId);

	/**
	 *
	 * @param integer $actionId        	
	 * @return boolean
	 */
	public function hasAnyGoodByActionId($actionId);

}

?>