<?php

namespace SoloAdvAction\Data;

interface ActionGoodsMapperInterface {

	/**
	 *
	 * @param integer $actionId        	
	 * @param integer $goodId        	
	 */
	public function addActionGoodInPassive($actionId, $goodId);

	/**
	 *
	 * @param integer $actionId        	
	 * @param integer $vendorId        	
	 */
	public function addActionGoodsByVendorIdInPassive($actionId, $vendorId);

	/**
	 *
	 * @param integer $actionId        	
	 * @param integer $categoryId        	
	 */
	public function addActionGoodsByCategoryIdInPassive($actionId, $categoryId);

}

?>