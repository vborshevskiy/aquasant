<?php

namespace SoloAdvAction\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class Actions extends AbstractPlugin {

	/**
	 *
	 * @param string $name        	
	 * @param string $arguments        	
	 * @throws \BadMethodCallException
	 * @return mixed
	 */
	public function __call($name, $arguments) {
		$service = $this->getController()->getServiceLocator()->get('\\SoloAdvAction\\Service\\ActionsService');
		if (!method_exists($service, $name)) {
			throw new \BadMethodCallException('Invalid actions method: ' . $name);
		}
		return call_user_func_array(array(
			$service,
			$name 
		), $arguments);
	}

}
