<?php

namespace SoloAdvAction\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class ActionsServiceFactory extends AbstractServiceFactory {

	protected function create() {
		$service = new ActionsService($this->getServiceLocator()->get('SoloAdvAction\\Data\\ActionsMapper'));
		$config = $this->getConfig('adv_actions');
		if ($config && isset($config['limits'])) {
			$service->setOption('limits', $config['limit']);
		}
		return $service;
	}

}

?>