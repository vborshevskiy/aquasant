<?php

namespace SoloAdvAction\Service;

use Solo\Stdlib\ArrayHelper;
use Zend\Paginator\Paginator;
use Solo\ServiceManager\ProvidesOptions;
use SoloAdvAction\Data\ActionsMapperInterface;

class ActionsService {
	
	use ProvidesOptions;

	/**
	 *
	 * @var ActionsMapperInterface
	 */
	private $actionsMapper;

	/**
	 * 
	 * @param ActionsMapperInterface $actionsMapper
	 */
	public function __construct(ActionsMapperInterface $actionsMapper) {
		$this->actionsMapper = $actionsMapper;
	}

	/**
	 * Получаем акции для отображения на главной странице
	 *
	 * @param int $perPage
	 *        	кол-во акций
	 * @return mixed
	 */
	public function getIndexActions($perPage = null) {
		if ((null === $perPage) && $this->hasOption('limits.mainPage')) {
			$perPage = intval($this->getOption('limits.mainPage'));
		}
		$actions = $this->actionsMapper->getIndexActions($perPage + 1);
		
		return array(
			'items' => $actions,
			'itemsLimit' => $perPage,
			'actionsCount' => $actions->count() 
		);
	}

	/**
	 * Получаем листинг акций
	 *
	 * @param int $page
	 *        	номер страницы
	 * @param int $perPage
	 *        	размер страницы
	 * @return array
	 */
	public function getActionsList($page = 0, $perPage = null) {
		if ((null === $perPage) && $this->hasOption('limits.listing')) {
			$perPage = intval($this->getOption('limits.listing'));
		}
		
		// номер страницы для запроса
		if ($page >= 1) {
			$queryPage = $page - 1;
		} else {
			$queryPage = $page;
		}
		
		$actions = $this->actionsMapper->getActionsList($queryPage, $perPage);
		$actionsQuant = $this->actionsMapper->countAllActions();
		
		// страницы
		$paginator = new Paginator(new \Zend\Paginator\Adapter\Null($actionsQuant));
		$paginator->setCurrentPageNumber((int)$page);
		$paginator->setItemCountPerPage($perPage);
		$pages = $paginator->getPages();
		
		$pagination = array(
			'previous' => $pages->previous,
			'next' => $pages->next 
		);
		
		return array(
			'items' => $actions,
			'pages' => $pagination 
		);
	}

	/**
	 * Информация об одной выбранной акции
	 *
	 * @param string $uid
	 *        	ключ акции
	 * @return mixed
	 */
	public function getAction($uid) {
		$action = $this->actionsMapper->getAction($uid);
		
		return $action;
	}

	/**
	 * Получение листинга идентификаторов товаров выбранной акции
	 *
	 * @param $actionUid текстовый
	 *        	ключ акции
	 *        	
	 * @return array null
	 */
	public function getActionGoods($actionUid) {
		$action = $this->actionsMapper->getAction($actionUid);
		
		if (empty($action['ActionID'])) {
			return null;
		}
		
		$goods = $this->actionsMapper->getActionGoods($action['ActionID']);
		$goodsIds = ArrayHelper::enumOneColumn($goods, 'GoodID');
		
		return $goodsIds;
	}

	/**
	 * Проверяем наличие товаров в акции
	 *
	 * @param int $actionID        	
	 *
	 * @return bool
	 */
	public function hasAnyGoodByActionId($actionID = 0) {
		return $this->actionsMapper->hasAnyGoodByActionId($actionID);
	}

}
