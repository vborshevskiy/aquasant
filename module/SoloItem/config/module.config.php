<?php

use Solo\Db\TableGateway\TableGateway;
use SoloItem\Data\CommentMapper;
use Solo\Db\QueryGateway\QueryGateway;
use SoloItem\Data\CommentTable;
use SoloItem\Data\YandexCommentsTable;

return array(
    'controller_plugins' => array(
        'invokables' => array(
            'item' => 'SoloItem\Controller\Plugin\Item'
        )
    ),
    'service_manager' => array(
        'aliases' => array(
            'TimesVisitedService' => '\SoloItem\Service\TimesVisitedService',
            'item_comment' => 'SoloItem\Service\CommentService'
        ),
        'factories' => array(
            '\SoloItem\Service\TimesVisitedService' => 'SoloItem\Service\TimesVisitedServiceFactory',
            'SoloItem\Data\CommentMapper' => function ($sm) {
                return new CommentMapper(new QueryGateway());
            },
            'SoloCatalog\Data\CommentTable' => function ($sm) {
                $gateway = new TableGateway('comments', 'GoodID');
                $table = new CommentTable($gateway);
                return $table;
            },
            'SoloCatalog\Data\YandexCommentsTable' => function ($sm) {
                $gateway = new TableGateway('yandex_comments', 'CommentID');
                $table = new YandexCommentsTable($gateway);
                return $table;
            },
            'SoloItem\Service\CommentService' => 'SoloItem\Service\CommentServiceFactory'
        )
    ),
    'router' => array(
        'routes' => array(
            'cleanupvisits' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'priority' => 1000,
                'options' => array(
                    'route' => '/cleanupvisits',
                    'defaults' => array(
                        'controller' => 'SoloItem\Controller\Cleaner',
                        'action' => 'index'
                    )
                )
            ),
            'updatemaxsales' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'priority' => 1000,
                'options' => array(
                    'route' => '/updatemaxsales',
                    'defaults' => array(
                        'controller' => 'SoloItem\Controller\Cleaner',
                        'action' => 'updateMaxSales'
                    )
                )
            )
        )
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
                'cleanupvisits' => array(
                    'options' => array(
                        'route' => 'cleanupvisits [<all>]',
                        'defaults' => array(
                            'controller' => 'SoloItem\Controller\Cleaner',
                            'action' => 'console',
                            'all' => 'false'
                        )
                    )
                ),
                'updatemaxsales' => array(
                    'options' => array(
                        'route' => 'updatemaxsales',
                        'defaults' => array(
                            'controller' => 'SoloItem\Controller\Cleaner',
                            'action' => 'updateMaxSales'
                        )
                    )
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'SoloItem\Controller\Cleaner' => 'SoloItem\Controller\CleanerController'
        )
    )
);
