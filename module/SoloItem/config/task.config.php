<?php

use SoloItem\Task\YandexCommentsReplicator;

return [
    'tasks' => [
        'yandex_comments' => 'SoloItem\Task\YandexCommentsReplicator',
    ],
    'service_manager' => [
        'factories' => [
            'SoloItem\Task\YandexCommentsReplicator' => function ($sm) {              
                $yandexCommentsTable = $sm->get('SoloItem\Data\YandexCommentsTable');
                $yandexCommentsTable->getTableGateway()->setTable($sm->get('triggers')->passive('yandex_comments'));
                $task = new YandexCommentsReplicator($yandexCommentsTable);
                return $task;
            },
        ],
    ],
];
