<?php

namespace SoloItem\Controller\Plugin;

use Solo\Mvc\Controller\Plugin\AbstractPlugin;
use SoloItem\Service\CommentService;

class Item extends AbstractPlugin {

    /**
     *
     * @return CommentService
     */
    public function comment() {
        return $this->getServiceLocator()->get('item_comment');
    }

}

?>