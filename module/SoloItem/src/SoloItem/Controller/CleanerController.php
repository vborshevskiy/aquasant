<?php

namespace SoloItem\Controller;

use Solo\Mvc\Controller\ActionController;

class CleanerController extends ActionController {

	public function indexAction() {
		$pRedis = $this->service('PredisClient');
		$timesVisited = $this->service('TimesVisitedService');
		echo 'Deleted entries: ' . $timesVisited->unsetOutOfDateKeys($pRedis, $this->const('watch_now_period'));
		exit();
	}

	public function consoleAction() {
		$all = $this->getRequest()->getParam('all');
		if ('true' === $all) {
			$all = true;
		} else {
			$all = false;
		}
		$pRedis = $this->service('PredisClient');
		$timesVisited = $this->service('TimesVisitedService');
		print 'Deleted entries: ' . $timesVisited->unsetOutOfDateKeys($pRedis, $this->const('watch_now_period'), $all);
		exit();
	}

	public function updateMaxSalesAction() {
		$pRedis = $this->service('PredisClient');
		$timesVisited = $this->service('TimesVisitedService');
		$params = array(
			'virtualMin' => $this->const('virtual_min'),
			'virtualMax' => $this->const('virtual_max') 
		);
		$p = $timesVisited->updateMaxSales($pRedis, $params);
		print 'updated ';
		var_dump($p);
		exit();
	}

	public function testVisitsAction() {
		$pRedis = $this->service('PredisClient');
		$timesVisited = $this->service('TimesVisitedService');
		$timesVisited->test($pRedis, 12127, $this->const('watch_now_period'));
	}

}
