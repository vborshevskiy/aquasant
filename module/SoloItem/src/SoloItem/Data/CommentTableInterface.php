<?php

namespace SoloItem\Data;

use Solo\Db\TableGateway\TableInterface;

interface CommentTableInterface extends TableInterface {

    /**
     * 
     * @param integer $goodID
     * @param string $userName
     * @param integer $siteId
     * @param integer $rating
     * @param string $text
     * @param string $good
     * @param string $bad
     * @param integer $userId
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function insertComment($goodID, $userName, $siteId, $rating, $text, $good, $bad, $userId);
    
}
