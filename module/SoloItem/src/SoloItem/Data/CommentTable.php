<?php

namespace SoloItem\Data;

use Solo\Db\TableGateway\AbstractTable;

class CommentTable extends AbstractTable implements CommentTableInterface {

    /**
     * (non-PHPdoc)
     * @see \SoloItem\Data\CommentsTableInterface::insertComment()
     */
    public function insertComment($goodId, $userName, $siteId, $rating, $text, $good, $bad, $userId) {
        $data = [
            'GoodID' => $goodId,
            'UserName' => $userName,
            'SiteID' => $siteId,
            'Rating' => $rating,
            'Text' => $text,
            'Good' => $good,
            'Bad' => $bad,
            'Date' => new \Zend\Db\Sql\Expression('NOW()'),
            'UserId' => $userId,
        ];
        if (is_null($userId)) {
            $data['IsRealBuyer'] = 0;
        }
        $insert = $this->createInsert();
        $insert->values($data);
        $sql = $insert->getSqlString($this->getTableGateway()->getAdapter()->getPlatform());
        return $this->query($sql);
    }

}
