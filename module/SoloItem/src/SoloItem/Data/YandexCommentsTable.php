<?php

namespace SoloItem\Data;

use Solo\Db\TableGateway\AbstractTable;

class YandexCommentsTable extends AbstractTable implements YandexCommentsTableInterface {
    
    /**
     * 
     * @return integer
     */
    public function insertSiteComments() {
        $sql = '
            INSERT INTO
                ' . $this->tableGateway->getTable() . '
                (
                    GoodID,
                    Date,
                    Rating,
                    ErpID,
                    IsRealBuyer,
                    UserName,
                    Text,
                    Good,
                    Bad
                )
            SELECT
                c.GoodID,
                c.Date,
                c.Rating,
                null,
                c.IsRealBuyer,
                c.UserName,
                c.Text,
                c.Good,
                c.Bad
            FROM
                comments c';
        return $this->query($sql);
    }

}
