<?php

namespace SoloItem\Data;

use Solo\Db\Mapper\AbstractMapper;

class CommentMapper extends AbstractMapper implements CommentMapperInterface {

    public function markAdded($commId) {
        $sql = 'UPDATE
                        comments
                SET
                        Added = 1
                WHERE
                        ID = ' . $commId;
        return $this->query($sql);
    }

    /**
     * 
     * @param integer $goodId
     * @return \Traversable
     */
    public function getRatings($goodId) {
        $sql = 'SELECT
                        COUNT(c.Rating) Count,
                        SUM(c.Rating) Sum
                FROM
                        comments c
                WHERE
                        c.Added = 1
                        AND c.GoodID = (
                                            SELECT
                                                    CASE
                                                    WHEN ag.OriginalGoodID > 0 THEN ag.OriginalGoodID
                                                    ELSE ag.GoodID
                                                    END
                                            FROM
                                                    #all_goods:active# ag
                                            WHERE
                                                    ag.GoodID = '.$goodId.'
                                            LIMIT 1
                                        )';
        return $this->query($sql)->current();
    }

    /**
     * 
     * @param integer $goodId
     * @param integer|null $start
     * @return type
     */
    public function getComments($goodId, $start = null) {
        $sql = 'SELECT
                        c.*
                FROM
                        comments c
                WHERE
                        c.GoodID = (
                                SELECT
                                        CASE
                                        WHEN ag.OriginalGoodID > 0 THEN ag.OriginalGoodID
                                        ELSE ag.GoodID
                                        END
                                FROM
                                        #all_goods:active# ag
                                WHERE
                                        ag.GoodID = '.$goodId.'
                                LIMIT 1
                        )';
        if ($start) {
            $sql.= '
                LIMIT
                        '.$start.',5';
        }
        return $this->query($sql)->toArray();
    }

    /**
     * 
     * @param array $deletedComments
     * @return integer
     */
    public function deleteComments($deletedComments) {
        $conditions = [];
        foreach ($deletedComments as $good) {
            if (array_key_exists('goodId', $good) && array_key_exists('viewId', $good)) {
                $conditions[] = '(GoodID = ' . $good['goodId'] . ' AND ViewID = ' . $good['viewId'] . ')';
            }
        }
        if (count($conditions) === 0) {
            return null;
        }
        $sql = 'DELETE
                FROM
                        #goods_images:passive#
                WHERE
                        ' . implode(' OR ', $conditions);
        return $this->query($sql);
    }
    
    /**
     * 
     * @param array $goodIds
     * @return array
     */
    public function getRatingByGoodIds($goodIds) {
        if (count($goodIds) == 0) {
            return [];
        }
        $sql = 'SELECT
                        com.GoodID,
                        SUM(com.Rating) / COUNT(*) AS Rating
                FROM
                        #yandex_comments:active# com
                WHERE
                        com.GoodID IN  ('.implode(',',$goodIds).')
                GROUP BY
                        com.GoodID';
        return $this->query($sql)->toArray();
    }
    
    /**
     * 
     * @param integer $commentId
     * @param integer $goodId
     */
    public function deleteComment($commentId, $goodId) {
        $sql = 'DELETE
                FROM
                        comments
                WHERE
                        GoodID = ' . $goodId . '
                        AND ID = ' . $commentId;
        return $this->query($sql);
    }
    
    /**
     * 
     * @param integer $goodId
     * @param integer $start
     * @param integer $limit
     * @return array
     */
    public function getCommentsWithYandex($goodId, $start = 0, $limit = 3) {
        $sql = 'SELECT
                        c.*
                FROM
                        #yandex_comments:active# c
                WHERE
                        c.GoodID = (
                                SELECT
                                        CASE
                                        WHEN ag.OriginalGoodID > 0 THEN ag.OriginalGoodID
                                        ELSE ag.GoodID
                                        END
                                FROM
                                        #all_goods:active# ag
                                WHERE
                                        ag.GoodID = ' . $goodId . '
                                LIMIT 1
                        )
                        AND
                        (
                            c.Text <> ""
                            OR
                            c.Good <> ""
                            OR
                            c.Bad <> ""
                        )
                ORDER BY
                        c.Date DESC
                ';
        if ($start > 0) {
            $sql .= '
                LIMIT
                        '. $start . ',' . $limit;
        }
        return $this->query($sql)->toArray();
    }
    
    /**
     * 
     * @param integer $goodId
     * @return \Traversable
     */
    public function getRatingsWithYandex($goodId) {
        $sql = 'SELECT
                        COUNT(c.Rating) Count,
                        SUM(c.Rating) Sum
                FROM
                        #yandex_comments:active# c
                WHERE
                        c.GoodID = (
                                            SELECT
                                                    CASE
                                                    WHEN ag.OriginalGoodID > 0 THEN ag.OriginalGoodID
                                                    ELSE ag.GoodID
                                                    END
                                            FROM
                                                    #all_goods:active# ag
                                            WHERE
                                                    ag.GoodID = '.$goodId.'
                                            LIMIT 1
                                        )
                        AND
                        (
                            c.Text <> ""
                            OR
                            c.Good <> ""
                            OR
                            c.Bad <> ""
                        )';
        return $this->query($sql)->current();
    }

}
