<?php

namespace SoloItem\Data;

interface CommentMapperInterface {

    public function markAdded($commId);
    
    public function getRatings($goodId);
    
    public function getComments($goodId, $start);
    
    public function deleteComments($deletedComments);
    
    /**
     * 
     * @param array $goodIds
     * @return array
     */
    public function getRatingByGoodIds($goodIds);
    
    /**
     * 
     * @param integer $commentId
     * @param integer $goodId
     */
    public function deleteComment($commentId, $goodId);


}

?>