<?php

namespace SoloItem\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\Service\ProvidesWebservice;
use SoloItem\Data\YandexCommentsTable;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloReplication\Service\DataMapper;

class YandexCommentsReplicator extends AbstractReplicationTask {

    use ProvidesWebservice;

    /**
     *
     * @var YandexCommentsTable
     */
    private $yandexCommentsTable;

    /**
     *
     * @param YandexCommentsTable $yandexCommentsTable
     */
    public function __construct(YandexCommentsTable $yandexCommentsTable) {
        $this->yandexCommentsTable = $yandexCommentsTable;
        $this->addSwapTable('yandex_comments');
        $this->scriptNotRespondingTimeInMinutes = 120 * 24;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SoloReplication\Service\TaskInterface::process()
     */
    public function process() {
        $this->processYandexComments();
    }
    
    /**
     *
     * @return \SoloReplication\Service\DataMapper
     */
    protected function creatYandexCommentsMapper() {
        $mapper = new DataMapper();
        $mapper->setMappings(
        [
            'GoodID' => '%d: ArticleId',
            'Rating' => '%d: Rating',
            'ErpID' => '%d: Id',
            'UserName' => 'UserName',
            'Text' => 'Comment',
            'Good' => 'Advantages',
            'Bad' => 'Disadvantages',
        ]);
        $context = $this;
        $mapper->setMapping('Date', 'AddDate', function ($field, $row) use ($context) {
            return $context->helper()->parseJSONDate($field);
        });

        return $mapper;
    }

    /**
     */
    protected function processYandexComments() {
        $this->log->info('Insert yandex comments');
        $mapper = $this->creatYandexCommentsMapper();
        $dataSet = [];
        $rdr = new UltimaJsonListReader($this->callWebMethod('GetArticleComments', [
            'FromYandex' => true,
        ]));
        if (!$rdr->isEmpty()) {
            $this->yandexCommentsTable->truncate();
        }
        $count = 0;
        foreach ($rdr as $row) {
            $dataSet[] = $mapper->convert($row);
            if (500 == sizeof($dataSet)) {
                $this->yandexCommentsTable->insertSet($dataSet);
                $count += sizeof($dataSet);
                $dataSet = [];
            }
        }
        if (0 < sizeof($dataSet)) {
            $this->yandexCommentsTable->insertSet($dataSet);
            $count += sizeof($dataSet);
        }
        
        $this->yandexCommentsTable->insertSiteComments();

        $this->log->info('Inserted comments = ' . $count);
    }

}
