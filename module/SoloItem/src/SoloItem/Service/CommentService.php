<?php

namespace SoloItem\Service;

use SoloItem\Data\CommentMapperInterface;
use SoloItem\Data\CommentTableInterface;
use Solo\ServiceManager\ServiceLocatorAwareService;
use SoloERP\Service\ProvidesWebservice;
use Solo\ServiceManager\Result;
use SoloERP\WebService\Reader\UltimaJsonListReader;

class CommentService extends ServiceLocatorAwareService {

    use ProvidesWebservice;

    /**
     *
     * @var CommentMapperInterface
     */
    private $commentMapper;

    /**
     *
     * @var CommentTableInterface 
     */
    private $commentTable;

    /**
     * 
     * @param CommentMapperInterface $commentMapper
     * @param CommentTableInterface $commentTable
     */
    public function __construct(CommentMapperInterface $commentMapper, CommentTableInterface $commentTable) {
        $this->commentMapper = $commentMapper;
        $this->commentTable = $commentTable;
    }

    /**
     * 
     * @param integer $goodID
     * @param string $userName
     * @param integer $siteId
     * @param integer $rating
     * @param string $text
     * @param string $good
     * @param string $bad
     * @param integer $userId
     * @return type
     */
    public function insertComment($goodID, $userName, $siteId, $rating, $text, $good, $bad, $userId) {
        return $this->commentTable->insertComment($goodID, $userName, $siteId, $rating, $text, $good, $bad, $userId);
    }

    /**
     * 
     * @param type $commentId
     * @return type
     */
    public function markAdded($commentId) {
        return $this->commentMapper->markAdded($commentId);
    }

    /**
     * 
     * @param string $userName
     * @param integer $goodId
     * @param integer $siteId
     * @param integer $rating
     * @param integer $mysqlId
     * @param string $text
     * @return Result
     */
    public function addComment($userName, $goodId, $siteId, $rating, $mysqlId, $text) {
        $wm = $this->getWebMethod('AddComment');
        $wm->addPar('UserName', $userName);
        $wm->addPar('ProductId', $goodId);
        $wm->addPar('SiteID', $siteId);
        $wm->addPar('Rating', $rating);
        $wm->addPar('MysqlId', $mysqlId);
        $wm->addPar('Text', $text);
        $response = $wm->call();
        $result = new Result();
        if ($response->hasError()) {
            $result->setError($response->getError());
        } else {
            $rdr = new UltimaJsonListReader($response);
            $result->result = $rdr->getValue('Result');
        }
        return $result;
    }

    /**
     * 
     * @param integer $goodId
     * @return integer
     */
    public function getRatingInfo($goodId) {
        $ratings = $this->commentMapper->getRatings($goodId);
        if ($ratings["Count"]) {
            return $ratings;
        }
        return null;
    }

    /**
     * 
     * @param integer $goodId
     * @param integer $start
     * @return \Traversable
     */
    public function getComments($goodId, $start = null) {
        $comments = $this->commentMapper->getComments($goodId, $start);
        if (count($comments) > 0) {
            return $comments;
        }
        return null;
    }

    /**
     * 
     * @param array $goodIds
     * @return array
     */
    public function getRatingByGoodIds($goodIds) {
        $ratings = $this->commentMapper->getRatingByGoodIds($goodIds);
        $result = [];
        foreach ($ratings as $rating) {
            $result[$rating['GoodID']] = $rating['Rating'];
        }
        return $result;
    }

    /**
     * 
     * @param integer $commentId
     * @param integer $goodId
     */
    public function deleteComment($commentId, $goodId) {
        if (!is_int($goodId)) {
            throw new \InvalidArgumentException('Good id must be integer');
        }
        if (!is_int($commentId)) {
            throw new \InvalidArgumentException('Comment id must be integer');
        }
        $this->commentMapper->deleteComment($commentId, $goodId);
    }
    
    /**
     * 
     * @param integer $goodId
     * @param integer $start
     * @param integer $limit
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getCommentsWithYandex($goodId, $start = 0, $limit = 3) {
        if (!is_int($goodId)) {
            throw new \InvalidArgumentException('Good id must be integer');
        }
        return $this->commentMapper->getCommentsWithYandex($goodId, $start, $limit);
    }
    
    /**
     * 
     * @param integer $goodId
     * @return \Traversable
     */
    public function getRatingWithYandex($goodId) {
        $ratings = $this->commentMapper->getRatingsWithYandex($goodId);
        if ($ratings['Count']) {
            return $ratings;
        }
        return null;
    }

}
