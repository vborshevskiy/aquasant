<?php

namespace SoloItem\Service;

use Solo\DateTime\DateTime;
use \Zend\Session\Container;
use SoloCatalog\Data\GoodsGateway;

class TimesVisitedService {

	/**
	 *
	 * @var array
	 */
	private $defaults = array(
		'watchTimePeriod' => 5,
		'showVirtual' => true,
		'virtualMin' => 1,
		'virtualMax' => 3,
		'virtualUpdateTime' => 2,
		'stats' => null 
	);

	/**
	 *
	 * @var SoloCatalog\Data\GoodsGateway
	 */
	private $queryGateway;

	public function __construct(GoodsGateway $queryGateway) {
		$this->queryGateway = $queryGateway;
	}

	/**
	 *
	 * @param \Predis\Client $pRedis        	
	 * @param int $itemId        	
	 * @param array $params        	
	 * @return int
	 */
	public function getTimesVisited($pRedis, $itemId, $params = []) {
		if (!$params) {
			$params = $this->defaults;
		}
		$container = new Container();
		$time = DateTime::now();
		$timeStamp = $time->getHours() * 3600 + $time->getMinutes() * 60;
		if (!$container->offsetExists('watch_now_unique_id')) {
			$uniqId = uniqid('');
			$container['watch_now_unique_id'] = $uniqId;
		} else {
			$uniqId = $container['watch_now_unique_id'];
		}
		$current = $pRedis->hget('item:' . $itemId, $timeStamp);
		if (strpos($current, $uniqId) === false) {
			if ($current != null) {
				$pRedis->hset('item:' . $itemId, $timeStamp, $current . '|' . $uniqId);
			} else {
				$pRedis->hset('item:' . $itemId, $timeStamp, $uniqId);
			}
			$pRedis->sadd('timeStamps', $timeStamp);
			if (!$pRedis->sismember('timeStamp:' . $timeStamp, $itemId)) {
				$pRedis->sadd('timeStamp:' . $timeStamp, $itemId);
			}
		}
		$keyData = $pRedis->hgetall('item:' . $itemId);
		$visits = [];
		$outOfDate = $timeStamp - ($params['watchTimePeriod'] + 1) * 60;
		foreach ($keyData as $key => $data) {
			if (($key > $outOfDate) && ($key <= $timeStamp)) {
				$visits = array_merge($visits, explode('|', $data));
			}
		}
		$visits = array_unique($visits);
		if ($params['showVirtual']) {
			$keyTtl = $params['virtualUpdateTime'] * 60;
			$keyStamp = intval($pRedis->hget('virtualUsersCount', $itemId . ':timeStamp'));
			$virtualUsers = intval($pRedis->hget('virtualUsersCount', $itemId . ':count'));
			if ((($timeStamp - $keyTtl) >= $keyStamp) || (!$virtualUsers)) {
				if ($params['stats'] == null) {
					$virtualUsers = 0;
				} else {
					try {
						$parts = (array)json_decode($pRedis->get('maxSalesInfo'));
						if (!$parts) {
							$parts = $this->updateMaxSales($pRedis, $params);
						}
						$max = floor($params['stats']->Quantity / $parts['step']);
						if ($max > $params['virtualMax']) {
							$max = $params['virtualMax'];
						}
						$virtualUsers = rand($params['virtualMin'], $max);
					} catch (\Exception $e) {
						$virtualUsers = rand($params['virtualMin'], $params['virtualMax']);
					}
				}
				$pRedis->hset('virtualUsersCount', $itemId . ':count', $virtualUsers);
				$pRedis->hset('virtualUsersCount', $itemId . ':timeStamp', $timeStamp);
			}
		} else {
			$virtualUsers = 0;
		}
		return count($visits) + $virtualUsers;
	}

	/**
	 *
	 * @param \Predis\Client $pRedis        	
	 * @return int
	 */
	public function unsetOutOfDateKeys($pRedis, $watchTimePeriod = 5, $deleteAllKeys = false) {
		if ($deleteAllKeys) {
			$keys = $pRedis->keys('*');
			if ($keys) {
				$pRedis->del($keys);
			}
			return count($keys);
		}
		set_time_limit(6000);
		$time = DateTime::now();
		$timeStamp = $time->getHours() * 3600 + $time->getMinutes() * 60;
		$timeStamps = $pRedis->smembers('timeStamps');
		for ($i = 0; $i <= $watchTimePeriod; $i++) {
			$key = array_search($timeStamp, $timeStamps);
			if ($key !== false) {
				unset($timeStamps[$key]);
			}
			$timeStamp -= 60;
		}
		$items = [];
		foreach ($timeStamps as $timeStamp) {
			$items = array_merge($items, $pRedis->smembers('timeStamp:' . $timeStamp));
			$pRedis->del('timeStamp:' . $timeStamp);
			$pRedis->srem('timeStamps', $timeStamp);
		}
		$items = array_unique($items);
		$pipe = $pRedis->multiExec();
		foreach ($items as $item) {
			foreach ($timeStamps as $stamp) {
				$pipe->hdel('item:' . $item, $stamp);
			}
		}
		$pipe->execute();
		return count($timeStamps) * count($items);
	}

	/**
	 *
	 * @param \Predis\Client $pRedis        	
	 * @param array $params        	
	 * @return array
	 */
	public function updateMaxSales($pRedis, $params = []) {
		if (!$params) {
			$params = $this->defaults;
		}
		$parts['max'] = $this->queryGateway->getMaxSales();
		$parts['count'] = $params['virtualMax'] - $params['virtualMin'] + 1;
		$parts['step'] = $parts['max'] / $parts['count'];
		$pRedis->set('maxSalesInfo', json_encode($parts));
		return $parts;
	}

	/**
	 *
	 * @param \Predis\Client $pRedis        	
	 * @return int
	 */
	public function test($pRedis, $itemId, $watchTimePeriod = 5) {
		set_time_limit(6000);
		$time = DateTime::now();
		$products = [];
		$sessions = [];
		$start = microtime(true);
		for ($do = 0; $do < 1000; $do++) {
			$item = $itemId + $do;
			if (isset($_SESSION['watch_now_unique_id'])) {
				$uniqId = $_SESSION['watch_now_unique_id'];
			} else {
				$uniqId = uniqid('');
				$_SESSION['watch_now_unique_id'] = $uniqId;
			}
			$timeStamp = $time->getHours() * 3600 + $time->getMinutes() * 60;
			$current = $pRedis->hget($item, $timeStamp);
			if (strpos($current, $uniqId + $do) === false) {
				if ($current != null) {
					$pRedis->hset('item:' . $item, $timeStamp, $current . '|' . $uniqId);
				} else {
					$pRedis->hset('item:' . $item, $timeStamp, $uniqId);
				}
				$pRedis->sadd('timeStamp:' . $timeStamp, $item);
				$pRedis->sadd('timeStamps', $timeStamp);
			}
			$keyData = $pRedis->hgetall('item:' . $item);
			$visits = [];
			$outOfDate = $timeStamp - $watchTimePeriod * 60;
			foreach ($keyData as $key => $data) {
				if ($key > $outOfDate) {
					$visits = array_merge($visits, explode('|', $data));
				}
			}
			$visits = array_unique($visits);
			$result = count($visits);
			echo 'item:' . $item . ' watch now: ' . $result . ' ';
		}
		$end = microtime(true);
		$timeSpend = $end - $start;
		$midTime = $timeSpend / 1000;
		echo '<br> uid: ' . $uniqId . ' time: ' . $timeSpend . ' mid time: ' . $midTime;
		exit();
	}

}

?>
