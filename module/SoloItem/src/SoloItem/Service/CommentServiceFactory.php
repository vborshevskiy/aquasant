<?php

namespace SoloItem\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class CommentServiceFactory extends AbstractServiceFactory {

    protected function create() {
        $commentMapper = $this->getServiceLocator()->get('SoloItem\\Data\\CommentMapper');
        $commentTable = $this->getServiceLocator()->get('SoloCatalog\\Data\\CommentTable');
        $service = new CommentService($commentMapper, $commentTable);
        return $service;
    }

}

?>