<?php

namespace SoloCatalog\Search;

use Solo\Search\IndexLocator\Sphinx\AbstractIndexLocator;

class CategoriesIndexLocator extends AbstractIndexLocator {

	/**
	 * (non-PHPdoc)
	 * @see \Solo\Search\IndexLocator\Sphinx\AbstractIndexLocator::processSearch()
	 */
	public function processSearch($expression) {
		$enableStar = true;
		$countSql = "SELECT COUNT(*) AS lim, 1 AS v FROM " . $this->getName() . " WHERE MATCH('*" . $expression . "*') GROUP BY v";
		$rows = $this->getConnection()->query($countSql)->execute();             
		$row = null;
		if (0 < $rows->count()) {
			$row = $rows->current();                       
		} else {
			$countSql = "SELECT COUNT(*) AS lim, 1 AS v FROM " . $this->getName() . " WHERE MATCH('" . $expression . "') GROUP BY v";
			$rows = $this->getConnection()->query($countSql)->execute();
			if (0 < $rows->count()) {
				$enableStar = false;
				$row = $rows->current();
			}
		}
	
		if (null !== $row) {                   
			$limit = ($this->options()->hasLimitItemsPerPage() ? $this->options()->getLimitItemsPerPage() : intval($row['lim']));                        
			if ($enableStar) $expression = '*' . $expression . '*';
			$sql = "SELECT
						id,
						uid,
						name
					FROM
						" . $this->getName() . "
					WHERE
						MATCH('" . $expression . "')                                        
					LIMIT
						" . $this->options()->getLimitPage() . ", " . $limit ."
                                        OPTION ranker=" . $this->getRanker();					                       
			$rows = $this->getConnection()->query($sql)->execute();
			$results = [];
			foreach ($rows as $row) {                                
				$categoryId = intval($row['id']);
				$results[$categoryId] = new \ArrayObject(
					[
						'CategoryID' => $categoryId,
						'CategoryUID' => $row['uid'],
						'CategoryName' => $row['name']
					]);
			}                        
			return $results;
		}
		return [];
	}

}

?>
