<?php

namespace SoloCatalog\Service;

use SoloCatalog\Data\ImagesMapper;
use Pel\PelDataWindow;
use Pel\PelJpeg;
use Pel\PelExif;
use Pel\PelIfd;
use Pel\PelEntryAscii;
use Pel\PelTag;
use Pel\PelTiff;

class ImageService {

	protected $imageMimeTypes = array(
		"jpeg" => "FFD8",
		"png" => "89504E470D0A1A0A",
		"gif" => "474946",
		"bmp" => "424D",
		"tiff" => "4949",
		"tiff" => "4D4D" 
	);

	/**
	 *
	 * @var ImagesMapper
	 */
	private $imagesMapper;

	/**
	 *
	 * @param ImagesMapper $imagesMapper        	
	 */
	public function __construct(ImagesMapper $imagesMapper) {
		$this->imagesMapper = $imagesMapper;
	}
	
	/*
	 * @param integer $goodId
	 */
	public function getImagesByGoodId($goodId) {
		return $this->imagesMapper->getImagesByGoodId($goodId);
	}

	public function getImagesByGoodIds($goodIds) {
		$result = [];
		
		if (0 < count($goodIds)) {
			$rows = $this->imagesMapper->getImagesByGoodIds($goodIds);
			foreach ($rows as $row) {
				$result[$row['GoodID']][] = $row;
			}
		}
		
		return $result;
	}
	
	/*
	 * @param integer $goodId
	 */
	public function getGoodMainImage($goodId) {
		return $this->imagesMapper->getGoodMainImage($goodId);
	}

	/**
	 *
	 * @param string $path        	
	 * @param array $deletedImages        	
	 * @return null
	 */
	public function deleteImage($path, $deletedImages, $sizes) {
		$deletedImagesInfo = $this->imagesMapper->getDeletedImages($deletedImages);
		$this->imagesMapper->deleteImages($deletedImages);
		foreach ($deletedImagesInfo as $image) {
			if (file_exists($path . $image['Url'])) {
				unlink($path . $image['Url']);
			}
			foreach ($sizes as $size) {
				if (file_exists($path . '/' . $size . '/' . $image['MiniatureUrl'])) {
					unlink($path . '/' . $size . '/' . $image['MiniatureUrl']);
				}
			}
		}
	}
	
	/*
	 * @param array $goodIds
	 */
	public function getGoodsMainImage($goodIds) {
		if (count($goodIds) === 0) {
			return null;
		}
		$result = [];
		$imagesData = $this->imagesMapper->getGoodsMainImage($goodIds);
		
		foreach ($imagesData as $imageData) {
			$productId = intval($imageData['GoodID']);
			if (!isset($result[$productId])) {
				$result[$productId] = $imageData;
			}
		}
		
		return $result;
	}

	/**
	 *
	 * @param string $imagePath        	
	 * @param string $imageName        	
	 * @param string $resizedPath        	
	 * @param integer $maxWidth        	
	 * @param integer $maxHeight        	
	 * @param array $exifHeaders        	
	 * @throws \RuntimeException
	 * @return boolean
	 */
	public function proportionalResize($imagePath, $imageName, $resizedPath, $maxWidth, $maxHeight, array $exifHeaders = []) {
		if (0 >= $maxWidth) {
			throw new \RuntimeException('Empty width');
		}
		if (0 >= $maxHeight) {
			throw new \RuntimeException('Empty height');
		}
		if (!file_exists($imagePath . $imageName)) {
			return false;
		}
		
		$image = new \Imagick($imagePath . $imageName);
		
		$imgWidth = $image->getImageWidth();
		$imgHeight = $image->getImageHeight();
		
		$factor = min($maxWidth / $imgWidth, $maxHeight / $imgHeight);
		
		$newWidth = ceil($imgWidth * $factor);
		$newHeight = ceil($imgHeight * $factor);
		if (($newWidth > $imgWidth) || ($newHeight > $imgHeight)) {
			$newWidth = $imgWidth;
			$newHeight = $imgHeight;
		}
		
		$image->resizeImage($newWidth, $newHeight, \Imagick::FILTER_LANCZOS, 1, true);
		
		if (!file_exists($imagePath . $maxWidth . 'x' . $maxHeight)) {
			mkdir($imagePath . $maxWidth . 'x' . $maxHeight);
		}
		$image->writeimage($resizedPath);
		
		// exif
		if (0 < count($exifHeaders)) {
			if ((false !== stripos($imageName, 'jpeg')) || (false !== stripos($imageName, 'jpg'))) {
				try {
					$data = new PelDataWindow(file_get_contents($resizedPath));
					if (PelJpeg::isValid($data)) {
						$jpeg = new PelJpeg();
						$jpeg->load($data);
						$exif = $jpeg->getExif();
						if (null === $exif) {
							$exif = new PelExif();
							$exif->setTiff(new PelTiff());
							$jpeg->setExif($exif);
						}
						$tiff = $jpeg->getExif()->getTiff();
						$ifd0 = new PelIfd(PelIfd::IFD0);
						
						$ifdExif = new PelIfd(PelIfd::EXIF);
						$ifd0->addSubIfd($ifdExif);
						
						$ifdCanon = new PelIfd(PelIfd::CANON_MAKER_NOTES);
						$ifd0->addSubIfd($ifdCanon);
						
						$tiff->setIfd($ifd0);
						
						foreach ($exifHeaders as $exifHeader) {
							switch ($exifHeader['name']) {
								case 'copyright':
									$tiffEntry = new PelEntryAscii(PelTag::COPYRIGHT, $exifHeader['value']);
									$ifd0->addEntry($tiffEntry);
									break;
								case 'owner':
									$tiffEntry = new PelEntryAscii(PelTag::OWNER_NAME, $exifHeader['value']);
									$ifd0->addEntry($tiffEntry);
									break;
								case 'description':
									$tiffEntry = new PelEntryAscii(PelTag::IMAGE_DESCRIPTION, $exifHeader['value']);
									$ifd0->addEntry($tiffEntry);
									break;
								case 'user-comment':
									$tiffEntry = new PelEntryAscii(PelTag::USER_COMMENT, $exifHeader['value']);
									$ifdExif->addEntry($tiffEntry);
									break;
							}
						}
						
						$jpeg->saveFile($resizedPath);
					}
				} catch (\Exception $ex) {
					// do nothing
				}
			}
		}
		// END exif
		
		return (file_exists($resizedPath));
	}

	public function resize($imagePath, $resizedPath, $maxWidth, $maxHeight) {
		if (0 >= $maxWidth) {
			throw new \RuntimeException('Empty width');
		}
		if (0 >= $maxHeight) {
			throw new \RuntimeException('Empty height');
		}
		if (!file_exists($imagePath)) {
			return false;
		}
		
		$image = new \Imagick($imagePath);
		
		$imgWidth = $image->getImageWidth();
		$imgHeight = $image->getImageHeight();
		
		$factor = max($maxWidth / $imgWidth, $maxHeight / $imgHeight);
		
		$newWidth = ceil($imgWidth * $factor);
		$newHeight = ceil($imgHeight * $factor);
		
		$image->resizeImage($newWidth, $newHeight, \Imagick::FILTER_LANCZOS, 1, true);
		
		$x = $y = 0;
		if ($newWidth > $newHeight) {
			$x = floor(($newWidth - $maxWidth) / 2);
		} else {
			$y = floor(($newHeight - $maxHeight) / 2);
		}
		
		if (false) {
			$image->cropImage($maxWidth, $maxHeight, $x, $y);
		}
		
		$image->writeimage($resizedPath);
		return (file_exists($resizedPath));
	}

	public function clearFolder($folder) {
		$files = glob($folder . '*');
		
		foreach ($files as $file) {
			if (is_file($file)) {
				unlink($file);
			}
		}
	}

	/**
	 *
	 * @param string $base64Image        	
	 * @return string
	 */
	public function saveBase64Image($base64Image, $imgPath, $imgName = null) {
		if (!file_exists($imgPath)) {
			mkdir($imgPath);
		}
		$image = base64_decode($base64Image);
		$mimeType = $this->getImageMimeType($image);
		$fileName = (!empty($imgName) ? $imgName : uniqid()) . '.' . ($mimeType !== null ? $mimeType : 'jpg');
		if (file_put_contents($imgPath . $fileName, $image)) {
			return $fileName;
		}
		return null;
	}

	/**
	 *
	 * @param string $hexData        	
	 * @return string
	 */
	protected function getBytesFromHexString($hexData) {
		for ($count = 0; $count < strlen($hexData); $count += 2) {
			$bytes[] = chr(hexdec(substr($hexData, $count, 2)));
		}
		return implode($bytes);
	}

	/**
	 *
	 * @param string $imageData        	
	 * @return string
	 */
	protected function getImageMimeType($imageData) {
		foreach ($this->imageMimeTypes as $mime => $hexBytes) {
			$bytes = $this->getBytesFromHexString($hexBytes);
			if (substr($imageData, 0, strlen($bytes)) == $bytes) {
				return $mime;
			}
		}
		return null;
	}

}

?>