<?php

namespace SoloCatalog\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class FiltersServiceFactory extends AbstractServiceFactory {

	protected function create() {
		$service = new FiltersService($this->getServiceLocator()->get('SoloCatalog\\Data\\FiltersMapper'));
		return $service;
	}

}

?>