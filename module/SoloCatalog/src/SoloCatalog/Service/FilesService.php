<?php

namespace SoloCatalog\Service;

use SoloCatalog\Data\BrandsMapperInterface;
use SoloCatalog\Data\FilesMapper;
use SoloCatalog\Entity\Brands\BrandCollection;
use SoloCatalog\Entity\Brands\Brand;

class FilesService {

    /**
     * @var FilesMapper
     */
    protected $filesMapper;

    public function __construct(FilesMapper $filesMapper) {
        $this->filesMapper = $filesMapper;
    }

    public function getFilesByGoodId($goodId) {
        return $this->filesMapper->getFilesByGoodId($goodId);
    }
}

?>