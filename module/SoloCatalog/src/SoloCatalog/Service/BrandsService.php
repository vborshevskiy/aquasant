<?php

namespace SoloCatalog\Service;

use SoloCatalog\Data\BrandsMapperInterface;
use SoloCatalog\Entity\Brands\BrandCollection;
use SoloCatalog\Entity\Brands\Brand;

class BrandsService {

    /**
     *
     * @var BrandsMapperInterface
     */
    private $brandsMapper;

    /**
     *
     * @param BrandsMapperInterface $brandsMapper        	
     */
    public function __construct(BrandsMapperInterface $brandsMapper) {
        $this->brandsMapper = $brandsMapper;
    }

    /**
     * 
     * @param array $brandIds
     * @throws \InvalidArgumentException
     * @return Brand
     */
    public function getBrandByIds($brandIds) {
        if (empty($brandIds)) {
            return [];
        }
        $brandsData = $this->brandsMapper->getBrandByIds($brandIds);
        $brandCollection = new BrandCollection();
        foreach ($brandsData as $brandData) {
            $brandCollection->add($this->createBrand($brandData));
        }
        return $brandCollection;
    }
    
    /**
     * 
     * @param integer $brandId
     * @throws \InvalidArgumentException
     * @return Brand
     */
    public function getBrandById($brandId) {
        if (is_int($brandId)) {
            throw new \InvalidArgumentException('Brand id must be integer');
        }
        $brandData = $this->brandsMapper->getBrandById($brandId);
        if (!is_null($brandData)) {
            return $this->createBrand($brandData);
        }
        return null;
    }
    
    /**
     * 
     * @param string $brandUid
     * @throws \InvalidArgumentException
     * @return Brand
     */
    public function getBrandByUid($brandUid) {
        $brandData = $this->brandsMapper->getBrandByUid($brandUid);
        if (!is_null($brandData)) {
            return $this->createBrand($brandData);
        }
        return null;
    }

    /**
     * 
     * @param ResultSet $brandData
     * @return Brand
     */
    private function createBrand($brandData) {
        $brand = new Brand();
        $brand->setId((int) $brandData['BrandID']);
        $brand->setName($brandData['BrandName']);
        $brand->setUid($brandData['BrandUID']);
        return $brand;
    }

}

?>