<?php

namespace SoloCatalog\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class SatelliteServiceFactory extends AbstractServiceFactory {

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \Solo\ServiceManager\AbstractServiceFactory::create()
	 */
	protected function create() {
		$groupsTable = $this->getServiceLocator()->get('SoloCatalog\Data\SatelliteGroupsTable');
		$goodsTable = $this->getServiceLocator()->get('SoloCatalog\Data\SatelliteGoodsTable');
		$goodsMapper = $this->getServiceLocator()->get('SoloCatalog\Data\SatelliteGoodsMapper');
		$service = new SatelliteService($groupsTable, $goodsTable, $goodsMapper);
		return $service;
	}

}

?>