<?php

namespace SoloCatalog\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class ImageServiceFactory extends AbstractServiceFactory {

    protected function create() {
        $imageMapper = $this->getServiceLocator()->get('SoloCatalog\\Data\\ImagesMapper');
        $service = new ImageService($imageMapper);
        return $service;
    }

}

?>