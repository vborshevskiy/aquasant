<?php

namespace SoloCatalog\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class GoodsServiceFactory extends AbstractServiceFactory {

	protected function create() {
        $goodsMapper = $this->getServiceLocator()->get('SoloCatalog\Data\GoodsMapper');
        $categoriesMapper = $this->getServiceLocator()->get('SoloCatalog\Data\CategoriesMapper');
        $arrivalGoodsMapper = $this->getServiceLocator()->get('SoloCatalog\Data\ArrivalGoodsMapper');
        $goodNotesMapper = $this->getServiceLocator()->get('SoloCatalog\Data\GoodNotesMapper');
		$service = new GoodsService($goodsMapper, $categoriesMapper, $arrivalGoodsMapper, $goodNotesMapper);
		$config = $this->getConfig();
		if (isset($config['catalog'])) {
			$settings = $config['catalog'];
			if (isset($settings['defaults']) && is_array($settings['defaults'])) {
				foreach ($settings['defaults'] as $name => $value) {
					$service->setDefaultValue($name, $value);
				}
			}
			if (isset($settings['sort_columns'])) {
				foreach ($settings['sort_columns'] as $uid => $column) {
					$service->addSortingColumn($uid, $column);
				}
			}
			if (isset($settings['sort_directions'])) {
				foreach ($settings['sort_directions'] as $direction) {
					$service->addSortingDirection($direction);
				}
			}

			$service->addSortingColumn('delivery', '0');
		}
		return $service;
	}

}

?>