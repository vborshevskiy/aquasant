<?php

namespace SoloCatalog\Service\Helper;

final class DateHelper {

    const DATE_FORMAT_WITH_YEAR = 1;

    const DATE_FORMAT_WITHOUT_YEAR = 2;

    protected $plurals = array(
        'days' => ['день', 'дня', 'дней'],
        'weeks' => ['неделя', 'недели', 'недель']
    );

    public static function isToday(\DateTime $date) {
        $checkingDate = new \DateTime('now');
        return ((int) $checkingDate->format('Y') === (int) $date->format('Y') && (int) $checkingDate->format('m') === (int) $date->format('m') && (int) $checkingDate->format('d') === (int) $date->format('d'));
    }
    public static function isTodayString($date){
        if(is_object($date)){
            return $this->isToday($date);
        }
        return (date("Y-m-d")==date("Y-m-d",strtotime($date)));
    }

    public static function getCountDayToDate(\DateTime $date) {
        $date1 = new \DateTime();
        $date1->setTimestamp(mktime(0, 0, 0, date('n'), date('j'), date('Y')));

        $date2 = new \DateTime();
        $date2->setTimestamp(mktime(0, 0, 0, date('n', $date->getTimestamp()), date('j', $date->getTimestamp()), date('Y', $date->getTimestamp())));

        return $date1->diff($date2)->days;
    }

    public static function isTomorrow(\DateTime $date) {
        $checkingDate = new \DateTime('tomorrow');
        return ((int) $checkingDate->format('Y') === (int) $date->format('Y') && (int) $checkingDate->format('m') === (int) $date->format('m') && (int) $checkingDate->format('d') === (int) $date->format('d'));
    }

    public static function isDayAfterTomorrow(\DateTime $date) {
        $checkingDate = new \DateTime('tomorrow + 1day');
        return ((int) $checkingDate->format('Y') === (int) $date->format('Y') && (int) $checkingDate->format('m') === (int) $date->format('m') && (int) $checkingDate->format('d') === (int) $date->format('d'));
    }

    public static function isIdenticalDates(\DateTime $date1, \DateTime $date2) {
        return ((int) $date1->format('Y') === (int) $date2->format('Y') && (int) $date1->format('m') === (int) $date2->format('m') && (int) $date1->format('d') === (int) $date2->format('d'));
    }

    public static function isFutureDate(\DateTime $date) {
        $now = new \DateTime('now');
        return ($now->diff($date)->format('%R') === '+');
    }

    public static function isFirstDateMore(\DateTime $firstDate, \DateTime $secondDate) {
        return ($firstDate->diff($secondDate)->format('%R') === '-');
    }

    public static function isInDaysIntervalDate(\DateTime $date, $days) {
        if ($days == 1 || $days == -1) {
            $intervalDate = new \DateTime('now +'.$days.' day');
        } else {
            $intervalDate = new \DateTime('now +'.$days.' days');
        }
        return ($intervalDate->diff($date)->format('%R') === '-');
    }

//    public static function isInWorkDaysIntervalDate(\DateTime $date, $days, $holidays = null) {
//        // если сегодня выходной, то пропускаем этот день
//        $startDate = new \DateTime('now');
//        if (self::isHoliday($startDate, $holidays)) {
//            $startDate->modify('+ 1day')->setTime(0,0,1);
//        }
//        // если передаваемая дата выходной, то обнуляем время
//        if (self::isHoliday($date, $holidays)) {
//            $date->setTime(0,0,0);
//        }
//
//        // считаем количество выходных, целиком (все 24 часа) попадающих в интервал между завтрашним днем и датой
//        $checkedDate = clone $startDate;
//        $checkedEndDate = clone $date;
//        $checkedEndDate->setTime(0,0,0);
//        $holidaysCount = 0;
//        while ($checkedEndDate->diff($checkedDate)->format('%R') === '-') {
//            if (self::isHoliday($checkedDate, $holidays)) {
//                $holidaysCount++;
//            }
//            $checkedDate->modify('+ 1day');
//        }
//
//        $intervalDate = clone $startDate;
//        $addingDays = $days + $holidaysCount;
//        $intervalDate->modify('+ ' . $addingDays . 'days');
//        return ($date->diff($intervalDate)->format('%R') === '+');
//    }

    public static function isInWorkDaysIntervalDate(\DateTime $date, $days, $holidays = null) {
        $startDate = new \DateTime('now');
        $startDate->setTime(12,0,0);
        $date->setTime(12,0,0);
        $daysDist = (int)$date->diff($startDate)->format('%d');
        if (is_array($holidays)) {
            foreach ($holidays as $holiday) {
                $holiday->setTime(12,0,0);
                if ($holiday->getTimestamp() >= $startDate->getTimestamp() && $holiday->getTimestamp() <= $date->getTimestamp()) {
                    $daysDist--;
                }
            }
        }
        return ($daysDist <= $days);
    }

    public static function getWorkDaysInterval(\DateTime $date, $holidays = null) {
        $holidayDaysTakenAway = false;
        $startDate = new \DateTime('now');
        $startDate->setTime(12,0,0);
        $date->setTime(12,0,0);
        $daysDist = (int)$date->diff($startDate)->format('%d');
        if (is_array($holidays)) {
            foreach ($holidays as $holiday) {
                $holiday->setTime(12,0,0);
                if ($holiday->getTimestamp() >= $startDate->getTimestamp() && $holiday->getTimestamp() <= $date->getTimestamp()) {
                    $holidayDaysTakenAway = true;
                    $daysDist--;
                }
            }
        }
        if ($holidayDaysTakenAway && $daysDist < 1) {
            $daysDist = 1;
        }
        if (0 <= $daysDist) {
            return $daysDist;
        }
        return null;
    }

    public static function isHoliday(\DateTime $date, $holidays = null) {
        if (is_array($holidays)) {
            return self::isDateInArray($date, $holidays);
        }
        return false;
    }

    public static function isFirstDateMoreByHouresAndMinutes(\DateTime $dateFirst, \DateTime $dateSecond) {
        if ($dateFirst->format('H') > $dateSecond->format('H')) return true;
        if ($dateFirst->format('H') == $dateSecond->format('H') && $dateFirst->format('i') > $dateSecond->format('i')) return true;
        return false;
    }

    public static function isLessThanSomeMinutes(\DateTime $date, $minutes = 5) {
        $checkingDate = new \DateTime('now');
        return ((int) $checkingDate->format('Y') === (int) $date->format('Y') && (int) $checkingDate->format('m') === (int) $date->format('m') && (int) $checkingDate->format('d') === (int) $date->format('d') && (int) $checkingDate->format('H') === (int) $date->format('H') && $checkingDate->diff($date)->format('%i') < $minutes);
    }

    public static function getMonthNameGenitive($month) {
        $names = [
            1 => 'января',
            2 => 'февраля',
            3 => 'марта',
            4 => 'апреля',
            5 => 'мая',
            6 => 'июня',
            7 => 'июля',
            8 => 'августа',
            9 => 'сентября',
            10 => 'октября',
            11 => 'ноября',
            12 => 'декабря'
        ];
        return $names[(int) $month];
    }

    /**
     *
     * @param integer $format
     * @param integer $val Timestamp
     * @return string
     */
    public function parseDate($format, $val) {
        if($format == self::DATE_FORMAT_WITH_YEAR) {
            //25 ноября 2014
            return date("j", $val).' '.$this->getMonthNameGenitive(date("n", $val)).' '.date("Y", $val);
        } elseif ($format == self::DATE_FORMAT_WITHOUT_YEAR) {
            // 25 ноября
            return date("j", $val).' '.$this->getMonthNameGenitive(date("n", $val));
        }
        return null;
    }

    /**
     *
     * @param \DateTime $date
     * @return string
     */
    public static function getTextDate(\DateTime $date) {
        return $date->format('d') . ' ' . self::getMonthNameGenitive($date->format('m'));
    }

    /*
     * @param array $dates
     * @throw RuntimeException
     * @return \DateTime
     */
    public static function getMaxDate($dates) {
        if (!is_array($dates)) {
            return null;
        }
        $result = null;
        foreach ($dates as $date) {
            if ($date instanceof \DateTime) {
                if (is_null($result) || $result->diff($date)->format('%R') === '+') {
                    $result = $date;
                }
            } else {
                throw new \RuntimeException('Date must be an instace of DateTime');
            }
        }
        return $result;
    }

    /*
     * @param array $dates
     * @throw RuntimeException
     * @return \DateTime
     */
    public static function getMinDate($dates) {
        if (!is_array($dates)) {
            return null;
        }
        $result = null;
        foreach ($dates as $date) {
            if ($date instanceof \DateTime) {
                if (is_null($result) || $result->diff($date)->format('%R') !== '+') {
                    $result = $date;
                }
            } else {
                throw new \RuntimeException('Date must be an instace of DateTime');
            }
        }
        return $result;
    }

    public static function isNewYear($date) {
        return ((int) $date->format('m') === 1 && (int) $date->format('d') === 1);
    }

    public static function isMonday($date) {
        return ((int) $date->format('w') === 1);
    }

    public static function getSerializeJsonDate($date) {
        return '/Date('.$date->getTimestamp().'000+0400)/';
    }

    public static function isDateInArray($needlyDate, $dates) {
        if (is_array($dates) && count($dates) > 0) {
            foreach ($dates as $date) {
                if (self::isIdenticalDates($needlyDate, $date)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function getRussianShortWeekdayByNumber($number) {
        $names = [
            1 => 'пн',
            2 => 'вт',
            3 => 'ср',
            4 => 'чт',
            5 => 'пт',
            6 => 'сб',
            7 => 'вс',
        ];
        if (array_key_exists($number, $names)) {
            return $names[$number];
        }
        return null;
    }

    public static function getRussianWeekdayByNumber($number) {
        $names = [
            1 => 'понедельник',
            2 => 'вторник',
            3 => 'среда',
            4 => 'четверг',
            5 => 'пятница',
            6 => 'суббота',
            7 => 'воскресенье',
        ];
        if (array_key_exists($number, $names)) {
            return $names[$number];
        }
        return null;
    }

    public static function getAccusativeRussianWeekdayByNumber($number) {
        $names = [
            1 => 'понедельник',
            2 => 'вторник',
            3 => 'среду',
            4 => 'четверг',
            5 => 'пятницу',
            6 => 'субботу',
            7 => 'воскресенье',
        ];
        if (array_key_exists($number, $names)) {
            return $names[$number];
        }
        return null;
    }

    public static function getFormattedTime($hours = 0, $minutes = 0) {
        $date = new \DateTime();
        $date->setTime($hours,$minutes,0);
        return $date->format('H:i');
    }

    /**
     *
     * @param string $date
     * @return DateTime
     */
    public static function parseJsonDate($date) {
        preg_match('/(\d{10})(\d{3})([\+\-]\d{4})/', $date, $matches);
        $ts = (int) $matches[1];
        
        if (0 == $ts) {
        	$ts = strtotime($date);
        }
        
        $dt = new \DateTime();
        $dt->setTimestamp($ts);
        return $dt;
    }

    public function plural($number, $intervalType = 'days')
    {
        $plural = $number%10==1 && $number%100!=11 ? 0 : (($number%10>=2 && $number%10<=4 && ($number%100<10 || $number%100>=20)) ? 1 : 2);

        return $this->plurals[$intervalType][$plural];
    }

    public function getDeliveryDateRange($date)
    {
        if (!$date instanceof \DateTime) {
            $date = \DateTime::createFromFormat('Y-m-d', $date);
        }

        $dayCount = DateHelper::getCountDayToDate($date);

        if ($dayCount > 6) {
            $dateRangeStart = floor($dayCount / 7);
            $intervalType = 'weeks';
        } else {
            $dateRangeStart = $dayCount;
            $intervalType = 'days';
        }

        $dateRangeEnd = $dateRangeStart + 1;

        return ($dateRangeStart ? join('−', [$dateRangeStart, $dateRangeEnd]) : $dateRangeEnd) . ' ' . $this->plural($dateRangeEnd, $intervalType);
    }

	public function getPickupDateDescription($date)
	{
		if (!$date instanceof \DateTime) {
			$date = \DateTime::createFromFormat('Y-m-d H:i:s', $date);
		}

		$txt = '';
		if ($this->isTomorrow($date)) {
			$txt = 'завтра';
		} elseif ($this->isToday($date)) {
			$txt = 'сегодня';
			$now = new \DateTime();
			$diff = $now->diff($date, true);
			if (1 > $diff->h && 0 == $diff->d) {
				$txt = 'сейчас';
			}
		}
		return $txt;
    }

    public function getDeliveryDateText($date) {
        return ($date->format('N') == 2 ? 'во ' : 'в ') .
            DateHelper::getAccusativeRussianWeekdayByNumber($date->format('N')) . ', ' .
            $date->format('j') . ' ' . DateHelper::getMonthNameGenitive($date->format('n'));
    }

    public function getPickupDateText($date) {
        return ($date->format('N') == 2 ? 'во ' : 'в ') .
            DateHelper::getAccusativeRussianWeekdayByNumber($date->format('N')) . ' <br />' .
            $date->format('j') . ' ' . DateHelper::getMonthNameGenitive($date->format('n'));
    }
    
    public function getPickupShortDateText($date) {
    	return $date->format('j') . ' ' . DateHelper::getMonthNameGenitive($date->format('n'));
    }
    
    /**
     * 
     * @param DateTime $date
     * @return string
     */
    public function getOutcomeShortDateText($date) {
    	$result = '';
    	if ($date instanceof \DateTime) {
    		$result = $date->format('j') . ' ' . DateHelper::getMonthNameGenitive($date->format('n'));
    	} elseif (is_string($date)) {
    		$normalizedTimestamp = strtotime($date);
    		if (0 < $normalizedTimestamp) {
    			$result = date('j', $normalizedTimestamp) . ' ' . DateHelper::getMonthNameGenitive(date('n', $normalizedTimestamp));
    		}
    	}
    	return $result;
    }


    public static function getDayName(\DateTime $date) {
        if (self::isToday($date)) {
            return 'сегодня';
        } elseif (DateHelper::isTomorrow($date)) {
            return 'завтра';
        } elseif (DateHelper::isDayAfterTomorrow($date)) {
            return 'послезавтра';
        }

        return '';
    }
}

?>
