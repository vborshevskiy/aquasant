<?php

namespace SoloCatalog\Service\Helper;

use Solo\ServiceManager\AbstractServiceFactory;

class PricesHelperFactory extends AbstractServiceFactory {

	protected function create() {
		$helper = new PricesHelper();
		$config = $this->getConfig();
		if (isset($config['catalog']) && isset($config['catalog']['prices'])) {
			$settings = $config['catalog']['prices'];
			if (isset($settings['columns'])) {
				foreach ($settings['columns'] as $column) {
					foreach ($column['types'] as $typeUid => $type) {
						$helper->addPriceColumn($column['categoryId'], $column['number'], $typeUid, $type['name']);
					}
				}
			}
			if (isset($settings['defaultColumnNumber'])) {
				$helper->setDefaultColumnNumber(intval($settings['defaultColumnNumber']));
			}
		}
		return $helper;
	}

}

?>