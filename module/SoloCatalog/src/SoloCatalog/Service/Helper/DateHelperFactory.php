<?php

namespace SoloCatalog\Service\Helper;

use Solo\ServiceManager\AbstractServiceFactory;

class DateHelperFactory extends AbstractServiceFactory {

	protected function create() {
		$helper = new DateHelper();
		return $helper;
	}

}

?>