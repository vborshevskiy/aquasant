<?php

/*
 * Старый хелпер, должен быть переделан после введения ценовых колонов для пользователей и ценовых зон
 */

namespace SoloCatalog\Service\Helper;

final class PricesHelper {
    
    const BONUS_COLUMN_ID = 12;
    
    const DEFAULT_PRICE_COLUMN_ID = 1;
    
    const DEFAULT_REGION_PRICE_COLUMN_ID = 1;

    /**
     *
     * @var array
     */
    private $priceColumns = [];

    /**
     *
     * @var integer
     */
    private $defaultPriceColumnNumber = -1;

//    /*
//     * Временно
//     */
//    public function getCityId() {
//        $cook = (Cookie::exists('selectedCity')) ? Cookie::get('selectedCity') : null;
//        if (!empty($cook))
//            return $cook;
//        else{
//            $cities = $this->facility()->cities()->getAllCities();
//            return $cities[0]['CityID'];
//        }
//    }

    /*
     * Временно
     */
    public function getPriceColumnId($isRegion = false) {
        if ($isRegion) {
            return self::DEFAULT_REGION_PRICE_COLUMN_ID;
        }
        return self::DEFAULT_PRICE_COLUMN_ID;
    }
    
    public function getBonusPriceColumnId() {
        return self::BONUS_COLUMN_ID;
    }

    /**
     *
     * @param integer $priceCategoryId        	
     * @param integer $number        	
     * @param integer $type        	
     * @param string $name        	
     */
    public function addPriceColumn($priceCategoryId, $number, $type, $name) {
        if (!isset($this->priceColumns[$priceCategoryId])) {
            $this->priceColumns[$priceCategoryId] = [
                'priceCategoryId' => $priceCategoryId,
                'number' => $number,
                'types' => []
            ];
        }
        $this->priceColumns[$priceCategoryId]['types'][$type] = [
            'type' => $type,
            'name' => $name
        ];
    }

    /**
     *
     * @param integer $number        	
     */
    public function setDefaultColumnNumber($number) {
        $this->defaultPriceColumnNumber = $number;
    }

    /**
     *
     * @return integer
     */
    public function getDefaultColumnNumber() {
        return $this->defaultPriceColumnNumber;
    }

    /**
     *
     * @return array | NULL
     */
    private function getDefaultColumn() {
        foreach ($this->priceColumns as $column) {
            if ($this->defaultPriceColumnNumber == $column['number']) {
                return $column;
            }
        }
        return null;
    }

    /**
     *
     * @return integer
     */
    public function getDefaultCategoryId() {
        $column = $this->getDefaultColumn();
        if (null !== $column) {
            return $column['priceCategoryId'];
        }
        return -1;
    }

    /**
     *
     * @param integer $priceCategoryId        	
     * @return integer
     */
    public function getColumnNumber($priceCategoryId) {
        if (array_key_exists($priceCategoryId, $this->priceColumns)) {
            return $this->priceColumns[$priceCategoryId]['number'];
        }
        return $this->getDefaultColumnNumber();
    }

    /**
     *
     * @param integer $priceCategoryId        	
     * @param string $type        	
     * @return string
     */
    public function getColumnName($priceCategoryId, $type = 'default') {
        if (array_key_exists($priceCategoryId, $this->priceColumns) && array_key_exists($type, $this->priceColumns[$priceCategoryId]['types'])) {
            return $this->priceColumns[$priceCategoryId]['types'][$type]['name'];
        }
        return $this->getDefaultColumnName($type);
    }

    /**
     *
     * @param string $type        	
     * @return string
     */
    public function getDefaultColumnName($type = 'default') {
        $defaultPriceColumn = $this->getDefaultColumn();
        if ((null !== $defaultPriceColumn) && array_key_exists($type, $defaultPriceColumn['types'])) {
            return $defaultPriceColumn['types'][$type]['name'];
        }
        return '';
    }

}

?>