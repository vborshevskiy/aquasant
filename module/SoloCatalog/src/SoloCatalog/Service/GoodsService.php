<?php

namespace SoloCatalog\Service;

use SoloCatalog\Data\GoodNotesMapper;
use SoloCatalog\Entity\Goods\FilterOptions;
use SoloCatalog\Entity\Goods\FilterGood;
use Solo\Cookie\Cookie;
use Solo\Stdlib\ArrayHelper;
use SoloCatalog\Data\GoodsMapperInterface;
use SoloCatalog\Data\CategoriesMapperInterface;
use SoloCatalog\Data\ArrivalGoodsMapperInterface;
use SoloCatalog\Entity\Avail\Avail;
use SoloCatalog\Entity\Avail\AvailCollection;
use Zend\Db\ResultSet\ResultSet;
use Solo\Db\QueryGateway\QueryGateway;

class GoodsService {

	private $defaults = [];

	private $sortingColumns = [];

	private $sortingDirections = [];

	/**
	 *
	 * @var GoodsMapperInterface
	 */
	private $goodsMapper;

	/**
	 *
	 * @var ArrivalGoodsMapperInterface
	 */
	private $arrivalGoodsMapper;

	/**
	 *
	 * @var CategoriesMapperInterface
	 */
	private $categoriesMapper;

	/**
	 *
	 * @var GoodNotesMapper
	 */
	private $goodNotesMapper;

	/**
	 *
	 * @param GoodsMapperInterface $goodsMapper
	 * @param CategoriesMapperInterface $categoriesMapper
	 */
	public function __construct(GoodsMapperInterface $goodsMapper, CategoriesMapperInterface $categoriesMapper, ArrivalGoodsMapperInterface $arrivalGoodsMapper, GoodNotesMapper $goodNotesMapper) {
		$this->goodsMapper = $goodsMapper;
		$this->categoriesMapper = $categoriesMapper;
		$this->arrivalGoodsMapper = $arrivalGoodsMapper;
		$this->goodNotesMapper = $goodNotesMapper;
	}

	public function createOptions() {
		return new FilterOptions();
	}

	public function setDefaultValue($name, $value) {
		$this->defaults[$name] = $value;
	}

	public function getDefaultValue($name) {
		if (array_key_exists($name, $this->defaults)) {
			return $this->defaults[$name];
		}
		return null;
	}

	public function setUserDefinedSorting($column, $direction) {
		$value = implode('|', array(
			$column,
			$direction
		));

		Cookie::set('catalog_sort', $value, null, 0, '/');
		$_COOKIE['catalog_sort'] = $value;
	}

	/**
	 * Получаем установленную сортировку для каталога, предположительно
	 * параметры сортировки передаются из контроллеров, если нет - смотрим cookies
	 *
	 * @param string $dir
	 *        	направление сортировки из GET (или руками)
	 * @param string $column
	 *        	колонка сортировки из GET (или руками)
	 * @return array null
	 */
	public function getUserDefinedSorting($dir = null, $column = null) {

		// смотрим что валидно направление и колонка
		if ($dir && in_array($dir, [
			'asc',
			'desc'
		]) !== false && $this->getSortingColumn($column)) {
			return [
				'column' => $column,
				'direction' => $dir
			];
		}

		// если в гете ничего путнего нет, пробуем поднять из куки
		$cook = (Cookie::exists('catalog_sort')) ? Cookie::get('catalog_sort') : null;
		if (!empty($cook)) {
			$params = explode('|', $cook);

			// валидируем куку
			if (2 == sizeof($params) && in_array($params[1], [
				'asc',
				'desc'
			]) !== false && $this->getSortingColumn($params[0])) {
				$sorting = array(
					'column' => trim($params[0]),
					'direction' => trim($params[1])
				);
				if (!empty($sorting['column']) && !empty($sorting['direction'])) {
					return $sorting;
				}
			}
		}
		return null;
	}

	public function hasUserDefinedSorting() {
		return Cookie::exists('catalog_sort');
	}

	public function getRandomWord() {
		$words = [
			'',
			'в интернете',
			'в интернет магазине Аквасант',
			'дешево'
		];
		return $words[rand(0, 3)];
	}

	public function enumFilterGoods(FilterOptions $options, $cityId, $priceCategoryId, $priceZoneId, $categoryId) {
		if (!$options->hasGoods()) {
			return [];
		}

		$rows = $this->goodsMapper->enumFilterGoods($options, $cityId, $priceCategoryId, $priceZoneId);
		$goods = $this->prepareGoods($rows, $categoryId);
		return $goods;
	}

	protected function prepareGoods($rows, $categoryId) {
		$goods = [];

		if (is_array($categoryId)) {
			$categories = $categoryId;
		} else {
			$categories = $this->categoriesMapper->enumCategoriesByParentId($categoryId);
		}

		$places = [];

		foreach ($rows as $row) {
			$good = new FilterGood();

			$good->setId(intval($row['GoodID']));
			$good->setName($row['GoodName']);
			$good->setAvatar($row['Avatar']);
			$good->setUrl('/' . $row['GoodID'] . '_' . $row['GoodEngName'] . '/');
			if (GOD_MODE) {
				$good->setUrl('/goods' . $good->getUrl() . '?polygon=1');
			} elseif (0 < sizeof($categories)) {
				$category = (isset($categories[$row['CategoryID']]) ? $categories[$row['CategoryID']] : null);
				if (null !== $category) {
					$good->setUrl('/' . $category['CategoryUID'] . $good->getUrl());
				} else {
					if (MANAGER_MODE) {
						$good->setUrl('/goods' . $good->getUrl() . '?manager=1');
					}
				}
			}

			$good->addPrice('retail', $row['Value']);
			$good->addPrice('prev', $row['PrevValue']);

			$onlyInShowroom = false;
			if (($row['GoodAvailQuantity'] == $row['WindowQuantity']) && (0 == $row['SuborderQuantity'])) {
				$onlyInShowroom = true;
			}
			$good->onlyInShowroom($onlyInShowroom);

			//if ((1 > $row["GoodAvailQuantity"]) || ((($row['GoodAvailQuantity'] == $row['WindowQuantity'])) && (0 < $row['SuborderQuantity']))) {
				//$good->additional('arrivalDate', \DateTime::createFromFormat('Y-m-d', $row['ArrivalDate']));
				if ($row["PickupDate"]) {
					$good->additional('pickupDate', \DateTime::createFromFormat('Y-m-d H:i:s', $row['PickupDate']));
				}
				if ($row["DeliveryDate"]) {
					$good->additional('deliveryDate', \DateTime::createFromFormat('Y-m-d H:i:s', $row['DeliveryDate']));
				}
				if ($row["ShowcaseRetrieveDate"]) {
					$good->additional('showcaseRetrieveDate', \DateTime::createFromFormat('Y-m-d H:i:s', $row['ShowcaseRetrieveDate']));
				}
			//}

			$good->isPackage(1 == $row['IsPackage']);
			$good->setCategoryName($row['CategoryName']);
			$good->setBrandName($row['BrandName']);

			if ($row['PlaceRow'] || $row['PlaceCell'] || $row['PlaceRack']) {
				$places[$row['GoodID']] = [
					'row' => $row['PlaceRow'],
					'cell' => $row['PlaceCell'],
					'rack' => $row['PlaceRack']
				];
			}
			if (isset($places[$row['GoodID']])) {
				$good->setShowRoomData($places[$row['GoodID']]);
			}

			$goods[$good->getId()] = $good;
		}

		if (0 < sizeof($goods)) {
			$warehouses = $this->goodsMapper->enumWarehouseQuantityByGoodIds(array_keys($goods));
			foreach ($warehouses as $row) {
				$goods[$row['GoodID']]->addWarehouse($row['WarehouseID'], $row['AvailQuantity'], $row['SuborderQuantity']);
			}
		}

		return $goods;
	}

	/**
	 * Sorting methods
	 */
	public function addSortingColumn($uid, $column) {
		$this->sortingColumns[$uid] = $column;
	}

	public function addSortingDirection($direction) {
		$this->sortingDirections[] = $direction;
	}

	public function getSortingColumn($uid) {
		if (array_key_exists($uid, $this->sortingColumns)) {
			return $this->sortingColumns[$uid];
		}
		return null;
	}

	public function getSortingColumnUidByColumnName($columnName) {
		return array_search($columnName, $this->sortingColumns);
	}

	public function getSortingDirection($direction) {
		if (in_array($direction, $this->sortingDirections)) {
			return strtolower($direction);
		}
		return null;
	}

	public function enumAvailabilityByGoodIds($goodIds, $cityId = null) {
		if (0 == sizeof($goodIds)) {
			return [];
		}
		$rows = $this->goodsMapper->enumAvailabilityByGoodIds($goodIds, $cityId);
		$goods = [];
		foreach ($rows as $row) {
			$goods[$row->GoodID] = $row;
		}
		return $goods;
	}

	public function enumWarehouseQuantityByGoodId($goodId) {
		$rows = $this->goodsMapper->enumWarehouseQuantityByGoodId($goodId);
		$warehouses = [];
		foreach ($rows as $row) {
			$warehouses[$row->WarehouseID] = $row;
		}
		return $warehouses;
	}

	public function enumWarehouseAvailabilityByGoodId($goodId) {
		$rows = $this->goodsMapper->enumWarehouseAvailabilityByGoodId($goodId);
		$warehouses = [];
		foreach ($rows as $row) {
			$warehouses[$row->WarehouseID] = $row;
		}
		return $warehouses;
	}

	public function enumWarehouseAvailabilityByGoodIds($goodIds) {
		if (0 == sizeof($goodIds)) {
			return [];
		}
		$rows = $this->goodsMapper->enumWarehouseAvailabilityByGoodIds($goodIds);
		$goods = [];
		foreach ($rows as $row) {
			$goods[$row->GoodID . '_' . $row->WarehouseID] = $row;
		}
		return $goods;
	}

	public function enumNewGoods($cityId, $limit = 3) {
		return $this->goodsMapper->enumNewGoods($cityId, $limit);
	}

	public function enumActionGoods($cityId, $limit = 3) {
		return $this->goodsMapper->enumActionGoods($cityId, $limit);
	}

	public function enumGoodsByIds($goodIds) {
		if (0 == sizeof($goodIds))
			return [];
		else {
			$rows = $this->goodsMapper->enumGoodsByIds($goodIds);
			$goods = [];
			foreach ($rows as $row) {
				$goods[$row->GoodID] = $row;
			}
			return $goods;
		}
	}

	public function getAvailGoodByGoodId($goodId, $cityId) {
		return $this->goodsMapper->getAvailGoodByGoodId($goodId, $cityId);
	}

	public function getGoodByGoodId($goodId) {
		return $this->goodsMapper->getGoodByGoodId($goodId);
	}

	//(пока только 6 - г. Москва, МКАД 14-й километр )
    public function getSupplierRemainsByGoodId($goodId, $officeId = 6) {
        return $this->goodsMapper->getSupplierRemainsByGoodId($goodId, $officeId);
    }

	public function getAvailGoodWithCategoryUIDByGoodId($goodId, $cityId) {
		return $this->goodsMapper->getAvailGoodWithCategoryUIDByGoodId($goodId, $cityId);
	}

	public function getGoodWithCategoryUIDByGoodId($goodId, $cityId) {
		return $this->goodsMapper->getGoodWithCategoryUIDByGoodId($goodId, $cityId);
	}

	/*
	 * @param integer $goodId @param integer $cityId @return AvailCollection
	 */
	public function getTotalAvailGoodByOffices($goodId, $cityId) {
		$availCollection = new AvailCollection();
		$availsData = $this->goodsMapper->getTotalAvailGoodByOffices($goodId, $cityId);
		foreach ($availsData as $availData) {
			$availCollection->add($this->createAvail($availData));
		}
		return $availCollection;
	}

	/*
	 * @param array $goodIds @param integer $cityId @return AvailCollection
	 */
	public function getAvailGoodsByOffices($goodIds, $cityId) {
		$availCollection = new AvailCollection();
		$availsData = $this->goodsMapper->getAvailByOffices($goodIds, $cityId);
		$subordersData = $this->goodsMapper->getSuborderByOffices($goodIds, $cityId);
		foreach ($availsData as $availData) {
			$avail = $this->createAvail($availData);
			$avail->setCityId($cityId);
			if (!is_null($availData['ArrivalDate'])) {
				$avail->setArrivalDate(\DateTime::createFromFormat('Y-m-d H:i:s', $availData['ArrivalDate']));
			}
			$availCollection->add($avail, $avail->getGoodId() . '-' . $avail->getOfficeId());
		}
		foreach ($subordersData as $suborderData) {
			if ($availCollection->keyExists($suborderData['GoodID'] . '-' . $suborderData['OfficeID'])) {
				$avail = $availCollection->offsetGet($suborderData['GoodID'] . '-' . $suborderData['OfficeID']);
				$avail->setSuborder((int)$suborderData['SuborderQuantity']);
				if (!is_null($suborderData['ArrivalDate'])) {
					$avail->setArrivalDate(\DateTime::createFromFormat('Y-m-d H:i:s', $suborderData['ArrivalDate']));
				}
				$avail->isTwin((bool)$suborderData['IsTwin']);
			} else {
				$avail = $this->createAvail($suborderData);
				$avail->setCityId($cityId);
				$availCollection->add($avail, $avail->getGoodId() . '-' . $avail->getOfficeId());
			}
		}
		return $availCollection;
	}

	/*
	 * @param array $availData @return Avail
	 */
	private function createAvail($availData) {
		$avail = new Avail();
		$avail->setGoodId((int)$availData['GoodID'])->setOfficeId($availData['OfficeID'])->setOfficeAddress($availData['OfficeAddress'])->setOfficeShortName($availData['OfficeShortName']);
		if (array_key_exists('OfficePhone', $availData)) {
			$avail->setOfficePhone($availData['OfficePhone']);
		}
		if (array_key_exists('AvailQuantity', $availData)) {
			$avail->setAvail($availData['AvailQuantity']);
		}
		if (array_key_exists('StoreID', $availData)) {
			$avail->setStoreId($availData['StoreID']);
		}
		if (isset($availData['ArrivalDate']) && !is_null($availData['ArrivalDate'])) {
			$avail->setArrivalDate(\DateTime::createFromFormat('Y-m-d H:i:s', $availData['ArrivalDate']));
		}
		if (isset($availData['SuborderQuantity']) && !is_null($availData['SuborderQuantity'])) {
			$avail->addSuborder($availData['SuborderQuantity']);
		}
		if (isset($availData['MainWarehouseSuborderQuantity']) && !is_null($availData['MainWarehouseSuborderQuantity'])) {
			$avail->addSuborder($availData['MainWarehouseSuborderQuantity']);
		}
		if (isset($availData['OfficeLocationId']) && !is_null($availData['OfficeLocationId'])) {
			$avail->setCityId($availData['OfficeLocationId']);
		}
		if (isset($availData['IsTwin']) && !is_null($availData['IsTwin'])) {
			$avail->isTwin((bool)$availData['IsTwin']);
		}
		return $avail;
	}

	/*
	 * @param integer $goodId @param integer $cityId @return mixed
	 */
	public function getAvailabilityByGoodId($goodId, $cityId) {
		return $this->goodsMapper->getAvailabilityByGoodId($goodId, $cityId);
	}

	/*
	 * @param array $goodIds @param integer $cityId @return array
	 */
	public function getAvailabilityByGoodIds($goodIds, $cityId) {
		$dataAvails = $this->goodsMapper->getAvailabilityByGoodIds($goodIds, $cityId);
		$avail = [];
		foreach ($dataAvails as $dataAvail) {
			$dataAvail['TotalQuantity'] = (int)$dataAvail['AvailQuantity'] + (int)$dataAvail['SuborderQuantity'];
			$avail[$dataAvail['GoodID']] = $dataAvail;
		}
		return $avail;
	}

	public function getImagesCount($goodId) {
		return intval($this->goodsMapper->getImagesCount($goodId));
	}

	public function getGoodDescription($goodId) {
		$descr = [];
		$props = $this->goodsMapper->getGoodDescription($goodId);

		$minSortIndex = PHP_INT_MAX;
		foreach ($props as $prop) {
			$minSortIndex = min($minSortIndex, $prop['GroupSortIndex']);
		}

		foreach ($props as $prop) {
			$propId = $prop['PropertyID'];
			$valueId = $prop['PropertyValueID'];
			$name = $prop['PropertyName'];
			$value = $prop['Value'];
			$unit = $prop['Unit'];
			$kindId = $prop['KindID'];
			$typeId = $prop['PropertyTypeID'];
			$hint = $prop['Hint'];
			$hintImage = $prop['HintImage'];
			$displayInSmallDescription = $prop['DisplayInSmallDescription'];
			$displayInMiddleDescription = $prop['DisplayInMiddleDescription'];
			$isComparable = $prop['IsComparable'];
			$isSelectable = $prop['IsSelectable'];
			if ($minSortIndex == $prop['GroupSortIndex']) {
				$displayInMiddleDescription = true;
			}
			if (false !== mb_stripos($name, 'гарантия')) {
				$displayInSmallDescription = 1;
			}
			if (isset($_GET['debug_selectable']) && (393 == $propId)) {
				$displayInSmallDescription = 1;
			}
			if (isset($_GET['debug_selectable']) && (393 == $propId)) {
				$isSelectable = 1;
			}

			if (null === $value) {
				continue;
				$value = 'Нет';
			}
			$value = trim(nl2br($value . ' ' . $unit));

			if (!is_null($value) && (2 !== $valueId) && !in_array($value, [
				'Нет',
				'нет'
			])) {
				$descr[$propId] = (object)[
					'name' => $name,
					'value' => $value,
					'propId' => $propId,
					'valueId' => $valueId,
					'kindId' => $kindId,
					'typeId' => $typeId,
					'hint' => $hint,
					'hintImage' => $hintImage,
					'displayInSmallDescription' => $displayInSmallDescription,
					'displayInMiddleDescription' => $displayInMiddleDescription,
					'isComparable' => $isComparable,
					'isSelectable' => $isSelectable
				];
			}
		}
		return $descr;
	}

	public function enumGoodDescriptionsByGoodIds($goodIds) {
		$descr['propertiesList'] = [];
		$descr['goods'] = [];
		$props = $this->goodsMapper->enumGoodDescriptionsByGoodIds($goodIds);

		foreach ($props as $prop) {
			$name = $prop['PropertyName'];
			$value = $prop['value'];
			if (!isset($descr['propertiesList'][$name])) {
				$descr['propertiesList'][$name] = array(
					'same' => $value,
					'goods' => 1
				);
			} elseif ($descr['propertiesList'][$name]['same'] === $value) {
				$descr['propertiesList'][$name]['goods']++;
			}
			if (null === $value) {
				$value = 'Нет';
			}
			$value = trim(nl2br($value));
			if (false !== mb_stripos($name, 'сайт')) {
				$value = '<a href="//' . $value . '/" target="_blank" ref="nofollow">' . $value . '</a>';
			}
			$descr['goods'][$prop['GoodID']][$name] = $value;
			// $descr[] = ['name' => $name, 'value' => $value];
		}
		return $descr;
	}

	public function enumAvailGoodIdsByGoodIds($goodIds, $softCategoryId = null) {
		if (0 == sizeof($goodIds)) {
			return [];
		}
		$rows = $this->goodsMapper->enumAvailGoodIdsByGoodIds($goodIds, $softCategoryId);
		return ArrayHelper::enumOneColumn($rows, 'GoodID');
	}

	public function enumGoodsAvailQuantitiesByWarehouses($goodIds, $warehouseIds) {
		$rows = $this->goodsMapper->enumWarehouseAvailabilityByGoodIdsAndWarehouseIds($goodIds, $warehouseIds);
		return $rows;
	}

	public function getStats($goodId) {
		return $this->goodsMapper->getStats($goodId);
	}

	public function getAvailQuantitiesByGoods($warehouseIds, $goodIds) {
		return $this->goodsMapper->selectByWarehouseIdsAndGoodIds($warehouseIds, $goodIds);
	}

	public function getDeliveryQuantitiesByGoods($goodIds) {
		return $this->goodsMapper->selectDeliveryQuantities($goodIds);
	}

	/**
	 *
	 * @param integer $goodId
	 * @param
	 *        	integer cityId
	 * @return array
	 */
	public function getDiscountGoods($goodId, $cityId) {
		$goods = $this->goodsMapper->getDiscountGoods($goodId, $cityId);
		$result = [];
		foreach ($goods as $good) {
			$result[$good['GoodID']] = new \ArrayObject($good);
		}
		return $result;
	}

	/**
	 *
	 * @param integer $goodId
	 * @param
	 *        	integer cityId
	 * @return array
	 */
	public function getOriginalGood($goodId, $cityId) {
		return $this->goodsMapper->getOriginalGood($goodId, $cityId);
	}

	/**
	 *
	 * @param integer $cityId
	 * @return array
	 */
	public function getDiscountGoodsByCityId($cityId) {
		if (is_null($cityId)) {
			return [];
		}
		return $this->goodsMapper->getDiscountGoodsByCityId($cityId);
	}

	/**
	 *
	 * @param array $goodIds
	 * @param integer $categoryId
	 * @return array
	 */
	public function getGoodIdsInSoftCategoryId($goodIds, $categoryId) {
		if (!is_array($goodIds) || count($goodIds) == 0) {
			return [];
		}
		$checkedGoodIds = $this->goodsMapper->getGoodIdsInSoftCategoryId($goodIds, $categoryId);
		if (count($checkedGoodIds) > 0) {
			return ArrayHelper::enumOneColumn($checkedGoodIds, 'GoodID');
		}
		return [];
	}

	/**
	 *
	 * @param integer $cityId
	 * @return integer
	 */
	public function getRandomAvailGood($cityId) {
		if (!is_integer($cityId)) {
			throw new \InvalidArgumentException('Good id must be integer');
		}
		return $this->goodsMapper->getRandomAvailGood($cityId);
	}

	/**
	 *
	 * @param integer $cityId
	 * @return integer
	 */
	public function getRandomSuborderGood($cityId) {
		if (!is_integer($cityId)) {
			throw new \InvalidArgumentException('Good id must be integer');
		}
		return $this->goodsMapper->getRandomSuborderGood($cityId);
	}

	/**
	 *
	 * @param
	 *        	$goodId
	 * @param
	 *        	$cityId
	 * @param
	 *        	$priceCategoryId
	 * @param int $limit
	 * @return \Zend\Cache\Storage\mixed
	 */
	public function enumSimilarGoods($goodId, $cityId, $priceCategoryId, $limit = 12) {
		if (!is_integer($goodId)) {
			throw new \InvalidArgumentException('Good id must be integer');
		}
		if (!is_integer($limit)) {
			throw new \InvalidArgumentException('Limit must be integer');
		}

		$result = [];

		$rows = $this->goodsMapper->enumSimilarGoods($goodId, $cityId, $priceCategoryId, $limit);
		foreach ($rows as $row) {
			$result[$row['GoodID']] = $row;
		}

		return $result;
	}

	public function enumSimilarGoodIds($goodId, $cityId, $priceCategoryId, $limit = 12) {
		if (!is_integer($goodId)) {
			throw new \InvalidArgumentException('Good id must be integer');
		}
		if (!is_integer($limit)) {
			throw new \InvalidArgumentException('Limit must be integer');
		}

		$result = $this->goodsMapper->enumSimilarGoodIds($goodId, $cityId, $priceCategoryId, $limit);

		return $result;
	}

	public function enumNotSimilarGoods($goodId, $cityId, $priceCategoryId, $limit = 12) {
		if (!is_integer($goodId)) {
			throw new \InvalidArgumentException('Good id must be integer');
		}
		if (!is_integer($limit)) {
			throw new \InvalidArgumentException('Limit must be integer');
		}

		$result = [];

		$rows = $this->goodsMapper->enumNotSimilarGoods($goodId, $cityId, $priceCategoryId, $limit);
		foreach ($rows as $row) {
			$result[$row['GoodID']] = $row;
		}

		return $result;
	}

	public function enumNotSimilarGoodIds($goodId, $cityId, $priceCategoryId, $limit = 12) {
		if (!is_integer($goodId)) {
			throw new \InvalidArgumentException('Good id must be integer');
		}
		if (!is_integer($limit)) {
			throw new \InvalidArgumentException('Limit must be integer');
		}

		$result = $this->goodsMapper->enumNotSimilarGoodIds($goodId, $cityId, $priceCategoryId, $limit);

		return $result;
	}

	/**
	 *
	 * @param
	 *        	$goodId
	 * @return array
	 */
	public function getPropertiesValuesByGoodId($goodId) {
		$rows = $this->goodsMapper->getPropertiesValuesByGoodId($goodId);

		$result = [];
		foreach ($rows as $row) {
			$result[$row['PropertyID']][$row['PropertyValueID']] = $row['PropertyValue'];
		}

		return $result;
	}

	/**
	 *
	 * @param
	 *        	$goodId
	 * @return array
	 */
	public function getIdenticalGoodIds($goodId) {
		$result = $this->goodsMapper->getIdenticalGoodIds($goodId);
		$rows = $result->toArray();

		return ArrayHelper::enumOneColumn($rows, 'GoodID');
	}

	/**
	 *
	 * @param
	 *        	$goodId
	 * @param
	 *        	$goodProperties
	 * @return array
	 */
	public function getGoodSelectableProperties($goodId, $goodProperties) {
		$result = [];

		$identicalGoodIds = $this->getIdenticalGoodIds($goodId);

		if (empty($identicalGoodIds)) {
			return $result;
		}

		$goodPropsValues = [];
		foreach ($goodProperties as $property) {
			if ($property->typeId == 1 || !$property->isComparable) {
				// continue;
			}
			// print_r($property); exit();
			$goodPropsValues[$property->propId] = $property->valueId;
		}

		$propsValuesOfIdenticalGoods = [];
		foreach ($identicalGoodIds as $goodId) {
			$propertiesOfGoodsWithTheSameModel = $this->getGoodDescription($goodId);
			foreach ($propertiesOfGoodsWithTheSameModel as $property) {
				if ($property->typeId == 1 || !$property->isComparable) {
					// continue;
				}
				$propsValuesOfIdenticalGoods[$goodId][$property->propId] = $property->valueId;
			}
		}

		$propertiesValues = $this->getPropertiesValuesByGoodId($goodId);
		$propertiesIds = array_keys($goodPropsValues);

		foreach ($propertiesIds as $propertyId) {
			$goodPropValueId = $goodPropsValues[$propertyId];
			// Temporary remove property from list
			unset($goodPropsValues[$propertyId]);

			foreach ($propsValuesOfIdenticalGoods as $goodId => $propsValues) {
				$propertyValueId = $propsValues[$propertyId];
				unset($propsValues[$propertyId]);

				if ($goodPropsValues != $propsValues || $goodPropValueId == $propertyValueId) {
					continue;
				}

				$good = $this->getGoodByGoodId($goodId);
				$category = $this->categoriesMapper->getCategoryByHardCategoryId((int)$good->CategoryID);

				$propertyValue = [];
				$propertyValueIds = explode(',', $propertyValueId);

				foreach ($propertyValueIds as $id) {
					$propertyValue[] = $propertiesValues[$propertyId][$id];
				}

				$result[$propertyId][$propertyValueId] = [
					'value' => join(', ', $propertyValue),
					'url' => '/' . $category->CategoryUID . '/' . $good->GoodID . '_' . $good->GoodEngName . '/'
				];
			}

			// Push temporary removed property back to list
			$goodPropsValues[$propertyId] = $goodPropValueId;
		}

		return $result;
	}

	public function getGoodNotesByGoodId($goodId) {
		return $this->goodNotesMapper->getNotesByGoodId($goodId);
	}

	public function getPackageComponents($packageId, $cityId, $priceCategoryId) {
		$result = $this->goodsMapper->getPackageComponents($packageId, $cityId, $priceCategoryId);
		$rows = $result->toArray();

		$result = [];

		foreach ($rows as $row) {
			$result[$row['GroupID']][$row['GoodID']] = $row;
		}

		return $result;
	}

	public function getPackageGroups($packageId) {
		$result = $this->goodsMapper->getPackageGroups($packageId);
		$rows = $result->toArray();

		$result = [];
		foreach ($rows as $row) {
			$result[$row['GroupID']] = $row;
		}

		return $result;
	}

	public function getAnotherPackages($packageId, $cityId, $priceCategoryId) {
		$result = $this->goodsMapper->getAnotherPackages($packageId, $cityId, $priceCategoryId);

		$rows = $result->toArray();

		$result = [];

		foreach ($rows as $row) {
			$result[$row['GoodID']] = $row;
		}

		return $result;
	}

	public function getPackagesByComponentId($componentId, $cityId, $priceCategoryId) {
		return $this->goodsMapper->getPackagesByComponentId($componentId, $cityId, $priceCategoryId);
	}

	public function getPackageComponentsIds($packageId) {
		$result = $this->goodsMapper->getPackageComponentsIds($packageId);

		return ArrayHelper::enumOneColumn($result, 'GoodID');
	}

	public function getPackageDefaultComponentsIds($packageId) {
		$result = $this->goodsMapper->getPackageDefaultComponentsIds($packageId);

		return ArrayHelper::enumOneColumn($result, 'GoodID');
	}

	public function getAvailableGoodsByCategoryId($categoryId, $cityId, $priceCategoryId) {
		$result = [];
		$rows = $this->goodsMapper->getAvailableGoodsByCategoryId($categoryId, $cityId, $priceCategoryId);
		foreach ($rows as $row) {
			$result[$row['GoodID']] = $row;
		}

		return $result;
	}

	public function getGoodsRemainsByStoreId($goodIds, $storeId) {
		$result = [];
		$goodIds = array_filter($goodIds, function ($goodId) {
			return (0 < intval($goodId));
		});
		if (0 < count($goodIds)) {
			$rows = $this->goodsMapper->getGoodsRemainsByStoreId($goodIds, $storeId);
			foreach ($rows as $row) {
				$result[$row['GoodID']] = $row['Remains'];
			}
		}

		return $result;
	}

    public function getStoresRemainsByGoodId($goodId) {
        return $this->goodsMapper->getStoresRemainsByGoodId(intval($goodId));
    }

	/**
	 *
	 * @param array $productIds
	 * @return array
	 */
	public function enumSiteCategoryIds(array $productIds) {
		$result = [];

		if (0 < count($productIds)) {
			$queryGateway = new QueryGateway();
			$sql = "SELECT
					gtsc.GoodID,
					gtsc.SoftCategoryID,
					gtsc.GoodToSoftCategoryID
				FROM #goods_to_soft_categories# gtsc
				WHERE
					gtsc.GoodID IN (" . implode(',', $productIds) . ")
				GROUP BY
					gtsc.GoodID";
			$rows = $queryGateway->query($sql);
			foreach ($rows as $row) {
				$result[$row->GoodID] = $row->SoftCategoryID;
			}
		}

		return $result;
	}

	/**
	 *
	 * @param integer $productId
	 * @return string
	 */
	public function getBrandNameByProductId($productId) {
		$result = '';

		$queryGateway = new QueryGateway();
		$sql = "SELECT b.BrandName FROM #all_goods# ag INNER JOIN #brands# b ON ag.BrandID = b.BrandID WHERE ag.GoodID = {$productId}";
		$rows = $queryGateway->query($sql);
		if (0 < $rows->count()) {
			$row = $rows->current();
			$result = $row['BrandName'];
		}

		return $result;
	}

	/**
	 *
	 * @param integer $productId
	 * @return string
	 */
	public function getCategoryNameByProductId($productId) {
		$result = '';

		$queryGateway = new QueryGateway();
		$sql = "SELECT sc.CategoryName FROM #all_goods# ag INNER JOIN #goods_to_soft_categories# gtsc ON ag.GoodID = gtsc.GoodID AND gtsc.IsPrimary = 1 INNER JOIN #soft_categories# sc ON gtsc.SoftCategoryID = sc.SoftCategoryID WHERE ag.GoodID = {$productId}";
		$rows = $queryGateway->query($sql);
		if (0 < $rows->count()) {
			$row = $rows->current();
			$result = $row['CategoryName'];
		}

		return $result;
	}

	/**
	 *
	 * @param integer $productId
	 * @return \ArrayObject
	 */
	public function getGoodSupplierRemains($productId) {
		$result = null;

		$queryGateway = new QueryGateway();
		$sql = "SELECT * FROM #arrival_goods# WHERE GoodID = {$productId}";
		$rows = $queryGateway->query($sql);
		if (0 < $rows->count()) {
			$result = $rows->current();
		}

		return $result;
	}

}

?>