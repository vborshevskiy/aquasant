<?php

namespace SoloCatalog\Service;

use SoloCatalog\Data\CurrenciesTableInterface;
use SoloCatalog\Entity\Currencies\Currency;
use SoloCatalog\Entity\Currencies\CurrenciesCollection;

class CurrenciesService {

    /**
     *
     * @var CurrenciesTableInterface
     */
    private $currenciesTable;

    /**
     *
     * @param CurrenciesTableInterface $currenciesTable        	
     */
    public function __construct(CurrenciesTableInterface $currenciesTable) {
        $this->currenciesTable = $currenciesTable;
    }
    
    /**
     * 
     * @param array $currenciesIds
     * @return CurrenciesCollection
     */
    public function getCurrenciesByIds(array $currenciesIds) {
        if (empty($currenciesIds)) {
            return [];
        }
        $currenciesData = $this->currenciesTable->getCurrenciesByIds($currenciesIds);
        $currenciesCollection = new CurrenciesCollection();
        foreach ($currenciesData as $currencyData) {
            $currenciesCollection->add($this->createCurrency($currencyData));
        }
        return $currenciesCollection;
    }
    
    /**
     * 
     * @param ResultSet $currencyData
     * @return Currency
     */
    private function createCurrency($currencyData) {
        $currency = new Currency();
        $currency->setId((int)$currencyData['CurrencyID']);
        $currency->setName($currencyData['CurrencyName']);
        $currency->setRate((float)$currencyData['CurrencyRate']);
        $currency->isMainCurrency((bool)$currencyData['MainCurrency']);
        return $currency;
    }

}

?>