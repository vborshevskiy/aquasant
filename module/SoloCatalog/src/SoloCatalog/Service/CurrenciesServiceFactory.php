<?php

namespace SoloCatalog\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class CurrenciesServiceFactory extends AbstractServiceFactory {

	protected function create() {
		$service = new CurrenciesService($this->getServiceLocator()->get('SoloCatalog\\Data\\CurrenciesTable'));
		return $service;
	}

}

?>