<?php

namespace SoloCatalog\Service;

use Solo\Stdlib\ArrayHelper;
use SoloCatalog\Data\CompatibleMapperInterface;
use SoloCatalog\Entity\Compatibles\Category;
use SoloCatalog\Entity\Compatibles\CategoriesCollection;
use SoloCatalog\Entity\Compatibles\Good;

class CompatibleService {

    /**
     *
     * @var CompatibleMapperInterface
     */
    private $compatibleMapper;

    /**
     * 
     * @param CompatibleMapperInterface $compatibleMapper
     */
    public function __construct(CompatibleMapperInterface $compatibleMapper) {
        $this->compatibleMapper = $compatibleMapper;
    }

    /**
     *
     * @param integer $goodId
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @throws \InvalidArgumentException
     * @return array
     */
    public function enumCompatibleGoodIds($goodId, $cityId, $isPostDelivery = false) {
        if (!is_integer($goodId)) {
            throw new \InvalidArgumentException('Good id must be integer');
        }
        if (!is_integer($cityId)) {
            throw new \InvalidArgumentException('City id must be integer');
        }
        $rows = $this->compatibleMapper->enumCompatibleGoodIds($goodId, $cityId, $isPostDelivery);
        return ArrayHelper::enumOneColumn($rows, 'GoodID');
    }

    /**
     *
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @throws \InvalidArgumentException
     * @return array
     */
    public function enumCompatibleGoodIdsOnTextPages($cityId, $isPostDelivery = false) {
        if (!is_integer($cityId)) {
            throw new \InvalidArgumentException('City id must be integer');
        }
        $rows = $this->compatibleMapper->enumCompatibleGoodIdsOnTextPages($cityId, $isPostDelivery);
        return ArrayHelper::enumOneColumn($rows, 'GoodID');
    }
    
    /**
     *
     * @param array $goodIds
     * @param integer $cityId
     * @throws \InvalidArgumentException
     * @return array
     */
    public function enumCompatibleGoodIdsByGoodIds($goodIds, $cityId, $isPostDelivery = false) {
        if (!is_array($goodIds)) {
            throw new \InvalidArgumentException('Good ids must be array');
        }
        if (count($goodIds) == 0) {
            throw new \InvalidArgumentException('Good ids can\'t be empty');
        }
        if (!is_integer($cityId)) {
            throw new \InvalidArgumentException('City id must be integer');
        }
        $result = new \stdClass();
        $result->compotibleGoodIds = [];
        $result->compotibleGoods = [];
        $categoriesInfo = $this->compatibleMapper->enumCompatibleCategoriesByGoodIds($goodIds, $cityId, $isPostDelivery);
        $goodsInfo = $this->compatibleMapper->enumCompatibleGoodIdsByGoodIds($goodIds, $cityId, $isPostDelivery);
        $compotibleGoodIds = [];
        foreach ($goodIds as $goodId) {
            $categoriesCollection = new CategoriesCollection();
            if (array_key_exists($goodId, $categoriesInfo)) {
                foreach ($categoriesInfo[$goodId] as $categoryInfo) {
                    $category = new Category((int)$categoryInfo['categoryId']);
                    $category->setName($categoryInfo['categoryName']);
                    $categoriesCollection->add($category);
                }
            }
            if (array_key_exists($goodId, $goodsInfo)) {
                foreach ($goodsInfo[$goodId] as $goodInfo) {
                    $compotibleGoodIds[] = $goodInfo['compatibleGoodId'];
                    if ($categoriesCollection->offsetExists($goodInfo['categoryId'])) {
                        $compatibleGood = new Good($goodInfo['compatibleGoodId']);
                        $categoriesCollection->offsetGet($goodInfo['categoryId'])->goods()->add($compatibleGood);
                    }
                }
            }
            $result->compotibleGoods[$goodId] = $categoriesCollection;
        }
        $result->compotibleGoodIds = array_unique($compotibleGoodIds);
        return $result;
    }

}