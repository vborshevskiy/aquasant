<?php

namespace SoloCatalog\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class SearchServiceFactory extends AbstractServiceFactory {

	protected function create() {            
		return new SearchService($this->getServiceLocator()->get('search_manager'), $this->getServiceLocator()->get('triggers'));
	}

}

?>