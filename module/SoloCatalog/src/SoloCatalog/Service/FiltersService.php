<?php

namespace SoloCatalog\Service;

use SoloCatalog\Entity\Filters\SelectedFilter\Brands;
use SoloCatalog\Entity\Filters\SelectedFilter\Warehouses;
use SoloCatalog\Entity\Filters\SelectedFilter\Features;
use SoloCatalog\Entity\Filters\SelectedFilter\Price;
use SoloCatalog\Entity\Filters\SelectedFilter\Limit;
use SoloCatalog\Entity\Filters\SelectedFilter\SelectedFilterSet;
use SoloCatalog\Entity\Filters\Filter;
use SoloCatalog\Entity\Filters\FilterSet;
use SoloCatalog\Entity\Filters\FilterValue;
use Solo\Stdlib\ArrayHelper;
use SoloCatalog\Service\Helper\DateHelper;
use SoloCatalog\Entity\Avail\Avail;
use SoloCatalog\Data\FiltersMapperInterface;
use SoloCatalog\Data\PropertiesTable;

class FiltersService {

    /**
     *
     * @var FiltersMapperInterface
     */
    private $filtersMapper;

    /**
     * 
     * @param FiltersMapperInterface $filtersMapper
     */
    public function __construct(FiltersMapperInterface $filtersMapper) {
        $this->filtersMapper = $filtersMapper;
    }

    /**
     *
     * @param FilterSet $set
     * @param integer $categoryId
     * @param boolean $isPostDelivery
     * @param array $priceRange
     * @throws \InvalidArgumentException
     */
    public function fillFeatureFiltersByCategoryId(FilterSet $set, $categoryId, $cityId, $isPostDelivery = false, array $priceRange = null, $zoneId = null, $priceCategoryId = null, array $setIds = []) {
        if (!is_integer($categoryId)) {
            throw new \InvalidArgumentException('Category id must be integer');
        }
        $rows = $this->filtersMapper->enumFeaturesByCategoryId($categoryId, $cityId, $isPostDelivery, $priceRange, $zoneId, $priceCategoryId, $setIds);

        $this->fillFeatureFilters($set, $rows, $categoryId);
    }

    /**
     * 
     * @param FilterSet $set
     * @param array $goodIds
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @param array $priceRange
     */
    public function fillFeatureFiltersByGoodIds(FilterSet $set, array $goodIds, $cityId, $isPostDelivery = false, array $priceRange = null) {
        if (0 < sizeof($goodIds)) {
            $rows = $this->filtersMapper->enumFeaturesByGoodIds($goodIds, $cityId, $isPostDelivery, $priceRange);
            $this->fillFeatureFilters($set, $rows);
        }
    }

    /**
     * 
     * @param FilterSet $set
     * @param integer $categoryId
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @param array $priceRange
     * @throws \InvalidArgumentException
     */
    public function fillWarehousesFiltersByCategoryId(FilterSet $set, $categoryId, $cityId, array $priceRange = null) {
        if (!is_integer($categoryId)) {
            throw new \InvalidArgumentException('Category id must be integer');
        }
        $rowsOrder = $this->filtersMapper->enumOrderGoodsByCategoryId($categoryId, $cityId, $priceRange);
        $rows = $this->filtersMapper->enumWarehousesByCategoryId($categoryId, $cityId, $priceRange);
        $this->fillWarehousesFilters($set, $rows, $rowsOrder);
    }

    /**
     * 
     * @param FilterSet $set
     * @param integer $categoryId
     * @param integer $cityId
     * @param array $priceRange
     * @throws \InvalidArgumentException
     */
    public function fillWarehousesFiltersDeliveryOnlyByCategoryId(FilterSet $set, $categoryId, $cityId, array $priceRange = null) {
        if (!is_integer($categoryId)) {
            throw new \InvalidArgumentException('Category id must be integer');
        }
        $rows = $this->filtersMapper->enumGoodsDeliveryDateByCategoryId($categoryId, $cityId, $priceRange);
        $this->fillWarehousesFiltersDeliveryOnly($set, $rows);
    }

    /**
     * 
     * @param FilterSet $set
     * @param array $goodIds
     * @param integer $cityId
     * @param array $priceRange
     */
    public function fillWarehousesFiltersDeliveryOnlyByGoodIds(FilterSet $set, array $goodIds, $cityId, array $priceRange = null) {
        if (0 < sizeof($goodIds)) {
            $rows = $this->filtersMapper->enumGoodsDeliveryDateByGoodIds($goodIds, $cityId, $priceRange);
            $this->fillWarehousesFiltersDeliveryOnly($set, $rows);
        }
    }

    /**
     * 
     * @param FilterSet $set
     * @param integer $categoryId
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @param array $priceRange
     * @throws \InvalidArgumentException
     */
    public function fillBrandsFiltersByCategoryId(FilterSet $set, $categoryId, $cityId, $isPostDelivery = false, array $priceRange = null, $zoneId = null, $priceCategoryId = null, array $setIds = []) {
        if (!is_integer($categoryId)) {
            throw new \InvalidArgumentException('Category id must be integer');
        }
        $rows = $this->filtersMapper->enumBrandsByCategoryId($categoryId, $cityId, $isPostDelivery, $priceRange, $zoneId, $priceCategoryId, $setIds);

        $this->fillBrandsFilters($set, $rows);
    }

    /**
     *
     * @param FilterSet $set        	
     * @param array $goodIds        	
     * @param array $priceRange        	
     * @throws \InvalidArgumentException
     */
    public function fillWarehousesFiltersByGoodIds(FilterSet $set, array $goodIds, $cityId, array $priceRange = null) {
        if (0 < sizeof($goodIds)) {
            $rows = $this->filtersMapper->enumWarehousesByGoodIds($goodIds, $cityId, $priceRange);
            $rowsOrder = $this->filtersMapper->enumOrderGoodsByGoodIds($goodIds, $cityId, $priceRange);
            $this->fillWarehousesFilters($set, $rows, $rowsOrder);
        }
    }

    /**
     * 
     * @param FilterSet $set
     * @param array $goodIds
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @param array $priceRange
     */
    public function fillBrandsFiltersByGoodIds(FilterSet $set, array $goodIds, $cityId, $isPostDelivery = false, array $priceRange = null) {
        if (0 < sizeof($goodIds)) {
            $rows = $this->filtersMapper->enumBrandsByGoodIds($goodIds, $cityId, $isPostDelivery, $priceRange);
            $this->fillBrandsFilters($set, $rows);
        }
    }

    /**
     * 
     * @param array $goodIds
     * @param integer $priceCategory
     * @param integer $zoneId
     * @param boolean $isPostDelivery
     * @return array | null
     */
    public function getPriceRange(array $goodIds, $priceCategory, $zoneId, $isPostDelivery = false) {
        if (0 < sizeof($goodIds)) {
            $result = $this->filtersMapper->getPriceRangeForGoodIds($goodIds, $priceCategory, $zoneId, $isPostDelivery);
            if (null !== $result) {
                return [
                    'minPrice' => $result['minPrice'],
                    'maxPrice' => $result['maxPrice']
                ];
            }
        }
        return null;
    }

    /**
     * 
     * @param integer $categoryId
     * @param integer $cityId
     * @param integer $priceCategory
     * @param integer $zoneId
     * @param boolean $isPostDelivery
     * @return type
     * @throws \InvalidArgumentException
     */
    public function getPriceRangeForCategory($categoryId, $cityId, $priceCategory, $zoneId, $isPostDelivery = false) {
        if (!is_integer($categoryId)) {
            throw new \InvalidArgumentException('Category id must be integer');
        }
        $result = $this->filtersMapper->getPriceRangeForCategory($categoryId, $cityId, $priceCategory, $zoneId, $isPostDelivery);
        if (null !== $result) {
            return [
                'minPrice' => $result['minPrice'],
                'maxPrice' => $result['maxPrice']
            ];
        }
        return null;
    }

    /**
     *
     * @param \Zend\Mvc\Controller\Plugin\Params $params        	
     * @throws \InvalidArgumentException
     * @return \SoloCatalog\Entity\Filters\SelectedFilter\SelectedFilterSet
     */
    public function extractFiltersFromQuery($params) {
        $set = new SelectedFilterSet();
        $query = $_GET;
        $page = $params->fromRoute('page');
        $brandId = $params->fromRoute('brandId');
        if (!is_null($page) && $page > 0) {
            $query['page'] = $page;
        }
        if (!is_null($brandId) && $brandId > 0) {
            $query['brands'] = $brandId;
        }

        if (0 < sizeof($query)) {
            $set->add('brands', Brands::createFromQuery($query));
            $set->add('warehouses', Warehouses::createFromQuery($query));
            $set->add('features', Features::createFromQuery($query));
            $set->add('price', Price::createFromQuery($query));
            $set->add('limit', Limit::createFromQuery($query));
        }
        if ($set->has('brands')) {
            $brandsFilter = $set->get('brands');
            $brands = $this->filtersMapper->enumBrandsInfo($set->get('brands')->enumIds());
            foreach ($brands as $brand) {
                $brandsFilter->addUid($brand['ID'], $brand['BrandUID']);
            }
        }
        return $set;
    }

    /**
     *
     * @param integer $categoryId        	
     * @throws \InvalidArgumentException
     * @return boolean
     */
    public function hasOnlyOnePropertyTemplateInCategory($categoryId) {
        if (!is_integer($categoryId)) {
            throw new \InvalidArgumentException('Category id must be integer');
        }
        $templateIds = $this->filtersMapper->enumPropertyTemplatesForCategory($categoryId);
        $uniqueTemplateIds = array_unique($templateIds, SORT_NUMERIC);
        return (1 == sizeof($uniqueTemplateIds));
    }

    /**
     *
     * @param array $goodIds        	
     * @return boolean
     */
    public function hasOnlyOnePropertyTemplateForGoods(array $goodIds) {
        if (0 == sizeof($goodIds)) {
            return false;
        }
        $templateIds = $this->filtersMapper->enumPropertyTemplatesForGoods($goodIds);
        $uniqueTemplateIds = array_unique($templateIds, SORT_NUMERIC);
        return (1 == sizeof($uniqueTemplateIds));
    }
    
    /**
     *
     * @param FilterSet $set        	
     * @param array $rows        	
     */
    private function fillFeatureFilters(FilterSet $set, array $rows, $categoryId = 0) {
        if (0 == sizeof($rows)) {
            return;
        }

        $propIds = array_unique(ArrayHelper::enumOneColumn($rows, 'AspectKey'));
        $valueIds = array_unique(ArrayHelper::enumOneColumn($rows, 'AspectValue'));

        $result = $this->filtersMapper->enumFeatureSettings($propIds, $valueIds, $categoryId);
        $settings = [];

        foreach ($result as $item) {
            if (!array_key_exists($item['pid'], $settings)) {
                $settings[$item['pid']] = array(
                    'id' => $item['pid'],
                    'title' => $item['name'],
                    'type' => $item['propType'],
                    'values' => []
                );
            }
            $value = trim($item['value']);
            if ('—' != $item['unit']) {
                $value .= ' ' . $item['unit'];
            }
            $settings[$item['pid']]['values'][$item['pvid']] = array(
                'id' => $item['pvid'],
                'title' => $value,
                'image' => $item['image']
            );
        }
        
        if (isset($_GET['debug_filter'])) {
        	print implode(',', $propIds); exit();
        }

        foreach ($settings as $setting) {
            $filter = new Filter($setting['id'], $setting['title']);

            if ($setting['unit'] != "") {
                $filter->setUnit($setting['unit']);
            }

            if ($setting['type'] == PropertiesTable::BOOLEAN_PEROPERTY_TYPE) {
                $filterValueYes = new FilterValue($setting['id'] . '-1', $setting['title']);
                $filter->addValue($filterValueYes);
            } elseif ($setting['type'] == PropertiesTable::NUMERIC_PROPERTY_TYPE) {
                $filter->setType($setting['type']);
            } else {
                foreach ($setting['values'] as $settingValue) {
                    $filterValue = new FilterValue($settingValue['id'], $settingValue['title'], null, $settingValue['image']);
                    $filter->addValue($filterValue);
                }
            }

            $set->addFilter($filter);
        }
        
        $minSortIndex = PHP_INT_MAX;
        foreach ($rows as $row) {
        	$minSortIndex = min($minSortIndex, $row['GroupSortIndex']);
        }

        foreach ($rows as $row) {
            $filter = $set->getFilter($row['AspectKey']);
            if ($filter) {
                $filter->setType($row['AspectType']);
                $filter->setMain($row['AspectKind'] == 4 ? 1 : 0);
                if ($row['GroupSortIndex'] == $minSortIndex) {
                	$filter->setMain(1);
                }
                $filter->setGroup($row['PropertyGroupID']);
                if ($filter->getType() == PropertiesTable::BOOLEAN_PEROPERTY_TYPE) {
                    $filterValueYes = $filter->getValue($row['AspectKey'] . '-1');
                    if (($row['AspectBool'] == 1)) {
                        $filterValueYes->addGood($set->addGood($row['GoodID']));
                    }
                } elseif ($filter->getType() == PropertiesTable::NUMERIC_PROPERTY_TYPE) {
                    if ($row['AspectNum'] > 0) {
                        $numericFilterValue = $filter->getValue($row['AspectNum']);
                        if (!$numericFilterValue) {
                            $numericFilterValue = new FilterValue();
                            $numericFilterValue->setId($row['AspectNum']);
                            $numericFilterValue->setTitle($row['AspectNum']);
                            $filter->addValue($numericFilterValue);
                        }
                        $numericFilterValue->addGood($set->addGood($row['GoodID']));
                    }
                } else {
                    $filterValue = $filter->getValue($row['AspectValue']);
                    if ($filterValue) {
                        $filterValue->addGood($set->addGood($row['GoodID']));
                    }
                }
            }
        }
    }

    /**
     * 
     * @param FilterSet $set
     * @param array $rows
     */
    private function fillWarehousesFiltersDeliveryOnly(FilterSet $set, array $rows) {
        $filterWarehouses = new Filter('warehouses', 'Доставка');
        foreach ($rows as $row) {
            $availDeliveryDate = \DateTime::createFromFormat('Y-m-d H:i:s', $row['AvailDeliveryDate']);
            $suborderDeliveryDate = \DateTime::createFromFormat('Y-m-d H:i:s', $row['SuborderDeliveryDate']);
            $minDeliveryDate = DateHelper::getMinDate([$availDeliveryDate,$suborderDeliveryDate]);
            if (DateHelper::isToday($minDeliveryDate)) {
                $deliveryKey = 0;
            } elseif (DateHelper::isTomorrow($minDeliveryDate)) {
                $deliveryKey = 1;
            } elseif (DateHelper::isDayAfterTomorrow ($minDeliveryDate)) {
                $deliveryKey = 2;
            } else {
                $deliveryKey = 3;
            }
            $orderFilterValue = $filterWarehouses->getValue('delivery' . $deliveryKey);
            if (null === $orderFilterValue) {
                $orderFilterValue = new FilterValue('delivery' . $deliveryKey, 'delivery' . $deliveryKey, Avail::getTextRangeDeliveryDate(\DateTime::createFromFormat('Y-m-d H:i:s', $row['AvailDeliveryDate'])));
                $filterWarehouses->addValue($orderFilterValue);
            }
            $orderFilterValue->addGood($set->addGood($row['GoodID']));
        }
        $set->addFilter($filterWarehouses);
    }

    /**
     *
     * @param FilterSet $set        	
     * @param array $rows        	
     */
    private function fillWarehousesFilters(FilterSet $set, array $rows, array $rowsOrder) {
        $filterWarehouses = new Filter('warehouses', 'Наличие в магазинах');
        foreach ($rows as $row) {
            $goodId = $row['GoodID'];
            $warehouseId = $row['WarehouseID'];
            $warehouseValueId = 'warehouse' . $warehouseId;
            $warehousesFilterValue = $filterWarehouses->getValue($warehouseValueId);
            if (null === $warehousesFilterValue) {
                $warehousesFilterValue = new FilterValue($warehouseValueId, $warehouseId);
                $filterWarehouses->addValue($warehousesFilterValue);
            }
            $warehousesFilterValue->addGood($set->addGood($goodId));
        }
        foreach ($rowsOrder as $row) {
            $arrivalKey = DateHelper::getCountDayToDate(\DateTime::createFromFormat('Y-m-d', $row['ArrivalDate']));
            $orderFilterValue = $filterWarehouses->getValue('order' . $arrivalKey);
            if (null === $orderFilterValue) {
                $orderFilterValue = new FilterValue('order' . $arrivalKey, 'order' . $arrivalKey, Avail::getTextRangeArrivalDate(\DateTime::createFromFormat('Y-m-d', $row['ArrivalDate'])));
                $filterWarehouses->addValue($orderFilterValue);
            }
            $orderFilterValue->addGood($set->addGood($row['GoodID']));
        }
        $set->addFilter($filterWarehouses);
    }

    /**
     *
     * @param FilterSet $set        	
     * @param array $rows        	
     */
    private function fillBrandsFilters(FilterSet $set, array $rows) {
        $filterBrands = new Filter('brands', 'Бренд');
        $brandIds = array_unique(ArrayHelper::enumOneColumn($rows, 'BrandID'));
        $brands = [];
        $brandsName = [];
        if ($brandIds[0] != null) {
            $brandsName = $this->filtersMapper->enumBrandsInfo($brandIds);
        }

        foreach ($brandsName as $brand) {
            $brands[$brand['ID']] = [
                'name' => $brand['BrandName'],
                'uid' => $brand['BrandUID'],
            ];
        }
        foreach ($rows as $row) {
            $goodId = $row['GoodID'];
            $brandId = $row['BrandID'];
            $brandValueName = $brands[$row['BrandID']]['name'];
            $brandValueUid = $brands[$row['BrandID']]['uid'];
            $brandValueId = 'brand' . $brandId;
            $brandsFilterValue = $filterBrands->getValue($brandValueId);
            if (null === $brandsFilterValue) {
                $brandsFilterValue = new FilterValue($brandValueId, $brandId, $brandValueName);
                $brandsFilterValue->additional('uid', $brandValueUid);
                $filterBrands->addValue($brandsFilterValue);
            }
            $brandsFilterValue->addGood($set->addGood($goodId));
        }
        $set->addFilter($filterBrands);
    }

    public function getFilterTypesByIds($propertyIds) {
        $rows = $this->filtersMapper->getFilterTypesByIds($propertyIds);

        $result = [];

        foreach ($rows as $row) {
            $result[$row['PropertyID']] = (int) $row['PropertyTypeID'];
        }

        return $result;
    }
}
