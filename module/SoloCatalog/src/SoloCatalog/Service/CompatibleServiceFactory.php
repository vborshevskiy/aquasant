<?php

namespace SoloCatalog\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class CompatibleServiceFactory extends AbstractServiceFactory {

	protected function create() {
		$service = new CompatibleService($this->getServiceLocator()->get('SoloCatalog\\Data\\CompatibleMapper'));
		return $service;
	}

}

?>