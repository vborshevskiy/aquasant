<?php

namespace SoloCatalog\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class MenuServiceFactory extends AbstractServiceFactory {

	protected function create() {
        $constants = $this->getServiceLocator()->get('constants');
        $menuMapper = $this->getServiceLocator()->get('SoloCatalog\\Data\\MenuMapper');
		$service = new MenuService($menuMapper,$constants);
		return $service;
	}

}

?>