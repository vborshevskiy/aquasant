<?php

namespace SoloCatalog\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class SetsServiceFactory extends AbstractServiceFactory {

	/**
	 *
	 * @see \Solo\ServiceManager\AbstractServiceFactory::create()
	 */
	protected function create() {
		$setsTable = $this->getServiceLocator()->get('SoloCatalog\Data\SetsTable');
		$groupsTable = $this->getServiceLocator()->get('SoloCatalog\Data\SetsGroupsTable');
		$setsToGoodsTable = $this->getServiceLocator()->get('SoloCatalog\Data\SetsToGoodsTable');
		$service = new SetsService($setsTable, $groupsTable, $setsToGoodsTable);
		return $service;
	}

}

?>