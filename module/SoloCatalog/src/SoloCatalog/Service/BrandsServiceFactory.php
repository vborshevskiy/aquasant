<?php

namespace SoloCatalog\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class BrandsServiceFactory extends AbstractServiceFactory {

	protected function create() {
		$service = new BrandsService($this->getServiceLocator()->get('SoloCatalog\\Data\\BrandsMapper'));
		return $service;
	}

}

?>