<?php

namespace SoloCatalog\Service;

use SoloCatalog\Entity\Categories\MainMenu;
use SoloCatalog\Entity\Categories\MenuItem;
use Solo\Stdlib\ArrayHelper;
use SoloCache\Service\ProvidesCache;
use SoloCatalog\Data\MenuMapperInterface;
use SoloSettings\Service\ConstantsService;

class MenuService {

    use ProvidesCache;

    const MENU_FILTER_KIND_ID = 'MenuFilterKindId';

    /**
     *
     * @var MenuMapperInterface
     */
    private $menuMapper;

    /**
     *
     * @var ConstantsService
     */
    private $constants;

    /**
     *
     * @param MenuMapperInterface $menuMapper
     * @param ConstantsService $constants
     */
    public function __construct(MenuMapperInterface $menuMapper, ConstantsService $constants) {
        $this->menuMapper = $menuMapper;
        $this->constants = $constants;
    }

    public function __call($name, $arguments) {
        if (!method_exists($this->menuMapper, $name)) {
            throw new \BadMethodCallException('Invalid catalog menu method: ' . $name);
        }
        return call_user_func_array(array(
            $this->menuMapper,
            $name
                ), $arguments);
    }

    /**
     *
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @param array $urlQuery
     * @return \SoloCatalog\Entity\Categories\MainMenu
     */
    public function getMenu($cityId, $isPostDelivery = false, array $urlQuery = []) {
        $items = $this->enumMenu($cityId, $isPostDelivery);
        return $this->createMainMenu($items, $urlQuery);
    }

    /**
     *
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @return \Zend\Cache\Storage\mixed | boolean | number | Ambigous
     */
    private function enumMenu($cityId, $isPostDelivery) {
        if ($this->cache()->has('menu-' . $cityId)) {
            //return $this->cache()->get('menu-' . $cityId);
        }
        $allCategories = $this->menuMapper->enumAllCategories();
        $availCategoryIds = $this->menuMapper->enumAllAvailCategoryIds($cityId, $isPostDelivery);
        $goodsCountByCategories = [];
        if (0 < count($availCategoryIds)) {
        	$goodsCountByCategories = $this->menuMapper->enumGoodsCountByCategoryIds($availCategoryIds, $cityId, $isPostDelivery);
        }
        $menuFilters = $this->menuMapper->enumMenuFilters($this->constants->get(self::MENU_FILTER_KIND_ID));

        $menu = array(
            'categories' => []
        );
        foreach ($allCategories as $category) {
            $avail = false;
            if (0 < $category['RealSoftCategoryID']) {
                $avail = in_array($category['RealSoftCategoryID'], $availCategoryIds);
            } else {
                $avail = in_array($category['SoftCategoryID'], $availCategoryIds);
            }
            if (!$avail)
                continue;
            $parents = ArrayHelper::findAll(
                            $allCategories, function ($value) use($category) {
                        return (($value['cLeft'] < $category['cLeft']) && ($value['cRight'] > $category['cRight']) && ($value['cLevel'] < $category['cLevel']));
                    });
            usort(
                    $parents, function ($a, $b) {
                if ($a['cLevel'] > $b['cLevel'])
                    return 1;
                if ($a['cLevel'] < $b['cLevel'])
                    return -1;
                return 0;
            });

            // make parents structure
            $prevParent = &$menu;
            foreach ($parents as $parent) {
                if (!isset($prevParent['categories'][$parent['SoftCategoryID']])) {
                    $prevParent['categories'][$parent['SoftCategoryID']] = array(
                        'SoftCategoryID' => $parent['SoftCategoryID'],
                        'CategoryName' => $parent['CategoryName'],
                        'Url' => '/' . $parent['CategoryUID'] . '/',
                        'SortIndex' => $parent['SortIndex'],
                        'cLevel' => $parent['cLevel'],
                        'goodsCount' => (array_key_exists($parent['SoftCategoryID'], $goodsCountByCategories) ? $goodsCountByCategories[$parent['SoftCategoryID']] : 0),
                        'filters' => (array_key_exists($parent['SoftCategoryID'], $menuFilters) ? $menuFilters[$parent['SoftCategoryID']] : []),
                        'categories' => []
                    );
                }
                $prevParent = &$prevParent['categories'][$parent['SoftCategoryID']];
            }

            $prevParent['categories'][$category['SoftCategoryID']] = array(
                'SoftCategoryID' => $category['SoftCategoryID'],
                'CategoryName' => $category['CategoryName'],
                'Url' => '/' . $category['CategoryUID'] . '/',
                'SortIndex' => $category['SortIndex'],
                'cLevel' => $category['cLevel'],
                'goodsCount' => (array_key_exists($category['SoftCategoryID'], $goodsCountByCategories) ? $goodsCountByCategories[$category['SoftCategoryID']] : 0),
                'filters' => (array_key_exists($category['SoftCategoryID'], $menuFilters) ? $menuFilters[$category['SoftCategoryID']] : []),
                'categories' => []
            );
        }
        $this->sortMenu($menu['categories']);

        $cacheTags = $this->cache()->prepareTags();
        $cacheTags->add($this->cache()->helper()->tagsFromMethod('enumAllCategories', $this->menuMapper));
        $cacheTags->add($this->cache()->helper()->tagsFromMethod('enumAllAvailCategoryIds', $this->menuMapper));
        $cacheTags->add($this->cache()->helper()->tagsFromMethod('enumGoodsCountByCategoryIds', $this->menuMapper));
        $cacheTags->add($this->cache()->helper()->tagsFromMethod('enumMenuFilters', $this->menuMapper));
        $this->cache()->set('menu-' . $cityId, $menu['categories'], $cacheTags);

        return $menu['categories'];
    }

    /**
     *
     * @param array $array        	
     * @return MainMenu
     */
    private function createMainMenu($array, $urlQuery = []) {
        $menu = new MainMenu();
        foreach ($array as $item) {
            $url = $item['Url'];
            if (0 < sizeof($urlQuery)) {
                $queryParams = [];
                foreach ($urlQuery as $name => $value) {
                    $queryParams[] = $name . '=' . $value;
                }
                $url .= '?' . implode('&', $queryParams);
            }

            $mi = new MenuItem();
            $mi->setId(intval($item['SoftCategoryID']));
            $mi->setName($item['CategoryName']);
            $mi->setUrl($url);
            $mi->addGoodsCount(intval($item['goodsCount']));
            $mi->addFilters($item['filters']);
            $mi->setLevel(intval($item['cLevel']));
            foreach ($item['categories'] as $subitem) {
                $child = $this->createMenuItem($subitem, $urlQuery);
                $mi->addItem($child);
                $mi->addGoodsCount($child->getGoodsCount());
                $mi->addFilters($child->getFilters());
            }
            $menu->addItem($mi);
        }
        return $menu;
    }

    private function createMenuItem($item, $urlQuery = []) {
        $url = $item['Url'];
        if (0 < sizeof($urlQuery)) {
            $queryParams = [];
            foreach ($urlQuery as $name => $value) {
                $queryParams[] = $name . '=' . $value;
            }
            $url .= '?' . implode('&', $queryParams);
        }

        $menu = new MenuItem();
        $menu->setId(intval($item['SoftCategoryID']));
        $menu->setName($item['CategoryName']);
        $menu->setLevel(intval($item['cLevel']));
        $menu->addGoodsCount(intval($item['goodsCount']));
        $menu->addFilters($item['filters']);
        $menu->setUrl($url);
        foreach ($item['categories'] as $subitem) {
            $child = $this->createMenuItem($subitem, $urlQuery);
            $menu->addGoodsCount($child->getGoodsCount());
            $menu->addFilters($child->getFilters());
            $menu->addItem($child);
        }
        return $menu;
    }

    private function sortMenu(&$categories) {
		usort(
			$categories, 
			function ($a, $b) {
				if ($a['SortIndex'] > $b['SortIndex']) return 1;
				if ($a['SortIndex'] < $b['SortIndex']) return -1;
				return 0;
			});
		foreach ($categories as &$category) {
			$this->sortMenu($category['categories']);
		}
	}

}

?>