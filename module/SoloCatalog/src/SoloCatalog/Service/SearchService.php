<?php

namespace SoloCatalog\Service;

use SoloSearch\Service\SearchManager;
use Solo\Search\Options;
use SoloSearch\Entity\AutocompleteResult;
use SoloSettings\Service\TriggersService;

class SearchService {

	/**
	 *
	 * @var SearchManager
	 */
	private $searchManager;

	/**
	 *
	 * @var Triggers
	 */
	private $triggers;

	/**
	 *
	 * @param SearchManager $searchManager        	
	 * @param Triggers $triggers        	
	 */
	public function __construct(SearchManager $searchManager, TriggersService $triggers) {
		$this->searchManager = $searchManager;
		$this->triggers = $triggers;
	}

	/**
	 *
	 * @param string $query        	
	 * @param integer $limit        	
	 * @throws \InvalidArgumentException
	 * @throws \OutOfRangeException
	 * @return array
	 */
	public function searchCategories($query, $limit = null) {
		/*
		 * if ((null !== $limit) && !is_integer($limit)) { throw new \InvalidArgumentException('Limit must be integer'); } if (0 >= $limit) { throw new \OutOfRangeException('Limit must be greater than zero'); }
		 */
		$locator = $this->searchManager->factoryLocator('categories');
		$options = new Options();
		if (null !== $limit) {
			$options->setLimitItemsPerPage($limit);
		}
		$locator->setName($locator->formatName($this->triggers->active('soft_categories')));
		return $locator->search($query, $options);
	}

	/**
	 *
	 * @param string $query        	
	 * @param integer $limit        	
	 * @throws \InvalidArgumentException
	 * @throws \OutOfRangeException
	 * @return array
	 */
	public function searchGoods($query, $limit = null) {
		/*
		 * if ((null !== $limit) && !is_integer($limit)) { throw new \InvalidArgumentException('Limit must be integer'); } if (0 >= $limit) { throw new \OutOfRangeException('Limit must be greater than zero'); }
		 */
		if (GOD_MODE) {
			$locator = $this->searchManager->factoryLocator('all_goods');
			$options = new Options();
			if (null !== $limit) {
				$options->setLimitItemsPerPage($limit);
			}
			$locator->setName($locator->formatName($this->triggers->active('all_goods')));
		} elseif (MANAGER_MODE) {
			$locator = $this->searchManager->factoryLocator('manager_goods');
			$options = new Options();
			if (null !== $limit) {
				$options->setLimitItemsPerPage($limit);
			}
			$locator->setName($locator->formatName($this->triggers->active('all_goods'), $this->triggers->active('avail_manager_goods')));
		} else {
			$locator = $this->searchManager->factoryLocator('goods');
			$options = new Options();
			if (null !== $limit) {
				$options->setLimitItemsPerPage($limit);
			}
			$locator->setName($locator->formatName($this->triggers->active('all_goods'), $this->triggers->active('avail_goods')));
		}
		return $locator->search($query, $options);
	}

	/**
	 *
	 * @return AutocompleteResult
	 */
	public function createAutocompleteResult($name) {
		return new AutocompleteResult($name);
	}

}

?>