<?php

namespace SoloCatalog\Service;

use SoloCatalog\Data\SatelliteGroupsTable;
use SoloCatalog\Data\SatelliteGoodsTable;
use SoloCatalog\Data\SatelliteGoodsMapper;

class SatelliteService {

	/**
	 *
	 * @var SatelliteGroupsTable
	 */
	private $groupsTable;

	/**
	 *
	 * @var SatelliteGoodsTable
	 */
	private $goodsTable;

	/**
	 *
	 * @var SatelliteGoodsMapper
	 */
	private $goodsMapper;

	/**
	 *
	 * @param SatelliteGroupsTable $groupsTable        	
	 * @param SatelliteGoodsTable $goodsTable        	
	 * @param SatelliteGoodsMapper $goodsMapper        	
	 */
	public function __construct(SatelliteGroupsTable $groupsTable, SatelliteGoodsTable $goodsTable, SatelliteGoodsMapper $goodsMapper) {
		$this->groupsTable = $groupsTable;
		$this->goodsTable = $goodsTable;
		$this->goodsMapper = $goodsMapper;
	}
	
	/**
	 * 
	 * @param integer $goodId
	 * @param integer $cityId
	 * @param integer $priceCategoryId
	 * @return \Zend\Db\ResultSet\ResultSet
	 */
	public function enumSatelliteGoods($goodId, $cityId, $priceCategoryId) {
		return $this->goodsMapper->enumSatelliteGoods($goodId, $cityId, $priceCategoryId);
	}

}

?>