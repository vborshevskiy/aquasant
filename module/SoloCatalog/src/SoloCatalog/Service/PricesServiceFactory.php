<?php

namespace SoloCatalog\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class PricesServiceFactory extends AbstractServiceFactory {

	protected function create() {
		$service = new PricesService($this->getServiceLocator()->get('SoloCatalog\\Data\\PricesMapper'), $this->getServiceLocator()->get('SoloCatalog\\Service\\Helper\PricesHelper'), $this->getServiceLocator()->get('SoloCatalog\\Service\\Helper\DateHelper'));
		return $service;
	}

}

?>