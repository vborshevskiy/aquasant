<?php

namespace SoloCatalog\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class FilesServiceFactory extends AbstractServiceFactory {

	protected function create() {
		$service = new FilesService($this->getServiceLocator()->get('SoloCatalog\\Data\\FilesMapper'));
		return $service;
	}

}

?>