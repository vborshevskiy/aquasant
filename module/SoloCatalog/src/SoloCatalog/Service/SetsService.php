<?php

namespace SoloCatalog\Service;

use SoloCatalog\Data\SetsTableInterface;
use SoloCatalog\Data\SetsGroupsTableInterface;
use SoloCatalog\Data\SetsToGoodsTableInterface;
use Solo\Db\QueryGateway\QueryGateway;
class SetsService {

	/**
	 * 
	 * @var SetsTableInterface
	 */
	private $setsTable;
	
	/**
	 * 
	 * @var SetsGroupsTableInterface
	 */
	private $groupsTable;
	
	/**
	 * 
	 * @var SetsToGoodsTableInterface
	 */
	private $setsToGoodsTable;
	
	/**
	 * 
	 * @param SetsTableInterface $setsTable
	 * @param SetsGroupsTableInterface $groupsTable
	 * @param SetsToGoodsTableInterface $setsToGoodsTable
	 */
	public function __construct(SetsTableInterface $setsTable, SetsGroupsTableInterface $groupsTable, SetsToGoodsTableInterface $setsToGoodsTable) {
		$this->setsTable = $setsTable;
		$this->groupsTable = $groupsTable;
		$this->setsToGoodsTable = $setsToGoodsTable;
	}
	
	/**
	 * 
	 * @param string $uid
	 * @param integer $categoryId
	 * @return ArrayObject
	 */
	public function getSetByUid($uid, $categoryId) {
		$result = null;
		
		$queryGateway = new QueryGateway();
		$sql = "SELECT DISTINCT
					s.*
				FROM #sets# s
					INNER JOIN #sets_groups# sg
						ON s.SetGroupID = sg.SetGroupID
						AND sg.SoftCategoryID = {$categoryId}
				WHERE
					s.SetUrlPart = '{$uid}'
				LIMIT 1";
		$rows = $queryGateway->query($sql);
		if (0 < $rows->count()) {
			$result = $rows->current();
		}
		
		return $result;
	}
	
	/**
	 * 
	 * @param array $uids
	 * @param integer $categoryId
	 * @return array
	 */
	public function getSetsByUids(array $uids, $categoryId) {
		$result = null;
		
		$queryGateway = new QueryGateway();
		$sql = "SELECT DISTINCT
					s.*
				FROM #sets# s
					INNER JOIN #sets_groups# sg
						ON s.SetGroupID = sg.SetGroupID
						AND sg.SoftCategoryID = {$categoryId}
				WHERE
					s.SetUrlPart IN ('".implode("', '", $uids)."')
				ORDER BY
					sg.SortIndex ASC,
					s.SortIndex ASC";
		$rows = $queryGateway->query($sql);
		foreach ($rows as $row) {
			$result[$row['SetID']] = $row;
		}
		
		return $result;
	}
	
	/**
	 * 
	 * @param integer $categoryId
	 * @return \Zend\Db\ResultSet\ResultSet
	 */
	public function getCategorySets($categoryId) {
		$queryGateway = new QueryGateway();
		$sql = "SELECT DISTINCT
					s.*
				FROM #sets# s
					INNER JOIN #sets_groups# sg
						ON s.SetGroupID = sg.SetGroupID
						AND sg.SoftCategoryID = {$categoryId}
					INNER JOIN #sets_to_goods# stg
						ON s.SetID = stg.SetID
					INNER JOIN #avail_goods# ag
						ON stg.GoodID = ag.GoodID
				ORDER BY
					s.SortIndex ASC";
		return $queryGateway->query($sql)->toArray();
	}
	
	/**
	 * 
	 * @param integer $categoryId
	 * @return \Zend\Db\ResultSet\ResultSet
	 */
	public function getGroupsByCategoryId($categoryId) {
		$queryGateway = new QueryGateway();
		$sql = "SELECT DISTINCT
					sg.*
				FROM #sets# s
					INNER JOIN #sets_groups# sg
						ON s.SetGroupID = sg.SetGroupID
						AND sg.SoftCategoryID = {$categoryId}
					INNER JOIN #sets_to_goods# stg
						ON s.SetID = stg.SetID
					INNER JOIN #avail_goods# ag
						ON stg.GoodID = ag.GoodID
				ORDER BY
					sg.SortIndex ASC";
		return $queryGateway->query($sql)->toArray();
	}
	
}

?>