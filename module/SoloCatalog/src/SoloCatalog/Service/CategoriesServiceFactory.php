<?php

namespace SoloCatalog\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class CategoriesServiceFactory extends AbstractServiceFactory {

	protected function create() {
		$service = new CategoriesService($this->getServiceLocator()->get('SoloCatalog\\Data\\CategoriesMapper'),$this->getServiceLocator()->get('SoloCatalog\\Data\\MenuMapper'));
		return $service;
	}

}

?>