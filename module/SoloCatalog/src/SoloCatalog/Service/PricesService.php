<?php

namespace SoloCatalog\Service;

use SoloCatalog\Service\Helper\PricesHelper;
use SoloCatalog\Service\Helper\DateHelper;
use SoloCatalog\Data\PricesMapperInterface;

class PricesService {

    /**
     *
     * @var PricesMapperInterface
     */
    private $pricesMapper;

    /**
     *
     * @var DateHelper
     */
    private $dateHelper;

    /**
     *
     * @var PricesHelper
     */
    private $helper;

    /**
     *
     * @param PricesMapperInterface $pricesMapper        	
     * @param PricesHelper $helper        	
     * @param DateHelper $dateHelper        	
     */
    public function __construct(PricesMapperInterface $pricesMapper, PricesHelper $helper, DateHelper $dateHelper) {
        $this->pricesMapper = $pricesMapper;
        $this->helper = $helper;
        $this->dateHelper = $dateHelper;
    }

    /**
     *
     * @param integer $goodId
     * @param integer $cityId
     * @param integer $priceCategoryId
     * @throws \InvalidArgumentException
     * @return NULL
     */
    public function getPriceByGoodId($goodId, $cityId, $priceCategoryId) {
        if (!is_integer($goodId)) {
            throw new \InvalidArgumentException('Good id must be integer');
        }
        if (!is_integer($cityId)) {
            throw new \InvalidArgumentException('City id must be integer');
        }
        if (!is_integer($priceCategoryId)) {
            throw new \InvalidArgumentException('Price category id must be integer');
        }
        return $this->pricesMapper->getPriceByGoodId($goodId, $cityId, $priceCategoryId);
    }

    /**
     *
     * @param integer $goodId
     * @param integer $cityId
     * @param integer $priceCategoryId
     * @throws \InvalidArgumentException
     * @return NULL
     */
    public function getPriceWithOldPriceByGoodId($goodId, $cityId, $priceCategoryId) {
        if (!is_integer($goodId)) {
            throw new \InvalidArgumentException('Good id must be integer');
        }
        if (!is_integer($cityId)) {
            throw new \InvalidArgumentException('City id must be integer');
        }
        if (!is_integer($priceCategoryId)) {
            throw new \InvalidArgumentException('Price category id must be integer');
        }
        $prices = $this->pricesMapper->getPriceWithOldPriceByGoodId($goodId, $cityId, $priceCategoryId);
        $result = new \stdClass();
        $result->value = (float) $prices->Value;
        $result->prevValue = (float) $prices->PrevValue;
        return $result;
    }

    /**
     *
     * @param integer $goodId
     * @param integer $cityId
     * @param integer $priceCategoryId
     * @throws \InvalidArgumentException
     * @return NULL
     */
    public function getPricesByGoodId($goodId, $priceCategoryId) {
        if (!is_numeric($goodId)) {
            throw new \InvalidArgumentException('Good id must be integer');
        }
        if (!is_numeric($priceCategoryId)) {
            throw new \InvalidArgumentException('Price category id must be integer');
        }
        $prices = [];
        $pricesData = $this->pricesMapper->getPricesByGoodId($goodId, $priceCategoryId);
        foreach ($pricesData as $priceData) {
            $prices[(int) $priceData['LocationID']] = floatval($priceData['Value']);
        }
        return $prices;
    }

    /**
     *
     * @param array $goodIds
     * @param integer $cityId
     * @param integer $priceColumnId
     * @return integer
     */
    public function getPriceByGoodIds($goodIds, $cityId, $priceColumnId) {
        if (count($goodIds) === 0) {
            return null;
        }
        $prices = [];
        $pricesData = $this->pricesMapper->getPriceByGoodIds($goodIds, $cityId, $priceColumnId);
        foreach ($pricesData as $priceData) {
            $prices[$priceData['GoodID']] = floatval($priceData['Value']);
        }
        return $prices;
    }

    public function getPriceWithOldPriceByGoodIds($goodIds, $cityId, $priceColumnId) {
        if (count($goodIds) === 0) {
            return null;
        }
        $prices = [];
        $result = $this->pricesMapper->getPriceWithOldPriceByGoodIds($goodIds, $cityId, $priceColumnId);
        $rows = $result->toArray();

        foreach ($rows as $row) {
            $prices[$row['GoodID']] = $row;
        }

        return $prices;
    }

    public function getZoneId($cityId) {
        $zone = $this->pricesMapper->getZoneId($cityId);
        return $zone['ZoneID'];
    }

    /**
     *
     * @param array $goodIds        	
     * @return multitype: multitype:Ambigous \Zend\Db\ResultSet\Zend\Db\ResultSet, \Zend\Db\Adapter\Driver\ResultInterface, \Zend\Db\ResultSet\Zend\Db\ResultSetInterface>
     */
    public function enumPricesByGoodIds(array $goodIds) {
        if (0 == sizeof($goodIds)) {
            return [];
        } else {
            $rows = $this->pricesMapper->enumPricesByGoodIds($goodIds);
            $prices = [];
            foreach ($rows as $row) {
                $prices[$row->GoodID] = $row;
            }
            return $prices;
        }
    }

    /**
     *
     * @return \SoloCatalog\Service\Helper\DateHelper
     */
    public function dateHelper() {
        return $this->dateHelper;
    }

    /**
     *
     * @return \SoloCatalog\Service\Helper\PricesHelper
     */
    public function helper() {
        return $this->helper;
    }

    /**
     * return float
     */
    public function getDeliveryCost($cityId) {
        return $this->pricesMapper->getDeliveryCost($cityId);
    }
    
    /**
     * 
     * @param array $goodIds
     * @param integer $cityId
     * @param integer $bonusPriceColumnId
     * @return array
     */
    public function getBonusPriceByGoodIds($goodIds, $cityId, $bonusPriceColumnId) {
        if (!is_array($goodIds) || count($goodIds) == 0) {
            return [];
        }
        $prices = $this->pricesMapper->getBonusPriceByGoodIds($goodIds, $cityId, $bonusPriceColumnId);
        $result = [];
        foreach ($prices as $price) {
            $result[(int)$price['GoodID']] = (float)$price['Value'];
        }
        return $result;
    }
    
    /**
     * 
     * @param integer $goodId
     * @param integer $cityId
     * @param integer $bonusPriceColumnId
     * @return float
     */
    public function getBonusPriceByGoodId($goodId, $cityId, $bonusPriceColumnId) {
        return $this->pricesMapper->getBonusPriceByGoodId($goodId, $cityId, $bonusPriceColumnId);
    }
    
    /**
     * 
     * @param array $goodIds
     * @param integer $cityId
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getAdditionalTkDeliveryPricesByGoodIds(array $goodIds, $cityId) {
        if (!is_array($goodIds)) {
            throw new \InvalidArgumentException('Goods ids must be integer');
        }
        if (!is_int($cityId)) {
            throw new \InvalidArgumentException('City id must be integer');
        }
        return $this->pricesMapper->getAdditionalTkDeliveryPricesByGoodIds($goodIds, $cityId);
    }
    
    /**
     * 
     * @return array
     */
    public function getDeliveryServicePrices() {
        $deliveryServicePrices = $this->pricesMapper->getDeliveryServicePrices();
        $result = [];
        foreach ($deliveryServicePrices as $deliveryServicePrice) {
            if (!array_key_exists((int)$deliveryServicePrice['DistanceFromCity'], $result)) {
                $result[(int)$deliveryServicePrice['DistanceFromCity']] = [
                    'description' => $deliveryServicePrice['DistanceFromCityDescription'],
                    'services' => [],
                ];
            }
            $deliveryServiceId = (int)$deliveryServicePrice['DeliveryServiceID'];
            $deliveryPrice = (float)$deliveryServicePrice['Price'];
            $result[(int)$deliveryServicePrice['DistanceFromCity']]['services'][$deliveryServiceId] = $deliveryPrice;
        }
        return $result;
    }

}
