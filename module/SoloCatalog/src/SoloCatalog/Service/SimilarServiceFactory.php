<?php

namespace SoloCatalog\Service;

use Solo\ServiceManager\AbstractServiceFactory;

class SimilarServiceFactory extends AbstractServiceFactory {

	protected function create() {
		$service = new SimilarService($this->getServiceLocator()->get('SoloCatalog\\Data\\SimilarMapper'));
		return $service;
	}

}

?>