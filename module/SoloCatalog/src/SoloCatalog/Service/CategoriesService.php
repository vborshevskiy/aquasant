<?php

namespace SoloCatalog\Service;

use Solo\Stdlib\ArrayHelper;
use SoloCatalog\Data\CategoriesMapperInterface;
use SoloCatalog\Data\MenuMapperInterface;

class CategoriesService {

    /**
     *
     * @var CategoriesMapperInterface
     */
    private $categoriesMapper;
    private $menuMapper;

    /**
     * 
     * @param CategoriesMapperInterface $categoriesMapper
     */
    public function __construct(CategoriesMapperInterface $categoriesMapper, MenuMapperInterface $menuMapper) {
        $this->categoriesMapper = $categoriesMapper;
        $this->menuMapper = $menuMapper;
    }

    /**
     * 
     * @param string $categoryUid
     * @throws \InvalidArgumentException
     * @return mixed
     */
    public function getCategoryByUid($categoryUid) {
        if (empty($categoryUid)) {
            throw new \InvalidArgumentException('Category uid can\'t be empty');
        }
        return $this->categoriesMapper->getCategoryByUid($categoryUid);
    }

    /**
     *
     * @param integer $categoryId        	
     * @throws \InvalidArgumentException
     * @return NULL
     */
    public function getCategoryById($categoryId) {
        if (!is_integer($categoryId)) {
            throw new \InvalidArgumentException('Category id must be integer');
        }
        return $this->categoriesMapper->getCategoryById($categoryId);
    }

    public function getCategoriesByIds($categoryIds) {
        $rows = $this->categoriesMapper->getCategoriesByIds($categoryIds);

        $result = [];
        foreach ($rows as $row) {
            $result[$row['SoftCategoryID']] = $row;
        }

        return $result;
    }


    /**
     *
     * @param integer $categoryId        	
     * @throws \InvalidArgumentException
     * @return NULL
     */
    public function getCategoriesByGoodsIds(array $goodsId) {
        $rows = $this->categoriesMapper->getCategoriesByGoodsIds($goodsId);
        $categories = [];
        foreach ($rows as $row) {
            $categories[$row['CategoryID']] = $row;
        }
        return $categories;
    }

    /**
     *
     * @param integer $hardCategoryId        	
     * @throws \InvalidArgumentException
     * @return mixed
     */
    public function getCategoryByHardCategoryId($hardCategoryId) {
        if (!is_integer($hardCategoryId)) {
            throw new \InvalidArgumentException('Hard category id must be integer');
        }
        return $this->categoriesMapper->getCategoryByHardCategoryId($hardCategoryId);
    }
    
    /**
     * 
     * @param integer $goodId
     * @return mixed
     */
    public function getPrimaryCategoryByGoodId($goodId) {
    	return $this->categoriesMapper->getPrimaryCategoryByGoodId($goodId);
    }

    /**
     *
     * @param integer $hardCategoryId        	
     * @throws \InvalidArgumentException
     * @return mixed
     */
    public function enumCategoriesByHardCategoryId($hardCategoryId) {
        if (!is_integer($hardCategoryId)) {
            throw new \InvalidArgumentException('Hard category id must be integer');
        }
        return $this->categoriesMapper->enumCategoriesByHardCategoryId($hardCategoryId);
    }

    /**
     *
     * @param array $ids        	
     * @throws \InvalidArgumentException
     * @return array
     */
    public function enumAvailCategoryIdsBySoftCategoryIds($ids) {
        if (!is_array($ids)) {
            throw new \InvalidArgumentException('Ids must be array');
        }
        if (0 == sizeof($ids)) {
            return [];
        }
        $rows = $this->categoriesMapper->enumAvailCategoryIdsBySoftCategoryIds($ids);
        return ArrayHelper::enumOneColumn($rows, 'SoftCategoryID');
    }

    public function getParentCategories($cLevel, $cLeft, $cRight) {
        return $this->categoriesMapper->getParentCategories($cLevel, $cLeft, $cRight);
    }
    
    /**
     * 
     * @param array $goodsIds
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @return array
     */
    public function getRelatedCategoriesByGoodsIds($goodsIds, $cityId, $isPostDelivery) {
        if (is_array($goodsIds) && count($goodsIds) > 0) {
            $categories = $this->categoriesMapper->getRelatedCategoriesByGoodsIds($goodsIds);
            $availCategories = [];
            if ($categories && (0 < count($categories))) {
                $categoriesWithCountGoods = $this->menuMapper->enumGoodsCountByCategoryIds(ArrayHelper::enumOneColumn($categories, 'SoftCategoryID'), $cityId, $isPostDelivery);
                foreach ($categories as $key => $category) {
                    if (key_exists($category['SoftCategoryID'], $categoriesWithCountGoods)) {
                        $availCategories[$category['SoftCategoryID']] = $category;
                    }
                    $availCategories[$category['SoftCategoryID']]['count'] = $categoriesWithCountGoods[$category['SoftCategoryID']];
                }
            }
            return $availCategories;
        }
        return [];
    }

    public function enumCategoriesByParentId($categoryId) {
        if (!is_integer($categoryId)) {
            throw new \InvalidArgumentException('Category id must be integer');
        }

        return $this->categoriesMapper->enumCategoriesByParentId($categoryId);
    }
}
