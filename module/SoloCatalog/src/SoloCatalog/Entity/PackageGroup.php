<?php

namespace SoloCatalog\Entity;

class PackageGroup {

	const SELECT_MODE_AND = 'and';

	const SELECT_MODE_OR = 'or';

	/**
	 *
	 * @var boolean
	 */
	private $isRequired = false;

	/**
	 *
	 * @var boolean
	 */
	private $isMain = false;

	/**
	 *
	 * @var string
	 */
	private $selectMode = self::SELECT_MODE_AND;

	/**
	 *
	 * @var array
	 */
	private $items = [];

	/**
	 *
	 * @param boolean $value        	
	 * @return boolean
	 */
	public function isRequired($value = null) {
		if (null !== $value) {
			$this->isRequired = $value;
		} else {
			return $this->isRequired;
		}
	}

	/**
	 *
	 * @param boolean $value        	
	 * @return boolean
	 */
	public function isMain($value = null) {
		if (null !== $value) {
			$this->isMain = $value;
		} else {
			return $this->isMain;
		}
	}

	/**
	 *
	 * @param string $mode        	
	 * @return \SoloCatalog\Entity\PackageGroup
	 */
	public function setSelectMode($mode) {
		$this->selectMode = $mode;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getSelectMode() {
		return $this->selectMode;
	}

	/**
	 *
	 * @param PackageGroupItem $item        	
	 * @return \SoloCatalog\Entity\PackageGroup
	 */
	public function addItem(PackageGroupItem $item) {
		$this->items[$item->getId()] = $item;
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getItems() {
		return $this->items;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasItems() {
		return (0 < count($this->items));
	}

	/**
	 *
	 * @param integer $id        	
	 */
	public function selectItem($id) {
		if (self::SELECT_MODE_OR == $this->getSelectMode()) {
			foreach ($this->items as $item) {
				$item->isSelected(false);
			}
		}
		if (isset($this->items[$id])) {
			$this->items[$id]->isSelected(true);
		}
	}

	/**
	 * Select items with default flag
	 */
	public function selectDefaultItems() {
		foreach ($this->getItems() as $item) {
			if ($item->isDefault()) {
				$this->selectItem($item->getId());
			}
		}
	}
	
	/**
	 * 
	 * @return boolean
	 */
	public function hasDefaultItems() {
		$result = false;
		
		foreach ($this->getItems() as $item) {
			if ($item->isDefault()) {
				$result = true;
			}
		}
		
		return $result;
	}
	
	/**
	 * 
	 * @return PackageGroupItem
	 */
	public function getFirstItem() {
		$result = null;
		
		if (0 < count($this->items)) {
			$result = reset($this->items);
		}
		
		return $result;
	}
	
	public function selectFirstItem() {
		if ($this->hasItems()) {
			$firstItem = $this->getFirstItem();
			if ($firstItem) {
				$this->selectItem($firstItem->getId());
			}
		}
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasSelectedItem() {
		foreach ($this->getItems() as $item) {
			if ($item->isSelected()) {
				return true;
			}
		}
		return false;
	}
	
	public function moveSelectedToTheTop() {
		$selectedItems = [];
		foreach ($this->items as $id => $item) {
			if ($item->isSelected()) {
				$selectedItems[$id] = $item;
			}
		}
		if (0 < count($selectedItems)) {
			$items = $selectedItems;
			foreach ($this->items as $id => $item) {
				if (!isset($items[$id])) {
					$items[$id] = $item;
				}
			}
			$this->items = $items;
		}
	}

}

?>