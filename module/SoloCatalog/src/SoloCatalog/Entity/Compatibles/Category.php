<?php

namespace SoloCatalog\Entity\Compatibles;

class Category {

    /**
     *
     * @var integer
     */
    private $id;
    
    /**
     *
     * @var string
     */
    private $name;
    
    /**
     *
     * @var GoodsCollection
     */
    private $goods;

    /**
     * 
     * @param integer $id
     */
    public function __construct($id = null) {
        if (!is_null($id)) {
            $this->setId($id);
        }
        $this->goods = new GoodsCollection();
    }

    /**
     *
     * @return integer $id
     */
    public function getId() {
        return $this->id;
    }

    /**
     * 
     * @param integer $id
     * @return \SoloCatalog\Entity\Compatibles\Category
     * @throws \InvalidArgumentException
     */
    public function setId($id) {
        if (!is_int($id)) {
            throw new \InvalidArgumentException('Category id must be integer');
        }
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string $name
     */
    public function getName() {
        return $this->name;
    }

    /**
     * 
     * @param string $name
     * @return \SoloCatalog\Entity\Compatibles\Category
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    
    /**
     * 
     * @return GoodsCollection
     */
    public function goods() {
        return $this->goods;
    }
}