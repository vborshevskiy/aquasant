<?php

namespace SoloCatalog\Entity\Compatibles;

use Solo\Collection\Collection;

class GoodsCollection extends Collection {

    /**
     * 
     * @param \SoloCatalog\Entity\Compatibles\Good $good
     * @param mixed $key
     * @return \SoloCatalog\Entity\Compatibles\Good
     * @throws \RuntimeException
     */
    public function add($good, $key = null) {
        if (!($good instanceof Good)) {
            throw new \RuntimeException('Good must be an instace of Good');
        }
        $key = (is_null($key) ? $good->getId() : $key);
        parent::add($good, $key);
        return $good;
    }

}