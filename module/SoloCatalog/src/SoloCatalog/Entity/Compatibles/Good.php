<?php

namespace SoloCatalog\Entity\Compatibles;

class Good {

    /**
     *
     * @var integer
     */
    private $id;
    
//    /**
//     *
//     * @var string
//     */
//    private $name;
//    
//    /**
//     *
//     * @var string
//     */
//    private $url;
//    
//    /**
//     *
//     * @var string
//     */
//    private $avatar;
//    
//    /**
//     *
//     * @var float
//     */
//    private $price = 0;
//    
//    /**
//     *
//     * @var float
//     */
//    private $bonusPrice = 0;
//    
//    /**
//     *
//     * @var integer
//     */
//    private $quantity = 0;

    /**
     * 
     * @param integer $id
     */
    public function __construct($id = null) {
        if (!is_null($id)) {
            $this->setId($id);
        }
    }

    /**
     *
     * @return integer $id
     */
    public function getId() {
        return $this->id;
    }

    /**
     * 
     * @param integer $id
     * @return \SoloCatalog\Entity\Compatibles\Good
     * @throws \InvalidArgumentException
     */
    public function setId($id) {
        if (!is_int($id)) {
            throw new \InvalidArgumentException('Good id must be integer');
        }
        $this->id = $id;
        return $this;
    }

//    /**
//     *
//     * @return string $name
//     */
//    public function getName() {
//        return $this->name;
//    }
//
//    /**
//     * 
//     * @param string $name
//     * @return \SoloCatalog\Entity\Compatibles\Good
//     */
//    public function setName($name) {
//        $this->name = $name;
//        return $this;
//    }
//
//    /**
//     * 
//     * @return string
//     */
//    public function getAvatar() {
//        return $this->avatar;
//    }
//
//    /**
//     * 
//     * @param string $avatar
//     * @return \SoloCatalog\Entity\Compatibles\Good
//     */
//    public function setAvatar($avatar) {
//        $this->avatar = $avatar;
//        return $this;
//    }
//
//    /**
//     * 
//     * @return string
//     */
//    public function getUrl() {
//        return $this->url;
//    }
//
//    /**
//     * 
//     * @param string $url
//     * @return \SoloCatalog\Entity\Compatibles\Good
//     */
//    public function setUrl($url) {
//        $this->url = $url;
//        return $this;
//    }
//    
//    /**
//     * 
//     * @return float
//     */
//    public function getPrice() {
//        return $this->price;
//    }
//
//    /**
//     * 
//     * @param float $price
//     * @return \SoloCatalog\Entity\Compatibles\Good
//     * @throws \InvalidArgumentException
//     */
//    public function setPrice($price) {
//        if (!is_numeric($price)) {
//            throw new \InvalidArgumentException('Good price must be numeric');
//        }
//        $this->price = $price;
//        return $this;
//    }
//    
//    /**
//     * 
//     * @return float
//     */
//    public function getBonusPrice() {
//        return $this->bonusPrice;
//    }
//
//    /**
//     * 
//     * @param float $bonusPrice
//     * @return \SoloCatalog\Entity\Compatibles\Good
//     * @throws \InvalidArgumentException
//     */
//    public function setBonusPrice($bonusPrice) {
//        if (!is_numeric($bonusPrice)) {
//            throw new \InvalidArgumentException('Good bonus price must be numeric');
//        }
//        $this->bonusPrice = $bonusPrice;
//        return $this;
//    }
//    
//    /**
//     * 
//     * @return integer
//     */
//    public function getQuantity() {
//        return $this->quantity;
//    }
//
//    /**
//     * 
//     * @param integer $quantity
//     * @return \SoloCatalog\Entity\Compatibles\Good
//     * @throws \InvalidArgumentException
//     */
//    public function setQuantity($quantity) {
//        if (!is_int($quantity)) {
//            throw new \InvalidArgumentException('Good quantity must be integer');
//        }
//        $this->quantity = $quantity;
//        return $this;
//    }

}