<?php

namespace SoloCatalog\Entity\Compatibles;

use Solo\Collection\Collection;

class CategoriesCollection extends Collection {

    /**
     * 
     * @param \SoloCatalog\Entity\Compatibles\Category $category
     * @param mixed $key
     * @return \SoloCatalog\Entity\Compatibles\Category
     * @throws \RuntimeException
     */
    public function add($category, $key = null) {
        if (!($category instanceof Category)) {
            throw new \RuntimeException('Category must be an instace of Category');
        }
        $key = (is_null($key) ? $category->getId() : $key);
        parent::add($category, $key);
        return $category;
    }
    
    /**
     * 
     * @return boolean
     */
    public function hasGoods() {
        foreach ($this->list as $category) {
            if ($category->goods()->count() > 0) {
                return true;
            }
        }
        return false;
    }

}