<?php

namespace SoloCatalog\Entity;

class PackageGroupItem {

	/**
	 *
	 * @var integer
	 */
	private $id;

	/**
	 *
	 * @var string
	 */
	private $name;

	/**
	 *
	 * @var string
	 */
	private $url;

	/**
	 *
	 * @var integer
	 */
	private $groupId;

	/**
	 *
	 * @var boolean
	 */
	private $isDefault = false;

	/**
	 *
	 * @var boolean
	 */
	private $isSelected = false;

	/**
	 *
	 * @var integer
	 */
	private $availQuantity;

	/**
	 *
	 * @var integer
	 */
	private $windowQuantity;

	/**
	 *
	 * @var string
	 */
	private $arrivalDate;

	/**
	 *
	 * @var string
	 */
	private $pickupDate;

	/**
	 *
	 * @var string
	 */
	private $deliveryDate;

	/**
	 *
	 * @var array
	 */
	private $place = [];

	/**
	 *
	 * @return number
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param number $id
	 */
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 *
	 * @param string $name
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 *
	 * @param string $url
	 */
	public function setUrl($url) {
		$this->url = $url;
		return $this;
	}

	/**
	 *
	 * @return number
	 */
	public function getGroupId() {
		return $this->groupId;
	}

	/**
	 *
	 * @param number $groupId
	 */
	public function setGroupId($groupId) {
		$this->groupId = $groupId;
		return $this;
	}

	/**
	 *
	 * @return number
	 */
	public function getAvailQuantity() {
		return $this->availQuantity;
	}

	/**
	 *
	 * @param number $availQuantity
	 */
	public function setAvailQuantity($availQuantity) {
		$this->availQuantity = $availQuantity;
		return $this;
	}

	/**
	 *
	 * @return number
	 */
	public function getWindowQuantity() {
		return $this->windowQuantity;
	}

	/**
	 *
	 * @param number $availQuantity
	 */
	public function setWindowQuantity($windowQuantity) {
		$this->windowQuantity = $windowQuantity;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getArrivalDate() {
		return $this->arrivalDate;
	}

	/**
	 *
	 * @param string $arrivalDate
	 */
	public function setArrivalDate($arrivalDate) {
		$this->arrivalDate = $arrivalDate;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getPickupDate() {
		return $this->pickupDate;
	}

	/**
	 *
	 * @param string $pickupDate
	 */
	public function setPickupDate($pickupDate) {
		$this->pickupDate = $pickupDate;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getDeliveryDate() {
		return $this->deliveryDate;
	}

	/**
	 *
	 * @param string $deliveryDate
	 */
	public function setDeliveryDate($deliveryDate) {
		$this->deliveryDate = $deliveryDate;
		return $this;
	}

	/**
	 *
	 * @return array
	 */
	public function getPlace() {
		return $this->place;
	}

	/**
	 *
	 * @param array $id
	 */
	public function setPlace(array $place) {
		$this->place = $place;
		return $this;
	}

	/**
	 *
	 * @param boolean $value
	 * @return boolean
	 */
	public function isDefault($value = null) {
		if (null !== $value) {
			$this->isDefault = $value;
		} else {
			return $this->isDefault;
		}
	}

	/**
	 *
	 * @param boolean $value
	 * @return boolean
	 */
	public function isSelected($value = null) {
		if (null !== $value) {
			$this->isSelected = $value;
		} else {
			return $this->isSelected;
		}
	}

}

?>