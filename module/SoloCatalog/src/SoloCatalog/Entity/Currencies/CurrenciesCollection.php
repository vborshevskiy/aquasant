<?php

namespace SoloCatalog\Entity\Currencies;

use Solo\Collection\Collection;

class CurrenciesCollection extends Collection {

    /**
     *
     * @param Currency $currency        	
     * @return Currency
     */
    public function add($currency, $key = null) {
        if (!($currency instanceof Currency)) {
            throw new \RuntimeException('Currency must be an instace of Currency');
        }
        $key = (is_null($key) ? $currency->getId() : $key);
        parent::add($currency, $key);
        return $currency;
    }

}

?>