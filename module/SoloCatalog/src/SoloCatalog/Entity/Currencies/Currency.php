<?php

namespace SoloCatalog\Entity\Currencies;

class Currency {

    /**
     *
     * @var integer
     */
    private $id = 0;

    /**
     *
     * @var string
     */
    private $name = '';
    
    /**
     *
     * @var float
     */
    private $rate = 0;
    
    /**
     *
     * @var boolean
     */
    private $mainCurrency = '';

    /**
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @param integer $id        	
     * @throws \InvalidArgumentException
     * @return Currency
     */
    public function setId($id) {
        if (!is_integer($id)) {
            throw new \InvalidArgumentException('Id must be integer');
        }
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     *
     * @param string $name        	
     * @return Currency
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    
    /**
     *
     * @return float
     */
    public function getRate() {
        return $this->rate;
    }

    /**
     *
     * @param float $rate        	
     * @throws \InvalidArgumentException
     * @return Currency
     */
    public function setRate($rate) {
        if (!is_float($rate)) {
            throw new \InvalidArgumentException('Rate must be float');
        }
        $this->rate = $rate;
        return $this;
    }
    
    /**
     *
     * @param null|boolean $mainCurrency        	
     * @throws \InvalidArgumentException
     * @return boolean|Currency
     */
    public function isMainCurrency($mainCurrency = null) {
        if (is_null($mainCurrency)) {
            return $this->mainCurrency;
        } else {
            if (!is_bool($mainCurrency)) {
                throw new \InvalidArgumentException('Main currency must be boolean');
            }
            $this->mainCurrency = $mainCurrency;
            return $this;
        }
    }

}

?>