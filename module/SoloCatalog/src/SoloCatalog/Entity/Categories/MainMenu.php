<?php

namespace SoloCatalog\Entity\Categories;

class MainMenu implements \IteratorAggregate {

	protected $colors = [
		'7fc873',
		'00a69c',
		'6177be',
		'bf6868',
		'e89455',
		'c54c82',
		'4fb8af',
		'6e89bb',
		'ee8260',
		'74bbd3',
		'7f7f7f' 
	];

	/**
	 *
	 * @var array
	 */
	private $items = [];

	/**
	 * (non-PHPdoc)
	 *
	 * @see IteratorAggregate::getIterator()
	 */
	public function getIterator() {
		return new \ArrayIterator($this->items);
	}

	/**
	 *
	 * @param MenuItem $item        	
	 * @return MenuItem
	 */
	public function addItem(MenuItem $item) {
		$this->items[$item->getId()] = $item;
		return $item;
	}

	/**
	 *
	 * @param integer $id        	
	 * @throws \InvalidArgumentException
	 * @return boolean
	 */
	public function selectById($id) {
		if (!is_integer($id)) {
			throw new \InvalidArgumentException('Id must be integer');
		}
		if (array_key_exists($id, $this->items)) {
			$this->items[$id]->setSelected(true);
			return true;
		}
		foreach ($this->items as $item) {
			if ($item->selectById($id)) {
				return true;
			}
		}
		return false;
	}

	/**
	 *
	 * @param integer $id        	
	 * @param array $parentIds        	
	 * @throws \InvalidArgumentException
	 * @return array
	 */
	public function enumChildsByParentId($id, $parentIds = []) {
		if (!is_integer($id)) {
			throw new \InvalidArgumentException('Id must be integer');
		}
		$tree = $this->items;
		
		if (is_array($parentIds) && count($parentIds) > 0) {
			foreach ($parentIds as $parentId) {
				if (array_key_exists($parentId, $tree)) {
					$tree = $tree[$parentId]->getItems();
				}
			}
		}
		if (array_key_exists($id, $tree)) {
			return $tree[$id]->getItems();
		}
		return false;
	}

	public function forChilds($item, $level) {
		if ($item) {
			foreach ($item as $i) {
				if ($level != $i->getLevel()) {
					$j = $i->getItems();
					$this->forChilds($j, $level);
				} else
					$i->delItems();
			}
		}
	}

	/**
	 *
	 * @return array
	 */
	public function toArray() {
		$struct = [];
		foreach ($this->items as $item) {
			$struct[] = $item->toArray();
		}
		return $struct;
	}

	/**
	 *
	 * @param boolean $prettyPrint        	
	 * @return string
	 */
	public function toJSON($prettyPrint = false) {
		$struct = $this->toArray();
		$options = JSON_UNESCAPED_UNICODE;
		if ($prettyPrint) {
			$options |= JSON_PRETTY_PRINT;
		}
		return json_encode($struct, $options);
	}
	
	/*
	 * @return array
	 */
	public function getColors() {
		return $this->colors;
	}

	/**
	 *
	 * @return array
	 */
	public function getItems() {
		return $this->items;
	}

	/**
	 *
	 * @param integer $id        	
	 * @return MenuItem
	 */
	public function findById($id) {
		$result = null;
		
		if (isset($this->items[$id])) {
			$result = $this->items[$id];
		}
		if (!$result) {
			foreach ($this->items as $subitem) {
				if (!$result) {
					$result = $subitem->findById($id);
				}
			}
		}
		
		return $result;
	}

}

?>