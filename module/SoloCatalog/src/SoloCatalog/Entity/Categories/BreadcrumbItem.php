<?php

namespace SoloCatalog\Entity\Categories;

class BreadcrumbItem {

	/**
	 *
	 * @var string
	 */
	private $title = '';

	/**
	 *
	 * @var string
	 */
	private $url = '';

	/**
	 *
	 * @var array
	 */
	private $subitems = [];

	/**
	 *
	 * @param string $title        	
	 * @param string $url        	
	 */
	public function __construct($title = null, $url = null) {
		if (null !== $title) $this->setTitle($title);
		if (null !== $url) $this->setUrl($url);
	}

	/**
	 *
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 *
	 * @return string
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 *
	 * @param string $title        	
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 *
	 * @param string $url        	
	 */
	public function setUrl($url) {
		$this->url = $url;
	}

	/**
	 *
	 * @param BreadcrumbItem $item        	
	 */
	public function addSubitem(BreadcrumbItem $item) {
		$this->subitems[] = $item;
	}

	/**
	 *
	 * @return array
	 */
	public function getSubitems() {
		return $this->subitems;
	}

}

?>