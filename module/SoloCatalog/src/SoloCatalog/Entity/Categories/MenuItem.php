<?php

namespace SoloCatalog\Entity\Categories;

class MenuItem {

	/**
	 *
	 * @var integer
	 */
	private $id = -1;

	/**
	 *
	 * @var string
	 */
	private $name = '';

	/**
	 *
	 * @var string
	 */
	private $url = '';

	/**
	 *
	 * @var integer
	 */
	private $level = 0;

	/**
	 *
	 * @var array
	 */
	private $items = [];

	/**
	 *
	 * @var MenuItem
	 */
	private $parent = null;

	/**
	 *
	 * @var boolean
	 */
	private $selected = false;

	/**
	 *
	 * @var integer
	 */
	private $goodsCount = 0;

	/**
	 *
	 * @var array
	 */
	private $filters = [];

	/**
	 *
	 * @var boolean
	 */
	private $isItemSelected = false;

	public function __construct($id = null, $name = null, $url = null, $level = null, MenuItem $parent = null) {
		if (null !== $id) $this->setId(intval($id));
		if (null !== $name) $this->setName($name);
		if (null !== $url) $this->setUrl($url);
		if (null !== $level) $this->setLevel(intval($level));
		if (null !== $parent) $this->setParent($parent);
	}

	/**
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 *
	 * @return string
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 *
	 * @return string
	 */
	public function getUid() {
		return trim($this->url, '/');
	}

	/**
	 *
	 * @return integer
	 */
	public function getLevel() {
		return $this->level;
	}

	/**
	 *
	 * @return boolean
	 */
	public function isSelected() {
		return $this->selected;
	}

	/**
	 *
	 * @param integer $id        	
	 * @throws \InvalidArgumentException
	 */
	public function setId($id) {
		if (!is_integer($id)) {
			throw new \InvalidArgumentException('Id must be integer');
		}
		$this->id = $id;
	}

	/**
	 *
	 * @param integer $level        	
	 * @throws \InvalidArgumentException
	 */
	public function setLevel($level) {
		if (!is_integer($level)) {
			throw new \InvalidArgumentException('Level must be integer');
		}
		$this->level = $level;
	}

	/**
	 *
	 * @param string $name        	
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 *
	 * @param string $url        	
	 */
	public function setUrl($url) {
		$this->url = $url;
	}

	/**
	 *
	 * @param MenuItem $parent        	
	 */
	public function setParent(MenuItem $parent) {
		$this->parent = $parent;
	}

	/**
	 *
	 * @return \SoloCatalog\Entity\Categories\MenuItem
	 */
	public function getParent() {
		return $this->parent;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasParent() {
		return (null !== $this->parent);
	}

	/**
	 *
	 * @param boolean $isItemSelected        	
	 */
	public function setHasSelectedItem($isItemSelected) {
		$this->isItemSelected = $isItemSelected;
		if (null !== $this->parent) {
			$this->parent->setHasSelectedItem($isItemSelected);
		}
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasSelectedItem() {
		return $this->isItemSelected;
	}

	/**
	 *
	 * @param boolean $selected        	
	 */
	public function setSelected($selected) {
		$this->selected = $selected;
		$parent = $this->parent;
		if (null !== $parent) {
			$parent->setHasSelectedItem(true);
		}
	}

	/**
	 *
	 * @param MenuItem $item        	
	 */
	public function addItem(MenuItem $item) {
		$item->setParent($this);
		$this->items[$item->getId()] = $item;
	}

	/**
	 *
	 * @return array
	 */
	public function getItems() {
		return $this->items;
	}

	public function delItems() {
		$this->items = [];
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasItems() {
		return (0 < sizeof($this->items));
	}

	/**
	 *
	 * @param integer $id        	
	 * @return boolean
	 */
	public function selectById($id) {
		if (array_key_exists($id, $this->items)) {
			$this->items[$id]->setSelected(true);
			return true;
		}
		foreach ($this->items as $item) {
			if ($item->selectById($id)) {
				return true;
			}
		}
		return false;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasSelected() {
		if ($this->isSelected()) {
			return true;
		}
		foreach ($this->items as $item) {
			if ($item->hasSelected()) {
				return true;
			}
		}
		return false;
	}

	/**
	 *
	 * @param integer $goodsCount        	
	 * @throws \InvalidArgumentException
	 */
	public function setGoodsCount($goodsCount) {
		if (!is_integer($goodsCount)) {
			throw new \InvalidArgumentException('Goods count must be integer');
		}
		$this->goodsCount = $goodsCount;
	}

	/**
	 *
	 * @return integer
	 */
	public function getGoodsCount() {
		return $this->goodsCount;
	}

	/**
	 *
	 * @param integer $goodsCount        	
	 * @throws \InvalidArgumentException
	 */
	public function addGoodsCount($goodsCount) {
		if (!is_integer($goodsCount)) {
			throw new \InvalidArgumentException('Goods count must be integer');
		}
		$this->goodsCount = $this->goodsCount + $goodsCount;
	}

	/**
	 *
	 * @return array
	 */
	public function getFilters() {
		return $this->filters;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasFilters() {
		return (count($this->filters) > 0);
	}

	/**
	 *
	 * @param array $filters        	
	 * @throws \InvalidArgumentException
	 */
	public function addFilters($filters) {
		if (!is_array($filters)) {
			throw new \InvalidArgumentException('Filters must be array');
		}
		$this->filters = $this->filters + $filters;
	}

	/**
	 *
	 * @return array
	 */
	public function getFilterKeys() {
		return array_keys($this->filters);
	}

	/**
	 *
	 * @return multitype:multitype: number string
	 */
	public function toArray() {
		$struct = array(
			'id' => $this->getId(),
			'title' => $this->getName(),
			'level' => $this->getLevel(),
			'url' => $this->getUrl() 
		);
		if (0 < sizeof($this->items)) {
			$struct['nodes'] = [];
			foreach ($this->items as $item) {
				$struct['nodes'][] = $item->toArray();
			}
		}
		return $struct;
	}

	/**
	 *
	 * @param integer $id        	
	 * @return MenuItem
	 */
	public function findById($id) {
		$result = null;
		
		if (isset($this->items[$id])) {
			$result = $this->items[$id];
		}
		if (!$result) {
			foreach ($this->items as $subitem) {
				if (!$result) {
					$result = $subitem->findById($id);
				}
			}
		}
		
		return $result;
	}

}

?>