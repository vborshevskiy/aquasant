<?php

namespace SoloCatalog\Entity\Filters\SelectedFilter;

class Warehouses extends AbstractFilter implements SelectedFilterInterface {

	private $ids = [];

	public function __construct($ids = null) {           
		if ((null !== $ids) && is_array($ids)) {
			foreach ($ids as $id) {
				$this->addId($id);
			}
		}
	}

	public function addId($id) {
		$this->ids[$id] = $id;
	}

	public function removeId($id) {
		if (array_key_exists($id, $this->ids)) {
			unset($this->ids[$id]);
		}
	}

	public function enumIds() {
		return $this->ids;
	}

	public static function createFromQuery($query) {
		if (isset($query['w'])) {
			$obj = new Warehouses(explode(',', $query['w']));
			return $obj;
		}
		return null;
	}

	public function toQueryString() {
		$param = $this->toQueryParam();              
		if (null !== $param) {
			return key($param) . '=' . current($param);
		}
		return '';
	}

	public function toQueryParam() {           
		if (0 < sizeof($this->ids)) {
			return array(
				'w' => implode(',', $this->ids) 
			);
		}
		return null;
	}

	public function copy() {
		return new Warehouses($this->ids);
	}

}

?>