<?php

namespace SoloCatalog\Entity\Filters\SelectedFilter;

class Price extends AbstractFilter implements SelectedFilterInterface {

	private $from = null;

	private $to = null;

        private $category = 1;
        
        /**
         * 
         * @param integer $from
         * @param integer $to
         */
        public function __construct($from = null, $to = null) {
		if (null !== $from) $this->from = $from;
		if (null !== $to) $this->to = $to;
	}
        
	/**
	 *
	 * @return the $from
	 */
	public function getFrom() {
		return $this->from;
	}

	/**
	 *
	 * @param string $from        	
	 */
	public function setFrom($from) {
		$this->from = $from;
	}
        /**
	 *
	 * @return integer $category
	 */
	public function getCategory() {
		return $this->category;
	}

	/**
	 *
	 * @param integer $category        	
	 */
	public function setCategory($category) {
		$this->category = $category;
	}
	/**
	 *
	 * @return integer $to
	 */
	public function getTo() {
		return $this->to;
	}

	/**
	 *
	 * @param integer $to        	
	 */
	public function setTo($to) {
		$this->to = $to;
	}

	public static function createFromQuery($query) {
		if (isset($query['pricemin'])&&(isset($query['pricemax']))) {		
			if (is_numeric($query['pricemin']) && is_numeric($query['pricemax'])) {
				$obj = new Price(intval($query['pricemin']), intval($query['pricemax']));
			}
			return $obj;
		}
		return null;
	}

        /**
         * 
         * @return string
         */
	public function toQueryString() {
		$param = $this->toQueryParam();
		if (null !== $param) {                    
                        return implode("&", $param);                    
		}
		return '';
	}

        /**
         * 
         * @return array|null
         */
	public function toQueryParam() {
		if ((null !== $this->from) && (null !== $this->to)) {
			return array(
				'pricemin=' . $this->from,
                                'pricemax=' . $this->to,
			);
		}
		return null;
	}

        /**
         * 
         * @return \SoloCatalog\Entity\Filters\SelectedFilter\Price
         */
	public function copy() {
		return new Price($this->from, $this->to);
	}

}

?>