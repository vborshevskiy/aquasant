<?php

namespace SoloCatalog\Entity\Filters\SelectedFilter;

abstract class AbstractFilter implements SelectedFilterInterface {

    /**
     *
     * @var integer|null
     */
    protected $priority = null;
    
    /**
     *
     * @var integer 
     */
    protected $defaultPriority = null;
    
    /**
     *
     * @var boolean
     */
    protected $isRouteParam = false;
    
    /**
     *
     * @var array
     */
    protected $uids = [];

    /**
     * 
     * @param integer|null $priority
     * @return integer
     */
    public function priority($priority = null) {
        if (!is_null($priority)) {
            $this->priority = (int)$priority;
        }
        return $this->priority;
    }
    
    /**
     * 
     * @param boolean|null $routeParam
     * @return boolean
     */
    public function routeParam($routeParam = null) {
        if (!is_null($routeParam)) {
            $this->isRouteParam = (bool)$routeParam;
        }
        return $this->isRouteParam;
    }
    
    /**
     * 
     * @param integer $id
     * @param string $uid
     * @return \SoloCatalog\Entity\Filters\SelectedFilter\SelectedFilterInterface
     */
    public function addUid($id, $uid) {
        $this->uids[$id] = $uid;
        return $this;
    }
    
    /**
     * 
     * @param integer $id
     * @return string
     */
    public function getUid($id) {
        if ($this->hasUid($id)) {
            return $this->uids[$id];
        }
        return null;
    }
    
    /**
     * 
     * @return array
     */
    public function getUids() {
        return $this->uids;
    }
    
    /**
     * 
     * @param array $uids
     * @return \SoloCatalog\Entity\Filters\SelectedFilter\SelectedFilterInterface
     */
    public function setUids($uids) {
        $this->uids = $uids;
        return $this;
    }
    
    /**
     * 
     * @param integer $id
     * @return \SoloCatalog\Entity\Filters\SelectedFilter\SelectedFilterInterface
     */
    public function removeUid($id) {
        if ($this->hasUid($id)) {
            unset($this->uids[$id]);
        }
        return $this;
    }
    
    /**
     * 
     * @param integer $id
     * @return boolean
     */
    public function hasUid($id) {
        return (array_key_exists($id, $this->uids));
    }

}

?>