<?php

namespace SoloCatalog\Entity\Filters\SelectedFilter;

class Brands extends AbstractFilter implements SelectedFilterInterface {

    private $ids = [];
    
    protected $isRouteParam = true;

    protected $priority = 1;
    
    protected $defaultPriority = 1;

    public function __construct($ids = null) {
        if ((null !== $ids) && is_array($ids)) {
            foreach ($ids as $id) {
                $this->addId($id);
            }
        }
    }

    public function addId($id,$forseAsGetParam = false) {
        $this->ids[$id] = $id;
        if ($forseAsGetParam) {
            $this->routeParam(false);
            $this->priority(-1);
        } else {
            $this->updateIsRouteParam();
        }
    }

    public function removeId($id,$forseAsGetParam = false) {

        if (array_key_exists($id, $this->ids)) {
            unset($this->ids[$id]);
        }
        if ($forseAsGetParam) {
            $this->routeParam(false);
            $this->priority(-1);
        } else {
            $this->updateIsRouteParam();
        }
    }
    
    public function updateIsRouteParam() {
        if (count($this->ids) > 1) {
            $this->routeParam(false);
            $this->priority(-1);
        } else {
            $this->routeParam(true);
            $this->priority($this->defaultPriority);
        }
    }

    public function enumIds() {
        return $this->ids;
    }

    public static function createFromQuery($query) {
        if (isset($query['b'])) {
            $obj = new Brands(explode(',', $query['b']));
            return $obj;
        } elseif (isset($query['brands'])) {            
            $obj = new Brands([$query['brands']]);
            return $obj;
        }
        return null;
    }

    public function toQueryString() {
        $param = $this->toQueryParam();
        if (is_array($param)) {
            return key($param) . '=' . current($param);
        } elseif (is_string($param)) {
            return $param;
        }
        return '';
    }

    public function toQueryParam() {
        if (0 < sizeof($this->ids)) {
            if ($this->routeParam()) {
                $brandId = current($this->enumIds());
                $brandUrlPath = '__';
                if ($this->hasUid($brandId)) {
                    $brandUrlPath .= $this->getUid($brandId) . '__';
                }
                $brandUrlPath .= $brandId;
                return $brandUrlPath;
            } else {
                return array(
                    'b' => implode(',', $this->ids)
                );
            }
        }
        return null;
    }

    public function copy() {
        $brands = new Brands($this->ids);
        $brands->setUids($this->getUids());
        $brands->priority($this->priority);
        $brands->routeParam($this->routeParam());
        return $brands;
    }
    
    public function count() {
        return count($this->ids);
    }

}

?>