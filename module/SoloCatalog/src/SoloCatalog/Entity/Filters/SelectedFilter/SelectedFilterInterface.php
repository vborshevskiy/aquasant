<?php

namespace SoloCatalog\Entity\Filters\SelectedFilter;

interface SelectedFilterInterface {

    public static function createFromQuery($query);

    public function toQueryString();

    public function toQueryParam();

    public function copy();

    public function priority();
}

?>