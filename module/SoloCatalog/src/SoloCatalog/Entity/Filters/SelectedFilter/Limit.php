<?php

namespace SoloCatalog\Entity\Filters\SelectedFilter;

class Limit extends AbstractFilter implements SelectedFilterInterface {

    private $page = null;
    
    protected $isRouteParam = true;
    
    protected $priority = 2;

    public function __construct($page = null) {
        if (null !== $page)
            $this->page = $page;
    }

    public function setPage($page) {
        $this->page = $page;
    }

    public function getPage() {
        return $this->page;
    }

    public static function createFromQuery($query) {
        if (isset($query['page']) && intval($query['page']) > 1) {
            $obj = new Limit(intval($query['page']));
            return $obj;
        }
        return null;
    }

    public function toQueryString() {
        $param = $this->toQueryParam();
        if (null !== $param) {
            if ($this->routeParam()) {
                return '___' . key($param) . current($param) . '/';
            } else {
                return key($param) . '=' . current($param);
            }
        }
        return '';
    }

    public function toQueryParam() {
        if ((null !== $this->page) && (0 < intval($this->page))) {
            return array(
                'page' => $this->page
            );
        }
        return null;
    }

    public function copy() {
        return new Limit($this->page);
    }

}

?>