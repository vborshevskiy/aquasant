<?php

namespace SoloCatalog\Entity\Filters\SelectedFilter;

class SelectedFilterSet {

    private $filters = [];

    public function add($name, $filter) {
        if (null !== $filter) {
            $this->filters[$name] = $filter;
            return true;
        }
        return false;
    }

    public function has($name) {
        return isset($this->filters[$name]);
    }

    public function get($name) {
        if ($this->has($name)) {
            return $this->filters[$name];
        }
        return null;
    }

    public function remove($name) {
        if ($this->has($name)) {
            unset($this->filters[$name]);
        }
    }

    public function isEmpty() {
        return (0 == sizeof($this->filters));
    }

    public function __call($name, $arguments) {
        if ($this->has($name)) {
            return $this->get($name);
        }
        return null;
    }

    public function toQueryString($excludeFilters = [], $excludeFeatures = []) {
        $getParams = [];
        $routeParams = [];
        foreach ($this->filters as $name => $filter) {
            if (!in_array($name, $excludeFilters)) {
                if ($filter->routeParam()) {
                    $routeParams[$filter->priority()] = $filter->toQueryString();
                } elseif ($filter instanceof Features) {
                    $getParams[] = $filter->toQueryString($excludeFeatures);
                } else {
                    $getParams[] = $filter->toQueryString();
                }
            }
        }
        ksort($routeParams);
        $url = '';
        foreach ($routeParams as $routeParam) {
            $url .= $routeParam;
        }
        if (($this->has('brands')) && (count($this->get('brands')->enumIds()) == 1)) {
            if ((!$this->has('limit'))) {
                $url.='/?';
            } else {
                $url.="?";
            }
        } else {
            $url .= '?';
        }

        if (count($getParams) > 0) {
            $url .= implode('&', $getParams);
        }
        return $url;
    }

    public function copy() {
        $clone = new SelectedFilterSet();
        foreach ($this->filters as $name => $filter) {
            $clone->add($name, $filter->copy());
        }
        return $clone;
    }

    public function hasAnyFilter($excludeFilters = []) {
        $filters = array_filter(array_keys($this->filters), function($filter) use ($excludeFilters) {
                    if (in_array($filter, $excludeFilters)) {
                        return false;
                    }
                    return true;
                });
        return (count($filters) > 0);
    }

}

?>