<?php

namespace SoloCatalog\Entity\Filters\SelectedFilter;

class Features extends AbstractFilter implements SelectedFilterInterface {

    private $values = [];

    public static function createFromQuery($query) {
        $obj = null;
        if (isset($query['f'])) {
            $obj = new Features();
            $groups = explode(';', $query['f']);
            foreach ($groups as $group) {
                $params = explode(':', $group);
                if (2 == sizeof($params)) {
                    $type = intval($params[0]);
                    $values = explode(',', $params[1]);
                    if (!empty($type) && (0 < sizeof($values))) {
                        $obj->addFeature($type, $values);
                    }
                }
            }
        }
        if ((isset($query['rmin']) && (isset($query['rmax'])))) {
            if ($obj == null)
                $obj = new Features();
            foreach ($query['rmin'] as $key => $value) {
                $obj->addFeature($key, $value . '_' . $query['rmax'][$key]);
            }
        }
        return $obj;
    }

    public function toQueryString($excludeFeatures = []) {
        $params = $this->toQueryParam($excludeFeatures);
        $queryString = '';
        $queryArray = [];
        if (null !== $params) {
            foreach ($params as $key => $param) {
                $queryArray[] = $key . '=' . $param;
            }
            $queryString = implode('&', $queryArray);
        }
        return $queryString;
    }

    public function toQueryParam($excludeFeatures = []) {
        $features = [];
        $featuresParams = null;
        foreach ($this->values as $type => $values) {
            if (!in_array($type, $excludeFeatures)) {
                if ((count($values) == 1) && (count(explode("_", current($values))) == 2)) {
                    $valArr = explode("_", current($values));
                    $featuresParams['rmin[' . $type . ']'] = $valArr[0];
                    $featuresParams['rmax[' . $type . ']'] = $valArr[1];
                } else {
                    $features[] = $type . ':' . implode(',', $values);
                }
            }
        }
        if (0 < sizeof($features)) {
            $featuresParams['f'] = implode(';', $features);
        }
        return $featuresParams;
    }

    public function addFeature($type, $value) {
        if (!isset($this->values[$type])) {
            $this->values[$type] = [];
        }
        if (is_array($value)) {
            foreach ($value as $item) {
                $this->values[$type][$item] = $item;
            }
        } else {
            $this->values[$type][$value] = $value;
        }
    }

    public function removeFeature($type, $value) {
        if (isset($this->values[$type])) {
            foreach ($this->values[$type] as $index => $item) {
                if ($item == $value) {
                    unset($this->values[$type][$index]);
                    if (0 == sizeof($this->values[$type])) {
                        unset($this->values[$type]);
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public function isEmpty() {
        return (0 == sizeof($this->values));
    }

    public function enumFeatures() {
        return $this->values;
    }

    public function isFilterSelect($filterId) {
        return key_exists($filterId, $this->values);
    }

    public function getFilterById($filterId) {
        return $this->values[$filterId];
    }

    public function enumFeatureValues() {
        $valueIds = [];
        foreach ($this->values as $value) {
            foreach ($value as $valueId) {
                $valueIds[] = $valueId;
            }
        }
        return $valueIds;
    }

    public function copy() {
        $clone = new Features();
        foreach ($this->values as $type => $value) {
            $clone->addFeature($type, $value);
        }
        return $clone;
    }

}

?>