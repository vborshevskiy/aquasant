<?php

namespace SoloCatalog\Entity\Filters;

class FilterValue {

    private $id = -1;
    private $title = '';
    private $label = '';
    private $image;
    private $goods = [];
    private $additionalValues = [];
    private $selected = false;
    private $filter = null;
    private $cachedGoodsCount = null;

    public function __construct($id = null, $title = null, $label = null, $image = null) {
        if (null !== $id)
            $this->id = $id;
        if (null !== $title)
            $this->title = $title;
        if (null !== $label)
            $this->label = $label;

        $this->image = $image;
    }

    /**
     *
     * @return the $id
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @param number $id        	
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     *
     * @return the $title
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     *
     * @param string $title        	
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     *
     * @return the $label
     */
    public function getLabel() {
        return $this->label;
    }

    /**
     * @return null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param null $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     *
     * @param string $label        	
     */
    public function setLabel($label) {
        $this->label = $label;
    }

    public function addGood(FilterGood $good) {
        $good->linkFilterValue($this);
        $this->goods[$good->getId()] = $good;
    }

    public function countGoods() {
        if (null !== $this->cachedGoodsCount) {
            return $this->cachedGoodsCount;
        }

        $count = 0;
        if ($this->getFilter()->getFilterSet()->hasSelectedFilters()) { // has any selected filter
            if ($this->getFilter()->hasSelectedValues()) { // this filter has selection
                if (1 == $this->getFilter()->getFilterSet()->countSelectedFilters()) {
                    $count = sizeof($this->goods);
                } else {
                    foreach ($this->goods as $good) {
                        if ($this->getFilter()->getFilterSet()->canUseGood($good, $this->getFilter()->getId())) {
                            $count++;
                        }
                    }
                }
            } else {
                foreach ($this->goods as $good) {
                    if ($this->getFilter()->getFilterSet()->canUseGood($good)) {
                        $count++;
                    }
                }
            }
        } else { // filter set has no selection
            $count = sizeof($this->goods);
        }
        return $count;
    }

    public function enumGoods() {
        $goods = [];
        if ($this->getFilter()->getFilterSet()->hasSelectedFilters()) { // has any selected filter
            if ($this->getFilter()->hasSelectedValues()) { // this filter has selection
                if (1 == $this->getFilter()->getFilterSet()->countSelectedFilters()) {
                    $goods = $this->goods;
                } else {
                    foreach ($this->goods as $good) {
                        if ($this->getFilter()->getFilterSet()->canUseGood($good, $this->getFilter()->getId())) {
                            $goods[$good->getId()] = $good;
                        }
                    }
                }
            } else {
                foreach ($this->goods as $good) {
                    if ($this->getFilter()->getFilterSet()->canUseGood($good)) {
                        $goods[$good->getId()] = $good;
                    }
                }
            }
        } else { // filter set has no selection
            $goods = $this->goods;
        }
        return $goods;
    }

    public function setFilter(Filter $filter) {
        $this->filter = $filter;
    }

    public function getFilter() {
        return $this->filter;
    }

    public function additional($name, $value = null) {
        if (null !== $value) {
            $this->additionalValues[$name] = $value;
            return $this->additionalValues[$name];
        } else {
            if (array_key_exists($name, $this->additionalValues)) {
                return $this->additionalValues[$name];
            }
            return null;
        }
    }

    public function select() {
        $this->selected = true;
    }

    public function isSelected() {
        return $this->selected;
    }

    public function resetCache() {
        $this->cachedGoodsCount = null;
    }

}

?>