<?php

namespace SoloCatalog\Entity\Filters;

class Filter implements \IteratorAggregate {

    private $id = -1;
    private $title = '';
    private $type = -1;
    private $main = 0;
    private $values = [];
    private $filterSet = null;
    private $min;
    private $unit = "";
    private $max;
    private $step;
    private $selectRange = [];
    private $urlUnsetFilter;
    private $isImageFilter = false;
    private $group;

    public function __construct($id = null, $title = null, $type = null) {
        if (null !== $id)
            $this->id = $id;
        if (null !== $title)
            $this->title = $title;
        if (null !== $type)
            $this->type = $type;
    }

    public function getIterator() {
        return new \ArrayIterator($this->values);
    }

    /**
     *
     * @return the $id
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param mixed $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }


    /**
     *
     * @return the $name
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     *
     * @return the $type
     */
    public function getType() {
        return $this->type;
    }

    /**
     *
     * @return the $main
     */

    /**
     *
     * @return the $type
     */
    public function getUrlSetFilter() {
        return $this->urlSetFilter;
    }

    /**
     *
     * @return the $type
     */
    public function getUrlUnsetFilter() {
        return $this->urlUnsetFilter;
    }

    /**
     *
     * @param field_type $id        	
     */
    public function setUrlSetFilter($url) {
        $this->urlSetFilter = $url;
    }

    public function setUrlUnsetFilter($url) {
        $this->urlUnsetFilter = $url;
    }

    public function getMain() {
        return $this->main;
    }

    /**
     *
     * @param field_type $main        	
     */
    public function setMain($main) {
        $this->main = $main;
    }

    /**
     *
     * @param field_type $id        	
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     *
     * @param field_type $title        	
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     *
     * @param field_type $type        	
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isImageFilter()
    {
        return $this->isImageFilter;
    }

    /**
     * @param bool $isImageFilter
     */
    public function setIsImageFilter($isImageFilter)
    {
        $this->isImageFilter = $isImageFilter;
    }

    public function addValue(FilterValue $value) {
        $value->setFilter($this);
        $this->values[$value->getId()] = $value;

        if ($value->getImage()) {
            $this->setIsImageFilter(true);
        }
    }

    public function removeValue($valueId) {
        if (array_key_exists($valueId, $this->values)) {
            unset($this->values[$valueId]);
            return true;
        }
        return false;
    }

    public function getValue($valueId) {
        if (array_key_exists($valueId, $this->values)) {
            return $this->values[$valueId];
        }
        return null;
    }

    public function setFilterSet(FilterSet $filterSet) {
        $this->filterSet = $filterSet;
    }

    public function getFilterSet() {
        return $this->filterSet;
    }

    /* Enum methods */

    public function enumValues() {
        return $this->values;
    }

    public function enumNonEmptyValues() {
        $values = [];
        foreach ($this->values as $value) {
            if (0 < $value->countGoods()) {
                $values[$value->getId()] = $value;
            }
        }
        return $values;
    }

    public function enumEmptySelectedValues() {
        $values = [];
        foreach ($this->values as $value) {
            if ($value->isSelected() && $value->countGoods() == 0) {
                $values[$value->getId()] = $value;
            }
        }
        return $values;
    }

    public function enumSelectedValues() {
        $values = [];
        foreach ($this->values as $value) {
            if ($value->isSelected()) {
                $values[$value->getId()] = $value;
            }
        }
        return $values;
    }

    public function enumNonEmptySelectedValues() {
        $values = [];
        foreach ($this->values as $value) {
            if ($value->isSelected() && (0 < $value->countGoods())) {
                $values[$value->getId()] = $value;
            }
        }
        return $values;
    }

    /* Count methods */

    public function countValues() {
        return sizeof($this->values);
    }

    public function countSelectedValues() {
        $count = 0;
        foreach ($this->values as $value) {
            if ($value->isSelected()) {
                $count++;
            }
        }
        return $count;
    }

    public function countNonEmptySelectedValues() {
        $count = 0;
        foreach ($this->values as $value) {
            if ($value->isSelected() && (0 < $value->countGoods())) {
                $count++;
            }
        }
        return $count;
    }

    /* Checkers */

    public function hasSelectedValues() {
        foreach ($this->values as $value) {
            if ($value->isSelected())
                return true;
        }
        return false;
    }

    public function hasNonEmptySelectedValues() {
        foreach ($this->values as $value) {
            if ($value->isSelected() && (0 < $value->countGoods()))
                return true;
        }
        return false;
    }

    public function isEmpty() {
        foreach ($this->values as $value) {
            if (0 < $value->countGoods())
                return false;
        }
        return true;
    }

    public function sortValues($f1, $f2) {
        if ($f1->countGoods() < $f2->countGoods())
            return 1;
        elseif ($f1->countGoods() > $f2->countGoods())
            return -1;
        else
            return 0;
    }

    public function sortValuesByCountGoods() {
        uasort($this->values, array($this, 'sortValues'));
    }
    
    public function sortValuesByDeliveryDate() {
        uasort($this->values, function ($f1, $f2) {
            if ($f1->getId() > $f2->getId()) {
                return 1;
            } elseif ($f1->getId() < $f2->getId()) {
                return -1;
            } else {
                return 0;
            }
        });
    }

    public function setMinValue() {
        if ($this->enumNonEmptyValues() != null) {
            $this->min = round(min(array_keys($this->enumNonEmptyValues())), 1);
        }
    }

    public function setMaxValue() {
        if ($this->enumNonEmptyValues() != null) {
            $this->max = round(max(array_keys($this->enumNonEmptyValues())), 1);
        }
    }

    public function setStepValue() {
        $step = ($this->max - $this->min) / 28;
        if ($step < 0.1) {
            $this->step = 0.1;
        } elseif ($step < 1) {
            $this->step = round($step, 1);
        } elseif ($step > 1000) {
            $this->step = 1000;
        } else {
            $this->step = ceil($step);
        }
    }

    public function getMinValue() {
        return $this->min;
    }

    public function getMaxValue() {
        
        return $this->max;
    }

    public function getStepValue() {
        return $this->step;
    }

    public function setUnit($unit) {
        $this->unit = $unit;
    }

    public function getUnit() {
        return $this->unit;
    }

    public function getSelectRange() {
        if ($this->selectRange) {
            return $this->selectRange[0] . ',' . $this->selectRange[1];
        } else {
            return $this->getMinValue() . ',' . $this->getMaxValue();
        }
    }

    public function getSelectRangeMax() {
        if ($this->selectRange) {
            return $this->selectRange[1];
        } else {
            return $this->getMaxValue();
        }
    }

    public function getSelectRangeMin() {
        if ($this->selectRange) {
            return $this->selectRange[0];
        } else {
            return $this->getMinValue();
        }
    }

    public function setSelectRange($min, $max) {
        $this->selectRange = array($min, $max);
    }

}

?>