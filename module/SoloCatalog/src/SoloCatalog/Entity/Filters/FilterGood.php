<?php

namespace SoloCatalog\Entity\Filters;

class FilterGood {

	private $id;

	private $filterValues = [];

	public function __construct($id = null) {
		if (null !== $id) $this->id = $id;
	}

	/**
	 *
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param field_type $id        	
	 */
	public function setId($id) {
		$this->id = $id;
	}

	public function linkFilterValue(FilterValue $item) {
		$this->filterValues[$item->getId()] = $item;
	}

	public function hasSelectedValue() {
		foreach ($this->filterValues as $value) {
			if ($value->isSelected()) {
				return true;
			}
		}
		return false;
	}

	public function hasAnyFilterValue() {         
		$values = [];
		if ((0 < func_num_args()) && is_array(func_get_arg(0))) {
			$values = func_get_arg(0);
		} else {
			$values = func_get_args();
		}             
		foreach ($values as $value) {                      
			if (array_key_exists($value, $this->filterValues)) return true;
		}
		return false;
	}

	public function hasSelectedValueExcept() {
		$values = [];
		if ((0 < func_num_args()) && is_array(func_get_arg(0))) {
			$values = func_get_arg(0);
		} else {
			$values = func_get_args();
		}
		foreach ($this->filterValues as $value) {
			if ($value->isSelected() && !in_array($value->getId(), $values)) {
				return true;
			}
		}
		return false;
	}

	public function enumFilterValueIds() {
		return array_keys($this->filterValues);
	}

	public function hasSelectedValues($valueIds) {
		foreach ($valueIds as $valueId) {
			if (!array_key_exists($valueId, $this->filterValues)) return false;
		}
		return true;
	}
    
    public function getFilterValues() {
        return $this->filterValues;
    }

}

?>