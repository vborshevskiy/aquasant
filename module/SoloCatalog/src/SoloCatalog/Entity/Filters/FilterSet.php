<?php

namespace SoloCatalog\Entity\Filters;

class FilterSet implements \IteratorAggregate {

    private $filters = [];
    private $goods = [];
    private $selectedFilters = [];

    public function getIterator() {
        return new \ArrayIterator($this->filters);
    }

    public function addFilter(Filter $filter) {
        $filter->setFilterSet($this);
        $this->filters[$filter->getId()] = $filter;
    }

    public function getFilter($filterId) {
        if (array_key_exists($filterId, $this->filters)) {
            return $this->filters[$filterId];
        }
        return null;
    }

    public function addGood($goodId) {
        if (!array_key_exists($goodId, $this->goods)) {
            $this->goods[$goodId] = new FilterGood($goodId);
        }
        return $this->goods[$goodId];
    }

    public function selectValue($filterId, $valueId) {
        $filter = $this->getFilter($filterId);
        if (null !== $filter) {
            if ($filter->getType() == 1) {                
                $range = explode('_', $valueId);
                if (sizeof($range) == 2) {
                    $min = $range[0];
                    $max = $range[1];
                    $filter->setSelectRange($min, $max);
                    foreach ($filter->enumValues() as $value) {
                        if (((float) $value->getId() >= (float) $min) && ((float) $value->getId() <= (float) $max)) {
                            $value->select();
                            if (!isset($this->selectedFilters[$filter->getId()])) {
                                $this->selectedFilters[$filter->getId()] = array(
                                    'filter' => $filter,
                                    'values' => []
                                );
                            }
                            $this->selectedFilters[$filter->getId()]['values'][$value->getId()] = $value;
                        }
                    }
                }
            } else {
                $value = $filter->getValue($valueId);

                if (null !== $value) {
                    $value->select();
                    if (!isset($this->selectedFilters[$filter->getId()])) {
                        $this->selectedFilters[$filter->getId()] = array(
                            'filter' => $filter,
                            'values' => []
                        );
                    }
                    $this->selectedFilters[$filter->getId()]['values'][$value->getId()] = $value;
                    $this->resetCache();
                }
            }
        }
    }
    
    /**
     * 
     * @param integer $filterId
     * @param integer $minValue
     * @param integer $maxValue
     */
    public function selectRange($filterId, $minValue, $maxValue) {
    	$this->selectValue($filterId, sprintf('%d_%d', $minValue, $maxValue));
    }

    /* Enum methods */

    public function enumSelectedFilterValues() {
        $values = [];
        foreach ($this->selectedFilters as $filter) {
            foreach ($filter['values'] as $value) {
                $values[$value->getId()] = $value;
            }
        }
        return $values;
    }

    public function enumSelectedGoodIds() {
        $criterion = [];
        foreach ($this->enumSelectedFilterValues() as $filterValue) {
            if (!isset($criterion[$filterValue->getFilter()->getId()])) {
                $criterion[$filterValue->getFilter()->getId()] = [];
            }
            $criterion[$filterValue->getFilter()->getId()][] = $filterValue->getId();
        }
        $goodIds = [];
        foreach ($this->goods as $good) {
            $exists = true;
            foreach ($criterion as $values) {
                if (!$good->hasAnyFilterValue($values)) {
                    $exists = false;
                }
            }
            if ($exists) {
                $goodIds[] = $good->getId();
            }
        }
        return $goodIds;
    }

    public function enumGoodIds() {
        return array_keys($this->goods);
    }

    /* Counters */

    public function countSelectedFilters() {
        return sizeof($this->selectedFilters);
    }

    /* Checkers */

    public function hasSelectedFilters() {
        return (0 < sizeof($this->selectedFilters));
    }

    public function canUseGood(FilterGood $good, $excludeFilterId = null) {
        foreach ($this->selectedFilters as $filterId => $filter) {
            if ((null === $excludeFilterId) || ((null !== $excludeFilterId) && ($filterId != $excludeFilterId))) {
                $valueIds = array_keys($filter['values']);
                if (!$good->hasAnyFilterValue($valueIds)) {
                    return false;
                }
            }
        }
        return true;
    }

    public function getGood($goodId) {
        if ($this->hasGood($goodId)) {
            return $this->goods[$goodId];
        }
        return null;
    }

    public function hasGood($goodId) {
        return array_key_exists($goodId, $this->goods);
    }

    private function resetCache() {
        foreach ($this->filters as $filter) {
            foreach ($filter as $value) {
                $value->resetCache();
            }
        }
    }

    public function getImageFilters() {
        $imageFilters = [];
        /** @var Filter $filter */
        foreach ($this as $filter) {
            if ($filter->isImageFilter()) {
                $imageFilters[] = $filter;
            }
        }

        return $imageFilters;
    }

    public function getSimpleFilters() {
        $simpleFilters = [];
        /** @var Filter $filter */
        foreach ($this as $filter) {
            if (!$filter->isImageFilter()) {
                $simpleFilters[] = $filter;
            }
        }

        return $simpleFilters;
    }
}

?>