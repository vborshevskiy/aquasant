<?php

namespace SoloCatalog\Entity\Goods;

class FilterOptionFeature {

	private $id = -1;

	private $valueId = -1;

	public function __construct($id = null, $valueId = null) {
		if (null !== $id) $this->id = $id;
		if (null !== $valueId) $this->valueId = $valueId;
	}

	/**
	 *
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param string $id        	
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 *
	 * @return the $valueId
	 */
	public function getValueId() {
		return $this->valueId;
	}

	/**
	 *
	 * @param string $valueId        	
	 */
	public function setValueId($valueId) {
		$this->valueId = $valueId;
	}

}

?>