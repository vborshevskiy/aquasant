<?php

namespace SoloCatalog\Entity\Goods;

class FilterGood {

	private $id;

	private $name;

	private $url;

    private $avatar;

	private $prices = [];

	private $warehouses = [];

	private $additionalValues = [];

	private $isPackage = false;

	private $categoryName;

	private $brandName;

	private $onlyInShowroom = false;

	private $showRoomData = [];

	public function __construct($id = null, $name = null) {
		if (null !== $id) $this->id = $id;
		if (null !== $name) $this->name = $name;
	}

	/**
	 *
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param field_type $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 *
	 * @return the $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 *
	 * @param field_type $name
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 *
	 * @return the $name
	 */
	public function getAvatar() {
		return $this->avatar;
	}

	/**
	 *
	 * @param field_type $avatar
	 */
	public function setAvatar($avatar) {
		$this->avatar = $avatar;
	}

	/**
	 *
	 * @return the $url
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 *
	 * @param field_type $url
	 */
	public function setUrl($url) {
		$this->url = $url;
	}

	public function addPrice($type, $value) {
		$this->prices[$type] = $value;
	}

	public function getPrice($type) {
		if (array_key_exists($type, $this->prices)) {
			return $this->prices[$type];
		}
		return null;
	}

	public function addWarehouse($warehouseId, $quantity, $suborderQuantity) {
		$this->warehouses[$warehouseId] = array(
			'quantity' => $quantity,
			'suborderQuantity' => $suborderQuantity
		);
	}

	public function getWarehouseQuantity($warehouseId) {
		if (array_key_exists($warehouseId, $this->warehouses)) {
			return $this->warehouses[$warehouseId]['quantity'];
		}
		return null;
	}

	public function getWarehouseSuborderQuantity($warehouseId) {
		if (array_key_exists($warehouseId, $this->warehouses)) {
			return $this->warehouses[$warehouseId]['suborderQuantity'];
		}
		return null;
	}

	public function additional($name, $value = null) {
		if (null !== $value) {
			$this->additionalValues[$name] = $value;
			return $this->additionalValues[$name];
		} else {
			if (array_key_exists($name, $this->additionalValues)) {
				return $this->additionalValues[$name];
			}
			return null;
		}
	}

	public function isPackage($value = null) {
		if (null !== $value) {
			$this->isPackage = $value;
		} else {
			return $this->isPackage;
		}
	}
	/**
	 * @return field_type
	 */
	public function getCategoryName() {
		return $this->categoryName;
	}


	/**
	 * @param field_type $categoryName
	 */
	public function setCategoryName($categoryName) {
		$this->categoryName = $categoryName;
		return $this;
	}
	/**
	 * @return field_type
	 */
	public function getBrandName() {
		return $this->brandName;
	}


	/**
	 * @param field_type $brandName
	 */
	public function setBrandName($brandName) {
		$this->brandName = $brandName;
		return $this;
	}

	/**
	 *
	 * @param boolean $value
	 * @return \SoloCatalog\Entity\Goods\FilterGood|boolean
	 */
	public function onlyInShowroom($value = null) {
		if (null !== $value) {
			$this->onlyInShowroom = $value;
			return $this;
		} else {
			return $this->onlyInShowroom;
		}
	}

	/**
	 * @return array $showRoomData
	 */
	public function getShowRoomData() {
		return $this->showRoomData;
	}


	/**
	 * @param array $showRoomData
	 */
	public function setShowRoomData(array $showRoomData) {
		$this->showRoomData = $showRoomData;
		return $this;
	}

}

?>