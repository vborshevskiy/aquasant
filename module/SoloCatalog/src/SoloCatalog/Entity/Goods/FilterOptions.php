<?php

namespace SoloCatalog\Entity\Goods;

class FilterOptions {

	private $brandIds = [];

	private $warehouseIds = [];

	private $priceRange = null;

	private $featureFilters = [];

	private $sorting = null;

	private $limitation = null;

	private $withSuborder = false;

	private $goodIds = [];

	/**
	 *
	 * @return the $brandIds
	 */
	public function getBrandIds() {
		return $this->brandIds;
	}

	public function addBrandId() {
		$ids = [];
		if ((0 < func_num_args()) && is_array(func_get_arg(0))) {
			$ids = func_get_arg(0);
		} else {
			$ids = func_get_args();
		}
		foreach ($ids as $id) {
			$this->brandIds[] = $id;
		}
	}

	public function hasBrands() {
		return (0 < sizeof($this->brandIds));
	}

	/**
	 *
	 * @return the $warehouseIds
	 */
	public function getWarehouseIds() {
		return $this->warehouseIds;
	}

	public function addWarehouseId() {
		$ids = [];
		if ((0 < func_num_args()) && is_array(func_get_arg(0))) {
			$ids = func_get_arg(0);
		} else {
			$ids = func_get_args();
		}
		foreach ($ids as $id) {
			$this->warehouseIds[] = $id;
		}
	}

	public function hasWarehouses() {
		return (0 < sizeof($this->warehouseIds));
	}

	/**
	 *
	 * @return the $priceRange
	 */
	public function getPriceRange() {            
		return $this->priceRange;
	}
        /**
	 *
	 * @return the $priceRange
	 */
	public function getPriceCategory() {            
		return $this->priceCategory;
	}

	/**
	 *
	 * @param field_type $priceRange        	
	 */
	public function setPriceRange($from, $to, $category, $zone) {
		$this->priceRange = array(
			'from' => $from,
			'to' => $to,
            'category' => $category,
            'zone' => $zone,
		);
	}

	public function hasPriceRange() {           
		return (null !== $this->priceRange);
	}

	/**
	 *
	 * @return the $featureFilters
	 */
	public function getFeatureFilters() {
		return $this->featureFilters;
	}

	public function addFeatureFilter() {
		$items = [];
		if ((0 < func_num_args()) && is_array(func_get_arg(0))) {
			$items = func_get_arg(0);
		} else {
			$items = func_get_args();
		}
		foreach ($items as $item) {
			$this->featureFilters[] = $item;
		}
	}

	public function hasFeatures() {
		return (0 < sizeof($this->featureFilters));
	}

	/**
	 *
	 * @return the $sorting
	 */
	public function getSorting($param = null) {
		if ((null !== $param) && array_key_exists($param, $this->sorting)) {
			return $this->sorting[$param];
		}
		return $this->sorting;
	}

	/**
	 *
	 * @param field_type $sorting        	
	 */
	public function setSorting($column, $direction, $param = null) {
		$this->sorting = array(
			'column' => $column,
			'direction' => $direction,
			'param' => $param 
		);
	}

	public function hasSorting() {
		return (null !== $this->sorting);
	}

	/**
	 *
	 * @return the $limitation
	 */
	public function getLimitation($param = null) {
		$this->correctLimitation();
		if ((null !== $param) && (null !== $this->limitation) && array_key_exists($param, $this->limitation)) {
			return $this->limitation[$param];
		}
		return $this->limitation;
	}

	private function correctLimitation() {
		if (null !== $this->limitation) {
			if ($this->limitation['currentPage'] > ceil(sizeof($this->goodIds) / $this->limitation['perPage'])) {
				$this->limitation['currentPage'] = 1;
			}
		}
	}

	/**
	 *
	 * @param field_type $limitation        	
	 */
	public function setLimitation($perPage, $currentPage = 1) {
		$this->limitation = array(
			'perPage' => $perPage,
			'currentPage' => $currentPage 
		);
	}

	public function hasLimitation() {
		return (null !== $this->limitation);
	}

	public function setWithSuborder($value) {
		$this->withSuborder = $value;
	}

	public function getWithSuborder() {
		return $this->withSuborder;
	}

	public function addGoodId($goodId) {
		$this->goodIds[] = $goodId;
	}

	public function enumGoodIds() {
		return $this->goodIds;
	}

	public function hasGoods() {
		return (0 < sizeof($this->goodIds));
	}

	public function getPriceColumn() {
		return $this->priceColumn;
	}

	public function setPriceColumn($priceColumnNumber) {
		$columns = [
			1 => 'retail',
			2 => 'small',
			3 => 'medium' 
		];
		if (isset($columns[$priceColumnNumber])) {
			return $columns[$priceColumnNumber];
		} else {
			return $columns[1];
		}
	}

}

?>