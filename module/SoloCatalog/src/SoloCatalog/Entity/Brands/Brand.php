<?php

namespace SoloCatalog\Entity\Brands;

class Brand {

    /**
     *
     * @var integer
     */
    private $id = 0;

    /**
     *
     * @var string
     */
    private $name = '';
    
    /**
     *
     * @var string
     */
    private $uid = '';

    /**
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @param integer $id        	
     * @throws \InvalidArgumentException
     * @return \SoloCatalog\Entity\Brands\Brand
     */
    public function setId($id) {
        if (!is_integer($id)) {
            throw new \InvalidArgumentException('Id must be integer');
        }
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     *
     * @param string $name        	
     * @return \SoloCatalog\Entity\Brands\Brand
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    
    /**
     *
     * @return string
     */
    public function getUid() {
        return $this->uid;
    }

    /**
     *
     * @param string $uid        	
     * @return \SoloCatalog\Entity\Brands\Brand
     */
    public function setUid($uid) {
        $this->uid = $uid;
        return $this;
    }

}

?>