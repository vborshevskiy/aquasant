<?php

namespace SoloCatalog\Entity\Brands;

use Solo\Collection\Collection;

class BrandCollection extends Collection {

    /**
     *
     * @param Brand $brand        	
     * @return Brand
     */
    public function add($brand, $key = null) {
        if (!($brand instanceof Brand)) {
            throw new \RuntimeException('Brand must be an instace of Brand');
        }
        $key = (is_null($key) ? $brand->getId() : $key);
        parent::add($brand, $key);
        return $brand;
    }

}

?>