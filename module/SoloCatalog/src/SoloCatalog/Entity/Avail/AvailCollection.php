<?php

namespace SoloCatalog\Entity\Avail;

use Solo\Collection\Collection;
use SoloCatalog\Service\Helper\DateHelper;
use SoloOrder\Entity\Order\GoodsCollection;

class AvailCollection extends Collection {

    /**
     *
     * @param Avail $avail        	
     * @return Avail
     */
    public function add($avail, $key = null) {
        if (!($avail instanceof Avail)) {
            throw new \RuntimeException('Avail must be an instace of Avail');
        }
        $key = (is_null($key) ? $avail->getOfficeId() : $key);
        parent::add($avail, $key);
        return $avail;
    }

    /*
     * @return integer
     */

    public function getMaxTotalQuantity() {
        $avails = [];
        foreach ($this->toArray() as $avail) {
            $avails[] = $avail->getTotalQuantity();
        }
        return max($avails);
    }

    /*
     * @return integer
     */

    public function getTotalAvail() {
        $totalAvail = 0;
        foreach ($this->toArray() as $avail) {
            $totalAvail += $avail->getAvail();
        }
        return $totalAvail;
    }
    
    /*
     * @return integer
     */

    public function getAvailByGoodId($goodId) {
        $totalAvail = 0;
        foreach ($this->toArray() as $avail) {
            if ($goodId === $avail->getGoodId()) {
                $totalAvail += $avail->getAvail();
            }
        }
        return $totalAvail;
    }

    /*
     * @return integer
     */

    public function getTotalSuborderByTwin() {
        $totalSuborder = 0;
        foreach ($this->toArray() as $avail) {
            if ($avail->isTwin() && !is_null($avail->getArrivalDate()) && DateHelper::isFutureDate($avail->getArrivalDate())) {
                $totalSuborder += $avail->getSuborder();
            }
        }
        return $totalSuborder;
    }
    
    /*
     * @return integer
     */

    public function getSuborderByTwinByGoodId($goodId) {
        $totalSuborder = 0;
        foreach ($this->toArray() as $avail) {
            if ($avail->isTwin() && !is_null($avail->getArrivalDate()) && DateHelper::isFutureDate($avail->getArrivalDate()) && $avail->getGoodId() === $goodId) {
                $totalSuborder += $avail->getSuborder();
            }
        }
        return $totalSuborder;
    }

    /*
     * @return integer
     */

    public function getTotalSuborder() {
        $totalSuborder = 0;
        foreach ($this->toArray() as $avail) {
            if (!is_null($avail->getArrivalDate()) && DateHelper::isFutureDate($avail->getArrivalDate())) {
                $totalSuborder += $avail->getSuborder();
            }
        }
        return $totalSuborder;
    }
    
    /*
     * @return integer
     */

    public function getSuborderByGoodId($goodId) {
        $totalSuborder = 0;
        foreach ($this->toArray() as $avail) {
            if (!is_null($avail->getArrivalDate()) && DateHelper::isFutureDate($avail->getArrivalDate()) && $avail->getGoodId() === $goodId) {
                $totalSuborder += $avail->getSuborder();
            }
        }
        return $totalSuborder;
    }

    /*
     * @param integer $count
     * @return \DateTime
     */

    public function getGoodArrivalDate($count) {
        $totalAvail = $this->getTotalAvail();
        $totalSuborder = $this->getTotalSuborder();
        if ($totalAvail + $totalSuborder < $count) {
            return null;
        }
        $suborders = [];
        foreach ($this->toArray() as $avail) {
            if (!is_null($avail->getArrivalDate()) && DateHelper::isFutureDate($avail->getArrivalDate())) {
                $suborders[$avail->getArrivalDate()->format('Y-m-d-H-i-s')] += $avail->getSuborder();
            }
        }
        ksort($suborders);
        $quantity = 0;
        while ($count > $quantity && count($suborders) > 0) {
            $date = key($suborders);
            $quantity += array_shift($suborders);
        }
        if ($count > $quantity) {
            return null;
        }
        return \DateTime::createFromFormat('Y-m-d-H-i-s', $date);
    }
    
    /*
     * @param integer $count
     * @return \DateTime
     */

    public function getGoodArrivalDateByGoodId($goodId,$count) {
        $totalAvail = $this->getAvailByGoodId($goodId);
        $totalSuborder = $this->getSuborderByGoodId($goodId);
        if ($totalAvail >= $count) {
            return false;
        }
        if ($totalAvail + $totalSuborder < $count) {
            return null;
        }
        $suborders = [];
        foreach ($this->toArray() as $avail) {
            if (!is_null($avail->getArrivalDate()) && DateHelper::isFutureDate($avail->getArrivalDate()) && $avail->getGoodId() === $goodId) {
                $suborders[$avail->getArrivalDate()->format('Y-m-d-H-i-s')] += $avail->getSuborder();
            }
        }
        ksort($suborders);
        $quantity = $this->getTotalAvail();
        while ($count > $quantity && count($suborders) > 0) {
            $date = key($suborders);
            $quantity += array_shift($suborders);
        }
        if ($count > $quantity) {
            return null;
        }
        return \DateTime::createFromFormat('Y-m-d-H-i-s', $date);
    }
    
    public function getAvailOffices(GoodsCollection $basketGoods) {
        $officeIds = [];
        $deficit = [];
        $quantities = $basketGoods->getQuantitiesByGoodIds();
        $avails = $this->getAvailsByOffices();
        foreach ($avails as $officeId => $avail) {
            $arrivaDate = [];
            foreach ($quantities as $goodId => $quantity) {
                if (!array_key_exists($goodId, $avail) || $avail[$goodId] < $quantity) {
                    $deficit[$officeId][] = $goodId;
                } else {
                    $arrivaDate[] = $this->offsetGet($goodId . '-' . $officeId)->getAvailDateByQuantity($quantity);
                }
            }
            if (!array_key_exists($officeId, $deficit) || count($deficit[$officeId]) === 0) {
                $fakeAvail = new Avail(DateHelper::getMaxDate($arrivaDate));
                $officeIds[$officeId] = $fakeAvail->getTextArrivalDate();
            }
        }
        return $officeIds;
    }
    
    public function getAvailsByOffices() {
        $result = [];
        foreach ($this->list as $avail) {
            if (!array_key_exists($avail->getOfficeId(), $result)) {
                $result[$avail->getOfficeId()] = [];
            }
            if (!array_key_exists($avail->getGoodId(), $result[$avail->getOfficeId()])) {
                $result[$avail->getOfficeId()][$avail->getGoodId()] = 0;
            }
            $result[$avail->getOfficeId()][$avail->getGoodId()] += $avail->getTotalQuantity();
        }
        return $result;
    }
    
    public function hasAvailOffice() {
        foreach ($this->list as $avail) {
            if ($avail->hasGoods()) {
                return true;
            }
        }
        return false;
    }
    
    public function getGoodAvail($goodId) {
        $avails = clone $this;
        foreach ($avails->toArray() as $avail) {
            if ($avail->getGoodId() != $goodId) {
                $avails->remove($avail);
            }
        }
        return $avails;
    }
    
    public function haveEnoughAvailGoods($goods) {
        foreach ($goods->toArray() as $good) {
            if ($this->getAvailByGoodId($good->getMarking()) < $good->getQuantity()) {
                return false;
            }
        }
        return true;
    }
    
    public function getAvailGoodOffices() {
        $offices = [];
        foreach ($this->list as $office) {
            if ($office->getAvail() > 0) {
                $offices[$office->getOfficeId()] = $office;
            }
        }
        return $offices;
    }
    
    public function getSuborderGoodOffices($excludedOfficesIds = []) {
        $offices = [];
        foreach ($this->list as $office) {
            if ($office->getSuborder() > 0 && !in_array($office->getOfficeId(), $excludedOfficesIds)) {
                $offices[$office->getOfficeId()] = $office;
            }
        }
        return $offices;
    }

}

?>