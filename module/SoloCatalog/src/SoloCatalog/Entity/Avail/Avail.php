<?php

namespace SoloCatalog\Entity\Avail;

use SoloCatalog\Service\Helper\DateHelper;

class Avail {

    /**
     *
     * @var integer
     */
    private $goodId = 0;

    /**
     *
     * @var integer
     */
    private $avail = 0;

    /**
     *
     * @var integer
     */
    private $suborder = 0;

    /**
     *
     * @var integer
     */
    private $storeId = null;

    /**
     *
     * @var integer
     */
    private $officeId = null;

    /**
     *
     * @var string
     */
    private $officeAddress = '';
    
    /**
     *
     * @var string
     */
    private $officeShortName = '';

    /**
     *
     * @var integer
     */
    private $cityId = null;

    /**
     *
     * @var \DateTime
     */
    private $arrivalDate = null;

    /**
     *
     * @var boolean
     */
    private $isTwin = false;
    
    /**
     *
     * @var string
     */
    private $officePhone = null;

    /**
     *
     * @return integer
     */
    public function __construct($arrivalDate = null) {
        if (null !== $arrivalDate)
            $this->setArrivalDate($arrivalDate);
    }

    public function getGoodId() {
        return $this->goodId;
    }

    /**
     *
     * @param integer $goodId        	
     * @throws \InvalidArgumentException
     * @return \SoloCatalog\Entity\Avail\Avail
     */
    public function setGoodId($goodId) {
        if (!is_integer($goodId)) {
            throw new \InvalidArgumentException('Good id must be integer');
        }
        $this->goodId = $goodId;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getAvail() {
        return $this->avail;
    }

    /**
     *
     * @param integer $avail        	
     * @return \SoloCatalog\Entity\Avail\Avail
     */
    public function setAvail($avail) {
        $this->avail = (int) $avail;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getSuborder() {
        return $this->suborder;
    }

    /**
     *
     * @param integer $suborder        	
     * @return \SoloCatalog\Entity\Avail\Avail
     */
    public function setSuborder($suborder) {
        $this->suborder = (int) $suborder;
        return $this;
    }
    
    /**
     *
     * @param integer $suborder        	
     * @return \SoloCatalog\Entity\Avail\Avail
     */
    public function addSuborder($suborder) {
        $this->suborder += (int) $suborder;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getStoreId() {
        return $this->avail;
    }

    /**
     *
     * @param integer $storeId        	
     * @return \SoloCatalog\Entity\Avail\Avail
     */
    public function setStoreId($storeId) {
        $this->storeId = (int) $storeId;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getOfficeId() {
        return $this->officeId;
    }

    /**
     *
     * @param integer $officeId        	
     * @return \SoloCatalog\Entity\Avail\Avail
     */
    public function setOfficeId($officeId) {
        $this->officeId = (int) $officeId;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getCityId() {
        return $this->cityId;
    }

    /**
     *
     * @param integer $cityId        	
     * @return \SoloCatalog\Entity\Avail\Avail
     */
    public function setCityId($cityId) {
        $this->cityId = (int) $cityId;
        return $this;
    }

    /**
     *
     * @param boolean|null $isTwin        	
     * @return boolean|\SoloCatalog\Entity\Avail\Avail
     */
    public function isTwin($isTwin = null) {
        if (is_null($isTwin)) {
            return $this->isTwin;
        }
        $this->isTwin = (bool) $isTwin;
        return $this;
    }

    /**
     *
     * @return \DateTime
     */
    public function getArrivalDate() {
        return $this->arrivalDate;
    }

    /**
     *
     * @param \DateTime $arrivalDate        	
     * @return \SoloCatalog\Entity\Avail\Avail
     */
    public function setArrivalDate(\DateTime $arrivalDate) {
        $this->arrivalDate = $arrivalDate;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getOfficeAddress() {
        return $this->officeAddress;
    }

    /**
     *
     * @param string $officeAddress        	
     * @return \SoloCatalog\Entity\Avail\Avail
     */
    public function setOfficeAddress($officeAddress) {
        $this->officeAddress = $officeAddress;
        return $this;
    }
    
    /**
     *
     * @return string
     */
    public function getOfficeShortName() {
        return $this->officeShortName;
    }

    /**
     *
     * @param string $officeShortName        	
     * @return \SoloCatalog\Entity\Avail\Avail
     */
    public function setOfficeShortName($officeShortName) {
        $this->officeShortName = $officeShortName;
        return $this;
    }
    
    /**
     *
     * @return string
     */
    public function getOfficePhone() {
        return $this->officePhone;
    }

    /**
     *
     * @param string $officePhone
     * @return \SoloCatalog\Entity\Avail\Avail
     */
    public function setOfficePhone($officePhone) {
        $this->officePhone = $officePhone;
        return $this;
    }
    
    /**
     *
     * @return boolean
     */
    public function hasOfficePhone() {
        return (!is_null($this->officePhone) && !empty($this->officePhone));
    }

    /**
     * @return integer
     */
    public function getTotalQuantity() {
        return $this->suborder + $this->avail;
    }

    /**
     * 
     * @return boolean
     */
    public function hasGoods() {
        return (($this->avail > 0) || ($this->suborder > 0 && !is_null($this->arrivalDate) && DateHelper::isFutureDate($this->arrivalDate)));
    }

    /**
     * 
     * @param integer $quantity
     * @return \DateTime
     */
    public function getAvailDateByQuantity($quantity) {
        if ($this->getAvail() >= $quantity) {
            return new \DateTime('now');
        }
        if ($this->getTotalQuantity() >= $quantity) {
            return $this->getArrivalDate();
        }
        return null;
    }

}