<?php

namespace SoloCatalog\Controller\Plugin;

use Solo\Mvc\Controller\Plugin\AbstractPlugin;
use SoloCatalog\Service\CategoriesService;
use SoloCatalog\Service\FiltersService;
use SoloCatalog\Service\GoodsService;
use SoloCatalog\Service\MenuService;
use SoloCatalog\Service\PricesService;
use SoloCatalog\Service\SimilarService;
use SoloCatalog\Service\CompatibleService;
use SoloCatalog\Service\SearchService;
use SoloCatalog\Service\BrandsService;
use SoloCatalog\Service\CurrenciesService;
use SoloCatalog\Service\ImageService;
use SoloCatalog\Service\SatelliteService;

class Catalog extends AbstractPlugin {

	/**
	 *
	 * @return CategoriesService
	 */
	public function categories() {
		return $this->getServiceLocator()->get('catalog_categories');
	}

	/**
	 *
	 * @return FiltersService
	 */
	public function filters() {
		return $this->getServiceLocator()->get('catalog_filters');
	}

	/**
	 *
	 * @return GoodsService
	 */
	public function goods() {
		return $this->getServiceLocator()->get('catalog_goods');
	}

	/**
	 *
	 * @return MenuService
	 */
	public function menu() {
		return $this->getServiceLocator()->get('catalog_menu');
	}

	/**
	 *
	 * @return PricesService
	 */
	public function prices() {
		return $this->getServiceLocator()->get('catalog_prices');
	}

	/**
	 *
	 * @return CompatibleService
	 */
	public function compatible() {
		return $this->getServiceLocator()->get('catalog_compatible');
	}

	/**
	 *
	 * @return SearchService
	 */
	public function search() {
		return $this->getServiceLocator()->get('catalog_search');
	}

	/**
	 *
	 * @return BrandsService
	 */
	public function brands() {
		return $this->getServiceLocator()->get('catalog_brands');
	}

	/**
	 *
	 * @return CurrenciesService
	 */
	public function currencies() {
		return $this->getServiceLocator()->get('catalog_currencies');
	}

	/**
	 *
	 * @return ImageService
	 */
	public function images() {
		return $this->getServiceLocator()->get('catalog_images');
	}

	public function files() {
		return $this->getServiceLocator()->get('catalog_files');
	}

	/**
	 *
	 * @return SatelliteService
	 */
	public function satellite() {
		return $this->getServiceLocator()->get('catalog_satellite');
	}

	/**
	 *
	 * @return \SoloCatalog\Service\SetsService
	 */
	public function sets() {
		return $this->getServiceLocator()->get('SoloCatalog\Service\SetsService');
	}

}

?>