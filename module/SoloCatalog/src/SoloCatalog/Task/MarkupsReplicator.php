<?php

namespace SoloCatalog\Task;

use SoloCatalog\Data\AllGoodsTableInterface;
use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\Service\ProvidesWebservice;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloReplication\Service\DataMapper;

class MarkupsReplicator extends AbstractReplicationTask {

    use ProvidesWebservice;

    /**
     *
     * @var AllGoodsTableInterface
     */
    protected $allGoodsTable;

    /**
     * @param AllGoodsTableInterface $allGoodsTable
     */
    public function __construct(AllGoodsTableInterface $allGoodsTable) {

        $this->allGoodsTable = $allGoodsTable;

        $this->addSwapTable('all_goods');
    }

    /**
     *
     * @see \SoloReplication\Service\TaskInterface::process()
     */
    public function process() {
        $this->processMarkups();
    }

    /**
     * Update markups
     */
    private function processMarkups() {
        $this->log->info('Begin markups');

        $rdr = new UltimaJsonListReader($this->callWebMethod('GetMarkups'));
        //$dataSet = [];
        $mapper = $this->createMurkupMapper();
        $updated = 0;
        foreach ($rdr as $row) {
            $data = $mapper->convert($row);
            if(!empty($data['Markup']) && $this->allGoodsTable->setMarkup($data['GoodID'], $data['Markup'])) $updated++;
        }

        $this->log->info('Updated = ' . $updated);
        $this->log->info('End markups');
    }

    /**
     *
     * @return \SoloReplication\Service\DataMapper
     */
    private function createMurkupMapper() {
        $mapper = new DataMapper([
            'GoodID' => '%d: ProductId',
            'Markup' => 'Markup',
        ]);
        return $mapper;
    }
}

?>