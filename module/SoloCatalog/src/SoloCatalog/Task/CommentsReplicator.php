<?php

namespace SoloCatalog\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\Service\ProvidesWebservice;
use SoloCatalog\Data\CommentsTable;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloReplication\Service\DataMapper;

class CommentsReplicator extends AbstractReplicationTask {

    use ProvidesWebservice;

    /**
     *
     * @var CommentsTable
     */
    private $commentsTable;

    /**
     *
     * @param CommentsTable $commentsTable        	
     */
    public function __construct(CommentsTable $commentsTable) {
        $this->commentsTable = $commentsTable;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SoloReplication\Service\TaskInterface::process()
     */
    public function process() {
        $this->processRealBuyers();
        $this->processDeleteComments();
    }
    
    /**
     * Delete comments
     */
    protected function processRealBuyers() {
        $this->log->info('Get real buyers comments');
        $uncheckedComments = $this->commentsTable->getUncheckedIsRealBuyersComments();
        if ($uncheckedComments->count() > 0) {
            foreach ($uncheckedComments as $uncheckedComment) {
                $rdr = new UltimaJsonListReader($this->callWebMethod('IsRealBuyer', [
                    'AgentId' => $uncheckedComment['UserID'],
                    'ArticleId' => $uncheckedComment['GoodID'],
                ]));
                $this->commentsTable->isRealBuyer($uncheckedComment['ID'], $rdr->getValue('Result'));
            }
        }
        $this->log->info('Real buyers comments setted');
    }

    /**
     *
     * @return \SoloReplication\Service\DataMapper
     */
    protected function createDeleteCommentsMapper() {
        $mapper = new DataMapper();
        $mapper->setMappings([
            'ID' => '%d: Id'
        ]);
        return $mapper;
    }

    /**
     * Delete comments
     */
    protected function processDeleteComments() {
        $this->log->info('Start delete comments');
        $mapper = $this->createDeleteCommentsMapper();
        $params = [];
        $lastUpdate = $this->lastUpdate()->get('comments', 'GetDeletedComments', $this->commentsTable->getTableGateway()->getTable());
        $this->log->info('Table:' . $this->commentsTable->getTableGateway()->getTable());
        $this->log->info('UpdatedAfter:' . $lastUpdate);
        if (!is_null($lastUpdate)) {
            $params['DeletedAfter'] = $lastUpdate;
        }
        $webServerDate = new UltimaJsonListReader($this->callWebMethod('GetNow'));
        $rdr = new UltimaJsonListReader($this->callWebMethod('GetDeletedComments', $params));
        $date = $webServerDate->getValue('Date');
        $this->lastUpdate()->set('comments', 'GetDeletedComments', $date, $this->commentsTable->getTableGateway()->getTable());

        foreach ($rdr as $row) {
            $dataSet[] = $mapper->convert($row);
            if (500 == sizeof($dataSet)) {
                $this->commentsTable->createDelete($dataSet);
                $dataSet = [];
            }
        }
        if (0 < sizeof($dataSet)) {
            $this->commentsTable->createDelete($dataSet);
        }


        $this->lastUpdate()->saveChanges();

        $this->log->info('Finish delete comments = ' . sizeof($dataSet));
    }
}
