<?php

namespace SoloCatalog\Task;

use SoloReplication\Service\AbstractReplicationTask;
use Solo\Db\QueryGateway\QueryGateway;
use Solo\DateTime\DateTime;

class ProblemProductsChecker extends AbstractReplicationTask {

	/**
	 *
	 * @see \SoloReplication\Service\TaskInterface::process()
	 */
	public function process() {
		$productIds = $this->getAvailProductIds();
		$this->log->info(sprintf('Found %d products to check', count($productIds)));
		
		$queryGateway = new QueryGateway();
		
		$report = [];
		foreach ($productIds as $productId => $options) {
			$product = $options['product'];
			$isPackage = ($product['IsPackage'] && !$product['IsVirtualPackage']);
			$isVirtualPackage = ($product['IsPackage'] && $product['IsVirtualPackage']);
			
			$errors = [];
			
			// prices
			$sql = "SELECT * FROM #goods_prices# WHERE GoodID = {$productId}";
			$rows = $queryGateway->query($sql);
			if (0 == $rows->count()) {
				$errors[] = 'no_prices';
			}
			
			// images
			$sql = "SELECT * FROM #goods_images# WHERE GoodID = {$productId}";
			$rows = $queryGateway->query($sql);
			if (0 == $rows->count()) {
				$errors[] = 'no_photos';
			}
			
			// no arrival dates / or incorrect
			/**
			 * $sql = "SELECT
			 * arg.*
			 * FROM #arrival_goods# arg
			 * WHERE
			 * (arg.ArrivalDate IS NULL || YEAR(arg.ArrivalDate) = '2037')
			 * && arg.GoodID = {$productId}";
			 * $rows = $queryGateway->query($sql);
			 * if (0 < $rows->count()) {
			 * $errors[] = 'no_arrival_date';
			 * }
			 */
			
			// no pickup dates / or null
			$sql = "SELECT
						grad.*
					FROM #good_reserve_avail_date# grad
					WHERE
						(grad.PickupDate IS NOT NULL || grad.DeliveryDate IS NOT NULL)
						&& grad.GoodID = {$productId}";
			$rows = $queryGateway->query($sql);
			if (0 == $rows->count()) {
				$errors[] = 'no_reserve_avail_date';
			}
			
			// links to categories
			$siteCategoryId = 0;
			$siteCategoryName = '';
			$sql = "SELECT
						sc.SoftCategoryID,
						sc.CategoryName
					FROM #goods_to_soft_categories# gtsc
					INNER JOIN #soft_categories# sc
						ON gtsc.SoftCategoryID = sc.SoftCategoryID
					WHERE
						gtsc.GoodID = {$productId}";
			$rows = $queryGateway->query($sql);
			if (0 == $rows->count()) {
				$errors[] = 'no_links';
			} else {
				$row = $rows->current();
				$siteCategoryId = intval($row->SoftCategoryID);
				$siteCategoryName = strval($row->CategoryName);
			}
			
			// properties
			if (!$isPackage) {
				$sql = "SELECT * FROM #goods_to_propvalues# gtp WHERE gtp.GoodID = {$productId}";
				$rows = $queryGateway->query($sql);
				if (0 == $rows->count()) {
					$errors[] = 'no_properties';
				}
			}
			
			// complect / not avail components
			if ($isPackage) {
				$sql = "SELECT
								ptg.PackageID
							FROM #goods_to_packages# gtp
								INNER JOIN #avail_goods# ag
									ON gtp.GoodID = ag.GoodID
								INNER JOIN #packages_to_groups# ptg
									ON gtp.PackageID = ptg.PackageID
									AND gtp.GroupID = ptg.GroupID
									AND ptg.IsMain = 1
    						WHERE gtp.PackageID = {$productId}";
				$rows = $queryGateway->query($sql);
				if (0 == $rows->count()) {
					$errors[] = 'no_avail_on_main_components';
				}
			}
			
			// complect / no linked components
			if ($isPackage) {
				$sql = "SELECT
								gtp.PackageID
							FROM #goods_to_packages# gtp
								INNER JOIN #all_goods# alg
									ON gtp.PackageID = alg.GoodID
									AND alg.IsPackage = 1
									AND alg.IsVirtualPackage = 0
								LEFT OUTER JOIN #avail_goods# ag
									ON gtp.GoodID = ag.GoodID
							WHERE gtp.PackageID = {$productId}
							GROUP BY gtp.PackageID
							HAVING 1 < COUNT(ag.GoodID)";
				$rows = $queryGateway->query($sql);
				if (0 == $rows->count()) {
					$errors[] = 'no_avail_on_all_components';
				}
			}
			
			$isComplectPart = false;
			if (!$isPackage) {
				$sql = "SELECT
							gtp.PackageID
						FROM #goods_to_packages# gtp
						WHERE gtp.GoodID = {$productId}";
				$rows = $queryGateway->query($sql);
				if (0 < $rows->count()) {
					$isComplectPart = true;
				}
			}
			
			if (0 < count($errors)) {
				$this->log->info(sprintf('Product %d has %d problems', $productId, count($errors)));
				
				$item = [
					'sku' => $productId,
					'name' => $product->GoodName,
					'siteCategoryId' => $siteCategoryId,
					'siteCategoryName' => $siteCategoryName,
					'brandId' => $product->BrandID,
					'propertyTemplateId' => '',
					'categoryId' => $product->CategoryID,
					'has_price' => (!in_array('no_price', $errors) ? 'есть' : 'нет'),
					'available' => ((!in_array('no_avail_on_main_components', $errors) || !in_array('no_avail_on_all_components', $errors)) ? 'есть' : 'нет'),
					'has_photo' => (!in_array('no_photos', $errors) ? 'есть' : 'нет'),
					// 'arrival_date' => in_array('no_arrival_date', $errors) ? 'отсутствует' : '',
					'reserve_avail_date' => in_array('no_reserve_avail_date', $errors) ? 'отсутствует' : '',
					'isPackage' => ($isPackage ? 'да' : 'нет'),
					'isProduct' => ((2 == $product->ViewMode) ? 'да' : 'нет'),
					'isVirtualPackage' => ($isVirtualPackage ? 'да' : 'нет'),
					'isComplectPart' => $isComplectPart ? 'да' : 'нет',
					'isEmptyArrivalDate' => 'нет',
					'hasDeliveryPrice' => 'есть'
				];
				
				$report[] = $item;
			}
		}
		
		$this->sendMailReport($report);
	}

	/**
	 *
	 * @return array
	 */
	private function getAvailProductIds() {
		$result = [];
		
		$queryGateway = new QueryGateway();
		
		// remains
		$sql = "SELECT
					ag.*,
					gr.Remains,
					gr.Reserve
				FROM #goods_remains# gr
					INNER JOIN #all_goods# ag
					ON gr.GoodID = ag.GoodID
				WHERE
					gr.Remains > 0
					AND gr.Remains > gr.Reserve";
		$rows = $queryGateway->query($sql);
		foreach ($rows as $row) {
			$remains = $row->Remains - $row->Reserve;
			if (!isset($result[$row->GoodID])) {
				$result[$row->GoodID] = [
					'quantity' => 0,
					'product' => $row 
				];
			}
			$result[$row->GoodID]['quantity'] += $remains;
		}
		
		// arrival
		$sql = "SELECT
					ag.*,
					gr.SuborderQuantity
				FROM #arrival_goods# gr
					INNER JOIN #all_goods# ag
					ON gr.GoodID = ag.GoodID
				WHERE
					gr.SuborderQuantity > 0";
		$rows = $queryGateway->query($sql);
		foreach ($rows as $row) {
			if (!isset($result[$row->GoodID])) {
				$result[$row->GoodID] = [
					'quantity' => 0,
					'product' => $row 
				];
			}
			$result[$row->GoodID]['quantity'] += $row->SuborderQuantity;
		}
		
		return $result;
	}

	/**
	 *
	 * @param array $report        	
	 */
	private function sendMailReport(array $report) {
		$mail = $this->createMail();
		//$mail->setTo('rgoryanin@it-solo.com');
		// $mail->addTo('vborshevskiy@taxasoftware.com');
		// $mail->setTo('v.detman@gmail.com');
		
		$mail->setTo('5049744@mail.ru');
		$mail->addCc('vborshevskiy@taxasoftware.com');
		
		$body = $this->createBody();
		$html = 'Сегодня все хорошо ;)';
		if (0 < count($report)) {
			$html = sprintf('Найдено <b>%d</b> товаров с проблемами<br />', count($report));
			$html .= 'из них:<ul>';
			
			$countNoPrice = array_sum(array_map(function ($row) {
				return (('нет' == $row['has_price']) ? 1 : 0);
			}, $report));
			if (0 < $countNoPrice) {
				$html .= '<li>без цен = ' . $countNoPrice . '</li>';
			}
			
			$countNoAvail = array_sum(array_map(function ($row) {
				return (('нет' == $row['available']) ? 1 : 0);
			}, $report));
			if (0 < $countNoAvail) {
				$html .= '<li>без наличия = ' . $countNoAvail . '</li>';
			}
			
			/*
			 * $countNoArrivalDates = array_sum(array_map(function ($row) { return (('отсутствует' == $row['arrival_date']) ? 1 : 0); }, $report)); if (0 < $countNoArrivalDates) { $html .= '<li>без даты поставки = ' . $countNoArrivalDates . '</li>'; }
			 */
			
			$countNoArrivalDates = array_sum(array_map(function ($row) {
				return (('отсутствует' == $row['reserve_avail_date']) ? 1 : 0);
			}, $report));
			if (0 < $countNoArrivalDates) {
				$html .= '<li>без даты поставки = ' . $countNoArrivalDates . '</li>';
			}
			
			$countNoPhoto = array_sum(array_map(function ($row) {
				return (('нет' == $row['has_photo']) ? 1 : 0);
			}, $report));
			if (0 < $countNoPhoto) {
				$html .= '<li>без фото = ' . $countNoPhoto . '</li>';
			}
			
			$countNoLinks = array_sum(array_map(function ($row) {
				return (('' == $row['siteCategoryName']) ? 1 : 0);
			}, $report));
			if (0 < $countNoLinks) {
				$html .= '<li>непривязанные к категориям = ' . $countNoLinks . '</li>';
			}
			
			$html .= '</ul>';
		}
		$body->addPart($this->createHtmlMessage($html));
		
		$subject = sprintf('Отчет по товарам не в продаже: %d позиций от %s', count($report), DateTime::now()->format('d.m.Y'));
		$mail->setSubject("=?utf-8?B?" . base64_encode($subject) . "?=");
		
		if (0 < count($report)) {
			$attachment = $this->createAttachmentFromString($this->createReportCsv($report), 'application/vnd.ms-excel', sprintf('problem_products_%s', DateTime::now()->format('d-m-Y')));
			$attachment->encoding = 'base64';
			$attachment->disposition = 'attachment';
			$attachment->type = sprintf('application/vnd.ms-excel; name="problem_products_%s.csv"', DateTime::now()->format('d-m-Y'));
			$attachment->filename = sprintf('problem_products_%s.csv', DateTime::now()->format('d-m-Y'));
			$body->addPart($attachment);
		}
		
		$mail->setBody($body);
		$this->sendMail($mail);
	}

	/**
	 *
	 * @param array $report        	
	 * @return string
	 */
	private function createReportCsv(array $report) {
		$result = '';
		
		$line = 'Артикул;Название товара;ID категории сайта;Название категории сайта;ID бренда;ID шаблона характеристик;ID категории в ерп;Цена;Наличие;Фото;Дата поставки;Комплект;Показывать как товар;Виртуальный комплект;Комплектующая;Без даты поставщика;Есть стоимость доставки';
		$line = iconv('UTF-8', 'WINDOWS-1251', $line);
		$result .= $line . PHP_EOL;
		
		foreach ($report as $row) {
			foreach ($row as $uid => $value) {
				$row[$uid] = str_replace(';', ':', $value);
			}
			$line = implode(';', array_values($row));
			$line = iconv('UTF-8', 'WINDOWS-1251', $line);
			$result .= $line . PHP_EOL;
		}
		
		return $result;
	}

	/**
	 *
	 * @return \Zend\Mail\Message
	 */
	private function createMail() {
		$mail = new \Zend\Mail\Message();
		
		$fromName = 'Santehbaza';
		$mail->addFrom('robot@'.DOMAIN_NAME, $fromName);
		
		return $mail;
	}

	/**
	 *
	 * @param string $content        	
	 * @param string $type        	
	 * @param string $id        	
	 * @return \Zend\Mime\Part
	 */
	private function createAttachmentFromString($content, $type = null, $id = null) {
		$part = new \Zend\Mime\Part($content);
		
		$part->encoding = 'base64';
		if (null !== $type) {
			$part->type = $type;
		}
		if (null !== $id) {
			$part->id = $id;
		}
		
		return $part;
	}

	/**
	 *
	 * @param
	 *        	array | scalar $parts
	 * @return \Zend\Mime\Message
	 */
	private function createBody($parts = null) {
		$body = new \Zend\Mime\Message();
		if (null !== $parts) {
			if (is_array($parts)) {
				$body->setParts($parts);
			} else {
				$body->setParts([
					$parts 
				]);
			}
		}
		return $body;
	}

	/**
	 *
	 * @param string $content        	
	 * @return \Zend\Mime\Part
	 */
	private function createHtmlMessage($content) {
		$part = new \Zend\Mime\Part($content);
		$part->type = \Zend\Mime\Mime::TYPE_HTML;
		$part->charset = 'utf-8';
		$part->encoding = \Zend\Mime\Mime::ENCODING_QUOTEDPRINTABLE;
		return $part;
	}

	/**
	 *
	 * @param \Zend\Mail\Message $mail        	
	 */
	private function sendMail(\Zend\Mail\Message $mail) {
		$transport = new \Zend\Mail\Transport\Sendmail();
		$transport->send($mail);
	}

}

?>