<?php

namespace SoloCatalog\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloReplication\Service\DataMapper;
use SoloERP\Service\ProvidesWebservice;
use SoloCatalog\Data\GoodsImagesTable;
use SoloCatalog\Data\GoodsImagesViewsTable;
use Solo\Db\QueryGateway\QueryGateway;

class GoodsImagesReplicator extends AbstractReplicationTask {
	
	use ProvidesWebservice;

	/**
	 *
	 * @var array
	 */
	protected $imageSizes = [
		'60x60',
		'260x280',
		'450x600',
		'864x1080',
		'360x400',
		'840x960' 
	];

	/**
	 *
	 * @var integer
	 */
	protected $delay = 900;

	/**
	 *
	 * @var GoodsImagesTable
	 */
	protected $goodsImagesTable;

	/**
	 *
	 * @var GoodsImagesViewsTable
	 */
	protected $goodsImagesViewsTable;

	/**
	 *
	 * @param GoodsImagesTable $goodsImagesTable        	
	 * @param GoodsImagesViewsTable $goodsImagesViewsTable        	
	 */
	public function __construct(GoodsImagesTable $goodsImagesTable, GoodsImagesViewsTable $goodsImagesViewsTable) {
		$this->goodsImagesTable = $goodsImagesTable;
		$this->goodsImagesViewsTable = $goodsImagesViewsTable;
		$this->addSwapTable('goods_images', 'goods_images_views');
		
		$this->eventName = 'repl.goods_images';
		
		$this->scriptNotRespondingTimeInMinutes = 60 * 2;
		ini_set('memory_limit', '1024M');
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \SoloReplication\Service\TaskInterface::process()
	 */
	public function process() {
		$this->processGoodsImages();
		$this->processGoodsImagesViews();
	}

	/**
	 * Initialize data mapper for goods_images table
	 *
	 * @return DataMapper
	 */
	protected function createGoodsImagesMapper() {
		$mapper = new DataMapper();
		$mapper->setMappings(
			[
				'GoodID' => '%d: ArticleId',
				'Url' => 'url',
				'ViewID ' => '%d: ViewId',
				'SortIndex' => '%d: SortIndex',
				'MiniatureUrl' => 'miniatureUrl',
				'ViewTemplateName' => 'ViewName' 
			]);
		return $mapper;
	}

	/**
	 * Fill goods currencies table
	 */
	protected function processGoodsImages() {
		$this->log->info('Insert images goods');
		$mapper = $this->createGoodsImagesMapper();
		$pRedis = $this->predisClient;
		$params = [];
		$lastUpdate = $this->lastUpdate()->get('goods_images', 'GetGoodPhotos', $this->goodsImagesTable->getTableGateway()->getTable());
		$params['UpdatedAfter'] = date('Y-m-d H:i:s', (strtotime($lastUpdate) - (60 * 30)));
		$delayDate = $this->helper()->changeJsonDate(-$this->delay, $lastUpdate);
		if (false && (empty($lastUpdate) || empty($delayDate))) {
			$this->removeSwapTable('goods_images');
			$this->log->warn('Empty update or delay date');
			return;
		}
		$this->log->info('Table:' . $this->goodsImagesTable->getTableGateway()->getTable());
		$this->log->info('UpdatedAfter:' . $lastUpdate);
		//$this->log->info('DelayDate:' . $delayDate);
		$webServerDate = new UltimaJsonListReader($this->callWebMethod('GetNow'));
		$rdr = new UltimaJsonListReader($this->callWebMethod('RedisGetProductPhotos', $params));
		$date = $webServerDate->getValue('Date');
		$this->log->info('Now:' . $date);
		$this->lastUpdate()->set('goods_images', 'GetGoodPhotos', $date, $this->goodsImagesTable->getTableGateway()->getTable());
		$dataSet = [];
		$keys = $rdr->getValue("Keys");
		if (count($keys)) {
			if (!isset($params['UpdatedAfter']) || (0 == $params['UpdatedAfter'])) {
				$this->goodsImagesTable->truncate();
				$this->log->info('Truncate table');
			}
			
			$deletedImages = [];
			foreach ($keys as $row) {
				$this->log->info($row);
				$pack = json_decode($pRedis->get($row));
				foreach ($pack as $item) {
					$item = new \ArrayObject((array)$item);
					$this->log->info('GoodId: ' . $item['ArticleId']);
					$this->log->info('ViewId: ' . $item['ViewId']);
					if (!empty($item['Content'])) {
						$imageName = $this->imageService()->saveBase64Image($item['Content'], $this->getImgPath(), $item['ArticleId'] . '-' . $item['ViewId']);
						$item['url'] = $imageName;
						foreach ($this->imageSizes as $imageSize) {
							list($maxWidth, $maxHeight) = explode('x', $imageSize);
							
							$imagePath = $this->getImgPath();
							$resizedPath = $this->getImgPath() . $imageSize . '/' . 'mini-' . $imageName;
							$this->imageService()->proportionalResize($imagePath, $imageName, $resizedPath, $maxWidth, $maxHeight);
						}
						$item['miniatureUrl'] = 'mini-' . $imageName;
						unset($item['Content']);
						$dataSet[] = $mapper->convert($item);
						if (50 == sizeof($dataSet)) {
							$this->goodsImagesTable->insertSet($dataSet, true);
							$dataSet = [];
						}
					} else {
						$this->log->info('Image deleted');
						$deletedImages[] = [
							'goodId' => $item['ArticleId'],
							'viewId' => $item['ViewId'] 
						];
					}
				}
				$pRedis->del($row);
			}
			if (count($deletedImages) > 0) {
				$this->imageService()->deleteImage($this->getImgPath(), $deletedImages, $this->imageSizes);
			}
			if (0 < sizeof($dataSet)) {
				$this->goodsImagesTable->insertSet($dataSet, true);
			}
			$this->lastUpdate()->saveChanges();
		} else {
			exit();
		}
		
		$this->log->info('Images inserted = ' . count($keys));
	}

	/**
	 * Initialize data mapper for images views table
	 *
	 * @return DataMapper
	 */
	protected function createGoodsImagesViewsMapper() {
		$context = $this;
		$mapper = new DataMapper();
		$mapper->setMappings(
			[
				'ViewID' => '%d: Id',
				'ViewName' => 'Name',
				'SortIndex' => '%d: SortIndex',
				'TemplateID' => 'FeatureSetId' 
			]);
		$mapper->setMapping('Required', 'Required', function ($field, $row) use($context) {
			return $context->helper()->parseBoolean($field);
		});
		return $mapper;
	}

	/**
	 * Fill images views table
	 */
	protected function processGoodsImagesViews() {
		$this->log->info('Insert images views');
		
		$mapper = $this->createGoodsImagesViewsMapper();
		
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetProductPhotoViews'));
		if (!$rdr->isEmpty()) {
			$this->goodsImagesViewsTable->truncate();
		}
		$dataSet = [];
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->goodsImagesViewsTable->insertSet($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->goodsImagesViewsTable->insertSet($dataSet);
		}
		
		$this->log->info('Images views inserted = ' . $rdr->count());
	}

}

?>