<?php

namespace SoloCatalog\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\Service\ProvidesWebservice;
use SoloReplication\Service\YandexYml;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloCatalog\Data\RemainsTable;
use SoloCatalog\Data\AvailGoodsTable;
use SoloCatalog\Data\AvailWarehousesTable;
use SoloReplication\Service\DataMapper;
use SoloCatalog\Data\GoodsRemainsMapper;
use SoloCatalog\Data\ArrivalGoodsTableInterface;
use SoloCatalog\Data\ArrivalGoodsMapperInterface;
use SoloDelivery\Data\DeliveryMapperInterface;
use Solo\Db\QueryGateway\QueryGateway;
use Solo\DateTime\DateTime;

class RemainsReplicator extends AbstractReplicationTask {

	use ProvidesWebservice;
	use YandexYml;

	/**
	 *
	 * @var GoodsRemainsMapper
	 */
	protected $goodsRemainsMapper;

	/**
	 *
	 * @var RemainsTable
	 */
	protected $remainsTable;

	/**
	 *
	 * @var string
	 */
	protected $sphinxConfigPath;

	/**
	 *
	 * @var AvailGoodsTable
	 */
	protected $availGoodsTable;

	/**
	 *
	 * @var ArrivalGoodsTableInterface
	 */
	protected $arrivalGoodsTable;

	/**
	 *
	 * @var ArrivalGoodsMapperInterface
	 */
	protected $arrivalGoodsMapper;

	/**
	 *
	 * @var DeliveryMapperInterface
	 */
	protected $deliveryMapper;

	/**
	 *
	 * @param RemainsTable $remainsTable
	 * @param GoodsRemainsMapper $goodsRemainsMapper
	 * @param AvailGoodsTable $availGoodsTable
	 * @param AvailWarehousesTable $availWarehousesTable
	 * @param ArrivalGoodsTableInterface $arrivalGoodsTable
	 * @param ArrivalGoodsMapperInterface $arrivalGoodsMapper
	 * @param string $sphinxConfigPath
	 * @param DeliveryMapperInterface $deliveryMapper
	 */
	public function __construct(RemainsTable $remainsTable, GoodsRemainsMapper $goodsRemainsMapper, AvailGoodsTable $availGoodsTable, AvailWarehousesTable $availWarehousesTable, ArrivalGoodsTableInterface $arrivalGoodsTable, ArrivalGoodsMapperInterface $arrivalGoodsMapper, $sphinxConfigPath, DeliveryMapperInterface $deliveryMapper) {
		$this->remainsTable = $remainsTable;
		$this->goodsRemainsMapper = $goodsRemainsMapper;
		$this->availGoodsTable = $availGoodsTable;
		$this->availWarehousesTable = $availWarehousesTable;
		$this->arrivalGoodsTable = $arrivalGoodsTable;
		$this->arrivalGoodsMapper = $arrivalGoodsMapper;
		$this->sphinxConfigPath = $sphinxConfigPath;
		$this->deliveryMapper = $deliveryMapper;
		$this->addSwapTable('goods_remains', 'avail_goods', 'avail_manager_goods', 'avail_warehouses', 'arrival_goods');
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \SoloReplication\Service\TaskInterface::process()
	 */
	public function process() {
		$this->processRemains();
		$this->processArrivalGoods();
		$this->processAvailGoods();
		$this->processAvailWarehouses();
		$this->rebuildSearchIndex();
		$this->buildYandexYml();
	}

	/**
	 * Initialize data mapper for filling all_goods table
	 *
	 * @return DataMapper
	 */
	protected function createRemainsMapper() {
		$mapper = new DataMapper();
		$mapper->setMappings([
			'GoodID' => '%d: ProductId',
			'StoreID' => '%d: StoreId',
			'SellSpeed' => '%f: SellSpeed',
			'Remains' => '%d: RemainsQuantity'
		]);
		$mapper->setMapping(
			'Reserve',
			'ReserveQuantity',
			function ($field, $row) {
				$quantity = $field;
				$remains = $row['RemainsQuantity'];
				if ($remains < $quantity) {
					$quantity = $remains;
				}
				return $quantity;
			});
		$mapper->setMapping('LocationId', 'StoreId', function ($field, $row) {
			return $this->goodsRemainsMapper->getLocationIdByStoreId($field);
		});
		$mapper->setMapping(
			'PlaceRow',
			'Place',
			function ($field) {
				$result = 0;
				$field = (array)$field;
				if (is_array($field) && isset($field['Row'])) {
					$result = intval($field['Row']);
				}
				return $result;
			});
		$mapper->setMapping(
			'PlaceCell',
			'Place',
			function ($field) {
				$result = 0;
				$field = (array)$field;
				if (is_array($field) && isset($field['Cell'])) {
					$result = intval($field['Cell']);
				}
				return $result;
			});
		$mapper->setMapping(
			'PlaceRack',
			'Place',
			function ($field) {
				$result = 0;
				$field = (array)$field;
				if (is_array($field) && isset($field['Rack'])) {
					$result = intval($field['Rack']);
				}
				return $result;
			});
		return $mapper;
	}

	/**
	 * Fill all goods table
	 */
	protected function processRemains() {
		$this->log->info('Insert remains');
		$mapper = $this->createRemainsMapper();
		$pRedis = $this->predisClient;
		$params = [
			'StoreIds' => $this->storesService()->getStoresIds()
		];
		// print json_encode($params, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)."\n"; exit();
		// print_r($this->callWebMethod('RedisGetProductRemains', $params)); exit();
		$rdr = new UltimaJsonListReader($this->callWebMethod('RedisGetProductRemains', $params));
		$this->log->info(sprintf('All remains: found %d keys', count($rdr)));
		if (!$rdr->isEmpty()) {
			$this->remainsTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('goods_remains');
			// $this->notify()->send($this->getNotifyMessage(__CLASS__, __METHOD__, 'RedisGetProductRemains', $params), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		$totalCount = 0;
		$dataSet = [];
		$keys = $rdr->getValue("Keys");
		if (count($keys) > 0) {
			foreach ($keys as $row) {
				$pack = json_decode($pRedis->get($row));
				foreach ($pack as $item) {
					$item = new \ArrayObject((array)$item);
					$item = $mapper->convert($item);
					$dataSet[] = $item;
					if (500 == sizeof($dataSet)) {
						$this->remainsTable->insertSet($dataSet);
						$totalCount += sizeof($dataSet);
						$dataSet = [];
					}
				}
				$pRedis->del($row);
				unset($pack);
			}
			if (0 < sizeof($dataSet)) {
				$this->remainsTable->insertSet($dataSet);
				$totalCount += sizeof($dataSet);
			}
		}
		$this->log->info('All remains inserted = ' . $totalCount);
	}

	/**
	 * Initialize data mapper for filling arrival_goods table
	 *
	 * @return DataMapper
	 */
	protected function createArrivalGoodsMapper() {
		$context = $this;
		$mapper = new DataMapper();
		$mapper->setMappings([
			'GoodID' => '%d: ArticleId',
			'OfficeID' => '%d: OfficeId'
		]);
		$mapper->setMapping('SuborderQuantity', 'Quantity', function ($field, $row) {
			$result = intval($field);
			if (0 == $result) {
				$result = 9999;
			}
			return $result;
		});
		$mapper->setMapping('ArrivalDate', 'ArrivalDate', function ($field, $row) use($context) {
			return $context->helper()->parseJSONDate($field);
		});
		return $mapper;
	}

	/**
	 * Fill arrival_goods table
	 */
	protected function processArrivalGoods() {
		$this->log->info('Insert arrival goods');

		$mapper = $this->createArrivalGoodsMapper();

		$rdr = new UltimaJsonListReader($this->callWebMethod('GetSupplierRemains'));
		if (!$rdr->isEmpty()) {
			$this->arrivalGoodsTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('arrival_goods');
			// $this->notify()->send($this->getNotifyMessage(__CLASS__, __METHOD__, 'GetSupplierRemains', []), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		$dataSet = [];
		foreach ($rdr as $row) {
			if (13737 == $row['ArticleId']) {
				print_r($row);
			}
			$row = $mapper->convert($row);
			$date = DateTime::createFromTimestamp(DateTime::createFromFormat('Y-m-d H:i:s', $row['ArrivalDate'])->getTimestamp());
			if (!$date instanceof DateTime) {
				$date = DateTime::createFromTimestamp(strtotime($row['ArrivalDate']));
			}
			if ($date instanceof DateTime) {
				if ($date->getYear() != DateTime::now()->getYear()) {
					//continue;
				}
			}

			$dataSet[] = $row;

			if (500 == sizeof($dataSet)) {
				$this->arrivalGoodsTable->insertSet($dataSet, true);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->arrivalGoodsTable->insertSet($dataSet, true);
		}

		$this->log->info('Arrival goods inserted = ' . $rdr->count());
	}

	private function processAvailGoods() {
		if ($this->isExistSwapTable('goods_remains') || $this->isExistSwapTable('arrival_goods')) {
			$this->log->info('Start fill avail_goods');
			$this->availGoodsTable->truncate();
			$countSet = 0;
			$goodsSuborderQuantity = $this->arrivalGoodsMapper->getSuborderQuantities();
			foreach ($this->goodsRemainsMapper->getLocations() as $city) {
				$this->log->info(sprintf('City location %d', $city['LocationID']));
				$arrivalDates = $this->deliveryMapper->getPickupDeliveryDatesByCityId((int)$city['LocationID']);

				$remains = [];

				$rows = $this->goodsRemainsMapper->getAllGoodsWithRemains($city['LocationID']);
				$this->log->info(sprintf('Found %d rows to update for location %d', count($rows), $city['LocationID']));
				foreach ($rows as $row) {
					$key = $row['GoodID'] . '-' . $city['LocationID'];
					if (!array_key_exists($key, $remains)) {
						$remains[$key] = [
							'GoodID' => $row['GoodID'],
							'HardCategoryID' => $row['HardCategoryID'],
							'CityID' => $row['LocationId'],
							'AvailQuantity' => 0,
							'SuborderQuantity' => 0,
							'DeliveryQuantity' => 0,
							'ArrivalDate' => (isset($arrivalDates[$row['GoodID']]) ? $arrivalDates[$row['GoodID']] : null),
							'BrandID' => $row['BrandID'],
							'SellSpeed' => $row['SellSpeed'],
							'WindowQuantity' => 0,
							'CanReserve' => 1
						];
					}

					$availQuantity = $row['AvailQuantity'];
					$remains[$key]['AvailQuantity'] += $availQuantity;

					if (isset($goodsSuborderQuantity[$city['LocationID']][$row['GoodID']]['SuborderQuantity'])) {
						$remains[$key]['SuborderQuantity'] += $goodsSuborderQuantity[$city['LocationID']][$row['GoodID']]['SuborderQuantity'];
					}

					if ($row['StoreIsDelivery']) {
						$remains[$key]['DeliveryQuantity'] += $row['AvailQuantity'];

						if (isset($goodsSuborderQuantity[$city['LocationID']][$row['GoodID']]['SuborderQuantity'])) {
							$remains[$key]['DeliveryQuantity'] += $goodsSuborderQuantity[$city['LocationID']][$row['GoodID']]['SuborderQuantity'];
						}
					}

					if (1 == $row['StoreID']) {
						$remains[$key]['WindowQuantity'] += $row['AvailQuantity'];
					}
				}

				// arrival goods
				$arrivalGoods = $this->deliveryMapper->getPickupDeliveriesByCityId($city['LocationID']);
				foreach ($arrivalGoods as $row) {
					$goodId = intval($row['GoodID']);
					$key = $goodId . '-' . $city['LocationID'];
					if (!isset($remains[$key])) {
						$remains[$key] = [
							'GoodID' => $goodId,
							'HardCategoryID' => $row['HardCategoryID'],
							'CityID' => $city['LocationID'],
							'AvailQuantity' => 0,
							'SuborderQuantity' => $row['SuborderQuantity'],
							'DeliveryQuantity' => 0,
							'ArrivalDate' => (isset($arrivalDates[$goodId]) ? $arrivalDates[$goodId] : null),
							'BrandID' => $row['BrandID'],
							'SellSpeed' => 0,
							'WindowQuantity' => 0,
							'CanReserve' => 1
						];
					}
					// $remains[$key]['AvailQuantity'] += $row['SuborderQuantity'];
					//$remains[$key]['SuborderQuantity'] += $row['SuborderQuantity'];
				}
				// END arrival goods

				$this->log->info(sprintf('Before: %d', count($remains)));
				$reasons = [];
				foreach ($remains as $goodId => &$remain) {
					if ((0 == $remain['AvailQuantity'] && 0 == $remain['SuborderQuantity'] && 0 == $remain['DeliveryQuantity']) || is_null($remain['ArrivalDate'])) {
						// @todo temporary
						// unset($remains[$goodId]);
					}
				}

				// can reserve
				foreach ($remains as $goodId => &$remain) {
					$canReserve = true;
					if ((0 == $remain['AvailQuantity']) && (0 == $remain['SuborderQuantity']) && (0 < $remain['WindowQuantity'])) {
						$canReserve = false;
					}
					$remains[$goodId]['CanReserve'] = ($canReserve ? 1 : 0);
				}
				// END can reserve

				$this->log->info(sprintf('After: %d', count($remains)));
				$this->log->info('Stop fill avail goods, inserted = ' . count($remains));
				$remainsChunks = array_chunk($remains, 500);
				foreach ($remainsChunks as $chunk) {
					$this->availGoodsTable->insertSet($chunk);
				}
				$countSet += sizeof($remains);
			}
			$this->log->info('Stop fill avail goods, inserted = ' . $countSet);
		} else {
			$this->removeSwapTable('avail_goods');
		}

		$queryGateway = new QueryGateway();

		// @todo temporary
		if (false) {
			$sql = "UPDATE #avail_goods:passive# SET ArrivalDate = NOW() WHERE ArrivalDate IS NULL";
			$queryGateway->query($sql);
		}

		if (true) {
			$sql = "SELECT DISTINCT
						ag.GoodID
					FROM #all_goods# ag
					WHERE ag.IsPackage = 1
					AND ag.IsVirtualPackage = 0
					AND ag.ViewMode <> 1
					AND ag.GoodID NOT IN (SELECT
							ptg.PackageID
						FROM #goods_to_packages# gtp
							INNER JOIN #avail_goods:passive# ag
								ON gtp.GoodID = ag.GoodID
							INNER JOIN #packages_to_groups# ptg
								ON gtp.PackageID = ptg.PackageID
								AND gtp.GroupID = ptg.GroupID
								AND ptg.IsMain = 1)";
			$emptyPackageIds = [];
			$rows = $queryGateway->query($sql);
			foreach ($rows as $row) {
				$emptyPackageIds[] = $row['GoodID'];
			}

			if (0 < count($emptyPackageIds)) {
				$sql = "DELETE FROM #avail_goods:passive# WHERE GoodID IN (" . implode(',', $emptyPackageIds) . ")";
				$queryGateway->query($sql);
			}
		}

		if (true) {
			$sql = "DELETE FROM #avail_goods:passive# WHERE AvailQuantity = 0 AND SuborderQuantity = 0 AND DeliveryQuantity = 0";
			$queryGateway->query($sql);

			$sql = "SELECT DISTINCT
						gtp.PackageID
					FROM #goods_to_packages:active# gtp
						INNER JOIN #all_goods# alg
							ON gtp.PackageID = alg.GoodID
							AND alg.IsPackage = 1
							AND alg.IsVirtualPackage = 0
							AND alg.ViewMode <> 1
						LEFT OUTER JOIN #avail_goods:passive# ag
							ON gtp.GoodID = ag.GoodID
					GROUP BY gtp.PackageID
						HAVING 0 = COUNT(ag.GoodID)";
			$emptyPackageIds = [];
			$rows = $queryGateway->query($sql);
			foreach ($rows as $row) {
				$emptyPackageIds[] = $row['PackageID'];
			}
			
			$sql = "SELECT
						ag.GoodID,
						COUNT(gtp.GoodID) AS ComponentCount
					FROM
						#all_goods# ag
					LEFT OUTER JOIN #goods_to_packages# gtp
						ON gtp.PackageID = ag.GoodID
					WHERE
						ag.IsPackage = 1
						AND ag.IsVirtualPackage = 0
						AND ag.ViewMode <> 1
					GROUP BY ag.GoodID
						HAVING 0 = COUNT(gtp.GoodID)";
			$rows = $queryGateway->query($sql);
			foreach ($rows as $row) {
				$emptyPackageIds[] = $row['GoodID'];
			}

			if (0 < count($emptyPackageIds)) {
				$sql = "DELETE FROM #avail_goods:passive# WHERE GoodID IN (" . implode(',', $emptyPackageIds) . ")";
				$queryGateway->query($sql);
			}
		}
		
		if (true) {
			$sql = "DELETE FROM #avail_goods:passive# WHERE GoodID NOT IN (
				SELECT
					gtsc.GoodID
				FROM #goods_to_soft_categories:active# gtsc
			)";
			$queryGateway->query($sql);
		}

		// clean up arrival dates
		// $sql = "UPDATE #avail_goods:passive# SET ArrivalDate = '".DateTime::now()->format('d.m.Y')."' WHERE SuborderQuantity = 0";
		// $queryGateway->query($sql);

		// $sql = "UPDATE #avail_goods:passive# SET ArrivalDate = NULL WHERE AvailQuantity > WindowQuantity AND 0 < SuborderQuantity";
		// $queryGateway->query($sql);

		// manager goods
		$sql = "TRUNCATE TABLE #avail_manager_goods:passive#";
		$queryGateway->query($sql);

		$sql = "INSERT INTO #avail_manager_goods:passive#
				SELECT * FROM #avail_goods:passive#";
		$queryGateway->query($sql);
		// END manager goods

		if (true) {
			// no photo for simple products
			$sql = "DELETE FROM #avail_goods:passive#
					WHERE GoodID NOT IN (
						SELECT DISTINCT
							gi.GoodID
						FROM #goods_images# gi
					)";
			$queryGateway->query($sql);
		}
		if (true) {
			$sql = "DELETE FROM #avail_goods:passive#
					WHERE
						GoodID NOT IN (
							SELECT
								DISTINCT ag.GoodID
							FROM #all_goods# ag
							INNER JOIN #goods_to_propvalues# gtp
								ON ag.GoodID = gtp.GoodID
							WHERE
								ag.IsPackage = 0
						)
						AND GoodID NOT IN (SELECT GoodID FROM #all_goods# WHERE IsPackage = 1)";
			$queryGateway->query($sql);
		}
		
		if (true) {
			$sql = "UPDATE #avail_goods:passive# SET AvailQuantity = AvailQuantity - WindowQuantity WHERE WindowQuantity > 0 AND WindowQuantity < AvailQuantity";
			$queryGateway->query($sql);
			
			$sql = "UPDATE #avail_goods:passive# SET WindowQuantity = 0";
			$queryGateway->query($sql);
			
			$sql = "DELETE FROM #avail_goods:passive# WHERE AvailQuantity = 0 AND SuborderQuantity = 0 AND DeliveryQuantity = 0";
			$queryGateway->query($sql);
		}
		
		if (true) {
			$sql = "DELETE FROM #avail_goods:passive# WHERE GoodID IN (
						SELECT
							grad.GoodID
						FROM #good_reserve_avail_date# grad
						WHERE grad.PickupDate IS NULL
						AND grad.AvailDeliveryDate IS NULL
						AND grad.SuborderDeliveryDate IS NULL
						AND grad.RetrieveDate IS NULL
						AND grad.DeliveryDate IS NULL
						AND grad.ShowcaseRetrieveDate IS NOT NULL
					)";
			$queryGateway->query($sql);
		}
	}

	private function processAvailWarehouses() {
		if ($this->isExistSwapTable('goods_remains', 'arrival_goods')) {
			$this->log->info('Start fill avail warehouses');
			$this->availWarehousesTable->truncate();
			$countSet = 0;
			foreach ($this->goodsRemainsMapper->getLocations() as $city) {
				$aw = [];
				foreach ($this->goodsRemainsMapper->getAllGoodsWithRemains($city['LocationID']) as $row) {
					if (!array_key_exists($row['GoodID'] . '_' . $row['OfficeID'], $aw)) {
						$aw[$row['GoodID'] . '_' . $row['OfficeID']] = array(
							'GoodID' => $row['GoodID'],
							'WarehouseID' => $row['OfficeID'],
							'AvailQuantity' => 0,
							'CategoryID' => $row['HardCategoryID']
						);
					}
					if ($row['StoreIsPickup'] && $row['OfficeIsPickup']) {
						$aw[$row['GoodID'] . '_' . $row['OfficeID']]['AvailQuantity'] += $row['AvailQuantity'];
					}
				}
				foreach ($aw as $goodId => $remain) {
					if (0 == $remain['AvailQuantity']) {
						unset($aw[$goodId]);
					}
				}
				$awChunks = array_chunk($aw, 500);
				foreach ($awChunks as $chunk) {
					$this->availWarehousesTable->insertSet($chunk);
				}
				$countSet += sizeof($aw);
			}
			$this->log->info('Stop fill avail warehouses, inserted = ' . $countSet);
		} else {
			$this->removeSwapTable('avail_warehouses');
		}
	}

	private function rebuildSearchIndex() {
		if ($this->isExistSwapTable('goods_remains', 'arrival_goods')) {
			$this->log->info('Start rebuild search goods index');
			$output = array();
			$cmd = 'indexer --all --rotate --config ' . $this->sphinxConfigPath;
			$this->log->info('Command: ' . $cmd);
			exec($cmd, $output);
			if (0 < sizeof($output)) {
				foreach ($output as $outputRow) {
					$this->log->info($outputRow);
				}
			}
			$this->log->info('Stop rebuild search goods index');
		}
	}

	private function buildYandexYml() {
		if ($this->isExistSwapTable('goods_remains', 'arrival_goods')) {
			$this->createYandexYml();
		}
	}

}
