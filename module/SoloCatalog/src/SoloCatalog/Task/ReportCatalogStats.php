<?php

namespace SoloCatalog\Task;

use SoloReplication\Service\AbstractReplicationTask;
use Solo\Db\QueryGateway\QueryGateway;
use Solo\DateTime\DateTime;

class ReportCatalogStats extends AbstractReplicationTask {

	/**
	 *
	 * @see \SoloReplication\Service\TaskInterface::process()
	 */
	public function process() {
		$queryGateway = new QueryGateway();
		
		$prevDay = date('Y-m-d', strtotime('-1 day'));
		$nowDay = date('Y-m-d', strtotime('now'));
		
		$sql = "SELECT * FROM report_catalog_stats rcs WHERE rcs.Date = '" . $prevDay . "'";
		$rows = $queryGateway->query($sql);
		$row = $rows->current();
		$prevDayId = $row->StatID;
		
		$sql = "SELECT * FROM report_catalog_stats rcs WHERE rcs.Date = '" . $nowDay . "'";
		$rows = $queryGateway->query($sql);
		$row = $rows->current();
		$nowDayId = $row->StatID;
		
		$sql = "SELECT COUNT(*) AS cnt FROM report_catalog_snapshots rcs WHERE rcs.StatID = '" . $prevDayId . "'";
		$rows = $queryGateway->query($sql);
		$row = $rows->current();
		$prevTotalCount = $row->cnt;
		
		$sql = "SELECT COUNT(*) AS cnt FROM report_catalog_snapshots rcs WHERE rcs.StatID = '" . $nowDayId . "'";
		$rows = $queryGateway->query($sql);
		$row = $rows->current();
		$nowTotalCount = $row->cnt;
		
		$totalCountDiff = abs($nowTotalCount - $prevTotalCount);
		
		// new products
		$newProducts = [];
		$sql = "SELECT
					rcs.GoodID,
					ag.GoodName
				FROM report_catalog_snapshots rcs
					LEFT OUTER JOIN #all_goods# ag
					ON rcs.GoodID = ag.GoodID
				WHERE rcs.StatID = " . $nowDayId . "
				AND rcs.GoodID NOT IN (SELECT
						rcs.GoodID
					FROM report_catalog_snapshots rcs
					WHERE rcs.StatID = " . $prevDayId . ")";
		$rows = $queryGateway->query($sql);
		foreach ($rows as $row) {
			$newProducts[] = [
				$row->GoodID,
				sprintf('https://www.santehbaza.ru/goods/%d/', $row->GoodID),
				$row->GoodName 
			];
		}
		
		// deleted products
		$deletedProducts = [];
		$sql = "SELECT
					rcs.GoodID,
					ag.GoodName
				FROM report_catalog_snapshots rcs
					LEFT OUTER JOIN #all_goods# ag
					ON rcs.GoodID = ag.GoodID
				WHERE rcs.StatID = " . $prevDayId . "
				AND rcs.GoodID NOT IN (SELECT
						rcs.GoodID
					FROM report_catalog_snapshots rcs
					WHERE rcs.StatID = " . $nowDayId . ")";
		$rows = $queryGateway->query($sql);
		foreach ($rows as $row) {
			$deletedProducts[] = [
				$row->GoodID,
				sprintf('https://www.santehbaza.ru/goods/%d/', $row->GoodID),
				$row->GoodName 
			];
		}
		
		$grandReport = [];
		$grandReport[] = 'Всего товаров было на начало дня: ' . $prevTotalCount;
		$grandReport[] = 'Прибавилось товаров: ' . (0 < count($newProducts) ? '+' . count($newProducts) : '0');
		$grandReport[] = 'Убавилось товаров: ' . (0 < count($deletedProducts) ? '-' . count($deletedProducts) : '0');
		$grandReport[] = 'Всего товаров на конец дня: ' . $nowTotalCount . ' ' . (0 < $totalCountDiff ? '(' . ($nowTotalCount > $prevTotalCount ? '+' : '-') . $totalCountDiff . ')' : '');
		$grandReport = implode('<br />', $grandReport);
		
		$this->sendMailReport($grandReport, $newProducts, $deletedProducts);
	}

	/**
	 *
	 * @param array $report        	
	 */
	private function sendMailReport($report, $newProducts, $deletedProducts) {
		$mail = $this->createMail();
		
		$mail->setTo('5049744@mail.ru');
		$mail->addCc('vborshevskiy@taxasoftware.com');
		
		$body = $this->createBody();
		$body->addPart($this->createHtmlMessage($report));
		
		$subject = '';
		if (0 < count($newProducts)) {
			$subject = sprintf('+%d новых товаров на сайте от %s', count($newProducts), DateTime::now()->format('d.m.Y'));
		} elseif (0 < count($deletedProducts)) {
			$subject = sprintf('-%d товаров на сайте от %s', count($deletedProducts), DateTime::now()->format('d.m.Y'));
		} else {
			$subject = sprintf('отчет по товарам на сайте от %s', DateTime::now()->format('d.m.Y'));
		}
		$mail->setSubject("=?utf-8?B?" . base64_encode($subject) . "?=");
		
		if (0 < count($newProducts)) {
			$attachment = $this->createAttachmentFromString($this->createReportCsv($newProducts), 'application/vnd.ms-excel', sprintf('new_products_%s', DateTime::now()->format('d-m-Y')));
			$attachment->encoding = 'base64';
			$attachment->disposition = 'attachment';
			$attachment->type = sprintf('application/vnd.ms-excel; name="new_products_%s.csv"', DateTime::now()->format('d-m-Y'));
			$attachment->filename = sprintf('new_products_%s.csv', DateTime::now()->format('d-m-Y'));
			$body->addPart($attachment);
		}
		
		if (0 < count($deletedProducts)) {
			$attachment = $this->createAttachmentFromString($this->createReportCsv($deletedProducts), 'application/vnd.ms-excel', sprintf('deleted_products_%s', DateTime::now()->format('d-m-Y')));
			$attachment->encoding = 'base64';
			$attachment->disposition = 'attachment';
			$attachment->type = sprintf('application/vnd.ms-excel; name="deleted_products_%s.csv"', DateTime::now()->format('d-m-Y'));
			$attachment->filename = sprintf('deleted_products_%s.csv', DateTime::now()->format('d-m-Y'));
			$body->addPart($attachment);
		}
		
		$mail->setBody($body);
		$this->sendMail($mail);
		
		print "mail sent\n";
	}

	/**
	 *
	 * @param array $products        	
	 * @return string
	 */
	private function createReportCsv(array $products) {
		$result = '';
		
		$line = 'Артикул;Ссылка;Название товара';
		$line = iconv('UTF-8', 'WINDOWS-1251', $line);
		$result .= $line . PHP_EOL;
		
		foreach ($products as $row) {
			foreach ($row as $uid => $value) {
				$row[$uid] = str_replace(';', ':', $value);
			}
			$line = implode(';', array_values($row));
			$line = iconv('UTF-8', 'WINDOWS-1251', $line);
			$result .= $line . PHP_EOL;
		}
		
		return $result;
	}

	/**
	 *
	 * @return \Zend\Mail\Message
	 */
	private function createMail() {
		$mail = new \Zend\Mail\Message();
		
		$fromName = 'Santehbaza';
		$mail->addFrom('robot@' . DOMAIN_NAME, $fromName);
		
		return $mail;
	}

	/**
	 *
	 * @param string $content        	
	 * @param string $type        	
	 * @param string $id        	
	 * @return \Zend\Mime\Part
	 */
	private function createAttachmentFromString($content, $type = null, $id = null) {
		$part = new \Zend\Mime\Part($content);
		
		$part->encoding = 'base64';
		if (null !== $type) {
			$part->type = $type;
		}
		if (null !== $id) {
			$part->id = $id;
		}
		
		return $part;
	}

	/**
	 *
	 * @param
	 *        	array | scalar $parts
	 * @return \Zend\Mime\Message
	 */
	private function createBody($parts = null) {
		$body = new \Zend\Mime\Message();
		if (null !== $parts) {
			if (is_array($parts)) {
				$body->setParts($parts);
			} else {
				$body->setParts([
					$parts 
				]);
			}
		}
		return $body;
	}

	/**
	 *
	 * @param string $content        	
	 * @return \Zend\Mime\Part
	 */
	private function createHtmlMessage($content) {
		$part = new \Zend\Mime\Part($content);
		$part->type = \Zend\Mime\Mime::TYPE_HTML;
		$part->charset = 'utf-8';
		$part->encoding = \Zend\Mime\Mime::ENCODING_QUOTEDPRINTABLE;
		return $part;
	}

	/**
	 *
	 * @param \Zend\Mail\Message $mail        	
	 */
	private function sendMail(\Zend\Mail\Message $mail) {
		$transport = new \Zend\Mail\Transport\Sendmail();
		$transport->send($mail);
	}

}

?>