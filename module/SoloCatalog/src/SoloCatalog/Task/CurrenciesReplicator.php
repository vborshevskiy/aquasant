<?php

namespace SoloCatalog\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloReplication\Service\DataMapper;
use SoloERP\Service\ProvidesWebservice;
use SoloCatalog\Data\CurrenciesTable;

class CurrenciesReplicator extends AbstractReplicationTask {

    use ProvidesWebservice;

    /**
     *
     * @var CurrenciesTable
     */
    protected $currenciesTable;

    /**
     *
     * @param CurrenciesTable $currenciesTable
     */
    public function __construct(CurrenciesTable $currenciesTable) {
        $this->currenciesTable = $currenciesTable;
        $this->addSwapTable('currencies');
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SoloReplication\Service\TaskInterface::process()
     */
    public function process() {
        $this->processCurrencies();
    }

    /**
     * Initialize data mapper for currencies table
     * 
     * @return DataMapper
     */
    protected function createCurrenciesMapper() {
        $context = $this;
        $mapper = new DataMapper();
        $mapper->setMappings([
            'CurrencyID' => '%d: Id',
            'CurrencyName' => 'Name',
            'CurrencyRate' => '%f: Rate',
        ]);
        $mapper->setMapping('MainCurrency', 'MainCurrency', function ($field, $row) use($context) {
            return $context->helper()->parseBoolean($field);
        });
        return $mapper;
    }

    /**
     * Fill currencies table
     */
    protected function processCurrencies() {
        $this->log->info('Insert currencies');

        $mapper = $this->createCurrenciesMapper();

        $rdr = new UltimaJsonListReader($this->callWebMethod('GetCurrencies'));
        if (!$rdr->isEmpty()) {
            $this->currenciesTable->truncate();
            $this->notify()->fileWarning(false);
        } else {
            $this->removeSwapTable('currencies');
            $this->notify()->send($this->getNotifyMessage(__CLASS__,__METHOD__,'GetCurrencies',[]), 'Пустой ответ по методу репликации');
            $this->notify()->fileWarning(true);
        }
        $dataSet = [];
        foreach ($rdr as $row) {
            $dataSet[] = $mapper->convert($row);
            if (500 == sizeof($dataSet)) {
                $this->currenciesTable->insertSet($dataSet);
                $dataSet = [];
            }
        }
        if (0 < sizeof($dataSet)) {
            $this->currenciesTable->insertSet($dataSet);
        }

        $this->log->info('Currencies inserted = ' . $rdr->count());
    }

}

?>