<?php

namespace SoloCatalog\Task;

use SoloReplication\Service\AbstractReplicationTask;
use Solo\Db\QueryGateway\QueryGateway;

class CollectCatalogStats extends AbstractReplicationTask {

	/**
	 *
	 * @see \SoloReplication\Service\TaskInterface::process()
	 */
	public function process() {
		$queryGateway = new QueryGateway();
		
		$now = date('Y-m-d');
		$sql = "INSERT INTO report_catalog_stats SET Date = '" . $now . "'";
		$queryGateway->query($sql);
		$sql = "SELECT * FROM report_catalog_stats WHERE Date = '" . $now . "'";
		$rows = $queryGateway->query($sql);
		$row = $rows->current();
		$statId = intval($row->StatID);
		
		$sql = "INSERT INTO report_catalog_snapshots (StatID, GoodID)
				SELECT " . $statId . ", GoodID FROM #avail_goods#";
		$queryGateway->query($sql);
	}

}

?>