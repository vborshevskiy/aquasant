<?php

namespace SoloCatalog\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloCatalog\Data\GoodsImagesTable;
use Solo\Db\QueryGateway\QueryGateway;

class GoodsImagesResizer extends AbstractReplicationTask {

	protected $imageSizes = [
		'60x60',
		'260x280',
		'450x600',
		'864x1080',
		'360x400',
		'840x960' 
	];

	/**
	 *
	 * @var GoodsImagesTable
	 */
	protected $goodsImagesTable;

	/**
	 *
	 * @param GoodsImagesTable $goodsImagesTable        	
	 */
	public function __construct(GoodsImagesTable $goodsImagesTable) {
		$this->goodsImagesTable = $goodsImagesTable;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \SoloReplication\Service\TaskInterface::process()
	 */
	public function process() {
		$this->processGoodsImagesResizer();
	}

	/**
	 * Fill goods currencies table
	 */
	protected function processGoodsImagesResizer() {
		$this->log->info('Iimages resizing starts');
		$issetUnresizedImages = true;
		$dataSet = [];
		while ($issetUnresizedImages) {
			$queryGateway = new QueryGateway();
			$sql = "SELECT
						gi.GoodID,
						gi.ViewID,
						gi.Url,
						gi.ViewTemplateName,
						ag.GoodName
					FROM #goods_images# gi
						INNER JOIN #all_goods# ag
							ON gi.GoodID = ag.GoodID";
			// $unresizedGoods = $this->goodsImagesTable->getUnresizedImages();
			$unresizedGoods = $queryGateway->query($sql);
			$issetUnresizedImages = ($unresizedGoods->count() > 0);
			if ($issetUnresizedImages) {
				foreach ($unresizedGoods as $unresizedGood) {
					$this->log->info('GoodId = ' . $unresizedGood['GoodID'] . ' | ' . 'ViewId = ' . $unresizedGood['ViewID']);
					if (file_exists($this->getImgPath() . $unresizedGood['Url'])) {
						foreach ($this->imageSizes as $imageSize) {
							list($maxWidth, $maxHeight) = explode('x', $imageSize);
							
							$imagePath = $this->getImgPath();
							$imageName = $unresizedGood['Url'];
							$resizedPath = $this->getImgPath() . '/' . $imageSize . '/' . 'mini-' . $unresizedGood['Url'];
							
							$exifHeaders = [];
							$exifHeaders[] = [
								'name' => 'owner',
								'value' => 'купить сантехнику www.'.DOMAIN_NAME 
							];
							$exifHeaders[] = [
								'name' => 'copyright',
								'value' => 'купить сантехнику www.'.DOMAIN_NAME 
							];
							
							$descriptionPrefix = sprintf('%s %s', $unresizedGood['GoodName'], $unresizedGood['ViewTemplateName']);
							switch ($imageSize) {
								case '864x1080':
									$exifHeaders[] = [
										'name' => 'description',
										'value' => $descriptionPrefix . ' большое изображение товара в продаже' 
									];
									$exifHeaders[] = [
										'name' => 'user-comment',
										'value' => $descriptionPrefix . ' большое изображение товара в продаже' 
									];
									break;
								case '450x600':
									$exifHeaders[] = [
										'name' => 'description',
										'value' => $descriptionPrefix . ' картинка товара в продаже' 
									];
									$exifHeaders[] = [
										'name' => 'user-comment',
										'value' => $descriptionPrefix . ' картинка товара в продаже' 
									];
									break;
								case '260x280':
									$exifHeaders[] = [
										'name' => 'description',
										'value' => $descriptionPrefix . ' маленькое изображение товара в продаже' 
									];
									$exifHeaders[] = [
										'name' => 'user-comment',
										'value' => $descriptionPrefix . ' маленькое изображение товара в продаже' 
									];
									break;
							}
							
							$result = $this->imageService()->proportionalResize($imagePath, $imageName, $resizedPath, $maxWidth, $maxHeight, $exifHeaders);
							if (!$result) {
								$this->log->warn('File not found');
							}
						}
						$dataSet[] = [
							'GoodID' => $unresizedGood['GoodID'],
							'ViewID' => $unresizedGood['ViewID'],
							'MiniatureUrl' => 'mini-' . $unresizedGood['Url'] 
						];
					} else {
						$this->log->warn('File not found');
						$dataSet[] = [
							'GoodID' => $unresizedGood['GoodID'],
							'ViewID' => $unresizedGood['ViewID'],
							'MiniatureUrl' => 'not-found.jpg' 
						];
					}
				}
				if (count($dataSet) > 0) {
					$this->goodsImagesTable->insertSet($dataSet, true);
				}
			}
		}
		$this->log->info('Images resizing finishs');
	}

}

?>