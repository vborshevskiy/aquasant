<?php

namespace SoloCatalog\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\Service\ProvidesWebservice;
use SoloCatalog\Data\SatelliteGroupsTable;
use SoloCatalog\Data\SatelliteGoodsTable;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloReplication\Service\DataMapper;
class SatelliteReplicator extends AbstractReplicationTask {

	use ProvidesWebservice;
	
	/**
	 * 
	 * @var SatelliteGroupsTable
	 */
	protected $groupsTable;
	
	/**
	 * 
	 * @var SatelliteGoodsTable
	 */
	protected $goodsTable;
	
	/**
	 * 
	 * @param SatelliteGroupsTable $groupsTable
	 * @param SatelliteGoodsTable $goodsTable
	 */
	public function __construct(SatelliteGroupsTable $groupsTable, SatelliteGoodsTable $goodsTable) {
		$this->groupsTable = $groupsTable;
		$this->goodsTable = $goodsTable;
		ini_set('memory_limit', '1024M');
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \SoloReplication\Service\TaskInterface::process()
	 */
	public function process() {
		$this->log->info('Insert satellites');
		
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetRelatedArticles'));
		if (!$rdr->isEmpty()) {
			$this->groupsTable->truncate();
			$this->goodsTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('satellite_groups');
			$this->removeSwapTable('satellite_goods');
			$this->notify()->send($this->getNotifyMessage(__CLASS__,__METHOD__,'GetRelatedArticles',[]), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		$groupsDataSet = [];
		$goodsDataSet = [];
		$groupsMapper = $this->createGroupsMapper();
		$goodsCount = 0;
		foreach ($rdr as $row) {
			$groupsDataSet[] = $groupsMapper->convert($row);
			
			$groupId = $row['LinkId'];
			foreach ($row['ArticleIds'] as $goodId) {
				foreach ($row['RelatedIds'] as $satelliteId) {
					$goodsDataSet[] = ['GoodID' => $goodId, 'SatelliteID' => $satelliteId, 'GroupID' => $groupId];
					
					if (500 <= count($goodsDataSet)) {
						$this->goodsTable->insertSet($goodsDataSet);
						$goodsDataSet = [];
					}
					
					$goodsCount++;
				}
			}
		}
		if (0 < count($goodsDataSet)) {
			$this->goodsTable->insertSet($goodsDataSet);
		}
		$this->groupsTable->insertSet($groupsDataSet);
		
		$this->log->info('Groups inserted = ' . $rdr->count());
		$this->log->info('Goods inserted = ' . $goodsCount);
	}
	
	/**
	 * 
	 * @return \SoloCatalog\Task\DataMapper
	 */
	protected function createGroupsMapper() {
		$mapper = new DataMapper();
		$mapper->setMappings([
			'GroupID' => '%d: LinkId',
			'GroupName' => 'Name',
			'SortTypeId' => '%d: SortTypeId',
			'SortIndex' => '%d: TopToShow'
			]);
		return $mapper;
	}
	
}

?>