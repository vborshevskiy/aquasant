<?php

namespace SoloCatalog\Task;

use SoloCatalog\Data\SuppliersTableInterface;
use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\Service\ProvidesWebservice;
use SoloCatalog\Data\SupplierGoodsTableInterface;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloReplication\Service\DataMapper;

class SuppliersReplicator extends AbstractReplicationTask {

    use ProvidesWebservice;

    /**
     *
     * @var SupplierGoodsTableInterface
     */
    protected $supplierGoodsTable;

    /**
     *
     * @var SuppliersTableInterface
     */
    protected $suppliersTable;

    /**
     * @param SuppliersTableInterface $suppliersTable
     * @param SupplierGoodsTableInterface $supplierToGoodsTable
     */
    public function __construct(SuppliersTableInterface $suppliersTable, SupplierGoodsTableInterface $supplierGoodsTable) {

        $this->suppliersTable = $suppliersTable;
        $this->supplierGoodsTable = $supplierGoodsTable;

        $this->addSwapTable('supplier_goods', 'suppliers');
    }

    /**
     *
     * @see \SoloReplication\Service\TaskInterface::process()
     */
    public function process() {
        $this->processSuppliers();
        $this->processSupplierGoods();
    }

    /**
     * Update supplier_goods
     */
    private function processSupplierGoods() {
        $this->log->info('Begin supplierToGoods');

        $rdr = new UltimaJsonListReader($this->callWebMethod('GetSupplierRemainsFull'));
        if (!$rdr->isEmpty()) {
            $this->supplierGoodsTable->truncate();
        } else $this->log->info('Empty');
        $dataSet = [];
        $mapper = $this->createSupplierGoodsMapper();
        foreach ($rdr as $row) {
            $dataSet[] = $mapper->convert($row);
            if (500 == sizeof($dataSet)) {
                $this->supplierGoodsTable->insertSet($dataSet);
                $dataSet = [];
            }
        }
        if (0 < sizeof($dataSet)) {
            $this->supplierGoodsTable->insertSet($dataSet);
        }

        $this->log->info('Inserted = ' . $rdr->count());

        $this->log->info('End supplierToGoods');
    }

    /**
     *
     * @return \SoloReplication\Service\DataMapper
     */
    private function createSupplierGoodsMapper() {
        $mapper = new DataMapper(
            [
                'GoodID' => '%d: ArticleId',
                'OfficeID' => '%d: OfficeId',
                'SupplierID' => '%d: SupplierID',
                'Quantity' => '%d: Quantity',
                'ArrivalDate' => 'ArrivalDate',
                'ProductionTerm' => '%d: ProductionTerm',
                'ArrivalQuantity' => '%d: ArrivalQuantity',
            ]);

        return $mapper;
    }

    /**
     * Update suppliers
     */
    private function processSuppliers() {
        $this->log->info('Begin suppliers');

        $rdr = new UltimaJsonListReader($this->callWebMethod('GetSuppliers'));
        if (!$rdr->isEmpty()) {
            $this->suppliersTable->truncate();
        }
        $dataSet = [];
        $mapper = $this->createSuppliersMapper();
        foreach ($rdr as $row) {
            $dataSet[] = $mapper->convert($row);
            if (500 == sizeof($dataSet)) {
                $this->suppliersTable->insertSet($dataSet);
                $dataSet = [];
            }
        }
        if (0 < sizeof($dataSet)) {
            $this->suppliersTable->insertSet($dataSet);
        }

        $this->log->info('Inserted = ' . $rdr->count());

        $this->log->info('End suppliers');
    }

    /**
     *
     * @return \SoloReplication\Service\DataMapper
     */
    private function createSuppliersMapper() {
        $mapper = new DataMapper([
            'SupplierID' => '%d: SupplierID',
            'SupplierName' => 'SupplierName',
        ]);
        $mapper->setMapping('LastPriceUpdated', 'LastPriceUpdated', function ($field) {
            return $this->helper()->parseJSONDate($field);
        });
        return $mapper;
    }
}

?>