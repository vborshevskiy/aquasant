<?php

namespace SoloCatalog\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloCatalog\Data\SitemapMapperInterface;
use Solo\Sitemap\SitemapIndex;
use Solo\Sitemap\Sitemap;
use Solo\Sitemap\Url;
use SoloCatalog\Entity\Goods\FilterOptions;
use SoloCatalog\Entity\Filters\FilterSet;
use SoloCatalog\Entity\Filters\SelectedFilter\SelectedFilterSet;
use SoloCatalog\Entity\Filters\SelectedFilter\Brands;
use Zend\Paginator\Paginator;
use SoloCatalog\Entity\Filters\SelectedFilter\Limit;

class SitemapCreator extends AbstractReplicationTask {
    
    /**
     *
     * @var SitemapMapperInterface
     */
    protected $sitemapMapper;
    
    /**
     *
     * @var array
     */
    protected $citiesSubdomains = [];

    /**
     *
     * @param SitemapMapperInterface $sitemapMapper
     * @param array $citiesSubdomains
     */
    public function __construct(SitemapMapperInterface $sitemapMapper, $citiesSubdomains = null) {
        $this->sitemapMapper = $sitemapMapper;
        if (is_array($citiesSubdomains)) {
            $this->citiesSubdomains = $citiesSubdomains;
        }
        $this->scriptNotRespondingTimeInMinutes = 60 * 24;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SoloReplication\Service\TaskInterface::process()
     */
    public function process() {
        $this->createSitemap();
    }
    
    protected function createSitemap() {
        $this->log->info('Start creating sitemap');
        $cityIds = $this->sitemapMapper->getCities();
        if (array_key_exists('citiesSubdomains', $this->getSitemapSettings())) {
            $citiesSubdomains = $this->getSitemapSettings()['citiesSubdomains'];
        } else {
            $this->log->err('Setting "citiesSubdomains" can\'t be empty');
            return;
        }
        if (array_key_exists('cities', $this->getSitemapSettings())) {
            $cities = $this->getSitemapSettings()['cities'];
        } else {
            $this->log->err('Setting "citiesSubdomains" can\'t be empty');
            return;
        }
        if (array_key_exists('mainDomain', $this->getSitemapSettings())) {
            $mainDomain = $this->getSitemapSettings()['mainDomain'];
        } else {
            $this->log->err('Setting "citiesMainDomain" can\'t be empty');
            return;
        }
        foreach ($cityIds as $cityId) {
            if (array_key_exists($cityId, $citiesSubdomains)) {
                $subdomain = $citiesSubdomains[$cityId];
            } else {
                $this->log->err('Setting "Subdomain" for city '.$cityId.' is empty');
                continue;
            }
            if (array_key_exists($cityId, $cities)) {
                $cityUid = $cities[$cityId];
            } else {
                $this->log->err('Setting "Cities" for city '.$cityId.' is empty');
                continue;
            }
            $this->log->info('City id = ' . $cityId);
            $goods = $this->sitemapMapper->getAvailGoods($cityId);
            $categories = $this->sitemapMapper->getCategories($cityId);
            $sitemapIndex = new SitemapIndex();
            $sitemapIndex->addSitemap($this->createCategoriesSitemap($categories, $subdomain, $mainDomain), 'categories');
            $sitemapIndex->addSitemap($this->createGoodsSitemap($goods, $subdomain, $mainDomain), 'goods');
            $sitemapIndex->addSitemap($this->createCategoriesSitemapFirstLevel($cityId, $categories, $subdomain, $mainDomain), 'categories_first_lvl');
            
            $path = 'public/sitemap/'.$cityUid.'/';
            $sitemapIndex->saveSitemap($path, $cityUid, $mainDomain, $subdomain);
        }
        $this->log->info('Stop creating sitemap');
    }
    
    /**
     * 
     * @param array $categories
     * @param string $subdomain
     * @param string $mainDomain
     * @return Sitemap
     */
    protected function createCategoriesSitemap($categories, $subdomain, $mainDomain) {
        $sitemap = new Sitemap('categories');
        
        //главная
        $urlMain = new Url();
        $urlMain->setLocation('http://' . (is_null($subdomain) || empty($subdomain) ? '' : $subdomain . '.') . $mainDomain . '/');
        $urlMain->setChangeFrequency(SitemapIndex::CHANGE_FREQUENCY_DAYLY);
        $sitemap->addUrl($urlMain);
        
        // главная каталога
        $urlCatalog = new Url();
        $urlCatalog->setLocation('http://' . (is_null($subdomain) || empty($subdomain) ? '' : $subdomain . '.') . $mainDomain . '/catalog/');
        $urlCatalog->setChangeFrequency(SitemapIndex::CHANGE_FREQUENCY_DAYLY);
        $sitemap->addUrl($urlCatalog);
        
        foreach ($categories as $category) {
            $url = new Url();
            $url->setLocation('http://' . (is_null($subdomain) || empty($subdomain) ? '' : $subdomain . '.') . $mainDomain . '/' . $category['CategoryUID'] . '/');
            $url->setChangeFrequency(SitemapIndex::CHANGE_FREQUENCY_DAYLY);
            $sitemap->addUrl($url);
        }
        
        $sitemap->setLastModified(new \DateTime());
        $sitemap->setLocation('categories.xml');
        return $sitemap;
    }
    
    /**
     * 
     * @param array $goods
     * @param string $subdomain
     * @param string $mainDomain
     * @return Sitemap
     */
    protected function createGoodsSitemap($goods, $subdomain, $mainDomain) {
        $sitemap = new Sitemap('goods');
        foreach ($goods as $good) {
            $url = new Url();
            $url->setLocation('http://' . (is_null($subdomain) || empty($subdomain) ? '' : $subdomain . '.') . $mainDomain . '/' . $good['CategoryUID'] . '/' . $good['GoodID'] . '_' . $good['GoodEngName'] . '/');
            $url->setChangeFrequency(SitemapIndex::CHANGE_FREQUENCY_DAYLY);
            $sitemap->addUrl($url);
        }
        $sitemap->setLastModified(new \DateTime());
        $sitemap->setLocation('goods.xml');
        return $sitemap;
    }
    
    protected function createCategoriesSitemapFirstLevel($cityId, $categories, $subdomain, $mainDomain) {
        $sitemap = new Sitemap('categories_first_lvl');
        foreach ($categories as $category) {
            $options = new FilterOptions();
            $filters = new FilterSet();
            $selectedFilters = new SelectedFilterSet();
            $categoryId = (int)$category['SoftCategoryID'];
            $this->filters()->fillBrandsFiltersByCategoryId($filters, $categoryId, $cityId);
            $this->setupFiltersBrandsSelection($filters, $options);
            $this->buildFiltersBrandsUrls($filters, $selectedFilters);
            $goodIds = (0 < $filters->countSelectedFilters()) ? $filters->enumSelectedGoodIds() : $filters->enumGoodIds();
            $options->setLimitation(9);
            $pagination = $this->createPagination($goodIds, 1, $options->getLimitation('perPage'), $selectedFilters);
//            foreach ($pagination as $page) {
//                if ($page['number'] > 1) {
//                    $url = new Url();
//                    $url->setLocation('http://' . (is_null($subdomain) || empty($subdomain) ? '' : $subdomain . '.') . $mainDomain . '/' . $category['CategoryUID'] . $page['url']);
//                    $url->setChangeFrequency(SitemapIndex::CHANGE_FREQUENCY_DAYLY);
//                    $sitemap->addUrl($url);
//                }
//            }
            if ($filters->getFilter('brands') && !$filters->getFilter('brands')->isEmpty()) {
                foreach ($filters->getFilter('brands') as $brand) {
                    if ((null !== $brand) && (0 < $brand->countGoods())) {
                        $url = new Url();
                        $url->setLocation('http://' . (is_null($subdomain) || empty($subdomain) ? '' : $subdomain . '.') . $mainDomain . '/' . $category['CategoryUID'] . $brand->additional('url'));
                        $url->setChangeFrequency(SitemapIndex::CHANGE_FREQUENCY_DAYLY);
                        $sitemap->addUrl($url);
                    }
//                    $urlSelectedFilters = $selectedFilters->copy();
//                    $brandFilter = new Brands();
//                    $brandFilter->addId($brand->getTitle());
//                    $brandFilter->addUid($brand->getTitle(), $brand->additional('uid'));
//                    $urlSelectedFilters->add('brands', $brandFilter);
//                    $urlOptions = new FilterOptions();
//                    if ($urlSelectedFilters->has('brands')) {
//                        $urlOptions->addBrandId($urlSelectedFilters->brands()->enumIds());
//                    }
//                    $urlFilters = new FilterSet();
//                    
//                    $this->filters()->fillBrandsFiltersByCategoryId($urlFilters, $categoryId, $cityId);
//                    $this->setupFiltersBrandsSelection($urlFilters, $urlOptions);
//                    $this->buildFiltersBrandsUrls($urlFilters, $urlSelectedFilters);
//            
//                    $urlFiltersGoodIds = (0 < $urlFilters->countSelectedFilters()) ? $urlFilters->enumSelectedGoodIds() : $urlFilters->enumGoodIds();
//                    $pagination = $this->createPagination($urlFiltersGoodIds, 1, $options->getLimitation('perPage'), $urlSelectedFilters);
//                    foreach ($pagination as $page) {
//                        if ($page['number'] > 1) {
//                            $url = new Url();
//                            $url->setLocation('http://' . (is_null($subdomain) || empty($subdomain) ? '' : $subdomain . '.') . $mainDomain . '/' . $category['CategoryUID'] . $page['url']);
//                            $url->setChangeFrequency(SitemapIndex::CHANGE_FREQUENCY_DAYLY);
//                            $sitemap->addUrl($url);
//                        }
//                    }
                }
            }
        }
        $sitemap->setLastModified(new \DateTime());
        $sitemap->setLocation('categories_first_lvl.xml');
        return $sitemap;
    }
    
    protected function setupFiltersBrandsSelection(FilterSet $filters, FilterOptions $options) {
        if ($options->hasBrands()) {
            $brands = $options->getBrandIds();
            foreach ($brands as $brand) {
                $value = (is_numeric($brand) ? 'brand' . $brand : 'brand' . Inflector::camelize($brand));
                $filters->selectValue('brands', $value);
            }
        }
    }

    protected function buildFiltersBrandsUrls(FilterSet $filters, SelectedFilterSet $selectedFilters, $forseAsGetParam = false) {
        foreach ($filters->getFilter('brands')->enumNonEmptyValues() as $filterValue) {
            $urlFilters = $selectedFilters->copy();
            $urlFilters->remove('limit');
            if (!$urlFilters->has('brands')) {
                $urlFilters->add('brands', new Brands());
            }
            $urlFilters->brands()->addId($filterValue->getTitle(), $forseAsGetParam);
            if (!is_null($filterValue->additional('uid'))) {
                $urlFilters->brands()->addUid($filterValue->getTitle(), $filterValue->additional('uid'));
            }
            $filterValue->additional('url', $urlFilters->toQueryString());
            $urlFilters->brands()->removeId($filterValue->getTitle(), $forseAsGetParam);
            $filterValue->additional('deleteUrl', $urlFilters->toQueryString());
        }
    }
    
    protected function createPagination(array $goodIds, $currentPage, $perPage, SelectedFilterSet $selectedFilters) {
        $paginator = new Paginator(new \Zend\Paginator\Adapter\ArrayAdapter($goodIds));
        $paginator->setCurrentPageNumber($currentPage);
        $paginator->setDefaultItemCountPerPage($perPage);
        $pages = $paginator->getPages();

        $pagesInRange = $pages->pagesInRange;
        foreach ($pagesInRange as $page) {
            if (false === strpos($page, 'delimiter')) {
                $urlFilters = $selectedFilters->copy();
                if (!$urlFilters->has('limit')) {
                    $urlFilters->add('limit', new Limit());
                }
                $urlFilters->limit()->routeParam(true);
                $urlFilters->limit()->setPage($page);
                $url = $urlFilters->toQueryString();

                $pagination[$page] = array(
                    'number' => $page,
                    'url' => $url
                );
            }
        }
        return $pagination;
    }

}

?>