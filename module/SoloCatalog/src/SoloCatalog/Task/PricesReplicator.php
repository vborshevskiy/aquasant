<?php

namespace SoloCatalog\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\Service\ProvidesWebservice;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloCatalog\Data\PricesTable;
use SoloReplication\Service\DataMapper;
use SoloReplication\Service\YandexYml;
use Solo\Db\QueryGateway\QueryGateway;

class PricesReplicator extends AbstractReplicationTask {
	
	use ProvidesWebservice;
	use YandexYml;

	/**
	 *
	 * @var AllGoodsTableInterface
	 */
	protected $pricesTable;

	/**
	 *
	 * @param PricesTable $pricesTable        	
	 */
	public function __construct(PricesTable $pricesTable) {
		$this->pricesTable = $pricesTable;
		$this->addSwapTable('goods_prices');
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \SoloReplication\Service\TaskInterface::process()
	 */
	public function process() {
		$this->processPrices();
		$this->buildYandexYml();
	}

	/**
	 * Initialize data mapper for filling all_goods table
	 *
	 * @return DataMapper
	 */
	protected function createPricesMapper() {
		$mapper = new DataMapper();
		$mapper->setMappings(
			[
				'GoodID' => '%d: ProductId',
				'CategoryId' => '%d: CategoryId',
				'ZoneId' => '%d: ZoneId',
				'Value' => '%d: Value',
				'PrevValue' => '%d: PrevValue' 
			]);
		return $mapper;
	}

	/**
	 * Fill all goods table
	 */
	protected function processPrices() {
		$emptyPrice = false;
		$this->log->info('Insert prices');
		$mapper = $this->createPricesMapper();
		$pRedis = $this->predisClient;
		$params = [];
		$rdr = new UltimaJsonListReader($this->callWebMethod('RedisGetProductPrices', $params));
		if (!$rdr->isEmpty()) {
			$this->pricesTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('goods_prices');
			$this->notify()->send($this->getNotifyMessage(__CLASS__, __METHOD__, 'RedisGetProductPrices', $params), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		$totalCount = 0;
		$dataSet = [];
		$keys = $rdr->getValue("Keys");
		if (count($keys) > 0) {
			foreach ($keys as $row) {
				$pack = json_decode($pRedis->get($row));
				foreach ($pack as $item) {
					$item = new \ArrayObject((array)$item);
					if ($item['Value'] > 0) {
						$dataSet[] = $mapper->convert($item);
						if (500 == sizeof($dataSet)) {
							$this->pricesTable->insertSet($dataSet);
							$totalCount += sizeof($dataSet);
							$dataSet = [];
						}
					} else {
						$emptyPrice = true;
					}
				}
				$pRedis->del($row);
				unset($pack);
			}
			if (0 < sizeof($dataSet)) {
				$this->pricesTable->insertSet($dataSet);
				$totalCount += sizeof($dataSet);
			}
		}
		if ($emptyPrice) {
			// $this->removeSwapTable('goods_prices');
			// $this->notify()->send($this->getNotifyMessage(__CLASS__,__METHOD__,'RedisGetProductPrices', $params), 'Реплика цен - пришло нулевое значение');
		}
		$this->log->info('All prices inserted = ' . $totalCount);
		
		$queryGateway = new QueryGateway();
		
		// @todo temporary
		if (false) {
			$sql = "INSERT INTO #goods_prices:passive#
	        		SELECT p.* FROM (
	        			SELECT
	        				ag.GoodID,
	        				2 AS ZoneID,
	        				1 AS CategoryID,
	        				10000 AS Value,
	        				11000 AS PrevValue
	        			FROM #all_goods:active# ag
	        			WHERE ag.GoodID NOT IN (SELECT gp.GoodID FROM #goods_prices:passive# gp)
	        		) AS p";
			$queryGateway->query($sql);
		}
		
		// update package prices
		if (false) {
			$sql = "SELECT
					g.GoodID AS PackageID,
					SUM(gp_comp.Value) AS PackagePrice
				FROM #all_goods:active# g
					INNER JOIN #goods_to_packages:active# gtp
						ON g.GoodID = gtp.PackageID
						AND gtp.IsDefault = 1
					INNER JOIN #avail_goods:active# ag
						ON gtp.GoodID = ag.GoodID
						AND ag.CityID = 2
					INNER JOIN #goods_prices:passive# gp_comp
						ON ag.GoodID = gp_comp.GoodID
						AND gp_comp.ZoneID = 1
						AND gp_comp.CategoryID = 1
				WHERE g.IsPackage = 1
					GROUP BY g.GoodID";
			$rows = $queryGateway->query($sql);
			foreach ($rows as $row) {
				$sql = "UPDATE #goods_prices:passive# SET Value = " . $row['PackagePrice'] . ", ZoneID = 1 WHERE GoodID = " . $row['PackageID'];
				$queryGateway->query($sql);
			}
		}
	}

	private function buildYandexYml() {
		if ($this->isExistSwapTable('goods_prices')) {
			$this->createYandexYml();
		}
	}

}
