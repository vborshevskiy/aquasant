<?php

namespace SoloCatalog\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloReplication\Service\YandexYml;

class YmlCreator extends AbstractReplicationTask {

    use YandexYml;

    /**
     * (non-PHPdoc)
     *
     * @see \SoloReplication\Service\TaskInterface::process()
     */
    public function process() {
        $this->createYandexYml();
    }

}
