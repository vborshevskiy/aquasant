<?php

namespace SoloCatalog\Task;

use SoloCatalog\Data\GoodNotesTable;
use SoloCatalog\Data\PropertyTemplatesToSiteCategoriesTable;
use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\Service\ProvidesWebservice;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloCatalog\Data\PropertiesTable;
use SoloCatalog\Data\PropertyTemplatesToCategoriesTable;
use SoloCatalog\Data\PropertyUnitsTable;
use SoloCatalog\Data\PropertyValuesTable;
use SoloCatalog\Data\GoodsFiltersAspectsMapper;
use SoloReplication\Service\DataMapper;
use SoloCatalog\Data\AllGoodsTable;
use SoloCatalog\Data\PropertiesGroupsTable;
use SoloCatalog\Data\GoodsToPropertyValuesTable;
use SoloCatalog\Data\PropertiesToPropertyTemplatesTable;
use Solo\Db\QueryGateway\QueryGateway;

class PropertiesReplicator extends AbstractReplicationTask {
	
	use ProvidesWebservice;

	const PROPERTY_VALUE_IMAGE_WIDTH = 80;

	const PROPERTY_VALUE_IMAGE_HEIGHT = 109;

	const PROPERTY_HINT_IMAGE_WIDTH = 198;

	const PROPERTY_HINT_IMAGE_HEIGHT = 178;

	/**
	 *
	 * @var AllGoodsTable
	 */
	protected $allGoodsTable;

	/**
	 *
	 * @var PropertiesTable
	 */
	protected $propertiesTable;

	/**
	 *
	 * @var PropertyTemplatesToCategoriesTable
	 */
	protected $propertyTemplatesToCategoriesTable;

	/**
	 *
	 * @var PropertyTemplatesToSiteCategoriesTable
	 */
	protected $propertyTemplatesToSiteCategoriesTable;

	/**
	 *
	 * @var PropertyUnitsTable
	 */
	protected $propertyUnitsTable;

	/**
	 *
	 * @var PropertyValuesTable
	 */
	protected $propertyValuesTable;

	/**
	 *
	 * @var $propertiesGroupsTable
	 */
	protected $propertiesGroupsTable;

	/**
	 *
	 * @var GoodsFiltersAspectsMapper
	 */
	protected $goodsFiltersAspectsMapper;

	/**
	 *
	 * @var GoodsToPropertyValuesTable
	 */
	protected $goodsToPropertyValuesTable;

	/**
	 *
	 * @var PropertiesToPropertyTemplatesTable
	 */
	protected $propertiesToPropertyTemplatesTable;

	/**
	 *
	 * @var GoodNotesTable
	 */
	protected $goodNotesTable;

	/**
	 *
	 * @param PropertiesTable $propertiesTable        	
	 * @param AllGoodsTable $allGoodsTable        	
	 * @param PropertyTemplatesToCategoriesTable $propertyTemplatesToCategoriesTable        	
	 * @param PropertyTemplatesToSiteCategoriesTable $propertyTemplatesToSiteCategoriesTable        	
	 * @param PropertyUnitsTable $propertyUnitsTable        	
	 * @param PropertyValuesTable $propertyValuesTable        	
	 * @param PropertiesGroupsTable $propertiesGroupsTable        	
	 * @param GoodsFiltersAspectsMapper $goodsFiltersAspectsMapper        	
	 * @param GoodsToPropertyValuesTable $goodsToPropertyValuesTable        	
	 * @param PropertiesToPropertyTemplatesTable $propertiesToPropertyTemplatesTable        	
	 */
	public function __construct(PropertiesTable $propertiesTable, AllGoodsTable $allGoodsTable, PropertyTemplatesToCategoriesTable $propertyTemplatesToCategoriesTable, PropertyTemplatesToSiteCategoriesTable $propertyTemplatesToSiteCategoriesTable, PropertyUnitsTable $propertyUnitsTable, PropertyValuesTable $propertyValuesTable, PropertiesGroupsTable $propertiesGroupsTable, GoodsFiltersAspectsMapper $goodsFiltersAspectsMapper, GoodsToPropertyValuesTable $goodsToPropertyValuesTable, PropertiesToPropertyTemplatesTable $propertiesToPropertyTemplatesTable, GoodNotesTable $goodNotesTable) 

	{
		$this->propertiesTable = $propertiesTable;
		$this->allGoodsTable = $allGoodsTable;
		$this->propertyTemplatesToCategoriesTable = $propertyTemplatesToCategoriesTable;
		$this->propertyTemplatesToSiteCategoriesTable = $propertyTemplatesToSiteCategoriesTable;
		$this->propertyUnitsTable = $propertyUnitsTable;
		$this->propertyValuesTable = $propertyValuesTable;
		$this->goodsFiltersAspectsMapper = $goodsFiltersAspectsMapper;
		$this->propertiesGroupsTable = $propertiesGroupsTable;
		$this->goodsToPropertyValuesTable = $goodsToPropertyValuesTable;
		$this->propertiesToPropertyTemplatesTable = $propertiesToPropertyTemplatesTable;
		$this->goodNotesTable = $goodNotesTable;
		
		$this->addSwapTable(
			[
				'goods_filters_aspects',
				'props',
				'templates_to_categories',
				'templates_to_site_categories',
				'prop_units',
				'prop_values',
				'prop_groups',
				'goods_to_propvalues',
				'props_to_templates',
				'good_notes' 
			]);
		
		ini_set('memory_limit', '1024M');
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \SoloReplication\Service\TaskInterface::process()
	 */
	public function process() {
		$this->processProperties();
		$this->processTemplatesToHardCategories();
		$this->processTemplatesToSiteCategories();
		$this->processPropertiesGroups();
		$this->processPropertyUnits();
		$this->processPropertyValues();
		$this->processGoodsToPropertyValues();
		$this->processPropertiesToPropertyTemplatesTable();
		$this->processGoodsFilterAspects();
		$this->processGoodNotes();
	}

	/**
	 *
	 * @return \SoloReplication\Service\DataMapper
	 */
	protected function createPropertiesMapper() {
		$context = $this;
		$mapper = new DataMapper();
		$mapper->setMappings(
			[
				'PropertyID' => '%d: Id',
				'PropertyName' => 'Name',
				'PropertyTypeID' => '%d: TypeId',
				'PropertyUnitID' => '%d: UnitId',
				'KindId' => '%d: KindId',
				'Hint' => 'Hint',
				'HintImage' => 'HintImage' 
			]);
		$mapper->setMapping('IsHidden', 'IsHidden', function ($field, $row) use($context) {
			return $context->helper()->parseBoolean($field);
		});
		$mapper->setMapping('SortValues', 'SortValues', function ($field, $row) use($context) {
			return $context->helper()->parseBoolean($field);
		});
		return $mapper;
	}

	/**
	 * Fill props table
	 */
	protected function processProperties() {
		$mapper = $this->createPropertiesMapper();
		
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetProductProperties'));
		$imagePath = $this->getImgPath() . '/pi/';
		
		if (!$rdr->isEmpty()) {
			$this->propertiesTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('props');
			$this->notify()->send($this->getNotifyMessage(__CLASS__, __METHOD__, 'GetProductProperties', []), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		$dataSet = [];
		foreach ($rdr as $row) {
			if (!empty($row['HintImage'])) {
				$imageName = $this->imageService()->saveBase64Image($row['HintImage'], $imagePath, $row['Id']);
				
				$row['HintImage'] = $imageName;
				$this->imageService()->resize($imagePath . $imageName, $imagePath . $imageName, self::PROPERTY_HINT_IMAGE_WIDTH, self::PROPERTY_HINT_IMAGE_HEIGHT);
			}
			
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->propertiesTable->insertSet($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->propertiesTable->insertSet($dataSet);
		}
		
		$this->log->info('Properties inserted = ' . $rdr->count());
	}

	/**
	 *
	 * @return \SoloReplication\Service\DataMapper
	 */
	protected function createTemplatesToHardCategoriesMapper() {
		$mapper = new DataMapper();
		$mapper->setMappings([
			'PropertyTemplateID' => '%d: ProductTemplateId',
			'CategoryID' => '%d: CategoryId' 
		]);
		
		return $mapper;
	}

	/**
	 *
	 * @return \SoloReplication\Service\DataMapper
	 */
	protected function createTemplatesToSiteCategoriesMapper() {
		$mapper = new DataMapper();
		$mapper->setMappings([
			'PropertyTemplateID' => '%d: ProductTemplateId',
			'SiteCategoryID' => '%d: SiteCategoryId' 
		]);
		
		return $mapper;
	}

	/**
	 *
	 * @return \SoloReplication\Service\DataMapper
	 */
	protected function createTemplatesToCategoriesMapper() {
		$mapper = new DataMapper();
		$mapper->setMappings([
			'PropertyTemplateID' => '%d: ProductTemplateId',
			'CategoryID' => '%d: CategoryId' 
		]);
		
		return $mapper;
	}

	/**
	 */
	protected function processTemplatesToHardCategories() {
		$mapper = $this->createTemplatesToCategoriesMapper();
		
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetProductTemplatesToNativeCategories'));
		if (!$rdr->isEmpty()) {
			$this->propertyTemplatesToCategoriesTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('templates_to_categories');
			$this->notify()->send($this->getNotifyMessage(__CLASS__, __METHOD__, 'GetProductTemplatesToNativeCategories', []), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		$dataSet = [];
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->propertyTemplatesToCategoriesTable->insertSet($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->propertyTemplatesToCategoriesTable->insertSet($dataSet);
		}
		
		$this->log->info('Templates to hard inserted = ' . $rdr->count());
		
		// @todo temporary
		$queryGateway = new QueryGateway();
		$sql = "INSERT INTO #templates_to_categories:passive# SET PropertyTemplateID = 115, CategoryID = 124, Prefix = NULL";
		// $queryGateway->query($sql);
	}

	protected function processTemplatesToSiteCategories() {
		$mapper = $this->createTemplatesToSiteCategoriesMapper();
		
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetProductTemplatesToSiteCategories'));
		if (!$rdr->isEmpty()) {
			$this->propertyTemplatesToSiteCategoriesTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('templates_to_site_categories');
			$this->notify()->send($this->getNotifyMessage(__CLASS__, __METHOD__, 'GetProductTemplatesToSiteCategories', []), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		$dataSet = [];
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->propertyTemplatesToSiteCategoriesTable->insertSet($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->propertyTemplatesToSiteCategoriesTable->insertSet($dataSet);
		}
		
		$this->log->info('Templates to site categories inserted = ' . $rdr->count());
		
		// @todo temporary
		$queryGateway = new QueryGateway();
		$sql = "INSERT INTO #templates_to_site_categories:passive# SET PropertyTemplateID = 115, SiteCategoryID = 53";
		// $queryGateway->query($sql);
	}

	/**
	 *
	 * @return \SoloReplication\Service\DataMapper
	 */
	protected function createPropertiesGroupsMapper() {
		$mapper = new DataMapper();
		$mapper->setMappings([
			'PropertyGroupID' => '%d: Id',
			'PropertyGroupName' => 'Name',
			'SortIndex' => '%d: SortIndex' 
		]);
		
		return $mapper;
	}

	protected function processPropertiesGroups() {
		$mapper = $this->createPropertiesGroupsMapper();
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetProductPropertyGroups'));
		if (!$rdr->isEmpty()) {
			$this->propertiesGroupsTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('prop_groups');
			$this->notify()->send($this->getNotifyMessage(__CLASS__, __METHOD__, 'GetProductPropertyGroups', []), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		$dataSet = [];
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->propertiesGroupsTable->insertSet($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->propertiesGroupsTable->insertSet($dataSet);
		}
		$this->log->info('Property values groups inserted = ' . $rdr->count());
	}

	/**
	 *
	 * @return \SoloReplication\Service\DataMapper
	 */
	protected function createPropertyUnitsMapper() {
		$mapper = new DataMapper();
		$mapper->setMappings([
			'PropertyUnitID' => '%d: Id',
			'UnitName' => 'Name' 
		]);
		
		return $mapper;
	}

	/**
	 */
	protected function processPropertyUnits() {
		$mapper = $this->createPropertyUnitsMapper();
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetProductPropertyUnits'));
		if (!$rdr->isEmpty()) {
			$this->propertyUnitsTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('prop_units');
			$this->notify()->send($this->getNotifyMessage(__CLASS__, __METHOD__, 'GetProductPropertyUnits', []), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		$dataSet = [];
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->propertyUnitsTable->insertSet($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->propertyUnitsTable->insertSet($dataSet);
		}
		
		$this->log->info('Property units inserted = ' . $rdr->count());
	}

	/**
	 *
	 * @return \SoloReplication\Service\DataMapper
	 */
	protected function createPropertyValuesMapper() {
		$mapper = new DataMapper();
		$mapper->setMappings(
			[
				'PropertyValueID' => '%d: Id',
				'PropertyID' => '%d: PropertyId',
				'PropertyValue' => 'Value',
				'ParentPropertyValueID' => '%d: parentPropertyValueId',
				'SortIndex' => '%d: SortIndex',
				'Image' => 'Image' 
			]);
		
		return $mapper;
	}

	/**
	 */
	protected function processPropertyValues() {
		$mapper = $this->createPropertyValuesMapper();
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetProductPropertyValues'));
		
		$imagePath = $this->getImgPath() . '/pvi/';
		
		if (!$rdr->isEmpty()) {
			$this->propertyValuesTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('prop_values');
			$this->notify()->send($this->getNotifyMessage(__CLASS__, __METHOD__, 'GetProductPropertyValues', []), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		$dataSet = [];
		foreach ($rdr as $row) {
			if (!empty($row['Image'])) {
				$imageName = $this->imageService()->saveBase64Image($row['Image'], $imagePath, $row['Id']);
				
				$row['Image'] = $imageName;
				$this->imageService()->resize($imagePath . $imageName, $imagePath . $imageName, self::PROPERTY_VALUE_IMAGE_WIDTH, self::PROPERTY_VALUE_IMAGE_HEIGHT);
			}
			
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->propertyValuesTable->insertSet($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->propertyValuesTable->insertSet($dataSet);
		}
		
		$this->log->info('Property values inserted = ' . $rdr->count());
	}

	/**
	 * Initialize data mapper for props to templates table
	 *
	 * @return DataMapper
	 */
	protected function createPropertyToTemplatesMapper() {
		$context = $this;
		$mapper = new DataMapper();
		$mapper->setMappings(
			[
				'PropertyID' => '%d: propertyId',
				'PropertyTemplateID' => '%d: productTemplateId',
				'PropertyGroupId' => '%d: propertyGroupId',
				'SortIndex' => '%d: sortIndex',
				'SortMode' => 'filterParams-sortMode',
				// 'ParentPropertyID' => '%d: ParentPropertyID',
				'MaxCount' => '%d: viewLimit',
				'FilterName' => 'filterParams-filterName' 
			]);
		$mapper->setMapping('IsHide', 'IsHide', function ($field, $row) use($context) {
			return $context->helper()->parseBoolean($field);
		});
		return $mapper;
	}

	/**
	 *
	 * @return \SoloReplication\Service\DataMapper
	 */
	protected function createGoodsToPropertyValuesMapper() {
		$context = $this;
		$mapper = new DataMapper();
		$mapper->setMappings(
			[
				'GoodID' => '%d: ProductId',
				'PropertyID' => '%d: PropertyId',
				'PropertyValueID' => '%d: ValueId',
				'ValueString' => 'StringValue',
				'ValueText' => 'TextValue',
				'ValueNumeric' => '%f: NumberValue' 
			]);
		$mapper->setMapping('ValueBoolean', 'BooleanValue', function ($field, $row) use($context) {
			return $context->helper()->parseBoolean($field);
		});
		
		return $mapper;
	}

	/**
	 */
	protected function processGoodsToPropertyValues() {
		$mapper = $this->createGoodsToPropertyValuesMapper();
		$totalCount = 0;
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetProductsToProductProperties'));
		if (!$rdr->isEmpty()) {
			$this->goodsToPropertyValuesTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('prop_values');
			$this->notify()->send($this->getNotifyMessage(__CLASS__, __METHOD__, 'GetProductsToProductProperties', []), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		if ($rdr->isEmpty()) {
			$this->removeSwapTable('goods_to_propvalues');
		}
		$dataSet = [];
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->goodsToPropertyValuesTable->insertSet($dataSet);
				$totalCount += sizeof($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->goodsToPropertyValuesTable->insertSet($dataSet);
			$totalCount += sizeof($dataSet);
		}
		$this->log->info('Goods to property values inserted = ' . $rdr->count());
		
		// @todo temporary OR not
		$queryGateway = new QueryGateway();
		$sql = "INSERT IGNORE INTO #goods_to_propvalues:passive#
        		SELECT p.* FROM (
					SELECT DISTINCT
        				gtp.PackageID AS GoodID,
        				gtp1.PropertyID,
			        	gtp1.PropertyValueID,
			        	gtp1.ValueString,
			        	gtp1.ValueNumber,
			        	gtp1.ValueBoolean,
			        	gtp1.ValueNumeric,
			        	gtp1.ValueText
        			FROM
        				#goods_to_packages# gtp
        			INNER JOIN #avail_goods# ag
        				ON gtp.GoodID = ag.GoodID
        				AND ag.CityID = 2
        			INNER JOIN
        				#goods_to_propvalues:passive# gtp1
        				ON gtp.GoodID = gtp1.GoodID
        			WHERE gtp.PackageID NOT IN (SELECT GoodID FROM #goods_to_propvalues:passive#)
        		) AS p";
		$queryGateway->query($sql);
		
		if (false) {
			$queryGateway = new QueryGateway();
			$sql = "INSERT IGNORE INTO #goods_to_propvalues:passive#
        		SELECT p.* FROM (
					SELECT DISTINCT
        				gtp.PackageID AS GoodID,
        				gtp1.PropertyID,
			        	gtp1.PropertyValueID,
			        	gtp1.ValueString,
			        	gtp1.ValueNumber,
			        	gtp1.ValueBoolean,
			        	gtp1.ValueNumeric,
			        	gtp1.ValueText
        			FROM
        				#goods_to_packages# gtp
        			INNER JOIN #avail_goods# ag
        				ON gtp.GoodID = ag.GoodID
        				AND ag.CityID = 2
					INNER JOIN #all_goods# g
						ON ag.GoodID = g.GoodID
						AND g.IsPackage = 1
						AND g.isVirtualPackage = 0
						AND g.ViewMode = 1
        			INNER JOIN
        				#goods_to_propvalues:passive# gtp1
        				ON gtp.GoodID = gtp1.GoodID
        			WHERE gtp.PackageID NOT IN (SELECT GoodID FROM #goods_to_propvalues:passive#)
        		) AS p";
			$queryGateway->query($sql);
		}
	}

	/**
	 *
	 * @return \SoloReplication\Service\DataMapper
	 */
	protected function createPropertiesToPropertyTemplatesMapper() {
		$context = $this;
		$mapper = new DataMapper();
		$mapper->setMappings(
			[
				'PropertyID' => '%d: PropertyId',
				'PropertyTemplateID' => '%d: TemplateId',
				'SortIndex' => '%d: PropertySortIndex',
				'PropertyGroupID' => '%d: PropertyGroupId' 
			]);
		
		$mapper->setMapping('DisplayInMiddleDescription', 'DisplayInMiddleDescription', function ($field, $row) use($context) {
			return $context->helper()->parseBoolean($field);
		});
		
		$mapper->setMapping('DisplayInSmallDescription', 'DisplayInSmallDescription', function ($field, $row) use($context) {
			return $context->helper()->parseBoolean($field);
		});
		
		$mapper->setMapping('IsComparable', 'Interactive', function ($field, $row) use($context) {
			return $context->helper()->parseBoolean($field);
		});
		
		$mapper->setMapping('IsSelectable', 'IsSelectable', function ($field, $row) use($context) {
			return $context->helper()->parseBoolean($field);
		});
		
		return $mapper;
	}

	/**
	 */
	protected function processPropertiesToPropertyTemplatesTable() {
		$mapper = $this->createPropertiesToPropertyTemplatesMapper();
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetProductPropertiesToTemplates'));
		if (!$rdr->isEmpty()) {
			$this->propertiesToPropertyTemplatesTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('props_to_templates');
			$this->notify()->send($this->getNotifyMessage(__CLASS__, __METHOD__, 'GetProductPropertiesToTemplates', []), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		$dataSet = [];
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->propertiesToPropertyTemplatesTable->insertSet($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->propertiesToPropertyTemplatesTable->insertSet($dataSet);
		}
		
		$this->log->info('Properties to property templates inserted = ' . $rdr->count());
	}

	/**
	 */
	protected function processGoodsFilterAspects() {
		if ($this->isExistSwapTable('goods_to_propvalues', 'props', 'props_to_templates')) {
			$this->goodsFiltersAspectsMapper->refillAspects();
			$this->log->info('Filters aspects filled');
		}
	}

	protected function createProductNotesMapper() {
		$mapper = new DataMapper();
		$mapper->setMappings([
			'GoodID' => '%d: ArticleId',
			'TypeID' => '%d: TypeId',
			'Text' => 'Text' 
		]);
		
		return $mapper;
	}

	protected function processGoodNotes() {
		$mapper = $this->createProductNotesMapper();
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetProductNotes'));
		if (!$rdr->isEmpty()) {
			$this->goodNotesTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('good_notes');
			$this->notify()->send($this->getNotifyMessage(__CLASS__, __METHOD__, 'GetProductNotes', []), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		$dataSet = [];
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->goodNotesTable->insertSet($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->goodNotesTable->insertSet($dataSet);
		}
		
		$this->log->info('Good notes inserted = ' . $rdr->count());
	}

}

?>