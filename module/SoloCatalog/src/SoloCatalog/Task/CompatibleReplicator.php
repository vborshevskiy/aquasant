<?php

namespace SoloCatalog\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloReplication\Service\DataMapper;
use SoloERP\Service\ProvidesWebservice;
use SoloCatalog\Data\CompatibleTable;

class CompatibleReplicator extends AbstractReplicationTask {

    use ProvidesWebservice;

    /**
     *
     * @var CompatibleTable
     */
    protected $compatibleTable;

    /**
     *
     * @param CompatibleTable $compatibleTable
     */
    public function __construct(CompatibleTable $compatibleTable) {
        $this->compatibleTable = $compatibleTable;
        $this->addSwapTable('compatible');
		ini_set('memory_limit', '1024M');
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SoloReplication\Service\TaskInterface::process()
     */
    public function process() {
        $this->processCompatible();
    }

    /**
     * Initialize data mapper for currencies table
     * 
     * @return DataMapper
     */
    protected function createCompatibleMapper() {
        $context = $this;
        $mapper = new DataMapper();
        $mapper->setMappings([
            'GoodID' => '%d: Id',
            'CompatibleGoodID' => '%d: SatelliteId',
            'SortIndex' => '%d: SortIndex',
        ]);
        $mapper->setMapping('OnOtherPages', 'OnOtherPages', function ($field, $row) use($context) {
            return $context->helper()->parseBoolean($field);
        });
        return $mapper;
    }

    /**
     * Fill currencies table
     */
    protected function processCompatible() {
        $this->log->info('Insert compatibles');

        $mapper = $this->createCompatibleMapper();

        $rdr = new UltimaJsonListReader($this->callWebMethod('GetSatellites'));
        if (!$rdr->isEmpty()) {
            $this->compatibleTable->truncate();
            $this->notify()->fileWarning(false);
        } else {
            $this->removeSwapTable('compatibles');
            $this->notify()->send($this->getNotifyMessage(__CLASS__,__METHOD__,'GetSatellites',[]), 'Пустой ответ по методу репликации');
            $this->notify()->fileWarning(true);
        }
        $dataSet = [];
        foreach ($rdr as $row) {
            $dataSet[] = $mapper->convert($row);
            if (500 == sizeof($dataSet)) {
                $this->compatibleTable->insertSet($dataSet);
                $dataSet = [];
            }
        }
        if (0 < sizeof($dataSet)) {
            $this->compatibleTable->insertSet($dataSet);
        }

        $this->log->info('Compatibles inserted = ' . $rdr->count());
    }

}

?>