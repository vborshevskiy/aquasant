<?php

namespace SoloCatalog\Task;

use SoloCatalog\Data\GoodsToPackagesTableInterface;
use SoloCatalog\Data\PackagesToGroupsTableInterface;
use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\Service\ProvidesWebservice;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloCatalog\Data\AllGoodsTableInterface;
use SoloReplication\Service\DataMapper;
use SoloCatalog\Data\WarrantyPeriodUnitsTableInterface;
use SoloCatalog\Data\DiscountReasonsTableInterface;
use Zend\Uri\Uri;

class AllGoodsReplicator extends AbstractReplicationTask {
	
	use ProvidesWebservice;

	/**
	 *
	 * @var AllGoodsTableInterface
	 */
	protected $allGoodsTable;

	/**
	 *
	 * @var WarrantyPeriodUnitsTableInterface
	 */
	protected $warrantyPeriodUnitsTable;

	/**
	 *
	 * @var DiscountReasonsTableInterface
	 */
	protected $discountReasonsTable;

	/**
	 *
	 * @var GoodsToPackagesTableInterface
	 */
	protected $goodsToPackagesTable;

	/**
	 *
	 * @var PackagesToGroupsTableInterface
	 */
	protected $packagesToGroupsTable;

	/**
	 *
	 * @param AllGoodsTableInterface $allGoodsTable        	
	 * @param WarrantyPeriodUnitsTableInterface $warrantyPeriodUnitsTable        	
	 */
	public function __construct(AllGoodsTableInterface $allGoodsTable, WarrantyPeriodUnitsTableInterface $warrantyPeriodUnitsTable, DiscountReasonsTableInterface $discountReasonsTable, GoodsToPackagesTableInterface $goodsToPackagesTable, PackagesToGroupsTableInterface $packagesToGroupsTable) {
		$this->allGoodsTable = $allGoodsTable;
		$this->warrantyPeriodUnitsTable = $warrantyPeriodUnitsTable;
		$this->discountReasonsTable = $discountReasonsTable;
		$this->goodsToPackagesTable = $goodsToPackagesTable;
		$this->packagesToGroupsTable = $packagesToGroupsTable;
		
		$this->addSwapTable('all_goods', 'warranty_period_units', 'discount_reasons', 'goods_to_packages', 'packages_to_groups');
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \SoloReplication\Service\TaskInterface::process()
	 */
	public function process() {
		$this->processGoods();
		$this->processWarrantyPeriodUnits();
		//$this->processDiscountReasons();
		$this->processGoodsToPackages();
		$this->processPackagesToGroups();
	}

	/**
	 * Initialize data mapper for filling all_goods table
	 *
	 * @return DataMapper
	 */
	protected function createGoodsMapper() {
		$context = $this;
		$mapper = new DataMapper();
		$mapper->setMappings(
			[
				'GoodID' => '%d: Id',
				'CategoryID' => '%d: CategoryId',
				'GoodName' => 'Name',
				'AccusativeCategoryName' => 'AccusativeCategoryName',
				'AccusativeDetailName' => 'AccusativeDetailName',
				'GenitiveDetailName' => 'GenitiveDetailName',
				'AblativeDetailName' => 'AblativeDetailName',
				'ShortVendorName' => 'ShortVendorName',
				'BrandID' => '%d: BrandId',
				'SerieID' => 'SerieId',
				'Weight' => '%f: Weight',
				'Volume' => '%f: Volume',
				'WarrantyPeriod' => 'WarrantyPeriod',
				'WarrantyPeriodUnitID' => '%d: WarrantyPeriodUnitId',
				'GoodDescription' => 'Description',
				'OriginalGoodID' => '%d: OriginalProductId',
				'DiscountReasonID' => '%d: DiscountReasonId',
				'DiscountText' => 'AutoDiscountDescription',
				'Model' => 'Model',
				'TemplateID' => '%d: FeatureSetId',
				'VendorCode' => 'VendorCode',
				'VideoUrl' => 'VideoUrl',
				'VideoName' => 'VideoName',
				'ViewMode' => '%d: ViewModeID',
				'GoogleCategoryName' => 'GoogleCategory'
			]);
		
		$mapper->setMapping(
			'VideoUrl', 
			'VideoUrl', 
			function ($field) {
				$uri = new Uri($field);
				$uid = '';
				$query = $uri->getQueryAsArray();
				if (isset($query['v'])) {
					$uid = $query['v'];
				} elseif (false !== stripos('embed', $uri->getPath())) {
					$path = explode('/', trim($uri->getPath(), '/'));
					$uid = $path[count($path) - 1];
				} else {
					$path = explode('/', trim($uri->getPath(), '/'));
					if (1 == count($path)) {
						$uid = current($path);
					}
				}
				$videoUrl = $field;
				if (!empty($uid)) {
					$videoUrl = sprintf('https://www.youtube.com/embed/%s', $uid);
				}
				return $videoUrl;
			});
		
		$mapper->setMapping('AddDate', 'CreationDate', function ($field) {
			return $this->helper()->parseJSONDate($field);
		});
		$mapper->setMapping('GoodEngName', '', function ($field, $row) use($context) {
			return $this->helper()->translitUrl($row['Name']);
		});
		$mapper->setMapping('BrandWarranty', 'VendorWarranty', function ($field) {
			return $this->helper()->parseBoolean($field);
		});
		$mapper->setMapping('IsPackage', 'IsPackage', function ($field, $row) use($context) {
			return $context->helper()->parseBoolean($field);
		});
		$mapper->setMapping('IsVirtualPackage', 'IsVirtualPackage', function ($field, $row) use($context) {
			return $context->helper()->parseBoolean($field);
		});
		return $mapper;
	}

	/**
	 * Fill all goods table
	 */
	protected function processGoods() {
		$this->log->info('Insert all goods');
		$mapper = $this->createGoodsMapper();
		$pRedis = $this->predisClient;
		$rdr = new UltimaJsonListReader($this->callWebMethod('RedisGetProducts'));
		if (!$rdr->isEmpty()) {
			$this->allGoodsTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('all_goods');
			$this->notify()->send($this->getNotifyMessage(__CLASS__, __METHOD__, 'RedisGetProducts', []), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		$totalCount = 0;
		$dataSet = [];
		$keys = $rdr->getValue("Keys");
		if (count($keys) > 0) {
			foreach ($keys as $row) {
				$pack = json_decode($pRedis->get($row));
				foreach ($pack as $item) {
					$item = new \ArrayObject((array)$item);
					if (2584 == $item['Id']) {
						print_r($item);
					}
					$dataSet[] = $mapper->convert($item);
					if (500 == sizeof($dataSet)) {
						$this->allGoodsTable->insertSet($dataSet, true);
						$totalCount += sizeof($dataSet);
						$dataSet = [];
					}
				}
				$pRedis->del($row);
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->allGoodsTable->insertSet($dataSet, true);
			$totalCount += sizeof($dataSet);
		}
		$this->log->info('All goods inserted = ' . $totalCount);
	}

	/**
	 * Initialize data mapper for warranty period units table
	 *
	 * @return DataMapper
	 */
	protected function createWarrantyPeriodUnitsMapper() {
		$mapper = new DataMapper();
		$mapper->setMappings(
			[
				'WarrantyPeriodUnitID' => '%d: Id',
				'WarrantyPeriodUnitName' => 'Name',
				'WarrantyPeriodUnitForm1' => 'TextForm1',
				'WarrantyPeriodUnitForm2' => 'TextForm2',
				'WarrantyPeriodUnitForm3' => 'TextForm3' 
			]);
		return $mapper;
	}

	/**
	 * Fill warranty period units table
	 */
	protected function processWarrantyPeriodUnits() {
		$this->log->info('Insert warranty period units');
		
		$mapper = $this->createWarrantyPeriodUnitsMapper();
		
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetWarrantyPeriodUnits'));
		if (!$rdr->isEmpty()) {
			$this->warrantyPeriodUnitsTable->truncate();
		}
		$dataSet = [];
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->warrantyPeriodUnitsTable->insertSet($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->warrantyPeriodUnitsTable->insertSet($dataSet);
		}
		
		$this->log->info('Warranty period units inserted = ' . $rdr->count());
	}

	/**
	 * Initialize data mapper for discount reasons table
	 *
	 * @return DataMapper
	 */
	protected function createDiscountReasonsMapper() {
		$mapper = new DataMapper();
		$mapper->setMappings([
			'DiscountReasonID' => '%d: Id',
			'DiscountReasonName' => 'Name' 
		]);
		return $mapper;
	}

	/**
	 * Fill discount reasons table
	 */
	protected function processDiscountReasons() {
		$this->log->info('Insert discount reasons');
		
		$mapper = $this->createDiscountReasonsMapper();
		
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetDiscountReasons'));
		if (!$rdr->isEmpty()) {
			$this->discountReasonsTable->truncate();
		}
		$dataSet = [];
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->discountReasonsTable->insertSet($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->discountReasonsTable->insertSet($dataSet);
		}
		
		$this->log->info('Discount reasons inserted = ' . $rdr->count());
	}

	protected function createGoodsToPackagesMapper() {
		$context = $this;
		$mapper = new DataMapper();
		$mapper->setMappings([
			'PackageID' => '%d: PackageId',
			'GroupID' => '%d: GroupId',
			'GoodID' => '%d: ArticleId',
			'SortIndex' => '%d: SortIndex'
		]);
		
		$mapper->setMapping('IsDefault', 'IsDefault', function ($field, $row) use($context) {
			return $context->helper()->parseBoolean($field);
		});
		
		return $mapper;
	}

	protected function processGoodsToPackages() {
		$this->log->info('Insert goods to packages');
		
		$mapper = $this->createGoodsToPackagesMapper();
		
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetProductsToPackages'));
		if (!$rdr->isEmpty()) {
			$this->goodsToPackagesTable->truncate();
		}
		$dataSet = [];
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->goodsToPackagesTable->insertSet($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->goodsToPackagesTable->insertSet($dataSet);
		}
		
		$this->log->info('Goods to packages inserted = ' . $rdr->count());
	}

	protected function createPackagesToGroupsMapper() {
		$context = $this;
		$mapper = new DataMapper();
		$mapper->setMappings(
			[
				'PackageID' => '%d: PackageId',
				'GroupID' => '%d: GroupId',
				'PickingMethodID' => '%d: PickingMethodId',
				'SortIndex' => '%d: SortOrder' 
			]);
		
		$mapper->setMapping('IsMandatory', 'IsMandatory', function ($field, $row) use($context) {
			return $context->helper()->parseBoolean($field);
		});
		$mapper->setMapping('IsMain', 'IsMain', function ($field, $row) use($context) {
			return $context->helper()->parseBoolean($field);
		});
		
		return $mapper;
	}

	protected function processPackagesToGroups() {
		$this->log->info('Insert packages to groups');
		
		$mapper = $this->createPackagesToGroupsMapper();
		
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetPackagesToGroups'));
		if (!$rdr->isEmpty()) {
			$this->packagesToGroupsTable->truncate();
		}
		$dataSet = [];
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->packagesToGroupsTable->insertSet($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->packagesToGroupsTable->insertSet($dataSet);
		}
		
		$this->log->info('Packages to groups inserted = ' . $rdr->count());
	}

}

?>