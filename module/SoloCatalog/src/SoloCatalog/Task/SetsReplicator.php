<?php

namespace SoloCatalog\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\Service\ProvidesWebservice;
use SoloCatalog\Data\SetsTableInterface;
use SoloCatalog\Data\SetsGroupsTableInterface;
use SoloCatalog\Data\SetsToGoodsTableInterface;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloReplication\Service\DataMapper;

class SetsReplicator extends AbstractReplicationTask {
	
	use ProvidesWebservice;

	/**
	 *
	 * @var SetsTableInterface
	 */
	protected $setsTable;

	/**
	 *
	 * @var SetsGroupsTableInterface
	 */
	protected $groupsTable;

	/**
	 *
	 * @var SetsToGoodsTableInterface
	 */
	protected $setsToGoodsTable;

	/**
	 *
	 * @param SetsTableInterface $setsTable        	
	 * @param SetsGroupsTableInterface $groupsTable        	
	 * @param SetsToGoodsTableInterface $setsToGoodsTable        	
	 */
	public function __construct(SetsTableInterface $setsTable, SetsGroupsTableInterface $groupsTable, SetsToGoodsTableInterface $setsToGoodsTable) {
		$this->setsTable = $setsTable;
		$this->groupsTable = $groupsTable;
		$this->setsToGoodsTable = $setsToGoodsTable;
		
		$this->addSwapTable('sets', 'sets_groups', 'sets_to_goods');
	}

	/**
	 *
	 * @see \SoloReplication\Service\TaskInterface::process()
	 */
	public function process() {
		$this->processSets();
		$this->processGroups();
		$this->processSetsToGoods();
	}

	/**
	 * Update sets
	 */
	private function processSets() {
		$this->log->info('Begin sets');
		
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetCategoriesProductSets'));
		if (!$rdr->isEmpty()) {
			$this->setsTable->truncate();
		}
		$dataSet = [];
		$mapper = $this->createSetsMapper();
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->setsTable->insertSet($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->setsTable->insertSet($dataSet);
		}
		
		$this->log->info('Inserted = ' . $rdr->count());
		
		$this->log->info('End sets');
	}

	/**
	 *
	 * @return \SoloReplication\Service\DataMapper
	 */
	private function createSetsMapper() {
		$context = $this;
		
		$mapper = new DataMapper(
			[
				'SetID' => '%d: SetID',
				'SetGroupID' => '%d: SetGroupID',
				'SetName' => 'SetName',
				'SetMetaTitle' => 'SetMetaTitle',
				'SetMetaDescription' => 'SetMetaDescription',
				'SetMetaKeywords' => 'SetMetaKeywords',
				'SortIndex' => '%d: SortIndex' 
			]);
		$mapper->setMapping('SetUrlPart', '', function ($field, $row) use($context) {
			return (!empty($row['SetUrlPart']) ? $this->helper()->editUrlName($row['SetUrlPart']) : $this->helper()->translitUrl($row['SetName']));
		});
		$mapper->setMapping('SelectedFeatures', 'SelectedFeatures', function ($field) {
			return json_encode($field, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		});
		$mapper->setMapping('IsMain', 'IsMain', function ($field, $row) use($context) {
			return $context->helper()->parseBoolean($field);
		});
		return $mapper;
	}

	/**
	 * Update groups
	 */
	private function processGroups() {
		$this->log->info('Begin groups');
		
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetCategoriesProductSetGroups'));
		if (!$rdr->isEmpty()) {
			$this->groupsTable->truncate();
		}
		$dataSet = [];
		$mapper = $this->createGroupsMapper();
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->groupsTable->insertSet($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->groupsTable->insertSet($dataSet);
		}
		
		$this->log->info('Inserted = ' . $rdr->count());
		
		$this->log->info('End groups');
	}

	/**
	 *
	 * @return \SoloReplication\Service\DataMapper
	 */
	private function createGroupsMapper() {
		$context = $this;
		$mapper = new DataMapper([
			'SetGroupID' => '%d: SetGroupID',
			'SoftCategoryID' => '%d: CategoryID',
			'ParentSetID' => '%d: ParentSetID',
			'SortIndex' => '%d: SortIndex' 
		]);
		$mapper->setMapping('IsMain', 'IsMain', function ($field, $row) use($context) {
			return $context->helper()->parseBoolean($field);
		});
		return $mapper;
	}

	/**
	 * Update links products to sets
	 */
	private function processSetsToGoods() {
		$this->log->info('Begin sets to groups');
		
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetCategoriesProductSetsProducts'));
		if (!$rdr->isEmpty()) {
			$this->setsToGoodsTable->truncate();
		}
		$dataSet = [];
		foreach ($rdr as $row) {
			foreach ($row['ProductIDs'] as $productId) {
				$dataSet[] = [
					'SetID' => $row['SetID'],
					'GoodID' => $productId 
				];
				if (500 == sizeof($dataSet)) {
					$this->setsToGoodsTable->insertSet($dataSet);
					$dataSet = [];
				}
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->setsToGoodsTable->insertSet($dataSet);
		}
		
		$this->log->info('Inserted = ' . $rdr->count());
		
		$this->log->info('End sets to groups');
	}

}

?>