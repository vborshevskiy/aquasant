<?php

namespace SoloCatalog\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloReplication\Service\DataMapper;
use SoloERP\Service\ProvidesWebservice;
use SoloCatalog\Data\DeliveryPricesTableInterface;
use SoloCatalog\Data\DeliveryServicePricesTable;
use Solo\Db\QueryGateway\QueryGateway;

class DeliveryReplicator extends AbstractReplicationTask {
    
    use ProvidesWebservice;

    /**
     *
     * @var DeliveryPricesTableInterface
     */
    protected $deliveryPricesTable;
    
    /**
     *
     * @var DeliveryServicePricesTable
     */
    protected $deliveryServicePricesTable;

    /**
     *
     * @param BrandsTable $deliveryPricesTable
     */
    public function __construct(DeliveryPricesTableInterface $deliveryPricesTable, DeliveryServicePricesTable $deliveryServicePricesTable) {
        $this->deliveryPricesTable = $deliveryPricesTable;
        $this->deliveryServicePricesTable = $deliveryServicePricesTable;
        $this->addSwapTable('delivery_prices', 'delivery_service_prices');
        $this->scriptNotRespondingTimeInMinutes = 60 * 24;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SoloReplication\Service\TaskInterface::process()
     */
    public function process() {
        $this->processDeliveryPrices();
//        $this->processDeliveryServicePrices();
    }

    /**
     * Initialize data mapper for delivery prices table
     * 
     * @return DataMapper
     */
    protected function createDeliveryPricesMapper() {
        $mapper = new DataMapper();
        $mapper->setMappings([
            'CityID' => '%d: LocationId',
            'GoodID' => '%d: ArticleId',
            'DeliveryPrice' => '%f: DeliveryPrice',
        ]);
        return $mapper;
    }
    
    /**
     * Fill delivery prices table
     */
    protected function processDeliveryPrices() {
        $this->log->info('Insert delivery prices');

        $mapper = $this->createDeliveryPricesMapper();

        $rdr = new UltimaJsonListReader($this->callWebMethod('GetDefautDeliveryPrices'));
        if (!$rdr->isEmpty()) {
            $this->deliveryPricesTable->truncate();
            $this->notify()->fileWarning(false);
        } else {
            $this->removeSwapTable('delivery_prices');
            $this->notify()->send($this->getNotifyMessage(__CLASS__,__METHOD__,'GetDefautDeliveryPrices',[]), 'Пустой ответ по методу репликации');
            $this->notify()->fileWarning(true);
        }
        $dataSet = [];
        foreach ($rdr as $row) {
            $dataSet[] = $mapper->convert($row);
            if (500 == sizeof($dataSet)) {
                $this->deliveryPricesTable->insertSet($dataSet);
                $dataSet = [];
            }
        }
        if (0 < sizeof($dataSet)) {
            $this->deliveryPricesTable->insertSet($dataSet);
        }
        
        $queryGateway = new QueryGateway();
        $sql = "UPDATE #delivery_prices:passive# SET CityID = 2 WHERE CityID = 77";
        $queryGateway->query($sql);

        $this->log->info('Delivery prices inserted = ' . $rdr->count());
    }
    
    /**
     * Initialize data mapper for delivery prices table
     * 
     * @return DataMapper
     */
    protected function createDeliveryServicePricesMapper() {
        $mapper = new DataMapper();
        $mapper->setMappings([
            'DeliveryServiceID' => '%d: DeliveryServiceID',
            'DistanceFromCity' => '%d: Distance',
            'DistanceFromCityDescription' => 'DistanceFromCityBorder',
            'Price' => '%f: Price',
            'DeliveryServiceDescription' => 'DeliveryService',
        ]);
        return $mapper;
    }

    /**
     * Fill delivery service prices table
     */
    protected function processDeliveryServicePrices() {
        $this->log->info('Insert service delivery prices');

        $mapper = $this->createDeliveryServicePricesMapper();

        $rdr = new UltimaJsonListReader($this->callWebMethod('GetDeliveryServicePrices'));
        if (!$rdr->isEmpty()) {
            $this->deliveryServicePricesTable->truncate();
            $this->notify()->fileWarning(false);
        } else {
            $this->removeSwapTable('delivery_service_prices');
            $this->notify()->send($this->getNotifyMessage(__CLASS__,__METHOD__,'GetDeliveryServicePrices',[]), 'Пустой ответ по методу репликации');
            $this->notify()->fileWarning(true);
        }
        $dataSet = [];
        foreach ($rdr as $row) {
            $dataSet[] = $mapper->convert($row);
            if (500 == sizeof($dataSet)) {
                $this->deliveryServicePricesTable->insertSet($dataSet);
                $dataSet = [];
            }
        }
        if (0 < sizeof($dataSet)) {
            $this->deliveryServicePricesTable->insertSet($dataSet);
        }

        $this->log->info('Delivery service prices inserted = ' . $rdr->count());
    }

}
