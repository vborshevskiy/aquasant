<?php

namespace SoloCatalog\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloReplication\Service\DataMapper;
use SoloERP\Service\ProvidesWebservice;
use SoloCatalog\Data\BrandsTable;

class BrandsReplicator extends AbstractReplicationTask {

    use ProvidesWebservice;

    /**
     *
     * @var BrandsTable
     */
    protected $brandsTable;

    /**
     *
     * @param BrandsTable $brandsTable
     */
    public function __construct(BrandsTable $brandsTable) {
        $this->brandsTable = $brandsTable;
        $this->addSwapTable('brands');
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SoloReplication\Service\TaskInterface::process()
     */
    public function process() {
        $this->processBrands();
    }

    /**
     * Initialize data mapper for brands table
     * 
     * @return DataMapper
     */
    protected function createBrandsMapper() {
        $context = $this;
        $mapper = new DataMapper();
        $mapper->setMappings([
            'BrandID' => '%d: Id',
            'BrandName' => 'Name',
        ]);
        $mapper->setMapping('BrandUID', '', function ($field, $row) use($context) {
            return $this->helper()->translitUrl($row['Name']);
        });
        return $mapper;
    }

    /**
     * Fill brands table
     */
    protected function processBrands() {
        $this->log->info('Insert brands');

        $mapper = $this->createBrandsMapper();

        $rdr = new UltimaJsonListReader($this->callWebMethod('GetBrands'));
        if (!$rdr->isEmpty()) {
            $this->brandsTable->truncate();
            $this->notify()->fileWarning(false);
        } else {
            $this->removeSwapTable('brands');
            $this->notify()->send($this->getNotifyMessage(__CLASS__,__METHOD__,'GetBrands',[]), 'Пустой ответ по методу репликации');
            $this->notify()->fileWarning(true);
        }
        $dataSet = [];
        foreach ($rdr as $row) {
            $dataSet[] = $mapper->convert($row);
            if (500 == sizeof($dataSet)) {
                $this->brandsTable->insertSet($dataSet);
                $dataSet = [];
            }
        }
        if (0 < sizeof($dataSet)) {
            $this->brandsTable->insertSet($dataSet);
        }

        $this->log->info('Brands inserted = ' . $rdr->count());
    }

}

?>