<?php

namespace SoloCatalog\Task;

use SoloCatalog\Data\FilesTable;
use SoloCatalog\Data\FilesToGoodsTable;
use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloReplication\Service\DataMapper;
use SoloERP\Service\ProvidesWebservice;

class FilesReplicator extends AbstractReplicationTask {

    use ProvidesWebservice;

    /**
     *
     * @var FilesTable
     */
    protected $filesTable;

    /**
     * @var FilesToGoodsTable
     */
    protected $filesToGoodsTable;

    /**
     *
     * @param FilesTable $filesTable
     * @param FilesToGoodsTable $filesToGoodsTable
     */
    public function __construct(FilesTable $filesTable, FilesToGoodsTable $filesToGoodsTable) {
        $this->filesTable = $filesTable;
        $this->filesToGoodsTable = $filesToGoodsTable;

        $this->addSwapTable('files', 'files_to_goods');
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SoloReplication\Service\TaskInterface::process()
     */
    public function process() {
        $this->processFiles();
        $this->processFilesToGoods();
    }

    /**
     * @return DataMapper
     */
    protected function createFilesMapper() {
        $context = $this;
        $mapper = new DataMapper();
        $mapper->setMappings([
            'FileID' => '%d: ID',
            'FileSize' => '%d: FileSize',
            'Comments' => 'Comments',
        ]);

        $mapper->setMapping('Name', 'FileName', function ($field, $row) use($context) {
            return substr($field, 0, strrpos($field, '.'));
        });

        $mapper->setMapping('FileName', 'FileName', function ($field, $row) use($context) {
            $name = substr($field, 0, strrpos($field, '.'));
            $extension = substr($field, strrpos($field, '.') + 1);
            return $this->helper()->translitUrl($name) . '.' . $extension;
        });

        $mapper->setMapping('FileExtension', 'FileName', function ($field, $row) use($context) {
            return substr($field, strrpos($field, '.') + 1);
        });

        return $mapper;
    }

    /**
     * Fill brands table
     */
    protected function processFiles() {
        $this->log->info('Insert files');
        $mapper = $this->createFilesMapper();
        $pRedis = $this->predisClient;
        $rdr = new UltimaJsonListReader($this->callWebMethod('RedisGetBrandFiles'));
        if (!$rdr->isEmpty()) {
            $this->filesTable->truncate();
            $this->notify()->fileWarning(false);
        } else {
            $this->removeSwapTable('files');
            $this->notify()->send($this->getNotifyMessage(__CLASS__,__METHOD__, 'RedisGetBrandFiles', []), 'Пустой ответ по методу репликации');
            $this->notify()->fileWarning(true);
        }
        $totalCount = 0;
        $dataSet = [];
        $keys = $rdr->getValue("Keys");
        if (count($keys) > 0) {
            foreach ($keys as $row) {
                $pack = json_decode($pRedis->get($row));
                foreach ($pack as $item) {
                    $item = new \ArrayObject((array) $item);
                    if (!empty($item['FileData'])) {
                        $data = base64_decode($item['FileData']);
                        unset($item['FileData']);

                        $item = $mapper->convert($item);
                        $dataSet[] = $item;

                        $this->saveFile($item['FileName'], $data);

                        if (500 == sizeof($dataSet)) {
                            $this->filesTable->insertSet($dataSet, true);
                            $totalCount += sizeof($dataSet);
                            $dataSet = [];
                        }
                    }
                }
                $pRedis->del($row);
            }
        }
        if (0 < sizeof($dataSet)) {
            $this->filesTable->insertSet($dataSet, true);
            $totalCount += sizeof($dataSet);
        }
        $this->log->info('All files inserted = ' . $totalCount);
    }

    /**
     * @param $fileName
     * @param $data
     */
    protected function saveFile($fileName, $data) {
        $filePath = getcwd() . '/public/files/';
        if (!file_exists($filePath)) {
            @mkdir($filePath);
        }

        file_put_contents($filePath . $fileName, $data);
    }

    /**
     * @return DataMapper
     */
    protected function createFilesToGoodsMapper() {
        $mapper = new DataMapper();
        $mapper->setMappings([
            'FileID' => '%d: FileId',
            'GoodID' => '%d: ProductId',
        ]);
        return $mapper;
    }

    protected function processFilesToGoods() {
        $this->log->info('Insert files to goods');

        $mapper = $this->createFilesToGoodsMapper();

        $rdr = new UltimaJsonListReader($this->callWebMethod('GetProductFiles'));
        if (!$rdr->isEmpty()) {
            $this->filesToGoodsTable->truncate();
            $this->notify()->fileWarning(false);
        } else {
            $this->removeSwapTable('brands');
            $this->notify()->send($this->getNotifyMessage(__CLASS__,__METHOD__,'GetProductFiles',[]), 'Пустой ответ по методу репликации');
            $this->notify()->fileWarning(true);
        }
        $dataSet = [];
        foreach ($rdr as $row) {
            $dataSet[] = $mapper->convert($row);
            if (500 == sizeof($dataSet)) {
                $this->filesToGoodsTable->insertSet($dataSet);
                $dataSet = [];
            }
        }
        if (0 < sizeof($dataSet)) {
            $this->filesToGoodsTable->insertSet($dataSet);
        }

        $this->log->info('Files to goods inserted = ' . $rdr->count());
    }
}

?>