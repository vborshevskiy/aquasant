<?php

namespace SoloCatalog\Task;

use SoloReplication\Service\AbstractReplicationTask;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use Solo\Collection\NestedSet\NestedSetBuilder;
use SoloCatalog\Data\SoftCategoriesTable;
use SoloReplication\Service\DataMapper;
use SoloERP\Service\ProvidesWebservice;
use SoloCatalog\Data\HardToSoftTable;
use SoloCatalog\Data\GoodsToSoftCategoriesTable;
use Solo\Db\QueryGateway\QueryGateway;

class CatalogReplicator extends AbstractReplicationTask {
	
	use ProvidesWebservice;

	/**
	 *
	 * @var SoftCategoriesTable
	 */
	protected $softCategoriesTable;

	/**
	 *
	 * @var HardToSoftTable
	 */
	protected $hardToSoftTable;

	/**
	 *
	 * @var GoodsToSoftCategoriesTable
	 */
	protected $goodsToSoftCategoriesTable;

	/**
	 *
	 * @var string
	 */
	protected $sphinxConfigPath;

	/**
	 *
	 * @param SoftCategoriesTable $softCategoriesTable        	
	 * @param HardToSoftTable $hardToSoftTable        	
	 */
	public function __construct(SoftCategoriesTable $softCategoriesTable, HardToSoftTable $hardToSoftTable, GoodsToSoftCategoriesTable $goodsToSoftCategories, $sphinxConfigPath) {
		$this->softCategoriesTable = $softCategoriesTable;
		$this->hardToSoftTable = $hardToSoftTable;
		$this->goodsToSoftCategoriesTable = $goodsToSoftCategories;
		$this->sphinxConfigPath = $sphinxConfigPath;
		
		$this->addSwapTable('soft_categories', 'hard_to_soft', 'goods_to_soft_categories');

		ini_set('memory_limit', '1024M');
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \SoloReplication\Service\TaskInterface::process()
	 */
	public function process() {
		$this->processSoftCategories();
		$this->processHardToSoft();
		$this->processGoodsToSoftCategories();
		$this->rebuildSearchIndex();
	}

	/**
	 * Initialize data mapper for soft categories table
	 *
	 * @return DataMapper
	 */
	protected function createSoftCategoriesMapper() {
		$context = $this;
		$mapper = new DataMapper();
		$mapper->setMappings([
			'SoftCategoryID' => '%d: Id',
			'CategoryName' => 'Name',
			'SortIndex' => '%d: SortIndex',
			'ParentID' => '%d: ParentId',
			'TemplateID' => '%d: TemplateId'
		]);
		$mapper->setMapping('cLevel', '', function () {
			return '-1';
		});
		$mapper->setMapping('cLeft', '', function () {
			return '-1';
		});
		$mapper->setMapping('cRight', '', function () {
			return '-1';
		});
		$mapper->setMapping('children', '', function () {
			return '0';
		});
		$mapper->setMapping('IsShowLinks', '', function () {
			return '0';
		});
		$mapper->setMapping('Link', '', function () {
			return '';
		});
		$mapper->setMapping('LinkedID', '', function () {
			return '0';
		});
		$mapper->setMapping('IsUrl', '', function () {
			return '0';
		});
		
		$mapper->setMapping(
			'CategoryUID', 
			'', 
			function ($field, $row) use($context) {
				return (!empty($row['UrlName']) ? $this->helper()->editUrlName($row['UrlName']) : $this->helper()->translitUrl($row['Name']));
			});
		$mapper->setMapping('BuyText', '', function ($field, $row) {
			return (!empty($row['BuyName']) ? $row['BuyName'] : '');
		});
		$mapper->setMapping('Description', '', function ($field, $row) {
			return (!empty($row['Description']) ? $row['Description'] : '');
		});
		$mapper->setMapping('RealSoftCategoryID', '', function ($field, $row) {
			return (!empty($row['UrlId']) ? intval($row['UrlId']) : 0);
		});
		
		return $mapper;
	}

	/**
	 * Fill soft categories table
	 */
	protected function processSoftCategories() {
		$this->log->info('Insert soft categories');
		
		$mapper = $this->createSoftCategoriesMapper();
		$params = [];
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetSiteCategories', $params));
		if (!$rdr->isEmpty()) {
			$this->softCategoriesTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('soft_categories');
			$this->notify()->send($this->getNotifyMessage(__CLASS__, __METHOD__, 'GetSiteCategories', $params), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		$dataSet = [];
		$nsBuilder = new NestedSetBuilder();
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->softCategoriesTable->insertSet($dataSet);
				$dataSet = [];
			}
			$nsBuilder->addNode(intval($row->offsetGet('Id')), intval($row->offsetGet('ParentId')));
		}
		if (0 < sizeof($dataSet)) {
			$this->softCategoriesTable->insertSet($dataSet);
		}
		
		$this->softCategoriesTable->updateLinkedCategoriesUids();
		
		// nested set initialize
		$list = $nsBuilder->buildList();
		foreach ($list as $item) {
			$this->softCategoriesTable->updateNestedSetParameters($item['id'], $item['level'], $item['left'], $item['right']);
		}
		
		$this->log->info('Soft categories inserted = ' . $rdr->count());
	}

	/**
	 * Initialize hard to soft table mapper
	 *
	 * @return DataMapper
	 */
	protected function createHardToSoftMapper() {
		$mapper = new DataMapper();
		$mapper->setMappings([
			'HardCategoryID' => '%d: CategoryId',
			'SoftCategoryID' => '%d: SiteCategoryId' 
		]);
		
		return $mapper;
	}

	/**
	 * Fill hard to soft table
	 */
	protected function processHardToSoft() {
		$this->log->info('Insert hard to soft');
		$mapper = $this->createHardToSoftMapper();
		$params = [];
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetNativeToSiteCategories', $params));
		if (!$rdr->isEmpty()) {
			$this->hardToSoftTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('hard_to_soft');
			$this->notify()->send($this->getNotifyMessage(__CLASS__, __METHOD__, 'GetNativeToSiteCategories', $params), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		$dataSet = [];
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->hardToSoftTable->insertSet($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->hardToSoftTable->insertSet($dataSet);
		}
		
		$this->log->info('Hard to soft inserted = ' . $rdr->count());
	}

	/**
	 *
	 * @return \SoloReplication\Service\DataMapper
	 */
	protected function createGoodsToSoftCategoriesMapper() {
		$context = $this;
		$mapper = new DataMapper();
		$mapper->setMappings([
			'GoodID' => '%d: ArticleId',
			'SoftCategoryID' => '%d: CategoryId' 
		]);
		$mapper->setMapping('IsPrimary', 'IsDefault', function ($field, $row) use($context) {
			return $context->helper()->parseBoolean($field);
		});
		
		return $mapper;
	}

	protected function processGoodsToSoftCategories() {
		$this->log->info('Insert goods to soft categories');
		$mapper = $this->createGoodsToSoftCategoriesMapper();
		$params = [];
		$rdr = new UltimaJsonListReader($this->callWebMethod('GetCategoriesToProducts', $params));
		if (!$rdr->isEmpty()) {
			$this->goodsToSoftCategoriesTable->truncate();
			$this->notify()->fileWarning(false);
		} else {
			$this->removeSwapTable('goods_to_soft_categories');
			$this->notify()->send($this->getNotifyMessage(__CLASS__, __METHOD__, 'GetCategoriesToProducts', $params), 'Пустой ответ по методу репликации');
			$this->notify()->fileWarning(true);
		}
		$dataSet = [];
		foreach ($rdr as $row) {
			$dataSet[] = $mapper->convert($row);
			if (500 == sizeof($dataSet)) {
				$this->goodsToSoftCategoriesTable->insertSet($dataSet);
				$dataSet = [];
			}
		}
		if (0 < sizeof($dataSet)) {
			$this->goodsToSoftCategoriesTable->insertSet($dataSet);
		}
		
		$queryGateway = new QueryGateway();
		if (0 == $rdr->count()) {
			$this->goodsToSoftCategoriesTable->truncate();
			
			$sql = "INSERT INTO #goods_to_soft_categories:passive# (GoodID, SoftCategoryID)
					SELECT ag.GoodID, hts.SoftCategoryID FROM #hard_to_soft:passive# hts INNER JOIN #all_goods:active# ag ON hts.HardCategoryID = ag.CategoryID";
			$queryGateway->query($sql);
		}
		
		// fix empty is primary categories
		$sql = "SELECT
					gtsc.GoodID,
					SUM(gtsc.IsPrimary),
					GROUP_CONCAT(gtsc.SoftCategoryID) AS siteCategories
				FROM #goods_to_soft_categories:passive# gtsc
				GROUP BY gtsc.GoodID
				HAVING (0 = SUM(gtsc.IsPrimary))";
		$rows = $queryGateway->query($sql);
		$this->log->info(sprintf('Found %d goods with empty is primaries', count($rows)));
		foreach ($rows as $row) {
			$siteCategoryIds = explode(',', $row->siteCategories);
			if (0 < count($siteCategoryIds)) {
				$siteCategoryId = reset($siteCategoryIds);
				$sql = "UPDATE #goods_to_soft_categories:passive# SET IsPrimary = 1 WHERE GoodID = ".$row['GoodID'].' AND SoftCategoryID = '.$siteCategoryId;
				$queryGateway->query($sql);
			}
		}
		
		$this->log->info('Goods to soft categories inserted = ' . $rdr->count());
	}

	private function rebuildSearchIndex() {
		$this->log->info('Start rebuild search goods index');
		$output = array();
		$cmd = 'indexer Categories_soft_categories_1 --rotate --config ' . $this->sphinxConfigPath;
		$this->log->info('Command: ' . $cmd);
		exec($cmd, $output);
		if (0 < sizeof($output)) {
			foreach ($output as $outputRow) {
				$this->log->info($outputRow);
			}
		}
		$cmd = 'indexer Categories_soft_categories_2 --rotate --config ' . $this->sphinxConfigPath;
		;
		$this->log->info('Command: ' . $cmd);
		exec($cmd, $output);
		if (0 < sizeof($output)) {
			foreach ($output as $outputRow) {
				$this->log->info($outputRow);
			}
		}
		
		$this->log->info('Stop rebuild search goods index');
	}

}

?>