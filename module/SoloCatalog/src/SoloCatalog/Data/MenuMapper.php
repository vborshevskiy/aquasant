<?php

namespace SoloCatalog\Data;

use Solo\Stdlib\ArrayHelper;
use SoloCache\Service\ProvidesCache;
use Solo\Db\Mapper\AbstractMapper;

class MenuMapper extends AbstractMapper implements MenuMapperInterface {

    use ProvidesCache;

    /**
     * @dependency_table soft_categories:active
     *
     * @return array
     */
    public function enumAllCategories() {
        $cacher = $this->createMethodCacher(__METHOD__, func_get_args());
        if ($cacher->has())
            return $cacher->get();

        $sql = "SELECT
                        sc.SoftCategoryID,
                        sc.CategoryName,
                        sc.CategoryUID,
                        sc.IsUrl,
                        sc.RealSoftCategoryID,
                        sc.cLeft,
                        sc.cRight,
                        sc.cLevel,
                        sc.SortIndex
                    FROM
                        #soft_categories# sc
                    WHERE
                        sc.IsUrl = 0
                        AND sc.cLevel >= 0
                    ORDER BY
                        cLeft ASC";
        $result = $this->query($sql)->toArray();
        $cacher->set($result);
        return $result;
    }

    /**
     * @dependency_table soft_categories:active
     * @dependency_table goods_to_soft_categories:active
     * @dependency_table goods_remains:active
     * @dependency_table all_goods:active
     *
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @return array
     */
    public function enumAllAvailCategoryIds($cityId = null, $isPostDelivery = false) {
    	if (MANAGER_MODE) {
    		$sql = "SELECT DISTINCT
					sc.SoftCategoryID
				FROM
					#soft_categories# sc
				INNER JOIN
					#goods_to_soft_categories# gtsc
					ON sc.SoftCategoryID = gtsc.SoftCategoryID
				INNER JOIN
					#all_goods# ag
					ON ag.GoodID = gtsc.GoodID ";
    		if ($isPostDelivery) {
    			$sql .= "
                                        AND ag.Volume > 0
                                        AND ag.Weight > 0
                    ";
    		}
    		if (!is_null($cityId)) {
    			$sql .= "INNER JOIN
                        #avail_manager_goods:active# avg
                        ON avg.GoodID = ag.GoodID AND avg.CityID = " . $cityId . " ";
    		} else {
    			$sql .= "INNER JOIN
                        #avail_manager_goods:active# avg
                        ON avg.GoodID = ag.GoodID ";
    		}
    		$sql .= "
				WHERE
					sc.IsUrl = 0
					AND sc.cLevel >= 0
					AND (avg.AvailQuantity > 0 OR avg.SuborderQuantity > 0)";
    		$rows = $this->query($sql);
    		$ids = ArrayHelper::enumOneColumn($rows, 'SoftCategoryID');
    		
    		$result = array_unique($ids, SORT_NUMERIC);
    		return $result;
    	}
    	
        $cacher = $this->createMethodCacher(__METHOD__, func_get_args());
        if ($cacher->has())
            return $cacher->get();

        $sql = "SELECT DISTINCT
					sc.SoftCategoryID
				FROM
					#soft_categories# sc
				INNER JOIN
					#goods_to_soft_categories# gtsc
					ON sc.SoftCategoryID = gtsc.SoftCategoryID
				INNER JOIN
					#all_goods# ag
					ON ag.GoodID = gtsc.GoodID ";
        if ($isPostDelivery) {
            $sql .= "
                                        AND ag.Volume > 0
                                        AND ag.Weight > 0
                    ";
        }
        if (!is_null($cityId)) {
            $sql .= "INNER JOIN
                        #avail_goods:active# avg
                        ON avg.GoodID = ag.GoodID AND avg.CityID = " . $cityId . " ";
        } else {
            $sql .= "INNER JOIN
                        #avail_goods:active# avg
                        ON avg.GoodID = ag.GoodID ";
        }
        $sql .= "
				WHERE
					sc.IsUrl = 0
					AND sc.cLevel >= 0
					AND (avg.AvailQuantity > 0 OR avg.SuborderQuantity > 0)";
        $rows = $this->query($sql);
        $ids = ArrayHelper::enumOneColumn($rows, 'SoftCategoryID');

        $result = array_unique($ids, SORT_NUMERIC);
        $cacher->set($result);
        return $result;
    }

    /**
     * @dependency_table soft_categories:active
     * @dependency_table goods_to_soft_categories:active
     * @dependency_table goods_remains:active
     * @dependency_table all_goods:active
     *
     * @param array $categoriesIds
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @return array
     */
    public function enumGoodsCountByCategoryIds(array $categoriesIds, $cityId = null, $isPostDelivery = false) {
    	if (MANAGER_MODE) {
    		$sql = "SELECT
                        sc.SoftCategoryID,
                        COUNT(ag.GoodID) as GoodsCount
                FROM
                        #soft_categories# sc
                INNER JOIN
                        #goods_to_soft_categories# gtsc
                        ON gtsc.SoftCategoryID = sc.SoftCategoryID
                INNER JOIN
                        #all_goods# ag
                        ON ag.GoodID = gtsc.GoodID
    			inner JOIN #goods_prices# gp ON ag.GoodID = gp.GoodID AND gp.ZoneID = 1 AND gp.CategoryID = 1 AND gp.`Value` > 0 ";
    		if ($isPostDelivery) {
    			$sql .= "
                        AND ag.Volume > 0
                        AND ag.Weight > 0
                    ";
    		}
    		if (!is_null($cityId)) {
    			$sql .= "INNER JOIN
                        #avail_manager_goods# avg
                        ON avg.GoodID = ag.GoodID AND avg.CityID = " . $cityId . " ";
    		} else {
    			$sql .= "INNER JOIN
                        #avail_manager_goods# avg
                        ON avg.GoodID = ag.GoodID ";
    		}
    		$sql.= "
                WHERE
                        (avg.AvailQuantity > 0 OR avg.SuborderQuantity > 0)
                        AND sc.SoftCategoryID IN (" . implode(',', $categoriesIds) . ")
                GROUP BY
                        sc.SoftCategoryID";
    		$rows = $this->query($sql);
    		//print_r($rows); exit();
    		$result = [];
    		foreach ($rows as $row) {
    			$result[(int) $row['SoftCategoryID']] = (int) $row['GoodsCount'];
    		}
    		
    		return $result;
    	}
    	
        if (is_null($categoriesIds) || count($categoriesIds) === 0) {
            return [];
        }
        $cacher = $this->createMethodCacher(__METHOD__, func_get_args());
        if ($cacher->has())
            return $cacher->get();

        $sql = "SELECT
                        sc.SoftCategoryID,
                        COUNT(ag.GoodID) as GoodsCount                        
                FROM
                        #soft_categories# sc
                INNER JOIN
                        #goods_to_soft_categories# gtsc
                        ON gtsc.SoftCategoryID = sc.SoftCategoryID
                INNER JOIN
                        #all_goods# ag
                        ON ag.GoodID = gtsc.GoodID
        		inner JOIN #goods_prices# gp ON ag.GoodID = gp.GoodID AND gp.ZoneID = 1 AND gp.CategoryID = 1 AND gp.`Value` > 0 ";
        if ($isPostDelivery) {
            $sql .= "
                        AND ag.Volume > 0
                        AND ag.Weight > 0
                    ";
        }
        if (!is_null($cityId)) {
            $sql .= "INNER JOIN
                        #avail_goods# avg
                        ON avg.GoodID = ag.GoodID AND avg.CityID = " . $cityId . " ";
        } else {
            $sql .= "INNER JOIN
                        #avail_goods# avg
                        ON avg.GoodID = ag.GoodID ";
        }
        $sql.= "
                WHERE
                        (avg.AvailQuantity > 0 OR avg.SuborderQuantity > 0)
                        AND sc.SoftCategoryID IN (" . implode(',', $categoriesIds) . ")
                GROUP BY
                        sc.SoftCategoryID";
        $rows = $this->query($sql);
        $result = [];
        foreach ($rows as $row) {
            $result[(int) $row['SoftCategoryID']] = (int) $row['GoodsCount'];
        }

        $cacher->set($result);
        return $result;
    }

    /**
     * @dependency_table props:active
     * @dependency_table props_to_templates:active
     * @dependency_table templates_to_categories:active
     * @dependency_table goods_to_soft_categories:active
     *
     * @param int $kindId
     * @return array
     */
    public function enumMenuFilters($kindId) {
        $cacher = $this->createMethodCacher(__METHOD__, func_get_args());
        if ($cacher->has())
            return $cacher->get();
        $sql = "SELECT DISTINCT
                        p.PropertyID,
                        p.PropertyName,
                        gtsc.SoftCategoryID
                FROM
                        #props# p
                INNER JOIN
                        #props_to_templates# pt
                        ON pt.PropertyID = p.PropertyID
                INNER JOIN
                        #templates_to_categories# thc
                        ON thc.PropertyTemplateID = pt.PropertyTemplateID
        		INNER JOIN #all_goods# ag
        				ON ag.CategoryID = thc.CategoryID
                INNER JOIN
                        #goods_to_soft_categories# gtsc
                        ON gtsc.GoodID = ag.GoodID
                WHERE
                        p.KindID = " . $kindId . "
                        AND p.IsHidden = 0";
        $rows = $this->query($sql);
        $result = [];
        foreach ($rows as $row) {
            $result[(int) $row['SoftCategoryID']][(int) $row['PropertyID']] = $row['PropertyName'];
        }

        $cacher->set($result);
        return $result;
    }

}

?>