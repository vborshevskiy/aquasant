<?php

namespace SoloCatalog\Data;

interface CompatibleMapperInterface {

    /**
     *
     * @param integer $goodId
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @return mixed
     */
    public function enumCompatibleGoodIds($goodId, $cityId, $isPostDelivery);
    
    /**
     * 
     * @param array $goodIds
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @return mixed
     */
    public function enumCompatibleGoodIdsByGoodIds($goodIds, $cityId, $isPostDelivery);
    
    /**
     * 
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @return mixed
     */
    public function enumCompatibleGoodIdsOnTextPages($cityId, $isPostDelivery);
    
    /**
     * 
     * @param array $goodIds
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @return array
     */
    public function enumCompatibleCategoriesByGoodIds($goodIds, $cityId, $isPostDelivery);
}