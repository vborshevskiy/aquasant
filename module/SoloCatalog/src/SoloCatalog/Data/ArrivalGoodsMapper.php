<?php

namespace SoloCatalog\Data;

use SoloCache\Service\ProvidesCache;
use Solo\Db\Mapper\AbstractMapper;

class ArrivalGoodsMapper extends AbstractMapper implements ArrivalGoodsMapperInterface {

    /**
     * @return array
     */
    public function getSuborderQuantities() {
        $sql = "SELECT
                                arg.GoodID,
                                of.OfficeLocationId,
                                SUM(arg.SuborderQuantity) AS SuborderQuantity,
                                MAX(grad.PickupDate) AS ArrivalDate
                        FROM
                                #arrival_goods:active# arg
                        INNER JOIN
                                #offices:active# of
                                ON of.OfficeID = arg.OfficeID
                                AND of.Hide = 0
                        INNER JOIN
                                #good_reserve_avail_date:active# grad
                                ON grad.GoodID = arg.GoodID
                                AND grad.CityID = of.OfficeLocationId
                        INNER JOIN
                                #all_goods:active# ag
                                ON ag.GoodID = arg.GoodID        				
                        GROUP BY
                                arg.GoodID,
                                of.OfficeLocationId";
        $suborderData = $this->query($sql, true)->toArray();
        $result = [];
        foreach ($suborderData as $suborder) {
            $result[(int) $suborder['OfficeLocationId']][(int) $suborder['GoodID']] = [
                'SuborderQuantity' => (int) $suborder['SuborderQuantity'],
                'ArrivalDate' => $suborder['ArrivalDate'],
            ];
        }
        return $result;
    }

}
