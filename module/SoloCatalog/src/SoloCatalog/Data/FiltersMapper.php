<?php

namespace SoloCatalog\Data;

use Solo\Stdlib\ArrayHelper;
use SoloCache\Service\ProvidesCache;
use Solo\Db\Mapper\AbstractMapper;

class FiltersMapper extends AbstractMapper implements FiltersMapperInterface {
	
	use ProvidesCache;

	/**
	 *
	 * @param integer $categoryId        	
	 * @param integer $cityId        	
	 * @param array $priceRange        	
	 * @return array
	 */
	public function enumWarehousesByCategoryId($categoryId, $cityId, array $priceRange = null) {
		if (GOD_MODE) {
			$hardCategoryIds = $this->enumHardCategoryIdsBySoftCategoryId($categoryId);
			if (0 == sizeof($hardCategoryIds)) {
				$hardCategoryIds = $this->enumChildHardCategoryIdsBySoftCategoryId($categoryId);
			}
			if (0 == sizeof($hardCategoryIds)) {
				return [];
			}
			
			$sql = "SELECT
                                    aw.GoodID,
                                    aw.WarehouseID,
                                    aw.AvailQuantity
                            FROM
                                    #all_goods# ag
                            INNER JOIN
                                    #avail_warehouses# aw
                                    ON
                                        ag.GoodID = aw.GoodID
                                        AND (ag.AvailQuantity > 0)
                            INNER JOIN #offices# of
                                    ON
                                        of.OfficeLocationId = " . $cityId . "
                                        AND of.OfficeID = aw.WarehouseID
                        ";
			if (null !== $priceRange) {
				$sql .= "
                            INNER JOIN
                                    #goods_prices# gp
                                    ON
                                        ag.GoodID = gp.GoodID
                                        AND gp.ZoneID = " . $priceRange['zone'] . "
                                        AND gp.CategoryID = " . $priceRange['category'] . "
                                        AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . "
                    ";
			}
			$sql .= "
                            WHERE
                                    ag.HardCategoryID IN (" . implode(',', $hardCategoryIds) . ")";
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		if (MANAGER_MODE) {
			$hardCategoryIds = $this->enumHardCategoryIdsBySoftCategoryId($categoryId);
			if (0 == sizeof($hardCategoryIds)) {
				$hardCategoryIds = $this->enumChildHardCategoryIdsBySoftCategoryId($categoryId);
			}
			if (0 == sizeof($hardCategoryIds)) {
				return [];
			}
			
			$sql = "SELECT
                                    aw.GoodID,
                                    aw.WarehouseID,
                                    aw.AvailQuantity
                            FROM
                                    #avail_manager_goods# ag
                            INNER JOIN
                                    #avail_warehouses# aw
                                    ON
                                        ag.GoodID = aw.GoodID
                                        AND (ag.AvailQuantity > 0)
                            INNER JOIN #offices# of
                                    ON
                                        of.OfficeLocationId = " . $cityId . "
                                        AND of.OfficeID = aw.WarehouseID
                        ";
			if (null !== $priceRange) {
				$sql .= "
                            INNER JOIN
                                    #goods_prices# gp
                                    ON
                                        ag.GoodID = gp.GoodID
                                        AND gp.ZoneID = " . $priceRange['zone'] . "
                                        AND gp.CategoryID = " . $priceRange['category'] . "
                                        AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . "
                    ";
			}
			$sql .= "
                            WHERE
                                    ag.HardCategoryID IN (" . implode(',', $hardCategoryIds) . ")";
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if ($cacher->has()) {
			return $cacher->get();
		}
		$hardCategoryIds = $this->enumHardCategoryIdsBySoftCategoryId($categoryId);
		if (0 == sizeof($hardCategoryIds)) {
			$hardCategoryIds = $this->enumChildHardCategoryIdsBySoftCategoryId($categoryId);
		}
		if (0 == sizeof($hardCategoryIds)) {
			return [];
		}
		
		$sql = "SELECT
                                    aw.GoodID,
                                    aw.WarehouseID,
                                    aw.AvailQuantity
                            FROM
                                    #avail_goods# ag
                            INNER JOIN
                                    #avail_warehouses# aw
                                    ON
                                        ag.GoodID = aw.GoodID 
                                        AND (ag.AvailQuantity > 0)
                            INNER JOIN #offices# of
                                    ON
                                        of.OfficeLocationId = " . $cityId . "
                                        AND of.OfficeID = aw.WarehouseID
                        ";
		if (null !== $priceRange) {
			$sql .= "
                            INNER JOIN
                                    #goods_prices# gp
                                    ON
                                        ag.GoodID = gp.GoodID 
                                        AND gp.ZoneID = " . $priceRange['zone'] . "
                                        AND gp.CategoryID = " . $priceRange['category'] . "
                                        AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . "
                    ";
		}
		$sql .= "
                            WHERE
                                    ag.HardCategoryID IN (" . implode(',', $hardCategoryIds) . ")
                                    AND ag.CityID = " . $cityId;
		$result = $this->query($sql)->toArray();
		$cacher->set($result);
		return $result;
	}

	/**
	 *
	 * @param integer $categoryId        	
	 * @param integer $cityId        	
	 * @param array $priceRange        	
	 * @return array
	 */
	public function enumGoodsDeliveryDateByCategoryId($categoryId, $cityId, array $priceRange = null) {
		if (GOD_MODE) {
			$hardCategoryIds = $this->enumHardCategoryIdsBySoftCategoryId($categoryId);
			if (0 == sizeof($hardCategoryIds)) {
				$hardCategoryIds = $this->enumChildHardCategoryIdsBySoftCategoryId($categoryId);
			}
			if (0 == sizeof($hardCategoryIds)) {
				return [];
			}
			$sql = "SELECT
                            avg.GoodID,
                            grad.AvailDeliveryDate,
                            grad.SuborderDeliveryDate
                    FROM
                            #all_goods# avg
                    INNER JOIN
                            #good_reserve_avail_date# grad
                            ON
                                avg.GoodID=grad.GoodID
                                AND avg.CityID = grad.CityID
                            ";
			if (null !== $priceRange) {
				$sql .= "INNER JOIN
                            #goods_prices# gp
                            ON
                                avg.GoodID = gp.GoodID
                                AND gp.ZoneID = " . $priceRange['zone'] . "
                                AND gp.CategoryID = " . $priceRange['category'] . "
                                AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
			}
			$sql .= "WHERE
                            avg.CategoryID IN (" . implode(',', $hardCategoryIds) . ")";
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		if (MANAGER_MODE) {
			$hardCategoryIds = $this->enumHardCategoryIdsBySoftCategoryId($categoryId);
			if (0 == sizeof($hardCategoryIds)) {
				$hardCategoryIds = $this->enumChildHardCategoryIdsBySoftCategoryId($categoryId);
			}
			if (0 == sizeof($hardCategoryIds)) {
				return [];
			}
			$sql = "SELECT
                            avg.GoodID,
                            grad.AvailDeliveryDate,
                            grad.SuborderDeliveryDate
                    FROM
                            #avail_manager_goods# avg
                    INNER JOIN
                            #good_reserve_avail_date# grad
                            ON
                                avg.GoodID=grad.GoodID
                                AND avg.CityID = grad.CityID
                            ";
			if (null !== $priceRange) {
				$sql .= "INNER JOIN
                            #goods_prices# gp
                            ON
                                avg.GoodID = gp.GoodID
                                AND gp.ZoneID = " . $priceRange['zone'] . "
                                AND gp.CategoryID = " . $priceRange['category'] . "
                                AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
			}
			$sql .= "WHERE
                            avg.CategoryID IN (" . implode(',', $hardCategoryIds) . ")";
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if ($cacher->has()) return $cacher->get();
		$hardCategoryIds = $this->enumHardCategoryIdsBySoftCategoryId($categoryId);
		if (0 == sizeof($hardCategoryIds)) {
			$hardCategoryIds = $this->enumChildHardCategoryIdsBySoftCategoryId($categoryId);
		}
		if (0 == sizeof($hardCategoryIds)) {
			return [];
		}
		$sql = "SELECT
                            avg.GoodID,
                            grad.AvailDeliveryDate,
                            grad.SuborderDeliveryDate
                    FROM
                            #avail_goods# avg
                    INNER JOIN
                            #good_reserve_avail_date# grad
                            ON
                                avg.GoodID=grad.GoodID
                                AND avg.CityID = grad.CityID
                            ";
		if (null !== $priceRange) {
			$sql .= "INNER JOIN
                            #goods_prices# gp
                            ON
                                avg.GoodID = gp.GoodID
                                AND gp.ZoneID = " . $priceRange['zone'] . "
                                AND gp.CategoryID = " . $priceRange['category'] . "
                                AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
		}
		$sql .= "WHERE
                            avg.HardCategoryID IN (" . implode(',', $hardCategoryIds) . ")
                            AND avg.CityID = " . $cityId;
		$result = $this->query($sql)->toArray();
		$cacher->set($result);
		return $result;
	}

	/**
	 *
	 * @see \SoloCatalog\Data\FiltersMapperInterface::enumGoodsDeliveryDateByGoodIds()
	 */
	public function enumGoodsDeliveryDateByGoodIds(array $goodIds, $cityId, array $priceRange = null) {
		if (GOD_MODE) {
			$sql = "
            SELECT
                        ag.GoodID,
                        grad.AvailDeliveryDate,
                        grad.SuborderDeliveryDate
                FROM
                        #all_goods# ag
                INNER JOIN
                        #good_reserve_avail_date# grad
                        ON ag.GoodID=grad.GoodID ";
			if (null !== $priceRange) {
				$sql .= "
                INNER JOIN
                        #goods_prices# gp
                        ON
                            ag.GoodID = gp.GoodID
                            AND gp.ZoneID = " . $priceRange['zone'] . "
                            AND gp.CategoryID = " . $priceRange['category'] . "
                            AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
			}
			$sql .= "WHERE
                        ag.GoodID IN (" . implode(',', $goodIds) . ")";
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		if (MANAGER_MODE) {
			$sql = "
            SELECT
                        ag.GoodID,
                        grad.AvailDeliveryDate,
                        grad.SuborderDeliveryDate
                FROM
                        #avail_manager_goods# ag
                INNER JOIN
                        #good_reserve_avail_date# grad
                        ON ag.GoodID=grad.GoodID ";
			if (null !== $priceRange) {
				$sql .= "
                INNER JOIN
                        #goods_prices# gp
                        ON
                            ag.GoodID = gp.GoodID
                            AND gp.ZoneID = " . $priceRange['zone'] . "
                            AND gp.CategoryID = " . $priceRange['category'] . "
                            AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
			}
			$sql .= "WHERE
                        ag.GoodID IN (" . implode(',', $goodIds) . ")";
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if ($cacher->has()) return $cacher->get();
		$sql = "
            SELECT
                        ag.GoodID,
                        grad.AvailDeliveryDate,
                        grad.SuborderDeliveryDate
                FROM
                        #avail_goods# ag
                INNER JOIN
                        #good_reserve_avail_date# grad
                        ON ag.GoodID=grad.GoodID ";
		if (null !== $priceRange) {
			$sql .= "
                INNER JOIN
                        #goods_prices# gp
                        ON
                            ag.GoodID = gp.GoodID
                            AND gp.ZoneID = " . $priceRange['zone'] . "
                            AND gp.CategoryID = " . $priceRange['category'] . "
                            AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
		}
		$sql .= "WHERE
                        ag.GoodID IN (" . implode(',', $goodIds) . ")
                        AND ag.CityID = " . $cityId;
		$result = $this->query($sql)->toArray();
		$cacher->set($result);
		return $result;
	}

	/**
	 *
	 * @param integer $categoryId        	
	 * @param integer $cityId        	
	 * @param boolean $isPostDelivery        	
	 * @param array $priceRange        	
	 * @return array
	 */
	public function enumBrandsByCategoryId($categoryId, $cityId, $isPostDelivery = false, array $priceRange = null, $zoneId = null, $priceCategoryId = null, array $setIds = []) {
		if (GOD_MODE) {
			$hardCategoryIds = $this->enumHardCategoryIdsBySoftCategoryId($categoryId);
			if (0 == sizeof($hardCategoryIds)) {
				$hardCategoryIds = $this->enumChildHardCategoryIdsBySoftCategoryId($categoryId);
			}
			if (0 == sizeof($hardCategoryIds)) {
				return [];
			}
			
			$sql = "
            SELECT
                        avg.GoodID,
                        avg.BrandID
                    FROM
                        #all_goods# avg
        			INNER JOIN #goods_to_soft_categories# gtsc
        				ON avg.GoodID = gtsc.GoodID
        				AND gtsc.SoftCategoryID = " . $categoryId . " ";
			if (null !== $priceRange) {
				$sql .= "
                    INNER JOIN
                        #goods_prices# gp
                        ON
                            avg.GoodID = gp.GoodID
                            AND gp.ZoneID = " . $priceRange['zone'] . "
                            AND gp.CategoryID = " . $priceRange['category'] . "
                            AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
			} elseif ($zoneId && $priceCategoryId) {
				$sql .= "
                    INNER JOIN
                        #goods_prices# gp
                        ON
                            avg.GoodID = gp.GoodID
                            AND gp.ZoneID = " . $zoneId . "
                            AND gp.CategoryID = " . $priceCategoryId;
			}
			if ($isPostDelivery) {
				$sql .= "
                    INNER JOIN
                        #all_goods# ag
                        ON
                            ag.GoodID = avg.GoodID
                            AND ag.Volume > 0
                            AND ag.Weight > 0
                    ";
			}
			if (0 < count($setIds)) {
				$sql .= " INNER JOIN #sets_to_goods# stg
        				ON avg.GoodID = stg.GoodID
        				AND stg.SetID IN (" . implode(',', $setIds) . ")";
			}
			$sql .= "
                    WHERE
                        avg.CategoryID IN (" . implode(',', $hardCategoryIds) . ")";
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		if (MANAGER_MODE) {
			$hardCategoryIds = $this->enumHardCategoryIdsBySoftCategoryId($categoryId);
			if (0 == sizeof($hardCategoryIds)) {
				$hardCategoryIds = $this->enumChildHardCategoryIdsBySoftCategoryId($categoryId);
			}
			if (0 == sizeof($hardCategoryIds)) {
				return [];
			}
			
			$sql = "
            SELECT
                        avg.GoodID,					
                        avg.BrandID					
                    FROM
                        #avail_manager_goods# avg 
        			INNER JOIN #goods_to_soft_categories# gtsc
        				ON avg.GoodID = gtsc.GoodID
        				AND gtsc.SoftCategoryID = " . $categoryId . " ";
			if (null !== $priceRange) {
				$sql .= "
                    INNER JOIN
                        #goods_prices# gp
                        ON
                            avg.GoodID = gp.GoodID 
                            AND gp.ZoneID = " . $priceRange['zone'] . "
                            AND gp.CategoryID = " . $priceRange['category'] . "						
                            AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
			} elseif ($zoneId && $priceCategoryId) {
				$sql .= "
                    INNER JOIN
                        #goods_prices# gp
                        ON
                            avg.GoodID = gp.GoodID
                            AND gp.ZoneID = " . $zoneId . "
                            AND gp.CategoryID = " . $priceCategoryId;
			}
			if ($isPostDelivery) {
				$sql .= "
                    INNER JOIN
                        #all_goods# ag
                        ON
                            ag.GoodID = avg.GoodID
                            AND ag.Volume > 0
                            AND ag.Weight > 0
                    ";
			}
			if (0 < count($setIds)) {
				$sql .= " INNER JOIN #sets_to_goods# stg
        				ON avg.GoodID = stg.GoodID
        				AND stg.SetID IN (" . implode(',', $setIds) . ")";
			}
			$sql .= "
                    WHERE
                        avg.HardCategoryID IN (" . implode(',', $hardCategoryIds) . ")
                        AND avg.CityID = " . $cityId;
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if (false && $cacher->has()) return $cacher->get();
		$hardCategoryIds = $this->enumHardCategoryIdsBySoftCategoryId($categoryId);
		if (0 == sizeof($hardCategoryIds)) {
			$hardCategoryIds = $this->enumChildHardCategoryIdsBySoftCategoryId($categoryId);
		}
		if (0 == sizeof($hardCategoryIds)) {
			return [];
		}
		
		$sql = "
            SELECT
                        avg.GoodID,					
                        avg.BrandID					
                    FROM
                        #avail_goods# avg 
        			INNER JOIN #goods_to_soft_categories# gtsc
        				ON avg.GoodID = gtsc.GoodID
        				AND gtsc.SoftCategoryID = " . $categoryId . " ";
		if (null !== $priceRange) {
			$sql .= "
                    INNER JOIN
                        #goods_prices# gp
                        ON
                            avg.GoodID = gp.GoodID 
                            AND gp.ZoneID = " . $priceRange['zone'] . "
                            AND gp.CategoryID = " . $priceRange['category'] . "						
                            AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
		} elseif ($zoneId && $priceCategoryId) {
			$sql .= "
                    INNER JOIN
                        #goods_prices# gp
                        ON
                            avg.GoodID = gp.GoodID
                            AND gp.ZoneID = " . $zoneId . "
                            AND gp.CategoryID = " . $priceCategoryId;
		}
		if ($isPostDelivery) {
			$sql .= "
                    INNER JOIN
                        #all_goods# ag
                        ON
                            ag.GoodID = avg.GoodID
                            AND ag.Volume > 0
                            AND ag.Weight > 0
                    ";
		}
		if (0 < count($setIds)) {
			$sql .= " INNER JOIN #sets_to_goods# stg
        				ON avg.GoodID = stg.GoodID
        				AND stg.SetID IN (" . implode(',', $setIds) . ")";
		}
		$sql .= "
                    WHERE
                        avg.HardCategoryID IN (" . implode(',', $hardCategoryIds) . ")
                        AND avg.CityID = " . $cityId;
		$result = $this->query($sql)->toArray();
		$cacher->set($result);
		return $result;
	}

	/**
	 *
	 * @param array $goodIds        	
	 * @param integer $cityId        	
	 * @param array $priceRange        	
	 * @return array
	 */
	public function enumWarehousesByGoodIds(array $goodIds, $cityId, array $priceRange = null) {
		if (GOD_MODE) {
			$sql = "
            SELECT
                        aw.GoodID,
                        aw.WarehouseID,
                        aw.AvailQuantity
                FROM
                        #all_goods# ag
                INNER JOIN
                        #avail_warehouses# aw
                        ON
                            ag.GoodID = aw.GoodID
                            AND (aw.AvailQuantity > 0) ";
			if (null !== $priceRange) {
				$sql .= "
                INNER JOIN
                        #goods_prices# gp
                        ON
                            ag.GoodID = gp.GoodID
                            AND gp.ZoneID = " . $priceRange['zone'] . "
                            AND gp.CategoryID = " . $priceRange['category'] . "
                            AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
			}
			$sql .= "WHERE
                        ag.GoodID IN (" . implode(',', $goodIds) . ")";
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		if (MANAGER_MODE) {
			$sql = "
            SELECT
                        aw.GoodID,
                        aw.WarehouseID,
                        aw.AvailQuantity
                FROM
                        #avail_manager_goods# ag
                INNER JOIN
                        #avail_warehouses# aw
                        ON
                            ag.GoodID = aw.GoodID
                            AND (aw.AvailQuantity > 0) ";
			if (null !== $priceRange) {
				$sql .= "
                INNER JOIN
                        #goods_prices# gp
                        ON
                            ag.GoodID = gp.GoodID
                            AND gp.ZoneID = " . $priceRange['zone'] . "
                            AND gp.CategoryID = " . $priceRange['category'] . "
                            AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
			}
			$sql .= "WHERE
                        ag.GoodID IN (" . implode(',', $goodIds) . ")
                        AND ag.CityID = " . $cityId;
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if ($cacher->has()) return $cacher->get();
		$sql = "
            SELECT
                        aw.GoodID,
                        aw.WarehouseID,
                        aw.AvailQuantity
                FROM
                        #avail_goods# ag
                INNER JOIN
                        #avail_warehouses# aw
                        ON  
                            ag.GoodID = aw.GoodID 
                            AND (aw.AvailQuantity > 0) ";
		if (null !== $priceRange) {
			$sql .= "
                INNER JOIN
                        #goods_prices# gp
                        ON
                            ag.GoodID = gp.GoodID 
                            AND gp.ZoneID = " . $priceRange['zone'] . "
                            AND gp.CategoryID = " . $priceRange['category'] . "
                            AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
		}
		$sql .= "WHERE
                        ag.GoodID IN (" . implode(',', $goodIds) . ")
                        AND ag.CityID = " . $cityId;
		$result = $this->query($sql)->toArray();
		$cacher->set($result);
		return $result;
	}

	/**
	 *
	 * @param array $goodIds        	
	 * @param integer $cityId        	
	 * @param boolean $isPostDelivery        	
	 * @param array $priceRange        	
	 * @return array
	 */
	public function enumBrandsByGoodIds(array $goodIds, $cityId, $isPostDelivery = false, array $priceRange = null) {
		if (GOD_MODE) {
			$sql = "SELECT
                            avg.GoodID,
                            avg.BrandID
                    FROM
                            #all_goods# avg ";
			if (null !== $priceRange) {
				$sql .= "
                    INNER JOIN
                            #goods_prices# gp
                                    ON
                                        avg.GoodID = gp.GoodID
                                        AND gp.ZoneID = " . $priceRange['zone'] . "
                                        AND gp.CategoryID = " . $priceRange['category'] . "
                                        AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
			}
			if ($isPostDelivery) {
				$sql .= "
                    INNER JOIN
                            #all_goods# ag
                            ON
                                ag.GoodID = avg.GoodID
                                AND ag.Volume > 0
                                AND ag.Weight > 0
                    ";
			}
			$sql .= "
                    WHERE
                            avg.GoodID IN (" . implode(',', $goodIds) . ")";
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		if (MANAGER_MODE) {
			$sql = "SELECT
                            avg.GoodID,
                            avg.BrandID
                    FROM
                            #avail_manager_goods# avg ";
			if (null !== $priceRange) {
				$sql .= "
                    INNER JOIN
                            #goods_prices# gp
                                    ON
                                        avg.GoodID = gp.GoodID
                                        AND gp.ZoneID = " . $priceRange['zone'] . "
                                        AND gp.CategoryID = " . $priceRange['category'] . "
                                        AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
			}
			if ($isPostDelivery) {
				$sql .= "
                    INNER JOIN
                            #all_goods# ag
                            ON
                                ag.GoodID = avg.GoodID
                                AND ag.Volume > 0
                                AND ag.Weight > 0
                    ";
			}
			$sql .= "
                    WHERE
                            avg.GoodID IN (" . implode(',', $goodIds) . ")
                            AND avg.CityID = " . $cityId;
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if ($cacher->has()) return $cacher->get();
		
		$sql = "SELECT
                            avg.GoodID,					
                            avg.BrandID
                    FROM
                            #avail_goods# avg ";
		if (null !== $priceRange) {
			$sql .= "
                    INNER JOIN
                            #goods_prices# gp
                                    ON
                                        avg.GoodID = gp.GoodID 
                                        AND gp.ZoneID = " . $priceRange['zone'] . "
                                        AND gp.CategoryID = " . $priceRange['category'] . "
                                        AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
		}
		if ($isPostDelivery) {
			$sql .= "
                    INNER JOIN
                            #all_goods# ag
                            ON
                                ag.GoodID = avg.GoodID
                                AND ag.Volume > 0
                                AND ag.Weight > 0
                    ";
		}
		$sql .= "
                    WHERE
                            avg.GoodID IN (" . implode(',', $goodIds) . ")
                            AND avg.CityID = " . $cityId;
		$result = $this->query($sql)->toArray();
		$cacher->set($result);
		return $result;
	}

	/**
	 *
	 * @param integer $softCategoryId        	
	 * @param integer $cityId        	
	 * @param boolean $isPostDelivery        	
	 * @param array $priceRange        	
	 * @return array
	 */
	public function enumFeaturesByCategoryId($softCategoryId, $cityId, $isPostDelivery = false, array $priceRange = null, $zoneId = null, $priceCategoryId = null, array $setIds = []) {
		if (GOD_MODE) {
			$hardCategoryIds = $this->enumHardCategoryIdsBySoftCategoryId($softCategoryId);
			
			if (0 == sizeof($hardCategoryIds)) {
				$hardCategoryIds = $this->enumChildHardCategoryIdsBySoftCategoryId($softCategoryId);
			}
			if (0 == sizeof($hardCategoryIds)) {
				return [];
			}
			$sql = "SELECT DISTINCT
                            gfa.GoodID,
                            gfa.AspectKey,
                            gfa.AspectValue,
                            gfa.AspectBool,
                            gfa.AspectType,
                            gfa.AspectNum,
                            gfa.AspectKind,
                            ptt.PropertyGroupID,
							pg.SortIndex AS GroupSortIndex
                    FROM
                            #goods_filters_aspects# gfa
                    INNER JOIN #templates_to_site_categories# ttsc
                           ON gfa.CategoryID = ttsc.SiteCategoryID
                    INNER JOIN #props_to_templates# ptt
                           ON ptt.PropertyTemplateID = ttsc.PropertyTemplateID AND gfa.AspectKey = ptt.PropertyID
                    INNER JOIN
                            #all_goods# avg
                            ON (avg.GoodID = gfa.GoodID)
                    INNER JOIN #goods_to_soft_categories# gtsc
                            ON avg.GoodID = gtsc.GoodID
                            AND gtsc.SoftCategoryID = " . $softCategoryId . "
                    INNER JOIN #prop_groups# pg
						ON ptt.PropertyGroupID = pg.PropertyGroupID ";
			if (null !== $priceRange) {
				$sql .= "
                    INNER JOIN
                            #goods_prices# gp
                            ON
                                avg.GoodID = gp.GoodID
                                AND gp.ZoneID = " . $priceRange['zone'] . "
                                AND gp.CategoryID = " . $priceRange['category'] . "
                                AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
			} elseif ($zoneId && $priceCategoryId) {
				$sql .= "
                    INNER JOIN
                            #goods_prices# gp
                            ON
                                avg.GoodID = gp.GoodID
                                AND gp.ZoneID = " . $zoneId . "
                                AND gp.CategoryID = " . $priceCategoryId;
			}
			if ($isPostDelivery) {
				$sql .= "
                    INNER JOIN
                            #all_goods# ag
                            ON
                                ag.GoodID = avg.GoodID
                                AND ag.Volume > 0
                                AND ag.Weight > 0
                    ";
			}
			if (0 < count($setIds)) {
				$sql .= " INNER JOIN #sets_to_goods# stg
        				ON avg.GoodID = stg.GoodID
        				AND stg.SetID IN (" . implode(',', $setIds) . ")";
			}
			$sql .= "
                    WHERE
                            gfa.CategoryID = " . $softCategoryId . "
                            AND (gfa.AspectValue <> 0 OR gfa.AspectType = " . PropertiesTable::NUMERIC_PROPERTY_TYPE . " OR gfa.AspectType = " . PropertiesTable::BOOLEAN_PEROPERTY_TYPE . ")";
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		if (MANAGER_MODE) {
			$hardCategoryIds = $this->enumHardCategoryIdsBySoftCategoryId($softCategoryId);
			
			if (0 == sizeof($hardCategoryIds)) {
				$hardCategoryIds = $this->enumChildHardCategoryIdsBySoftCategoryId($softCategoryId);
			}
			if (0 == sizeof($hardCategoryIds)) {
				return [];
			}
			$sql = "SELECT DISTINCT
                            gfa.GoodID,
                            gfa.AspectKey,
                            gfa.AspectValue,
                            gfa.AspectBool,
                            gfa.AspectType,
                            gfa.AspectNum,
                            gfa.AspectKind,
                            ptt.PropertyGroupID,
							pg.SortIndex AS GroupSortIndex
                    FROM
                            #goods_filters_aspects# gfa
                    INNER JOIN #templates_to_site_categories# ttsc
                           ON gfa.CategoryID = ttsc.SiteCategoryID
                    INNER JOIN #props_to_templates# ptt
                           ON ptt.PropertyTemplateID = ttsc.PropertyTemplateID AND gfa.AspectKey = ptt.PropertyID
                    INNER JOIN
                            #avail_manager_goods# avg
                            ON (avg.GoodID = gfa.GoodID AND avg.CityID = " . $cityId . ")
                    INNER JOIN #goods_to_soft_categories# gtsc
                            ON avg.GoodID = gtsc.GoodID
                            AND gtsc.SoftCategoryID = " . $softCategoryId . "
                    INNER JOIN #prop_groups# pg
						ON ptt.PropertyGroupID = pg.PropertyGroupID ";
			if (null !== $priceRange) {
				$sql .= "
                    INNER JOIN
                            #goods_prices# gp
                            ON
                                avg.GoodID = gp.GoodID
                                AND gp.ZoneID = " . $priceRange['zone'] . "
                                AND gp.CategoryID = " . $priceRange['category'] . "
                                AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
			} elseif ($zoneId && $priceCategoryId) {
				$sql .= "
                    INNER JOIN
                            #goods_prices# gp
                            ON
                                avg.GoodID = gp.GoodID
                                AND gp.ZoneID = " . $zoneId . "
                                AND gp.CategoryID = " . $priceCategoryId;
			}
			if ($isPostDelivery) {
				$sql .= "
                    INNER JOIN
                            #all_goods# ag
                            ON
                                ag.GoodID = avg.GoodID
                                AND ag.Volume > 0
                                AND ag.Weight > 0
                    ";
			}
			if (0 < count($setIds)) {
				$sql .= " INNER JOIN #sets_to_goods# stg
        				ON avg.GoodID = stg.GoodID
        				AND stg.SetID IN (" . implode(',', $setIds) . ")";
			}
			$sql .= "
                    WHERE
                            gfa.CategoryID = " . $softCategoryId . "
                            AND (gfa.AspectValue <> 0 OR gfa.AspectType = " . PropertiesTable::NUMERIC_PROPERTY_TYPE . " OR gfa.AspectType = " . PropertiesTable::BOOLEAN_PEROPERTY_TYPE . ")";
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if (false && $cacher->has()) return $cacher->get();
		
		$hardCategoryIds = $this->enumHardCategoryIdsBySoftCategoryId($softCategoryId);
		
		if (0 == sizeof($hardCategoryIds)) {
			$hardCategoryIds = $this->enumChildHardCategoryIdsBySoftCategoryId($softCategoryId);
		}
		if (0 == sizeof($hardCategoryIds)) {
			return [];
		}
		$sql = "SELECT DISTINCT 
                            gfa.GoodID,
                            gfa.AspectKey,
                            gfa.AspectValue,
                            gfa.AspectBool,
                            gfa.AspectType,
                            gfa.AspectNum,
                            gfa.AspectKind,
                            ptt.PropertyGroupID,
							pg.SortIndex AS GroupSortIndex
                    FROM
                            #goods_filters_aspects# gfa
                    INNER JOIN #templates_to_site_categories# ttsc
                           ON gfa.CategoryID = ttsc.SiteCategoryID
                    INNER JOIN #props_to_templates# ptt
                           ON ptt.PropertyTemplateID = ttsc.PropertyTemplateID AND gfa.AspectKey = ptt.PropertyID
                    INNER JOIN
                            #avail_goods# avg
                            ON (avg.GoodID = gfa.GoodID AND avg.CityID = " . $cityId . ")
                    INNER JOIN #goods_to_soft_categories# gtsc
                            ON avg.GoodID = gtsc.GoodID
                            AND gtsc.SoftCategoryID = " . $softCategoryId . "
                    INNER JOIN #prop_groups# pg
						ON ptt.PropertyGroupID = pg.PropertyGroupID ";
		if (null !== $priceRange) {
			$sql .= "
                    INNER JOIN
                            #goods_prices# gp
                            ON
                                avg.GoodID = gp.GoodID
                                AND gp.ZoneID = " . $priceRange['zone'] . "
                                AND gp.CategoryID = " . $priceRange['category'] . "
                                AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
		} elseif ($zoneId && $priceCategoryId) {
			$sql .= "
                    INNER JOIN
                            #goods_prices# gp
                            ON
                                avg.GoodID = gp.GoodID
                                AND gp.ZoneID = " . $zoneId . "
                                AND gp.CategoryID = " . $priceCategoryId;
		}
		if ($isPostDelivery) {
			$sql .= "
                    INNER JOIN
                            #all_goods# ag
                            ON
                                ag.GoodID = avg.GoodID
                                AND ag.Volume > 0
                                AND ag.Weight > 0
                    ";
		}
		if (0 < count($setIds)) {
			$sql .= " INNER JOIN #sets_to_goods# stg
        				ON avg.GoodID = stg.GoodID
        				AND stg.SetID IN (" . implode(',', $setIds) . ")";
		}
		$sql .= "
                    WHERE
                            gfa.CategoryID = " . $softCategoryId . "
                            AND (gfa.AspectValue <> 0 OR gfa.AspectType = " . PropertiesTable::NUMERIC_PROPERTY_TYPE . " OR gfa.AspectType = " . PropertiesTable::BOOLEAN_PEROPERTY_TYPE . ")";
		// print_r($result = $this->query($sql)->getDataSource()->getResource()->queryString); die;
		
		if (isset($_GET['debug_sql'])) {
			print_r($this->query($sql)); exit();
		}
		$result = $this->query($sql)->toArray();
		$cacher->set($result);
		return $result;
	}

	/**
	 *
	 * @param array $goodIds        	
	 * @param integer $cityId        	
	 * @param array $priceRange        	
	 * @return array
	 */
	public function enumOrderGoodsByGoodIds(array $goodIds, $cityId, array $priceRange = null) {
		if (GOD_MODE) {
			$sql = "
            SELECT
                        ag.GoodID,
                        ag.ArrivalDate
                FROM
                        #all_goods# ag ";
			if (null !== $priceRange) {
				$sql .= "
                INNER JOIN
                        #goods_prices# gp
                        ON
                            ag.GoodID = gp.GoodID
                            AND gp.ZoneID = " . $priceRange['zone'] . "
                            AND gp.CategoryID = " . $priceRange['category'] . "
                            AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
			}
			$sql .= "
                WHERE
                            ag.GoodID IN (" . implode(',', $goodIds) . ")
                            AND ag.AvailQuantity=0";
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		if (MANAGER_MODE) {
			$sql = "
            SELECT
                        ag.GoodID,
                        ag.ArrivalDate
                FROM
                        #avail_manager_goods# ag ";
			if (null !== $priceRange) {
				$sql .= "
                INNER JOIN
                        #goods_prices# gp
                        ON
                            ag.GoodID = gp.GoodID
                            AND gp.ZoneID = " . $priceRange['zone'] . "
                            AND gp.CategoryID = " . $priceRange['category'] . "
                            AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
			}
			$sql .= "
                WHERE
                            ag.GoodID IN (" . implode(',', $goodIds) . ")
                            AND ag.AvailQuantity=0 AND ag.CityID = " . $cityId;
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if ($cacher->has()) return $cacher->get();
		$sql = "
            SELECT
                        ag.GoodID,
                        ag.ArrivalDate
                FROM
                        #avail_goods# ag ";
		if (null !== $priceRange) {
			$sql .= "
                INNER JOIN
                        #goods_prices# gp
                        ON
                            ag.GoodID = gp.GoodID
                            AND gp.ZoneID = " . $priceRange['zone'] . "
                            AND gp.CategoryID = " . $priceRange['category'] . "
                            AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
		}
		$sql .= "
                WHERE
                            ag.GoodID IN (" . implode(',', $goodIds) . ")
                            AND ag.AvailQuantity=0 AND ag.CityID = " . $cityId;
		$result = $this->query($sql)->toArray();
		$cacher->set($result);
		return $result;
	}

	/**
	 *
	 * @param integer $softCategoryId        	
	 * @param integer $cityId        	
	 * @param array $priceRange        	
	 * @return array
	 */
	public function enumOrderGoodsByCategoryId($softCategoryId, $cityId, array $priceRange = null) {
		if (GOD_MODE) {
			$hardCategoryIds = $this->enumHardCategoryIdsBySoftCategoryId($softCategoryId);
			if (0 == sizeof($hardCategoryIds)) {
				$hardCategoryIds = $this->enumChildHardCategoryIdsBySoftCategoryId($softCategoryId);
			}
			if (0 == sizeof($hardCategoryIds)) {
				return [];
			}
			$sql = "SELECT
                            avg.GoodID,
                            ag.ArrivalDate
                    FROM
                            #all_goods# avg
							LEFT OUTER JOIN #avail_goods# ag
								ON avg.GoodID = ag.GoodID
                    ";
			if (null !== $priceRange) {
				$sql .= "
                    INNER JOIN
                            #goods_prices# gp
                            ON
                                avg.GoodID = gp.GoodID
                                AND gp.ZoneID = " . $priceRange['zone'] . "
                                AND gp.CategoryID = " . $priceRange['category'] . "
                                AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
			}
			$sql .= "
                    WHERE
                            avg.CategoryID IN (" . implode(',', $hardCategoryIds) . ")";
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		if (MANAGER_MODE) {
			$hardCategoryIds = $this->enumHardCategoryIdsBySoftCategoryId($softCategoryId);
			if (0 == sizeof($hardCategoryIds)) {
				$hardCategoryIds = $this->enumChildHardCategoryIdsBySoftCategoryId($softCategoryId);
			}
			if (0 == sizeof($hardCategoryIds)) {
				return [];
			}
			$sql = "SELECT
                            avg.GoodID,
                            avg.ArrivalDate
                    FROM
                            #avail_manager_goods# avg
                    ";
			if (null !== $priceRange) {
				$sql .= "
                    INNER JOIN
                            #goods_prices# gp
                            ON
                                avg.GoodID = gp.GoodID
                                AND gp.ZoneID = " . $priceRange['zone'] . "
                                AND gp.CategoryID = " . $priceRange['category'] . "
                                AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
			}
			$sql .= "
                    WHERE
                            avg.ArrivalDate IS NOT NULL
                            AND avg.HardCategoryID IN (" . implode(',', $hardCategoryIds) . ")
                            AND avg.AvailQuantity=0 AND avg.CityID = " . $cityId;
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if ($cacher->has()) return $cacher->get();
		
		$hardCategoryIds = $this->enumHardCategoryIdsBySoftCategoryId($softCategoryId);
		if (0 == sizeof($hardCategoryIds)) {
			$hardCategoryIds = $this->enumChildHardCategoryIdsBySoftCategoryId($softCategoryId);
		}
		if (0 == sizeof($hardCategoryIds)) {
			return [];
		}
		$sql = "SELECT
                            avg.GoodID,
                            avg.ArrivalDate
                    FROM
                            #avail_goods# avg
                    ";
		if (null !== $priceRange) {
			$sql .= "
                    INNER JOIN
                            #goods_prices# gp
                            ON
                                avg.GoodID = gp.GoodID                                         
                                AND gp.ZoneID = " . $priceRange['zone'] . "
                                AND gp.CategoryID = " . $priceRange['category'] . "
                                AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
		}
		$sql .= "
                    WHERE
                            avg.ArrivalDate IS NOT NULL
                            AND avg.HardCategoryID IN (" . implode(',', $hardCategoryIds) . ")
                            AND avg.AvailQuantity=0 AND avg.CityID = " . $cityId;
		$result = $this->query($sql)->toArray();
		$cacher->set($result);
		return $result;
	}

	/**
	 *
	 * @param array $goodIds        	
	 * @param integer $cityId        	
	 * @param boolean $isPostDelivery        	
	 * @param array $priceRange        	
	 * @return array
	 */
	public function enumFeaturesByGoodIds(array $goodIds, $cityId, $isPostDelivery = false, array $priceRange = null) {
		if (GOD_MODE) {
			$sql = "SELECT
                            gfa.GoodID,
                            gfa.AspectKey,
                            gfa.AspectValue,
                            gfa.AspectType
                    FROM
                            #goods_filters_aspects# gfa
                    INNER JOIN
                            #all_goods# avg
                            ON
                                avg.GoodID = gfa.GoodID";
			if (null !== $priceRange) {
				$sql .= "
                    INNER JOIN
                            #goods_prices# gp
					ON
                                            avg.GoodID = gp.GoodID
                                            AND gp.ZoneID = " . $priceRange['zone'] . "
                                            AND gp.CategoryID = " . $priceRange['category'] . "
                                            AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
			}
			if ($isPostDelivery) {
				$sql .= "
                    INNER JOIN
                            #all_goods# ag
                            ON
                                ag.GoodID = avg.GoodID
                                AND ag.Volume > 0
                                AND ag.Weight > 0
                    ";
			}
			$sql .= "
                    WHERE
                            gfa.GoodID IN (" . implode(',', $goodIds) . ")" . "
                            AND (gfa.AspectValue <> 0 OR gfa.AspectType = " . PropertiesTable::NUMERIC_PROPERTY_TYPE . " OR gfa.AspectType = " . PropertiesTable::BOOLEAN_PEROPERTY_TYPE . ")";
			
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		if (MANAGER_MODE) {
			$sql = "SELECT
                            gfa.GoodID,
                            gfa.AspectKey,
                            gfa.AspectValue,
                            gfa.AspectType
                    FROM
                            #goods_filters_aspects# gfa
                    INNER JOIN
                            #avail_manager_goods# avg
                            ON
                                avg.GoodID = gfa.GoodID
                                AND avg.CityID = " . $cityId . " ";
			if (null !== $priceRange) {
				$sql .= "
                    INNER JOIN
                            #goods_prices# gp
					ON
                                            avg.GoodID = gp.GoodID
                                            AND gp.ZoneID = " . $priceRange['zone'] . "
                                            AND gp.CategoryID = " . $priceRange['category'] . "
                                            AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
			}
			if ($isPostDelivery) {
				$sql .= "
                    INNER JOIN
                            #all_goods# ag
                            ON
                                ag.GoodID = avg.GoodID
                                AND ag.Volume > 0
                                AND ag.Weight > 0
                    ";
			}
			$sql .= "
                    WHERE
                            gfa.GoodID IN (" . implode(',', $goodIds) . ")" . "
                            AND (gfa.AspectValue <> 0 OR gfa.AspectType = " . PropertiesTable::NUMERIC_PROPERTY_TYPE . " OR gfa.AspectType = " . PropertiesTable::BOOLEAN_PEROPERTY_TYPE . ")";
			
			$result = $this->query($sql)->toArray();
			return $result;
		}
		
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if ($cacher->has()) return $cacher->get();
		
		$sql = "SELECT
                            gfa.GoodID,
                            gfa.AspectKey,
                            gfa.AspectValue,
                            gfa.AspectType
                    FROM
                            #goods_filters_aspects# gfa
                    INNER JOIN
                            #avail_goods# avg
                            ON
                                avg.GoodID = gfa.GoodID
                                AND avg.CityID = " . $cityId . " ";
		if (null !== $priceRange) {
			$sql .= "
                    INNER JOIN
                            #goods_prices# gp
					ON
                                            avg.GoodID = gp.GoodID                                         
                                            AND gp.ZoneID = " . $priceRange['zone'] . "
                                            AND gp.CategoryID = " . $priceRange['category'] . "
                                            AND gp.Value BETWEEN " . $priceRange['from'] . " AND " . $priceRange['to'] . " ";
		}
		if ($isPostDelivery) {
			$sql .= "
                    INNER JOIN
                            #all_goods# ag
                            ON
                                ag.GoodID = avg.GoodID
                                AND ag.Volume > 0
                                AND ag.Weight > 0
                    ";
		}
		$sql .= "
                    WHERE
                            gfa.GoodID IN (" . implode(',', $goodIds) . ")" . "
                            AND (gfa.AspectValue <> 0 OR gfa.AspectType = " . PropertiesTable::NUMERIC_PROPERTY_TYPE . " OR gfa.AspectType = " . PropertiesTable::BOOLEAN_PEROPERTY_TYPE . ")";
		
		$result = $this->query($sql)->toArray();
		$cacher->set($result);
		return $result;
	}

	/**
	 * @dependency_table props:active
	 * @dependency_table prop_values:active
	 * @dependency_table props_to_templates:active
	 * @dependency_table prop_units:active
	 *
	 * @param array $propIds        	
	 * @param array $valueIds        	
	 * @return array
	 */
	public function enumFeatureSettings(array $propIds, array $valueIds, $categoryId = 0) {
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		//if ($cacher->has()) return $cacher->get();
		
		// get template id
		$templateId = 0;
		if ($categoryId && !$templateId) {
			$sql = "SELECT TemplateID FROM #soft_categories# WHERE SoftCategoryID = {$categoryId} LIMIT 1";
			$rows = $this->query($sql);
			if (0 < $rows->count()) {
				$row = $rows->current();
				$templateId = intval($row['TemplateID']);
			}
		}
		if ($categoryId && !$templateId) {
			$sql = "SELECT DISTINCT
						ttc.PropertyTemplateID
					FROM #goods_to_soft_categories# gtsc
						INNER JOIN #avail_goods# ag
							ON gtsc.GoodID = ag.GoodID
						INNER JOIN #templates_to_categories# ttc ON ag.HardCategoryID = ttc.CategoryID
					WHERE gtsc.SoftCategoryID = {$categoryId}";
			$rows = $this->query($sql);
			if (0 < $rows->count()) {
				$row = $rows->current();
				$templateId = intval($row['PropertyTemplateID']);
			}
		}
		
		$sql = "
            SELECT DISTINCT 
                        p.PropertyID AS pid,
                        pv.PropertyValueID pvid,
                        p.PropertyName AS name,
                        pv.PropertyValue AS value,
                        p.PropertyTypeID AS propType,
                        pv.Image as image,
						pg.SortIndex AS pgSortIndex,
						ptt.SortIndex AS pttSortIndex
                FROM
                        #props# p
                LEFT JOIN
                        #prop_values# pv
                        ON
                            pv.PropertyID = p.PropertyID
                            AND pv.PropertyValueID IN (" . implode(',', $valueIds) . ")
                INNER JOIN
                        #props_to_templates# ptt
                        ON p.PropertyID = ptt.PropertyID
                        ".(0 < $templateId ? ' AND ptt.PropertyTemplateID = '.$templateId : '')."
                INNER JOIN #prop_groups# pg
						ON ptt.PropertyGroupID = pg.PropertyGroupID 
                LEFT JOIN
                        #prop_units# pu
                        ON p.PropertyUnitID = pu.PropertyUnitID
                WHERE p.PropertyID IN (" . implode(',', $propIds) . ")
                ORDER BY
                		pg.SortIndex ASC,
                        ptt.SortIndex ASC";
		
		$result = $this->query($sql)->toArray();
		if (isset($_GET['debug_filter'])) {
			print_r($this->query($sql)); exit();
		}
		$cacher->set($result);
		return $result;
	}

	/**
	 *
	 * @param array $brandsIds        	
	 * @return array
	 */
	public function enumBrandsInfo(array $brandsIds) {
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if ($cacher->has()) return $cacher->get();
		$sql = "SELECT
                        b.BrandID as ID,
                        b.BrandName,
                        b.BrandUID
                    FROM
                        #brands# b
                    WHERE 
                        b.BrandID IN (" . implode(',', $brandsIds) . ")";
		$result = $this->query($sql)->toArray();
		$cacher->set($result);
		return $result;
	}

	/**
	 *
	 * @param array $goodIds        	
	 * @param integer $priceCategory        	
	 * @param integer $zoneId        	
	 * @param boolean $isPostDelivery        	
	 * @return array
	 */
	public function getPriceRangeForGoodIds(array $goodIds, $priceCategory, $zoneId, $isPostDelivery = false) {
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if ($cacher->has()) return $cacher->get();
		
		$sql = "
            SELECT
                        MIN(gp.Value) AS minPrice,
                        MAX(gp.Value) AS maxPrice
                FROM
                        #goods_prices# gp";
		if ($isPostDelivery) {
			$sql .= "
                    INNER JOIN
                            #all_goods# ag
                            ON
                                ag.GoodID = gp.GoodID
                                AND ag.Volume > 0
                                AND ag.Weight > 0
                    ";
		}
		$sql .= "
                WHERE
                        gp.GoodID IN (" . implode(',', $goodIds) . ")
                        AND gp.ZoneID = " . $zoneId . "
                        AND gp.CategoryID = " . $priceCategory;
		$rows = $this->query($sql);
		if (0 < $rows->count()) {
			$result = $rows->current();
			$cacher->set($result);
			return $result;
		}
		return null;
	}

	/**
	 *
	 * @param integer $categoryId        	
	 * @param integer $cityId        	
	 * @param integer $priceCategory        	
	 * @param integer $zoneId        	
	 * @param boolean $isPostDelivery        	
	 * @return array | null
	 */
	public function getPriceRangeForCategory($categoryId, $cityId, $priceCategory, $zoneId, $isPostDelivery = false) {
		if (GOD_MODE) {
			$hardCategoryIds = $this->enumHardCategoryIdsBySoftCategoryId($categoryId);
			if (0 == sizeof($hardCategoryIds)) {
				$hardCategoryIds = $this->enumChildHardCategoryIdsBySoftCategoryId($categoryId);
			}
			if (0 == sizeof($hardCategoryIds)) {
				return null;
			}
			$sql = "SELECT
                            MIN(gp.Value) AS minPrice,
                            MAX(gp.Value) AS maxPrice
                    FROM
                            #all_goods# avg
                    INNER JOIN
                            #goods_prices# gp
                            ON
                                avg.GoodID = gp.GoodID
                                AND gp.ZoneID = " . $zoneId . "
                                AND gp.CategoryID = " . $priceCategory . " ";
			if ($isPostDelivery) {
				$sql .= "
                    INNER JOIN
                            #all_goods# ag
                            ON
                                ag.GoodID = avg.GoodID
                                AND ag.Volume > 0
                                AND ag.Weight > 0
                    ";
			}
			$sql .= "
                    WHERE
                            avg.CategoryID IN (" . implode(',', $hardCategoryIds) . ")";
			$rows = $this->query($sql);
			if (0 < $rows->count()) {
				$result = $rows->current();
				return $result;
			}
			return null;
		}
		
		if (MANAGER_MODE) {
			$hardCategoryIds = $this->enumHardCategoryIdsBySoftCategoryId($categoryId);
			if (0 == sizeof($hardCategoryIds)) {
				$hardCategoryIds = $this->enumChildHardCategoryIdsBySoftCategoryId($categoryId);
			}
			if (0 == sizeof($hardCategoryIds)) {
				return null;
			}
			$sql = "SELECT
                            MIN(gp.Value) AS minPrice,
                            MAX(gp.Value) AS maxPrice
                    FROM
                            #avail_manager_goods# avg
                    INNER JOIN
                            #goods_prices# gp
                            ON
                                avg.GoodID = gp.GoodID
                                AND gp.ZoneID = " . $zoneId . "
                                AND gp.CategoryID = " . $priceCategory . " ";
			if ($isPostDelivery) {
				$sql .= "
                    INNER JOIN
                            #all_goods# ag
                            ON
                                ag.GoodID = avg.GoodID
                                AND ag.Volume > 0
                                AND ag.Weight > 0
                    ";
			}
			$sql .= "
                    WHERE
                            avg.HardCategoryID IN (" . implode(',', $hardCategoryIds) . ")
                            AND avg.CityID = " . $cityId;
			$rows = $this->query($sql);
			if (0 < $rows->count()) {
				$result = $rows->current();
				return $result;
			}
			return null;
		}
		
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if ($cacher->has()) return $cacher->get();
		
		$hardCategoryIds = $this->enumHardCategoryIdsBySoftCategoryId($categoryId);
		if (0 == sizeof($hardCategoryIds)) {
			$hardCategoryIds = $this->enumChildHardCategoryIdsBySoftCategoryId($categoryId);
		}
		if (0 == sizeof($hardCategoryIds)) {
			return null;
		}
		$sql = "SELECT
                            MIN(gp.Value) AS minPrice,
                            MAX(gp.Value) AS maxPrice
                    FROM
                            #avail_goods# avg
                    INNER JOIN
                            #goods_prices# gp
                            ON
                                avg.GoodID = gp.GoodID
                                AND gp.ZoneID = " . $zoneId . "
                                AND gp.CategoryID = " . $priceCategory . " ";
		if ($isPostDelivery) {
			$sql .= "
                    INNER JOIN
                            #all_goods# ag
                            ON
                                ag.GoodID = avg.GoodID
                                AND ag.Volume > 0
                                AND ag.Weight > 0
                    ";
		}
		$sql .= "
                    WHERE
                            avg.HardCategoryID IN (" . implode(',', $hardCategoryIds) . ")
                            AND avg.CityID = " . $cityId;
		$rows = $this->query($sql);
		if (0 < $rows->count()) {
			$result = $rows->current();
			$cacher->set($result);
			return $result;
		}
		return null;
	}

	/**
	 *
	 * @param integer $softCategoryId        	
	 * @return array
	 */
	public function enumPropertyTemplatesForCategory($softCategoryId) {
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if ($cacher->has()) return $cacher->get();
		
		$sql = "
            SELECT
                    PropertyTemplateID
            FROM
                    #templates_to_site_categories#
            WHERE
                    SiteCategoryID =" . $softCategoryId;
		$rows = $this->query($sql);
		$result = ArrayHelper::enumOneColumn($rows, 'PropertyTemplateID');
		$cacher->set($result);
		return $result;
	}

	/**
	 *
	 * @param array $goodIds        	
	 * @return array
	 */
	public function enumPropertyTemplatesForGoods(array $goodIds) {
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if ($cacher->has()) return $cacher->get();
		$sql = "
            SELECT
                        ttsc.PropertyTemplateID
                FROM
                        #all_goods# alg
                INNER JOIN #goods_to_soft_categories# gtsc
                        ON alg.GoodID = gtsc.GoodID
                INNER JOIN
                        #templates_to_site_categories# ttsc
                        ON gtsc.SoftCategoryID = ttsc.SiteCategoryID
                WHERE
                        alg.GoodID IN (" . implode(',', $goodIds) . ")";
		$rows = $this->query($sql);
		$result = ArrayHelper::enumOneColumn($rows, 'PropertyTemplateID');
		// var_dump($result);die;
		$cacher->set($result);
		return $result;
	}

	/**
	 *
	 * @param integer $softCategoryId        	
	 * @return array
	 */
	private function enumHardCategoryIdsBySoftCategoryId($softCategoryId) {
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if (false && $cacher->has()) return $cacher->get();
		
		$sql = "
            SELECT DISTINCT
                    ag.CategoryID AS HardCategoryID
                FROM
                    #goods_to_soft_categories# gtsc
        		INNER JOIN #all_goods# ag
        			ON gtsc.GoodID = ag.GoodID
                WHERE
                    gtsc.SoftCategoryID = " . $softCategoryId;
		$rows = $this->query($sql);
		$result = ArrayHelper::enumOneColumn($rows, 'HardCategoryID');
		$cacher->set($result);
		return $result;
	}

	/**
	 *
	 * @param integer $softCategoryId        	
	 * @return array
	 */
	private function enumChildHardCategoryIdsBySoftCategoryId($softCategoryId) {
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if (false && $cacher->has()) return $cacher->get();
		
		$sql = "
            SELECT
                        *
                FROM
                        #soft_categories#
                WHERE
                        SoftCategoryID = " . $softCategoryId;
		$category = $this->query($sql)->current();
		
		$sql = "
            SELECT
                        SoftCategoryID
                FROM
                        #soft_categories#
                WHERE
                        cLeft > " . $category->cLeft . "
                        AND cRight < " . $category->cRight . "
                        AND cLevel > " . $category->cLevel;
		$rows = $this->query($sql);
		$childCategoryIds = ArrayHelper::enumOneColumn($rows, 'SoftCategoryID');
		
		if (0 == sizeof($childCategoryIds)) {
			return [];
		}
		$sql = "
            SELECT DISTINCT
                        ag.CategoryID AS HardCategoryID
                FROM
                        #goods_to_soft_categories# gtsc
        		INNER JOIN #all_goods# ag
        			ON gtsc.GoodID = ag.GoodID
                WHERE
                        gtsc.SoftCategoryID IN (" . implode(',', $childCategoryIds) . ")";
		$rows = $this->query($sql);
		$result = ArrayHelper::enumOneColumn($rows, 'HardCategoryID');
		$cacher->set($result);
		return $result;
	}

	public function getFilterTypesByIds($propertyIds) {
		$sql = "SELECT PropertyID, PropertyTypeID FROM #props# WHERE PropertyID IN (" . join(',', $propertyIds) . ")";
		
		return $this->query($sql);
	}

}
