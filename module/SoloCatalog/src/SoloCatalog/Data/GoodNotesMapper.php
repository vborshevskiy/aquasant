<?php

namespace SoloCatalog\Data;

use Solo\Db\Mapper\AbstractMapper;

class GoodNotesMapper extends AbstractMapper {

    public function getNotesByGoodId($goodId) {
        $sql = 'SELECT 
                  gn.TypeID,
                  gn.Text
                FROM 
                  #good_notes# gn
                WHERE 
                  gn.GoodID = ' . $goodId;

        return $this->query($sql);
    }
}
