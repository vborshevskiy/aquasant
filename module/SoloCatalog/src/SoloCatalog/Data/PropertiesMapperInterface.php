<?php

namespace SoloCatalog\Data;

use Zend\Db\ResultSet\ResultSet;

interface PropertiesMapperInterface {

	/**
	 *
	 * @return ResultSet
	 */
	public function findAllBrandsAndModels();

}

?>