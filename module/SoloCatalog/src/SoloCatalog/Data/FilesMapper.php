<?php

namespace SoloCatalog\Data;

use Solo\Db\Mapper\AbstractMapper;

class FilesMapper extends AbstractMapper implements FilesMapperInterface {

    public function getFilesByGoodId($goodId) {
        $sql = 'SELECT
                    f.*
                FROM 
                    #files# f
                INNER JOIN 
                    #files_to_goods# ftg
                ON 
                    f.FileID = ftg.FileID
                WHERE 
                    ftg.GoodID = '. $goodId;

        return $this->query($sql);
    }
}
