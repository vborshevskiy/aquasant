<?php

namespace SoloCatalog\Data;

use Solo\Db\TableGateway\TableInterface;

interface AllGoodsTableInterface extends TableInterface {
	
	/**
	 * @return array
	 */
	public function enumAllGoodIds();

}

?>