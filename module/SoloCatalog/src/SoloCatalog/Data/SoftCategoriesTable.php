<?php

namespace SoloCatalog\Data;

use Solo\Db\TableGateway\AbstractTable;

class SoftCategoriesTable extends AbstractTable implements SoftCategoriesTableInterface {

	/**
	 * (non-PHPdoc)
	 * @see \SoloCatalog\Data\SoftCategoriesTableInterface::updateLinkedCategoriesUids()
	 */
	public function updateLinkedCategoriesUids() {
		$sql = "UPDATE
					".$this->getTableGateway()->getTable()." sc1
				INNER JOIN
					" . $this->getTableGateway()->getTable() . " sc2
					ON sc1.SoftCategoryID = sc2.RealSoftCategoryID
				SET
					sc2.CategoryUID = sc1.CategoryUID
				WHERE
					sc2.RealSoftCategoryID > 0";
		$this->query($sql);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \SoloCatalog\Data\SoftCategoriesTableInterface::updateNestedSetParameters()
	 */
	public function updateNestedSetParameters($categoryId, $level, $left, $right) {
		$update = $this->createUpdate();
		$update->set(['cLevel' => $level, 'cLeft' => $left, 'cRight' => $right]);
		$update->where(['SoftCategoryID' => $categoryId]);
		return $this->tableGateway->updateWith($update);
	}
	
}

?>