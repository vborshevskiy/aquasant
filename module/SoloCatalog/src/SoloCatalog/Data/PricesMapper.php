<?php

namespace SoloCatalog\Data;

use SoloCache\Service\ProvidesCache;
use Solo\Db\Mapper\AbstractMapper;

class PricesMapper extends AbstractMapper implements PricesMapperInterface {

    use ProvidesCache;

    /**
     *
     * @param integer $goodId
     * @param integer $cityId
     * @param integer $priceColumnId
     * @return integer
     */
    public function getPriceByGoodId($goodId, $cityId, $priceColumnId) {
        $sql = 'SELECT
                        gp.Value
                FROM
                        #goods_prices:active# gp
                INNER JOIN
                        #locations:active# loc
                        ON loc.LocationZoneID = gp.ZoneID
                WHERE
                        gp.GoodID = ' . $goodId . '
                        AND loc.LocationID = ' . $cityId . '
                        AND gp.CategoryID = ' . $priceColumnId . '
                LIMIT 1';
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            return $rows->current()->Value;
        }
        return 0;
    }

    /**
     *
     * @param integer $goodId
     * @param integer $cityId
     * @param integer $priceColumnId
     * @return Ambigous <\Solo\Db\QueryGateway\Ambigous, \Zend\Db\Adapter\Driver\StatementInterface, \Zend\Db\ResultSet\Zend\Db\ResultSet, \Zend\Db\Adapter\Driver\ResultInterface, \Zend\Db\ResultSet\Zend\Db\ResultSetInterface>
     */
    public function getPriceWithOldPriceByGoodId($goodId, $cityId, $priceColumnId) {
        $sql = 'SELECT
                        gp.Value,
                        gp.PrevValue
                FROM
                        #goods_prices:active# gp
                INNER JOIN
                        #locations:active# loc
                        ON loc.LocationZoneID = gp.ZoneID
                WHERE
                        gp.GoodID = ' . $goodId . '
                        AND loc.LocationID = ' . $cityId . '
                        AND gp.CategoryID = ' . $priceColumnId . '
                LIMIT 1';
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            return $rows->current();
        }
        return null;
    }

    public function getPriceWithOldPriceByGoodIds($goodIds, $cityId, $priceColumnId) {
        $sql = 'SELECT
                        gp.GoodID,
                        gp.Value,
                        gp.PrevValue
                FROM
                        #goods_prices:active# gp
                INNER JOIN
                        #locations:active# loc
                        ON loc.LocationZoneID = gp.ZoneID
                WHERE
                        gp.GoodID IN (' . implode(',', $goodIds) . ')
                        AND loc.LocationID = ' . $cityId . '
                        AND gp.CategoryID = ' . $priceColumnId;
        return $this->query($sql);
    }

    /**
     *
     * @param integer $goodId
     * @param integer $priceColumnId
     * @return integer
     */
    public function getPricesByGoodId($goodId, $priceColumnId) {
        $sql = 'SELECT
                        gp.ZoneID,
                        gp.Value,
                        loc.LocationID
                FROM
                        #goods_prices:active# gp
                INNER JOIN
                        #locations:active# loc
                        ON loc.LocationZoneID = gp.ZoneID
                WHERE
                        gp.GoodID = ' . $goodId . '
                        AND gp.CategoryID = ' . $priceColumnId;
        return $this->query($sql)->toArray();
    }

    /**
     *
     * @param array $goodIds
     * @param integer $cityId
     * @param integer $priceColumnId
     * @return array
     */
    public function getPriceByGoodIds($goodIds, $cityId, $priceColumnId) {
        $sql = 'SELECT
                        gp.GoodID,
                        gp.Value
                FROM
                        #goods_prices:active# gp
                INNER JOIN
                        #locations:active# loc
                        ON loc.LocationZoneID = gp.ZoneID
                WHERE
                        gp.GoodID IN (' . implode(',', $goodIds) . ')
                        AND loc.LocationID = ' . $cityId . '
                        AND gp.CategoryID = ' . $priceColumnId;
        return $this->query($sql);
    }

    /**
     * @param integer $cityId     
     * @return integer
     */
    public function getZoneId($cityId) {
        $sql = 'SELECT
                        LocationZoneID as ZoneID                        
                FROM
                        #locations:active#              
                WHERE
                        LocationID = ' . $cityId;
        return $this->query($sql)->current();
    }

    /**
     * @dependency_table goods_prices:active
     *
     * @param array $goodIds        	
     * @return Ambigous <\Solo\Db\QueryGateway\Ambigous, \Zend\Db\Adapter\Driver\StatementInterface, \Zend\Db\ResultSet\Zend\Db\ResultSet, \Zend\Db\Adapter\Driver\ResultInterface, \Zend\Db\ResultSet\Zend\Db\ResultSetInterface>
     */
    public function enumPricesByGoodIds(array $goodIds) {
        $cacher = $this->createMethodCacher(__METHOD__, func_get_args());
        if ($cacher->has())
            return $cacher->get();

        $sql = "SELECT
					gp.*
				FROM
					#goods_prices# gp
				WHERE
					gp.GoodID IN (" . implode(',', $goodIds) . ")";
        $result = $this->query($sql);
        $cacher->set($result);
        return $result;
    }
    
    /**
     * 
     * @param integer $cityId
     * @param integer $goodId
     * @return float
     */
    public function getDeliveryCost($cityId) {
        if (!is_int($cityId)) {
            return null;
        }
        $sql = 'SELECT
                        dpr.DeliveryPrice
                FROM
                        #delivery_prices:active# dpr
                WHERE
                        dpr.CityID = '.$cityId.'
                LIMIT 1';
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            return (float)$rows->current()->DeliveryPrice;
        }
        return null;
    }
    
    /**
     * 
     * @param array $goodIds
     * @return array
     */
    public function getAdditionalTkDeliveryPricesByGoodIds($goodIds, $cityId) {
        if (!is_array($goodIds) || count($goodIds) == 0) {
            return [];
        }
        $sql = 'SELECT
                        dp.GoodID,
                        SUM(dp.AdditionalTKPrice) AS AdditionalPrice
                FROM
                        #delivery_prices:active# dp
                WHERE
                        dp.GoodID IN (' . implode(',', $goodIds) . ')
                        AND dp.CityID = ' . $cityId . '
                GROUP BY
                        dp.GoodID';
        $rows = $this->query($sql)->toArray();
        $result = [];
        foreach ($rows as $row) {
            $result[(int)$row['GoodID']] = (float)$row['AdditionalPrice'];
        }
        return $result;
    }

    /**
     * 
     * @param array $goodIds
     * @param integer $cityId
     * @param integer $bonusPriceColumnId
     * @return array
     */
    public function getBonusPriceByGoodIds($goodIds, $cityId, $bonusPriceColumnId) {
        if (!is_array($goodIds) || count($goodIds) == 0) {
            return [];
        }
        $cacher = $this->createMethodCacher(__METHOD__, func_get_args());
        if ($cacher->has())
            return $cacher->get();

        $sql = 'SELECT
                            gp.GoodID,
                            gp.Value
                        FROM
                            #goods_prices# gp
                        INNER JOIN
                            #locations# loc
                            ON loc.LocationZoneID = gp.ZoneID
                        WHERE
                            gp.CategoryID = '.$bonusPriceColumnId.'
                            AND loc.LocationID = '.$cityId.'
                            AND gp.Value > 0
                            AND gp.GoodID IN (' . implode(',', $goodIds) . ')';
        $result = $this->query($sql)->toArray();
        $cacher->set($result);
        return $result;
    }
    
    /**
     * 
     * @param integer $goodId
     * @param integer $cityId
     * @param integer $bonusPriceColumnId
     * @return float
     */
    public function getBonusPriceByGoodId($goodId, $cityId, $bonusPriceColumnId) {
        $sql = 'SELECT
                            gp.Value
                        FROM
                            #goods_prices# gp
                        INNER JOIN
                            #locations# loc
                            ON loc.LocationZoneID = gp.ZoneID
                        WHERE
                            gp.CategoryID = '.$bonusPriceColumnId.'
                            AND loc.LocationID = '.$cityId.'
                            AND gp.Value > 0
                            AND gp.GoodID = '.$goodId.'
                        LIMIT 1';
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            return (float)$rows->current()->Value;
        }
        return null;
    }
    
    /**
     * 
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getDeliveryServicePrices() {
        $sql = '
            SELECT
                            dsp.*
                    FROM
                            #delivery_service_prices:active# dsp
                    ORDER BY
                            dsp.DistanceFromCity
        ';
        return $this->query($sql);
    }

}
