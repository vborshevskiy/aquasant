<?php

namespace SoloCatalog\Data;

use SoloCache\Service\ProvidesCache;
use Solo\Db\Mapper\AbstractMapper;

class GoodsRemainsMapper extends AbstractMapper implements GoodsRemainsMapperInterface {

    use ProvidesCache;

    /**
     * @dependency_table stores:active, offices:active
     *
     * @param integer $storeId
     * @return int
     */
    public function getLocationIdByStoreId($storeId) {
        $sql = "SELECT
                        of.OfficeLocationId as id
                FROM
                        #stores# st
                LEFT JOIN
                        #offices# of
                        ON of.OfficeID = st.StoreOfficeID
                WHERE
                        st.StoreID = $storeId
                LIMIT 1
				";
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            $result = $rows->current();
            return $result['id'];
        }
        return null;
    }

    public function getLocations() {
        $sql = "SELECT *
				FROM
					#locations#
				";
        $rows = $this->query($sql)->toArray();
        return $rows;
    }

    public function getStores() {
        $sql = "SELECT DISTINCT
                        StoreID
                FROM
                        #stores#
                ";
        $rows = $this->query($sql)->toArray();
        return $rows;
    }

    public function getAllGoodsWithRemains($cityId) {
        $sql = "SELECT DISTINCT
                        gr.GoodID,
                        gr.StoreID,
                        st.StoreOfficeID as OfficeID,
                        gr.LocationId,
                        alg.CategoryID AS HardCategoryID,
                        alg.BrandID,
                        gr.SellSpeed,
						gr.PlaceRow,
						gr.PlaceCell,
						gr.PlaceRack,
                        gr.Remains - gr.Reserve AS AvailQuantity,
                        st.IsPickup AS StoreIsPickup,
                        st.IsDelivery AS StoreIsDelivery,
                        off.IsDelivery AS OfficeIsDelivery,
                        off.IsPickup AS OfficeIsPickup
                FROM
                        #goods_remains:passive# gr
                INNER JOIN
                        #all_goods:active# alg
                        ON alg.GoodID = gr.GoodID
                INNER JOIN
                        #stores:active# st
                        ON st.StoreID = gr.StoreID
                INNER JOIN
                        #offices:active# off
                        ON st.StoreOfficeID=off.OfficeID
                INNER JOIN
                        #locations:active# loc
                        ON off.OfficeLocationId=loc.LocationID";
        // @todo temporary
         if (false) {
                $sql .= " INNER JOIN
                       #goods_prices:active# pr
                       ON (gr.GoodID=pr.GoodID
                       AND loc.LocationZoneID=pr.ZoneID
                       AND pr.CategoryID=1
                       AND pr.Value>0)";
         }
         $sql .= " WHERE
                        gr.LocationId = $cityId";
        $rows = $this->query($sql, true)->toArray();
        return $rows;
    }

}

?>