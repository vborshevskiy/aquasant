<?php

namespace SoloCatalog\Data;

interface FiltersMapperInterface {

    /**
     *
     * @param integer $categoryId        	
     * @param array $priceRange        	
     * @return array
     */
    public function enumWarehousesByCategoryId($categoryId, $cityId, array $priceRange);

    /**
     * 
     * @param integer $categoryId
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @param array $priceRange
     * @return array
     */
    public function enumBrandsByCategoryId($categoryId, $cityId, $isPostDelivery = false, array $priceRange = null);

    /**
     *
     * @param array $goodIds        	
     * @param array $priceRange        	
     */
    public function enumWarehousesByGoodIds(array $goodIds, $cityId, array $priceRange);

    /**
     * 
     * @param array $goodIds
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @param array $priceRange
     * @return array
     */
    public function enumBrandsByGoodIds(array $goodIds, $cityId, $isPostDelivery = false, array $priceRange = null);

    /**
     * 
     * @param integer $softCategoryId
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @param array $priceRange
     * @return array
     */
    public function enumFeaturesByCategoryId($softCategoryId, $cityId, $isPostDelivery = false, array $priceRange = null);

    /**
     * 
     * @param array $goodIds
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @param array $priceRange
     * @return array
     */
    public function enumFeaturesByGoodIds(array $goodIds, $cityId, $isPostDelivery = false, array $priceRange = null);

    /**
     * 
     * @param integer $categoryId
     * @param integer $cityId
     * @param array $priceRange
     * @return array
     */
    public function enumGoodsDeliveryDateByCategoryId($categoryId, $cityId, array $priceRange = null);

    public function enumGoodsDeliveryDateByGoodIds(array $goodIds, $cityId, array $priceRange = null);

    /**
     * 
     * @param integer $softCategoryId
     * @param integer $cityId
     * @param array $priceRange
     * @return array
     */
    public function enumOrderGoodsByCategoryId($softCategoryId, $cityId, array $priceRange = null);

    public function enumOrderGoodsByGoodIds(array $goodIds, $cityId, array $priceRange = null);

    /**
     *
     * @param array $propIds        	
     * @param array $valueIds        	
     * @return array
     */
    public function enumFeatureSettings(array $propIds, array $valueIds);

    /**
     *
     * @param array $goodIds        	
     * @return array NULL
     */
    public function getPriceRangeForGoodIds(array $goodIds, $priceCategory, $zoneId);

    /**
     * 
     * @param integer $categoryId
     * @param integer $cityId
     * @param integer $priceCategory
     * @param integer $zoneId
     * @param boolean $isPostDelivery
     * @return array | null
     */
    public function getPriceRangeForCategory($categoryId, $cityId, $priceCategory, $zoneId, $isPostDelivery = false);

    /**
     *
     * @param integer $softCategoryId        	
     * @return array
     */
    public function enumPropertyTemplatesForCategory($softCategoryId);

    /**
     *
     * @param array $goodIds        	
     * @return array
     */
    public function enumPropertyTemplatesForGoods(array $goodIds);
}

?>