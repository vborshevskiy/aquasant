<?php

namespace SoloCatalog\Data;

interface CategoriesMapperInterface {

	/**
	 * @param string $categoryUid
	 * @return mixed
	 */
	public function getCategoryByUid($categoryUid);
	
	/**
	 * @param integer $categoryId
	 * @return mixed | NULL
	 */
	public function getCategoryById($categoryId);
	
	/**
	 * @param integer $hardCategoryId
	 * @return mixed | NULL
	 */
	public function getCategoryByHardCategoryId($hardCategoryId);
	
	/**
	 * 
	 * @param integer $goodId
	 * @return mixed | NULL
	 */
	public function getPrimaryCategoryByGoodId($goodId);
	
	/**
	 *
	 * @param array $ids
	 * @return \Zend\Db\ResultSet\Zend\Db\ResultSet
	 */
	public function enumAvailCategoryIdsBySoftCategoryIds(array $ids);
	
	/**
	 * @param integer $categoryId
	 * @return array
	 */
	public function enumCategoriesByParentId($categoryId);
	
	/**
	 * @param integer $hardCategoryId
	 * @return array | NULL
	 */
	public function enumCategoriesByHardCategoryId($hardCategoryId);
	
}

?>