<?php

namespace SoloCatalog\Data;

use Solo\Db\Mapper\AbstractMapper;

class SitemapMapper extends AbstractMapper implements SitemapMapperInterface {

    /**
     *
     * @return array
     */
    public function getCities() {
        $sql = 'SELECT DISTINCT
                        cit.CityID
                FROM
                        #offices:active# of
                INNER JOIN
                        #cities:active# cit
                        ON cit.CityID = of.OfficeLocationId
                WHERE
                        of.Hide = 0';
        return \Solo\Stdlib\ArrayHelper::enumOneColumn($this->query($sql)->toArray(), 'CityID');
    }
    
    /**
     * 
     * @param integer $cityId
     * @return array
     */
    public function getAvailGoods($cityId) {
        $sql = 'SELECT
                        ag.GoodID,
                        ag.GoodEngName,
                        sc.CategoryUID
                FROM
                        #all_goods:active# ag
                INNER JOIN
                        #avail_goods:active# avg
                        ON avg.GoodID = ag.GoodID
                        AND avg.CityID = ' . $cityId.'
                INNER JOIN
                        #goods_to_soft_categories:active# gtsc
                        ON gtsc.GoodID = ag.GoodID
                INNER JOIN
                        #soft_categories:active# sc
                        ON sc.SoftCategoryID = gtsc.SoftCategoryID';
        $rows = $this->query($sql);
        $result = [];
        foreach ($rows as $row) {
            $result[(int)$row['GoodID']] = $row;
        }
        return $result;
    }
    
    public function getCategories($cityId) {
        $sql = 'SELECT DISTINCT
                        sc.SoftCategoryID,
                        sc.CategoryUID
                FROM
                        #all_goods:active# ag
                INNER JOIN
                        #avail_goods:active# avg
                        ON avg.GoodID = ag.GoodID
                        AND avg.CityID = ' . $cityId.'
                INNER JOIN
                        #goods_to_soft_categories:active# gtsc
                        ON gtsc.GoodID = ag.GoodID
                INNER JOIN
                        #soft_categories:active# sc
                        ON sc.SoftCategoryID = gtsc.SoftCategoryID';
        $rows = $this->query($sql);
        $result = [];
        foreach ($rows as $row) {
            $result[(int)$row['SoftCategoryID']] = $row;
        }
        return $result;
    }
}

?>