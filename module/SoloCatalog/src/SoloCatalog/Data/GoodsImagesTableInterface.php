<?php

namespace SoloCatalog\Data;

use Solo\Db\TableGateway\TableInterface;

interface GoodsImagesTableInterface extends TableInterface {

    public function getUnresizedImages();
}

?>