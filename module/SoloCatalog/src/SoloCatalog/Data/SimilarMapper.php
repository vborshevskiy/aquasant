<?php

namespace SoloCatalog\Data;

use SoloCache\Service\ProvidesCache;
use Solo\Db\Mapper\AbstractMapper;

class SimilarMapper extends AbstractMapper implements SimilarMapperInterface {
	
	use ProvidesCache;

	/**
	 * @dependency_table all_goods:active
	 * @dependency_table avail_goods:active
	 * @dependency_table goods_prices:active
	 *
	 * @param integer $goodId        	
	 * @param integer $limit        	
	 * @param string $priceColumn        	
	 * @return \Zend\Cache\Storage\mixed multitype: Ambigous <\Solo\Db\QueryGateway\Ambigous, \Zend\Db\Adapter\Driver\StatementInterface, \Zend\Db\ResultSet\Zend\Db\ResultSet, \Zend\Db\Adapter\Driver\ResultInterface, \Zend\Db\ResultSet\Zend\Db\ResultSetInterface>
	 */
	public function enumSimilarGoods($goodId, $cityId, $limit = 12, $priceColumn = 'RetailPrice') {
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if ($cacher->has()) return $cacher->get();
		
		$good = $this->getAvailGoodById($goodId, $cityId);
		if (null == $good) {
			return [];
		}
		$sql = "SELECT
					alg.GoodID,
					alg.GoodName,
					alg.GoodEngName,
					gp." . $priceColumn . " AS Price,
					gp.BonusPrice
				FROM
					#all_goods# alg
				INNER JOIN
					#avail_goods# ag
					ON (
						alg.GoodID = ag.GoodID
						AND ag.CityID = " . $cityId . "
					)
				INNER JOIN
					catalog_info ci
					ON ci.goods_id = alg.GoodID
				INNER JOIN
					#goods_prices@cityId:{$cityId}# gp
					ON (
						gp.CityID = " . $cityId . "
						AND gp.GoodID = alg.GoodID
					)
				INNER JOIN
					pic_found pf
					ON pf.good_id = alg.GoodID
				WHERE
					alg.GoodID <> " . $good->GoodID . "
					AND alg.CategoryID = " . $good->HardCategoryID . "
				ORDER BY
					alg.AddDate DESC
				LIMIT " . $limit;
		$result = $this->query($sql);
		$cacher->set($result);
		return $result;
	}

	/**
	 * @dependency_table avail_goods:active
	 *
	 * @param integer $goodId        	
	 * @return \ArrayObject
	 */
	private function getAvailGoodById($goodId, $cityId) {
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if ($cacher->has()) return $cacher->get();
		
		$sql = "SELECT
					ag.*
				FROM
					#avail_goods# ag
				WHERE
					ag.GoodID = " . $goodId . "
					AND ag.CityID = " . $cityId . "
				LIMIT 1";
		$rows = $this->query($sql);
		if (0 < $rows->count()) {
			$result = $rows->current();
			$cacher->set($result);
			return $result;
		}
		return null;
	}

}

?>