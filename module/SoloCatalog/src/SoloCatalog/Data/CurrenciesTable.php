<?php

namespace SoloCatalog\Data;

use Solo\Db\TableGateway\AbstractTable;

class CurrenciesTable extends AbstractTable implements CurrenciesTableInterface {

    /**
     * (non-PHPdoc)
     * @see \SoloCatalog\Data\CurrenciesTableInterface::getCurrencyById()
     */
    public function getCurrencyById($currencyId) {
        $select = $this->createSelect();
        $select->where(['CurrencyID' => $currencyId]);
        return $this->tableGateway->selectWith($select);
    }
    
    /**
     * (non-PHPdoc)
     * @see \SoloCatalog\Data\CurrenciesTableInterface::getCurrenciesByIds()
     */
    public function getCurrenciesByIds($currenciesIds) {
        $select = $this->createSelect();
        $select->where(['CurrencyID' => $currenciesIds]);
        return $this->tableGateway->selectWith($select);
    }
}

?>