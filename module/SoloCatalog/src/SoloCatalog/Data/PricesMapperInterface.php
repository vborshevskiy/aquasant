<?php

namespace SoloCatalog\Data;

interface PricesMapperInterface {
    
    const OVERSIZED_MOSCOW_DELIVERY_ID = 12;
    const MEDIUM_MOSCOW_DELIVERY_ID = 20;
    const SMALL_MOSCOW_DELIVERY_ID = 9;

    /**
     *
     * @param integer $goodId
     * @param integer $cityId
     * @param integer $priceColumnId
     * @return integer
     */
    public function getPriceByGoodId($goodId, $cityId, $priceColumnId);

    /**
     *
     * @param integer $goodId
     * @param integer $cityId
     * @param integer $priceColumnId
     * @return Ambigous <\Solo\Db\QueryGateway\Ambigous, \Zend\Db\Adapter\Driver\StatementInterface, \Zend\Db\ResultSet\Zend\Db\ResultSet, \Zend\Db\Adapter\Driver\ResultInterface, \Zend\Db\ResultSet\Zend\Db\ResultSetInterface>
     */
    public function getPriceWithOldPriceByGoodId($goodId, $cityId, $priceColumnId);

    /**
     *
     * @param integer $goodId
     * @param integer $priceColumnId
     * @return integer
     */
    public function getPricesByGoodId($goodId, $priceColumnId);

    /**
     *
     * @param array $goodIds
     * @param integer $cityId
     * @param integer $priceColumnId
     * @return array
     */
    public function getPriceByGoodIds($goodIds, $cityId, $priceColumnId);

    /** 	 
     * @param integer $cityId    
     * @return integer
     */
    public function getZoneId($cityId);

    /**
     *
     * @param array $goodIds        	
     * @return Ambigous <\Solo\Db\QueryGateway\Ambigous, \Zend\Db\Adapter\Driver\StatementInterface, \Zend\Db\ResultSet\Zend\Db\ResultSet, \Zend\Db\Adapter\Driver\ResultInterface, \Zend\Db\ResultSet\Zend\Db\ResultSetInterface>
     */
    public function enumPricesByGoodIds(array $goodIds);
    
    /**
     * 
     * @param array $goodIds
     * @param integer $cityId
     * @param integer $bonusPriceColumnId
     * @return array
     */
    public function getBonusPriceByGoodIds($goodIds, $cityId, $bonusPriceColumnId);
    
    /**
     * 
     * @param integer $goodId
     * @param integer $cityId
     * @param integer $bonusPriceColumnId
     * @return float
     */
    public function getBonusPriceByGoodId($goodId, $cityId, $bonusPriceColumnId);
    
    /**
     * 
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getDeliveryServicePrices();
    
}
