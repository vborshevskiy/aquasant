<?php

namespace SoloCatalog\Data;

use Solo\Db\Mapper\AbstractMapper;

class BrandsMapper extends AbstractMapper implements BrandsMapperInterface {

    /**
     * 
     * @param array $brandIds
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getBrandByIds($brandIds) {
        $sql = 'SELECT
                        b.*
                    FROM
                        #brands:active# b
                    WHERE
                        BrandID IN ('.implode(',',$brandIds).')';
        return $this->query($sql);
    }
    
    /**
     * 
     * @param integer $brandId
     * @return mixed
     */
    public function getBrandById($brandId) {
        $sql = 'SELECT
                        b.*
                    FROM
                        #brands:active# b
                    WHERE
                        BrandID  = '.$brandId;
        $rows = $this->query($sql);
        if ($rows->count() > 0) {
            return $rows->current();
        }
        return null;
    }
    
    /**
     * 
     * @param string $brandUid
     * @return mixed
     */
    public function getBrandByUid($brandUid) {
        $sql = 'SELECT
                        b.*
                    FROM
                        #brands:active# b
                    WHERE
                        BrandUID  = "'.$brandUid.'"';
        $rows = $this->query($sql);
        if ($rows->count() > 0) {
            return $rows->current();
        }
        return null;
    }

}
