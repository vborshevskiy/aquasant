<?php

namespace SoloCatalog\Data;

use Solo\Db\Mapper\AbstractMapper;

class GoodsFiltersAspectsMapper extends AbstractMapper implements GoodsFiltersAspectsMapperInterface {

	public function refillAspects() {
		$sql = "TRUNCATE TABLE #goods_filters_aspects:passive#";
		$this->query($sql);
		
		$sql = "INSERT INTO #goods_filters_aspects:passive#
				(                    
                    SELECT DISTINCT
                            NULL AS ID,
                            ag.GoodID,
                            gtsc.SoftCategoryID,
                            p.PropertyTypeID AS AspectType,
                            gpv.PropertyID,
                            gpv.PropertyValueID,
                            p.KindID as AspectKind,
                            gpv.ValueBoolean,
                            gpv.ValueNumeric,
                            pv.Image
                    FROM
                            #avail_goods:active# ag
                    INNER JOIN
                            #goods_to_propvalues:passive# gpv
                                ON
                                gpv.GoodID = ag.GoodID
                    INNER JOIN
                            #prop_values:passive# pv
                            ON
                            pv.PropertyValueId = gpv.PropertyValueId            
                    INNER JOIN
                            #all_goods:active# alg
                                ON
                                alg.GoodID = ag.GoodID
                    INNER JOIN #goods_to_soft_categories:active# gtsc
                               ON gtsc.GoodID = alg.GoodID
                    INNER JOIN
                            #props:passive# p
                                ON
                                p.PropertyID = gpv.PropertyID
                    INNER JOIN
                            #props_to_templates:passive# ptm
                                ON
                                ptm.PropertyID = gpv.PropertyID				
                    #WHERE 
                        #p.KindID IN (2,4)
                        )";
		$this->query($sql);
	}
	
}

?>