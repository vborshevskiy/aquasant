<?php

namespace SoloCatalog\Data;

use Solo\Db\TableGateway\TableInterface;

interface CurrenciesTableInterface extends TableInterface {

    /**
	 * 
	 * @param integer $currencyId
     * @return mixed
	 */
	public function getCurrencyById($currencyId);
    
    /**
	 * 
	 * @param $currenciesIds
     * @return mixed
	 */
    public function getCurrenciesByIds($currenciesIds);
}

?>