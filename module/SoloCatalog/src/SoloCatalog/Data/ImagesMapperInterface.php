<?php

namespace SoloCatalog\Data;

interface ImagesMapperInterface {
    
    /*
     * @param integer $goodId
     * @return array
     */
    public function getImagesByGoodId($goodId);
    
    /*
     * @param integer $goodId
     * @return mixed
     */
    public function getGoodMainImage($goodId);

}

?>