<?php

namespace SoloCatalog\Data;

use Zend\Db\ResultSet\ResultSet;
use SoloCatalog\Entity\Goods\FilterOptions;

interface GoodsMapperInterface {

	/**
	 *
	 * @param FilterOptions $options        	
	 * @param integer $cityId        	
	 * @return array
	 */
	public function enumFilterGoods(FilterOptions $options, $cityId, $priceCategoryId, $priceZoneId);

	/**
	 *
	 * @param array $goodIds        	
	 * @return array
	 */
	public function enumWarehouseQuantityByGoodIds(array $goodIds);

	/**
	 *
	 * @param integer $goodId        	
	 * @return \Zend\Db\ResultSet\Zend\Db\ResultSetInterface
	 */
	public function enumWarehouseQuantityByGoodId($goodId);

	/**
	 *
	 * @param integer $goodId        	
	 * @return \Zend\Db\ResultSet\Zend\Db\ResultSetInterface
	 */
	public function enumWarehouseAvailabilityByGoodId($goodId);

	/**
	 *
	 * @param integer $goodId
	 * @param integer $cityId
	 * @return \ArrayObject
	 */
	public function getAvailabilityByGoodId($goodId, $cityId);
    
    /**
	 *
	 * @param array $goodIds
	 * @param integer $cityId
	 * @return \ArrayObject
	 */
	public function getAvailabilityByGoodIds($goodIds, $cityId);
        
        /**
	 *
	 * @param array $goodIds
	 * @param integer $cityId
	 * @return \ArrayObject
	 */        
        public function getAvailGoodWithCategoryUIDByGoodId($goodIds, $cityId);
        
        /**
         *
         * @param array $goodIds
         * @param integer $cityId
         * @return \ArrayObject
         */
        public function getGoodWithCategoryUIDByGoodId($goodIds, $cityId);

	/**
	 *
	 * @param array $goodIds
     * @param integer|null $cityId
	 * @return \Zend\Db\ResultSet\Zend\Db\ResultSetInterface
	 */
	public function enumAvailabilityByGoodIds(array $goodIds, $cityId);

	/**
	 *
	 * @param array $goodIds        	
	 * @return \Zend\Db\ResultSet\Zend\Db\ResultSetInterface
	 */
	public function enumWarehouseAvailabilityByGoodIds(array $goodIds);

	/**
	 *
	 * @param integer $limit        	
	 * @return array
	 */
	public function enumNewGoods($cityId, $limit);

	/**
	 *
	 * @param int $limit        	
	 * @return array
	 */
	public function enumActionGoods($cityId, $limit);

	/**
	 *
	 * @param array $goodIds        	
	 * @return \Zend\Db\ResultSet\Zend\Db\ResultSetInterface
	 */
	public function enumGoodsByIds(array $goodIds);

	/**
	 *
	 * @param integer $goodId        	
	 * @return \ArrayObject
	 */
	public function getAvailGoodByGoodId($goodId, $cityId);

	/**
	 * Получаем список свойств выбранного товара
	 *
	 * @param integer $goodId
	 *        	идентификатор товара
	 *        	
	 * @return mixed
	 */
	public function getGoodByGoodId($goodId);

    /**
     * Получаем остатки товара у поставщиков
     *
     * @param integer $goodId
     *        	идентификатор товара
     *
     * @return mixed
     */
    public function getSupplierRemainsByGoodId($goodId, $officeId);

	/**
	 *
	 * @param integer $goodId        	
	 * @return integer
	 */
	public function getImagesCount($goodId);

	/**
	 *
	 * @param int $goodId        	
	 * @return \Zend\Db\ResultSet\Zend\Db\ResultSetInterface
	 */
	public function getGoodDescription($goodId);

	/**
	 *
	 * @param array $goodIds        	
	 * @return array
	 */
	public function enumGoodDescriptionsByGoodIds(array $goodIds);

	/**
	 *
	 * @param array $goodIds        	
	 * @param integer|null $softCategoryId
	 * @return ResultSet
	 */
	public function enumAvailGoodIdsByGoodIds(array $goodIds, $softCategoryId = null);

	/**
	 *
	 * @param integer $goodId        	
	 * @return mixed
	 */
	public function getStats($goodId);

	/**
	 *
	 * @return integer
	 */
	public function getMaxSales();

	/**
	 *
	 * @param array $warehouseIds        	
	 * @param array $goodIds        	
	 * @return array
	 */
	public function selectByWarehouseIdsAndGoodIds($warehouseIds, $goodIds);
    
    /**
	 *
	 * @param integer $goodId
     * @param integer $cityId	
	 */
    public function getTotalAvailGoodByOffices($goodId, $cityId);
    
    /**
	 *
	 * @param array $goodIds
     * @param integer $cityId	
	 */
    public function getAvailByOffices($goodIds, $cityId);
    
    /**
	 *
	 * @param array $goodIds
     * @param integer $cityId	
	 */
    public function getSuborderByOffices($goodIds, $cityId);
    
    /**
	 *
     * @param integer $goodId
     * @param integer cityId
     * @return array
	 */
    public function getDiscountGoods($goodId, $cityId);
    
    /**
	 *
     * @param integer $goodId
     * @param integer cityId
     * @return \Zend\Db\ResultSet\Zend\Db\ResultSetInterface
	 */
    public function getOriginalGood($goodId,$cityId);
    
    public function getDiscountGoodsByCityId($cityId);
    
    /**
     * 
     * @param integer $cityId
     * @return integer
     */
    public function getRandomAvailGood($cityId);
    
    /**
     * 
     * @param integer $cityId
     * @return integer
     */
    public function getRandomSuborderGood($cityId);
    
    
}

?>