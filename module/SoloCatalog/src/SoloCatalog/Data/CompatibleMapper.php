<?php

namespace SoloCatalog\Data;

use SoloCache\Service\ProvidesCache;
use Solo\Db\Mapper\AbstractMapper;

class CompatibleMapper extends AbstractMapper implements CompatibleMapperInterface {

    use ProvidesCache;
    /**
     * 
     * @param integer $goodId
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @return mixed
     */
    public function enumCompatibleGoodIds($goodId, $cityId, $isPostDelivery = false) {
        $sql = 'SELECT
                        c.CompatibleGoodID as GoodID
                FROM
                        #compatible:active# c
                INNER JOIN
                        #avail_goods:active# avg
                        ON avg.GoodID = c.CompatibleGoodID
                INNER JOIN
                        #all_goods:active# ag
                        ON ag.GoodID = c.CompatibleGoodID
                WHERE
                        c.GoodID = ' . intval($goodId) . '
                        AND avg.CityID = ' . intval($cityId);
        if ($isPostDelivery) {
            $sql .= '
                        AND ag.Volume > 0
                        AND ag.Weight > 0
                    ';
        }
        $sql .= '
                ORDER BY
                        c.SortIndex
                ';
        return $this->query($sql)->toArray();
    }
    
    /**
     * 
     * @param array $goodIds
     * @param integer $cityId
     * @param boolean $isPostDelivery Description
     * @return array
     */
    public function enumCompatibleGoodIdsByGoodIds($goodIds, $cityId, $isPostDelivery = false) {
        $sql = 'SELECT
                        c.GoodID,
                        c.CompatibleGoodID,
                        sc.SoftCategoryID
                FROM
                        #compatible:active# c
                INNER JOIN
                        #avail_goods:active# avg
                        ON avg.GoodID = c.CompatibleGoodID
                INNER JOIN
                        #all_goods:active# ag
                        ON ag.GoodID = c.CompatibleGoodID
                INNER JOIN
                        #goods_to_soft_categories:active# gtsc
                        ON ag.GoodID = gtsc.GoodID
                INNER JOIN
                        #soft_categories:active# sc
                        ON sc.SoftCategoryID = gtsc.SoftCategoryID
                WHERE
                        c.GoodID IN (' . implode(',',$goodIds) . ')
                        AND c.CompatibleGoodID NOT IN (' . implode(',',$goodIds) . ')
                        AND avg.CityID = ' . intval($cityId);
        if ($isPostDelivery) {
            $sql .= '
                        AND ag.Volume > 0
                        AND ag.Weight > 0
                    ';
        }
        $rows = $this->query($sql);
        $result = [];
        if (0 < $rows->count()) {
            foreach ($rows as $row) {
                $result[(int)$row['GoodID']][] = [
                    'goodId' => (int)$row['GoodID'],
                    'compatibleGoodId' => (int)$row['CompatibleGoodID'],
                    'categoryId' => (int)$row['SoftCategoryID'],
                ];
            }
        }
        return $result;
    }
    
    /**
     * 
     * @param array $goodIds
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @return array
     */
    public function enumCompatibleCategoriesByGoodIds($goodIds, $cityId, $isPostDelivery = false) {
        $sql = 'SELECT DISTINCT
                        c.GoodID,
                        sc.SoftCategoryID,
                        sc.CategoryName
                FROM
                        #compatible:active# c
                INNER JOIN
                        #avail_goods:active# avg
                        ON avg.GoodID = c.CompatibleGoodID
                INNER JOIN
                        #all_goods:active# ag
                        ON ag.GoodID = c.CompatibleGoodID
                INNER JOIN
                        #goods_to_soft_categories:active# gtsc
                        ON gtsc.GoodID = ag.GoodID
                INNER JOIN
                        #soft_categories:active# sc
                        ON sc.SoftCategoryID = gtsc.SoftCategoryID
                WHERE
                        c.GoodID IN (' . implode(',',$goodIds) . ')
                        AND c.CompatibleGoodID NOT IN (' . implode(',',$goodIds) . ')
                        AND avg.CityID = ' . intval($cityId);
        if ($isPostDelivery) {
            $sql .= '
                        AND ag.Volume > 0
                        AND ag.Weight > 0
                    ';
        }
        $rows = $this->query($sql);
        $result = [];
        if (0 < $rows->count()) {
            foreach ($rows as $row) {
                $result[(int)$row['GoodID']][] = [
                    'goodId' => (int)$row['GoodID'],
                    'categoryId' => (int)$row['SoftCategoryID'],
                    'categoryName' => $row['CategoryName'],
                ];
            }
        }
        return $result;
    }
    
    /**
     * 
     * @param integer $cityId
     * @param boolean $isPostDelivery
     * @return mixed
     */
    public function enumCompatibleGoodIdsOnTextPages($cityId, $isPostDelivery = false) {
        $sql = 'SELECT
                        c.CompatibleGoodID as GoodID
                FROM
                        #compatible:active# c
                INNER JOIN
                        #avail_goods:active# avg
                        ON avg.GoodID = c.CompatibleGoodID
                INNER JOIN
                        #all_goods:active# ag
                        ON ag.GoodID = c.CompatibleGoodID
                WHERE
                        c.OnOtherPages > 0
                        AND avg.CityID = ' . intval($cityId);
        if ($isPostDelivery) {
            $sql .= '
                        AND ag.Volume > 0
                        AND ag.Weight > 0
                    ';
        }
        $sql .= '
                ORDER BY
                        c.SortIndex
                ';
        return $this->query($sql)->toArray();
    }

}
