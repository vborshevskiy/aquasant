<?php

namespace SoloCatalog\Data;

interface MenuMapperInterface {

    /**
     *
     * @return array
     */
    public function enumAllCategories();

    /**
     *
     * @param integer $cityId
     * @return array
     */
    public function enumAllAvailCategoryIds($cityId);

    /**
     *
     * @param array $categoriesIds
     * @param integer $cityId
     * @return array
     */
    public function enumGoodsCountByCategoryIds(array $categoriesIds, $cityId);

    /**
     *
     * @param integer $kindId
     * @return array
     */
    public function enumMenuFilters($kindId);
}

?>