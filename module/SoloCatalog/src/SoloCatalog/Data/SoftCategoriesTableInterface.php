<?php

namespace SoloCatalog\Data;

use Solo\Db\TableGateway\TableInterface;

interface SoftCategoriesTableInterface extends TableInterface {

	public function updateLinkedCategoriesUids();

	/**
	 *
	 * @param integer $categoryId        	
	 * @param integer $level        	
	 * @param integer $left        	
	 * @param integer $right        	
	 */
	public function updateNestedSetParameters($categoryId, $level, $left, $right);

}

?>