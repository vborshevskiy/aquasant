<?php

namespace SoloCatalog\Data;

use Solo\Db\TableGateway\AbstractTable;

class PropertiesTable extends AbstractTable implements PropertiesTableInterface {
    
    const NUMERIC_PROPERTY_TYPE = 1;
    
    const BOOLEAN_PEROPERTY_TYPE = 2;
    
    const STRING_PEROPERTY_TYPE = 3;
    
    const TEXT_PEROPERTY_TYPE = 4;
    
    const ONE_OF_MANY_PROPERTY_TYPE = 5;
    
    const MANY_OF_MANY_PROPERTY_TYPE = 6;
    
    const COMPOSITE_PROPERTY_TYPE = 7;
}

?>