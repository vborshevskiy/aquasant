<?php

namespace SoloCatalog\Data;

use Solo\Db\TableGateway\TableInterface;

interface CommentsTableInterface extends TableInterface {
    
    /**
     * 
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getUncheckedIsRealBuyersComments();
    
    /**
     * 
     * @param integer $commentId
     * @param boolean $isRealBuyer
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function isRealBuyer($commentId, $isRealBuyer);
    
}
