<?php

namespace SoloCatalog\Data;

use Solo\Db\TableGateway\AbstractTable;

class GoodsImagesTable extends AbstractTable implements GoodsImagesTableInterface {

    public function getUnresizedImages() {
        $select = $this->createSelect();
        $select->columns(['GoodID', 'ViewID', 'Url']);
        $select->where('MiniatureUrl IS NULL OR MiniatureUrl = ""');
        $select->limit(500);

        return $this->tableGateway->selectWith($select);
    }

}

?>