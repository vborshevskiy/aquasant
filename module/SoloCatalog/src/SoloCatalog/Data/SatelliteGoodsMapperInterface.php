<?php

namespace SoloCatalog\Data;

interface SatelliteGoodsMapperInterface {

	/**
	 *
	 * @param integer $goodId        	
	 * @param integer $cityId        	
	 * @param integer $priceCategoryId        	
	 */
	public function enumSatelliteGoods($goodId, $cityId, $priceCategoryId);

}

?>