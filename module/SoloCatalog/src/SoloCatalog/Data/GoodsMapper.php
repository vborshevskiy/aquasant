<?php

namespace SoloCatalog\Data;

use Solo\Stdlib\ArrayHelper;
use SoloCatalog\Entity\Goods\FilterOptions;
use Solo\Db\Mapper\AbstractMapper;
use SoloCache\Service\ProvidesCache;

class GoodsMapper extends AbstractMapper implements GoodsMapperInterface {
	use ProvidesCache;

	/**
	 *
	 * @param FilterOptions $options        	
	 * @return array
	 */
	public function enumFilterGoods(FilterOptions $options, $cityId, $priceCategoryId, $priceZoneId) {
		if (GOD_MODE) {
			$hasWarehouseSorting = ($options->hasSorting() && (null !== $options->getSorting('param')) && ('warehouse' == $options->getSorting('param')));
			
			$sql = "SELECT
                                        ag.GoodID,
                                        ag.GoodName,
                                        ag.CategoryID,
                                        ag.GoodEngName,
                                        ag.OriginalGoodID,
                                        ag.DiscountText,
                                        gp.Value,
                                        gp.PrevValue,
										gr.PlaceRow,
										gr.PlaceCell,
										gr.PlaceRack,
                                        1 AS GoodAvailQuantity,
                                        '' AS ArrivalDate,
                                        1 AS DeliveryQuantity,
										0 AS WindowQuantity,
										0 AS SuborderQuantity,
                                        (SELECT
                                                        MiniatureUrl
                                                FROM
                                                        #goods_images# gi
                                                INNER JOIN
                                                        #goods_images_views# giv
                                                        ON gi.ViewID = giv.ViewID
                                                WHERE
                                                        gi.GoodID = ag.GoodID
                                                ORDER BY
                                                        giv.SortIndex
                                                LIMIT 1
                                        ) as Avatar
                                        " . ($hasWarehouseSorting && $options->getSorting('column') > 0 ? ', aw.AvailQuantity' . ($options->getWithSuborder() ? '+agd.SuborderQuantity AS AvailQuantity' : '') : '') . ",
                        				ag.IsPackage
                                FROM
					#all_goods# ag ";
			$sql .= " LEFT OUTER JOIN #goods_remains# gr ON ag.GoodID = gr.GoodID ";
			$sql .= "               INNER JOIN
					#all_goods# agd
                                        ON (ag.GoodID = agd.GoodID )
			                                        LEFT OUTER JOIN
			                                        #goods_prices# gp
			                                        ON (
			                                        ag.GoodID = gp.GoodID
			                                        AND gp.ZoneID = $priceZoneId
			                                        AND gp.CategoryID = $priceCategoryId
			                                        )";
			// $sql .= " INNER JOIN (SELECT * FROM #goods_images# WHERE GoodID IN (" . implode(',', $options->enumGoodIds()) . ")) gi ON ag.GoodID = gi.GoodID ";
			$sql .= "
                                WHERE
                                        ag.GoodID IN (" . implode(',', $options->enumGoodIds()) . ")";
			if ($options->hasSorting()) {
				$sorting = $options->getSorting();
				
				if ('(0 < agd.AvailQuantity)' == $sorting['column']) {
					$sorting['column'] = 'agd.GoodName';
				}
				
				$sql .= " ORDER BY ";
				if ('(0 < agd.AvailQuantity)' == $sorting['column']) {
					$sql .= $sorting['column'] . ' ' . $sorting['direction'] . ", (0 < (agd.AvailQuantity - agd.WindowQuantity)) DESC";
				} else {
					$sql .= $sorting['column'] . ' ' . $sorting['direction'];
				}
				if ('(0 < agd.AvailQuantity)' == $sorting['column']) {
					$sql .= " , agd.ArrivalDate ASC, gp.Value ASC";
				}
			}
			if ($options->hasLimitation()) {
				$limit = $options->getLimitation();
				$sql .= " LIMIT " . (($limit['currentPage'] - 1) * $limit['perPage']) . ", " . $limit['perPage'];
			}
			// print_r($this->query($sql)); exit();
			$rows = $this->query($sql)->toArray();
			return $rows;
		}
		
		if (MANAGER_MODE) {
			$hasWarehouseSorting = ($options->hasSorting() && (null !== $options->getSorting('param')) && ('warehouse' == $options->getSorting('param')));
			
			$sql = "SELECT
                                        ag.GoodID,
                                        ag.GoodName,
                                        ag.CategoryID,
                                        ag.GoodEngName,
                                        ag.OriginalGoodID,
                                        ag.DiscountText,
                                        gp.Value,
                                        gp.PrevValue,
										gr.PlaceRow,
										gr.PlaceCell,
										gr.PlaceRack,
                                        agd.AvailQuantity AS GoodAvailQuantity,
                                        agd.ArrivalDate,
                                        agd.DeliveryQuantity,
										agd.WindowQuantity,
										agd.SuborderQuantity,
										grad.PickupDate,
										grad.DeliveryDate,
										grad.ShowcaseRetrieveDate,
                                        (SELECT
                                                        MiniatureUrl
                                                FROM
                                                        #goods_images# gi
                                                INNER JOIN
                                                        #goods_images_views# giv
                                                        ON gi.ViewID = giv.ViewID
                                                WHERE
                                                        gi.GoodID = ag.GoodID
                                                ORDER BY
                                                        giv.SortIndex
                                                LIMIT 1
                                        ) as Avatar
                                        " . ($hasWarehouseSorting && $options->getSorting('column') > 0 ? ', aw.AvailQuantity' . ($options->getWithSuborder() ? '+agd.SuborderQuantity AS AvailQuantity' : '') : '') . ",
                        				ag.IsPackage
                                FROM
					#all_goods# ag";
			$sql .= " LEFT OUTER JOIN #goods_remains# gr ON ag.GoodID = gr.GoodID ";
			$sql .= " LEFT OUTER JOIN #good_reserve_avail_date# grad ON ag.GoodID = grad.GoodID ";
			$sql .= " INNER JOIN #avail_manager_goods# agd ON (ag.GoodID = agd.GoodID AND agd.CityID = " . $cityId . " )
					INNER JOIN #goods_prices# gp ON (ag.GoodID = gp.GoodID AND gp.ZoneID = $priceZoneId AND gp.CategoryID = $priceCategoryId)";
			// $sql .= " INNER JOIN (SELECT * FROM #goods_images# WHERE GoodID IN (" . implode(',', $options->enumGoodIds()) . ")) gi ON ag.GoodID = gi.GoodID ";
			$sql .= "
                                WHERE
                                        (TRUE OR grad.PickupDate IS NOT NULL)
			                                        	AND ag.GoodID IN (" . implode(',', $options->enumGoodIds()) . ")";
			if ($options->hasSorting()) {
				$sorting = $options->getSorting();
				
				$sql .= " ORDER BY ";
				if ('(0 < agd.AvailQuantity)' == $sorting['column']) {
					$sql .= $sorting['column'] . ' ' . $sorting['direction'] . ", (0 < (agd.AvailQuantity - agd.WindowQuantity)) DESC";
				} else {
					$sql .= $sorting['column'] . ' ' . $sorting['direction'];
				}
				if ('(0 < agd.AvailQuantity)' == $sorting['column']) {
					$sql .= " , agd.ArrivalDate ASC, gp.Value ASC";
				}
			}
			if ($options->hasLimitation()) {
				$limit = $options->getLimitation();
				$sql .= " LIMIT " . (($limit['currentPage'] - 1) * $limit['perPage']) . ", " . $limit['perPage'];
			}
			$rows = $this->query($sql)->toArray();
			if (isset($_GET['debug_sql'])) {
				print_r($this->query($sql));
				exit();
			}
			// echo '<pre>',print_r($sql),'</pre>';
			// echo '<pre>',print_r($rows),'</pre>'; die();
			return $rows;
		}
		
		$hasWarehouseSorting = ($options->hasSorting() && (null !== $options->getSorting('param')) && ('warehouse' == $options->getSorting('param')));
		// Выборка для пользователя
		$sql = "SELECT
					ag.GoodID,
					ag.GoodName,
					ag.CategoryID,
					sc.CategoryName,
					ag.GoodEngName,
					ag.OriginalGoodID,
					ag.DiscountText,
					gp.Value,
					gp.PrevValue,
					gr.PlaceRow,
					gr.PlaceCell,
					gr.PlaceRack,
					agd.AvailQuantity AS GoodAvailQuantity,
					agd.ArrivalDate,
					agd.DeliveryQuantity,
					agd.WindowQuantity,
					agd.SuborderQuantity,
					grad.PickupDate,
					grad.DeliveryDate,
					grad.ShowcaseRetrieveDate,
					(SELECT MiniatureUrl FROM #goods_images# gi
						INNER JOIN #goods_images_views# giv ON gi.ViewID = giv.ViewID
						WHERE gi.GoodID = ag.GoodID ORDER BY giv.SortIndex LIMIT 1
					) as Avatar
					" . ($hasWarehouseSorting && $options->getSorting(
			'column') > 0 ? ', aw.AvailQuantity' . ($options->getWithSuborder() ? '+agd.SuborderQuantity AS AvailQuantity' : '') : '') . ",
					ag.IsPackage,
					b.BrandName

				FROM #all_goods# ag

				INNER JOIN #goods_to_soft_categories# gtsc ON ag.GoodID = gtsc.GoodID AND gtsc.IsPrimary = 1
				LEFT OUTER JOIN #goods_remains# gr ON ag.GoodID = gr.GoodID
				LEFT OUTER JOIN #good_reserve_avail_date# grad ON ag.GoodID = grad.GoodID
				INNER JOIN #soft_categories# sc ON gtsc.SoftCategoryID = sc.SoftCategoryID
				INNER JOIN #avail_goods# agd ON (ag.GoodID = agd.GoodID AND agd.CityID = " . $cityId . " )
				INNER JOIN #goods_prices# gp ON (ag.GoodID = gp.GoodID AND gp.ZoneID = $priceZoneId AND gp.CategoryID = $priceCategoryId)
				LEFT OUTER JOIN #brands# b ON ag.BrandID = b.BrandID

				WHERE
					(grad.PickupDate IS NOT NULL) &&
					ag.GoodID IN (" . implode(',', $options->enumGoodIds()) . ")
				";
		if ($options->hasSorting()) {
			$sorting = $options->getSorting();
			if ('agd.AvailQuantity' == $sorting['column']) {
				$sorting['direction'] = 'desc';
			}
			
			$sql .= " ORDER BY ";
			if ('(0 < agd.AvailQuantity)' == $sorting['column']) {
				$sql .= $sorting['column'] . ' ' . $sorting['direction'] . ", (0 < (agd.AvailQuantity - agd.WindowQuantity)) DESC";
			} else {
				$sql .= $sorting['column'] . ' ' . $sorting['direction'];
			}
			if ('(0 < agd.AvailQuantity)' == $sorting['column']) {
				$sql .= " , agd.ArrivalDate ASC, gp.Value ASC";
			}
		}
		if ($options->hasLimitation()) {
			$limit = $options->getLimitation();
			$sql .= " LIMIT " . (($limit['currentPage'] - 1) * $limit['perPage']) . ", " . $limit['perPage'];
		}
		if (isset($_GET['debug_sql'])) {
			print $sql; exit();
		}
		// print $sql; exit();
		$rows = $this->query($sql)->toArray();
		return $rows;
	}

	/**
	 *
	 * @param array $goodIds        	
	 * @return array
	 */
	public function enumWarehouseQuantityByGoodIds(array $goodIds) {
		$sql = "SELECT
					aw.GoodID,
					aw.WarehouseID,
					aw.AvailQuantity,
					ag.SuborderQuantity
				FROM
					#avail_goods# ag
				INNER JOIN
					#avail_warehouses# aw
					ON (ag.GoodID = aw.GoodID)
				WHERE
					aw.GoodID IN (" . implode(',', $goodIds) . ")";
		return $this->query($sql)->toArray();
	}

	/**
	 *
	 * @param int $goodId        	
	 * @return \Zend\Db\ResultSet\Zend\Db\ResultSetInterface
	 */
	public function enumWarehouseQuantityByGoodId($goodId) {
		$sql = "SELECT
					aw.GoodID,
					aw.WarehouseID,
					aw.AvailQuantity,
					ag.SuborderQuantity
				FROM
					#avail_goods# ag
				INNER JOIN
					#avail_warehouses# aw
					ON (ag.GoodID = aw.GoodID)
				WHERE
					aw.GoodID = " . $goodId;
		return $this->query($sql);
	}

	/**
	 *
	 * @param int $goodId        	
	 * @return \Zend\Db\ResultSet\Zend\Db\ResultSetInterface
	 */
	public function enumWarehouseAvailabilityByGoodId($goodId) {
		$sql = "SELECT
					aw.*
				FROM
					#avail_warehouses# aw
				WHERE
					aw.GoodID = " . $goodId;
		return $this->query($sql);
	}

	/**
	 *
	 * @param int $goodId        	
	 * @return \ArrayObject
	 */
	public function getAvailabilityByGoodId($goodId, $cityId) {
		if (MANAGER_MODE) {
			$sql = "SELECT
					ag.*
				FROM
					#avail_manager_goods# ag
				WHERE
					ag.GoodID = " . intval($goodId) . "
					AND ag.CityID = " . $cityId . "
				LIMIT 1";
			$rows = $this->query($sql);
			if (0 < $rows->count()) {
				return $rows->current();
			}
			return null;
		}
		
		$sql = "SELECT
					ag.*
				FROM
					#avail_goods# ag
				WHERE
					ag.GoodID = " . intval($goodId) . "
					AND ag.CityID = " . $cityId . "
				LIMIT 1";
		$rows = $this->query($sql);
		if (0 < $rows->count()) {
			return $rows->current();
		}
		return null;
	}

	/**
	 *
	 * @param array $goodIds        	
	 * @param int $cityId        	
	 * @return array
	 */
	public function getAvailabilityByGoodIds($goodIds, $cityId) {
		if (MANAGER_MODE) {
			if (count($goodIds) === 0) {
				return [];
			}
			$sql = "SELECT
					ag.GoodID,
                    ag.AvailQuantity,
                    ag.SuborderQuantity,
					ag.WindowQuantity,
                    ag.ArrivalDate,
                    0 AS WarehouseId
				FROM
					#avail_manager_goods# ag
				WHERE
					ag.GoodID IN (" . implode(',', $goodIds) . ")
					AND ag.CityID = " . $cityId;
			return $this->query($sql)->toArray();
		}
		
		if (count($goodIds) === 0) {
			return [];
		}
		$sql = "SELECT
					ag.GoodID,
                    ag.AvailQuantity,
                    ag.SuborderQuantity,
					ag.WindowQuantity,
                    ag.ArrivalDate,
                    0 AS WarehouseId
				FROM
					#avail_goods# ag
				WHERE
					ag.GoodID IN (" . implode(',', $goodIds) . ")
					AND ag.CityID = " . $cityId;
		return $this->query($sql)->toArray();
	}

	/**
	 *
	 * @param array $goodIds        	
	 * @param integer|null $cityId        	
	 * @return \Zend\Db\ResultSet\Zend\Db\ResultSetInterface
	 */
	public function enumAvailabilityByGoodIds(array $goodIds, $cityId = null) {
		if (MANAGER_MODE) {
			$sql = "SELECT
					ag.*
				FROM
					#avail_manager_goods# ag
				WHERE
					ag.GoodID IN (" . implode(',', $goodIds) . ") ";
			if (!is_null($cityId)) {
				$sql .= "
                    AND ag.CityID = " . (int)$cityId;
			}
			return $this->query($sql);
		}
		
		$sql = "SELECT
					ag.*
				FROM
					#avail_goods# ag
				WHERE
					ag.GoodID IN (" . implode(',', $goodIds) . ") ";
		if (!is_null($cityId)) {
			$sql .= "
                    AND ag.CityID = " . (int)$cityId;
		}
		return $this->query($sql);
	}

	/**
	 *
	 * @param array $goodIds        	
	 * @return \Zend\Db\ResultSet\Zend\Db\ResultSetInterface
	 */
	public function enumWarehouseAvailabilityByGoodIds(array $goodIds) {
		$sql = "SELECT
					aw.*
				FROM
					#avail_warehouses# aw
				WHERE
					aw.GoodID IN (" . implode(',', $goodIds) . ")";
		return $this->query($sql);
	}

	public function enumWarehouseAvailabilityByGoodIdsAndWarehouseIds($goodIds, $warehouseIds) {
		$sql = "SELECT
					aw.*
				FROM
					#avail_warehouses# aw
				WHERE
					aw.GoodID IN (" . implode(',', $goodIds) . ") AND aw.WarehouseID IN (" . implode(',', $warehouseIds) . ")";
		return $this->query($sql);
	}

	/**
	 *
	 * @param int $limit        	
	 * @return array
	 */
	public function enumNewGoods($cityId, $limit) {
		$sql = "SELECT
					alg.*,
					gp.*
				FROM
					#all_goods# alg
				INNER JOIN
					#avail_goods# ag
					ON (
						alg.GoodID = ag.GoodID
						AND ag.CityID = " . $cityId . "
					)
				INNER JOIN
					#goods_prices@cityId:{$cityId}# gp
					ON (
						alg.GoodID = gp.GoodID
						AND gp.CityID = " . $cityId . "
					)
				INNER JOIN
					pic_found pf
					ON alg.GoodID = pf.good_id
				WHERE
					alg.IsService = 0
					AND alg.HasTakeAndRunAction = 0
					AND alg.ParentGoodID = 0
				ORDER BY
					alg.AddDate DESC, RAND()
				LIMIT " . $limit;
		return $this->query($sql)->toArray();
	}

	/**
	 *
	 * @param int $limit        	
	 * @return array
	 */
	public function enumActionGoods($cityId, $limit) {
		$sql = "SELECT
					alg.*,
					gp.*
				FROM
					#all_goods# alg
				INNER JOIN
					#avail_goods# ag
					ON (
						alg.GoodID = ag.GoodID
						AND ag.CityID = " . $cityId . "
					)
				INNER JOIN
					#goods_prices@cityId:{$cityId}# gp
					ON (
						alg.GoodID = gp.GoodID
						AND gp.CityID = " . $cityId . "
					)
				INNER JOIN
					pic_found pf
					ON alg.GoodID = pf.good_id
				WHERE
					alg.HasTakeAndRunAction = 1
					AND alg.IsService = 0
					AND alg.ParentGoodID = 0
				ORDER BY
					RAND()
				LIMIT " . $limit;
		return $this->query($sql)->toArray();
	}

	/**
	 *
	 * @param array $goodIds        	
	 * @return \Zend\Db\ResultSet\Zend\Db\ResultSetInterface
	 */
	public function enumGoodsByIds(array $goodIds) {
		$sql = "SELECT
                                alg.*,
                                sc.CategoryUID,
								b.BrandName
                            FROM
                                #all_goods:active# alg
                            INNER JOIN
                                #goods_to_soft_categories:active# gtsc
                                ON gtsc.GoodID = alg.GoodID
                            INNER JOIN
                                #soft_categories:active# sc
                                ON sc.SoftCategoryID = gtsc.SoftCategoryID
							LEFT OUTER JOIN #brands# b
								ON alg.BrandID = b.BrandID
                            WHERE
                                  alg.GoodID IN (" . implode(',', $goodIds) . ")
                            GROUP BY
                                alg.GoodID";
		return $this->query($sql);
	}

	/**
	 *
	 * @param int $goodId        	
	 * @return \ArrayObject
	 */
	public function getAvailGoodByGoodId($goodId, $cityId) {
		$sql = "SELECT
					alg.*
				FROM
					#avail_goods# ag
				INNER JOIN
					#all_goods# alg
					ON ag.GoodID = alg.GoodID
				WHERE
					alg.GoodID = " . intval($goodId) . "
					AND ag.CityID = " . $cityId . "
				LIMIT 1";
		$rows = $this->query($sql);
		if (0 < $rows->count()) {
			return $rows->current();
		}
		return null;
	}

	/**
	 * Получаем список свойств выбранного товара
	 *
	 * @param int $goodId
	 *        	идентификатор товара
	 *        	
	 * @return mixed
	 */
	public function getGoodByGoodId($goodId) {
		$sql = 'SELECT
                    alg.*,
                    dr.DiscountReasonName,
                    sc.CategoryUID,
                    sc.SoftCategoryID,
					sc.CategoryName,
                    wpu.WarrantyPeriodUnitForm1,
                    wpu.WarrantyPeriodUnitForm2,
                    wpu.WarrantyPeriodUnitForm3,
                    br.BrandName
                FROM
                    #all_goods# alg
                INNER JOIN
                    #goods_to_soft_categories:active# gtsc
                    ON gtsc.GoodID = alg.GoodID
                INNER JOIN
                    #soft_categories:active# sc
                    ON sc.SoftCategoryID = gtsc.SoftCategoryID
                LEFT JOIN
                    #warranty_period_units# wpu
                    ON wpu.WarrantyPeriodUnitID = alg.WarrantyPeriodUnitID
                LEFT JOIN
                    #discount_reasons# dr
                    ON dr.DiscountReasonID = alg.DiscountReasonID
                LEFT JOIN
                    #brands# br
                    ON br.BrandID = alg.BrandID
                WHERE
                    alg.GoodID = ' . intval($goodId) . '
                LIMIT 1';
		$rows = $this->query($sql);
		
		if (0 < $rows->count()) {
			return $rows->current();
		}
		return null;
	}

    /**
     * Получаем остатки товара у поставщиков
     *
     * @param int $goodId  	идентификатор товара
     * @param int $officeId  	идентификатор базы
     *
     * @return mixed
     */
    public function getSupplierRemainsByGoodId($goodId, $officeId) {
        $sql = 'SELECT sg.Quantity, s.SupplierName,
                  DATE_FORMAT(s.LastPriceUpdated, "%d.%m") as LastPriceUpdated,
                  IF(sg.ArrivalDate = min.ArrivalDate, DATE_FORMAT(sg.ArrivalDate, "%d.%m"), 0) as ArrivalDate,
                  sg.ProductionTerm, sg.ArrivalQuantity
                  FROM #supplier_goods# sg
                    JOIN #suppliers# s ON s.SupplierID = sg.SupplierID
                    JOIN (SELECT GoodID, min(ArrivalDate) as ArrivalDate FROM supplier_goods_2 GROUP BY GoodID) min ON min.GoodID = sg.GoodID
                  WHERE (sg.Quantity > 0 or sg.ProductionTerm > 0) and sg.GoodID = ' . intval($goodId) . ' and sg.OfficeID = ' . intval($officeId);

        $rows = $this->query($sql);

        if (0 < $rows->count()) {
            return $rows->toArray();
        }
        return null;
    }

	public function getAvailGoodWithCategoryUIDByGoodId($goodId, $cityId) {
		$sql = "SELECT
                alg.*,
                                    sc.CategoryUID
            FROM
                #avail_goods# ag
            INNER JOIN
                #all_goods# alg
                ON ag.GoodID = alg.GoodID
                            INNER JOIN
                                    #goods_to_soft_categories:active# gtsc
                                    ON gtsc.GoodID = alg.GoodID
                            INNER JOIN
                                    #soft_categories:active# sc
                                    ON sc.SoftCategoryID = gtsc.SoftCategoryID
            WHERE
                alg.GoodID = " . intval($goodId) . "
                AND ag.CityID = " . $cityId . "
            LIMIT 1";
		$rows = $this->query($sql);
		if (0 < $rows->count()) {
			return $rows->current();
		}
		return null;
	}

	/**
	 *
	 * @see \SoloCatalog\Data\GoodsMapperInterface::getGoodWithCategoryUIDByGoodId()
	 */
	public function getGoodWithCategoryUIDByGoodId($goodId, $cityId) {
		$sql = "SELECT
                alg.*,
                                    sc.CategoryUID
            FROM
                #all_goods# alg
                            LEFT OUTER JOIN
                                    #goods_to_soft_categories:active# gtsc
                                    ON gtsc.GoodID = alg.GoodID
                            LEFT OUTER JOIN
                                    #soft_categories:active# sc
                                    ON sc.SoftCategoryID = gtsc.SoftCategoryID
            WHERE
                alg.GoodID = " . intval($goodId) . "
            LIMIT 1";
		$rows = $this->query($sql);
		if (0 < $rows->count()) {
			return $rows->current();
		}
		return null;
	}

	/**
	 *
	 * @param int $goodId        	
	 * @return int
	 */
	public function getImagesCount($goodId) {
		$sql = "SELECT images_num FROM catalog_info WHERE goods_id = " . $goodId;
		$rows = $this->query($sql);
		if (0 < $rows->count()) {
			return $rows->current()->images_num;
		}
		return 0;
	}

	/**
	 *
	 * @param int $goodId        	
	 * @return \Zend\Db\ResultSet\ResultSet
	 */
	public function getGoodDescription($goodId) {
		$sql = "SELECT
                            pt.SortIndex,
                            gpv.GoodID,
                            gpv.PropertyID,
                            p.PropertyName,
                            p.KindID,
                            p.PropertyTypeID,
                            p.Hint,
                            p.HintImage,
                            pu.UnitName AS Unit,
                            CASE
                              WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) THEN gpv.PropertyValueID
                              WHEN gpv.PropertyValueID > 1 THEN GROUP_CONCAT(gpv.PropertyValueID SEPARATOR ',')
                            END AS PropertyValueID,
                            pt.DisplayInMiddleDescription,
                            pt.DisplayInSmallDescription,
                            pt.IsComparable,
							pt.IsSelectable,
                            CASE
                                    WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueText <> '' THEN gpv.ValueText
                                    WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueString <> '' THEN gpv.ValueString
                                    WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueNumeric IS NOT NULL AND gpv.ValueNumeric > 0 THEN gpv.ValueNumeric
                                    WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueBoolean = 1 THEN 'Да'
                                    #WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueBoolean = 0 THEN 'Нет'
                                    WHEN gpv.PropertyValueID > 1 THEN GROUP_CONCAT(pv.PropertyValue SEPARATOR ',\n')
                            END AS Value,
							pg.SortIndex AS GroupSortIndex
                    FROM
                            #goods_to_propvalues:active# gpv

                    INNER JOIN
                            #all_goods:active# ag
                            ON
                                ag.GoodID = " . $goodId . "

                    INNER JOIN
                            #props:active# p
                            ON
                                p.PropertyID = gpv.PropertyID
                                AND p.KindID IN (1,2)
                                AND p.IsHidden = 0

                    LEFT JOIN
                            #templates_to_categories:active# tc
                            ON
                                tc.CategoryID = ag.CategoryID

                    LEFT JOIN
                            #props_to_templates:active# pt
                            ON
                                pt.PropertyID = gpv.PropertyID
                                AND pt.PropertyTemplateID = tc.PropertyTemplateID
                    LEFT OUTER JOIN #prop_groups# pg
                            ON pg.PropertyGroupID = pt.PropertyGroupID

                    LEFT OUTER JOIN
                            #prop_values:active# pv
                            ON
                                pv.PropertyValueID = gpv.PropertyValueID

                    LEFT JOIN
                            #prop_units:active# pu
                            ON
                                pu.PropertyUnitID = p.PropertyUnitID

                    WHERE
                            gpv.GoodID = " . $goodId . "

                    GROUP BY
                            gpv.GoodID,
                            gpv.PropertyID,
                            p.PropertyName

                    ORDER BY
                            pg.SortIndex ASC,
                            pt.SortIndex ASC";
		
		if (isset($_GET['debug_filter'])) {
			print_r($this->query($sql));exit();
		}
		
		return $this->query($sql)->toArray();
	}

	/**
	 *
	 * @param array $goodIds        	
	 * @return \Zend\Db\ResultSet\ResultSet
	 */
	public function enumGoodDescriptionsByGoodIds(array $goodIds) {
		$sql = "SELECT
                            pt.SortIndex,
                            gpv.GoodID,
                            gpv.PropertyID,
                            p.PropertyName,
                            pu.UnitName AS unit,
                            CASE
                                    WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueText <> '' THEN gpv.ValueText
                                    WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueString <> '' THEN gpv.ValueString
                                    WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueNumeric IS NOT NULL AND gpv.ValueNumeric > 0 THEN gpv.ValueNumeric
                                    WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueBoolean = 1 THEN 'Да'
                                    #WHEN (gpv.PropertyValueID = 1 OR pv.PropertyValueID IS NULL) AND gpv.ValueBoolean = 0 THEN 'Нет'
                                    WHEN gpv.PropertyValueID > 1 THEN GROUP_CONCAT(pv.PropertyValue SEPARATOR ',\n')
                            END AS value
                    FROM
                            #goods_to_propvalues:active# gpv
                    INNER JOIN
                            #all_goods:active# ag
                    ON
                            ag.GoodID = gpv.GoodID

                    INNER JOIN
                            #templates_to_categories:active# tc
                            ON
                                tc.CategoryID = ag.CategoryID

                    INNER JOIN
                            #props:active# p
                            ON
                                p.PropertyID = gpv.PropertyID
                                AND p.KindID IN (1,2)
                                AND p.IsHidden = 0

                    INNER JOIN
                            #props_to_templates:active# pt
                            ON
                                pt.PropertyID = gpv.PropertyID AND pt.PropertyTemplateID = tc.PropertyTemplateID

                    LEFT OUTER JOIN
                            #prop_values:active# pv
                            ON
                                pv.PropertyValueID = gpv.PropertyValueID

                    LEFT JOIN
                            #prop_units:active# pu
                            ON
                                pu.PropertyUnitID = p.PropertyUnitID

                    WHERE
                            gpv.GoodID IN (" . implode(',', $goodIds) . ")

                    GROUP BY
                            gpv.GoodID,
                            gpv.PropertyID,
                            p.PropertyName

                    ORDER BY
                            pt.SortIndex ASC";
		
		return $this->query($sql)->toArray();
	}

	public function enumAvailGoodIdsByGoodIds(array $goodIds, $softCategoryId = null) {
		if (GOD_MODE) {
			$sql = "SELECT
                                ag.GoodID
                        FROM
                                #all_goods# ag";
			if (is_int($softCategoryId)) {
				$sql .= "
                        INNER JOIN
                                #goods_to_soft_categories# gtsc
                                ON gtsc.GoodID = ag.GoodID
                        INNER JOIN
                                #soft_categories# sc
                                ON sc.SoftCategoryID = gtsc.SoftCategoryID
                    ";
			}
			$sql .= "
                        WHERE
                                ag.GoodID IN (" . implode(',', $goodIds) . ")";
			if (is_int($softCategoryId)) {
				$sql .= "
                        AND
                                sc.SoftCategoryID = " . $softCategoryId . "
                    ";
			}
			return $this->query($sql);
		}
		
		if (MANAGER_MODE) {
			$sql = "SELECT
                                ag.GoodID
                        FROM
                                #avail_manager_goods# ag";
			if (is_int($softCategoryId)) {
				$sql .= "
                        INNER JOIN
                                #goods_to_soft_categories# gtsc
                                ON gtsc.GoodID = ag.GoodID
                        INNER JOIN
                                #soft_categories# sc
                                ON sc.SoftCategoryID = gtsc.SoftCategoryID
                    ";
			}
			$sql .= "
                        WHERE
                                ag.GoodID IN (" . implode(',', $goodIds) . ")";
			if (is_int($softCategoryId)) {
				$sql .= "
                        AND
                                sc.SoftCategoryID = " . $softCategoryId . "
                    ";
			}
			return $this->query($sql);
		}
		
		$sql = "SELECT
                                ag.GoodID
                        FROM
                                #avail_goods# ag";
		if (is_int($softCategoryId)) {
			$sql .= "
                        INNER JOIN
                                #goods_to_soft_categories# gtsc
                                ON gtsc.GoodID = ag.GoodID
                        INNER JOIN
                                #soft_categories# sc
                                ON sc.SoftCategoryID = gtsc.SoftCategoryID
                    ";
		}
		$sql .= "
                        WHERE
                                ag.GoodID IN (" . implode(',', $goodIds) . ")";
		if (is_int($softCategoryId)) {
			$sql .= "
                        AND
                                sc.SoftCategoryID = " . $softCategoryId . "
                    ";
		}
		return $this->query($sql);
	}

	public function getStats($goodId) {
		$sql = "SELECT *
				FROM #goods_sales#
				WHERE GoodID = " . $goodId;
		$rows = $this->query($sql);
		if (0 < $rows->count()) {
			return $rows->current();
		}
		return null;
	}

	private function enumHardCategoriesBySoftCategoryId($categoryId) {
		$sql = "SELECT DISTINCT
					ag.CategoryID AS HardCategoryID
				FROM
					#goods_to_soft_categories# gtsc
				INNER JOIN #all_goods# ag
					ON gtsc.GoodID = ag.GoodID
				WHERE
					gtsc.SoftCategoryID = '" . $categoryId . "'";
		$rows = $this->query($sql);
		$ids = ArrayHelper::enumOneColumn($rows, 'HardCategoryID');
		return $ids;
	}

	private function enumGoodIdsByFeatureFilters($filters) {
		return [];
	}

	/**
	 *
	 * @return integer
	 */
	public function getMaxSales() {
		$sql = 'SELECT Max(Quantity) as max FROM #goods_sales#';
		return intval($this->query($sql)->toArray()[0]['max']);
	}

	/**
	 *
	 * @param array $warehouseIds        	
	 * @param array $goodIds        	
	 * @return array
	 */
	public function selectByWarehouseIdsAndGoodIds($warehouseIds, $goodIds) {
		$sql = "
                SELECT aw.GoodID,
                       ag.GoodName,
                       aw.WarehouseID,
                       aw.AvailQuantity
                FROM #avail_warehouses# aw
                JOIN #all_goods# ag ON (aw.GoodID=ag.GoodID)
                WHERE
                        aw.WarehouseID IN (" . implode(', ', $warehouseIds) . ") AND
                        aw.GoodID IN (" . implode(',', $goodIds) . ")
                ORDER BY
                        aw.GoodID ASC
        ";
		return $this->query($sql)->toArray();
	}

	/**
	 *
	 * @param array $goodIds        	
	 */
	public function selectDeliveryQuantities($goodIds) {
		$sql = "select ag.GoodID, ag.GoodName, avg.DeliveryQuantity AvailQuantity FROM #avail_goods# avg RIGHT JOIN #all_goods# ag ON (avg.GoodID=ag.GoodID) WHERE ag.GoodID IN (" . implode(
			',', 
			$goodIds) . ")";
		return $this->query($sql)->toArray();
	}

	/**
	 *
	 * @param integer $goodId        	
	 * @param integer $cityId        	
	 * @return array
	 */
	public function getTotalAvailGoodByOffices($goodId, $cityId) {
		$sql = 'SELECT
                        CASE
                            WHEN avw.GoodID IS NOT NULL THEN avw.GoodID
                            WHEN arg.GoodID IS NOT NULL THEN arg.GoodID
                        END AS GoodID,
                        CASE
                            WHEN SUM(avw.AvailQuantity) IS NOT NULL THEN SUM(avw.AvailQuantity)
                            ELSE 0
                        END AS AvailQuantity,
                        CASE
                            WHEN SUM(avw.MainWarehouseSuborderQuantity) IS NOT NULL THEN SUM(avw.MainWarehouseSuborderQuantity)
                            ELSE 0
                        END AS MainWarehouseSuborderQuantity,
                        of.OfficeID,
                        of.OfficeAddress,
                        of.OfficeShortName,
                        of.OfficePhone,
                        of.OfficeLocationId,
                        CASE
                            WHEN SUM(arg.SuborderQuantity) IS NOT NULL THEN SUM(arg.SuborderQuantity)
                            ELSE 0
                        END AS SuborderQuantity,
                        grad.PickupDate AS ArrivalDate,
                        grad.SuborderDeliveryDate AS SuborderDate,
                        arg.IsTwin
                FROM
                        #offices:active# of
                LEFT OUTER JOIN
                        #avail_warehouses:active# avw
                        ON avw.WarehouseID = of.OfficeID
                        AND avw.GoodID = ' . $goodId . '
                LEFT OUTER JOIN
                        #arrival_goods:active# arg
                        ON arg.OfficeID = of.OfficeID
                        AND arg.GoodID = ' . $goodId . '
                LEFT OUTER JOIN
                        #good_reserve_avail_date:active# grad
                        ON grad.GoodID = ' . $goodId . '
                        AND grad.CityID = ' . $cityId . '
                WHERE
                        of.OfficeLocationId = ' . $cityId . '
                        AND of.Hide = 0
                        AND of.IsPickup = 1
                GROUP BY
                        of.OfficeID';
		return $this->query($sql)->toArray();
	}

	/**
	 *
	 * @param array $goodIds        	
	 * @param integer $cityId        	
	 * @return array
	 */
	public function getAvailByOffices($goodIds, $cityId) {
		$sql = 'SELECT
                        avw.GoodID,
                        avw.AvailQuantity,
                        avw.MainWarehouseSuborderQuantity,
                        grad.PickupDate AS ArrivalDate,
                        st.StoreID,
                        of.OfficeID,
                        of.OfficePhone,
                        of.OfficeAddress,
                        of.OfficeShortName
                FROM
                        #offices:active# of
                INNER JOIN
                        #stores:active# st
                        ON st.StoreOfficeID = of.OfficeID
                INNER JOIN
                        #avail_warehouses:active# avw
                        ON avw.WarehouseID = of.OfficeID
                        AND avw.GoodID IN (' . implode(',', $goodIds) . ')
                INNER JOIN
                        #good_reserve_avail_date:active# grad
                        ON grad.GoodID = avw.GoodID
                        AND grad.CityID = of.OfficeLocationId
                WHERE
                        of.OfficeLocationId = ' . $cityId . '
                        AND of.Hide = 0';
		return $this->query($sql)->toArray();
	}

	/**
	 *
	 * @param array $goodIds        	
	 * @param integer $cityId        	
	 * @return array
	 */
	public function getSuborderByOffices($goodIds, $cityId) {
		$sql = 'SELECT
                        arg.GoodID,
                        of.OfficeID,
                        of.OfficeAddress,
                        of.OfficeShortName,
                        of.OfficePhone,
                        arg.SuborderQuantity,
                        arg.IsTwin,
                        grad.PickupDate AS ArrivalDate
                FROM
                        #arrival_goods:active# arg
                INNER JOIN
                        #offices:active# of
                        ON of.OfficeID = arg.OfficeID
                        AND of.Hide = 0
                INNER JOIN
                        #good_reserve_avail_date# grad
                        ON grad.GoodID = arg.GoodID
                        AND grad.CityID = of.OfficeLocationId
                WHERE
                        of.OfficeLocationId = ' . $cityId . '
                        AND of.IsPickup = 1
                        AND arg.GoodID IN (' . implode(',', $goodIds) . ')';
		return $this->query($sql)->toArray();
	}

	/**
	 *
	 * @param integer $goodId        	
	 * @param
	 *        	integer cityId
	 * @return array
	 */
	public function getDiscountGoods($goodId, $cityId) {
		$sql = 'SELECT
                        alg.GoodID,
                        alg.GoodEngName,
                        sc.CategoryUID,
                        gp.Value AS Price
                FROM
                        #all_goods# alg
                INNER JOIN
                        #goods_to_soft_categories:active# gtsc
                        ON gtsc.GoodID = alg.GoodID
                INNER JOIN
                        #soft_categories:active# sc
                        ON sc.SoftCategoryID = gtsc.SoftCategoryID
                INNER JOIN
                        #goods_prices:active# gp
                        ON gp.GoodID = alg.GoodID
                        AND gp.CategoryID = 1
                INNER JOIN
                        #cities:active# cit
                        ON cit.CityZoneID = gp.ZoneID
                        AND cit.CityID = ' . $cityId . '
                INNER JOIN
                        #avail_goods:active# ag
                        ON ag.GoodID = alg.GoodID
                WHERE
                        alg.OriginalGoodID = ' . $goodId . '
                        AND gp.Value > 0';
		return $this->query($sql)->toArray();
	}

	/**
	 *
	 * @param integer $goodId        	
	 * @param
	 *        	integer cityId
	 * @return array
	 */
	public function getOriginalGood($goodId, $cityId) {
		$sql = 'SELECT
                        alg.GoodID,
                        alg.GoodEngName,
                        sc.CategoryUID,
                        gp.Value AS Price
                FROM
                        #all_goods# alg
                INNER JOIN
                        #goods_to_soft_categories:active# gtsc
                        ON gtsc.GoodID = alg.GoodID
                INNER JOIN
                        #soft_categories:active# sc
                        ON sc.SoftCategoryID = gtsc.SoftCategoryID
                INNER JOIN
                        #goods_prices:active# gp
                        ON gp.GoodID = alg.GoodID
                        AND gp.CategoryID = 1
                INNER JOIN
                        #cities:active# cit
                        ON cit.CityZoneID = gp.ZoneID
                        AND cit.CityID = ' . $cityId . '
                INNER JOIN
                        #avail_goods:active# ag
                        ON ag.GoodID = alg.GoodID
                WHERE
                        alg.GoodID = ' . $goodId . '
                        AND gp.Value > 0
                LIMIT 1';
		$rows = $this->query($sql);
		if (0 < $rows->count()) {
			return $rows->current();
		}
		return null;
	}

	/**
	 *
	 * @param integer $cityId        	
	 * @return array
	 */
	public function getDiscountGoodsByCityId($cityId) {
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if ($cacher->has()) return $cacher->get();
		$sql = "SELECT
					ag.GoodID
				FROM
					#avail_goods# ag
                                INNER JOIN
                                        #all_goods# alg
                                        ON ag.GoodID = alg.GoodID
                                INNER JOIN
                                        #goods_to_soft_categories# gtsc
                                        ON gtsc.GoodID = alg.GoodID
                                WHERE
                                        alg.OriginalGoodID>0 AND ag.CityID=" . $cityId;
		$result = $this->query($sql)->toArray();
		$cacher->set($result);
		return $result;
	}

	/**
	 *
	 * @param array $goodIds        	
	 * @param integer $categoryId        	
	 * @return array
	 */
	public function getGoodIdsInSoftCategoryId($goodIds, $categoryId) {
		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if ($cacher->has()) return $cacher->get();
		$sql = 'SELECT DISTINCT
                            ag.GoodID
                    FROM
                            #all_goods:active# ag
                    INNER JOIN
                            #goods_to_soft_categories:active# gtsc
                            ON gtsc.GoodID = ag.GoodID
                    WHERE
                            ag.GoodID IN (' . implode(',', $goodIds) . ')
                            AND gtsc.SoftCategoryID = ' . $categoryId;
		$result = $this->query($sql)->toArray();
		$cacher->set($result);
		return $result;
	}

	/**
	 *
	 * @param integer $cityId        	
	 * @return integer
	 */
	public function getRandomAvailGood($cityId) {
		$sql = 'SELECT
                            avg.GoodID
                    FROM
                            #avail_goods:active# avg
                    INNER JOIN
                            #all_goods:active# ag
                            ON ag.GoodID = avg.GoodID
                    INNER JOIN
                            #goods_to_soft_categories:active# gtsc
                            ON gtsc.GoodID = ag.GoodID
                    INNER JOIN
                            #soft_categories:active# sc
                            ON sc.SoftCategoryID = gtsc.SoftCategoryID
                    INNER JOIN
                            #cities:active# cit
                            ON cit.CityID = ' . $cityId . '
                    INNER JOIN
                            #goods_prices:active# gp
                            ON gp.GoodID = avg.GoodID
                            AND gp.CategoryID = 1
                            AND gp.Value > 0
                            AND gp.ZoneID = cit.CityZoneID
                    WHERE
                            avg.CityID = ' . $cityId . '
                            AND avg.AvailQuantity > 0
                    ORDER BY
                            RAND()
                    LIMIT 1';
		$rows = $this->query($sql);
		if (0 < $rows->count()) {
			return (int)$rows->current()->GoodID;
		}
		return null;
	}

	/**
	 *
	 * @param integer $cityId        	
	 * @return integer
	 */
	public function getRandomSuborderGood($cityId) {
		$sql = 'SELECT
                            avg.GoodID
                    FROM
                            #avail_goods:active# avg
                    INNER JOIN
                            #all_goods:active# ag
                            ON ag.GoodID = avg.GoodID
                    INNER JOIN
                            #goods_to_soft_categories:active# gtsc
                            ON gtsc.GoodID = ag.GoodID
                    INNER JOIN
                            #soft_categories:active# sc
                            ON sc.SoftCategoryID = gtsc.SoftCategoryID
                    INNER JOIN
                            #cities:active# cit
                            ON cit.CityID = ' . $cityId . '
                    INNER JOIN
                            #goods_prices:active# gp
                            ON gp.GoodID = avg.GoodID
                            AND gp.CategoryID = 1
                            AND gp.Value > 0
                            AND gp.ZoneID = cit.CityZoneID
                    WHERE
                            avg.CityID = ' . $cityId . '
                            AND avg.AvailQuantity = 0
                            AND avg.SuborderQuantity > 0
                    ORDER BY
                            RAND()
                    LIMIT 1';
		$rows = $this->query($sql);
		if (0 < $rows->count()) {
			return (int)$rows->current()->GoodID;
		}
		return null;
	}

	/**
	 *
	 * @param int $goodId        	
	 * @param
	 *        	$cityId
	 * @param int $priceCategoryId        	
	 * @param int $limit        	
	 * @return array \Zend\Db\ResultSet\ResultSet
	 */
	public function enumSimilarGoods($goodId, $cityId, $priceCategoryId, $limit = 12) {
		$good = $this->getAvailGoodById($goodId, $cityId);
		
		if (null == $good) {
			return [];
		}
		$sql = "SELECT DISTINCT
					alg.GoodID,
					alg.GoodName,
					alg.GoodEngName,
					sc.CategoryUID,
					sc.CategoryName,
					ag.AvailQuantity,
					ag.ArrivalDate,
					gp.Value,
					gp.PrevValue,
					grad.PickupDate,
					grad.DeliveryDate,
					grad.ShowcaseRetrieveDate,
					b.BrandName
				FROM
					#all_goods# alg
				LEFT OUTER JOIN #good_reserve_avail_date# grad ON alg.GoodID = grad.GoodID
				INNER JOIN
					#".(MANAGER_MODE ? 'avail_manager_goods' : 'avail_goods')."# ag
					ON (
						alg.GoodID = ag.GoodID
						AND ag.CityID = " . $cityId . "
					)
				INNER JOIN
					#goods_prices# gp
					ON gp.GoodID = alg.GoodID AND gp.CategoryID = " . $priceCategoryId . "
				INNER JOIN
                    #locations:active# loc
                    ON loc.LocationZoneID = gp.ZoneID AND loc.LocationID = " . $cityId . "
                INNER JOIN
                    #goods_to_soft_categories:active# gtsc
                    ON gtsc.GoodID = alg.GoodID
                INNER JOIN
                    #soft_categories:active# sc
                    ON sc.SoftCategoryID = gtsc.SoftCategoryID
                LEFT OUTER JOIN #brands# b
                    ON alg.BrandID = b.BrandID
				WHERE
					alg.GoodID <> " . $good->GoodID . "
					AND alg.CategoryID = " . $good->HardCategoryID . " AND
					alg.SerieID = (SELECT SerieID FROM #all_goods# WHERE GoodID = " . $goodId . ")
				ORDER BY
					alg.AddDate DESC
				LIMIT " . $limit;
		
		$result = $this->query($sql);

		if (isset($_GET['debug_similar'])) {
			print_r($result); exit();
		}
		
		return $result;
	}

	public function enumSimilarGoodIds($goodId, $cityId, $priceCategoryId, $limit = 12) {
		$good = $this->getAvailGoodById($goodId, $cityId);
		
		if (null == $good) {
			return [];
		}

		$sql = "SELECT DISTINCT
					alg.GoodID
				FROM
					#all_goods# alg
				INNER JOIN
					#".(MANAGER_MODE ? 'avail_manager_goods' : 'avail_goods')."# ag
					ON (
						alg.GoodID = ag.GoodID
						AND ag.CityID = " . $cityId . "
					)
				INNER JOIN
					#goods_prices# gp
					ON gp.GoodID = alg.GoodID AND gp.CategoryID = " . $priceCategoryId . "
				INNER JOIN
                    #locations:active# loc
                    ON loc.LocationZoneID = gp.ZoneID AND loc.LocationID = " . $cityId . "
                INNER JOIN
                    #goods_to_soft_categories:active# gtsc
                    ON gtsc.GoodID = alg.GoodID
                INNER JOIN
                    #soft_categories:active# sc
                    ON sc.SoftCategoryID = gtsc.SoftCategoryID
				WHERE
					alg.GoodID <> " . $good->GoodID . "
					AND alg.CategoryID = " . $good->HardCategoryID . " AND
					alg.SerieID = (SELECT SerieID FROM #all_goods# WHERE GoodID = " . $goodId . ")
				ORDER BY
					alg.AddDate DESC
				LIMIT " . $limit;
		
		$result = $this->query($sql);
		$ids = ArrayHelper::enumOneColumn($result, 'GoodID');
		return $ids;
	}

	public function enumNotSimilarGoods($goodId, $cityId, $priceCategoryId, $limit = 12) {
		$good = $this->getAvailGoodById($goodId, $cityId);
		
		if (null == $good) {
			return [];
		}
		$sql = "SELECT DISTINCT
					alg.GoodID,
					alg.GoodName,
					alg.GoodEngName,
					sc.CategoryUID,
					sc.CategoryName,
					ag.AvailQuantity,
					ag.ArrivalDate,
					gp.Value,
					gp.PrevValue,
					grad.PickupDate,
					grad.DeliveryDate,
					grad.ShowcaseRetrieveDate,
					b.BrandName
				FROM
					#all_goods# alg
				LEFT OUTER JOIN #good_reserve_avail_date# grad ON alg.GoodID = grad.GoodID
				INNER JOIN
					#avail_goods# ag
					ON (
						alg.GoodID = ag.GoodID
						AND ag.CityID = " . $cityId . "
					)
				INNER JOIN
					#goods_prices# gp
					ON gp.GoodID = alg.GoodID AND gp.CategoryID = " . $priceCategoryId . "
				INNER JOIN
                    #locations:active# loc
                    ON loc.LocationZoneID = gp.ZoneID AND loc.LocationID = " . $cityId . "
                INNER JOIN
                    #goods_to_soft_categories:active# gtsc
                    ON gtsc.GoodID = alg.GoodID
                INNER JOIN
                    #soft_categories:active# sc
                    ON sc.SoftCategoryID = gtsc.SoftCategoryID
                LEFT OUTER JOIN #brands# b
                   	ON alg.BrandID = b.BrandID
				WHERE
					alg.GoodID <> " . $good->GoodID . "
					AND alg.CategoryID = " . $good->HardCategoryID . " AND
					(alg.SerieID <> (SELECT SerieID FROM #all_goods# WHERE GoodID = " . $goodId . ") OR alg.SerieID IS NULL)
				ORDER BY
					ag.ArrivalDate ASC
				LIMIT " . $limit;
		// print_r($this->query($sql)); exit();
		
		$result = $this->query($sql);
		
		return $result;
	}

	public function enumNotSimilarGoodIds($goodId, $cityId, $priceCategoryId, $limit = 12) {
		$good = $this->getAvailGoodById($goodId, $cityId);
		
		if (null == $good) {
			return [];
		}
		$sql = "SELECT DISTINCT
					alg.GoodID
				FROM
					#all_goods# alg
				INNER JOIN
					#avail_goods# ag
					ON (
						alg.GoodID = ag.GoodID
						AND ag.CityID = " . $cityId . "
					)
				INNER JOIN
					#goods_prices# gp
					ON gp.GoodID = alg.GoodID AND gp.CategoryID = " . $priceCategoryId . "
				INNER JOIN
                    #locations:active# loc
                    ON loc.LocationZoneID = gp.ZoneID AND loc.LocationID = " . $cityId . "
                INNER JOIN
                    #goods_to_soft_categories:active# gtsc
                    ON gtsc.GoodID = alg.GoodID
                INNER JOIN
                    #soft_categories:active# sc
                    ON sc.SoftCategoryID = gtsc.SoftCategoryID
				WHERE
					alg.GoodID <> " . $good->GoodID . "
					AND alg.CategoryID = " . $good->HardCategoryID . " AND
					(alg.SerieID <> (SELECT SerieID FROM #all_goods# WHERE GoodID = " . $goodId . ") OR alg.SerieID IS NULL)
				ORDER BY
					alg.AddDate DESC
				LIMIT " . $limit;
		
		$result = $this->query($sql);
		$ids = ArrayHelper::enumOneColumn($result, 'GoodID');
		return $ids;
	}

	/**
	 *
	 * @param
	 *        	$goodId
	 * @param
	 *        	$cityId
	 * @return array \ArrayObject null \Zend\Cache\Storage\mixed
	 */
	private function getAvailGoodById($goodId, $cityId) {
		if (MANAGER_MODE) {
			$sql = "SELECT
					ag.*
				FROM
					#avail_manager_goods# ag
				WHERE
					ag.GoodID = " . $goodId . "
					AND ag.CityID = " . $cityId . "
				LIMIT 1";
				$rows = $this->query($sql);
				if (0 < $rows->count()) {
					$result = $rows->current();
					return $result;
				}
				return null;
		}

		$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
		if ($cacher->has()) return $cacher->get();
		
		$sql = "SELECT
					ag.*
				FROM
					#avail_goods# ag
				WHERE
					ag.GoodID = " . $goodId . "
					AND ag.CityID = " . $cityId . "
				LIMIT 1";
		$rows = $this->query($sql);
		if (0 < $rows->count()) {
			$result = $rows->current();
			$cacher->set($result);
			return $result;
		}
		return null;
	}

	/**
	 *
	 * @param
	 *        	$goodId
	 * @return \Zend\Db\ResultSet\ResultSet
	 */
	public function getPropertiesValuesByGoodId($goodId) {
		$sql = "SELECT
                    pv.PropertyID, pv.PropertyValueID, pv.PropertyValue
                FROM
                    #prop_values# pv
                INNER JOIN
                    #goods_to_propvalues# gtpv ON pv.PropertyID = gtpv.PropertyID AND gtpv.GoodID = " . $goodId . "
               ";
		
		return $this->query($sql);
	}

	/**
	 *
	 * @param
	 *        	$goodId
	 * @return \Zend\Db\ResultSet\ResultSet
	 */
	public function getIdenticalGoodIds($goodId) {
		$sql = "SELECT
                    ag.GoodID
                FROM
                    #all_goods# ag
                INNER JOIN
                    #avail_goods# agd ON agd.GoodID = ag.GoodID
                WHERE
                    ag.SerieID = (SELECT SerieID FROM #all_goods# WHERE GoodID = " . $goodId . ")
                    AND ag.CategoryID = (SELECT CategoryID FROM #all_goods# WHERE GoodID = " . $goodId . ")
                    AND ag.GoodID <> " . $goodId;
		
		return $this->query($sql);
	}

	public function getPackageComponents($packageId, $cityId, $priceCategoryId) {
		if (MANAGER_MODE) {
			$sql = "SELECT DISTINCT
                    alg.GoodID,
                    alg.GoodName,
                    alg.GoodEngName,
                    gtp.GroupID,
                    gtp.IsDefault,
                    ag.AvailQuantity,
					ag.WindowQuantity,
					ag.ArrivalDate,
					sc.CategoryUID,
        			alg.IsPackage,
        			alg.IsVirtualPackage,
        			alg.ViewMode,
					gr.PlaceRow,
					gr.PlaceCell,
					gr.PlaceRack,
					grad.PickupDate,
					grad.DeliveryDate,
					grad.ShowcaseRetrieveDate
                FROM
                    #all_goods# alg
				LEFT OUTER JOIN #goods_remains# gr ON alg.GoodID = gr.GoodID
				LEFT OUTER JOIN #good_reserve_avail_date# grad ON alg.GoodID = grad.GoodID
                INNER JOIN
                    #avail_manager_goods# ag
                ON (
                    ag.GoodID = alg.GoodID
                    AND ag.CityID = " . $cityId . "
                )
                INNER JOIN
					#goods_prices# gp
				ON
				    gp.GoodID = alg.GoodID AND gp.CategoryID = " . $priceCategoryId . "
				INNER JOIN
                    #locations:active# loc
                    ON loc.LocationZoneID = gp.ZoneID AND loc.LocationID = " . $cityId . "
                INNER JOIN
                    #goods_to_soft_categories# gtsc
                ON
                    gtsc.GoodID = alg.GoodID
                INNER JOIN
                    #soft_categories# sc
                ON
                    sc.SoftCategoryID = gtsc.SoftCategoryID
                INNER JOIN
                    #goods_to_packages# gtp
                ON
                    gtp.GoodID = alg.GoodID
                WHERE
                    PackageID=" . $packageId . "
                ORDER BY
                   	gtp.SortIndex ASC";
			
			if (isset($_GET['test'])) {
				// print_r($this->query($sql)); exit();
			}
			
			return $this->query($sql);
		}
		
		$sql = "SELECT DISTINCT
                    alg.GoodID,
                    alg.GoodName,
                    alg.GoodEngName,
                    gtp.GroupID,
                    gtp.IsDefault,
                    ag.AvailQuantity,
					ag.WindowQuantity,
					ag.ArrivalDate,
					sc.CategoryUID,
        			alg.IsPackage,
        			alg.IsVirtualPackage,
        			alg.ViewMode,
					gr.PlaceRow,
					gr.PlaceCell,
					gr.PlaceRack,
					grad.PickupDate,
					grad.DeliveryDate,
					grad.ShowcaseRetrieveDate
                FROM
                    #all_goods# alg
				LEFT OUTER JOIN #goods_remains# gr ON alg.GoodID = gr.GoodID
				LEFT OUTER JOIN #good_reserve_avail_date# grad ON alg.GoodID = grad.GoodID
                INNER JOIN
                    #avail_goods# ag
                ON (
                    ag.GoodID = alg.GoodID
                    AND ag.CityID = " . $cityId . "
                )
                INNER JOIN
					#goods_prices# gp
				ON
				    gp.GoodID = alg.GoodID AND gp.CategoryID = " . $priceCategoryId . "
				INNER JOIN
                    #locations:active# loc
                    ON loc.LocationZoneID = gp.ZoneID AND loc.LocationID = " . $cityId . "
                INNER JOIN
                    #goods_to_soft_categories# gtsc
                ON
                    gtsc.GoodID = alg.GoodID
                INNER JOIN
                    #soft_categories# sc
                ON
                    sc.SoftCategoryID = gtsc.SoftCategoryID
                INNER JOIN
                    #goods_to_packages# gtp
                ON
                    gtp.GoodID = alg.GoodID
                WHERE
                    PackageID=" . $packageId . "
                ORDER BY
                   	gtp.SortIndex ASC";
		
		if (isset($_GET['test'])) {
			// print_r($this->query($sql)); exit();
		}
		
		return $this->query($sql);
	}

	public function getPackageComponentsIds($packageId) {
		$sql = "SELECT
                    gtp.GoodID
                FROM
                    #goods_to_packages# gtp
                WHERE
                    PackageID=" . $packageId;
		
		return $this->query($sql);
	}

	public function getPackageDefaultComponentsIds($packageId) {
		$sql = "SELECT DISTINCT
                    gtp.GoodID
                FROM
                    #goods_to_packages# gtp
    				INNER JOIN #avail_goods# ag
    					ON gtp.GoodID = ag.GoodID
                WHERE
                    PackageID = " . $packageId . " AND IsDefault = 1";
		return $this->query($sql);
	}

	public function getPackageGroups($packageId) {
		$sql = "SELECT
                  ptg.GroupID,
                  ptg.IsMandatory,
                  ptg.PickingMethodID,
        			ptg.IsMain,
        			ptg.SortIndex
                FROM
                  #packages_to_groups# ptg
                WHERE
                  PackageID=" . $packageId . "
                ORDER BY ptg.SortIndex ASC";
		
		return $this->query($sql);
	}

	public function getAnotherPackages($packageId, $cityId, $priceCategoryId) {
		$sql = "SELECT
                  	alg.GoodID,
					alg.GoodName,
					alg.GoodEngName,
					alg.CategoryID,
					ag.AvailQuantity,
					ag.WindowQuantity,
					ag.ArrivalDate,
					gp.Value,
					gp.PrevValue,
					sc.CategoryUID,
					sc.CategoryName,
					grad.PickupDate,
					grad.DeliveryDate,
					grad.ShowcaseRetrieveDate,
					b.BrandName
                FROM
                    #all_goods# alg
				LEFT OUTER JOIN #good_reserve_avail_date# grad ON alg.GoodID = grad.GoodID
               	INNER JOIN
                    #avail_goods# ag
                    ON (
                        ag.GoodID = alg.GoodID
                        AND ag.CityID = " . $cityId . "
                    )
				INNER JOIN
					#goods_prices# gp
				ON
				    gp.GoodID = alg.GoodID AND gp.CategoryID = " . $priceCategoryId . "
				INNER JOIN
                    #locations:active# loc
                    ON loc.LocationZoneID = gp.ZoneID AND loc.LocationID = " . $cityId . "
                INNER JOIN
                    #goods_to_soft_categories# gtsc
                ON
                    gtsc.GoodID = alg.GoodID
                INNER JOIN
                    #soft_categories# sc
                ON
                    sc.SoftCategoryID = gtsc.SoftCategoryID
                LEFT OUTER JOIN #brands# b
                	ON alg.BrandID = b.BrandID
                WHERE
                    alg.GoodID <> {$packageId} AND
                    alg.CategoryID = (SELECT CategoryID FROM #all_goods# alg2 WHERE alg2.GoodID={$packageId}) AND
                    alg.SerieID = (SELECT SerieID FROM #all_goods# alg2 WHERE alg2.GoodID={$packageId}) AND
                    alg.IsPackage = 1 AND alg.IsVirtualPackage=0
        ";
		
		return $this->query($sql);
	}

	public function getPackagesByComponentId($componentId, $cityId, $priceCategoryId) {
		$sql = "SELECT
                  	alg.GoodID,
					alg.GoodName,
					alg.GoodEngName,
					sc.CategoryUID,
					ag.AvailQuantity,
					ag.ArrivalDate,
					gp.Value,
					gp.PrevValue
                FROM
                    #goods_to_packages# gtp
                INNER JOIN
                    #all_goods# alg
                ON
                    gtp.PackageID = alg.GoodID
               	INNER JOIN
                    #avail_goods# ag
                    ON (
                        ag.GoodID = alg.GoodID
                        AND ag.CityID = " . $cityId . "
                    )
				INNER JOIN
					#goods_prices# gp
				ON
				    gp.GoodID = alg.GoodID AND gp.CategoryID = " . $priceCategoryId . "
				INNER JOIN
                    #locations:active# loc
                    ON loc.LocationZoneID = gp.ZoneID AND loc.LocationID = " . $cityId . "
                INNER JOIN
                    #goods_to_soft_categories# gtsc
                ON
                    gtsc.GoodID = alg.GoodID
                INNER JOIN
                    #soft_categories# sc
                ON
                    sc.SoftCategoryID = gtsc.SoftCategoryID

                WHERE
                    gtp.GoodID = {$componentId}
                GROUP BY alg.GoodID
        ";
		
		return $this->query($sql);
	}

	public function getAvailableGoodsByCategoryId($categoryId, $cityId, $priceCategoryId) {
		if (MANAGER_MODE) {
			$sql = "SELECT
                  	alg.GoodID,
					alg.GoodName,
					alg.GoodEngName,
					sc.CategoryUID,
					sc.CategoryName,
					ag.AvailQuantity,
					ag.ArrivalDate,
					gp.Value,
					gp.PrevValue,
					gr.PlaceRow,
					gr.PlaceCell,
					gr.PlaceRack,
					grad.PickupDate,
					grad.DeliveryDate,
					grad.ShowcaseRetrieveDate,
					b.BrandName
                FROM
                    #all_goods# alg
               	INNER JOIN
                    #avail_goods# ag
                    ON (
                        ag.GoodID = alg.GoodID
                        AND ag.CityID = " . $cityId . "
                    )
				INNER JOIN
					#goods_prices# gp
				ON
				    gp.GoodID = alg.GoodID AND gp.CategoryID = " . $priceCategoryId . " AND gp.Value < gp.PrevValue
				INNER JOIN
                    #locations:active# loc
                    ON loc.LocationZoneID = gp.ZoneID AND loc.LocationID = " . $cityId . "
			                    INNER JOIN
			                    #goods_to_soft_categories# gtsc
			                    ON
			                    gtsc.GoodID = alg.GoodID
			                    INNER JOIN
			                    #soft_categories# sc
			                    ON
			                    sc.SoftCategoryID = gtsc.SoftCategoryID
			                    LEFT OUTER JOIN #good_reserve_avail_date# grad ON alg.GoodID = grad.GoodID
			                    LEFT OUTER JOIN #brands# b
			                    ON ag.BrandID = b.BrandID
			                    JOIN #goods_remains# gr
			                    ON alg.GoodID = gr.GoodID
			                    WHERE
			                    sc.SoftCategoryID  = $categoryId
			                    ORDER BY
			                    (gp.PrevValue - gp.Value) DESC,
			                    alg.AddDate DESC
			                    LIMIT 9";
			
			return $this->query($sql);
		}
		
		$sql = "SELECT
                  	alg.GoodID,
					alg.GoodName,
					alg.GoodEngName,
					sc.CategoryUID,
					sc.CategoryName,
					ag.AvailQuantity,
					ag.ArrivalDate,
					gp.Value,
					gp.PrevValue,
					gr.PlaceRow,
					gr.PlaceCell,
					gr.PlaceRack,
					grad.PickupDate,
					grad.DeliveryDate,
					grad.ShowcaseRetrieveDate,
					b.BrandName
                FROM
                    #all_goods# alg
               	INNER JOIN
                    #avail_goods# ag
                    ON (
                        ag.GoodID = alg.GoodID
                        AND ag.CityID = " . $cityId . "
                    )
                    AND ((ag.AvailQuantity + ag.SuborderQuantity) > ag.WindowQuantity)
				INNER JOIN
					#goods_prices# gp
				ON
				    gp.GoodID = alg.GoodID AND gp.CategoryID = " . $priceCategoryId . " AND gp.Value < gp.PrevValue
				INNER JOIN
                    #locations:active# loc
                    ON loc.LocationZoneID = gp.ZoneID AND loc.LocationID = " . $cityId . "
                INNER JOIN
                    #goods_to_soft_categories# gtsc
                ON
                    gtsc.GoodID = alg.GoodID
                INNER JOIN
                    #soft_categories# sc
                ON
                    sc.SoftCategoryID = gtsc.SoftCategoryID
				LEFT OUTER JOIN #good_reserve_avail_date# grad ON alg.GoodID = grad.GoodID
                LEFT OUTER JOIN #brands# b
                	ON ag.BrandID = b.BrandID
				JOIN #goods_remains# gr
					ON alg.GoodID = gr.GoodID
                WHERE
                    sc.SoftCategoryID  = $categoryId
                ORDER BY
                	(gp.PrevValue - gp.Value) DESC,
					alg.AddDate DESC
                LIMIT 9";
		
		return $this->query($sql);
	}

	public function getGoodsRemainsByStoreId($goodIds, $storeId) {
		$sql = "SELECT
                    gr.GoodID, gr.Remains
                FROM
                    #goods_remains# gr
                WHERE
                    gr.StoreID = {$storeId} and gr.GoodID IN (" . join(',', $goodIds) . ")";
		
		return $this->query($sql);
	}

    public function getStoresRemainsByGoodId($goodId) {
        $sql = 'SELECT
                    gr.*, (gr.Remains - gr.Reserve) as Avail
                FROM
                    #goods_remains# gr
                WHERE
                    gr.GoodID = ' . $goodId . ' and gr.Remains - gr.Reserve > 0';

        return $this->query($sql)->toArray();
    }

}

?>