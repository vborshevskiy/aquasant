<?php

namespace SoloCatalog\Data;

interface GoodsRemainsMapperInterface {

	/**	
	 * @param integer $storeId        	
	 * @return int
	 */
	public function getLocationIdByStoreId($storeId);	
        
        public function getLocations();
        
        public function getAllGoodsWithRemains($cityId);

}

?>