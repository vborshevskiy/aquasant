<?php

namespace SoloCatalog\Data;

interface ArrivalGoodsMapperInterface {

	/**
	 *
	 * @return \Zend\Db\ResultSet\Zend\Db\ResultSet
	 */
	public function getSuborderQuantities();

}

?>