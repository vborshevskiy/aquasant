<?php

namespace SoloCatalog\Data;

use Solo\Db\Mapper\AbstractMapper;

class SatelliteGoodsMapper extends AbstractMapper implements SatelliteGoodsMapperInterface {

	/**
	 * (non-PHPdoc)
	 *
	 * @see \SoloCatalog\Data\SatelliteGoodsMapperInterface::enumSatelliteGoods()
	 */
	public function enumSatelliteGoods($goodId, $cityId, $priceCategoryId) {
		$sql = "SELECT DISTINCT
					alg.GoodID,
					alg.GoodName,
					alg.GoodEngName,
					sc.CategoryUID,
					ag.AvailQuantity,
					ag.WindowQuantity,
					ag.ArrivalDate,
					gp.Value,
					gp.PrevValue,
					sgg.GroupID,
					sgg.GroupName,
					gr.PlaceRow,
					gr.PlaceCell,
					gr.PlaceRack,
					grad.PickupDate,
					grad.DeliveryDate,
					grad.ShowcaseRetrieveDate
				FROM #satellite_goods# sg
				LEFT OUTER JOIN #goods_remains# gr ON sg.SatelliteID = gr.GoodID
				LEFT OUTER JOIN #good_reserve_avail_date# grad ON sg.SatelliteID = grad.GoodID
				INNER JOIN #satellite_groups# sgg
					ON sg.GroupID = sgg.GroupID
				INNER JOIN
					#all_goods# alg
					ON sg.SatelliteID = alg.GoodID
				INNER JOIN
					#avail_goods# ag
					ON (
						alg.GoodID = ag.GoodID
						AND ag.CityID = " . $cityId . "
					)
				INNER JOIN
					#goods_prices# gp
					ON gp.GoodID = alg.GoodID AND gp.CategoryID = " . $priceCategoryId . " ";
		if (false) {
			$sql .= "
					INNER JOIN
	                    #locations:active# loc
	                    ON loc.LocationZoneID = gp.ZoneID AND loc.LocationID = " . $cityId . " ";
		}
		$sql .= "
                INNER JOIN
                    #goods_to_soft_categories:active# gtsc
                    ON gtsc.GoodID = alg.GoodID
                INNER JOIN
                    #soft_categories:active# sc
                    ON sc.SoftCategoryID = gtsc.SoftCategoryID
				WHERE
                    sg.GoodID = " . $goodId;

		$result = $this->query($sql);
		if (isset($_GET['debug_satellite'])) {
			print_r($result); exit();
		}

		return $result;
	}

}

?>