<?php

namespace SoloCatalog\Data;
use Solo\Stdlib\ArrayHelper;
use Solo\Db\TableGateway\AbstractTable;

class AllGoodsTable extends AbstractTable implements AllGoodsTableInterface {

	/**
	 * (non-PHPdoc)
	 * @see \SoloCatalog\Data\AllGoodsTableInterface::enumAllGoodIds()
	 */
	public function enumAllGoodIds() {
		$select = $this->createSelect();
		$select->columns(['GoodID']);		
		$select->order(['GoodID ASC']);		
		$result = $this->tableGateway->selectWith($select);
		return ArrayHelper::enumOneColumn($result, 'GoodID');
	}

    public function setMarkup($goodId, $markup) {
        $markup = str_replace(',', '.', $markup);
        $update = $this->createUpdate();
        $update->set([
            'Markup' => $markup,
        ]);
        $update->where([
            'GoodID' => $goodId,
        ]);
        return $this->tableGateway->updateWith($update);
    }
	
}

?>