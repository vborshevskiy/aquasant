<?php

namespace SoloCatalog\Data;

use Solo\Db\TableGateway\AbstractTable;

class CommentsTable extends AbstractTable implements CommentsTableInterface {

    /**
     * (non-PHPdoc)
     * @see \SoloItem\Data\CommentsTableInterface::getUncheckedIsRealBuyersComments()
     */
    public function getUncheckedIsRealBuyersComments() {
        $select = $this->createSelect();
        $select->where([
            'IsRealBuyer' => null,
        ]);
        $select->where->greaterThan('UserID', 0);
        return $this->tableGateway->selectWith($select);
    }

    /**
     * (non-PHPdoc)
     * @see \SoloItem\Data\CommentsTableInterface::isRealBuyer()
     */
    public function isRealBuyer($commentId, $isRealBuyer) {
        $update = $this->createUpdate();
        $update->set([
            'IsRealBuyer' => (int)$isRealBuyer,
        ]);
        $update->where([
            'ID' => $commentId,
        ]);
        return $this->tableGateway->updateWith($update);
    }

}
