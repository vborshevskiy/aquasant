<?php

namespace SoloCatalog\Data;

use SoloCache\Service\ProvidesCache;
use Solo\Db\Mapper\AbstractMapper;

class CategoriesMapper extends AbstractMapper implements CategoriesMapperInterface {

    use ProvidesCache;

    /**
     * @dependency_table soft_categories:active
     *
     * @param array $goodsIds        	
     * @return mixed
     */
    public function getCategoriesByGoodsIds($goodsIds) {
        if (!is_array($goodsIds) || count($goodsIds) == 0) {
            return [];
        }
        $cacher = $this->createMethodCacher(__METHOD__, func_get_args());
        if ($cacher->has())
            return $cacher->get();
        $sql = "SELECT
                            count(*) as countGoodsInCategory, 
                            ag.CategoryID, 
                            sc.CategoryUID, 
                            sc.CategoryName
                    FROM 				
                            #all_goods# ag
                    INNER JOIN
                            #goods_to_soft_categories# gtsc
                            ON ag.GoodID = gtsc.GoodID
                    INNER JOIN
                            #soft_categories# sc 
                            ON (gtsc.SoftCategoryID=sc.SoftCategoryID)
                    WHERE
                            ag.GoodID IN (" . implode(',', $goodsIds) . ")                               
                    GROUP BY
                            ag.CategoryId
                    ORDER BY
                            countGoodsInCategory DESC";
        $rows = $this->query($sql)->toArray();
        $cacher->set($rows);
        return $rows;
    }

    /**
     * 
     * @param array $goodsIds
     * @return array
     */
    public function getRelatedCategoriesByGoodsIds($goodsIds) {
        $cacher = $this->createMethodCacher(__METHOD__, func_get_args());
        if ($cacher->has())
            return $cacher->get();
        $sql = "SELECT DISTINCT
                                sc.SoftCategoryID,
                                sc.CategoryName,
                                sc.CategoryUID
                        FROM
                                #compatible# rg
                        INNER JOIN
                                #all_goods# alg
                                ON alg.GoodID = rg.CompatibleGoodID
                        INNER JOIN
                                #goods_to_soft_categories# gtsc
                                ON gtsc.GoodID = ag.GoodID
                        INNER JOIN 
                                #soft_categories# sc
                                ON gtsc.SoftCategoryID=hts.SoftCategoryID
                        WHERE
                                rg.GoodID in (" . implode(',', $goodsIds) . ")";
        $result = $this->query($sql)->toArray();
        $cacher->set($result);
        return $result;
    }

    /**
     * 
     * @param string $categoryUid
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getCategoryByUid($categoryUid) {
        $cacher = $this->createMethodCacher(__METHOD__, func_get_args());
        if ($cacher->has())
            return $cacher->get();

        $sql = "SELECT
                            sc.*
                    FROM
                            #soft_categories# sc
                    WHERE
                            sc.CategoryUID = '" . $categoryUid . "'
                            AND RealSoftCategoryID = 0
                            AND IsUrl = 0
                            AND cLevel >= 0";
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            $result = $rows->current();
            $cacher->set($result);
            return $result;
        }
        return null;
    }

    /**
     * @dependency_table soft_categories:active
     *
     * @param integer $categoryId        	
     * @return mixed | NULL
     */
    public function getCategoryById($categoryId) {
        $cacher = $this->createMethodCacher(__METHOD__, func_get_args());
        if ($cacher->has())
            return $cacher->get();

        $sql = "SELECT
					sc.*
				FROM
					#soft_categories# sc
				WHERE
					sc.SoftCategoryID = '" . $categoryId . "'
				LIMIT 1";
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            $result = $rows->current();
            $cacher->set($result);
            return $result;
        }
        return null;
    }

    public function getCategoriesByIds($categoryIds) {
        $sql = "SELECT
					sc.*
				FROM
					#soft_categories# sc
				WHERE
					sc.SoftCategoryID  IN (" . implode(',', $categoryIds) . ")
				";
        return$this->query($sql);
    }

    /**
     * @dependency_table goods_to_soft_categories:active
     * @dependency_table soft_categories:active
     *
     * @param integer $hardCategoryId        	
     * @return mixed | NULL
     */
    public function getCategoryByHardCategoryId($hardCategoryId) {
        $cacher = $this->createMethodCacher(__METHOD__, func_get_args());
        if ($cacher->has())
            return $cacher->get();

        $sql = "SELECT
					sc.*
        		FROM #all_goods# ag
        		INNER JOIN #goods_to_soft_categories# gtsc
        			ON ag.GoodID = gtsc.GoodID
				INNER JOIN #soft_categories# sc
					ON gtsc.SoftCategoryID = sc.SoftCategoryID
        			AND sc.RealSoftCategoryID = 0
					AND sc.IsUrl = 0
					AND sc.cLevel >= 0
				WHERE
					ag.CategoryID = " . $hardCategoryId . "
				ORDER BY
					sc.cLevel DESC, sc.SortIndex DESC
				LIMIT 1";
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            $result = $rows->current();
            $cacher->set($result);
            return $result;
        }
        return null;
    }
    
    /**
     * @dependency_table goods_to_soft_categories:active
     * @dependency_table soft_categories:active
     *
     * @param integer $hardCategoryId
     * @return mixed | NULL
     */
    public function getPrimaryCategoryByGoodId($goodId) {
    	$cacher = $this->createMethodCacher(__METHOD__, func_get_args());
    	if ($cacher->has())
    		return $cacher->get();
    	
    	$sql = "SELECT
					sc.*
        		FROM #goods_to_soft_categories# gtsc
				INNER JOIN #soft_categories# sc
					ON gtsc.SoftCategoryID = sc.SoftCategoryID
        			AND sc.RealSoftCategoryID = 0
					AND sc.IsUrl = 0
					AND sc.cLevel >= 0
				WHERE
					gtsc.GoodID = {$goodId}
    				AND gtsc.IsPrimary = 1
				LIMIT 1";
    	$rows = $this->query($sql);
    	if (0 < $rows->count()) {
    		$result = $rows->current();
    		$cacher->set($result);
    		return $result;
    	}
    	return null;
    }

    /**
     *
     * @param array $ids        	
     * @return \Zend\Db\ResultSet\Zend\Db\ResultSet
     */
    public function enumAvailCategoryIdsBySoftCategoryIds(array $ids) {
    	if (GOD_MODE) {
    		$sql = "SELECT
					DISTINCT sc.SoftCategoryID
				FROM
					#soft_categories# sc
				INNER JOIN
					#goods_to_soft_categories# gtsc
					ON sc.SoftCategoryID = gtsc.SoftCategoryID
				INNER JOIN
					#all_goods# ag
					ON gtsc.GoodID = gtsc.GoodID
				WHERE
					sc.SoftCategoryID IN (" . implode(',', $ids) . ")
					AND sc.cLevel >= 0";
    		$result = $this->query($sql)->toArray();
    		return $result;
    	}
    	
    	if (MANAGER_MODE) {
    		$sql = "SELECT
					DISTINCT sc.SoftCategoryID
				FROM
					#soft_categories# sc
				INNER JOIN
					#goods_to_soft_categories# gtsc
					ON sc.SoftCategoryID = gtsc.SoftCategoryID
				INNER JOIN
					#avail_manager_goods# ag
					ON gtsc.GoodID = gtsc.GoodID
				WHERE
					sc.SoftCategoryID IN (" . implode(',', $ids) . ")
					AND sc.cLevel >= 0";
    		$result = $this->query($sql)->toArray();
    		return $result;
    	}
    	
        $cacher = $this->createMethodCacher(__METHOD__, func_get_args());
        if ($cacher->has()) {
        	return $cacher->get();
        }

        $sql = "SELECT
					DISTINCT sc.SoftCategoryID
				FROM
					#soft_categories# sc
				INNER JOIN
					#goods_to_soft_categories# gtsc
					ON sc.SoftCategoryID = gtsc.SoftCategoryID
				INNER JOIN
					#avail_goods# ag
					ON gtsc.GoodID = gtsc.GoodID
				WHERE
					sc.SoftCategoryID IN (" . implode(',', $ids) . ")
					AND sc.cLevel >= 0";
        $result = $this->query($sql)->toArray();
        $cacher->set($result);
        return $result;
    }

    public function getParentCategories($cLevel, $cLeft, $cRight) {
        $cacher = $this->createMethodCacher(__METHOD__, func_get_args());
        if ($cacher->has())
            return $cacher->get();

        $sql = "SELECT DISTINCT
                        sc.SoftCategoryID,
                        sc.CategoryName,
                        sc.CategoryUID,
                        sc.cLevel
                FROM
                        #soft_categories:active# sc
                WHERE
                        sc.cLeft < " . $cLeft . "
                        AND sc.cRight > " . $cRight . "
                        AND sc.cLevel < " . $cLevel . "
                ORDER BY
                        sc.cLevel";
        $result = $this->query($sql)->toArray();
        $cacher->set($result);
        return $result;
    }

    /**
     * @dependency_table soft_categories:active
     * @dependency_table goods_to_soft_categories:active
     *
     * @param unknown $categoryId        	
     * @return array
     */
    public function enumCategoriesByParentId($categoryId) {
        $cacher = $this->createMethodCacher(__METHOD__, func_get_args());
        if ($cacher->has())
            return $cacher->get();

        $category = $this->getCategoryById($categoryId);
        $sql = "SELECT DISTINCT
					sc.*,
					ag.CategoryID AS HardCategoryID
				FROM
					#soft_categories# sc
				INNER JOIN
					#goods_to_soft_categories# gtsc
					ON sc.SoftCategoryID = gtsc.SoftCategoryID
        		INNER JOIN #all_goods# ag
        			ON gtsc.GoodID = ag.GoodID
				WHERE
					sc.cLevel >= " . $category->cLevel . "
					AND sc.cLeft >= " . $category->cLeft . "
					AND sc.cRight <= " . $category->cRight . "
				ORDER BY
					sc.cLevel ASC";
        $rows = $this->query($sql)->toArray();
        $categories = [];
        foreach ($rows as $row) {
            $categories[$row['HardCategoryID']] = $row;
        }
        $cacher->set($categories);
        return $categories;
    }

    /**
     * @dependency_table goods_to_soft_categories:active
     * @dependency_table soft_categories:active
     *
     * @param integer $hardCategoryId        	
     * @return array | NULL
     */
    public function enumCategoriesByHardCategoryId($hardCategoryId) {
        $cacher = $this->createMethodCacher(__METHOD__, func_get_args());
        if ($cacher->has())
            return $cacher->get();

        $sql = "SELECT DISTINCT
					sc.*
        		FROM #all_goods# ag
				INNER JOIN #goods_to_soft_categories# gtsc
        			ON ag.GoodID = gtsc.GoodID
				INNER JOIN
					#soft_categories# sc
					ON gtsc.SoftCategoryID = sc.SoftCategoryID
        			AND sc.RealSoftCategoryID = 0
					AND sc.IsUrl = 0
					AND sc.cLevel >= 0
				WHERE
					ag.CategoryID = " . $hardCategoryId . "
				ORDER BY
					sc.cLevel DESC, sc.SortIndex DESC";
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            $result = $rows->toArray();
            $cacher->set($result);
            return $result;
        }
        return null;
    }

}
