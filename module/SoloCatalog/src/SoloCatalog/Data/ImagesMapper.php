<?php

namespace SoloCatalog\Data;

use SoloCache\Service\ProvidesCache;
use Solo\Db\Mapper\AbstractMapper;

class ImagesMapper extends AbstractMapper implements ImagesMapperInterface {
	
	use ProvidesCache;

	/*
     * @param integer $goodId
     * @return array
     */
    public function getImagesByGoodId($goodId) {
        $sql = 'SELECT
                        gi.*
                FROM
                        #goods_images:active# gi
                INNER JOIN
                        #all_goods:active# ag
                        ON ag.GoodID = gi.GoodID
                INNER JOIN
                        #goods_images_views:active# giv
                        ON gi.ViewID = giv.ViewID
                WHERE
                        gi.GoodID = '.$goodId.'
                ORDER BY
                        giv.SortIndex,
                        gi.SortIndex';
        if (isset($_GET['debug_images'])) {
        	//print_r($this->query($sql)); exit();
        }
        return $this->query($sql)->toArray();
    }

    public function getImagesByGoodIds($goodIds) {
        $sql = 'SELECT
                        gi.*
                FROM
                        #goods_images:active# gi
                INNER JOIN
                        #all_goods:active# ag
                        ON ag.GoodID = gi.GoodID
                INNER JOIN
                        #goods_images_views:active# giv
                        ON gi.ViewID = giv.ViewID
                WHERE
                        gi.GoodID IN ('. implode(',', $goodIds).')
                ORDER BY
                        giv.SortIndex,
                        gi.SortIndex';
        return $this->query($sql)->toArray();
    }
    
    /*
     * @param integer $goodId
     * @return mixed
     */
    public function getGoodMainImage($goodId) {
        $sql = 'SELECT
                        gi.*
                FROM
                        #goods_images:active# gi
                INNER JOIN
                        #all_goods:active# ag
                        ON ag.GoodID = gi.GoodID
                INNER JOIN
                        #goods_images_views:active# giv
                        ON gi.ViewID = giv.ViewID
                WHERE
                        gi.GoodID = '.$goodId.'
                ORDER BY
                        giv.SortIndex
                LIMIT 1';
        $rows = $this->query($sql);
        if (0 < $rows->count()) {
			$result = $rows->current();
			return $result;
		}
		return null;
    }
    
    /*
     * @param array $goodIds
     * @return mixed
     */
    public function getGoodsMainImage($goodIds) {
        $sql = 'SELECT 
                  * 
                  FROM (
                    SELECT
                            gi.*
                    FROM
                            #goods_images:active# gi
                    INNER JOIN
                            #all_goods:active# ag
                            ON ag.GoodID = gi.GoodID
                    INNER JOIN
                            #goods_images_views:active# giv
                            ON gi.ViewID = giv.ViewID
                    WHERE
                            gi.GoodID IN ('.implode(',', $goodIds).')
                    ORDER BY
                            giv.SortIndex ASC,
                            gi.SortIndex ASC
                  ) t1';

        return $this->query($sql)->toArray();
    }
    
    /*
     * @param array $deletedImages
     * @return mixed
     */
    public function getDeletedImages($deletedImages) {
        $conditions = [];
        foreach ($deletedImages as $good) {
            if (array_key_exists('goodId', $good) && array_key_exists('viewId', $good)) {
                $conditions[] = '(gi.GoodID = '.$good['goodId'].' AND gi.ViewID = '.$good['viewId'].')';
            }
        }
        if (count($conditions) === 0) {
            return null;
        }
        $sql = 'SELECT
                        *
                FROM
                        #goods_images:passive# gi
                WHERE
                        '. implode(' OR ', $conditions);
        return $this->query($sql)->toArray();
    }
    
    /*
     * @param array $goodIdsAndViewIds
     * @return mixed
     */
    public function deleteImages($deletedImages) {
        $conditions = [];
        foreach ($deletedImages as $good) {
            if (array_key_exists('goodId', $good) && array_key_exists('viewId', $good)) {
                $conditions[] = '(GoodID = '.$good['goodId'].' AND ViewID = '.$good['viewId'].')';
            }
        }
        if (count($conditions) === 0) {
            return null;
        }
        $sql = 'DELETE
                FROM
                        #goods_images:passive#
                WHERE
                        '. implode(' OR ', $conditions);
        return $this->query($sql);
    }

}

?>