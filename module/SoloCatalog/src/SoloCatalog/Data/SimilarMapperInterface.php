<?php

namespace SoloCatalog\Data;

interface SimilarMapperInterface {

	/**
	 *
	 * @param integer $goodId        	
	 * @param integer $limit        	
	 * @param string $priceColumn        	
	 * @return \Zend\Cache\Storage\mixed multitype: Ambigous <\Solo\Db\QueryGateway\Ambigous, \Zend\Db\Adapter\Driver\StatementInterface, \Zend\Db\ResultSet\Zend\Db\ResultSet, \Zend\Db\Adapter\Driver\ResultInterface, \Zend\Db\ResultSet\Zend\Db\ResultSetInterface>
	 */
	public function enumSimilarGoods($goodId, $cityId, $limit, $priceColumn);

	/**
	 *
	 * @param integer $goodId        	
	 * @return \ArrayObject
	 */
	private function getAvailGoodById($goodId, $cityId);

}

?>