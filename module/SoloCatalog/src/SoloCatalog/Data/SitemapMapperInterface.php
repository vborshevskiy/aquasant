<?php

namespace SoloCatalog\Data;

interface SitemapMapperInterface {

    /**
     *
     * @return array
     */
    public function getCities();
    
    /**
     * 
     * @param integer $cityId
     * @return array
     */
    public function getAvailGoods($cityId);
}

?>