<?php
return [
	'search' => [
		'indexes' => [
			'categories' => [
				'name' => 'Categories',
				'nameTemplate' => 'Categories_%s',
				'fieldWeights' => [
					'name' => 10 
				] 
			],
			'goods' => [
				'name' => 'Goods',
				'nameTemplate' => 'Goods_%s_%s',
				'fieldWeights' => [
					'name' => 10 
				] 
			],
			'all_goods' => [
				'name' => 'Goods',
				'nameTemplate' => 'Goods_%s',
				'fieldWeights' => [
					'name' => 10 
				] 
			],
			'manager_goods' => [
				'name' => 'Goods',
				'nameTemplate' => 'Goods_%s_%s',
				'fieldWeights' => [
					'name' => 10 
				] 
			] 
		] 
	] 
];