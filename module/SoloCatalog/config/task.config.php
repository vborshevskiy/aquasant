<?php

use SoloCatalog\Task\AllGoodsReplicator;
use SoloCatalog\Task\CatalogReplicator;
use SoloCatalog\Task\FilesReplicator;
use SoloCatalog\Task\MarkupsReplicator;
use SoloCatalog\Task\PropertiesReplicator;
use SoloCatalog\Task\BrandsReplicator;
use SoloCatalog\Task\PricesReplicator;
use SoloCatalog\Task\RemainsReplicator;
use SoloCatalog\Task\CurrenciesReplicator;
use SoloCatalog\Task\GoodsImagesReplicator;
use SoloCatalog\Task\CompatibleReplicator;
use SoloCatalog\Task\GoodsImagesResizer;
use SoloCatalog\Task\DeliveryReplicator;
use SoloCatalog\Task\CommentsReplicator;
use SoloCatalog\Task\SitemapCreator;
use SoloCatalog\Task\SatelliteReplicator;
use SoloCatalog\Task\SetsReplicator;
use SoloCatalog\Task\ProblemProductsChecker;
use SoloCatalog\Task\CollectCatalogStats;
use SoloCatalog\Task\ReportCatalogStats;
use SoloCatalog\Task\SuppliersReplicator;


return [
    'tasks' => [
        'all_goods' => 'SoloCatalog\Task\AllGoodsReplicator',
        'catalog' => 'SoloCatalog\Task\CatalogReplicator',
        'properties' => 'SoloCatalog\Task\PropertiesReplicator',
        'brands' => 'SoloCatalog\Task\BrandsReplicator',
        'currencies' => 'SoloCatalog\Task\CurrenciesReplicator',
        'prices' => 'SoloCatalog\Task\PricesReplicator',
        'remains' => 'SoloCatalog\Task\RemainsReplicator',
        'goods_images' => 'SoloCatalog\Task\GoodsImagesReplicator',
        'goods_resizer' => 'SoloCatalog\Task\GoodsImagesResizer',
        'compatible' => 'SoloCatalog\Task\CompatibleReplicator',
        'delivery_prices' => 'SoloCatalog\Task\DeliveryReplicator',
        'comments' => 'SoloCatalog\Task\CommentsReplicator',
        'yml' => 'SoloCatalog\Task\YmlCreator',
        'sitemap' => 'SoloCatalog\Task\SitemapCreator',
        'files' => 'SoloCatalog\Task\FilesReplicator',
        'satellite' => 'SoloCatalog\Task\SatelliteReplicator',
        'sets' => 'SoloCatalog\Task\SetsReplicator',
        'problem_products_checker' => 'SoloCatalog\Task\ProblemProductsChecker',
        'collect_catalog_stats' => 'SoloCatalog\Task\CollectCatalogStats',
        'report_catalog_stats' => 'SoloCatalog\Task\ReportCatalogStats',
        'suppliers' => 'SoloCatalog\Task\SuppliersReplicator',
        'markups' => 'SoloCatalog\Task\MarkupsReplicator',
    ],
    'service_manager' => [
        'factories' => [
            'SoloCatalog\Task\CatalogReplicator' => function ($sm) {
                $softCategoriesTable = $sm->get('SoloCatalog\Data\SoftCategoriesTable');
                $softCategoriesTable->getTableGateway()->setTable($sm->get('triggers')->passive('soft_categories'));
                $hardToSoftTable = $sm->get('SoloCatalog\Data\HardToSoftTable');
                $hardToSoftTable->getTableGateway()->setTable($sm->get('triggers')->passive('hard_to_soft'));
                $goodToSoftTable = $sm->get('SoloCatalog\Data\GoodToSoftCategoryTable');
                $goodToSoftTable->getTableGateway()->setTable($sm->get('triggers')->passive('goods_to_soft_categories'));
                $sphinxConfigPath = $sm->get('Config')['search']['config_tpl'];
                $task = new CatalogReplicator($softCategoriesTable, $hardToSoftTable, $goodToSoftTable, $sphinxConfigPath);
                return $task;
            },
            'SoloCatalog\Task\AllGoodsReplicator' => function ($sm) {
                $allGoodsTable = $sm->get('SoloCatalog\Data\AllGoodsTable');
                $allGoodsTable->getTableGateway()->setTable($sm->get('triggers')->passive('all_goods'));
                $warrantyPeriodUnitsTable = $sm->get('SoloCatalog\Data\WarrantyPeriodUnitsTable');
                $warrantyPeriodUnitsTable->getTableGateway()->setTable($sm->get('triggers')->passive('warranty_period_units'));
                $discountReasonsTable = $sm->get('SoloCatalog\Data\DiscountReasonsTable');
                $discountReasonsTable->getTableGateway()->setTable($sm->get('triggers')->passive('discount_reasons'));
                $goodsToPackagesTable = $sm->get('SoloCatalog\Data\GoodsToPackagesTable');
                $goodsToPackagesTable->getTableGateway()->setTable($sm->get('triggers')->passive('goods_to_packages'));
                $packagesToGroupsTable = $sm->get('SoloCatalog\Data\PackagesToGroupsTable');
                $packagesToGroupsTable->getTableGateway()->setTable($sm->get('triggers')->passive('packages_to_groups'));

                $task = new AllGoodsReplicator($allGoodsTable, $warrantyPeriodUnitsTable, $discountReasonsTable, $goodsToPackagesTable, $packagesToGroupsTable);
                return $task;
            },
            'SoloCatalog\Task\BrandsReplicator' => function ($sm) {
                $brandsTable = $sm->get('SoloCatalog\Data\BrandsTable');
                $brandsTable->getTableGateway()->setTable($sm->get('triggers')->passive('brands'));
                $task = new BrandsReplicator($brandsTable);
                return $task;
            },
            'SoloCatalog\Task\PricesReplicator' => function ($sm) {
                $pricesTable = $sm->get('SoloCatalog\Data\PricesTable');
                $pricesTable->getTableGateway()->setTable($sm->get('triggers')->passive('goods_prices'));
                $task = new PricesReplicator($pricesTable);
                return $task;
            },
            'SoloCatalog\Task\RemainsReplicator' => function ($sm) {
                $sphinxConfigPath = $sm->get('Config')['search']['config_tpl'];
                $remainsTable = $sm->get('SoloCatalog\Data\RemainsTable');
                $remainsTable->getTableGateway()->setTable($sm->get('triggers')->passive('goods_remains'));
                $goodsRemainsMapper = $sm->get('SoloCatalog\Data\GoodsRemainsMapper');
                $availGoodsTable = $sm->get('SoloCatalog\Data\AvailGoodsTable');
                $availGoodsTable->getTableGateway()->setTable($sm->get('triggers')->passive('avail_goods'));
                $availWarehousesTable = $sm->get('SoloCatalog\Data\AvailWarehousesTable');
                $availWarehousesTable->getTableGateway()->setTable($sm->get('triggers')->passive('avail_warehouses'));
                $arrivalGoodsTable = $sm->get('SoloCatalog\Data\ArrivalGoodsTable');
                $arrivalGoodsTable->getTableGateway()->setTable($sm->get('triggers')->passive('arrival_goods'));
                $arrivalGoodsMapper = $sm->get('SoloCatalog\Data\ArrivalGoodsMapper');
                $deliveryMapper = $sm->get('SoloDelivery\Data\DeliveryMapper');
                $task = new RemainsReplicator($remainsTable, $goodsRemainsMapper, $availGoodsTable, $availWarehousesTable, $arrivalGoodsTable,
                        $arrivalGoodsMapper, $sphinxConfigPath, $deliveryMapper);
                return $task;
            },
            'SoloCatalog\Task\CurrenciesReplicator' => function ($sm) {
                $currenciesTable = $sm->get('SoloCatalog\Data\CurrenciesTable');
                $currenciesTable->getTableGateway()->setTable($sm->get('triggers')->passive('currencies'));
                $task = new CurrenciesReplicator($currenciesTable);
                return $task;
            },
            'SoloCatalog\Task\PropertiesReplicator' => function ($sm) {
                $propertiesTable = $sm->get('SoloCatalog\Data\PropertiesTable');
                $propertiesTable->getTableGateway()->setTable($sm->get('triggers')->passive('props'));
                $propertyUnitsTable = $sm->get('SoloCatalog\Data\PropertyUnitsTable');
                $propertyUnitsTable->getTableGateway()->setTable($sm->get('triggers')->passive('prop_units'));
                $propertyValuesTable = $sm->get('SoloCatalog\Data\PropertyValuesTable');
                $propertyValuesTable->getTableGateway()->setTable($sm->get('triggers')->passive('prop_values'));
                $propertiesGroupsTable = $sm->get('SoloCatalog\Data\PropertiesGroupsTable');
                $propertiesGroupsTable->getTableGateway()->setTable($sm->get('triggers')->passive('prop_groups'));
                $propertyTemplatesToCategoriesTable = $sm->get('SoloCatalog\Data\PropertyTemplatesToCategoriesTable');
                $propertyTemplatesToCategoriesTable->getTableGateway()->setTable($sm->get('triggers')->passive('templates_to_categories'));
                $propertyTemplatesToSiteCategoriesTable = $sm->get('SoloCatalog\Data\PropertyTemplatesToSiteCategoriesTable');
                $propertyTemplatesToSiteCategoriesTable->getTableGateway()->setTable($sm->get('triggers')->passive('templates_to_site_categories'));
                $goodsFiltersAspectsMapper = $sm->get('SoloCatalog\Data\GoodsFiltersAspectsMapper');
                $allGoodsTable = $sm->get('SoloCatalog\Data\AllGoodsTable');
                $goodsToPropertyValuesTable = $sm->get('SoloCatalog\Data\GoodsToPropertyValuesTable');
                $goodsToPropertyValuesTable->getTableGateway()->settable($sm->get('triggers')->passive('goods_to_propvalues'));
                $propertiesToPropertyTemplatesTable = $sm->get('SoloCatalog\Data\PropertiesToPropertyTemplatesTable');
                $propertiesToPropertyTemplatesTable->getTableGateway()->settable($sm->get('triggers')->passive('props_to_templates'));
                $goodNotesTable = $sm->get('SoloCatalog\Data\GoodNotesTable');
                $goodNotesTable->getTableGateway()->setTable($sm->get('triggers')->passive('good_notes'));

                $task = new PropertiesReplicator($propertiesTable, $allGoodsTable, $propertyTemplatesToCategoriesTable, $propertyTemplatesToSiteCategoriesTable, $propertyUnitsTable,
                        $propertyValuesTable, $propertiesGroupsTable, $goodsFiltersAspectsMapper, $goodsToPropertyValuesTable, $propertiesToPropertyTemplatesTable,
                        $goodNotesTable
                    );

                return $task;
            },
            'SoloCatalog\Task\GoodsImagesReplicator' => function ($sm) {
                $goodsImagesTable = $sm->get('SoloCatalog\Data\GoodsImagesTable');
                $goodsImagesTable->getTableGateway()->setTable($sm->get('triggers')->passive('goods_images'));
                $goodsImagesViewsTable = $sm->get('SoloCatalog\Data\GoodsImagesViewsTable');
                $goodsImagesViewsTable->getTableGateway()->setTable($sm->get('triggers')->passive('goods_images_views'));
                $task = new GoodsImagesReplicator($goodsImagesTable, $goodsImagesViewsTable);
                return $task;
            },
            'SoloCatalog\Task\GoodsImagesResizer' => function ($sm) {
                $goodsImagesTable = $sm->get('SoloCatalog\Data\GoodsImagesTable');
                $goodsImagesTable->getTableGateway()->setTable($sm->get('triggers')->passive('goods_images'));
                $task = new GoodsImagesResizer($goodsImagesTable);
                return $task;
            },
            'SoloCatalog\Task\CompatibleReplicator' => function ($sm) {
                $compatibleTable = $sm->get('SoloCatalog\Data\CompatibleTable');
                $compatibleTable->getTableGateway()->setTable($sm->get('triggers')->passive('compatible'));
                $task = new CompatibleReplicator($compatibleTable);
                return $task;
            },
            'SoloCatalog\Task\DeliveryReplicator' => function ($sm) {
                $deliveryPricesTable = $sm->get('SoloCatalog\Data\DeliveryPricesTable');
                $deliveryPricesTable->getTableGateway()->setTable($sm->get('triggers')->passive('delivery_prices'));
                $deliveryServicePricesTable = $sm->get('SoloCatalog\Data\DeliveryServicePricesTable');
                $deliveryServicePricesTable->getTableGateway()->setTable($sm->get('triggers')->passive('delivery_service_prices'));
                $task = new DeliveryReplicator($deliveryPricesTable, $deliveryServicePricesTable);
                return $task;
            },
            'SoloCatalog\Task\CommentsReplicator' => function ($sm) {
                $commentsTable = $sm->get('SoloCatalog\Data\CommentsTable');
                $commentsTable->getTableGateway()->setTable($sm->get('triggers')->active('comments'));
                $task = new CommentsReplicator($commentsTable);
                return $task;
            },
            'SoloCatalog\Task\SitemapCreator' => function ($sm) {
                $sitemapMapper = $sm->get('SoloCatalog\Data\SitemapMapper');
                $citiesSubdomains = $sm->get('Config')['subdomains']['citiesSubdomains'];
                $task = new SitemapCreator($sitemapMapper, $citiesSubdomains);
                return $task;
            },
            'SoloCatalog\Task\FilesReplicator' => function ($sm) {
                $filesTable = $sm->get('SoloCatalog\Data\FilesTable');
                $filesTable->getTableGateway()->setTable($sm->get('triggers')->passive('files'));
                $filesToGoodsTable = $sm->get('SoloCatalog\Data\FilesToGoodsTable');
                $filesToGoodsTable->getTableGateway()->setTable($sm->get('triggers')->passive('files_to_goods'));

                $task = new FilesReplicator($filesTable, $filesToGoodsTable);
                return $task;
            },
            'SoloCatalog\Task\SatelliteReplicator' => function ($sm) {
            	$groupsTable = $sm->get('SoloCatalog\Data\SatelliteGroupsTable');
            	$groupsTable->getTableGateway()->setTable($sm->get('triggers')->passive('satellite_groups'));
            	$goodsTable = $sm->get('SoloCatalog\Data\SatelliteGoodsTable');
            	$goodsTable->getTableGateway()->setTable($sm->get('triggers')->passive('satellite_goods'));

            	$task = new SatelliteReplicator($groupsTable, $goodsTable);
            	return $task;
            },
            'SoloCatalog\Task\SetsReplicator' => function ($sm) {
            	$setsTable = $sm->get('SoloCatalog\Data\SetsTable');
            	$setsTable->getTableGateway()->setTable($sm->get('triggers')->passive('sets'));

            	$groupsTable = $sm->get('SoloCatalog\Data\SetsGroupsTable');
            	$groupsTable->getTableGateway()->setTable($sm->get('triggers')->passive('sets_groups'));

            	$setsToGoodsTable = $sm->get('SoloCatalog\Data\SetsToGoodsTable');
            	$setsToGoodsTable->getTableGateway()->setTable($sm->get('triggers')->passive('sets_to_goods'));

            	$task = new SetsReplicator($setsTable, $groupsTable, $setsToGoodsTable);
            	return $task;
            },
            'SoloCatalog\Task\ProblemProductsChecker' => function ($sm) {
            	$task = new ProblemProductsChecker();
            	return $task;
            },
            'SoloCatalog\Task\CollectCatalogStats' => function ($sm) {
            	$task = new CollectCatalogStats();
            	return $task;
            },
            'SoloCatalog\Task\ReportCatalogStats' => function ($sm) {
            	$task = new ReportCatalogStats();
            	return $task;
            },
            'SoloCatalog\Task\SuppliersReplicator' => function ($sm) {
                $suppliersTable = $sm->get('SoloCatalog\Data\SuppliersTable');
                $suppliersTable->getTableGateway()->setTable($sm->get('triggers')->passive('suppliers'));

                $supplierGoodsTable = $sm->get('SoloCatalog\Data\SupplierGoodsTable');
                $supplierGoodsTable->getTableGateway()->setTable($sm->get('triggers')->passive('supplier_goods'));

                $task = new SuppliersReplicator($suppliersTable, $supplierGoodsTable);
                return $task;
            },
            'SoloCatalog\Task\MarkupsReplicator' => function ($sm) {
                $allGoodsTable = $sm->get('SoloCatalog\Data\AllGoodsTable');
                $allGoodsTable->getTableGateway()->setTable($sm->get('triggers')->passive('all_goods'));

                $task = new MarkupsReplicator($allGoodsTable);
                return $task;
            },

        ]
    ]
];
