<?php

return [
    'sitemap' => [
        'citiesSubdomains' => [
            2 => '',
            30 => 'nsk',
            31 => 'rnd',
        ],
        'cities' => [
            2 => 'msk',
            30 => 'nsk',
            31 => 'rnd',
        ],
        'mainDomain' => 'energoboom.ru',
    ],
];