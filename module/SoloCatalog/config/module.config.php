<?php

use SoloCatalog\Data\FilesMapper;
use SoloCatalog\Data\FilesTable;
use SoloCatalog\Data\FilesToGoodsTable;
use SoloCatalog\Data\GoodNotesMapper;
use SoloCatalog\Data\GoodNotesTable;
use SoloCatalog\Data\GoodsToPackagesTable;
use SoloCatalog\Data\PackagesToGroupsTable;
use SoloCatalog\Data\SupplierGoodsTable;
use SoloCatalog\Data\SuppliersTable;
use SoloCatalog\Search\GoodsIndexLocator;
use SoloCatalog\Search\CategoriesIndexLocator;
use Solo\Db\QueryGateway\QueryGateway;
use Solo\Db\TableGateway\TableGateway;

use SoloCatalog\Data\CategoriesMapper;
use SoloCatalog\Data\CompatibleMapper;
use SoloCatalog\Data\EstimateMapper;
use SoloCatalog\Data\FiltersMapper;
use SoloCatalog\Data\GoodsRemainsMapper;
use SoloCatalog\Data\GoodsMapper;
use SoloCatalog\Data\MenuMapper;
use SoloCatalog\Data\PricesMapper;
use SoloCatalog\Data\SimilarMapper;
use SoloCatalog\Data\PropertiesMapper;
use SoloCatalog\Data\GoodsFiltersAspectsMapper;
use SoloCatalog\Data\ImagesMapper;
use SoloCatalog\Data\ArrivalGoodsMapper;
use SoloCatalog\Data\BrandsMapper;
use SoloCatalog\Data\SitemapMapper;

use SoloCatalog\Data\AllGoodsTable;
use SoloCatalog\Data\PricesTable;
use SoloCatalog\Data\RemainsTable;
use SoloCatalog\Data\SoftCategoriesTable;
use SoloCatalog\Data\HardToSoftTable;
use SoloCatalog\Data\GoodsToPropertyValuesTable;
use SoloCatalog\Data\PropertiesToPropertyTemplatesTable;
use SoloCatalog\Data\PropertyTemplatesToCategoriesTable;
use SoloCatalog\Data\PropertyTemplatesToSiteCategoriesTable;
use SoloCatalog\Data\PropertyUnitsTable;
use SoloCatalog\Data\PropertyValuesTable;
use SoloCatalog\Data\BrandsTable;
use SoloCatalog\Data\CurrenciesTable;
use SoloCatalog\Data\PropertiesTable;
use SoloCatalog\Data\PropertiesGroupsTable;
use SoloCatalog\Data\GoodsImagesTable;
use SoloCatalog\Data\AvailGoodsTable;
use SoloCatalog\Data\AvailWarehousesTable;
use SoloCatalog\Data\WarrantyPeriodUnitsTable;
use SoloCatalog\Data\GoodsImagesViewsTable;
use SoloCatalog\Data\ArrivalGoodsTable;
use SoloCatalog\Data\DiscountReasonsTable;
use SoloCatalog\Data\DeliveryPricesTable;
use SoloCatalog\Data\CommentsTable;
use SoloCatalog\Data\DeliveryServicePricesTable;
use SoloCatalog\Data\GoodsToSoftCategoriesTable;
use SoloCatalog\Data\SatelliteGroupsTable;
use SoloCatalog\Data\SatelliteGoodsTable;
use SoloCatalog\Data\SatelliteGoodsMapper;
use SoloCatalog\Data\SetsGroupsTable;
use SoloCatalog\Data\SetsTable;
use SoloCatalog\Data\SetsToGoodsTable;

return [
    'controller_plugins' => [
        'invokables' => [
            'catalog' => 'SoloCatalog\Controller\Plugin\Catalog'
        ]
    ],
    'service_manager' => [
        'aliases' => [
            'catalog_categories' => 'SoloCatalog\Service\CategoriesService',
            'catalog_filters' => 'SoloCatalog\Service\FiltersService',
            'catalog_goods' => 'SoloCatalog\Service\GoodsService',
            'catalog_menu' => 'SoloCatalog\Service\MenuService',
            'catalog_prices' => 'SoloCatalog\Service\PricesService',
            'catalog_remains' => 'SoloCatalog\Service\RemainsService',
            'catalog_compatible' => 'SoloCatalog\Service\CompatibleService',
            'catalog_search' => 'SoloCatalog\Service\SearchService',
            'catalog_brands' => 'SoloCatalog\Service\BrandsService',
            'catalog_currencies' => 'SoloCatalog\Service\CurrenciesService',
            'catalog_images' => 'SoloCatalog\Service\ImageService',
            'catalog_files' => 'SoloCatalog\Service\FilesService',
            'catalog_satellite' => 'SoloCatalog\Service\SatelliteService'
        ],
        'search_indexes' => [
            'goods' => 'SoloCatalog\Search\GoodsIndexLocator',
            'all_goods' => 'SoloCatalog\Search\GoodsIndexLocator',
            'manager_goods' => 'SoloCatalog\Search\GoodsIndexLocator',
            'categories' => 'SoloCatalog\Search\CategoriesIndexLocator'
        ],
        'factories' => [
            'SoloCatalog\Data\GoodNotesTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('good_notes'), 'NoteID');
                $table = new GoodNotesTable($gateway);
                return $table;
            },
            'SoloCatalog\Data\AllGoodsTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('all_goods'), 'GoodID');
                $table = new AllGoodsTable($gateway);
                return $table;
            },
            'SoloCatalog\Data\SetsGroupsTable' => function ($sm) {
            	$gateway = new TableGateway($sm->get('triggers')->active('sets_groups'), 'SetGroupID');
            	$table = new SetsGroupsTable($gateway);
            	return $table;
            },
            'SoloCatalog\Data\SetsTable' => function ($sm) {
            	$gateway = new TableGateway($sm->get('triggers')->active('sets'), 'SetID');
            	$table = new SetsTable($gateway);
            	return $table;
            },
            'SoloCatalog\Data\SetsToGoodsTable' => function ($sm) {
            	$gateway = new TableGateway($sm->get('triggers')->active('sets_to_goods'), 'SetToGoodID');
            	$table = new SetsToGoodsTable($gateway);
            	return $table;
            },
            'SoloCatalog\Data\PricesTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('goods_prices'), 'GoodID');
                $table = new PricesTable($gateway);
                return $table;
            },
            'SoloCatalog\Data\SatelliteGroupsTable' => function ($sm) {
            	$gateway = new TableGateway($sm->get('triggers')->active('satellite_groups'), 'GroupID');
            	$table = new SatelliteGroupsTable($gateway);
            	return $table;
            },
            'SoloCatalog\Data\SatelliteGoodsTable' => function ($sm) {
            	$gateway = new TableGateway($sm->get('triggers')->active('satellite_goods'), 'SatelliteGoodID');
            	$table = new SatelliteGoodsTable($gateway);
            	return $table;
            },
            'SoloCatalog\Data\SatelliteGoodsMapper' => function ($sm) {
            	return new SatelliteGoodsMapper(new QueryGateway());
            },
            'SoloCatalog\Data\RemainsTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('goods_remains'), 'GoodID');
                $table = new RemainsTable($gateway);
                return $table;
            },
            'SoloCatalog\Data\SoftCategoriesTable' => function($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('soft_categories'), 'SoftCategoryID');
                $table = new SoftCategoriesTable($gateway);
                return $table;
            },
            'SoloCatalog\Data\HardToSoftTable' => function($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('hard_to_soft'), ['HardCategoryID', 'SoftCategoryID']);
                $table = new HardToSoftTable($gateway);
                return $table;
            },
            'SoloCatalog\Data\GoodToSoftCategoryTable' => function($sm) {
            	$gateway = new TableGateway($sm->get('triggers')->active('goods_to_soft_categories'), ['GoodToSoftCategoryID']);
            	$table = new GoodsToSoftCategoriesTable($gateway);
            	return $table;
            },
            'SoloCatalog\Data\GoodsToPropertyValuesTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('goods_to_propvalues'), ['GoodID', 'PropertyID', 'PropertyValueID']);
                return new GoodsToPropertyValuesTable($gateway);
            },
            'SoloCatalog\Data\PropertiesTable' => function($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('props'), 'PropertyID');
                return new PropertiesTable($gateway);
            },
            'SoloCatalog\Data\PropertiesToPropertyTemplatesTable' => function($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('props_to_templates'), ['PropertyID', 'PropertyTemplateID']);
                return new PropertiesToPropertyTemplatesTable($gateway);
            },
            'SoloCatalog\Data\PropertyTemplatesToCategoriesTable' => function($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('templates_to_categories'), ['PropertyTemplateID', 'CategoryID']);
                return new PropertyTemplatesToCategoriesTable($gateway);
            },
            'SoloCatalog\Data\PropertyTemplatesToSiteCategoriesTable' => function($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('templates_to_site_categories'), ['PropertyTemplateID', 'SiteCategoryID']);
                return new PropertyTemplatesToSiteCategoriesTable($gateway);
            },
            'SoloCatalog\Data\PropertyUnitsTable' => function($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('prop_units'), 'PropertyUnitID');
                return new PropertyUnitsTable($gateway);
            },
            'SoloCatalog\Data\BrandsTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('brands'), 'BrandID');
                return new BrandsTable($gateway);
            },
            'SoloCatalog\Data\CurrenciesTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('currencies'), 'CurrencyID');
                return new CurrenciesTable($gateway);
            },
            'SoloCatalog\Data\GoodsImagesTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('goods_images'), ['GoodID','ViewID']);
                $table = new GoodsImagesTable($gateway);
                return $table;
            },
            'SoloCatalog\Data\PropertyValuesTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('prop_values'), 'PropertyValueID');
                return new PropertyValuesTable($gateway);
            },
            'SoloCatalog\Data\AvailWarehousesTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('avail_warehouses'), 'GoodID');
                return new AvailWarehousesTable($gateway);
            },
            'SoloCatalog\Data\AvailGoodsTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('avail_goods'), 'GoodID');
                return new AvailGoodsTable($gateway);
            },
            'SoloCatalog\Data\PropertiesGroupsTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('prop_groups'), 'PropertyGroupID');
                return new PropertiesGroupsTable($gateway);
            },
            'SoloCatalog\Data\WarrantyPeriodUnitsTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('warranty_period_units'), 'WarrantyPeriodUnitID');
                return new WarrantyPeriodUnitsTable($gateway);
            },
            'SoloCatalog\Data\GoodsImagesViewsTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('goods_images_views'), 'ViewID');
                return new GoodsImagesViewsTable($gateway);
            },
            'SoloCatalog\Data\ArrivalGoodsTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('arrival_goods'), ['GoodID', 'IsTwin', 'OfficeID']);
                return new ArrivalGoodsTable($gateway);
            },
            'SoloCatalog\Data\DiscountReasonsTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('discount_reasons'), 'DiscountReasonID');
                return new DiscountReasonsTable($gateway);
            },
            'SoloCatalog\Data\DeliveryPricesTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('delivery_prices'), ['GoodID','CityID']);
                return new DeliveryPricesTable($gateway);
            },
            'SoloCatalog\Data\CommentsTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('comments'), 'ID');
                $table = new CommentsTable($gateway);
                return $table;
            },
            'SoloCatalog\Data\DeliveryServicePricesTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('delivery_service_prices'), 'DeliveryServiceID');
                $table = new DeliveryServicePricesTable($gateway);
                return $table;
            },
            'SoloCatalog\Data\FilesTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('files'), 'FileID');
                $table = new FilesTable($gateway);
                return $table;
            },
            'SoloCatalog\Data\FilesToGoodsTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('files_to_goods'), 'FileID');
                $table = new FilesToGoodsTable($gateway);
                return $table;
            },
            'SoloCatalog\Data\GoodsToPackagesTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('goods_to_packages'), ['GoodID', 'PackageID']);
                $table = new GoodsToPackagesTable($gateway);
                return $table;
            },
            'SoloCatalog\Data\PackagesToGroupsTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('packages_to_groups'), ['PackageID', 'GroupID']);
                $table = new PackagesToGroupsTable($gateway);
                return $table;
            },
            'SoloItem\Data\CommentMapper' => function ($sm) {
                return new CommentMapper(new QueryGateway());
            },
            'SoloCatalog\Data\CategoriesMapper' => function ($sm) {
                return new CategoriesMapper(new QueryGateway());
            },
            'SoloCatalog\Data\CompatibleMapper' => function ($sm) {
                return new CompatibleMapper(new QueryGateway());
            },
            'SoloCatalog\Data\EstimateMapper' => function ($sm) {
                return new EstimateMapper(new QueryGateway());
            },
            'SoloCatalog\Data\FiltersMapper' => function ($sm) {
                return new FiltersMapper(new QueryGateway());
            },
            'SoloCatalog\Data\GoodsMapper' => function ($sm) {
                return new GoodsMapper(new QueryGateway());
            },
            'SoloCatalog\Data\MenuMapper' => function ($sm) {
                return new MenuMapper(new QueryGateway());
            },
            'SoloCatalog\Data\PricesMapper' => function ($sm) {
                return new PricesMapper(new QueryGateway());
            },
            'SoloCatalog\Data\SimilarMapper' => function ($sm) {
                return new SimilarMapper(new QueryGateway());
            },
            'SoloCatalog\Data\PropertiesMapper' => function ($sm) {
                return new PropertiesMapper(new QueryGateway());
            },
            'SoloCatalog\Data\GoodsFiltersAspectsMapper' => function ($sm) {
                return new GoodsFiltersAspectsMapper(new QueryGateway());
            },
            'SoloCatalog\Data\GoodsRemainsMapper' => function ($sm) {
                return new GoodsRemainsMapper(new QueryGateway());
            },
            'SoloCatalog\Data\ImagesMapper' => function ($sm) {
                return new ImagesMapper(new QueryGateway());
            },
            'SoloCatalog\Data\ArrivalGoodsMapper' => function ($sm) {
                return new ArrivalGoodsMapper(new QueryGateway());
            },
            'SoloCatalog\Data\SitemapMapper' => function ($sm) {
                return new SitemapMapper(new QueryGateway());
            },
            'SoloCatalog\Data\BrandsMapper' => function ($sm) {
                return new BrandsMapper(new QueryGateway());
            },
            'SoloCatalog\Data\GoodNotesMapper' => function ($sm) {
                return new GoodNotesMapper(new QueryGateway());
            },
            'SoloCatalog\Data\FilesMapper' => function ($sm) {
                return new FilesMapper(new QueryGateway());
            },
            'SoloCatalog\Service\FilesService' => 'SoloCatalog\Service\FilesServiceFactory',
            'SoloCatalog\Service\CategoriesService' => 'SoloCatalog\Service\CategoriesServiceFactory',
            'SoloCatalog\Service\FiltersService' => 'SoloCatalog\Service\FiltersServiceFactory',
            'SoloCatalog\Service\GoodsService' => 'SoloCatalog\Service\GoodsServiceFactory',
            'SoloCatalog\Service\SetsService' => 'SoloCatalog\Service\SetsServiceFactory',
            'SoloCatalog\Service\MenuService' => 'SoloCatalog\Service\MenuServiceFactory',
            'SoloCatalog\Service\PricesService' => 'SoloCatalog\Service\PricesServiceFactory',
            'SoloCatalog\Service\Helper\PricesHelper' => 'SoloCatalog\Service\Helper\PricesHelperFactory',
            'SoloCatalog\Service\SimilarService' => 'SoloCatalog\Service\SimilarServiceFactory',
            'SoloCatalog\Service\CompatibleService' => 'SoloCatalog\Service\CompatibleServiceFactory',
            'SoloCatalog\Service\SearchService' => 'SoloCatalog\Service\SearchServiceFactory',
            'SoloCatalog\Service\BrandsService' => 'SoloCatalog\Service\BrandsServiceFactory',
            'SoloCatalog\Service\CurrenciesService' => 'SoloCatalog\Service\CurrenciesServiceFactory',
            'SoloCatalog\Service\ImageService' => 'SoloCatalog\Service\ImageServiceFactory',
            'SoloCatalog\Service\SatelliteService' => 'SoloCatalog\Service\SatelliteServiceFactory',
            'SoloCatalog\Search\GoodsIndexLocator' => function ($sm) {
                $config = $sm->get('Config');
                if (isset($config['search'])) {
                    $settings = $config['search'];
                    $default = $settings['default'];
                    $index = $settings['indexes']['goods'];
                    $locator = new GoodsIndexLocator($index['name'], $sm->get('SphinxConnection'), array_merge($default, $index));
                    $locator->events()->attach('search.pre', $settings['formatters']['simple_sphinx']);
                    return $locator;
                }
                return null;
            },
            'SoloCatalog\Search\AllGoodsIndexLocator' => function ($sm) {
            	$config = $sm->get('Config');
            	if (isset($config['search'])) {
            		$settings = $config['search'];
            		$default = $settings['default'];
            		$index = $settings['indexes']['all_goods'];
            		$locator = new GoodsIndexLocator($index['name'], $sm->get('SphinxConnection'), array_merge($default, $index));
            		$locator->events()->attach('search.pre', $settings['formatters']['simple_sphinx']);
            		return $locator;
            	}
            	return null;
            },
            'SoloCatalog\Search\ManagerGoodsIndexLocator' => function ($sm) {
            	$config = $sm->get('Config');
            	if (isset($config['search'])) {
            		$settings = $config['search'];
            		$default = $settings['default'];
            		$index = $settings['indexes']['manager_goods'];
            		$locator = new GoodsIndexLocator($index['name'], $sm->get('SphinxConnection'), array_merge($default, $index));
            		$locator->events()->attach('search.pre', $settings['formatters']['simple_sphinx']);
            		return $locator;
            	}
            	return null;
            },
            'SoloCatalog\Search\CategoriesIndexLocator' => function ($sm) {
                $config = $sm->get('Config');
                if (isset($config['search'])) {
                    $settings = $config['search'];
                    $default = $settings['default'];
                    $index = $settings['indexes']['categories'];
                    $locator = new CategoriesIndexLocator($index['name'], $sm->get('SphinxConnection'), array_merge($default, $index));
                    $locator->events()->attach('search.pre', $settings['formatters']['simple_sphinx']);
                    return $locator;
                }
                return null;
            },
            'SoloCatalog\Data\SuppliersTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('suppliers'), 'SupplierID');

                $table = new SuppliersTable($gateway);

                return $table;
            },
            'SoloCatalog\Data\SupplierGoodsTable' => function ($sm) {
                $gateway = new TableGateway($sm->get('triggers')->active('supplier_goods'), ['GoodID', 'SupplierID', 'OfficeID']);
                $table = new SupplierGoodsTable($gateway);
                return $table;
            },
        ]
    ]
];