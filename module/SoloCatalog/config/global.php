<?php

return [
    'catalog' => [
        'defaults' => [
            'goods_per_page' => 50,
            'sort_column' => 'avail',
            'sort_direction' => 'desc',
            'view' => 'tile',
        ],
        'sort_columns' => [
            'price' => 'Value',
			'avail' => '(0 < agd.AvailQuantity)'
        ],
        'sort_directions' => [
            'asc',
            'desc'
        ],
        'onlyDeliveryCitiesIds' => [
            //2,
        ],
        'cities_map_config' => [
            2 => [
                'latitude' => 55.7103495750234,
                'longitude' => 37.628050081633776,
                'zoom' => 11,
            ],
        ],
        'prices' => [
            'defaultColumnNumber' => 1,
            'columns' => [
                [
                    'number' => 1,
                    'categoryId' => 65,
                    'types' => [
                        'default' => [
                            'name' => 'RetailPrice'
                        ],
                        'purchase' => [
                            'name' => 'PurchaseRetailPrice'
                        ]
                    ]
                ],
                [
                    'number' => 2,
                    'categoryId' => 66,
                    'types' => [
                        'default' => [
                            'name' => 'SmallWholesalePrice'
                        ],
                        'purchase' => [
                            'name' => 'PurchaseSmallWholesalePrice'
                        ]
                    ]
                ],
                [
                    'number' => 3,
                    'categoryId' => 67,
                    'types' => [
                        'default' => [
                            'name' => 'MediumWholesalePrice'
                        ],
                        'purchase' => [
                            'name' => 'PurchaseMediumWholesalePrice'
                        ]
                    ]
                ]
            ]
        ]
    ]
];
