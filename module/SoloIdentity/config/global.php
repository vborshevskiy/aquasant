<?php

return [
    'identity' => [
        'bonusBalanceExpire' => 60,
        'balanceExpire' => 60,
        'maxBonusesPaidPercent' => 100,
    ]
];
