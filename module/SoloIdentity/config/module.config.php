<?php

use SoloIdentity\Entity\Ultima\User;
use Solo\Stdlib\ClassUtils;
use SoloIdentity\Entity\Ultima\Agent;
use SoloIdentity\Service\Ultima\ManagerService;
use SoloIdentity\Controller\Plugin\Auth;

return array(
    'controller_plugins' => array(
        'invokables' => array(
            'auth' => 'SoloIdentity\Controller\Plugin\Auth',
            'identity' => 'SoloIdentity\Controller\Plugin\Identity'
        )
    ),
    'service_manager' => array(
        'aliases' => array(
            'authenticate_service' => '\SoloIdentity\Service\Ultima\AuthenticateService',
            'user_service' => '\SoloIdentity\Service\Ultima\UserService',
            'agent_service' => '\SoloIdentity\Service\Ultima\AgentService',
            'address_service' => '\SoloIdentity\Service\Ultima\AddressService',
            'recovery_service' => '\SoloIdentity\Service\Ultima\RecoveryService',
            'address_instance' => '\SoloIdentity\Entity\Ultima\Address',
            'auth_plugin' => '\SoloIdentity\Controller\Plugin\Auth',
        ),
        'factories' => array(
            '\SoloIdentity\Service\Ultima\AuthenticateService' => function ($sm) {
                return new \SoloIdentity\Service\Ultima\AuthenticateService();
            },
            '\SoloIdentity\Service\Ultima\UserService' => function ($sm) {
                return new \SoloIdentity\Service\Ultima\UserService();
            },
            '\SoloIdentity\Service\Ultima\AgentService' => function ($sm) {
                return new \SoloIdentity\Service\Ultima\AgentService();
            },
            '\SoloIdentity\Entity\Ultima\Address' => function ($sm) {
                $address = new \SoloIdentity\Entity\Ultima\Address;
                $address->setDeliveryCostGetter($sm->get('\SoloIdentity\Service\Ultima\Behavior\DeliveryCostGetter'));
                return $address;
            },        
            '\SoloIdentity\Service\Ultima\AddressService' => function ($sm) {
                $service = new \SoloIdentity\Service\Ultima\AddressService();
                return $service;
            },
            '\SoloIdentity\Service\Ultima\RecoveryService' => function ($sm) {
                return new \SoloIdentity\Service\Ultima\RecoveryService();
            },
            '\SoloIdentity\Service\Ultima\ManagerService' => function ($sm) {
                $serv = new ManagerService();
                $config = $sm->get('Config');
                if (isset($config['identity']) && isset($config['identity']['manager'])) {
                    $settings = $config['identity']['manager'];
                    $serv->setDefaultEmail($settings['email']);
                }
                return $serv;
            },
            '\SoloIdentity\Service\Ultima\Behavior\DeliveryCostGetter' => function ($sm) {
                $deliveryCostGetter = new \SoloIdentity\Service\Ultima\Behavior\DeliveryCostGetter();
                $deliveryCostGetter->setDeliveryCostGateway($sm->get('\SoloIdentity\Data\DeliveryCostGateway'));
                return $deliveryCostGetter;
            },
            '\SoloIdentity\Entity\Ultima\User' => function ($sm) {
                $user = new User();
                if (ClassUtils::hasTrait($user, 'SoloIdentity\\Entity\\Ultima\\ProvidesUserBalance')) {
                    $config = $sm->get('Config');
                    if (isset($config['identity'])) {
                        $settings = $config['identity'];
                        $user->setBonusBalanceExpire($settings['bonusBalanceExpire']);
                    }
                }
                return $user;
            },
            '\SoloIdentity\Entity\Ultima\Agent' => function ($sm) {
                $agent = new Agent();
                if (ClassUtils::hasTrait($agent, 'SoloIdentity\\Entity\\Ultima\\ProvidesAgentBalance')) {
                    $config = $sm->get('Config');
                    if (isset($config['identity'])) {
                        $settings = $config['identity'];
                        $agent->setBalanceExpire($settings['balanceExpire']);
                    }
                }
                return $agent;
            },
            '\SoloIdentity\Controller\Plugin\Auth' => function ($sm) {
                return new Auth();
            },
            'user_session' => function ($sm) {
                return new \Zend\Session\Container('identity');
            },
            'fake_user_session' => function ($sm) {
                return new \Zend\Session\Container('fake_identity');
            },
        ),
        'shared' => [
            '\SoloIdentity\Entity\Ultima\User' => false,
            '\SoloIdentity\Entity\Ultima\Agent' => false,
            '\SoloIdentity\Entity\Ultima\Address' => false,
            'user_session' => false,
            'fake_user_session' => false,
        ]
    )
);