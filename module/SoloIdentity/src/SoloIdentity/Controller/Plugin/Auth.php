<?php

namespace SoloIdentity\Controller\Plugin;

use SoloIdentity\Entity\Ultima\User;
use SoloIdentity\Entity\Ultima\FakeUser;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use SoloIdentity\Entity\Ultima\Agent;

/**
 * Auth
 *
 * @author Slava Tutrinov
 */
class Auth extends AbstractPlugin {

    /**
     *
     * @var User
     */
    private $user = null;
    
    /**
     *
     * @var FakeUser
     */
    private $fakeUser = null;

    /**
     * Checks is user authentificated
     *
     * @return boolean
     */
    public function logged() {
        return (null !== $this->getUser());
    }

    /**
     * @return boolean
     */
    public function loggedFake() {
        return (null !== $this->getFakeUser());
    }

    /**
     *
     * @return \SoloIdentity\Entity\Ultima\Agent
     */
    public function getCurrentAgent() {
        return $this->getUser()->getCurrentAgent();
    }

    /**
     *
     * @param integer|Agent $agentOrId        	
     * @throws \RuntimeException
     */
    public function setCurrentAgent($agentOrId) {
        $agent = null;
        if (is_integer($agentOrId)) {
            $agent = $this->getUser()->agents()[$agentOrId];
        }
        if (is_object($agentOrId) && ($agentOrId instanceof Agent)) {
            $agent = $agentOrId;
        }
        if (null === $agent) {
            throw new \RuntimeException('Failed to set current agent by id or by object reference');
        }
        $user = $this->getUser()->setCurrentAgent($agent);

        $session = $this->getSession();
        if (isset($session->user)) {
            unset($session->user);
            $session->user = $user->serialize();
        }
    }

    /**
     *
     * @return \SoloIdentity\Entity\Ultima\User
     */
    public function getUser() {
        if (null === $this->user) {
            $session = $this->getSession();
            if (isset($session->user)) {
                $user = new User();
                $user->unserialize($session->user);
                $this->user = $user;
                
                if (is_array($session->agents)) {
                    foreach ($session->agents as $item) {
                        $agent = new Agent();
                        $agent->unserialize($item);

                        $user->addAgent($agent);
                    }
                    $user->setCurrentAgent($user->agents()[$user->getCurrentAgent()]);
                    $this->user = $user;
                }

                if (isset($session->adresses) && is_array($session->adresses)) {
                    foreach ($session->adresses as $address)
                        $user->addAddress($address);
                    $this->user = $user;
                }
                
                if (\Solo\Stdlib\ClassUtils::hasTrait($user, 'SoloIdentity\\Entity\\Ultima\\ProvidesUserBalance')) {
                    $user->events()->attach('bonusBalanceExpired', function ($e) {
                        $user = $e->getTarget();
                        $bonusBalanceResult = $this->getController()->getServiceLocator()->get('SoloIdentity\\Service\\Ultima\\AgentService')->getBonusBalance($user->getSecurityKey());
                        if ($bonusBalanceResult->success() && isset($bonusBalanceResult->bonusBalance)) {
                            $user->setBonusBalance($bonusBalanceResult->bonusBalance);
                        }
                    });
                }
                
                if (\Solo\Stdlib\ClassUtils::hasTrait($user, 'SoloIdentity\\Entity\\Ultima\\ProvidesUserBalance')) {
                    $user->events()->attach('userChange.post', function ($e) {
                        $user = $e->getTarget();
                        $userSession = $bonusBalanceResult = $this->getController()->getServiceLocator()->get('user_session');
                        $userSession->user = $user->serialize();
                    });
                }
            }
        }
        return $this->user;
    }

    /**
     *
     * @return \SoloIdentity\Entity\Ultima\User
     */
    public function getFakeUser() {
        if (null === $this->fakeUser) {
            $session = $this->getFakeSession();
            if (isset($session->fakeUser)) {
                $fakeUser = new FakeUser();
                $fakeUser->unserialize($session->fakeUser);
                $this->fakeUser = $fakeUser;
            }
        }
        return $this->fakeUser;
    }

    public function logout() {
        $session = $this->getSession();
        if (isset($session->user)) {
            unset($session->user);
        }
        if (isset($session->agents)) {
            unset($session->agents);
        }
        if (isset($session->adresses)) {
            unset($session->adresses);
        }
        $this->user = null;
    }

    /**
     *
     * @param string $login        	
     * @param string $password        	
     * @return \SoloIdentity\Service\Ultima\Result\AuthResult
     */
    public function login($login, $password, $reserveId = null) {
        $authResult = $this->getController()->getServiceLocator()->get('authenticate_service')->authenticate($login, $password, $reserveId);
        if ($authResult->success() && isset($authResult->user)) {

            $session = $this->getSession();
            
            $this->enumAgents($authResult->user->getSecurityKey());

            $fakesession = $this->getFakeSession();
            $fakesession->fakeUser = null;
            unset($fakesession->fakeUser);


            $deliveryAdressesResult = $this->getController()->getServiceLocator()->get('address_service')->getDeliveryAdresses($authResult->user->getSecurityKey());
            if ($deliveryAdressesResult->success() && isset($deliveryAdressesResult->items))
                $session->adresses = $deliveryAdressesResult->items;
            
            $session->user = $authResult->user->serialize();
        }

        return $authResult;
    }
    
    public function enumAgents($securityKey) {
        $agents = [];
        $session = $this->getSession();
        $agentsResult = $this->getController()->getServiceLocator()->get('agent_service')->enumAgents($securityKey);
        if ($agentsResult->success() && isset($agentsResult->agents)) {
            foreach ($agentsResult->agents as $agent) {
                $agents[$agent->getId()] = $agent->serialize();
            }
        }
        $session->agents = $agents;
        return;
    }

    /**
     *
     * @return \Zend\Session\Container
     */
    public function getSession() {
        return $this->getController()->getServiceLocator()->get('user_session');
    }

    /**
     *
     * @return \Zend\Session\Container
     */
    public function getFakeSession() {
        return $this->getController()->getServiceLocator()->get('fake_user_session');
    }

}

?>
