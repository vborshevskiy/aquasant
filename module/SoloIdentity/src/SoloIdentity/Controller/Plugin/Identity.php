<?php

namespace SoloIdentity\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use SoloIdentity\Service\Ultima\AgentService;
use SoloIdentity\Service\Ultima\AddressService;
use SoloIdentity\Service\Ultima\UserService;
use SoloIdentity\Service\Ultima\RecoveryService;
use SoloIdentity\Service\Ultima\ManagerService;

class Identity extends AbstractPlugin {

    /**
     *
     * @return UserService
     */
    public function users() {
        return $this->getController()->getServiceLocator()->get('user_service');
    }

    /**
     *
     * @return AgentService
     */
    public function agents() {
        return $this->getController()->getServiceLocator()->get('agent_service');
    }

    /**
     *
     * @return AddressService
     */
    public function addresses() {
        return $this->getController()->getServiceLocator()->get('address_service');
    }

    /**
     *
     * @return RecoveryService
     */
    public function recovery() {
        return $this->getController()->getServiceLocator()->get('recovery_service');
    }

    /**
     *
     * @return ManagerService
     */
    public function manager() {
        return $this->getController()->getServiceLocator()->get('\\SoloIdentity\\Service\\ManagerService');
    }

}
