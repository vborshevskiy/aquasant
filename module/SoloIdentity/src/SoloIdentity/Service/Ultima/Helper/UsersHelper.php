<?php

namespace SoloIdentity\Service\Ultima\Helper;

use SoloIdentity\Entity\Ultima\Agent;

class UsersHelper {

	/**
	 *
	 * @return multitype:multitype:string number
	 */
	public function enumFirmStructs() {
		return [
			[
				'key' => Agent::FIRM_STRUCT_COMMERCIAL,
				'value' => 'коммерческая орг.' 
			],
			[
				'key' => Agent::FIRM_STRUCT_GOV,
				'value' => 'государственная орг.' 
			],
			[
				'key' => Agent::FIRM_STRUCT_IB,
				'value' => 'ИП' 
			] 
		];
	}

}

?>