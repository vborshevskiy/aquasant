<?php

namespace SoloIdentity\Service\Ultima\Helper;

use SoloIdentity\Entity\Ultima\Address;

class AddressHelper {

	/**
	 *
	 * @param Address $address        	
	 * @return boolean
	 */
	public function isMoscow(Address $address) {
		return !preg_match("/Московская\sобласть/", $address->getParameter('text'));
	}

	/**
	 *
	 * @param Address $address        	
	 * @return string
	 */
	public function getFormattedPhone(Address $address) {
		preg_match("/^\\+7(\\d{3})(\\d+)$/", $address->getPhone(), $matches);
		return $matches[1] . "," . $matches[2];
	}

    public function geocode($address)
    {
        $coordinates = false;

        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

            $url = 'http://geocode-maps.yandex.ru/1.x/?geocode=' . $address;
            curl_setopt($curl, CURLOPT_URL, $url);

            $xml = curl_exec($curl);

            $dom = new \DOMDocument();
            $dom->loadXML($xml);

            $point = $dom->getElementsByTagName('Point')->item(0);
            $pos = $point->getElementsByTagName('pos')->item(0);

            $coordinates = $pos->nodeValue;
        }

        return $coordinates;
    }
    
    public function geocodeAddress($address)
    {
    	$addressText = '';
    
    	if ($curl = curl_init()) {
    		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    
    		$url = 'http://geocode-maps.yandex.ru/1.x/?geocode=' . $address;
    		curl_setopt($curl, CURLOPT_URL, $url);
    
    		$xml = curl_exec($curl);
    
    		$dom = new \DOMDocument();
    		$dom->loadXML($xml);
    		
    		$addressText = $dom->getElementsByTagName('GeocoderMetaData')->item(0)->getElementsByTagName('text')->item(0)->nodeValue;
    	}
    
    	return $addressText;
    }
}

?>