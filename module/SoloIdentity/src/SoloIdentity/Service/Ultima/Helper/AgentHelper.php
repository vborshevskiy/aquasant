<?php

namespace SoloIdentity\Service\Ultima\Helper;

use SoloIdentity\Entity\Ultima\Agent;

class AgentHelper {

	/**
	 *
	 * @param Agent $agent        	
	 * @param integer $length        	
	 * @return string
	 */
	public function getShortName(Agent $agent, $length = 20) {
		$nameParts = explode(" ", $agent->getName());
		$name = "";
		foreach ($nameParts as $np) {
			$tmp = $name . " " . $np;
			if (mb_strlen(trim($tmp), "utf-8") > $length) break;
			$name .= " " . $np;
		}
		$name = trim($name);
		if (mb_strlen(trim($agent->getName()), 'utf-8') > mb_strlen($name, "utf-8")) {
			$name .= ' ...';
		}
		return $name;
	}

}

?>