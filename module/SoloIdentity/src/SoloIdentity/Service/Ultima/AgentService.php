<?php

namespace SoloIdentity\Service\Ultima;

use Solo\ServiceManager\ServiceLocatorAwareService;
use SoloERP\Service\ProvidesWebservice;
use Solo\Stdlib\ClassUtils;
use Solo\ServiceManager\Result;
use SoloERP\WebService\Reader\UltimaObjectReader;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloIdentity\Entity\Ultima\Agent;
use SoloERP\WebService\Reader\UltimaListReader;
use SoloIdentity\Service\Ultima\Result\FindAgentByRequisitesResult;
use SoloIdentity\Entity\Ultima\AgentRequisites;
use SoloIdentity\Service\Ultima\Helper\AgentHelper;

class AgentService extends ServiceLocatorAwareService {

	use ProvidesWebservice;

	/**
	 *
	 * @var AgentHelper
	 */
	private $helper = null;

	/**
	 *
	 * @return \SoloIdentity\Service\Ultima\Helper\AgentHelper
	 */
	public function helper() {
		if (null === $this->helper) {
			$this->helper = new AgentHelper();
		}
		return $this->helper;
	}

	/**
	 *
	 * @return SoloIdentity\Entity\Ultima\Agent
	 */
	public function newAgent() {
		$agent = $this->getServiceLocator()->get('SoloIdentity\\Entity\\Ultima\\Agent');

		// add balance events
		if (ClassUtils::hasTrait($agent, 'SoloIdentity\\Entity\\Ultima\\ProvidesUserBalance')) {
			$agent->events()->attach(
				'balanceExpired',
				function ($e) {
					$agent = $e->getTarget();
					$balanceResult = $this->enumAgentsBalance($agent->getUser()->getId());
					if ($balanceResult->success() && isset($balanceResult->agents) && is_array($balanceResult->agents)) {
						foreach ($balanceResult->agents as $agentBalance) {
							if ($agent->getUser()->agents()->keyExists($agentBalance['agentId'])) {
								$agent->getUser()->agents()[$agentBalance['agentId']]->setBalance($agentBalance['balance']);
							}
						}
					}
				});
		}

		return $agent;
	}

	public function newFakeAgent() {
		$agent = $this->getServiceLocator()->get('SoloIdentity\\Entity\\Ultima\\Agent');
		$agent->isFake(true);
		return $agent;
	}

	/**
	 * Gets agents for specified user
	 *
	 * @param string $securityKey
	 * @return \Solo\ServiceManager\Result
	 */
	public function enumAgents($securityKey = null) {
		$wm = $this->getWebMethod('GetClientAgents');
		$response = $wm->call();

		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$agents = [];
			$rdr = new UltimaJsonListReader($response);
			foreach ($rdr as $item) {
				$obj = $this->newAgent();
				$obj->setId(intval($item['Id']));
				$obj->setName(strval($item['Name']));

				// $obj->setTypeId(intval($item['TypeID']));

				$obj->setBalance(-floatval($item['BalanceAmount']));
				// if ($item['PriceCategoryId']) {
				// $obj->setPriceCategoryId(intval($item['PriceCategoryId']));
				// }

				// $obj->requisites()->setInn($item['Inn'])->setKpp($item['Kpp'])->setOkpo($item['Okpo'])->setSettlementAccount($item['SettlementAccount'])->setBic($item['Bic'])->setBankName($item['BankName'])->setBankCorrAccount($item['BankCorrAccount'])->setEmail($item['Email']);
				$obj->requisites()->setInn($item['Inn'])->setAddress($item['Address']);
				$obj->requisites()->setKpp(strval($item['Kpp']));
				$obj->requisites()->setBic(strval($item['Bic']));
				$obj->requisites()->setBankName(strval($item['BankName']));
				$obj->requisites()->setSettlementAccount(strval($item['SettlementAccount']));
				$obj->requisites()->setBankCorrAccount(strval($item['BankCorrAccount']));
				$obj->requisites()->setAddress(strval($item['Address']));

				$agents[] = $obj;
			}
			$result->agents = $agents;
		}
		return $result;
	}

	/**
	 *
	 * @param Agent $agent
	 * @return \Solo\ServiceManager\Result
	 */
	public function createAgent(Agent $agent) {
		$wm = $this->getWebMethod('CreateAgent');
		$wm->addPar('Name', $agent->getName());
		$wm->addPar('Inn', $agent->requisites()->getInn());
		$wm->addPar('Address', $agent->requisites()->getAddress());
		$wm->addPar('Kpp', $agent->requisites()->getKpp() ? : '');
		$wm->addPar('Opf', $agent->requisites()->getOpf() ? : '');

		// $wm->addPar('Okpo', $agent->requisites()->getOkpo());

		if ($agent->requisites()->getSettlementAccount()) {
			$wm->addPar('SettlementAccount', $agent->requisites()->getSettlementAccount());
		}

		if ($agent->requisites()->getBic()) {
			$wm->addPar('Bic', $agent->requisites()->getBic());
		}

		if ($agent->requisites()->getBankName()) {
			$wm->addPar('BankName', $agent->requisites()->getBankName());
		}

		if ($agent->requisites()->getBankCorrAccount()) {
			$wm->addPar('BankCorrAccount', $agent->requisites()->getBankCorrAccount());
		}
		if ($agent->requisites()->getOgrn()) {
			$wm->addPar('OGRN', $agent->requisites()->getOgrn());
		}
		if ($agent->requisites()->getOgrnDate()) {
			$wm->addPar('OGRNDate', $agent->requisites()->getOgrnDate()->format('Y-m-d H:i:s'));
		}

		$response = $wm->call();
		
		if (false) {
			print '<pre>';
			print json_encode($wm->getPars(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
			print_r($response);
			print '</pre>';
			exit();
		}

		$result = new Result();
		if ($response->hasError()) {
			$errorMessage = null;
			if ($response->hasReadableErrorMessage()) {
				$errorMessage = $response->getErrorMessage();
			}
			$result->setError($response->getError(), $errorMessage);
		} else {
			$rdr = new UltimaJsonListReader($response);
			$agent->setId(intval($rdr->getValue('Id')));

			$rdr = new UltimaJsonListReader($response);
			$result->Success = $rdr->getValue('Success');
			$result->agentId = $rdr->getValue('Id');

			$session = $this->getServiceLocator()->get('user_session');
			$agents = $session->agents;
			$agents[$agent->getId()] = $agent->serialize();
			$session->agents = $agents;
		}
		$result->setOriginalRequest(json_encode($wm->getPars()));
		$result->setOriginalResponse($response->getResponse()->getContent());

		return $result;
	}

	/**
	 *
	 * @param integer $userId
	 * @param integer $agentId
	 * @return \Solo\ServiceManager\Result
	 */
	public function getAgentInfo($userId, $agentId) {
		$wm = $this->getWebMethod('GetBusinessAgentInfo');
		$wm->setUserId($userId);
		$wm->setAgentId($agentId);
		$response = $wm->call();

		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaObjectReader($response);
			$result->accountBankName = $rdr->AccountBankName;
			$result->accountCorrAcount = $rdr->AccountCorrAccount;
			$result->accountSettlementAccount = $rdr->AccountSettlementAccount;
			$result->bic = $rdr->Bic;
			$result->inn = $rdr->INN;
			$result->address = $rdr->JurAddress;
			$result->kpp = $rdr->kpp;
			$result->agentName = $rdr->AgentName;
		}
		return $result;
	}

	/**
	 *
	 * @param string $securityKey
	 * @return \Solo\ServiceManager\Result
	 */
	public function getBonusBalance($securityKey = null) {
		$wm = $this->getWebMethod('GetClientBonusBalance');
		// $wm->addPar('SecurityKey', $securityKey);
		$response = $wm->call();

		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$result->bonusBalance = floatval($response->getData()->Value);
		}
		return $result;
	}

	/**
	 *
	 * @param integer $userId
	 * @return \Solo\ServiceManager\Result
	 */
	public function enumAgentsBalance($userId) {
		$wm = $this->getWebMethod('GetUserAgents');
		$wm->setUserId($userId);
		$response = $wm->call();

		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$agents = [];
			$rdr = new UltimaListReader($response);
			foreach ($rdr as $item) {
				$agents[] = [
					'agentId' => intval($item['AgentID']),
					'balance' => -floatval($item['AgentBalance'])
				];
			}
			$result->agents = $agents;
		}
		return $result;
	}

	/**
	 *
	 * @param string $inn
	 * @param string $kpp
	 * @param string $okpo
	 * @return \SoloIdentity\Service\Ultima\Result\FindAgentByRequisitesResult
	 */
	public function findByRequisites($inn, $kpp = '', $okpo = '') {
		$wm = $this->getWebMethod('FindAgentsByRequisite');
		$wm->setInn($inn);
		$wm->setKpp($kpp);
		$wm->setOkpo($okpo);
		$response = $wm->call();

		$result = new FindAgentByRequisitesResult();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaObjectReader($response);
			$obj = new AgentRequisites();
			$obj->setInn($rdr->INN);
			$obj->setKpp($rdr->KPP);
			$obj->setOkpo($rdr->OKPO);
			$obj->setBic($rdr->Bic);
			$obj->setAddress($rdr->JurAddress);
			$obj->setAccountBankName($rdr->AccountBankName);
			$obj->setAccountSettlementAccount($rdr->AccountSettlementAccount);
			$obj->setAccountCorrAccount($rdr->AccountCorrAccount);

			$result->requisites = $obj;
			$result->userId = $rdr->SiteLoginID;
			$result->agentId = $rdr->AgentID;
			$result->agentName = $rdr->AgentName;
		}
		return $result;
	}

	/**
	 *
	 * @param integer $agentId
	 * @return \Solo\ServiceManager\Result
	 */
	public function checkIsLinkedToUser($agentId) {
		$wm = $this->getWebMethod('CheckLinkAgentToUserJB');
		$wm->setAgentId($agentId);
		$response = $wm->call();

		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$result->status = intval($response->getData());
		}
		return $result;
	}

}

?>