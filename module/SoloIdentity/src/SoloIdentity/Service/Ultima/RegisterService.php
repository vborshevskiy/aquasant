<?php

namespace SoloIdentity\Service\Ultima;

use SoloERP\Service\ProvidesWebservice;
use SoloERP\WebService\Reader\UltimaObjectReader;
use SoloIdentity\Service\Ultima\Result\RegisterResult;
use Solo\ServiceManager\ServiceLocatorAwareService;
use SoloIdentity\Entity\Ultima\ProvidesFakeAgent;
use SoloIdentity\Entity\Phone;

class RegisterService extends ServiceLocatorAwareService {

    use ProvidesWebservice;

    /**
     *
     * @param string $phone        	
     * @param string $email        	
     * @param string $fio        	
     * @param boolean $sendSms        	
     * @return \SoloIdentity\Service\Ultima\Result\RegisterResult
     */
    public function register($phone, $email, $fio, $sendSms = true) {           
        $wm = $this->getWebMethod('CreateLogin');
        $wm->setPhone(Phone::createFromString($phone)->toString('7{code}{number}'));
        $wm->setEmail($email);
        $wm->setFio($fio);
        $wm->setSendSms($sendSms);        
        $response = $wm->call();        
        $result = new RegisterResult();     
        if ($response->hasError()) {
            $result->setError($response->getError());
        } else {
            $rdr = new UltimaObjectReader($response);            
            $result->userId = $rdr->userID;                      
        }
        return $result;
    }

    /**
     * Create anonymous user
     * 
     * @return \SoloIdentity\Service\Ultima\Result\RegisterResult
     */
    public function registerFakeUser() {
        return $this->register(ProvidesFakeAgent::getFakePhone(), ProvidesFakeAgent::getFakeEmail(), ProvidesFakeAgent::getFakeFio(), false);
    }

}

?>