<?php

namespace SoloIdentity\Service\Ultima;

use SoloERP\Service\ProvidesWebservice;
use Solo\ServiceManager\Result;
use SoloIdentity\Entity\Ultima\Address;
use Solo\ServiceManager\ServiceLocatorAwareService;
use SoloERP\WebService\Reader\UltimaListReader;
use SoloIdentity\Service\Ultima\Result\DeleteAddressResult;
use SoloIdentity\Service\Ultima\Result\CreateAddressResult;
use SoloERP\WebService\Reader\UltimaObjectReader;
use SoloIdentity\Service\Ultima\Helper\AddressHelper;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloIdentity\Entity\Ultima\AddressInterface;
use SoloIdentity\Entity\Ultima\DeliveryAddresses;
use SoloIdentity\Entity\Ultima\PostDeliveryAddresses;
use SoloIdentity\Entity\Ultima\PostPickupAddresses;
use SoloIdentity\Entity\Phone;

class AddressService extends ServiceLocatorAwareService implements \Zend\EventManager\EventManagerAwareInterface {

    use ProvidesWebservice,
        \Solo\EventManager\ProvidesEvents;

    /**
     *
     * @var AddressHelper
     */
    private $helper = null;

    /**
     *
     * @return \SoloIdentity\Service\Ultima\Helper\AddressHelper
     */
    public function helper() {
        if (null === $this->helper) {
            $this->helper = new AddressHelper();
        }
        return $this->helper;
    }

    /**
     *
     * @param AddressInterface $address
     * @param integer $agentId
     * @throws \InvalidArgumentException
     * @return \Solo\ServiceManager\Result
     */
    public function createAddress(AddressInterface &$address, $agentId = null) {
        if (!$address->getAddress()) {
            throw new \InvalidArgumentException('Address doesn\'t have address');
        }
        if ($address instanceof DeliveryAddresses) {
            if (!$address->getLongitude()) {
                throw new \InvalidArgumentException('Address doesn\'t have longitude');
            }
            if (!$address->getLatitude()) {
                throw new \InvalidArgumentException('Address doesn\'t have latitude');
            }
        } elseif ($address instanceof PostDeliveryAddresses) {
            if (!$address->getTransportCompanyUid()) {
                throw new \InvalidArgumentException('Address doesn\'t have transport company uid');
            }
        } elseif ($address instanceof PostPickupAddresses) {
            if (!$address->getPekWarehouseId()) {
                throw new \InvalidArgumentException('Address doesn\'t have pek warehouse id');
            }
        }

        $session = $this->getServiceLocator()->get('user_session');

        if (!isset($session->user)) {
            $session = $this->getServiceLocator()->get('fake_user_session');
        }

        $adresses = $session->adresses;

        if ($adresses) {
            foreach ($adresses as $item) {
                if ($item instanceof DeliveryAddresses && $address instanceof DeliveryAddresses && $item->getLongitude() == $address->getLongitude() && $item->getLatitude() == $address->getLatitude() && $item->getFloor() == $address->getFloor()) {
                    return $item;
                }
                if ($item instanceof PostDeliveryAddresses && $address instanceof PostDeliveryAddresses && $item->getAddress() == $address->getAddress() && $item->getFloor() == $address->getFloor()) {
                    return $item;
                }
                if ($item instanceof PostPickupAddresses && $address instanceof PostPickupAddresses && $item->getPekWarehouseId() == $address->getPekWarehouseId() && $item->getFloor() == $address->getFloor()) {
                    return $item;
                }
            }
        }

        $wm = $this->getWebMethod('CreateDeliveryAddress');
        if (null !== $agentId) {
            $wm->addPar('AgentId', $agentId);
        }

        $wm->addPar('Address', $address->getAddress());
		$wm->addPar('Floor', $address->getFloor());
		$wm->addPar('ElevatorTypeID', $address->getElevatorTypeId());

        if ($address instanceof DeliveryAddresses) {
            $wm->addPar('Longitude', $address->getLongitude());
            $wm->addPar('Latitude', $address->getLatitude());
        } elseif ($address instanceof PostDeliveryAddresses) {
            $wm->addPar('Longitude', -1);
            $wm->addPar('Latitude', -1);
            $wm->addPar('RegionCode', $address->getRegionId());
            $wm->addPar('Index', $address->getIndex());
            if (is_int($address->getSdekId())) {
                $wm->addPar('SdekCode', $address->getSdekId());
            }
            if (is_int($address->getPekId())) {
                $wm->addPar('PekCode', $address->getPekId());
            }
        } elseif ($address instanceof PostPickupAddresses) {
            $wm->addPar('Longitude', -1);
            $wm->addPar('Latitude', -1);
            $wm->addPar('RegionCode', $address->getRegionId());
            $wm->addPar('PecWarehouseId', $address->getPekWarehouseId());
        }
        if ($address->getPhone()) {
            $wm->addPar('Phone', $address->getPhone());
        }
        if ($address->getUserName()) {
            $wm->addPar('UserName', $address->getUserName());
        }
//         print '<h1>Don\'t panic. Debugging</h1>';
//         print '<pre>'.json_encode($wm->getPars(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT).'</pre>';
//         exit();
		//mail('v.detman@gmail.com', 'santbaza.ru: create address '.date("H:i"), json_encode($wm->getPars(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
        $response = $wm->call();

        $newAddress = new UltimaJsonListReader($response);
        $result = new CreateAddressResult();
        if ($response->hasError()) {
            $result->setError($response->getError());
        } else {
            $result->setData($response->getData());
            $address->setId((int)$newAddress->getValue('Id'));
            $adresses[$address->getId()] = $address;
            $session->adresses = $adresses;
        }
        return $address;
    }

    public function updateAddress(DeliveryAddresses &$address, $securityKey, $agentId = null, $newAgentId = null) {
        if (!$address->getAddress()) {
            throw new \InvalidArgumentException('Address doesn\'t have address');
        }
        if (!$address->getLongitude()) {
            throw new \InvalidArgumentException('Address doesn\'t have post longitude');
        }
        if (!$address->getLatitude()) {
            throw new \InvalidArgumentException('Address doesn\'t have linked latitude');
        }
        $wm = $this->getWebMethod('UpdateDeliveryAddress');

        if (null !== $agentId) {
            $wm->addPar('AgentId', $agentId);
        }

        if (null !== $newAgentId) {
            $wm->addPar('NewAgentId', $newAgentId);
        }

        $wm->addPar('SecurityKey', $securityKey);
        $wm->addPar('Id', $address->getId());
        $wm->addPar('Address', $address->getAddress());
        $wm->addPar('Longitude', $address->getLongitude());
        $wm->addPar('Latitude', $address->getLatitude());
		$wm->addPar('Floor', $address->getFloor());
		$wm->addPar('ElevatorTypeID', 0);
        if ($address->getPhone())
            $wm->addPar('Phone', Phone::createFromString($address->getPhone())->toString('7{code}{number}'));
        if ($address->getUserName())
            $wm->addPar('UserName', $address->getUserName());

        $response = $wm->call();
        $rdr = new UltimaJsonListReader($response);
        $result = new CreateAddressResult();
        if ($response->hasError()) {
            $result->setError($response->getError());
        } else {
            $result->setData($response->getData());
            //$address->setId($rdr->getValue('SecurityKey'));

            $session = $this->getServiceLocator()->get('user_session');
            if (!isset($session->user))
                $session = $this->getServiceLocator()->get('fake_user_session');
            $adresses = $session->adresses;
            $adresses[$address->getId()] = $address;
            $session->adresses = $adresses;
        }
        return $address;
    }

    /**
     *
     * @param integer $agentId
     * @param integer $addressId
     * @return \SoloIdentity\Service\Ultima\Result\DeleteAddressResult
     */
    public function deleteAddress($addressId, $securityKey, $agentId = null) {
        $wm = $this->getWebMethod('DeleteDeliveryAddress');
        $wm->addPar('SecurityKey', $securityKey);
        $wm->addPar('Id', $addressId);
        if ($agentId)
            $wm->addPar('AgentId', $agentId);
        $response = $wm->call();

        $result = new DeleteAddressResult();
        if ($response->hasError()) {
            $result->setError($response->getError());
        } else {

            $session = $this->getServiceLocator()->get('user_session');
            if (!isset($session->user))
                $session = $this->getServiceLocator()->get('fake_user_session');
            $adresses = $session->adresses;
            unset($adresses[$addressId]);
            $session->adresses = $adresses;
        }
        return $result;
    }

    /**
     * Gets addresses for specified agentId
     *
     * @param integer $agentId
     * @return \Solo\ServiceManager\Result
     */
    public function enumAddresses($agentId) {
        $wm = $this->getWebMethod('GetAddressList');
        $wm->setAgentId($agentId);
        $response = $wm->call();

        $result = new Result();

        if ($response->hasError()) {
            $result->setError($response->getError());
        } else {
            $addresses = [];
            $rdr = new UltimaListReader($response);
            foreach ($rdr as $item) {
                $obj = $this->emptyAddress();
                $obj->setId(intval($item['AddressID']));
                $obj->setPostIndex($item['PostIndex']);
                $obj->setPhone($item['Phone']);
                $obj->setDescription($item['Description']);

                if (isset($item['DescrJSON']) && !empty($item['DescrJSON'])) {
                    $parameters = json_decode($item['DescrJSON'], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                    foreach ($parameters as $paramName => $param) {
                        $obj->setParameter($paramName, $param);
                    }
                }

                $addresses[] = $obj;
            }
            $result->addresses = $addresses;
        }
        return $result;
    }

    /**
     *
     * @param integer $agentId
     * @param integer $addressId
     * @return \Solo\ServiceManager\Result
     */
    function getDeletedAddress($agentId, $addressId) {
        $wm = $this->getWebMethod('GetDeletedAgentAddress');
        $wm->setAgentId($agentId);
        $wm->setAddressId($addressId);
        $response = $wm->call();

        $result = new Result();
        if ($response->hasError()) {
            $result->setError($response->getError());
        } else {
            $rdr = new UltimaObjectReader($response);
            $obj = new Address();
            $obj->setId(intval($rdr->AddressID));
            $obj->setPostIndex($rdr->PostIndex);
            $obj->setPhone($rdr->Phone);
            $obj->setDescription($rdr->Description);
            array_walk(json_decode($rdr->DescrJSON, true), function ($value, $name) use($obj) {
                $obj->setParameter($name, $value);
            });

            $result->address = $obj;
        }
        return $result;
    }

    /**
     *
     * @return \SoloIdentity\Entity\Ultima\Address
     */
    public function emptyAddress() {
        return $this->getServiceLocator()->get('address_instance');
    }

    public function getDeliveryAdresses($securityKey = null, $agentId = null) {
        $items = [];
        $wm = $this->getWebMethod('GetDeliveryAddresses');
		//$wm->addPar('SecurityKey', $securityKey);
		//$wm->addPar('AgentId', $agentId);
        $response = $wm->call();

        $result = new Result();

        if ($response->hasError()) {
            $result->setError($response->getError());
        } else {
            $rdr = new UltimaJsonListReader($response);
            foreach ($rdr as $adress) {
                if ($adress['Latitude'] >= 0 && $adress['Longitude'] >= 0) {
                    $item = new DeliveryAddresses();
                } elseif ($adress['PecWarehouseId']) {
                    $item = new PostPickupAddresses();
                } else {
                    $item = new PostDeliveryAddresses();
                }
                $item->setId(intval($adress['Id']));
                $item->setAddress($adress['Address']);
				$item->setFloor($adress['Floor']);
                $item->setUserName($adress['UserName']);
                $item->setPhone($adress['Phone']);
                if ($item instanceof DeliveryAddresses) {
                    $item->setLatitude($adress['Latitude']);
                    $item->setLongitude($adress['Longitude']);
                } elseif ($item instanceof PostDeliveryAddresses) {
                    $item->setRegionId($adress['RegionCode']);
//                    if (isset($adress['Index'])) {
//                        $item->setIndex($adress['Index']);
//                    }
                } elseif ($item instanceof PostPickupAddresses) {
                    $item->setPekWarehouseId($adress['PecWarehouseId']);
                }

                $items[$adress['Id']] = $item;
            }
            $result->items = $items;
        }
        return $result;
    }

    public function getTextAddress($town, $street, $room) {
        $addressArray = [];
        if (!empty($town)) {
            $addressArray[] = $town;
        }
        $addressArray[] = $street;
        if (!empty($room)) {
            $addressArray[] = 'кв/офис ' . $room;
        }
        return implode(', ', $addressArray);
    }

    public function createDeliveryAddressInstance($fullAddress, $floor = null) {
        $address = new DeliveryAddresses();

        $coordinates = $this->helper()->geocode($fullAddress);

        list($longitude, $latitude) = explode(' ', $coordinates);

        $address->setLatitude($latitude);
        $address->setLongitude($longitude);
        $address->setAddress($fullAddress);

		if (null !== $floor) {
			$address->setFloor(intval($floor));
		}

        return $address;
    }

    /**
     *
     * @param string $transportCompanyUid
     * @param string $addressText
     * @param string $phoneCode
     * @param string $phoneNumber
     * @param string $userName
     * @param \SoloDelivery\Entity\PostDeliveryRegion $region
     * @param string index
     * @return PostDeliveryAddresses
     */
    public function createPostDeliveryAddressInstance($transportCompanyUid, $addressText, $phoneCode, $phoneNumber, $userName, $region, $index) {
        $address = new PostDeliveryAddresses();
        $address->setTransportCompanyUid($transportCompanyUid);
        $address->setAddress($addressText);
        $address->setPhoneCode($phoneCode);
        $address->setPhoneNumber($phoneNumber);
        $address->setUserName($userName);
        $address->setRegionId($region->getId());
        $address->setSdekId($region->getSdekId());
        $address->setPekId($region->getPekId());
        $address->setIndex($index);
        return $address;
    }

    /**
     *
     * @param \SoloDelivery\Entity\PekWarehouse $pekWarehouse
     * @param \SoloIdentity\Entity\AbstractUser $user
     * @param integer $regionId
     * @param string|null $transportCompanyUid
     * @return PostPickupAddresses
     */
    public function createPostPickupAddressInstance(\SoloDelivery\Entity\PekWarehouse $pekWarehouse, \SoloIdentity\Entity\AbstractUser $user, $regionId, $transportCompanyUid = 'pecom') {
        $address = new PostPickupAddresses();
        $address->setTransportCompanyUid($transportCompanyUid);
        $address->setAddress($pekWarehouse->warehouseAddress);
        $address->setPhoneCode($user->getPhoneCode());
        $address->setPhoneNumber($user->getPhoneNumber());
        $address->setUserName($user->getName());
        $address->setRegionId($regionId);
        $address->setPekWarehouseId($pekWarehouse->warehousePekID);
        return $address;
    }

}
