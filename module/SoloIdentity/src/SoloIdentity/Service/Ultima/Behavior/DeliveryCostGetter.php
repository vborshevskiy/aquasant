<?php
namespace SoloIdentity\Service\Ultima\Behavior;

/**
 * Description of DeliveryCostGetter
 *
 * @author slava
 */
class DeliveryCostGetter extends \Solo\ServiceManager\ServiceLocatorAwareService implements GetDeliveryCostBehavior {
    
    /**
     * @var \SoloIdentity\Data\DeliveryCostGateway
     */
    protected $deliveryCostGateway = null;
    
    /**
     * 
     * @return \SoloIdentity\Data\DeliveryCostGateway
     */
    public function getDeliveryCostGateway() {
        return $this->deliveryCostGateway;
    }

    /**
     * 
     * @param \SoloIdentity\Data\DeliveryCostGateway $deliveryCostGateway
     */
    public function setDeliveryCostGateway(\SoloIdentity\Data\DeliveryCostGateway $deliveryCostGateway) {
        $this->deliveryCostGateway = $deliveryCostGateway;
    }
   
    public function getDeliveryCostsByPostIndex($postIndex) {
        return $this->getDeliveryCostGateway()->getDeliveryCostsByPostIndex($postIndex);
    }
    
    public function __sleep() {
        return array();
    }
    
    public function __wakeup() {
        $gateway = new \SoloIdentity\Data\DeliveryCostGateway;
        $this->deliveryCostGateway = $gateway;
    }
    
}

?>
