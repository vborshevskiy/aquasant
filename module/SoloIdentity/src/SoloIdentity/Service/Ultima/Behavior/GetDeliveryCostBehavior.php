<?php
namespace SoloIdentity\Service\Ultima\Behavior;

/**
 *
 * @author slava
 */
interface GetDeliveryCostBehavior {
    
    public function getDeliveryCostsByPostIndex($postIndex);
    
}

?>
