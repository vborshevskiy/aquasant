<?php

namespace SoloIdentity\Service\Ultima\Result;

use Solo\ServiceManager\Result;

final class UserUpdatePasswordResult extends Result {

	/**
	 * Specified password not equals current password
	 *
	 * @var integer
	 */
	const ERROR_INVALID_CURRENT_PASSWORD = 110;

}

?>