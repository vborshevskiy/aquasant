<?php

namespace SoloIdentity\Service\Ultima\Result;

use Solo\ServiceManager\Result;

final class UserPasswordResetResult extends Result {

	/**
	 * Specified password not equals current password
	 *
	 * @var integer
	 */
	const ERROR_INVALID_DATA = 500;

}

?>