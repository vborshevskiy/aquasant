<?php

namespace SoloIdentity\Service\Ultima\Result;

use Solo\ServiceManager\Result;

final class RecoveryByPhoneResult extends Result {

	/**
	 * User with specified phone not exists
	 *
	 * @var integer
	 */
	const ERROR_PHONE_NOT_EXISTS = 106;

	/**
	 * Undefined error occurs
	 *
	 * @var integer
	 */
	const ERROR_UNDEFINED = 107;

}

?>