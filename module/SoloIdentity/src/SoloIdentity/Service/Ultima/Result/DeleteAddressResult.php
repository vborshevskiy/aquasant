<?php

namespace SoloIdentity\Service\Ultima\Result;

use Solo\ServiceManager\Result;

final class DeleteAddressResult extends Result {

	const ERROR_ADDRESS_NOT_BELONG_TO_KA = 301; // данному КА не принадлежит данный адрес доставки
	const ERROR_UNDEFINED = 302; // неидентифицируемая ошибка удаления адреса
}

?>