<?php

namespace SoloIdentity\Service\Ultima\Result;

use Solo\ServiceManager\Result;

final class UserUpdateResult extends Result {

	/**
	 * Specified email already registered in another user
	 *
	 * @var integer
	 */
	const ERROR_EMAIL_ALREADY_EXISTS = 100;

	/**
	 * Specified phone number already registerd in another user
	 *
	 * @var integer
	 */
	const ERROR_PHONE_ALREADY_REGISTERED = 101;
        
        const ERROR = 500;

}

?>