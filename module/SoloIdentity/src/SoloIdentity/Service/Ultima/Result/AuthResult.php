<?php

namespace SoloIdentity\Service\Ultima\Result;

use Solo\ServiceManager\Result;

final class AuthResult extends Result {

	/**
	 * Specified credentials not exists
	 *
	 * @var integer
	 */
	const ERROR_LOGIN_PASS_PAIR_NOT_EXISTS = 204;

	/**
	 * Undefinied error occurs
	 *
	 * @var integer
	 */
	const ERROR_UNDEFINED = 500;

}

?>