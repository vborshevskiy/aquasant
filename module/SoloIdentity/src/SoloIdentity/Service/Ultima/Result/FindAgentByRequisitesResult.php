<?php

namespace SoloIdentity\Service\Ultima\Result;

use Solo\ServiceManager\Result;

final class FindAgentByRequisitesResult extends Result {

	/**
	 * Agent with specified requisites not found
	 *
	 * @var integer
	 */
	const ERROR_AGENT_NOT_FOUND = 204;

}

?>