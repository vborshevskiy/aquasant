<?php

namespace SoloIdentity\Service\Ultima\Result;

use Solo\ServiceManager\Result;

final class CreateAddressResult extends Result {

	/**
	 * Address with specified address line already exists
	 *
	 * @var integer
	 */
	const ERROR_ADDRESS_ALREADY_EXISTS = 303;

	/**
	 * Incorrect / not existing post index
	 *
	 * @var integer
	 */
	const ERROR_BAD_POST_INDEX = 304;

	/**
	 * Unidentified error while address creating
	 *
	 * @var integer
	 */
	const ERROR_UNDEFINED = 305;

}

?>