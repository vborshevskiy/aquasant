<?php

namespace SoloIdentity\Service\Ultima\Result;

use Solo\ServiceManager\Result;

final class RegisterResult extends Result {
	const ERROR_PHONE_ALREADY_REGISTERED = 100; // Email уже зарегистрирован
	const ERROR_UNDEFINED = 500; // неидентифицируемая ошибка создания логина
}
?>
