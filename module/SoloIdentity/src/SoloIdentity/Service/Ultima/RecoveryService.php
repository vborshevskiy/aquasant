<?php

namespace SoloIdentity\Service\Ultima;

use SoloERP\Service\ProvidesWebservice;
use Solo\ServiceManager\ServiceLocatorAwareService;
use Solo\ServiceManager\Result;
use SoloIdentity\Service\Ultima\Result\RecoveryByPhoneResult;

class RecoveryService extends ServiceLocatorAwareService {
	
	use ProvidesWebservice;

	/**
	 *
	 * @return \Solo\ServiceManager\Result
	 */
	public function generatePassword() {
		$response = $this->getWebMethod('GeneratePasswordJB');
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$result->password = $response->getData();
		}
		return $result;
	}

	/**
	 *
	 * @param string $phone        	
	 * @return \SoloIdentity\Service\Ultima\Result\RecoveryByPhoneResult
	 */
	public function recoveryByPhone($phone) {
		$wm = $this->getWebMethod('RestorePass');
		$wm->setPhone($phone);
		$response = $wm->call();
		$result = new RecoveryByPhoneResult();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$result->password = $response->getData();
		}
		return $result;
	}

}

?>