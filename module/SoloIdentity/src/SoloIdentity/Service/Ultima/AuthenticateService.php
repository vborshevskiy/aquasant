<?php

namespace SoloIdentity\Service\Ultima;

use SoloERP\Service\ProvidesWebservice;
use SoloIdentity\Service\Ultima\Result\AuthResult;
use Solo\ServiceManager\ServiceLocatorAwareService;

class AuthenticateService extends ServiceLocatorAwareService {

	use ProvidesWebservice;

	/**
	 *
	 * @param mixed $login
	 * @param string $password
	 * @throws \RuntimeException
	 * @return \SoloIdentity\Service\Ultima\Result\AuthResult
	 */
	public function authenticate($login, $password = null, $reserveId = null) {
		$logManager = $this->getServiceLocator()->get('\\SoloLog\\Service\\LogManager');
		$logger = $logManager->create('auth_logger');
		$result = new AuthResult();
		$userResult = $this->getServiceLocator()->get(__NAMESPACE__ . '\\UserService')->getUserByLoginAndPassword($login, $password, $reserveId);
		
		// by phone
		if (!$userResult->success()) {
			if (false !== stripos($login, '+')) {
				$phone = str_replace('+', '', $login);
				$userResult = $this->getServiceLocator()->get(__NAMESPACE__ . '\\UserService')->getUserByLoginAndPassword($phone, $password, $reserveId);
			}
		}

		$logger->info('Метод SignInClientWithPhone');
		$logger->info('Входные: ' . $userResult->getOriginalRequest());
		$logger->info('Выходные: ' . $userResult->getOriginalResponse());
		$logger->info('Статус ошибки: ' . $userResult->getErrorCode());
		// if ($userResult->success() && isset($userResult->securityKey)) {
		if ($userResult->success()) {
			// load ClientInfo
			$infoResult = $this->getServiceLocator()->get(__NAMESPACE__ . '\\UserService')->GetClientInfo();
			if ($infoResult->success()) {
				$user = $infoResult->user;
				if ($user->isManager()) {
					$user->setPassword($password);
				}
				$result->user = $user;
			}
			$logger->info('Метод GetClientInfo');
			$logger->info('Входные: ' . $infoResult->getOriginalRequest());
			$logger->info('Выходные: ' . $infoResult->getOriginalResponse());
		} else {
			$result->setError($userResult->getErrorCode());
		}

		return $result;
	}

}

?>