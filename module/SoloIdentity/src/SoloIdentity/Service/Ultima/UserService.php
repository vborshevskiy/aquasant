<?php

namespace SoloIdentity\Service\Ultima;

use Solo\ServiceManager\ServiceLocatorAwareService;
use SoloERP\Service\ProvidesWebservice;
use Solo\Stdlib\ClassUtils;
use Solo\ServiceManager\Result;
use SoloERP\WebService\Reader\UltimaObjectReader;
use SoloERP\WebService\Reader\UltimaJsonListReader;
use SoloIdentity\Entity\Ultima\User;
use SoloIdentity\Service\Ultima\Result\UserUpdateResult;
use SoloIdentity\Service\Ultima\Result\UserPasswordResetResult;
use SoloIdentity\Service\Ultima\Result\UserUpdatePasswordResult;
use SoloIdentity\Service\Ultima\Result\AuthResult;
use SoloIdentity\Service\Ultima\Helper\UsersHelper;
use SoloIdentity\Service\Ultima\Result\RegisterResult;
use SoloIdentity\Entity\Ultima\ProvidesFakeAgent;
use SoloIdentity\Entity\Phone;

class UserService extends ServiceLocatorAwareService {
	
	use ProvidesWebservice;

	/**
	 *
	 * @var UsersHelper
	 */
	private $helper = null;

	/**
	 *
	 * @return \SoloIdentity\Service\Ultima\Helper\UsersHelper
	 */
	public function helper() {
		if (null === $this->helper) {
			$this->helper = new UsersHelper();
		}
		return $this->helper;
	}

	public function register($phone, $password = null, $email = null, $firstName = null, $lastName = null) {
		$fake = FALSE;
		if ($phone == ProvidesFakeAgent::getFakePhone()) $fake = TRUE;
		$result = new RegisterResult();
		// проверка доступности email
		if (!$fake) {
			$wm = $this->getWebMethod('IsClientExists');
			$wm->addPar('Phone', $phone);
			$response = $wm->call();
			if ($response->hasError()) {
				$result->setError($response->getError());
			} else {
				$rdr = new UltimaJsonListReader($response);
			}
		}
		if ($fake || !$rdr->getValue("Exists")) {
			$wm = $this->getWebMethod('CreateClient');
			$wm->addPar('Phone', Phone::createFromString($phone)->toString('7{code}{number}'));
			if (!empty($firstName)) {
				$wm->addPar('FirstName', $firstName);
			}
			if (!empty($lastName)) {
				$wm->addPar('LastName', $lastName);
			}
			
			if (!empty($password)) {
				$wm->addPar('Password', $password);
			}
			
			if (!empty($name)) $wm->addPar('Name', $name);
			if (!empty($email)) $wm->addPar('Email', $email);
			
			if (!empty($parentClientId)) $wm->addPar('ParentMlmClientId', $parentClientId);
			if (!empty($socialId)) $wm->addPar('SocialId', $socialId);
			if (!empty($socialCode)) $wm->addPar('SocialCode', $socialCode);
			
			$response = $wm->call();
			
			if ($response->hasError()) {
				$result->setError($response->getError());
			} else {
				$rdr = new UltimaJsonListReader($response);
				$result->userId = $rdr->getValue('Id');
				$result->SecurityKey = $rdr->getValue('SecurityKey');
				$result->Password = $rdr->getValue('Password');
			}
		} else {
			$result->setError(RegisterResult::ERROR_PHONE_ALREADY_REGISTERED);
		}
		return $result;
	}

	/**
	 * Create anonymous user
	 *
	 * @return \SoloIdentity\Service\Ultima\Result\RegisterResult
	 */
	public function registerFakeUser() {
		return $this->register(
			ProvidesFakeAgent::getFakePhone(), 
			ProvidesFakeAgent::getFakeAgentEmail(), 
			ProvidesFakeAgent::getFakePassword(), 
			ProvidesFakeAgent::getFakeFirstName(), 
			ProvidesFakeAgent::getFakeLastName());
	}

	/**
	 *
	 * @return SoloIdentity\Entity\Ultima\User
	 */
	public function newUser() {
		$user = $this->getServiceLocator()->get('SoloIdentity\\Entity\\Ultima\\User');
		// add balance events
		if (ClassUtils::hasTrait($user, 'SoloIdentity\\Entity\\Ultima\\ProvidesUserBalance')) {
			$user->events()->attach(
				'bonusBalanceExpired', 
				function ($e) {
					$user = $e->getTarget();
					$bonusBalanceResult = $this->getServiceLocator()->get(__NAMESPACE__ . '\\AgentService')->getBonusBalance($user->getSecurityKey());
					if ($bonusBalanceResult->success() && isset($bonusBalanceResult->bonusBalance)) {
						$user->setBonusBalance($bonusBalanceResult->bonusBalance);
					}
				});
		}
		return $user;
	}

	public function unlinkAgentFromClient($securityKey, $id) {
		$wm = $this->getWebMethod('UnlinkAgentFromClient');
		$wm->addPar('SecurityKey', $securityKey);
		$wm->addPar('Id', $id);
		$response = $wm->call();
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaJsonListReader($response);
			$result->Success = $rdr->getValue('Success');
		}
		return $result;
	}

	public function linkAgentToClient($securityKey, $id) {
		$wm = $this->getWebMethod('LinkAgentToClient');
		$wm->addPar('SecurityKey', $securityKey);
		$wm->addPar('Id', $id);
		$response = $wm->call();
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaJsonListReader($response);
			$result->Success = $rdr->getValue('Success');
		}
		return $result;
	}

	public function GetClientAgents($securityKey, $includeBalance = FALSE) {
		$wm = $this->getWebMethod('GetClientAgents');
		$wm->addPar('SecurityKey', $securityKey);
		// $wm->addPar('IncludeBalance', $IncludeBalance);
		$response = $wm->call();
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaJsonListReader($response);
			$result->Success = $rdr->getValue('Success');
		}
		return $result;
	}

	/**
	 *
	 * @param string $login        	
	 * @param string $password        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getUserByLoginAndPassword($login, $password, $reserveId = null) {
		if (is_numeric($login)) {
			$wm = $this->getWebMethod('SignInClientWithPhone');
			$wm->addPar('Phone', Phone::createFromString($login)->toString('7{code}{number}'));
		} else {
			$wm = $this->getWebMethod('SignInClientWithEmail');
			$wm->addPar('Email', $login);
		}
		
		if ($password !== false) {
			$wm->addPar('PasswordRequired', true);
			$wm->addPar('Password', $password);
		}
		
		$response = $wm->call();
		
		$result = new AuthResult();
		$result->setOriginalRequest(json_encode($wm->getPars()));
		$result->setOriginalResponse($response->getResponse()->getContent());
		
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaJsonListReader($response);
			$result->Success = (bool)$rdr->getValue('Success');
			
			if (!$result->Success) $result->setError(AuthResult::ERROR_LOGIN_PASS_PAIR_NOT_EXISTS);
			// elseif($reserveId) {
			// $wm = $this->getWebMethod('SendReserveNotify');
			// $wm->addPar('Id', $reserveId);
			// $response = $wm->call();
			// }
		}
		return $result;
	}

	/**
	 * 
	 * @param integer $clientId
	 * @return \Solo\ServiceManager\Result
	 */
	public function getClientInfo($clientId = null) {
		$wm = $this->getWebMethod('GetClientInfo');
		if (null !== $clientId) {
			$wm->addPar('AgentId', $clientId);
		}
		$response = $wm->call();
		$result = new Result();
		$result->setOriginalRequest(json_encode($wm->getPars()));
		$result->setOriginalResponse($response->getResponse()->getContent());
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaJsonListReader($response);
			$user = $this->newUser();
			$user->setId($rdr->getValue('Id'));
			$user->setEmail($rdr->getValue('Email'));
			$user->setPhone($rdr->getValue('Phone'));
			$user->setName($rdr->getValue('FirstName'));
			if ($rdr->getValue('ManagerID')) {
				$user->setManagerId($rdr->getValue('ManagerID'));
				$user->isManager(true);
			}
			if ($rdr->getValue('IsManager')) {
				$user->isManager((bool)$rdr->getValue('IsManager'));
			}
			if ($rdr->getValue('IsSuperManager')) {
				$user->isSuperManager((bool)$rdr->getValue('IsSuperManager'));
			}
			
			$result->user = $user;
		}
		return $result;
	}

	/**
	 *
	 * @param string $phone        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getUserInfoByPhone($phone) {
		$wm = $this->getWebMethod('GetBusinessLoginDebug');
		$wm->setPhone($phone);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getErrorCode());
		} else {
			$rdr = new UltimaObjectReader($response);
			$user = $this->newUser();
			$user->setId(intval($rdr->ID));
			$user->setName(strval($rdr->Name));
			$user->setPassword(strval($rdr->Password));
			$user->setEmail(strval($rdr->Email));
			$user->setPhone(strval($rdr->Phone));
			$user->setTypeId(intval($rdr->LoginTypeID));
			$user->setNaturalPersonId(intval($rdr->NaturalPersonID));
			if ($rdr->PriceCategoryID) {
				$user->setPriceCategoryId(intval($rdr->PriceCategoryID));
			}
			$result->user = $user;
		}
		return $result;
	}

	/**
	 *
	 * @param integer $userId        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getUserInfoById($userId) {
		$wm = $this->getWebMethod('GetUserById');
		$wm->setUserId($userId);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getErrorCode());
		} else {
			$rdr = new UltimaObjectReader($response);
			$user = $this->newUser();
			$user->setId(intval($rdr->ID));
			$user->setName(strval($rdr->Name));
			$user->setPassword(strval($rdr->Password));
			$user->setEmail(strval($rdr->Email));
			$user->setPhone(strval($rdr->Phone));
			$user->setTypeId(intval($rdr->LoginTypeID));
			$user->setNaturalPersonId(intval($rdr->NaturalPersonID));
			if ($rdr->PriceCategoryID) {
				$user->setPriceCategoryId(intval($rdr->PriceCategoryID));
			}
			$result->user = $user;
		}
		return $result;
	}

	/**
	 *
	 * @param string $email        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getUserInfoByEmail($email) {
		$wm = $this->getWebMethod('GetUserByEmailNew');
		$wm->setEmail($email);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getErrorCode());
		} else {
			$rdr = new UltimaObjectReader($response);
			$user = $this->newUser();
			$user->setId(intval($rdr->ID));
			$user->setName(strval($rdr->Name));
			$user->setPassword(strval($rdr->Password));
			$user->setEmail(strval($rdr->Email));
			$user->setPhone(strval($rdr->Phone));
			$user->setTypeId(intval($rdr->LoginTypeID));
			$user->setNaturalPersonId(intval($rdr->NaturalPersonID));
			if ($rdr->PriceCategoryID) {
				$user->setPriceCategoryId(intval($rdr->PriceCategoryID));
			}
			$result->user = $user;
		}
		return $result;
	}

	/**
	 *
	 * @param User $user        	
	 * @return \SoloIdentity\Service\Ultima\Result\UserUpdateResult
	 */
	public function update(User $user, $passwordOld = '', $passwordNew = '') {
		$wm = $this->getWebMethod('UpdateClientInfo');
		$wm->addPar('SecurityKey', $user->getSecurityKey());
		if ($user->getPhone() != '') $wm->addPar('Phone', $user->getPhone());
		if ($user->getEmail() != '') $wm->addPar('Email', $user->getEmail());
		if ($user->getName() != '') $wm->addPar('Name', $user->getName());
		
		if (!empty($passwordOld)) $wm->addPar('passwordOld', $passwordOld);
		if (!empty($passwordNew)) $wm->addPar('passwordNew', $passwordNew);
		$response = $wm->call();
		$rdr = new UltimaJsonListReader($response);
		$success = $rdr->getValue('Success');
		
		$result = new UserUpdateResult($success);
		if ($response->hasError()) {
			$result->setError($response->getError());
		}
		return $result;
	}

	public function updateClientInfo($phone, $email = null) {
		$wm = $this->getWebMethod('UpdateClientInfo');
		
		$wm->addPar('Phone', $phone);
		if ($email) {
			$wm->addPar('Email', $email);
		}
		
		$response = $wm->call();
		
		$rdr = new UltimaJsonListReader($response);
		$success = $rdr->getValue('Success');
		
		$result = new UserUpdateResult($success);
		if ($response->hasError()) {
			$result->setError($response->getError());
			$data = json_decode($response->getResponse()->getContent(), true);
			if (is_array($data)) {
				if ($data['UserReadableError']) {
					$result->setData($data['ErrorText']);
				}
			}
			if (false !== mb_stripos($result->getData(), 'Уже есть клиенты с Email')) {
				$result->setData('Такая почта уже зарегистрирована в системе, укажите другой адрес электронной почты');
			}
		}
		
		return $result;
	}

	public function passwordReset($login) {
		$wm = $this->getWebMethod('SendClientPassword');
		
		$wm->addPar('Phone', Phone::createFromString($login)->toString('7{code}{number}'));
		
		$response = $wm->call();
		$rdr = new UltimaJsonListReader($response);
		
		$Password = $rdr->getValue('NewPassword');
		$result = new Result();
		
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$result->Password = $Password;
		}
		return $result;
	}

	public function updatePassword($currentPassword, $newPassword) {
		$wm = $this->getWebMethod('ChangeClientPassword');
		
		$wm->addPar('CurrentPassword', $currentPassword);
		$wm->addPar('NewPassword', $newPassword);
		$response = $wm->call();
		
		$result = new UserUpdatePasswordResult();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaJsonListReader($response);
			$result->success($rdr->getValue('Success'));
		}
		
		return $result;
	}

	/**
	 *
	 * @param integer $id        	
	 * @param string $password        	
	 * @param string $phone        	
	 * @param boolean $sendCleanPass        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getOldUserByIdAndPassword($id, $password, $phone = null, $sendCleanPass = false) {
		$wm = $this->getWebMethod('GetUserByAnyIDNew');
		$wm->setId($id);
		$wm->setPassword(md5($password));
		if ($sendCleanPass) {
			$wm->setPass($password);
		}
		$wm->setPhone($phone);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setData($response->getData());
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaObjectReader($response);
			$item = new User();
			$item->setId(intval($rdr->ID));
			$item->setName(trim($rdr->Name));
			$item->setPassword(trim($rdr->Password));
			$item->setTypeId(intval($rdr->LoginTypeID));
			$item->setEmail(trim($rdr->Login));
			$item->setNaturalPersonId(intval($rdr->NaturalPersonID));
			$item->setPhone(trim($rdr->Phone));
			
			$result->user = $item;
		}
		return $result;
	}

	/**
	 *
	 * @param string $login        	
	 * @param string $password        	
	 * @param boolean $oldUser        	
	 */
	public function checkPassword($login, $password, $oldUser = false) {
		$result = new AuthResult();
		if ($oldUser) {
			$oldUserInfoResult = $this->getOldUserByIdAndPassword($login, $password);
			if ($oldUserInfoResult->success()) {
				$result->setData($oldUserInfoResult->getData()->getId());
			} else {
				$result->setError($oldUserInfoResult->getErrorCode());
			}
		} else {
			$wm = $this->getWebMethod('GetBusinessLogin');
			$wm->setPhone($login);
			$wm->setPassword(md5($password));
			$response = $wm->call();
			
			if ($response->hasError()) {
				$result->setError($response->getError());
			}
			$result->setData($response->getData());
		}
		return $result->success();
	}

	public function generatePassword($length = 6) {
		return 'passw0rd';
		// $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		// return substr(str_shuffle($chars),0, $length);
	}

	public function isClientExists($phone) {
		$wm = $this->getWebMethod('IsClientExists');
		$wm->addPar('Phone', $phone);
		
		$response = $wm->call();
		
		$result = new Result();
		
		if ($response->hasError()) {
			$result->setError($response->getError());
			$result->setData($response->getResponse()->getReasonPhrase());
		} else {
			$rdr = new UltimaJsonListReader($response);
			$result->success($rdr->getValue('Exists'));
			$result->setData($rdr->getValue('AgentId'));
		}
		
		return $result;
	}

}

?>