<?php

namespace SoloIdentity\Service\Ultima;

use Solo\ServiceManager\ServiceLocatorAwareService;
use SoloERP\Service\ProvidesWebservice;
use Solo\ServiceManager\Result;
use SoloERP\WebService\Reader\UltimaObjectReader;
use SoloIdentity\Entity\Ultima\Manager;

class ManagerService extends ServiceLocatorAwareService {
	
	use ProvidesWebservice;

	/**
	 *
	 * @var string
	 */
	private $defaultEmail = null;

	/**
	 *
	 * @return string
	 */
	public function getDefaultEmail() {
		return $this->defaultEmail;
	}

	/**
	 *
	 * @param string $email        	
	 */
	public function setDefaultEmail($email) {
		$this->defaultEmail = $email;
	}

	/**
	 *
	 * @param integer $userId        	
	 * @return \Solo\ServiceManager\Result
	 */
	public function getManager($userId) {
		$wm = $this->getWebMethod('GetBusinessManager');
		$wm->setUserId($userId);
		$response = $wm->call();
		
		$result = new Result();
		if ($response->hasError()) {
			$result->setError($response->getError());
		} else {
			$rdr = new UltimaObjectReader($response);
			$item = new Manager();
			$item->setId(intval($rdr->ID));
			$item->setFio(trim($rdr->ManagerFIO));
			$item->setPhones(trim($rdr->ManagerPhones));
			$item->setEmail(trim($rdr->ManagerEmail));
			$item->setPhoto(trim($rdr->Foto));
			$item->setDefaultEmail($this->getDefaultEmail());
			
			$result->manager = $item;
		}
		return $result;
	}

}

?>