<?php

namespace SoloIdentity\Entity;

use Zend\Validator\EmailAddress;

class AbstractUser {

    /**
     *
     * @var integer
     */
    private $id = null;

    /**
     *
     * @var string
     */
    private $password = null;

    /**
     *
     * @var string
     */
    private $email = null;

    /**
     *
     * @var string
     */
    private $name = null;

    /**
     *
     * @var string
     */
    private $phone = null;

    /**
     *
     * @var string
     */
    private $securityKey = null;

    /**
     *
     * @return integer $id
     */
    public function getId() {
        return $this->id;
    }

    /**
     * 
     * @param integer $id
     * @throws \InvalidArgumentException
     * @return \SoloIdentity\Entity\AbstractUser
     */
    public function setId($id) {
        if (!is_integer($id)) {
            throw new \InvalidArgumentException('Id must be integer');
        }
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string $lastName
     */
    public function getName() {
        return $this->name;
    }
    
    /**
     * 
     * @return string | null
     */
    public function getNameFromLogin() {
        $nameArray = explode(' ', $this->name);
        if (isset($nameArray[0])) {
            return $nameArray[0];
        }
        return null;
    }
    
    /**
     * 
     * @return string | null
     */
    public function getSernameFromLogin() {
        $nameArray = explode(' ', $this->name);
        if (isset($nameArray[0])) {
            array_shift($nameArray);
            return implode(' ', $nameArray);
        }
        return null;
    }

    /**
     * 
     * @param string $name
     * @return \SoloIdentity\Entity\AbstractUser
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @return string $password
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * 
     * @param string $password
     * @return \SoloIdentity\Entity\AbstractUser
     */
    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }

    /**
     *
     * @return string $securityKey
     */
    public function getSecurityKey() {
        return $this->securityKey;
    }

    /**
     * 
     * @param string $securityKey
     * @return \SoloIdentity\Entity\AbstractUser
     */
    public function setSecurityKey($securityKey) {
        $this->securityKey = $securityKey;
        return $this;
    }

    /**
     *
     * @return string $email
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * 
     * @param string $email
     * @throws \InvalidArgumentException
     * @return \SoloIdentity\Entity\AbstractUser
     */
    public function setEmail($email) {
        if (false && $email && !(new EmailAddress())->isValid($email)) {
            throw new \InvalidArgumentException('Email must be well-formed email address');
        }
        $this->email = $email;
        return $this;
    }

    /**
     *
     * @return string $phone
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * 
     * @param string $phone
     * @return \SoloIdentity\Entity\AbstractUser
     */
    public function setPhone($phone) {
        $this->phone = $phone;
        return $this;
    }

    /**
     * Gets phone code
     *
     * @return string
     */
    public function getPhoneCode() {
        preg_match("/^(\\d{3})\\d{7}$/", $this->getPhone(), $matches);
        return $matches[1];
    }

    /**
     * Gets phone number
     *
     * @return string
     */
    public function getPhoneNumber() {
        preg_match("/^\\d{3}(\\d{7})$/", $this->getPhone(), $matches);
        return $matches[1];
    }

}
