<?php

namespace SoloIdentity\Entity;

use Solo\Collection\Collection;
use SoloIdentity\Entity\Ultima\Agent;

class AgentCollection extends Collection {

    /**
     * (non-PHPdoc)
     * 
     * @see \Solo\Collection\Collection::add()
     */
    public function add($agent, $key = null) {
        if (!($agent instanceof Agent)) {
            throw new \RuntimeException('Agent must be an instace of SoloIdentity\Entity\Ultima\Agent ');
        }
        if (null === $key) {
            $key = $agent->getId();            //var_dump($key);
        }
        return parent::add($agent, $key);
    }

    /**
     * (non-PHPdoc)
     * 
     * @see \Solo\Collection\Collection::remove()
     */
    public function remove($agent) {
        if (!($agent instanceof Agent)) {
            throw new \RuntimeException('Agent must be an instace of SoloIdentity\Entity\Ultima\Agent ');
        }
        return parent::remove($agent);
    }

}

?>