<?php

namespace SoloIdentity\Entity;

abstract class AbstractAddress {

	/**
	 *
	 * @var integer
	 */
	private $id = null;

	/**
	 *
	 * @var string
	 */
	private $name = null;

	/**
	 *
	 * @var string
	 */
	private $description = null;

	/**
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * 
	 * @param integer $id
	 * @throws \InvalidArgumentException
	 * @return \SoloIdentity\Entity\AbstractAddress
	 */
	public function setId($id) {
		if (!is_integer($id)) {
			throw new \InvalidArgumentException('Id must be integer');
		}
		$this->id = $id;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * 
	 * @param string $name
	 * @return \SoloIdentity\Entity\AbstractAddress
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * 
	 * @param string $description
	 * @return \SoloIdentity\Entity\AbstractAddress
	 */
	public function setDescription($description) {
		$this->description = $description;
		return $this;
	}

}

?>