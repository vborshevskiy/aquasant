<?php

namespace SoloIdentity\Entity\Ultima;

use Solo\Collection\Collection;

class PhoneCollection extends Collection {

    /**
     *
     * @param AgentPhone $phone        	
     * @return AgentPhone
     */
    public function add($phone, $key = null) {
        if (!($phone instanceof AgentPhone)) {
            throw new \RuntimeException('Phone must be an instace of AgentPhone');
        }
        parent::add($phone, $phone->getId());
        return $phone;
    }

}

?>