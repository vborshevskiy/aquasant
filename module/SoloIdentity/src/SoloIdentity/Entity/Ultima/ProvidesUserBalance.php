<?php

namespace SoloIdentity\Entity\Ultima;

use Zend\EventManager\EventManagerAwareInterface;

trait ProvidesUserBalance {

    /**
     *
     * @var float
     */
    private $bonusBalance = 0;

    /**
     *
     * @var integer
     */
    private $bonusBalanceLastUpdateTimestamp = -1;

    /**
     *
     * @var integer
     */
    private $bonusBalanceExpireSeconds = 0;

    /**
     *
     * @return float
     */
    public function getBonusBalance($expired = false) {
        if ($expired || ($this->isBonusBalanceExpired() && ($this instanceof EventManagerAwareInterface))) {
            $this->getEventManager()->trigger('bonusBalanceExpired', $this, []);
        }
        return $this->bonusBalance;
    }

    /**
     * Force bonus balance update
     */
    public function updateBonusBalance() {
        $this->setBonusBalance($this->getBonusBalance(true));
    }

    /**
     *
     * @param float $bonusBalance        	
     * @throws \InvalidArgumentException
     */
    public function setBonusBalance($bonusBalance) {
        if ($this instanceof EventManagerAwareInterface) {
            $args = $this->getEventManager()->prepareArgs(compact('bonusBalance'));
            $this->getEventManager()->trigger('change.pre', $this, $args);
        }
        if (!is_numeric($bonusBalance)) {
            throw new \InvalidArgumentException('Bonus balance must be numeric');
        }

        $this->bonusBalance = $bonusBalance;
        $this->bonusBalanceLastUpdateTimestamp = time();
        
        if ($this instanceof EventManagerAwareInterface) {
            $args = $this->getEventManager()->prepareArgs(compact('bonusBalance'));
            $this->getEventManager()->trigger('userChange.post', $this, $args);
        }
    }

    /**
     *
     * @return boolean
     */
    private function isBonusBalanceExpired() {
        return ($this->bonusBalanceExpireSeconds < (time() - $this->bonusBalanceLastUpdateTimestamp));
    }

    /**
     *
     * @param integer $seconds        	
     */
    public function setBonusBalanceExpire($seconds) {
        $this->bonusBalanceExpireSeconds = $seconds;
    }

}

?>