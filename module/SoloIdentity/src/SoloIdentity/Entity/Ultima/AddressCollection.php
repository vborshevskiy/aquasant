<?php

namespace SoloIdentity\Entity\Ultima;

use Solo\Collection\Collection;

class AddressCollection extends Collection {

    /**
     * (non-PHPdoc)
     * 
     * @see \Solo\Collection\Collection::add()
     */
    public function add($address, $key = null) {
        if (!($address instanceof AddressInterface)) {
            throw new \RuntimeException('Address must be an instace of SoloIdentity\Entity\Ultima\AddressInterface ');
        }
        return parent::add($address, $address->getId());
    }

    /**
     * 
     * @param integer $id
     * @return AddressInterface
     */
    public function getDeliveryAddressessById($id) {
        foreach ($this as $address) {
            if ($id == $address->getId()) {
                return $address;
            }
        }
    }
    
    /**
     * 
     * @return \SoloIdentity\Entity\Ultima\AddressCollection
     */
    public function getDeliveryAddresses() {
        $result = clone $this;
        $result->list = array_filter($result->list, function($address) {
            return ($address instanceof DeliveryAddresses);
        });
        return $result;
    }
    
    /**
     * 
     * @return \SoloIdentity\Entity\Ultima\AddressCollection
     */
    public function getPostDeliveryAddresses($regionId) {
        $result = clone $this;
        $result->list = array_filter($result->list, function($address) use ($regionId) {
            return ($address instanceof PostDeliveryAddresses && $address->getRegionId() === $regionId);
        });
        return $result;
    }
    
    /**
     * 
     * @return \SoloIdentity\Entity\Ultima\AddressCollection
     */
    public function getPostPickupAddresses($regionId) {
        $result = clone $this;
        $result->list = array_filter($result->list, function($address) use ($regionId) {
            return ($address instanceof PostPickupAddresses && $address->getRegionId() === $regionId);
        });
        return $result;
    }

}