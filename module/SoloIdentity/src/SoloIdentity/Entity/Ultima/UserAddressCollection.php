<?php

namespace SoloIdentity\Entity\Ultima;

use Solo\Collection\Collection;

class UserAddressCollection extends Collection {

	/**
	 *
	 * @var User
	 */
	private $user;

	public function __construct(User $user, $iterable = null) {
		$this->user = $user;
		parent::__construct($iterable);
	}

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \Solo\Collection\Collection::add()
	 */
	public function add(Address $object) {
		throw new \LogicException('Method ' . __CLASS__ . '::' . __METHOD__ . ' not allowed');
	}

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \Solo\Collection\Collection::remove()
	 */
	public function remove(Address $object) {
		foreach ($this->user->agents() as $agent) {
			$agent->addresses()->remove($object);
		}
	}

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \Solo\Collection\Collection::removeAt()
	 */
	public function removeAt($index) {
		foreach ($this->user->agents() as $agent) {
			$agent->addresses()->removeAt($index);
		}
	}

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \Solo\Collection\Collection::clear()
	 */
	public function clear() {
		foreach ($this->user->agents() as $agent) {
			$agent->addresses()->clear();
		}
	}

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \Solo\Collection\Collection::toArray()
	 */
	public function toArray() {
		return array_merge(array_map(function ($agent) {
			return $agent->addresses()->toArray();
		}, $this->user->agents()));
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \Solo\Collection\Collection::offsetGet()
	 */
	public function offsetGet($index) {
		foreach ($this->user->agents() as $agent) {
			if (isset($agent->addresses()[$index])) {
				return $agent->addresses()[$index];
			}
		}
		return null;
	}

	/**
	 * (non-PHPdoc)
	 *
	 * @see \Solo\Collection\Collection::offsetSet()
	 */
	public function offsetSet($index, Address $object) {
		foreach ($this->user->agents() as $agent) {
			if (isset($agent->addresses()[$index])) {
				$agent->addresses()[$index] = $object;
			}
		}
	}

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \Solo\Collection\Collection::offsetExists()
	 */
	public function offsetExists($index) {
		foreach ($this->user->agents() as $agent) {
			if (isset($agent->addresses()[$index])) {
				return true;
			}
		}
		return false;
	}

	/**
	 * (non-PHPdoc)
	 * 
	 * @see \Solo\Collection\Collection::offsetUnset()
	 */
	public function offsetUnset($index) {
		foreach ($this->user->agents() as $agent) {
			if (isset($agent->addresses()[$index])) {
				unset($agent->addresses()[$index]);
			}
		}
	}

}

?>