<?php

namespace SoloIdentity\Entity\Ultima;

use SoloIdentity\Entity\AbstractAddress;
use Solo\EventManager\ProvidesEvents;
use Zend\EventManager\EventManagerAwareInterface;

class Address extends AbstractAddress implements EventManagerAwareInterface, \Serializable {

    use ProvidesEvents;

    /**
     *
     * @var string
     */
    private $phone = null;

    /**
     *
     * @var integer
     */
    private $postIndex = null;

    /**
     *
     * @var float
     */
    private $lightDeliveryCost = null;

    /**
     *
     * @var float
     */
    private $hardDeliveryCost = null;

    /**
     *
     * @var array
     */
    private $parameters = [];

    /**
     *
     * @var Agent
     */
    private $agent = null;

    /**
     *
     * @var \SoloIdentity\Service\Ultima\Behavior\GetDeliveryCostBehavior
     */
    private $deliveryCostGetter = null;

    /**
     *
     * @param integer $id        	
     * @throws \InvalidArgumentException
     * @return Address
     */
    public function setId($id) {
        $args = $this->events()->prepareArgs(compact('id'));
        $this->events()->trigger('change.pre', $this, $args);

        parent::setId($id);

        $this->events()->trigger('change.post', $this, $args);
        return $this;
    }

    /**
     *
     * @param string $name        	
     * @throws \InvalidArgumentException
     * @return Address
     */
    public function setName($name) {
        $args = $this->events()->prepareArgs(compact('name'));
        $this->events()->trigger('change.pre', $this, $args);

        parent::setName($name);

        $this->events()->trigger('change.post', $this, $args);
        return $this;
    }

    /**
     *
     * @param string $description        	
     * @throws \InvalidArgumentException
     * @return Address
     */
    public function setDescription($description) {
        $args = $this->events()->prepareArgs(compact('description'));
        $this->events()->trigger('change.pre', $this, $args);

        parent::setDescription($description);

        $this->events()->trigger('change.post', $this, $args);
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     *
     * @param string $phone        	
     * @return \SoloIdentity\Entity\Ultima\Address
     */
    public function setPhone($phone) {
        $args = $this->events()->prepareArgs(compact('phone'));
        $this->events()->trigger('change.pre', $this, $args);

        $this->phone = $phone;

        $this->events()->trigger('change.post', $this, $args);
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getPostIndex() {
        return $this->postIndex;
    }

    /**
     *
     * @param integer $postIndex        	
     * @throws \InvalidArgumentException
     * @return \SoloIdentity\Entity\Ultima\Address
     */
    public function setPostIndex($postIndex) {
        if (!is_integer($postIndex)) {
            throw new \InvalidArgumentException('Post index must be integer');
        }

        $args = $this->events()->prepareArgs(compact('postIndex'));
        $this->events()->trigger('change.pre', $this, $args);

        $this->postIndex = $postIndex;

        $this->events()->trigger('change.post', $this, $args);
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getLightDeliveryCost() {
        if (null === $this->lightDeliveryCost) {
            $this->getDeliveryCosts();
        }
        return $this->lightDeliveryCost;
    }

    /**
     *
     * @param float $lightDeliveryCost        	
     * @throws \InvalidArgumentException
     * @return \SoloIdentity\Entity\Ultima\Address
     */
    public function setLightDeliveryCost($lightDeliveryCost) {
        if (!is_numeric($lightDeliveryCost)) {
            throw new \InvalidArgumentException('Light delivery cost must be numeric');
        }

        $args = $this->events()->prepareArgs(compact('lightDeliveryCost'));
        $this->events()->trigger('change.pre', $this, $args);

        $this->lightDeliveryCost = $lightDeliveryCost;

        $this->events()->trigger('change.post', $this, $args);
        return $this;
    }

    /**
     * 
     * @return \SoloIdentity\Service\Ultima\Behavior\GetDeliveryCostBehavior
     */
    public function getDeliveryCostGetter() {
        return $this->deliveryCostGetter;
    }

    /**
     * 
     * @param \SoloIdentity\Service\Ultima\Behavior\GetDeliveryCostBehavior $deliveryCostGetter
     */
    public function setDeliveryCostGetter(\SoloIdentity\Service\Ultima\Behavior\GetDeliveryCostBehavior $deliveryCostGetter) {
        $this->deliveryCostGetter = $deliveryCostGetter;
    }

    /**
     *
     * @return float
     */
    public function getHardDeliveryCost() {
        if ($this->hardDeliveryCost === null) {
            $this->getDeliveryCosts();
        }
        return $this->hardDeliveryCost;
    }

    /**
     *
     * @param float $hardDeliveryCost        	
     * @throws \InvalidArgumentException
     * @return \SoloIdentity\Entity\Ultima\Address
     */
    public function setHardDeliveryCost($hardDeliveryCost) {
        if (!is_numeric($hardDeliveryCost)) {
            throw new \InvalidArgumentException('Hard delivery cost must be numberic');
        }

        $args = $this->events()->prepareArgs(compact('hardDeliveryCost'));
        $this->events()->trigger('change.pre', $this, $args);

        $this->hardDeliveryCost = $hardDeliveryCost;

        $this->events()->trigger('change.post', $this, $args);
        return $this;
    }

    /**
     *
     * @param string $name        	
     * @param mixed $value        	
     * @return \SoloIdentity\Entity\Ultima\Address
     */
    public function setParameter($name, $value) {
        $args = $this->events()->prepareArgs([
            'parameterName' => $name,
            'parameterValue' => $value
                ]);
        $this->events()->trigger('change.pre', $this, $args);

        $this->parameters[$name] = $value;

        $this->events()->trigger('change.post', $this, $args);
        return $this;
    }

    /**
     * Gets specified custom parameters of address
     *
     * @param string $name        	
     * @throws \RuntimeException
     * @return mixed
     */
    public function getParameter($name) {
        if (!array_key_exists($name, $this->parameters)) {
            throw new \RuntimeException('Undefined parameter \'' . $name . '\'');
        }
        return $this->parameters[$name];
    }
    
    /**
     * Set parameters for json description
     * @param array $parameters
     */
    public function setParameters(array $parameters) {
        $this->parameters = $parameters;
    }

    /**
     * Gets array of generic parameters of address
     *
     * @return array
     */
    public function getParameters() {
        return $this->parameters;
    }

    /**
     * 
     * @param Agent $agent
     * @return \SoloIdentity\Entity\Ultima\Address
     */
    public function setAgent(Agent $agent) {
        $this->agent = $agent;
        return $this;
    }

    /**
     *
     * @return \SoloIdentity\Entity\Ultima\Agent
     */
    public function getAgent() {
        return $this->agent;
    }

    /**
     * Checks has address linked agent
     *
     * @return boolean
     */
    public function hasAgent() {
        return (null !== $this->agent);
    }

    /**
     * Gets phone code
     *
     * @return string
     */
    public function getPhoneCode() {
        preg_match("/^\\+7(\\d{3})\\d{7}$/", $this->getPhone(), $matches);
        return intval($matches[1]);
    }

    /**
     * Gets phone number
     *
     * @return string
     */
    public function getPhoneNumber() {
        preg_match("/^\\+7\\d{3}(\\d{7})$/", $this->getPhone(), $matches);
        return intval($matches[1]);
    }

    public function serialize() { //die('3');
//        $data = new \stdClass;
//        $data->id = $this->getId();
//        $data->name = $this->getName();
//        $data->description = $this->getDescription();
//        $data->phone = $this->getPhone();
//        $data->postIndex = $this->getPostIndex();
//        $data->lightDeliveryCost = $this->getLightDeliveryCost();
//        $data->hardDeliveryCost = $this->getHardDeliveryCost();
////        $data->agent = $this->getAgent();
//        $data->parameters = $this->getParameters();
//        $data->deliveryCostGetter = $this->getDeliveryCostGetter();
//        return \Zend\Serializer\Serializer::serialize($data);
    }

    public function unserialize($serialized) { 
//        $data = \Zend\Serializer\Serializer::unserialize($serialized);
//        $this->setId($data->id);
//        $this->setName($data->name);
//        $this->setDescription($data->description);
//        $this->setPhone($data->phone);
//        $this->setPostIndex($data->postIndex);
//        $this->setDeliveryCostGetter($data->deliveryCostGetter);
//        if (!is_null($data->lightDeliveryCost)) {
//            $this->setLightDeliveryCost($data->lightDeliveryCost);
//        }
//        if (!is_null($data->hardDeliveryCost)) {
//            $this->setHardDeliveryCost($data->hardDeliveryCost);
//        }
////        $this->setAgent($data->agent);
//        foreach ($data->parameters as $paramName => $paramValue) {
//            $this->setParameter($paramName, $paramValue);
//        }
    }
    
    private function getDeliveryCosts() {
        $costs = $this->getDeliveryCostGetter()->getDeliveryCostsByPostIndex($this->getPostIndex());
        if ($costs !== null) {
            $this->setLightDeliveryCost(intval($costs['LightCost']));
            $this->setHardDeliveryCost(intval($costs['HardCost']));
        }
    }

}

?>