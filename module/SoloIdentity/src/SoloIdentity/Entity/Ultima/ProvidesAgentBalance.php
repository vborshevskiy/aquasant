<?php

namespace SoloIdentity\Entity\Ultima;

use Zend\EventManager\EventManagerAwareInterface;

trait ProvidesAgentBalance {

	/**
	 *
	 * @var float
	 */
	private $balance = 0;

	/**
	 *
	 * @var integer
	 */
	private $balanceLastUpdateTimestamp = -1;

	/**
	 *
	 * @var integer
	 */
	private $balanceExpireSeconds = 0;

	/**
	 *
	 * @return float
	 */
	public function getBalance() {
		if ($this->isBalanceExpired() && ($this instanceof EventManagerAwareInterface)) {
			$this->getEventManager()->trigger('balanceExpired', $this, []);
		}
		return $this->balance;
	}

	/**
	 *
	 * @param float $balance        	
	 * @throws \InvalidArgumentException
	 */
	public function setBalance($balance) {
		if ($this instanceof EventManagerAwareInterface) {
			$args = $this->getEventManager()->prepareArgs(compact('balance'));
			$this->getEventManager()->trigger('change.pre', $this, $args);
		}
		if (!is_numeric($balance)) {
			throw new \InvalidArgumentException('Balance must be numeric');
		}
		
		$this->balance = $balance;
		$this->balanceLastUpdateTimestamp = time();
		
		if ($this instanceof EventManagerAwareInterface) {
			$args = $this->getEventManager()->prepareArgs(compact('balance'));
			$this->getEventManager()->trigger('change.post', $this, $args);
		}
	}

	/**
	 *
	 * @return boolean
	 */
	private function isBalanceExpired() {
		return ($this->balanceExpireSeconds < (time() - $this->balanceLastUpdateTimestamp));
	}

	/**
	 *
	 * @param integer $seconds        	
	 */
	public function setBalanceExpire($seconds) {
		$this->balanceExpireSeconds = $seconds;
	}

}

?>