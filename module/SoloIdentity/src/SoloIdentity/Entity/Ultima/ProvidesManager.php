<?php

namespace SoloIdentity\Entity\Ultima;

trait ProvidesManager {

	/**
	 *
	 * @var Manager
	 */
	private $manager = null;

	/**
	 *
	 * @return Manager
	 */
	public function getManager() {
		return $this->manager;
	}

	/**
	 *
	 * @param Manager $manager        	
	 */
	public function setManager(Manager $manager) {
		$this->manager = $manager;
	}

}

?>