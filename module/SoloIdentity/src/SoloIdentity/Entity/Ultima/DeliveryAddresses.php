<?php

namespace SoloIdentity\Entity\Ultima;

class DeliveryAddresses implements AddressInterface {

    /**
     *
     * @var integer
     */
    private $id = null;

    /**
     *
     * @var string
     */
    private $address = null;

    /**
     *
     * @var string
     */
    private $phone = null;

    /**
     *
     * @var decimal
     */
    private $latitude = null;

    /**
     *
     * @var decimal
     */
    private $longitude = null;

    /**
     *
     * @var string
     */
    private $userName = null;

	/**
     *
     * @var int
     */
    private $floor = null;
    
    /**
     * 
     * @var integer
     */
    private $elevatorTypeId = null;

    /**
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @param integer $id
     * @throws \InvalidArgumentException
     */
    public function setId($id) {
        if (!is_integer($id)) {
            throw new \InvalidArgumentException('Id must be integer');
        }
        $this->id = $id;
    }

    /**
     *
     * @return string
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     *
     * @param string $address
     */
    public function setAddress($address) {
        $this->address = $address;
    }

    /**
     *
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     *
     * @param string $phone
     */
    public function setPhone($phone) {
        $this->phone = $phone;
    }

    /**
     *
     * @return decimal
     */
    public function getLatitude() {
        return $this->latitude;
    }

    /**
     *
     * @param decimal
     */
    public function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    /**
     *
     * @return decimal
     */
    public function getLongitude() {
        return $this->longitude;
    }

    /**
     *
     * @param decimal
     */
    public function setLongitude($longitude) {
        $this->longitude = $longitude;
    }

    /**
     *
     * @return null|string
     */
    public function getCode() {
        preg_match("/^(\\d{3})\\d{7}$/", str_replace(' ','',$this->getPhone()), $matches);
        if (isset($matches[1])) {
            return $matches[1];
        }
        return null;
    }

    /**
     *
     * @return null|string
     */
    public function getPhoneNumber() {
        preg_match("/^\\d{3}(\\d{7})$/", str_replace(' ','',$this->getPhone()), $matches);
        if (isset($matches[1])) {
            return $matches[1];
        }
        return null;
    }

    /**
     *
     * @return string
     */
    public function getUserName() {
        return $this->userName;
    }

    /**
     *
     * @param string $userName
     */
    public function setUserName($userName) {
        $this->userName = $userName;
    }

	/**
     *
     * @return int
     */
    public function getFloor() {
        return $this->floor;
    }

    /**
     *
     * @param string $floor
     */
    public function setFloor($floor) {
        $this->floor = $floor;
    }
	/**
	 * @return number
	 */
	public function getElevatorTypeId() {
		return $this->elevatorTypeId;
	}


	/**
	 * @param number $elevatorTypeId
	 */
	public function setElevatorTypeId($elevatorTypeId) {
		$this->elevatorTypeId = $elevatorTypeId;
		return $this;
	}



}
