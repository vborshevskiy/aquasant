<?php

namespace SoloIdentity\Entity\Ultima;

trait ProvidesFakeAgent {

	/**
	 *
	 * @return string
	 */
	public static function getFakePassword() {
		return 'SuperPassword';
	}

	/**
	 *
	 * @return string
	 */
	public static function getFakePhone() {
		return '70000000003';
	}

	/**
	 *
	 * @return string
	 */
	public static function getFakeAgentEmail() {
		return 'test8@test.com';
	}

	/**
	 *
	 * @return string
	 */
	public static function getFakeFio() {
		return '';
	}

	public static function getFakeFirstName() {
	    return 'John';
    }

    public static function getFakeLastName() {
	    return 'Doe';
    }

	/**
	 *
	 * @var boolean
	 */
	private $isFake = false;

	/**
	 *
	 * @param boolean $isFake        	
	 * @throws \InvalidArgumentException
	 * @return boolean
	 */
	public function isFake($isFake = null) {
		if (null !== $isFake) {
			if (!is_bool($isFake)) {
				throw new \InvalidArgumentException('Is fake must be boolean');
			}
			$this->isFake = $isFake;
		} else {
			return $this->isFake;
		}
	}

}

?>