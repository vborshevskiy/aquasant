<?php

namespace SoloIdentity\Entity\Ultima;

use SoloIdentity\Entity\AbstractUser;
use Zend\EventManager\EventManagerAwareInterface;
use Solo\EventManager\ProvidesEvents;

class FakeUser extends AbstractUser implements \Serializable, EventManagerAwareInterface {

    use ProvidesEvents;
    
    /**
     * 
     * @return boolean
     */
    public function isFake() {
        return true;
    }

    public function serialize() {
        $data = new \stdClass;
        $data->id = $this->getId();
        $data->name = $this->getName();
        $data->email = $this->getEmail();
        $data->phone = $this->getPhone();
        $data->password = $this->getPassword();
        $data->securityKey = $this->getSecurityKey();    
        $result = \Zend\Serializer\Serializer::serialize($data);
        return $result;
    }

    public function unserialize($serialized) {
        /*@var $data \SoloIdentity\Entity\Ultima\User */
        $data = \Zend\Serializer\Serializer::unserialize($serialized);
        $this->setId($data->id);
        $this->setName($data->name);
        $this->setEmail($data->email);
        $this->setPhone($data->phone);
        $this->setPassword($data->password);
        $this->setSecurityKey($data->securityKey);
    }

}

?>