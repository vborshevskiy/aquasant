<?php

namespace SoloIdentity\Entity\Ultima;

class Manager {

	/**
	 *
	 * @var integer
	 */
	private $id = null;

	/**
	 *
	 * @var string
	 */
	private $fio = null;

	/**
	 *
	 * @var string
	 */
	private $phones = null;

	/**
	 *
	 * @var string
	 */
	private $email = null;

	/**
	 *
	 * @var string
	 */
	private $photo = null;

	/**
	 *
	 * @var string
	 */
	private $defaultEmail = null;

	/**
	 *
	 * @var string
	 */
	private $photoPath = null;

	/**
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * 
	 * @param integer $id
	 * @throws \InvalidArgumentException
	 * @return \SoloIdentity\Entity\Ultima\Manager
	 */
	public function setId($id) {
		if (!is_integer($id)) {
			throw new \InvalidArgumentException('Id must be integer');
		}
		$this->id = $id;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getFio() {
		return $this->fio;
	}

	/**
	 * 
	 * @param string $fio
	 * @return \SoloIdentity\Entity\Ultima\Manager
	 */
	public function setFio($fio) {
		$this->fio = $fio;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getPhones() {
		return $this->phones;
	}

	/**
	 * 
	 * @param string $phones
	 * @return \SoloIdentity\Entity\Ultima\Manager
	 */
	public function setPhones($phones) {
		$this->phones = $phones;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * 
	 * @param string $email
	 * @return \SoloIdentity\Entity\Ultima\Manager
	 */
	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getPhoto() {
		return $this->photo;
	}

	/**
	 * 
	 * @param string $photo
	 * @return \SoloIdentity\Entity\Ultima\Manager
	 */
	public function setPhoto($photo) {
		$this->photo = $photo;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getDefaultEmail() {
		return $this->defaultEmail;
	}

	/**
	 * 
	 * @param string $defaultEmail
	 * @return \SoloIdentity\Entity\Ultima\Manager
	 */
	public function setDefaultEmail($defaultEmail) {
		$this->defaultEmail = $defaultEmail;
		return $this;
	}

	/**
	 *
	 * @return string
	 */
	public function getPhotoPath() {
		return $this->photoPath;
	}

	/**
	 * 
	 * @param string $photoPath
	 * @return \SoloIdentity\Entity\Ultima\Manager
	 */
	public function setPhotoPath($photoPath) {
		$this->photoPath = $photoPath;
		return $this;
	}

}

?>