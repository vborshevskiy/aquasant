<?php

namespace SoloIdentity\Entity\Ultima;

use SoloIdentity\Entity\AbstractAgent;
use Zend\EventManager\EventManagerAwareInterface;
use Solo\EventManager\ProvidesEvents;
use SoloIdentity\Entity\Ultima\ProvidesAgentBalance;
use SoloIdentity\Entity\Ultima\ProvidesFakeAgent;

class Agent extends AbstractAgent implements EventManagerAwareInterface, \Serializable {

    use ProvidesEvents;

    use ProvidesAgentBalance;

    use ProvidesFakeAgent;

    /**
     * 
     * @var integer
     */
    const TYPE_NATURAL_PERSON = 20019;

    /**
     * 
     * @var integer
     */
    const TYPE_COMMERCIAL_ORGANIZATION = 20020;

    /**
     * 
     * @var integer
     */
    const FIRM_STRUCT_COMMERCIAL = 1;

    /**
     * 
     * @var integer
     */
    const FIRM_STRUCT_GOV = 2;

    /**
     * 
     * @var integer
     */
    const FIRM_STRUCT_IB = 4;

    /**
     *
     * @var integer
     */
    private $typeId = null;

    /**
     *
     * @var integer
     */
    private $firmStructId = null;

    /**
     *
     * @var integer
     */
    private $priceCategoryId = null;

    /**
     *
     * @var AgentRequisites
     */
    private $_requisites = null;

    /**
     *
     * @var PhoneCollection
     */
    private $phoneList = null;

    /**
     *
     * @var AddressCollection
     */
    private $addressList;
    
    /**
     *
     * @var string
     */    
    private $address;

    
    
    public function __construct() {
        $this->_requisites = new AgentRequisites();        
//        $this->addressList = new AddressCollection();
//        $this->phoneList = new PhoneCollection();
    }

    /**
     *
     * @param integer $id        	
     * @throws \InvalidArgumentException
     * @return \SoloIdentity\Entity\Ultima\Agent
     */
    public function setId($id) {
        $args = $this->events()->prepareArgs(compact('id'));
        $this->events()->trigger('change.pre', $this, $args);

        parent::setId($id);

        $this->events()->trigger('change.post', $this, $args);
        return $this;
    }

    /**
     *
     * @param string $name        	
     * @return \SoloIdentity\Entity\Ultima\Agent
     */
    public function setName($name) {
        $args = $this->events()->prepareArgs(compact('name'));
        $this->events()->trigger('change.pre', $this, $args);

        parent::setName($args['name']);

        $this->events()->trigger('change.post', $this, $args);
        return $this;
    }

//    /**
//     *
//     * @return integer
//     */
//    public function getTypeId() {
//        return $this->typeId;
//    }
//
//    /**
//     *
//     * @param integer $typeId        	
//     * @throws \InvalidArgumentException
//     * @return \SoloIdentity\Entity\Ultima\Agent
//     */
//    public function setTypeId($typeId) {
//        if (!is_integer($typeId)) {
//            throw new \InvalidArgumentException('Type id must be integer');
//        }
//
//        $args = $this->events()->prepareArgs(compact('typeId'));
//        $this->events()->trigger('change.pre', $this, $args);
//
//        $this->typeId = $typeId;
//
//        $this->events()->trigger('change.post', $this, $args);
//        return $this;
//    }
//
//    /**
//     *
//     * @return integer
//     */
//    public function getFirmStructId() {
//        return $this->firmStructId;
//    }
//
//    /**
//     *
//     * @param integer $firmStructId        	
//     * @throws \InvalidArgumentException
//     * @return \SoloIdentity\Entity\Ultima\Agent
//     */
//    public function setFirmStructId($firmStructId) {
//        if (!is_integer($firmStructId)) {
//            throw new \InvalidArgumentException('Firm structure id must be integer');
//        }
//
//        $args = $this->events()->prepareArgs(compact('firmStructId'));
//        $this->events()->trigger('change.pre', $this, $args);
//
//        $this->firmStructId = $firmStructId;
//
//        $this->events()->trigger('change.post', $this, $args);
//        return $this;
//    }
//
//    /**
//     *
//     * @return integer $priceCategoryId
//     */
//    public function getPriceCategoryId() {
//        return $this->priceCategoryId;
//    }
//
//    /**
//     *
//     * @param integer $priceCategoryId        	
//     * @throws \InvalidArgumentException
//     * @return \SoloIdentity\Entity\Ultima\Agent
//     */
//    public function setPriceCategoryId($priceCategoryId) {
//        if (!is_integer($priceCategoryId)) {
//            throw new \InvalidArgumentException('Price category id must be integer');
//        }
//
//        $args = $this->events()->prepareArgs(compact('priceCategoryId'));
//        $this->events()->trigger('change.pre', $this, $args);
//
//        $this->priceCategoryId = $priceCategoryId;
//
//        $this->events()->trigger('change.post', $this, $args);
//        return $this;
//    }
//
//    /**
//     *
//     * @return string
//     */
//    public function getPriceCategoryName() {
//        if ($this->isNaturalPerson()) {
//            return 'purchase';
//        }
//        return 'default';
//    }

    /**
     *
     * @param User $user        	
     * @return \SoloIdentity\Entity\Ultima\Agent
     */
    public function setUser(\SoloIdentity\Entity\AbstractUser $user) {
        parent::setUser($user);
        return $this;
    }

    /**
     *
     * @return User
     */
    public function getUser() {
        return parent::getUser();
    }

    /**
     * Checks is current agent is natural person
     *
     * @return boolean
     */
    public function isNaturalPerson() {
        return (self::TYPE_NATURAL_PERSON == $this->getTypeId());
    }

    /**
     * Checks is current agent is commercial organization
     *
     * @return boolean
     */
    public function isCommercialOrganization() {
        return (self::TYPE_COMMERCIAL_ORGANIZATION == $this->getTypeId());
    }

    /**
     *
     * @return \SoloIdentity\Entity\Ultima\AgentRequisites
     */
    public function requisites() {
        return $this->_requisites;
    }

//    /**
//     *
//     * @return \SoloIdentity\Entity\Ultima\AddressCollection
//     */
//    public function addresses() {
//        return $this->addressList;
//    }
//
//    /**
//     *
//     * @param Address $address        	
//     * @return Address
//     */
//    public function addAddress(Address $address) {
//        $address->setAgent($this);
//        $this->addresses()->add($address);
//        return $address;
//    }
//
//    public function removeAddress($index) {
//        $this->addresses()->removeAt($index);
//    }
//
//    /**
//     *
//     * @return \SoloIdentity\Entity\Ultima\PhoneCollection
//     */
//    public function phones() {
//        return $this->phoneList;
//    }
//
//    /**
//     *
//     * @param AgentPhone $phone        	
//     * @return AgentPhone
//     */
//    public function addPhone(AgentPhone $phone) {
//        $phone->setAgent($this);
//        $this->phones()->add($phone);
//        return $phone;
//    }

    public function serialize() {
        $data = new \stdClass;
        $data->id = $this->getId();
        $data->name = $this->getName();
        $data->address = $this->requisites()->getAddress();
        $data->inn = $this->requisites()->getInn();
//        $data->user = $this->getUser();
       // $data->typeId = $this->getTypeId();
//        $data->firmStructId = $this->getFirmStructId();
        //$data->priceCategoryId = $this->getPriceCategoryId();
        
        
        $data->kpp = $this->requisites()->getKpp();
        $data->bic = $this->requisites()->getBic();
//        $data->okpo = $this->requisites()->getOkpo();
        $data->bankName = $this->requisites()->getBankName();
////        $data->email = $this->requisites()->getEmail();
        $data->settlementAccount = $this->requisites()->getSettlementAccount();
        $data->bankCorrAccount = $this->requisites()->getBankCorrAccount();
        $data->opf = $this->requisites()->getOpf();
        
//        $data->phoneList = $this->phones();
//        $data->addressList = $this->addresses();
        return \Zend\Serializer\Serializer::serialize($data);
    }

    public function unserialize($serialized) {       
        $data = \Zend\Serializer\Serializer::unserialize($serialized);  

        $this->_requisites = new AgentRequisites();
        //$this->requisites()->setInn($data->inn)->setKpp($data->kpp)->setOkpo($data->okpo)->setSettlementAccount($data->settlementAccount)->setBic($data->bic)->setBankName($data->bankName)->setBankCorrAccount($data->bankCorrAccount)->setEmail($data->email);
        //var_dump($data); die;
        $this->setId($data->id);
        $this->requisites()->setInn($data->inn);
        $this->requisites()->setKpp($data->kpp);
        $this->setName($data->name);
        $this->requisites()->setAddress($data->address);

        $this->requisites()->setSettlementAccount($data->settlementAccount);
//        $this->requisites()->setOkpo($data->okpo);
        $this->requisites()->setBic($data->bic);
        $this->requisites()->setBankCorrAccount($data->bankCorrAccount);
        $this->requisites()->setBankName($data->bankName);
        if ($data->opf) {
        	$this->requisites()->setOpf($data->opf);
        }
//        $this->setUser($data->user);
       // $this->setTypeId($data->typeId);
//        if ($data->firmStructId !== null) {
//            $this->setFirmStructId($data->firmStructId);
//        } 
//        //$this->setPriceCategoryId($data->priceCategoryId);
//        $this->phoneList = $data->phoneList;
//        $this->addressList = $data->addressList;
        
        //var_dump($this); die;
    }

}

?>