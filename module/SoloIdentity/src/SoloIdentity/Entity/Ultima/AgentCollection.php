<?php

namespace SoloIdentity\Entity\Ultima;

class AgentCollection extends \SoloIdentity\Entity\AgentCollection {

    /**
     * Gets agent which is natural person
     *
     * @return Agent NULL
     */
    public function getNaturalPerson() {
        foreach ($this as $agent) {
            if ($agent->isNaturalPerson()) {
                return $agent;
            }
        }
        return null;
    }

    /**
     * Get array of agents which are commercial organizations
     *
     * @return multitype:mixed
     */
    public function enumCommercialOrganizations() {
        $list = [];
        foreach ($this as $agent) {
            if ($agent->isCommercialOrganization()) {
                $list[] = $agent;
            }
        }
        return $list;
    }

    public function getAgentById($id) {
        foreach ($this as $agent) {
            if ($id == $agent->getId()) {
                return $agent;
            }
        }
    }

}

?>