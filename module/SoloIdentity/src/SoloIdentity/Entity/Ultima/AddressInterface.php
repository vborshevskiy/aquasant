<?php

namespace SoloIdentity\Entity\Ultima;

interface AddressInterface {
    
    /**
     * 
     * @return integer
     */
    public function getId();

    /**
     * 
     * @return string
     */
    public function getAddress();
    
}
