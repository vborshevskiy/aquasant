<?php

namespace SoloIdentity\Entity\Ultima;

use SoloIdentity\Entity\AbstractPhone;
use Zend\EventManager\EventManagerAwareInterface;
use Solo\EventManager\ProvidesEvents;

class AgentPhone extends AbstractPhone implements EventManagerAwareInterface {
	
	use ProvidesEvents;

	/**
	 *
	 * @var integer
	 */
	private $id = null;

	/**
	 *
	 * @var Agent
	 */
	private $agent = null;

	/**
	 *
	 * @param string $number        	
	 * @throws \InvalidArgumentException
	 */
	public function setNumber($number) {
		$args = $this->events()->prepareArgs(compact('number'));
		$this->events()->trigger('change.pre', $this, $args);
		
		parent::setNumber($number);
		
		$this->events()->trigger('change.post', $this, $args);
	}

	/**
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 *
	 * @param integer $id        	
	 * @throws \InvalidArgumentException
	 */
	public function setId($id) {
		$args = $this->events()->prepareArgs(compact('id'));
		$this->events()->trigger('change.pre', $this, $args);
		
		if (!is_integer($id)) {
			throw new \InvalidArgumentException('Id must be integer');
		}
		$this->id = $id;
		
		$this->events()->trigger('change.post', $this, $args);
	}

	/**
	 *
	 * @return \SoloIdentity\Entity\Ultima\Agent
	 */
	public function getAgent() {
		return $this->agent;
	}

	/**
	 * 
	 * @param Agent $agent
	 * @return \SoloIdentity\Entity\Ultima\AgentPhone
	 */
	public function setAgent(Agent $agent) {
		$this->agent = $agent;
		return $this;
	}

	/**
	 * Gets phone code
	 *
	 * @return string
	 */
	public function getPhoneCode() {
		preg_match("/^\\+7(\\d{3})\\d{7}$/", $this->getNumber(), $matches);
		return $matches[1];
	}

	/**
	 * Gets phone number
	 *
	 * @return string
	 */
	public function getPhoneNumber() {
		preg_match("/^\\+7\\d{3}(\\d{7})$/", $this->getNumber(), $matches);
		return $matches[1];
	}

	/**
	 *
	 * @return string
	 */
	public function __toString() {
		return $this->getNumber();
	}

}

?>