<?php

namespace SoloIdentity\Entity\Ultima;

class PostPickupAddresses implements AddressInterface {
    
    /**
     *
     * @var integer
     */
    private $id = null;

    /**
     *
     * @var string
     */
    private $transportCompanyUid;

    /**
     *
     * @var string
     */
    private $address = null;
    
    /**
     *
     * @var string
     */
    private $index = null;

    /**
     *
     * @var string
     */
    private $phoneCode = null;
    
    /**
     *
     * @var string
     */
    private $phoneNumber = null;
    
    /**
     *
     * @var string
     */
    private $userName = null;
    
    /**
     *
     * @var integer
     */
    private $regionId = null;
    
    /**
     *
     * @var string
     */
    private $pekId = null;


    /**
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @param integer $id        	
     * @throws \InvalidArgumentException
     */
    public function setId($id) {
        if (!is_integer($id)) {
            throw new \InvalidArgumentException('id must be integer');
        }
        $this->id = $id;
    }
    
    /**
     * 
     * @param string $uid
     */
    public function setTransportCompanyUid($uid) {
        if (!in_array($uid, [
            'sdek',
            'ems',
            'pecom',
        ])) {
             throw new \InvalidArgumentException('Invalid transport company');
        }
        $this->transportCompanyUid = $uid;
    }
    
    /**
     * 
     * @return string
     */
    public function getTransportCompanyUid() {
        return $this->transportCompanyUid;
    }

    /**
     *
     * @return string
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     *
     * @param string $address     	
     */
    public function setAddress($address) {
        $this->address = $address;
    }
    
    /**
     * 
     * @return string
     */
    public function getIndex() {
        return $this->index;
    }
    
    /**
     * 
     * @param string $index
     */
    public function setIndex($index) {
        $this->index = $index;
    }

    /**
     *
     * @return string
     */
    public function getPhoneNumber() {
        return $this->phoneNumber;
    }

    /**
     *
     * @param string $number     	
     */
    public function setPhoneNumber($number) {
        $this->phoneNumber = $number;
    }

    /**
     * 
     * @return string
     */
    public function getPhoneCode() {
        return $this->phoneCode;
    }
    
    /**
     * 
     * @param string $code
     */
    public function setPhoneCode($code) {
        $this->phoneCode = $code;
    }
    
    /**
     * 
     * @param string $phone
     */
    public function setPhone($phone) {
        preg_match("/^(\\d{3})\\d{7}$/", $phone, $matches);
        if (isset($matches[1])) {
            $this->phoneCode = $matches[1];
        }
        preg_match("/^\\d{3}(\\d{7})$/", $phone, $matches);
        if (isset($matches[1])) {
            $this->phoneNumber = $matches[1];
        }
    }

    /**
     * 
     * @return string
     */
    public function getUserName() {
        return $this->userName;
    }
    
    /**
     * 
     * @param string $userName
     */
    public function setUserName($userName) {
        $this->userName = $userName;
    }
    
    /**
     * 
     * @return string
     */
    public function getPhone() {
        return $this->phoneCode . $this->phoneNumber;
    }
    
    /**
     * 
     * @param integer $regionId
     */
    public function setRegionId($regionId) {
        $this->regionId = $regionId;
    }

    /**
     * 
     * @return integer
     */
    public function getRegionId() {
        return $this->regionId;
    }
    
    /**
     * 
     * @param integer $pekId
     */
    public function setPekWarehouseId($pekId) {
        $this->pekId = $pekId;
    }

    /**
     * 
     * @return integer
     */
    public function getPekWarehouseId() {
        return $this->pekId;
    }

}
