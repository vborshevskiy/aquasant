<?php

namespace SoloIdentity\Entity\Ultima;

use Zend\Validator\StringLength;
use Zend\EventManager\EventManagerAwareInterface;
use Solo\EventManager\ProvidesEvents;
use Zend\Validator\EmailAddress;

class AgentRequisites implements EventManagerAwareInterface {

    use ProvidesEvents;

    /**
     *
     * @var Agent
     */
    private $agent = null;

    /**
     *
     * @var integer
     */
    private $inn = null;

    /**
     *
     * @var integer
     */
    private $kpp = null;

    /**
     *
     * @var integer
     */
    private $bic = null;

    /**
     *
     * @var integer
     */
    private $okpo = null;

    /**
     *
     * @var string
     */
    private $address = null;

    /**
     *
     * @var string
     */
    private $bankName = null;

    /**
     *
     * @var string
     */
    private $email = null;

    /**
     *
     * @var integer
     */
    private $settlementAccount = null;

    /**
     *
     * @var integer
     */
    private $bankCorrAccount = null;
    
    /**
     * 
     * @var string
     */
    private $opf = null;
    
    /**
     * 
     * @var string
     */
    private $ogrn = null;
    
    /**
     * 
     * @var \DateTime
     */
    private $ogrnDate = null;

    /**
     *
     * @return \SoloIdentity\Entity\Ultima\Agent
     */
    public function getAgent() {
        return $this->agent;
    }

    /**
     *
     * @param \SoloIdentity\Entity\Ultima\Agent $agent        	
     */
    public function setAgent($agent) {
        $this->agent = $agent;
        return $this;
    }
    

    /**
     *
     * @return integer
     */
    public function getInn() {
        return $this->inn;
    }

    /**
     * 
     * @param integer $inn
     * @throws \InvalidArgumentException
     * @return \SoloIdentity\Entity\Ultima\AgentRequisites
     */
    public function setInn($inn) {
//		if (!is_integer($inn)) {
//			throw new \InvalidArgumentException('INN must be integer');
//		}
        if ((null !== $this->agent) && $this->getAgent()->isNaturalPerson() && !(new StringLength(['min' => 12, 'max' => 12]))->isValid(strval($inn))) {
            throw new \InvalidArgumentException('INN must contain 12 characters for natural person');
        }
        if ((null !== $this->agent) && $this->getAgent()->isCommercialOrganization() && !(new StringLength(['min' => 10, 'max' => 10]))->isValid(strval($inn))) {
            throw new \InvalidArgumentException('INN must contain 10 characters for commercial organization');
        }

        $args = $this->events()->prepareArgs(compact('inn'));
        $this->events()->trigger('change.pre', $this, $args);

        $this->inn = $inn;

        $this->events()->trigger('change.post', $this, $args);

        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getKpp() {
        return $this->kpp;
    }

    /**
     * 
     * @param integer $kpp
     * @throws \InvalidArgumentException
     * @return \SoloIdentity\Entity\Ultima\AgentRequisites
     */
    public function setKpp($kpp) {
//		if (!is_integer($kpp)) {
//			throw new \InvalidArgumentException('KPP must be integer');
//		}
//        if (!(new StringLength(['min' => 9, 'max' => 9]))->isValid(strval($kpp))) {
//            throw new \InvalidArgumentException('KPP must contain 9 characters');
//        }

        $args = $this->events()->prepareArgs(compact('kpp'));
        $this->events()->trigger('change.pre', $this, $args);

        $this->kpp = $kpp;

        $this->events()->trigger('change.post', $this, $args);

        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getBic() {
        return $this->bic;
    }

    /**
     * 
     * @param integer $bic
     * @throws \InvalidArgumentException
     * @return \SoloIdentity\Entity\Ultima\AgentRequisites
     */
    public function setBic($bic) {
//        if (!is_integer($bic)) {
//            throw new \InvalidArgumentException('BIC must be integer');
//        }
//        if (!(new StringLength(['min' => 9, 'max' => 9]))->isValid(strval($bic))) {
//            throw new \InvalidArgumentException('BIC must contain 9 characters');
//        }

        $args = $this->events()->prepareArgs(compact('bic'));
        $this->events()->trigger('change.pre', $this, $args);

        $this->bic = $bic;

        $this->events()->trigger('change.post', $this, $args);

        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getOkpo() {
        return $this->okpo;
    }

    /**
     * 
     * @param integer $okpo
     * @throws \InvalidArgumentException
     * @return \SoloIdentity\Entity\Ultima\AgentRequisites
     */
    public function setOkpo($okpo) {
//        if (!is_integer($okpo)) {
//            throw new \InvalidArgumentException('OKPO must be integer');
//        }
//        if (!(new StringLength(['min' => 8, 'max' => 9]))->isValid(strval($okpo))) {
//            throw new \InvalidArgumentException('OKPO must contain 8 or 9 characters');
//        }

        $args = $this->events()->prepareArgs(compact('okpo'));
        $this->events()->trigger('change.pre', $this, $args);

        $this->okpo = $okpo;

        $this->events()->trigger('change.post', $this, $args);

        return $this;
    }

    /**
     *
     * @return string
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     *
     * @param string $address
     * @return \SoloIdentity\Entity\Ultima\AgentRequisites        	
     */
    public function setAddress($address) {
        $this->address = $address;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getBankName() {
        return $this->bankName;
    }

    /**
     *
     * @param string $bankName        	
     * @return \SoloIdentity\Entity\Ultima\AgentRequisites
     */
    public function setBankName($bankName) {
        $args = $this->events()->prepareArgs(compact('bankName'));
        $this->events()->trigger('change.pre', $this, $args);

        $this->bankName = $bankName;

        $this->events()->trigger('change.post', $this, $args);

        return $this;
    }

    /**
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     *
     * @param string $email        
     * @throws \InvalidArgumentException	
     * @return \SoloIdentity\Entity\Ultima\AgentRequisites
     */
    public function setEmail($email) {
        if (false && $email && !(new EmailAddress())->isValid($email)) {
            throw new \InvalidArgumentException('Email must be well-formed email address');
        }

        $args = $this->events()->prepareArgs(compact('email'));
        $this->events()->trigger('change.pre', $this, $args);

        $this->email = $email;

        $this->events()->trigger('change.post', $this, $args);

        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getSettlementAccount() {
        return $this->settlementAccount;
    }

    /**
     * 
     * @param integer $settlementAccount
     * @throws \InvalidArgumentException
     * @return \SoloIdentity\Entity\Ultima\AgentRequisites
     */
    public function setSettlementAccount($settlementAccount) {
//        if (!is_integer($settlementAccount)) {
//            throw new \InvalidArgumentException('Account settlement account must be integer');
//        }

        $args = $this->events()->prepareArgs(compact('settlementAccount'));
        $this->events()->trigger('change.pre', $this, $args);

        $this->settlementAccount = $settlementAccount;

        $this->events()->trigger('change.post', $this, $args);

        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getBankCorrAccount() {
        return $this->bankCorrAccount;
    }

    /**
     * 
     * @param integer $bankCorrAccount
     * @throws \InvalidArgumentException
     * @return \SoloIdentity\Entity\Ultima\AgentRequisites
     */
    public function setBankCorrAccount($bankCorrAccount) {
//        if (!is_integer($bankCorrAccount)) {
//            throw new \InvalidArgumentException('Account correspondential account must be integer');
//        }

        $args = $this->events()->prepareArgs(compact('bankCorrAccount'));
        $this->events()->trigger('change.pre', $this, $args);

        $this->bankCorrAccount = $bankCorrAccount;

        $this->events()->trigger('change.post', $this, $args);

        return $this;
    }
	/**
	 * @return string
	 */
	public function getOpf() {
		return $this->opf;
	}


	/**
	 * @param string $opf
	 */
	public function setOpf($opf) {
		$this->opf = $opf;
		return $this;
	}
	/**
	 * @return string
	 */
	public function getOgrn() {
		return $this->ogrn;
	}


	/**
	 * @param string $ogrn
	 */
	public function setOgrn($ogrn) {
		$this->ogrn = $ogrn;
		return $this;
	}


	/**
	 * @return DateTime
	 */
	public function getOgrnDate() {
		return $this->ogrnDate;
	}


	/**
	 * @param DateTime $ogrnDate
	 */
	public function setOgrnDate($ogrnDate) {
		$this->ogrnDate = $ogrnDate;
		return $this;
	}





}

?>