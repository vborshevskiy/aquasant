<?php

namespace SoloIdentity\Entity\Ultima;

use SoloIdentity\Entity\AbstractUser;
use SoloIdentity\Entity\Ultima\AgentCollection;
use SoloIdentity\Entity\Ultima\UserAddressCollection;
use Zend\EventManager\EventManagerAwareInterface;
use Solo\EventManager\ProvidesEvents;

class User extends AbstractUser implements \Serializable, EventManagerAwareInterface {
	
	use ProvidesEvents;
	
	use ProvidesUserBalance;

	/**
	 *
	 * @var integer
	 */
	const TYPE_NORMAL = 1;

	/**
	 *
	 * @var integer
	 */
	const TYPE_SUPER = 2;

	/**
	 *
	 * @var integer
	 */
	private $naturalPersonId = null;

	/**
	 *
	 * @var integer
	 */
	private $priceCategoryId = null;

	/**
	 *
	 * @var integer
	 */
	private $typeId = null;

	/**
	 *
	 * @var AgentCollection
	 */
	private $agentList;

	/**
	 *
	 * @var AddressCollection
	 */
	private $addressList;

	/**
	 *
	 * @var AddressCollection
	 */
	private $postAddressList;

	/**
	 *
	 * @var boolean
	 */
	private $isManager = false;

	/**
	 *
	 * @var boolean
	 */
	private $isSuperManager = false;

	/**
	 *
	 * @var integer
	 */
	private $managerId = -1;
	
	/**
	 * 
	 * @var string
	 */
	private $password = '';

	/**
	 *
	 * @var Agent
	 */
	private $currentAgent = null;

	public function __construct() {
		$this->agentList = new AgentCollection();
		$this->addressList = new AddressCollection();
		$this->postAddressList = new AddressCollection();
	}

	/**
	 *
	 * @return integer
	 */
	public function getNaturalPersonId() {
		return $this->naturalPersonId;
	}

	/**
	 *
	 * @param integer $naturalPersonId        	
	 * @throws \InvalidArgumentException
	 * @return \SoloIdentity\Entity\Ultima\User
	 */
	public function setNaturalPersonId($naturalPersonId) {
		if (!is_integer($naturalPersonId)) {
			throw new \InvalidArgumentException('Natural person id must be integer');
		}
		
		$args = $this->events()->prepareArgs(compact('naturalPersonId'));
		$this->events()->trigger('change.pre', $this, $args);
		
		$this->naturalPersonId = $naturalPersonId;
		
		$this->events()->trigger('change.post', $this, $args);
		return $this;
	}

	/**
	 *
	 * @return integer
	 */
	public function getPriceCategoryId() {
		if (null !== $this->priceCategoryId) {
			return $this->priceCategoryId;
		}
		if ($this->hasCurrentAgent() && (null !== $this->getCurrentAgent()->getPriceCategoryId())) {
			return $this->getCurrentAgent()->getPriceCategoryId();
		}
		return -1;
	}

	/**
	 *
	 * @param integer $priceCategoryId        	
	 * @throws \InvalidArgumentException
	 * @return \SoloIdentity\Entity\Ultima\User
	 */
	public function setPriceCategoryId($priceCategoryId) {
		if (!is_integer($priceCategoryId)) {
			throw new \InvalidArgumentException('Price category id must be integer');
		}
		
		$args = $this->events()->prepareArgs(compact('priceCategoryId'));
		$this->events()->trigger('change.pre', $this, $args);
		
		$this->priceCategoryId = $priceCategoryId;
		
		$this->events()->trigger('change.post', $this, $args);
		
		return $this;
	}

	/**
	 *
	 * @return integer $typeId
	 */
	public function getTypeId() {
		return $this->typeId;
	}

	/**
	 *
	 * @param integer $typeId        	
	 * @throws \InvalidArgumentException
	 * @return \SoloIdentity\Entity\Ultima\User
	 */
	public function setTypeId($typeId) {
		if (!is_integer($typeId)) {
			throw new \InvalidArgumentException('Type id must be integer');
		}
		
		$args = $this->events()->prepareArgs(compact('typeId'));
		$this->events()->trigger('change.pre', $this, $args);
		
		$this->typeId = $typeId;
		
		$this->events()->trigger('change.post', $this, $args);
		return $this;
	}

	/**
	 *
	 * @return \SoloIdentity\Entity\AgentCollection
	 */
	public function agents() {
		return $this->agentList;
	}

	/**
	 *
	 * @return boolean
	 */
	public function hasAgents() {
		return (count($this->agentList) > 0);
	}

	/**
	 *
	 * @return \SoloIdentity\Entity\AgentCollection
	 */
	public function addresses() {
		return $this->addressList;
	}

	/**
	 *
	 * @return \SoloIdentity\Entity\AgentCollection
	 */
	public function postAddresses() {
		return $this->postAddressList;
	}

	/**
	 *
	 * @param AddressInterface $address        	
	 * @return Address
	 */
	public function addAddress(AddressInterface $address) {
		$this->addresses()->add($address);
	}

	/**
	 *
	 * @param Agent $agent        	
	 * @return \SoloIdentity\Entity\Ultima\User
	 */
	public function setCurrentAgent($agent) {
		$args = $this->events()->prepareArgs(compact('agent'));
		$this->events()->trigger('currentAgentChange.pre', $this, $args);
		
		$this->currentAgent = $agent;
		
		$this->events()->trigger('currentAgentChange.post', $this, $args);
		
		return $this;
	}

	/**
	 *
	 * @return \SoloIdentity\Entity\Ultima\Agent
	 */
	public function getCurrentAgent() {
		return $this->currentAgent;
	}

	/**
	 * Checks has user current selected agent
	 *
	 * @return boolean
	 */
	public function hasCurrentAgent() {
		return (null !== $this->currentAgent);
	}

	/**
	 *
	 * @return boolean
	 */
	public function isNormalUser() {
		return (self::TYPE_NORMAL == $this->getTypeId());
	}

	/**
	 *
	 * @return boolean
	 */
	public function isSuperUser() {
		return (self::TYPE_SUPER == $this->getTypeId());
	}

	/**
	 * Add agent to current user
	 *
	 * @param Agent $agent        	
	 * @return Agent
	 */
	public function addAgent(Agent $agent) {
		$agent->setUser($this);
		$this->agents()->add($agent);
		return $agent;
	}

	public function replaceAgent(Agent $agent) {
		foreach ($this->agents() as $key => $ag) {
			if ($agent->getId() == $ag->getId()) {
				$this->agents()->removeAt($key);
				return;
			}
		}
	}

	/**
	 *
	 * @return boolean
	 */
	public function isFake() {
		return false;
	}

	public function serialize() {
		$data = new \stdClass();
		// $data->agentList = $this->agents();
		// $data->addressList = $this->addresses();
		
		if ($this->getCurrentAgent()) {
			$data->currentAgent = $this->getCurrentAgent()->getId();
		}
		
		$data->id = $this->getId();
		$data->name = $this->getName();
		$data->email = $this->getEmail();
		$data->phone = $this->getPhone();
		$data->isManager = $this->isManager();
		$data->isSuperManager = $this->isSuperManager();
		$data->managerId = $this->getManagerId();
		$data->securityKey = $this->getSecurityKey();
		$data->bonusBalance = $this->getBonusBalance();
		$data->password = $this->getPassword();
		$data->bonusBalanceExpireSeconds = $this->bonusBalanceExpireSeconds;
		$data->bonusBalanceLastUpdateTimestamp = $this->bonusBalanceLastUpdateTimestamp;
		$result = \Zend\Serializer\Serializer::serialize($data);
		return $result;
	}

	public function unserialize($serialized) {
		/* @var $data \SoloIdentity\Entity\Ultima\User */
		$data = \Zend\Serializer\Serializer::unserialize($serialized);
		$this->setId($data->id);
		$this->setName($data->name);
		$this->setEmail($data->email);
		$this->setPhone($data->phone);
		$this->isManager($data->isManager);
		$this->isSuperManager($data->isSuperManager);
		$this->setManagerId($data->managerId);
		$this->setSecurityKey($data->securityKey);
		$this->setPassword($data->password);
		$this->bonusBalance = (float)$data->bonusBalance;
		$this->bonusBalanceExpireSeconds = (int)$data->bonusBalanceExpireSeconds;
		$this->bonusBalanceLastUpdateTimestamp = (int)$data->bonusBalanceLastUpdateTimestamp;
		
		if (isset($data->currentAgent)) {
			$this->setCurrentAgent($data->currentAgent);
		}
	}

	/**
	 *
	 * @param boolean $value        	
	 * @return \SoloIdentity\Entity\Ultima\User | boolean
	 */
	public function isManager($value = null) {
		if (null !== $value) {
			$this->isManager = $value;
			return $this;
		} else {
			return $this->isManager;
		}
	}

	/**
	 *
	 * @param boolean $value        	
	 * @return \SoloIdentity\Entity\Ultima\User | boolean
	 */
	public function isSuperManager($value = null) {
		if (null !== $value) {
			$this->isSuperManager = $value;
			return $this;
		} else {
			return $this->isSuperManager;
		}
	}

	/**
	 *
	 * @return number
	 */
	public function getManagerId() {
		return $this->managerId;
	}

	/**
	 *
	 * @param number $managerId        	
	 */
	public function setManagerId($managerId) {
		$this->managerId = $managerId;
		return $this;
	}
	/**
	 * @return string
	 */
	public function getPassword() {
		return $this->password;
	}


	/**
	 * @param string $password
	 */
	public function setPassword($password) {
		$this->password = $password;
		return $this;
	}



}

?>