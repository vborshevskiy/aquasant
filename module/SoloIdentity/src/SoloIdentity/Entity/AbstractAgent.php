<?php

namespace SoloIdentity\Entity;

abstract class AbstractAgent {

	/**
	 *
	 * @var integer
	 */
	private $id = null;

	/**
	 *
	 * @var string
	 */
	private $name = null;

	/**
	 *
	 * @var AbstractUser
	 */
	private $user = null;

	/**
	 *
	 * @return integer $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * 
	 * @param integer $id
	 * @throws \InvalidArgumentException
	 * @return \SoloIdentity\Entity\AbstractAgent
	 */
	public function setId($id) {
//		if (!is_integer($id)) {
//			throw new \InvalidArgumentException('Id must be integer');
//		}
		$this->id = $id;
		return $this;
	}

	/**
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * 
	 * @param string $name
	 * @return \SoloIdentity\Entity\AbstractAgent
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	/**
	 *
	 * @return AbstractUser $user
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * 
	 * @param AbstractUser $user
	 * @return \SoloIdentity\Entity\AbstractAgent
	 */
	public function setUser(AbstractUser $user) {
		$this->user = $user;
		return $this;
	}

}

?>