<?php

namespace SoloIdentity\Entity;

abstract class AbstractPhone {

	/**
	 *
	 * @var string
	 */
	private $number = null;

	/**
	 *
	 * @return string
	 */
	public function getNumber() {
		return $this->number;
	}

	/**
	 * 
	 * @param string $number
	 * @return \SoloIdentity\Entity\AbstractPhone
	 */
	public function setNumber($number) {
		$this->number = $number;
		return $this;
	}

}

?>