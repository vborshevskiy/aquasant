<?php

namespace SoloIdentity\Entity;

final class RegisterGenericParameters implements \ArrayAccess {

	/**
	 * Parameters set
	 *
	 * @var array
	 */
	private $params = [];

	/**
	 * Password
	 *
	 * @var string
	 */
	private $password = null;

	/**
	 * Add param
	 *
	 * @param string $name        	
	 * @param mixed $value        	
	 * @param boolean $required        	
	 */
	public function addParam($name, $value, $required = false) {
		$this->params[$name] = [
			'value' => $value,
			'required' => $required 
		];
	}

	/**
	 * Returns specified parameter value
	 *
	 * @param string $name        	
	 * @throws \OutOfBoundsException
	 */
	public function getParam($name) {
		if (!array_key_exists($name, $this->params)) {
			throw new \OutOfBoundsException('Parameter \'' . $name . '\' doesn\'t exists');
		}
		return $this->params[$name]['value'];
	}

	/**
	 * Checks is specified parameter required
	 *
	 * @param string $name        	
	 * @throws \OutOfBoundsException
	 */
	public function isRequiredParam($name) {
		if (!array_key_exists($name, $this->params)) {
			throw new \OutOfBoundsException('Parameter \'' . $name . '\' doesn\'t exists');
		}
		return $this->params[$name]['required'];
	}

	/**
	 * Sets password
	 *
	 * @param unknown $password        	
	 */
	public function setPassword($password) {
		$this->password = $password;
	}

	/**
	 * Gets password
	 *
	 * @return string
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * Check is password empty
	 *
	 * @return boolean
	 */
	public function isPasswordEmpty() {
		return (null === $this->password);
	}

	/**
	 * ArrayAccess implementation
	 *
	 * @param string $offset        	
	 * @param mixed $value        	
	 * @throws \InvalidArgumentException
	 */
	public function offsetSet($offset, $value) {
		if (is_numeric($offset)) {
			throw new \InvalidArgumentException('Parameter \'' . $offset . '\' can\'t be numeric');
		}
		$this->params[$offset] = $value;
	}

	/**
	 * ArrayAccess implementation
	 *
	 * @param string $offset        	
	 * @throws \OutOfBoundsException
	 * @return multitype:
	 */
	public function offsetGet($offset) {
		if (!array_key_exists($offset, $this->params)) {
			throw new \OutOfBoundsException('Parameter ' . $offset . ' doesn\'t exists');
		}
		return $this->params[$offset]['value'];
	}

	/**
	 * ArrayAccess implementation
	 *
	 * @param string $offset        	
	 * @return boolean
	 */
	public function offsetExists($offset) {
		return array_key_exists($offset, $this->params);
	}

	/**
	 * ArrayAccess implementation
	 *
	 * @param string $offset        	
	 * @return boolean
	 */
	public function offsetUnset($offset) {
		if (array_key_exists($offset, $this->params)) {
			unset($this->params, $offset);
			return true;
		}
		return false;
	}

}

?>