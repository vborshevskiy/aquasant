<?php

namespace SoloIdentity\Data;

use Solo\Db\QueryGateway\QueryGateway;
use SoloCache\Service\ProvidesCache;

/**
 * Delivery costs Data provider for addresses 
 *
 * @author slava
 */
class DeliveryCostGateway extends QueryGateway {
    
    use ProvidesCache;

    public function getDeliveryCostsByPostIndex($postIndex) {
        $cacher = $this->createMethodCacher(__METHOD__, func_get_args());
        if ($cacher->has())
            return $cacher->get();

        $sql = "SELECT LightCost, HardCost FROM #postindex_costs# WHERE PostIndex=" . $postIndex;

        $rows = $this->query($sql);
        if (0 < $rows->count()) {
            $result = $rows->current();
            $cacher->set($result);
            return $result;
        }
        return null;
    }

}

?>
