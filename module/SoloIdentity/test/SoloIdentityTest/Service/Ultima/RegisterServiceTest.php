<?php

namespace SoloIdentityTest\Service\Ultima;

/**
 * RegisterServiceTest
 *
 * @author slava
 */
class RegisterServiceTest extends \PHPUnit_Framework_TestCase {

    use \SoloERP\Service\ProvidesWebservice;

    /**
     *
     * @var \SoloIdentity\Service\Ultima\UserService
     */
    public $userService = null;

    /**
     *
     * @var \SoloIdentity\Service\LoginEditorService
     */
    public $loginEditorService = null;
    public $registerResult = null;
    public $identityInfo = [];
    public $userId = [];
    public $isNewLogin = false;

    public function setUp() {
        $serviceManager = \SoloIdentityTest\Bootstrap::getServiceManager();
        $this->userService = $serviceManager->get('user_service');
    }

    public function loginProvider() {
        $prefix = "+7";
        return array(
            array($prefix . \Zend\Math\Rand::getString(10, '0123456789'), 'test@mail.com', 'Test Test', true),
        );
    }

    /**
     * @test
     * @dataProvider loginProvider
     * @expectedException PHPUnit_Framework_ExpectationFailedException
     */
    public function testRegister($phone, $email, $fio, $sendSms) {
        $registerResult = $this->userService->register($phone, $email, $fio, $sendSms);
        $this->registerResult = $registerResult;
        try {
            $this->assertEquals(get_class($registerResult), '\SoloIdentity\Service\Ultima\Result\RegisterResult');
            $isNewUser = false;
            try {
                $this->assertInternalType(PHPUnit_Framework_Constraint_IsType::TYPE_STRING, $registerResult->password);
                $this->assertInternalType(PHPUnit_Framework_Constraint_IsType::TYPE_INT, $registerResult->userId);
                $this->userId = $registerResult->userId;
                $this->isNewLogin = true;
                static::$identityInfo = array(
                    'phone' => $phone,
                    'email' => $email,
                    'password' => $registerResult->password
                );
                $isNewUser = true;
            } catch (PHPUnit_Framework_ExpectationFailException $e) {
                $this->assertInternalType(PHPUnit_Framework_Constraint_IsType::TYPE_INT, $registerResult->getErrorCode());
            }
        } catch (PHPUnit_Framework_ExpecatitonFailedException $ex) {
            $this->fail("Incorrect type of register result!");
        }
    }

}

?>
