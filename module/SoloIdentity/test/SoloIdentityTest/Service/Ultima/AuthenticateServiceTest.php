<?php
namespace SoloIdentityTest\Service\Ultima;
use SoloIdentityTest\Service\Ultima\RegisterServiceTest;

/**
 * AuthenticateServiceTest
 *
 * @author slava
 */
class AuthenticateServiceTest extends \PHPUnit_Framework_TestCase {
    
    /**
     *
     * @var \SoloIdentity\Service\Ultima\AuthenticateService
     */
    public $authService = null;
    
    public function setUp() {
        $serviceManager = \SoloIdentityTest\Bootstrap::getServiceManager();
        $this->authService = $serviceManager->get('authenticate_service');
    }
    
    public function loginsProvider() {
        return array(
            array('dev@tutrinov.pro', 'i1kVV4'),
            array('+79046114067', 'i1kVV4'),
        );
    }
    
    /**
     * @dataProvider loginsProvider
     * @expectedException PHPUnit_Framework_ExpectationFailedException
     */
    public function testAuthenticate($login, $password) {
        try {
            $authResult = $this->authService->authenticate($login, $password);
            try {
                $this->assertEquals(get_class($authResult), 'SoloIdentity\Service\Ultima\Result\AuthResult');
                $user = $authResult->user;
                $this->assertEquals(get_class($user), 'SoloIdentity\Entity\Ultima\User');
                $agentsCollection = $user->agents();
                $this->assertEquals(get_class($agentsCollection), 'SoloIdentity\Entity\Ultima\AgentCollection');
                $naturalPerson = $user->getCurrentAgent();
                $naturalPersonId = $user->getNaturalPersonId();
                $defaultAgentId = $naturalPerson->getId();
                $this->assertEquals($defaultAgentId, $naturalPersonId);
                $this->assertEquals(get_class($naturalPerson->addresses()), 'SoloIdentity\Entity\Ultima\AddressCollection');
            } catch (PHPUnit_Framework_ExpectationFailedException $ex) {
                $this->assertType(PHPUnit_Framework_Constraint_IsType::TYPE_INT, $authResult->getErrorCode());
                $this->assertTrue(($authResult->getErrorCode() == \SoloIdentity\Service\Ultima\Result\AuthResult::ERROR_PHONE_PASS_PAIR_NOT_EXISTS || $authResult->getErrorCode() == \SoloIdentity\Service\Ultima\Result\AuthResult::ERROR_UNDEFINED));
            }
        } catch (\RuntimeException $ex) {
            $this->assertEquals('User don\'t have natural person agent', $ex->getMessage());
        }
    }
    
    public function tearDown() {
        
    }
    
}

?>
