<?php
namespace SoloTest\SoloCabinetTest\Service\Ultima;
/**
 * Description of ReserveServiceTest
 *
 * @author slava
 */
class ReserveServiceTest extends \PHPUnit_Framework_TestCase {
    
    /**
     *
     * @var \SoloCabinet\Service\Ultima\ReservesService
     */
    protected $service = null;
    protected $sm = null;
    
    public function setUp() {
        $this->sm = \SoloTest\Bootstrap::getServiceManager();
        \Zend\Mvc\Application::init(\SoloTest\Bootstrap::$config);
        $this->service = $this->sm->get('\SoloCabinet\Service\ReservesService');
    }
    
    public function reserveProvider() {
        return [
            [5743102,2208948,25883452],
        ];
    }
    
    /**
     * 
     * @dataProvider reserveProvider
     */
    public function testGetReserve($userId, $agentId, $docId) {
        $result = $this->service->getReserve($userId, $agentId, $docId);
    }
    
}

?>
