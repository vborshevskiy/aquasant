<?php
return array(
    'modules' => array(
        'Solo',
        'SoloDelivery',
        'Application',
        'SoloWarehouse',
        'SoloERP',
        'SoloIdentity',
        'SoloCompare',
        'SoloCatalog',
        'SoloSettings',
        'SoloCache',
        'SoloKladr',
        'SoloSearch',
        'SoloOrder',
        'SoloCabinet',
        'Predis',
        'Smarty',
    ),
    'module_listener_options' => array(
        'config_glob_paths'    => array(
            '../config/autoload/{,*.}{global,local}.php',
        ),
        'module_paths' => array(
            'module',
            'vendor',
        ),
    ),
);
?>
