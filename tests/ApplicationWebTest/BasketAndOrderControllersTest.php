<?php

namespace SoloTest\ApplicationWebTest;

/**
 * BasketAndOrderControllersTest
 *
 * @author slava
 */
class BasketAndOrderControllersTest extends \PHPUnit_Extensions_Selenium2TestCase {

    /**
     *
     * @var \PHPUnit_Extensions_SeleniumTestCase
     */
    public static $inst = null;
    
    public function setUp() {
        if (static::$inst === null) {
            static::$inst = $this;
            $this->setBrowserUrl("http://localhost:5113/");
            $this->setBrowser("*firefox");
            $this->shareSession(true);
        }
    }
    
    /**
     * @group logged
     */
    public function testLoginAction() {
        $this->url('/login/');
        $this->timeouts()->implicitWait(20000);
        $this->byId("page-login")->click();
        $this->byId("page-login")->value("9046114067");
        $this->byId("page-pass")->click();
        $this->byId("page-pass")->value("i1kVV4");
        $this->byId("page-pass")->click();
        $this->keys("\n");
        $this->timeouts()->implicitWait(20000);
    }

    public function testBasketPutoneAction() {
        \Zend\Mvc\Application::init(\SoloTest\Bootstrap::$config);
        $serviceManager = \SoloTest\Bootstrap::getServiceManager();
        $this->controller = new \Application\Controller\BasketController();
        $this->request = new \Zend\Http\PhpEnvironment\Request;
        $this->routeMatch = new \Zend\Mvc\Router\RouteMatch(array('controller' => 'basket'));
        $this->event = new \Zend\Mvc\MvcEvent;
        $config = $serviceManager->get('Config');
        $routeConfig = isset($config['router']) ? $config['router'] : array();
        $router = \Zend\Mvc\Router\Http\TreeRouteStack::factory($routeConfig);

        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($serviceManager);

        /* init required controller plugins */
        $this->controller->getPluginManager()
                ->setInvokableClass('catalog', 'SoloCatalog\Controller\Plugin\Catalog')
                ->setInvokableClass('basket', 'SoloBasket\Controller\Plugin\Basket')
                ->setInvokableClass('warehouses', 'SoloWarehouse\Controller\Plugin\Warehouses');

        $this->warehouses = $this->controller->stores()->enumStoreIds();

        $availCategories = $this->controller->catalog()->menu()->enumAllAvailCategoryIds();
        shuffle($availCategories);
        $filters = new \SoloCatalog\Entity\Filters\FilterSet();
        for ($i = 0; $i < 10; $i++) {
            $category = current($availCategories);
            $this->controller->catalog()->filters()->fillFeatureFiltersByCategoryId($filters, intval($category), $this->controller->getConfig('city.id'));
        }
        $goodIds = $filters->enumGoodIds();
        shuffle($goodIds);
        $options = new \SoloCatalog\Entity\Goods\FilterOptions();
        $k = (sizeof($goodIds) <= 10) ? sizeof($goodIds) : 10;
        for ($i = 0; $i < $k; $i++) {
            $options->addGoodId($goodIds[$i]);
        }
        $goods = $this->controller->catalog()->goods()->enumFilterGoods($options, $this->controlerr->getConfig('city.id'));
        $return = array();
        $warehouses = $this->warehouses;
        array_walk($goods, function($val, $key) use(&$return, &$warehouses, &$k) {
                    $wh = null;
                    $quantity = null;
                    shuffle($warehouses);
                    foreach ($warehouses as $warehouseId) {
                        $whQuantity = $val->getWarehouseQuantity($warehouseId);
                        if ($whQuantity > 0) {
                            $wh = $warehouseId;
                            $quantity = $whQuantity;
                            break;
                        }
                    }
                    if (in_array($wh, array(67,1))) {
                        $return[] = array($val, $wh, $quantity);
                    }
                });
        $i = 0;
        foreach ($return as $vr) {
            $this->url("basket/putone?id=" . $vr[0]->getId() . "&count=" . $vr[2] . "&from=" . $vr[1]);
            $this->timeouts()->implicitWait(20000);
            file_put_contents("/home/www/JR/www/data/cache/test_".($vr[0]->getId())."_".$vr[2]."_".$vr[1].".png", $this->currentScreenshot());
            $this->timeouts()->implicitWait(30000);
        }
        $this->url("basket/state");
        $this->timeouts()->implicitWait(20000);
        file_put_contents("/home/www/JR/www/data/cache/state.png", $this->currentScreenshot());
        $this->timeouts()->implicitWait(20000);
    }

    /**
     * @depends testBasketPutoneAction
     */
    public function testBasketIndexAction() {
        $seleniumResponse = $this->url("basket/index");
//        $this->timeouts()->implicitWait(10000);
//        file_put_contents("/home/www/JR/www/data/cache/testbasket.png", $this->currentScreenshot());
//        $this->timeouts()->implicitWait(10000);
    }
    
    /**
     * @depends testBasketIndexAction
     */
    public function testOrderIndexPage() {
        $seleniumResponse = $this->url("order/index");
        $this->timeouts()->implicitWait(10000);
        file_put_contents("/home/www/JR/www/data/cache/testorder.png", $this->currentScreenshot());
        $this->timeouts()->implicitWait(10000);
    }
    
    public function testSpecificOrder() {
        $links = $this->elements($this->using('css selector')->value('*[class="order-link"]'));
        /*@var $link \PHPUnit_Extensions_Selenium2TestCase_Element */
        $i = sizeof($links);
        for ($j = 0; $j < $i; $j++) {
            $this->timeouts()->implicitWait(20000);
            $links = $this->elements($this->using('css selector')->value('*[class="order-link"]'));
            $link = $links[$j];
            $link->click();
            $this->timeouts()->implicitWait(60000);
            file_put_contents("/home/www/JR/www/data/cache/test_order".$j.".png", $this->currentScreenshot());
            $this->timeouts()->implicitWait(60000);
            $this->back();
        }
    }
    
    public static function tearDownAfterClass() {
//        static::$inst->stop();
    }

}

?>
