<?php

namespace SoloTest\ApplicationWebTest;

/**
 * Description of CabinetControllerTest
 *
 * @author slava
 */
class CabinetControllerTest extends \PHPUnit_Extensions_Selenium2TestCase {

    /**
     *
     * @var \PHPUnit_Extensions_SeleniumTestCase
     */
    public static $inst = null;

    public function setUp() {
        if (static::$inst === null) {
            static::$inst = $this;
            $this->setBrowserUrl("http://localhost:5113/");
            $this->setBrowser("*firefox");
            $this->shareSession(true);
        }
    }
    
    /**
     * @covers \Application\Controller\LoginController::indexAction()
     */
    public function testLoginAction() {
        $this->url('/login/');
        $this->timeouts()->implicitWait(10000);
        $this->byId("page-login")->click();
        $this->byId("page-login")->value("904611");
        $this->byId("page-pass")->click();
        $this->byId("page-pass")->value("i1kVV4");
        $this->byId("page-pass")->click();
        $this->keys("\n");
        $this->timeouts()->implicitWait(20000);
        $this->assertTrue($this->byCssSelector(".server-error")->displayed());
        $this->assertEquals("Неверный формат телефона или e-mail", $this->byCssSelector(".server-error")->text());
        $this->byId("page-login")->click();
        $this->byId("page-login")->value("9046114067");
        $this->byId("page-pass")->click();
        $this->byId("page-pass")->value("i1kVV4");
        $this->byId("page-pass")->click();
        $this->keys("\n");
        $this->timeouts()->implicitWait(20000);
        file_put_contents("/home/www/JR/www/data/cache/login2.png", $this->currentScreenshot());
        $this->timeouts()->implicitWait(20000);
        $this->moveto($this->byCssSelector(".top .auth"));
        $this->timeouts()->implicitWait(10000);
    }
    
    public function testIndexAction() {
        $this->url('/cabinet/');
        $this->timeouts()->implicitWait(10000);
        file_put_contents("/home/www/JR/www/data/cache/cabinet.png", $this->currentScreenshot());
        $this->timeouts()->implicitWait(10000);
    }
    
    public static function tearDownAfterClass() {}

}

?>
