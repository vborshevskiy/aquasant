<?php
namespace SoloTest\SoloOrderTest\Service;

/**
 * AbstractBasketServiceTest
 *
 * @author slava
 */
abstract class AbstractBasketServiceTest extends \PHPUnit_Framework_TestCase {
    
    protected $sm = null;
    
    /**
     *
     * @var \SoloBasket\Service\AbstractBasketService
     */
    protected $basketService = null;
    
    public function setUp() {
        \Zend\Mvc\Application::init(\SoloTest\Bootstrap::getConfig());
        $this->sm = \SoloTest\Bootstrap::getServiceManager();
        $this->basketService = $this->sm->get('basket_service');
    }
    
    public function testAddGood() {
        $basketGood = $this->sm->get('basket_good_instance');
        $this->assertTrue($basketGood instanceof \SoloOrder\Entity\Basket\AbstractGood);
        $basketGood->setId(123123);
        $basketGood->setQuantity(6);
        $basketGood->setWarehouseId(6);
        $this->basketService->addGood($basketGood);
        $this->assertTrue($this->basketService->getBasketStorage() instanceof \SoloOrder\Entity\Basket\AbstractStorage);
        $collection = $this->basketService->getBasketStorage()->getGoodsCollection();
        $this->assertEquals(1, $collection->count());
        $basketGoodInstance = $collection->current();
        $this->assertEquals(6, $basketGoodInstance->getQuantity());
        $this->assertEquals(6, $basketGoodInstance->getWarehouseId());
        $this->basketService->addGood($basketGood);
        $collection = $this->basketService->getBasketStorage()->getGoodsCollection();
        reset($collection);
        $basketGoodInstance = $collection->current();
        $this->assertEquals(12, $basketGoodInstance->getQuantity());
        $this->assertEquals(6, $basketGoodInstance->getWarehouseId());
        
        $goodWithAnotherWarehouseValue = $this->sm->get('basket_good_instance');
        $goodWithAnotherWarehouseValue->setId(54);
        $goodWithAnotherWarehouseValue->setQuantity(6);
        $goodWithAnotherWarehouseValue->setWarehouseId(2);
        $this->basketService->addGood($goodWithAnotherWarehouseValue);
        $collection = $this->basketService->getGoods();
        $this->assertEquals(2, $collection->count());
        
        
        $cloneGood = clone $goodWithAnotherWarehouseValue;
        $this->basketService->addGood($cloneGood);
        $this->assertEquals(2, $this->basketService->getGoods()->count());
        
        $goods = $this->basketService->getGoods();
        foreach ($goods as $good) {
            if ($good->getId() == 54) {
                $this->assertEquals(12, $good->getQuantity());
            } elseif ($good->getId() == 123123) {
                $this->assertEquals(12, $good->getQuantity());
            }
        }
    }
    
    /**
     * @depends testAddGood
     */
    public function testGetGoods() {
//        $this->markTestSkipped();
        $goodsCollection = $this->basketService->getGoods();
        $this->assertEquals(2, $goodsCollection->count());
        return $goodsCollection;
    }
    
    /**
     * @depends testGetGoods
     */
    public function testRemoveGood($goodsCollection) {
        $this->assertEquals(2, $goodsCollection->count());
        $goodsCollection->rewind();
        $first = $goodsCollection->current();
        $this->basketService->removeGood($first);
        $this->assertEquals(1, $this->basketService->getGoods()->count());
        $second = $goodsCollection->current();
        $this->basketService->removeGood($second);
        $this->assertEquals(0, $this->basketService->getGoods()->count());
    }
    
}

?>
