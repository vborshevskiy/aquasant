<?php
namespace SoloTest\SoloOrderTest\Service\Ultima;

/**
 * Description of Order ServiceTest
 *
 * @author slava
 */
class OrderServiceTest extends \PHPUnit_Framework_TestCase {
    
    /**
     *
     * @var \SoloOrder\Service\Ultima\OrderService
     */
    protected $orderService = null;
    
    public function setUp() {
        $sm = \SoloTest\Bootstrap::getServiceManager();
        $this->orderService = $sm->get('order_service');
    }
    
    public function testGetOrders() {
        $this->markTestSkipped();
        $this->orderService->getOrders();
    }
    
}

?>
