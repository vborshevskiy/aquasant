<?php
namespace SoloTest\SoloOrderTest\Service;

/**
 * Description of Order ServiceTest
 *
 * @author slava
 */
class AbstractOrderServiceTest extends \PHPUnit_Framework_TestCase {
    
    protected $sm = null;
    protected $config = null;
    protected $orders = null;
    
    public function setUp() {
        \Zend\Mvc\Application::init(\SoloTest\Bootstrap::getConfig());
        $this->sm = \SoloTest\Bootstrap::getServiceManager();
        $this->config = \SoloTest\Bootstrap::getConfig();
    }
    
    public function testSetLocalStorage() {
        $localStorage = $this->sm->get('order_storage_instance');
        $orderServiceClass = "\\".get_class($this->sm->get('order_service'));
        $orderService = new $orderServiceClass;
        $this->assertAttributeNotInstanceOf("\SoloOrder\Entity\Order\AbstractStorage", 'localStorage', $orderService);
        $orderService->setLocalStorage($localStorage);
        $this->assertAttributeInstanceOf("\SoloOrder\Entity\Order\AbstractStorage", 'localStorage', $orderService);
        return $orderService;
    }
    
    /**
     * @depends testSetLocalStorage
     */
    public function testGetLocalStorage($orderService) {
        $this->assertTrue(($orderService->getLocalStorage() instanceof \SoloOrder\Entity\Order\AbstractStorage));
    }
    
    public function orderProvider() {
        $orders = [];
        $sm = \SoloTest\Bootstrap::getServiceManager();
        for ($i = 0; $i < 4; $i++) {
            $orderInstance = $sm->get('order_instance');
            $orderInstance->setId($i+1);
            for ($j = 0; $j < 5; $j++) {
                $orderGood = $sm->get('order_good_instance');
                $orderGood->setMarking($j+1000);
                $orderGood->setPrice(500);
                $orderGood->setName("Test order good");
                $orderInstance->addGood($orderGood);
            }
            $orders[] = array($orderInstance);
        }
        $this->orders = $orders;
        return $this->orders;
    }
    
    /**
     * @dataProvider orderProvider
     * @depends testAddOrder
     */
    public function testGetOrder($order) {
        $orderFromSession = $this->sm->get('order_service')->getOrder($order->getId());
        $this->assertEquals($order, $orderFromSession);
        $this->assertAttributeInstanceOf("\SoloOrder\Entity\Order\GoodsCollection", 'orderGoodsCollection', $orderFromSession);
        $firstOrderGood = $orderFromSession->getOrderGoodsCollection()->current();
        $firstOrderGoodBeforeSessionHandling = $order->getOrderGoodsCollection()->current();
        $this->assertEquals($firstOrderGood, $firstOrderGoodBeforeSessionHandling);
    }
    
    public function tearDown() {
        $storage = new \Zend\Session\Storage\SessionStorage;
        $storage->clear();
    }
    
}

?>
