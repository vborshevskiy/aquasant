<?php

namespace SoloTest\SoloOrderTest\Service;

/**
 * Description of BasketServiceTest
 *
 * @author slava
 */
class BasketServiceTest extends AbstractBasketServiceTest {

    /**
     * @depends testAddGood
     */
    public function testGetGroups() {
        $groups = $this->basketService->getGroups();
        $this->assertTrue($groups instanceof \SoloOrder\Entity\Basket\GroupCollection);
        $this->assertEquals(2, $groups->count());
        $firstGroup = $groups->current();
        $this->assertTrue($firstGroup instanceof \SoloOrder\Entity\Basket\AbstractGroup);
        $this->assertAttributeInstanceOf('\SoloOrder\Entity\Basket\GoodsCollection', 'goods', $firstGroup);
        $firstGood = $firstGroup->getGoods()->current();
        return array($firstGood, $firstGroup);
    }
    
    /**
     * @depends testGetGroups
     */
    public function testGetGroup($basketGood) {
        $good = $basketGood[0];
        $group = $basketGood[1];
        $groupFromBasket = $this->basketService->getGroup($good->getWarehouseId(), $good->getDelivery());
        $this->assertEquals($group, $groupFromBasket);
    }

}

?>
